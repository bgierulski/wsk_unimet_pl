<?php
class SpecialOrders extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_user;
	private $_products_count = 0;
	private $_ids = array();
	private $_mpk = array();
	private $_names = array();
	private $_symbols = array();
	private $_producers = array();
	private $_comments = array();
	private $_units = array();
	private $_amounts = array();
	private $_payment;
	private $_document;
	private $_comment;
	private $_special_order = array();
	
	private $_mpk_keys = array();
	private $_budget = array();
	private $_analyst = array();
	private $_manager = array();
	private $_receiving = array();
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		
		$this->_user = $this->getSession('b2b_user');
		$this->_special_order = $this->getSession('b2b_special_order');
		
		if (!is_array($this->_special_order))
		{
			$this->_special_order = array(array(), array());
		}
		
		if (true === $_SESSION["b2b_full_offer"] && $_POST['operation'] == 'to_basket')
		{
			if (!isset($_POST["counts"]))
			{
				$this->_addSpecialOrderElement((int)$_POST['product_id'], (float)str_replace(',', '.', $_POST['count']));
			}
			else 
			{
				$actives = $_POST["actives"];
				$counts = $_POST["counts"];
					
				if (is_array($actives))
				{
					foreach ($actives as $product_id)
					{
						$this->_addSpecialOrderElement($product_id, (float)str_replace(',', '.', $counts[$product_id]));
					}
				}
			}
		}
	
		if ($_POST['operation'] == 'add_special_order')
		{
			$this->_ids = $_POST["ids"];
			$this->_mpk = $_POST["mpk"];
			$this->_names = $_POST["names"];
			$this->_symbols = $_POST["symbols"];
			$this->_producers = $_POST["producers"];
			$this->_comments = $_POST["comments"];
			$this->_amounts = $_POST["amounts"];
			$this->_units = $_POST["units"];
			$this->_payment = $_POST['payment'];
			$this->_document = $_POST['document'];
			$this->_comment = mysql_real_escape_string($_POST['comment']);
			$this->_mpk_keys = $_POST["mpk_keys"];
			$this->_budget = $_POST["budget"];
			$this->_analyst = $_POST["analyst"];
			$this->_manager = $_POST["manager"];
			$this->_receiving = $_POST["receiving"];
			
			$this->_validateOrder();
		}
	}
	
	public function checkUserType()
	{
		if (true === $this->getSession("b2b_full_offer"))
		{
			return false;
		}
		
		$result = $this->_db->query('SELECT individual, what_with_new FROM groups_tree_user WHERE user_id = '.(int)$this->_user['parent_id']) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			mysql_free_result($result);
			
			if ($row['individual'] == 1)
			{
				return true;
			}
		}
		
		return false;
	}
	
	public function getSpecialOrderInfo()
	{
		$counter = 0;
		
		if (is_array($this->_special_order) && !empty($this->_special_order))
		{
			foreach ($this->_special_order[1] as $key => $row)
			{
				$counter++;
			}
		}
		
		return $counter;
	}
	
	public function changeSpecialOrder($id)
	{
		if (is_array($this->_special_order) && !empty($this->_special_order))
		{
			$details = array();
		
			foreach ($this->_special_order[1] as $key => $row)
			{
				if ((int)$row['id'] != $id)
				{
					array_push($details, array(
						'id' => $row['id'], 
						'mpk' => $row['mpk'], 
						'name' => $row['name'], 
						'symbol' => $row['symbol'], 
						'producer' => $row['producer'], 
						'comment' => $row['comment'], 
						'amount' => $row['amount'],
						'unit' => $row['unit']
					));
				}
			}
			
			$this->_special_order[1] = $details;
			$this->setSession('b2b_special_order', $this->_special_order);
		}
	}
	
	public function regroupSpecialOrder()
	{
		if (is_array($this->_special_order) && !empty($this->_special_order))
		{
			foreach ($this->_special_order[1] as $key => $row)
			{
				$values["ids"][$this->_products_count] = (int)$row['id'];
				$values["mpk"][$this->_products_count] = trim($row['mpk']);
				$values["names"][$this->_products_count] = trim($row['name']);
				$values["symbols"][$this->_products_count] = trim($row['symbol']);
				$values["producers"][$this->_products_count] = trim($row['producer']);
				$values["comments"][$this->_products_count] = trim($row['comment']);
				$values["amounts"][$this->_products_count] = trim($row['amount']);
				$values["units"][$this->_products_count] = trim($row['unit']);
				$this->_products_count++;
			}
			
			if (isset($this->_special_order[0][0]))
			{
				$values['document'] = $this->_special_order[0][0];
			}
			if (isset($this->_special_order[0][1]))
			{
				$values['payment'] = $this->_special_order[0][1];
			}
			if (isset($this->_special_order[0][2]))
			{
				$values['comment'] = $this->_special_order[0][2];
			}
			
			$values['products_count'] = $this->_products_count;
			
			$this->setSession("values", $values);
		}
	}

	public function getSpecialOrderYears()
	{
		$query = "SELECT DISTINCT DATE_FORMAT(`o`.`add_date`,'%Y') `year` 
		FROM `orders` `o`
		JOIN `order_details` `od` ON 
		`o`.`id` = `od`.`order_id`  
		WHERE `od`.`query_id` > 0 ORDER BY `o`.`add_date` DESC";

		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getSpecialOrders($year = null)
	{
		$query = "SELECT orders.id, add_date, name, status, symbol, order_details.producer, order_details.query_id, order_details.mpk, orders.comment 
		as order_comment, order_details.description as comment, netto_price, brutto_price, tax, amount, user_id, wsk_mpk.unimet_representative, order_details.unit 
		FROM orders JOIN order_details ON orders.id = order_details.order_id LEFT JOIN wsk_mpk ON orders.id = wsk_mpk.order_id 
		WHERE query_id > 0 AND ";

		if (!is_null($year))
		{
			$query .= "((status IN (4,5) AND orders.add_date >= '" . $year . "-01-01' AND orders.add_date < '" . ($year + 1) . "-01-01') OR status = 8) ";
		}
		else
		{
			$query .= "status IN (4,5,8) ";
		}

		$query .= " ORDER BY orders.add_date DESC";

		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		return $this->_db->mysql_fetch_all($result);
	}
	
	private function _addSpecialOrderElement($product_id, $amount)
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT products.id_hermes, product_versions.name, products.symbol, products.unit, producers.name as producer_name FROM products_status JOIN products ON 
			products_status.id = products.id_hermes JOIN product_versions ON products_status.id = product_versions.product_id JOIN producers ON products.producer_id = producers.id 
			WHERE version_id = 1 AND products_status.user = '.$this->_user['parent_id'].' AND status = 1 AND products_status.id = '.$product_id;
		}
		else
		{
			$query = 'SELECT products.id_hermes, product_versions.name, products.symbol, products.unit, producers.name as producer_name FROM products JOIN product_versions ON 
			products.id_hermes = product_versions.product_id JOIN producers ON products.producer_id = producers.id WHERE version_id = 1 AND products.id_hermes = '.$product_id;
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();

		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			
			switch ($row['unit'])
			{
				case 'SZT': $unit = 8;  break;
				case 'KPL': $unit = 2;  break;
				case 'KG': $unit = 1;  break;
				case 'OP': $unit = 6;  break;
				case 'M2': $unit = 3;  break;
				case 'MB': $unit =5;  break;
				case 'M3': $unit = 4;  break;
				case 'PAR': $unit = 7; break;
				default: $unit = 0; break;
			}
			
			array_push($this->_special_order[1], array('id' => $row['id_hermes'], 'mpk' => '', 'name' => $row['name'], 'symbol' => $row['symbol'], 'producer' => $row['producer_name'], 'comment' => $row['comment'], 'amount' => $amount, 'unit' => $unit));
			$this->setSession('b2b_special_order', $this->_special_order);
			$this->setSession('wsk_special_order_change', $row['id_hermes']);
		}
	}
	
	public function getLimits()
	{
		$result = $this->_db->query('SELECT `days`, query_limit FROM b2b_details WHERE id = 1') or $this->_db->raise_error();
		$row = mysql_fetch_array($result);
		return $row;
	}
	
	private function _validateOrder()
	{
		$this->clear();
		
		$this->_comment = trim(mysql_real_escape_string($_POST['comment']));
		$this->_document = trim(mysql_real_escape_string($_POST['document']));
		$this->_payment = trim(mysql_real_escape_string($_POST['payment']));
		
		if (!$this->validateTextarea($this->_comment))
		{
			$this->_errors++;
			$errors['comment'] = 'Komentarz jest wymagany';
		}
		
		if (is_array($this->_names))
		{
			foreach ($this->_names as $i => $name)
			{
				if ((trim($this->_mpk[$i]) != "" || trim($this->_names[$i]) != "" || trim($this->_symbols[$i]) != "" || trim($this->_producers[$i]) != "" || $this->_units[$i] > 0) && !(float)str_replace(',', '.', $this->_amounts[$i]))
				{
					$this->_errors++;
					$errors['amounts'] = 'Ilość produktów jest wymagana';
				}
				
				if (trim($this->_names[$i]) == "" && (trim($this->_mpk[$i]) != "" || trim($this->_symbols[$i]) != "" || trim($this->_producers[$i]) != "" || $this->_units[$i] > 0 || (float)str_replace(',', '.', $this->_amounts[$i])))
				{
					$this->_errors++;
	
					$errors['names'] = 'Nazwy produktów są wymagane';
				}
				
				if (trim($this->_mpk[$i]) == "" && (trim($this->_names[$i]) != "" || trim($this->_symbols[$i]) != "" || trim($this->_producers[$i]) != "" || $this->_units[$i] > 0 || (float)str_replace(',', '.', $this->_amounts[$i])))
				{
					$this->_errors++;
	
					$errors['mpk'] = 'Numery MPK są wymagane';
				}
				if ($this->_units[$i] == 0 && (trim($this->_names[$i]) != "" || trim($this->_symbols[$i]) != "" || trim($this->_producers[$i]) != "" || trim($this->_mpk[$i]) != "" || (float)str_replace(',', '.', $this->_amounts[$i])))
				{
					$this->_errors++;
	
					$errors['units'] = 'Jednostki miary są wymagane';
				}
				
				if (trim($this->_mpk[$i]) != "" || trim($this->_names[$i]) != "" || trim($this->_symbols[$i]) != "" || $this->_units[$i] > 0 || !(float)str_replace(',', '.', $this->_amounts[$i]))
				{
					$values["ids"][$this->_products_count] = (int)$this->_ids[$i];
					$values["mpk"][$this->_products_count] = trim($this->_mpk[$i]);
					$values["names"][$this->_products_count] = trim($this->_names[$i]);
					$values["symbols"][$this->_products_count] = trim($this->_symbols[$i]);
					$values["producers"][$this->_products_count] = trim($this->_producers[$i]);
					$values["comments"][$this->_products_count] = trim($this->_comments[$i]);
					$values["amounts"][$this->_products_count] = trim($this->_amounts[$i]);
					$values["units"][$this->_products_count] = trim($this->_units[$i]);
					$this->_products_count++;
				}
			}
		}
		
		foreach ($_POST["mpk_keys"] as $key => $mpk)
		{
				
		}
		
		if (!$this->_errors)
		{
			foreach ($this->_mpk_keys as $key => $mpk)
			{
				if ((int)$this->_budget[$key] == 0 || (int)$this->_analyst[$key] == 0 || (int)$this->_manager[$key] == 0 || (int)$this->_receiving[$key] == 0)
				{
					$this->_errors++;
					$errors['paths'] = 'Ścieżki są niekompletne';
					$values['step'] = 2;
				}
			}
		}
		else
		{
			$values['step'] = 1;
		}
		
		if ($this->_errors)
		{
			if (!$this->_products_count)
			{
				$this->_products_count = 1;
			}
		
			$values['payment'] = $this->_payment;
			$values['document'] = $this->_document;
			$values['comment'] = $this->_comment;
			$values['products_count'] = $this->_products_count;
			$values["mpk_keys"] = $this->_mpk_keys;
			$values["budget"] = $this->_budget;
			$values["analyst"] = $this->_analyst;
			$values["manager"] = $this->_manager;
			$values["receiving"] = $this->_receiving;
				
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
			
			$special_order[0] = array($this->_document, $this->_payment, $this->_comment);
			$special_order[1] = array();
			
			if (is_array($this->_names))
			{
				foreach ($this->_names as $i => $name)
				{
					array_push($special_order[1], array('id' => (int)$this->_ids[$i], 'mpk' => $this->_mpk[$i], 'name' => $this->_names[$i], 'symbol' => $this->_symbols[$i], 'producer' => $this->_producers[$i], 'comment' => $this->_comments[$i], 'unit' => $this->_units[$i], 'amount' => (float)str_replace(',', '.', $this->_amounts[$i])));
				}
			}
			
			$this->setSession('b2b_special_order', $special_order);
		}
		else if ($this->_products_count)
		{
			$basket = new Basket();
			$netto_value = $basket->getNettoValue();
		
			$limits = $this->getLimits();
		
			if ($netto_value <= $limits['query_limit'])
			{
				unset($_SESSION["b2b_special_order"]);
				$this->_addSpecialOrder();
			}
		}
	}
	
	private function _addSpecialOrder()
	{
		$prices = new Prices();	
		$orders = new Orders();
		
		foreach ($this->_mpk_keys as $key => $mpk)
		{
			$this->_db->query('INSERT INTO orders VALUES (\'\', \'\', '.$this->_user['id'].', \'0\', \'0\', 8, \''.$this->_document.'\', 
			\''.$this->_payment.'\', \''.$this->_comment.'\',  \''.date('Y-m-d H:i:s').'\', \'0000-00-00\')') or $this->_db->raise_error();
			
			$result = $this->_db->query('SELECT id FROM users WHERE id = '.$this->_user['id'].' AND ordering = 1') or $this->_db->raise_error();
			$order_id = mysql_insert_id();
			
			$netto_sum = 0;
			$brutto_sum = 0;
			
			if ($order_id)
			{
				foreach ($this->_names as $i => $name)
				{
					if (!strcmp($this->_mpk[$i], $mpk))
					{
						$row = array();
						$result = $this->_db->query('SELECT id_hermes, tax FROM products WHERE id_hermes = '.(int)$this->_ids[$i]) or $this->_db->raise_error();
						
						if (mysql_num_rows($result))
						{
							$row = mysql_fetch_array($result);
							$tax = $row['tax'];
							$netto_price = $prices->getPrice($row['id_hermes'], 4);
							$brutto_price = $prices->getPrice($row['id_hermes'], 1);
						}
						else
						{
							$tax = 0;
							$netto_price = 0;
							$brutto_price = 0;
						}
						
						$this->_db->query('INSERT INTO order_details VALUES (\'\', '.$order_id.', '.(int)$row['id_hermes'].', \''.trim(mysql_real_escape_string($this->_names[$i])).'\', \''.trim(mysql_real_escape_string($this->_producers[$i])).'\', 						
						\''.trim(mysql_real_escape_string($this->_symbols[$i])).'\', \''.$this->_units[$i].'\', \''.trim(mysql_real_escape_string($this->_comments[$i])).'\', \''.(float)$netto_price.'\', '.(int)$tax.', \''.(float)$brutto_price.'\', 
						\''.(float)str_replace(',', '.', $this->_amounts[$i]).'\', 0, \''.mysql_real_escape_string($mpk).'\', \'\', 
						'.$order_id.', \'\', 1, 1)') or $this->_db->raise_error();
						
						$netto_sum += ((float)$netto_price * (float)str_replace(',', '.', $this->_amounts[$i]));
						$brutto_sum += ((float)str_replace(',', '.', $this->_amounts[$i]) * (float)$netto_price * (1+($tax / 100)));
					}
				}
				
				$this->_db->query('UPDATE orders SET `netto_value` = `netto_value` + \''.sprintf("%0.2f", $netto_sum).'\', `brutto_value` = `brutto_value` + \''.sprintf("%0.2f", $brutto_sum).'\' 
				WHERE id = '.$order_id);
				
				$result = $this->_db->query('SELECT DISTINCT netto_value, mpk FROM order_details JOIN orders ON order_details.order_id = orders.id WHERE order_details.order_id = '.$order_id) or $this->_db->raise_error();
				
				if (mysql_num_rows($result))
				{
					$order = mysql_fetch_array($result);
					mysql_free_result($result);
					
					$result = $this->_db->query('SELECT email, add_date, login FROM users WHERE id = '.$this->_user['id']) or $this->_db->raise_error();
							
					if (mysql_num_rows($result))
					{
						$contractor = mysql_fetch_array($result);
					}
					else
					{
						$contractor = array('email' => '', 'add_date' => '1970-01-01', 'login' => '1970-01-01');
					}
							
					mysql_free_result($result);
						
					$result = $this->_db->query('SELECT id, email, add_date, login FROM users WHERE id = '.(int)$this->_analyst[$key]) or $this->_db->raise_error();
							
					if (mysql_num_rows($result))
					{
						$analyst = mysql_fetch_array($result);
					}
					else
					{
						$analyst = array('id' => 0, 'email' => '', 'add_date' => '1970-01-01', 'login' => '1970-01-01');
					}
							
					mysql_free_result($result);
							
					$result = $this->_db->query('SELECT id, email, add_date, login FROM users WHERE id = '.(int)$this->_manager[$key]) or $this->_db->raise_error();
						
					if (mysql_num_rows($result))
					{
						$manager = mysql_fetch_array($result);
					}
					else
					{
						$manager = array('id' => 0, 'email' => '', 'add_date' => '1970-01-01', 'login' => '1970-01-01');
					}
							
					mysql_free_result($result);
							
					$result = $this->_db->query('SELECT id, email, add_date, login FROM users WHERE id = '.(int)$this->_receiving[$key]) or $this->_db->raise_error();
							
					if (mysql_num_rows($result))
					{
						$receiving = mysql_fetch_array($result);
					}
					else
					{
						$receiving = array('id' => 0, 'email' => '', 'add_date' => '1970-01-01', 'login' => '1970-01-01');
					}
							
					mysql_free_result($result);
							
					$query = 'INSERT INTO wsk_mpk VALUES (\'\', '.$order_id.', \''.mysql_real_escape_string($mpk).'\', '.(int)$this->_budget[$key].', \''.$order['netto_value'].'\',
					'.$this->_user['id'].', \''.mysql_real_escape_string($contractor['email']).'\', \''.mysql_real_escape_string($contractor['login']).'\', 
					\''.date('Y-m-d').'\', 0, '.(int)$analyst['id'].', \''.mysql_real_escape_string($analyst['email']).'\', 
					\''.mysql_real_escape_string($analyst['login']).'\', \'1970-01-01\', 0, 0, \'\', \'\', \''.date('Y-m-d').'\',
					'.(int)$manager['id'].', \''.mysql_real_escape_string($manager['email']).'\', \''.mysql_real_escape_string($manager['login']).'\', 
					\'1970-01-01\', 0, '.(int)$receiving['id'].', \''.mysql_real_escape_string($receiving['email']).'\', 
					\''.mysql_real_escape_string($receiving['login']).'\', \'1970-01-01\', 0, \'\')';
					
					$this->_db->query($query) or $this->_db->raise_error();
				}
				
				$orders->saveOrderChangeMessage($order_id, 'oczekującym zapytaniu');
			}
			
			$special_order_id = $order_id;
			
			// zapytanie nadesłane - tworzenie historii
			
			$this->_db->query('INSERT INTO orders VALUES (\'\', \'\', '.$this->_user['id'].', \'0\', \'0\', 11, \''.$this->_document.'\', 
			\''.$this->_payment.'\', \''.$this->_comment.'\',  \''.date('Y-m-d H:i:s').'\', \'0000-00-00\')') or $this->_db->raise_error();
			
			$result = $this->_db->query('SELECT id FROM users WHERE id = '.$this->_user['id'].' AND ordering = 1') or $this->_db->raise_error();
			$order_id = mysql_insert_id();
			
			$netto_sum = 0;
			$brutto_sum = 0;
			
			if ($order_id)
			{
				foreach ($this->_names as $i => $name)
				{
					if (!strcmp($this->_mpk[$i], $mpk))
					{
						$row = array();
						$result = $this->_db->query('SELECT id_hermes, tax FROM products WHERE id_hermes = '.(int)$this->_ids[$i]) or $this->_db->raise_error();
						
						if (mysql_num_rows($result))
						{
							$row = mysql_fetch_array($result);
							$tax = $row['tax'];
							$netto_price = $prices->getPrice($row['id_hermes'], 4);
							$brutto_price = $prices->getPrice($row['id_hermes'], 1);
						}
						
						$this->_db->query('INSERT INTO order_details VALUES (\'\', '.$order_id.', '.(int)$row['id_hermes'].', \''.trim(mysql_real_escape_string($this->_names[$i])).'\', \''.trim(mysql_real_escape_string($this->_producers[$i])).'\',  
						\''.trim(mysql_real_escape_string($this->_symbols[$i])).'\', \''.$this->_units[$i].'\', \''.trim(mysql_real_escape_string($this->_comments[$i])).'\', \''.(float)$netto_price.'\', '.(int)$tax.', \''.(float)$brutto_price.'\', 
						\''.(float)str_replace(',', '.', $this->_amounts[$i]).'\', 0, \''.mysql_real_escape_string($mpk).'\', \'\', 
						'.$special_order_id.', \'\', 1, 1)') or $this->_db->raise_error();
						
						$netto_sum += ((float)$netto_price * (float)str_replace(',', '.', $this->_amounts[$i]));
						$brutto_sum += ((float)str_replace(',', '.', $this->_amounts[$i]) * (float)$netto_price * (1+($tax / 100)));
					}
				}
				
				$this->_db->query('UPDATE orders SET `netto_value` = `netto_value` + \''.sprintf("%0.2f", $netto_sum).'\', `brutto_value` = `brutto_value` + \''.sprintf("%0.2f", $brutto_sum).'\' 
				WHERE id = '.$order_id);
				
				$result = $this->_db->query('SELECT DISTINCT netto_value, mpk FROM order_details JOIN orders ON order_details.order_id = orders.id WHERE order_details.order_id = '.$order_id) or $this->_db->raise_error();
				
				if (mysql_num_rows($result))
				{
					$order = mysql_fetch_array($result);
					mysql_free_result($result);
					
					$result = $this->_db->query('SELECT email, add_date, login FROM users WHERE id = '.$this->_user['id']) or $this->_db->raise_error();
							
					if (mysql_num_rows($result))
					{
						$contractor = mysql_fetch_array($result);
					}
					else
					{
						$contractor = array('email' => '', 'add_date' => '1970-01-01', 'login' => '1970-01-01');
					}
							
					mysql_free_result($result);
						
					$result = $this->_db->query('SELECT id, email, add_date, login FROM users WHERE id = '.(int)$this->_analyst[$key]) or $this->_db->raise_error();
							
					if (mysql_num_rows($result))
					{
						$analyst = mysql_fetch_array($result);
					}
					else
					{
						$analyst = array('id' => 0, 'email' => '', 'add_date' => '1970-01-01', 'login' => '1970-01-01');
					}
							
					mysql_free_result($result);
							
					$result = $this->_db->query('SELECT id, email, add_date, login FROM users WHERE id = '.(int)$this->_manager[$key]) or $this->_db->raise_error();
						
					if (mysql_num_rows($result))
					{
						$manager = mysql_fetch_array($result);
					}
					else
					{
						$manager = array('id' => 0, 'email' => '', 'add_date' => '1970-01-01', 'login' => '1970-01-01');
					}
							
					mysql_free_result($result);
							
					$result = $this->_db->query('SELECT id, email, add_date, login FROM users WHERE id = '.(int)$this->_receiving[$key]) or $this->_db->raise_error();
							
					if (mysql_num_rows($result))
					{
						$receiving = mysql_fetch_array($result);
					}
					else
					{
						$receiving = array('id' => 0, 'email' => '', 'add_date' => '1970-01-01', 'login' => '1970-01-01');
					}
							
					mysql_free_result($result);
							
					$query = 'INSERT INTO wsk_mpk VALUES (\'\', '.$order_id.', \''.mysql_real_escape_string($mpk).'\', '.(int)$this->_budget[$key].', \''.$order['netto_value'].'\',
					'.$this->_user['id'].', \''.mysql_real_escape_string($contractor['email']).'\', \''.mysql_real_escape_string($contractor['login']).'\', 
					\''.date('Y-m-d').'\', 0, '.(int)$analyst['id'].', \''.mysql_real_escape_string($analyst['email']).'\', 
					\''.mysql_real_escape_string($analyst['login']).'\', \'1970-01-01\', 0, 0, \'\', \'\', \''.date('Y-m-d').'\',
					'.(int)$manager['id'].', \''.mysql_real_escape_string($manager['email']).'\', \''.mysql_real_escape_string($manager['login']).'\', 
					\'1970-01-01\', 0, '.(int)$receiving['id'].', \''.mysql_real_escape_string($receiving['email']).'\', 
					\''.mysql_real_escape_string($receiving['login']).'\', \'1970-01-01\', 0, \'\')';
					
					$this->_db->query($query) or $this->_db->raise_error();
				}
			}
			
			// zapytania nadesłane - koniec tworzenia historii
		}
		
		$communicats['ok'] = 'Zapytanie zostało przekazane.';
		$this->setSession("communicats", $communicats);
		header("Location: zapytania,lista.html");
	}
	
	public function acceptOrder($order_id, $comment)
	{
		$result = $this->_db->query('SELECT comment FROM special_orders WHERE id = '.$order_id) or $this->_db->raise_error();
		
		if (mysql_num_rows($result) && $this->_user['admin'] == 1) 
		{
			$row = mysql_fetch_array($result);
			$query = 'UPDATE special_orders SET status = 2, comment = \''.mysql_real_escape_string($row['comment']."\n\n--- KOMENTARZ ADMINISTRATORA ---\n\n".$comment).'\' WHERE id = '.$order_id;
			$this->_db->query($query);
			
			$communicats['ok'] = 'wycena została zaakceptowana.';
			$this->setSession("communicats", $communicats);
		}
	}
	
	public function rejectOrder($order_id, $comment)
	{
		$result = $this->_db->query('SELECT comment FROM special_orders WHERE id = '.$order_id) or $this->_db->raise_error();
		
		if (mysql_num_rows($result) && $this->_user['admin'] == 1)
		{
			$row = mysql_fetch_array($result);
			$query = 'UPDATE special_orders SET status = 0, comment = \''.mysql_real_escape_string($row['comment']."\n\n--- KOMENTARZ ADMINISTRATORA ---\n\n".$comment).'\' WHERE id = '.$order_id;
			$this->_db->query($query);
			
			$communicats['ok'] = 'wycena została odrzucona, ponownie przekazano do wyceny.';
			$this->setSession("communicats", $communicats);
		}
	}
	
	public function moveToBasket($order_id)
	{
		$query = 'SELECT special_orders.user_id, special_order_details.mpk, special_order_details.product_id, special_order_details.name as query_name, 
		special_order_details.symbol as query_symbol, special_order_details.comment as query_comment, special_order_details.netto_price as query_netto_price, 
		special_order_details.tax as query_tax, special_order_details.brutto_price as query_brutto_price, special_order_details.amount as query_amount, 
		IFNULL(products.id_hermes, 0) as id_hermes, product_versions.name, products.symbol, products.weight, products.tax, products.pallet, products.unit, 
		groups_tree.name as group_name FROM special_orders JOIN special_order_details ON special_orders.id = special_order_details.order_id LEFT JOIN 
		products ON special_order_details.product_id = products.id_hermes LEFT JOIN product_versions ON products.id_hermes = product_versions.product_id 
		LEFT JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE special_order_details.order_id = '.$order_id;
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		$products = $this->getSession("basket_products");
		
		if (mysql_num_rows($result))
		{
			while($row = mysql_fetch_array($result)) 
   			{
   				if ($row['user_id'] == $this->_user['id'])
   				{
	   				$controll = false;
	   				
	   				foreach ($products as $key => $product)
	   				{
	   					if ($product['product_id'] == $row['product_id'] && (float)$row['query_netto_price'] == (float)$product['netto_price'] && (float)$row['query_tax'] == (float)$product['tax'])
	   					{
	   						$controll = true;
	   						
	   						$products[$key]['count'] += $row['query_amount'];
	   						$products[$key]['netto_value'] = sprintf("%0.2f", $products[$key]['netto_price'] * $products[$key]['count']);
	   						$products[$key]['brutto_value'] = sprintf("%0.2f", $products[$key]['brutto_price'] * $products[$key]['count']);
	   					}
	   				}
	   				
	   				if (false == $controll)
		   			{
		   				array_push($products, array(
		   					'product_id' => $row['product_id'], 
		   					'subgroup' => $row['group_name'], 
		   					'name' => (($row['id_hermes'] > 0) ? $row['name'] : $row['query_name']), 
		   					'index' => (($row['id_hermes'] > 0) ? $row['symbol'] : $row['query_symbol']), 
		   					'tax' => $row['query_tax'], 
		   					'weight' => (($row['id_hermes'] > 0) ? $row['weight'] : 0), 
		   					'size' => (($row['id_hermes'] > 0) ? (($row['pallet'] == 1) ? 'T' : 'N') : 'N'), 
		   					'unit' => (($row['id_hermes'] > 0) ? strtolower($row['unit']) : ''), 
		   					'count' => (((int)$row['query_amount'] == (float)$row['query_amount']) ? (int)$row['query_amount'] : $row['query_amount']), 
		   					'netto_price' => $row['query_netto_price'], 
		   					'netto_value' => sprintf("%0.2f", $row['query_netto_price']*$row['query_amount']), 
		   					'brutto_price' => $row['query_brutto_price'], 
		   					'brutto_value' => sprintf("%0.2f", $row['query_brutto_price']*$row['query_amount']), 
		   					'mpk' => $row['mpk'], 
		   					'info' => $row['query_comment'], 
		   					'query_id' => $order_id
		   				));
		   			}
   				}
   			}
		}
		
		$this->_db->query('UPDATE special_orders SET status = 3 WHERE id = '.$order_id) or $this->_db->raise_error();
		$this->setSession("basket_products", $products);
		
		$communicats['ok'] = 'produkty z zapytania dodano do koszyka.';
		$this->setSession("communicats", $communicats);
	}
	
	private function _sendUserMessage()
	{
		$message = '
		<p><img src="http://www.sklep.unimet.pl/images/logotype.png"></p>
		<p><strong>Użytkownik złożył zapytanie w serwisie wsk.unimet.pl</strong></p>
		<p>Login użytkownika: <strong>'.$this->_user['name'].'</strong></p>';
		
		$mimemail = new nomad_mimemail(); 
		
		$from		= "newsletter@unimet.pl";	 
		$html		= '<html><body>'.iconv("UTF-8", "ISO-8859-2", $message).'</body></html>';
		$subject    = 'www.wsk.unimet.pl - informacja o nowym zapytaniu';
		
		$to	= $this->_user['email'];
   			
   		$mimemail->set_smtp_log(true); 
		$smtp_host	= "swarog.az.pl";
		$smtp_user	= "newsletter@unimet.pl";
		$smtp_pass	= "1hadesa2madmax";
		$mimemail->set_smtp_host($smtp_host);
		$mimemail->set_smtp_auth($smtp_user, $smtp_pass); 
		$mimemail->new_mail($from, $to, iconv("UTF-8", "ISO-8859-2", $subject), "", $html);
		$mimemail->send(); 	
	}
	
	private function _sendAdminMessage()
	{
		$message = '
		<p><img src="http://www.sklep.unimet.pl/images/logotype.png"></p>
		<p><strong>Klient złożył zapytanie</strong></p>
		<p>Login klienta: <strong>'.$this->_user['login'].'</strong></p>';

		
		$mimemail = new nomad_mimemail(); 
		
		$from		= "newsletter@unimet.pl";	 
		$html		= '<html><body>'.iconv("UTF-8", "ISO-8859-2", $message).'</body></html>';
		$subject    = 'www.wsk.unimet.pl - informacja o nowym zapytaniu';
		
		$result = $this->_db->query('SELECT email FROM users WHERE email != \'\' AND id_hermes = 0 AND status = 0') 
		or $this->_db->raise_error();
		
		while($row = mysql_fetch_array($result)) 
   		{
   			if ($this->validateEmail($row['email']))
	   		{
	   			$to	= $row['email'];
	   			
	   			$mimemail->set_smtp_log(true); 
				$smtp_host	= "swarog.az.pl";
				$smtp_user	= "newsletter@unimet.pl";
				$smtp_pass	= "1hadesa2madmax";
				$mimemail->set_smtp_host($smtp_host);
				$mimemail->set_smtp_auth($smtp_user, $smtp_pass); 
				$mimemail->new_mail($from, $to, iconv("UTF-8", "ISO-8859-2", $subject), "", $html);
				$mimemail->send(); 	
	   		}
   		}
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}

$special_orders = new SpecialOrders();
?>
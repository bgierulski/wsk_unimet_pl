<div id="title_element">
	<p>
	<strong>
	<?php
	switch ($action)
	{
		case 'edit': ?>Przegląd zamówienia<?php break;
		default: ?>Lista zamówień specjalnych<?php break;
	}
	?>
	</strong></p>
</div>

<div id="page_padding">
<?php
switch ($action)
{
	case 'edit': include('new_order.php'); break;
	default: include('orders_list.php'); break;
}
?>
</div>
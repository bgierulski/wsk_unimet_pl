<?php 
error_reporting(E_ALL ^ E_NOTICE);
session_start();

/*
if($_GET['test']==1234){

	$_SESSION['test']=1234;
	
}


if($_SESSION['test']<>1234){
	die('Serwer chwilowo niedostepny. Prosze sprobowac okolo 10:15');
}
*/

include('../include/config.php');
include(BASE_DIR.'nomad/nomad_mimemail.inc.php');
include(BASE_DIR.'class/class.DataBase.php');
include(BASE_DIR.'class/class.Validators.php');
include(BASE_DIR.'class/class.b2b.php');
include(BASE_DIR.'class/class.Pages.php');
include(BASE_DIR.'../login/include/class/class.Forms.php');



$db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);

$b2b_info = $b2b_details->getDetails();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<base href="<?php echo BASE_ADDRESS; ?>login/" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="reply-to" content="Adres_e-mail" />
<meta name="author" content="Autor_dokumentu" />
<meta name="description" content="Opis" />
<link rel="stylesheet" href="../css/b2b.css" type="text/css" />
<link rel="stylesheet" href="../css/subpage.css" type="text/css" />
<script type="text/javascript" src="../js/jquery-1.4.js"></script>
<script type="text/javascript" src="../js/jquery.marquee.js"></script>
<script type="text/javascript" src="../js/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery.scrollTo.js"></script>

<script type="text/javascript" src="js/operations.js"></script>

<link rel="stylesheet" type="text/css" href="../js/fancybox/jquery.fancybox-1.3.1.css" media="screen" />
<script type="text/javascript" src="../js/fancybox/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="../js/fancybox/jquery.fancybox-1.3.1.js"></script>

<!--[if lte IE 7]>
<style type="text/css">
.wrapper {position:relative;}
.cell {position:absolute; top:50%;}
.hack {position:relative; top:-50%;}
</style>
<![endif]-->

<title><?php echo DATABASE; ?> <?php echo $b2b_info['title']; ?></title>
</head>

<body>

<?php 
if($_GET['param1'] == 123123123){
	$_SESSION['handler']='ok';
}

if($_SESSION['handler']<>'ok' and date('U')<1288623600)
{
	die(' W zwiazku z aktualizacja strony b2b.unimet.pl do nowej wersji system bedzie nieczynny do wtorku do godziny 6:00<br>
	Przepraszamy za utrudnienia. <br><br>
	Jednocześnie informujemy, iż zamowienia mogą Państwo składać przez sklep.unimet.pl <br>
	Wszystkie Państwa rabaty zostaną uwzględnione przez DH.
	');
}

unset($_SESSION['handler']);
?>

<div id="content_center">

	<div id="top_content">
	
		<div id="logotype">
			<a href="./"><img src="../images/logotype.png" alt="" /></a>
		</div>
		
		<div class="clear"></div>
	</div>
	
	<div id="menu_content">
	<?php 
	include(BASE_DIR.'../login/include/horizontal_menu.php');
	?>
	</div>
	
	<div id="main_content">
	 
		<table cellspacing="0" cellpadding="0">
		<tr>
			<td id="page" style="padding:5px 31px 5px 31px;">
			<?php 
			switch ((int)$param1)
			{
				case 5: include(BASE_DIR.'../login/include/registration.php'); break;
				case 6: include(BASE_DIR.'../login/include/forgotten_password.php'); break;
				case 7: include(BASE_DIR.'../login/include/registration.php'); break;
				case 8: include(BASE_DIR.'../login/include/regulations.php'); break;
				case 9: include(BASE_DIR.'../login/include/contact.php'); break;
				case 13: include(BASE_DIR.'../login/include/contact.php'); break;
				default: include(BASE_DIR.'../login/include/login.php'); break;
			}
			?>
			</td>
		</tr>
		</table>
		 
	</div>
	
	<div id="footer_content">
	<div id="footer_box">
	<?php 
	$page = $pages->getPage(3);
	echo str_replace(array('\"', "\'"), array('"', "'"), $page['content']);
	?>
	</div>
	</div>
	
</div>

</body>
</html>
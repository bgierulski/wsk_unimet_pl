<?php 
$params = $_GET;

if ($_SESSION["b2b_user"]['id'])
{
	$id = (int)$params['param3'];
	$prev_status = $orders->getOrderStatus($id);
	
	if ($_POST['operation'] == "reject_order")
	{
		$orders->cancelOrder((int)$_POST['id'], $_POST['comment']);
	}
	else
	{
		switch ($params['param2'])
		{
			case "akceptuj":
				$orders->acceptOrder($id);
				break;
				
			case "odebralem":
				$orders->receiveOrder($id);
				break;
		}
	}
	
	switch ($prev_status)
	{
		case 6: $params['param2'] = 'u_analityka'; break;
		case 7: $params['param2'] = 'u_kierownika'; break;
		case 1: $params['param2'] = 'w_unimet'; break;
		case 2: $params['param2'] = 'w_unimet'; break;
		case 3: $params['param2'] = 'w_drodze'; break;
		case 4: $params['param2'] = 'archiwum'; break;
		case 5: $params['param2'] = 'odrzucone'; break;
		case 9: $params['param2'] = 'u_zamawiajacego'; break;
		case 10: $params['param2'] = 'u_administratora'; break;
		case 8: $params['param2'] = 'u_zamawiajacego'; break;
	}
}

$user_names = $orders->getUsersNames();
?>

<div id="page_header">
	<div class="details">
		<p><strong><a href="./zamowienia.html" <?php if (empty($params['param2'])) { echo 'class="active"'; } ?>>Zamówienia</a></strong> 
		<?php 
		switch ($params['param2'])
		{
			case 'u_analityka': $type = 1; break;
			case 'u_kierownika': $type = 2; break;
			case 'w_unimet': $type = 3; break;
			case 'w_drodze': $type = 4; break;
			case 'archiwum': $type = 5; break;
			case 'odrzucone': $type = 6; break;
			case 'u_zamawiajacego': $type = 7; break;
			case 'u_administratora': $type = 8; break;
			default: $type = 0; break;
		}
		?>
		<?php if ($_SESSION["communicats"]['ok']) { echo ' - '.$_SESSION["communicats"]['ok']; } unset($_SESSION["communicats"]['ok']); ?>
		<?php if ($_SESSION["communicats"]['error']) { echo ' - '.$_SESSION["communicats"]['error']; } unset($_SESSION["communicats"]['error']); ?>
		</p>
	</div>
	
	<div class="box">
		<p><strong><a href="./zamowienia,archiwum,<?php echo date('Y'); ?>.html" <?php if ($params['param2'] == 'archiwum') { echo 'class="active"'; } ?>>Archiwum</a></strong></p>
	</div>
	<div class="box">
		<p><strong><a href="./zamowienia,odrzucone.html" <?php if ($params['param2'] == 'odrzucone') { echo 'class="active"'; } ?>>Odrzucone</a></strong></p>
	</div>
	<div class="box">
		<p><strong><a href="./zamowienia,w_drodze.html" <?php if ($params['param2'] == 'w_drodze') { echo 'class="active"'; } ?>>W drodze</a></strong></p>
	</div>
	<div class="box">
		<p><strong><a href="./zamowienia,w_unimet.html" <?php if ($params['param2'] == 'w_unimet') { echo 'class="active"'; } ?>>W Unimet</a></strong></p>
	</div>
	<div class="box">
		<p><strong><a href="./zamowienia,u_kierownika.html" <?php if ($params['param2'] == 'u_kierownika') { echo 'class="active"'; } ?>>U kierownika</a></strong></p>
	</div>
	<div class="box">
		<p><strong><a href="./zamowienia,u_analityka.html" <?php if ($params['param2'] == 'u_analityka') { echo 'class="active"'; } ?>>U analityka</a></strong></p>
	</div>
	<div class="box">
		<p><strong><a href="./zamowienia,u_administratora.html" <?php if ($params['param2'] == 'u_administratora') { echo 'class="active"'; } ?>>U administratora</a></strong></p>
	</div>
	<div class="box">
		<p><strong><a href="./zamowienia,u_zamawiajacego.html" <?php if ($params['param2'] == 'u_zamawiajacego') { echo 'class="active"'; } ?>>U zamawiającego</a></strong></p>
	</div>
	<div class="clear"></div>
</div>

<?php 
$limits = $orders->getLimits();
?>

<p id="params" style="display:none;"><?php echo serialize($params); ?></p>

<div id="page_content">
<div class="archive_content empty">
</div>

<div id="trader_comment">
<p></p>
<span><img src="images/close_icon.png" alt="" class="close" /></span>
</div>

<div id="reject_order_form">
<div>
	<form method="post" action="" name="reject_order">
	<input type="hidden" name="id" value="0" />
	<input type="hidden" name="operation" value="reject_order" />
	<p><textarea cols="0" rows="0" name="comment"></textarea></p>
	<p class="button"><a id="reject_form_submit" href="./">ODRZUĆ ZAMÓWIENIE</a></p>
	</form>
	<span class="close"><img src="images/close_icon.png" alt="" /></span>
</div>
</div>

</div>

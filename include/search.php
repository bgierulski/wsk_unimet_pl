<?php
$key = $_SESSION['b2b_search_key'];
$data = explode(' ', $key);
$search = array();
$replace = array();

if ($_POST['operation'] == 'favorite_products')
{
	$products->addFavoriteProducts($_POST["actives"]); 
}

if (true == $groups->checkUserType())
{
	$query = 'SELECT id_hermes, products_status.status as product_status, (SELECT count(product_id) FROM product_replacements WHERE product_id = id_hermes) as replacement, subgroup_id, 
	groups_tree.name as subgroup_name, symbol, product_versions.name, product_versions.description, weight, pallet, unit, producers.name as producer_name, 
	IFNULL(DATEDIFF(END , NOW()), \'-1\') AS news FROM products_status JOIN products ON products_status.id = products.id_hermes AND products_status.status = 1 
	AND products_status.user = '.(int)$_SESSION["b2b_user"]['parent_id'].' JOIN product_versions ON products.id_hermes = product_versions.product_id AND version_id = 1 
	JOIN product_details ON products.id_hermes = product_details.product_id JOIN producers ON products.producer_id = producers.id JOIN groups_tree ON 
	products.subgroup_id = groups_tree.id LEFT JOIN newses ON products.id_hermes = newses.product_id WHERE (';
}
else
{
	$query = 'SELECT id_hermes, products_status.status as product_status, (SELECT count(product_id) FROM product_replacements WHERE product_id = id_hermes) as replacement, subgroup_id, 
	groups_tree.name as subgroup_name, symbol, product_versions.name, product_versions.description, weight, pallet, unit, producers.name as producer_name, 
	IFNULL(DATEDIFF(END , NOW()), \'-1\') AS news FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id AND version_id = 1 
	JOIN product_details ON products.id_hermes = product_details.product_id JOIN producers ON products.producer_id = producers.id JOIN groups_tree ON 
	products.subgroup_id = groups_tree.id LEFT JOIN newses ON products.id_hermes = newses.product_id LEFT JOIN products_status ON products.id_hermes = products_status.id AND products_status.user = '.$_SESSION["b2b_user"]['parent_id'].' WHERE (';
}
			
$query .= 'product_versions.name LIKE \'%'.$key.'%\' OR (';
			
foreach ($data as $element)
{
	array_push($search, $products->strToLower($element));
	array_push($search, $products->strToLower($element, 1));
	array_push($replace, '<b>'.$products->strToLower($element).'</b>');
	array_push($replace, '<b>'.$products->strToLower($element, 1).'</b>');
				
	$query .= 'product_versions.name LIKE \'%'.$element.'%\' AND ';
}
			
$query = substr($query, 0, -5);
$query .= ') OR products.ean LIKE \'%'.$key.'%\' OR (';
			
foreach ($data as $element)
{
	$query .= 'products.ean LIKE \'%'.$element.'%\' AND ';
}
			
$query = substr($query, 0, -5);
$query .= ') OR products.symbol LIKE \'%'.$key.'%\' OR (';
			
foreach ($data as $element)
{
	$query .= 'products.symbol LIKE \'%'.$element.'%\' AND ';
}
			
$query = substr($query, 0, -5);
$query .= ')) AND subgroup_id > 0';
		
$result = $db->query($query);

if (mysql_num_rows($result))
{
	?>
	<div id="page_header">
		<div class="details">
			<p>Wyników wyszukiwania: 
			<?php 
			echo mysql_num_rows($result); 
			?>
			</p>
		</div>
		<div class="box">
			<p><span id="select_products">zaznacz</span> / <span id="unselect_products">odznacz</span> wszystkie</p>
		</div>
		<div class="box">
			<p class="legend" id="legend_1"><strong>LEGENDA</strong></p> 
			<div class="legend_details" id="legend_details_1"><?php include(BASE_DIR.'products_lengend.php'); ?></div>
		</div>
		<div class="clear"></div>
	</div>
	
	<?php
	$promotions = $products->getPromotions();
	
	$products_list = $db->mysql_fetch_all($result);
	?>
	
	<div id="page_content">

		<table cellspacing="0" cellpadding="0" class="subgroup" style="border-bottom:none;">
		<tr>
			<th class="td_image">&nbsp;</th>
			<th class="td_product"><p>Produkt</p></th>
			<th class="td_symbol"><p>Symbol</p></th> 
			<th class="status"><p>&nbsp;</p></th>
			<th class="td_description"><div>&nbsp;</div></th>
			<th class="td_replacement">&nbsp;</th>
			<th class="td_weight"><div>Waga</div></th>
			<th class="td_producer"><div>Producent</div></th>
			<th class="td_availability"><div>&nbsp;</div></th>
			<?php if (true !== $_SESSION["b2b_full_offer"]) { ?>
			<th class="td_price"><div>Cena netto</div></th>
			<?php } ?>
			<th class="td_amount"><div>Ilość</div></th>
			<th class="td_unit"><div>JM.</div></th>
		</tr>
		</table>
		<div id="products_content">
			<table cellspacing="0" cellpadding="0" class="subgroup" style="border-top:none;">
			<form method="POST" action="" name="to_basket">
			<input id="product_operation" type="hidden" name="operation" value="to_basket" />
			<?php 
			
			foreach ($products_list as $i => $product)
			{
				?>
				<tr>
					<td class="td_image">
						<div class="wrapper" id="product_<?php echo $product['id_hermes']; ?>"><div class="cell"><div class="hack"><p>
						<img src="<?php echo BASE_ADDRESS; ?>/include/image.php?id=<?php echo (int)$product['id_hermes']; ?>&w=38&h=38" alt="" border="0">
						</p></div></div></div>
					</td>
					<td class="td_product">
						<strong><a href="#" onClick="javascript:pop_up(<?php echo $product['id_hermes']; ?>); return false;"><?php echo $product['name']; ?></a></strong>
					</td>
					<td class="td_symbol">
						<?php echo $product['symbol']; ?>
					</td>
					<td class="status">
					<?php 
					if (in_array($product['id_hermes'], $promotions) && $_SESSION["b2b_user"]['promotions'] == 1) { ?><p><span>P</span></p><?php }
					if ($product['news'] >= 0) { ?><p><strong>N</strong></p><?php }
					if (!in_array($product['id_hermes'], $promotions) && $product['news'] < 0) { ?>&nbsp;<?php }
					?>
					</td>
					<td class="td_description">
						<?php if (!empty($product['description'])) { ?>
						<p><a href="#" onClick="javascript:pop_up(<?php echo $product['id_hermes']; ?>); return false;"><strong id="description_<?php echo $product['id_hermes']; ?>"><img src="images/comment.png" alt="" /></strong></a></p>
						<?php } ?>
					</td>
					<td class="td_replacement"><p><?php if ($product['replacement'] > 0) { ?><img src="images/replacement.png" alt="" /><?php } else { ?>&nbsp;<?php } ?></p></td>
					<td class="td_weight">
						<p>
						<?php if ($product['weight'] > 0) { echo $product['weight']; } else { echo '&nbsp;'; } ?>
						<span><?php if ($product['pallet'] == 1) { ?><img src="images/element_3.png" alt="" title="transport paletowy" /><?php } ?></span>
						</p>
					</td>
					<td class="td_producer">
						<p><?php echo $product['producer_name']; ?></p>
					</td>
					<td class="td_availability">
						<p><img src="./magazyn.php?id=<?php echo $product['id_hermes']; ?>" alt="" /></p>
					</td>
					<?php if (true !== $_SESSION["b2b_full_offer"]) { ?>
					<td class="td_price">
						<p><?php echo $prices->getPrice($product['id_hermes'], 4); ?> zł</p>
					</td>
					<?php } ?>
					<td class="td_amount">
					<?php 
					if (true !== $_SESSION["b2b_full_offer"] || $product['product_status'] == 0)
					{
						?>
						<div>
						<span><input class="product_count" type="text" id="count_<?php echo $product['id_hermes']; ?>" name="counts[<?php echo $product['id_hermes']; ?>]" value="1" /></span> 
						<span><input class="product_checkbox" type="checkbox" id="active_<?php echo $product['id_hermes']; ?>" name="actives[]" value="<?php echo $product['id_hermes']; ?>" /></span>
						<span class="favorite_product"><img id="favorite_<?php echo $product['id_hermes']; ?>" src="images/element_4.png" alt="" title="dodaj do ulubionych" /></span>
						</div>
						<?php 
					}
					else
					{
						?><div>Produkt dostępny w ofercie zawężonej</div><?php
					}
					?>
					</td>
					<td class="td_unit">
						<p><?php echo strtolower($product['unit']); ?></p>
					</td>
				</tr>
				<?php
			}
			?>
			</table>
		</div>

	</div>
	<?php
	
	if (count($products_list))
	{
		?>				
		<div class="action">
			<div style="float:left;">
				<p class="button legend" id="legend_2">
				<a onClick="javascript:return false;" style="cursor:pointer; width:90px;">LEGENDA</a>
				</p>
				<div class="legend_details" id="legend_details_2" style="padding:5px;"><?php include(BASE_DIR.'products_lengend.php'); ?></div>
			</div>
			<div>
				<?php if (true !== $_SESSION["b2b_full_offer"]) { ?>
					<p class="button" style="padding-right:40px;">
					<a onClick="javascript:favorite_products();" style="cursor:pointer;">PRODUKTY ULUBIONE</a>
					</p>
					<p class="button" style="padding-right:40px;">
					<a href="./" id="add_to_favorite">DODAJ DO ULUBIONYCH</a>
					</p>
					<p class="button">
					<a id="add_to_order" href="./">DODAJ DO ZAMÓWIENIA</a>
					</p>
				<?php } ?>
			</div>
			<div class="clear"></div>
		</div>
		</form>
		<?php 
	}
}
else
{
	?>
	<div id="page_header">
		<div class="details">
			<p>Brak wyników wyszukiwania</p>
		</div>
		
		<div class="clear"></div>
	</div>
	
	<div id="page_content">
	<p>&nbsp;</p>
	<p>W chwili obecnej w systemie <a href="http://b2b.unimet.pl">b2b.unimet.pl</a> brak jest szukanego produktu, nie oznacza to jednak, że produkt jest nieosiągalny.</p>
	<p>W celu uzyskania szczegółowych informacji prosimy o kontakt telefoniczny lub e-mailowy z Państwa Opiekunem Handlowym.</p>
	</div>
	<?php
	if ($persons['guardian_name'] != '')
	{
		?>
		<p>&nbsp;</p>
		<p><strong>Państwa Opiekun Handlowy:</strong></p>
		<p><?php echo $users->textControll($persons['guardian_name']); ?></p>
		<?php if (!empty($persons['guardian_phone'])) { ?><p>tel.: <?php echo $persons['guardian_phone']; ?></p><?php } ?>
		<?php if (!empty($persons['guardian_email'])) { ?><p><a href="mailto:<?php echo $persons['guardian_email']; ?>"><?php echo $persons['guardian_email']; ?></a></p><?php } ?>
		<?php
	}
}
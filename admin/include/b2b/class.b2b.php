<?php
class B2B extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_title;
	private $_marquee;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
	}
	
	public function getDetails()
	{
		$result = $this->_db->query('SELECT title, marquee FROM b2b_details WHERE id = 1') or $this->_db->raise_error();
		$row = mysql_fetch_array($result);
		
		$values['title'] = $row['title'];
		$values['marquee'] = $row['marquee'];
		
		$this->setSession("values", $values);
	}
	
	public function validateB2B()
	{
		$this->clear();
		
		$this->_title = $values['title'] = trim(mysql_escape_string($_POST['title']));
		$this->_marquee = $values['marquee'] = trim(mysql_escape_string($_POST['marquee']));
		
		if (empty($this->_title))
		{
			$this->_errors++;
			$errors['title'] = 'Tytuł podstrony jest wymagany';
		}
		
		if (!$this->_errors)
		{
			$this->_saveChanges();
		}
		else 
		{

			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
		
	}
	
	private function _saveChanges()
	{
		$this->_db->query('UPDATE b2b_details SET title = \''.$this->_title.'\', marquee = \''.$this->_marquee.'\' WHERE id = 1') or $this->_db->raise_error();
		
		$communicats['ok'] = 'Szczegóły zostały wyedytowane';
		$this->setSession("communicats", $communicats);
		
		$this->getDetails();
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}
?>
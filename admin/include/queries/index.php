<?php 
if (in_array($action, array("edit", "show")))
{
	$orders->setRepresentative((int)$id);
}
?>

<div id="title_element">
	<p>
	<strong>
	<?php
	switch ($action)
	{
		case 'waiting': ?>Podgląd zapytań oczekujących na realizację<?php break;
		case 'ongoing': ?>Podgląd zapytań w trakcie realizacji<?php break;
		case 'realized': ?>Podgląd zapytań zrealizowanych<?php break;
		case 'archium': ?>Podgląd zapytań archiwalnych<?php break;
		case 'anulowane': ?>Podgląd zapytań odrzuconych<?php break;
		case 'return': ?>Zwrócone zapytania<?php break;
		case 'edit': ?>Edycja danych<?php break;
		default: ?>Podgląd wszystkich zapytań<?php break;
	}
	?>
	</strong></p>
</div>

<div id="page_padding">
<?php

$user_names = $orders->getUsersNames();

switch ($action)
{
	case 'check': include('edit_order.php'); break;
	case 'edit': include('edit_order.php'); break;
	case 'show': include('edit_order.php'); break;
	default: include('orders_list.php'); break;
}
?>
</div>
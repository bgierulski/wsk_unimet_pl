<?php 
if ($_GET['param2'] == 'szablon')
{
	$templates->saveTemplate();
}

$limits = $orders->getLimits();
$brutto = $basket->getBruttoValue();
?>

<div id="page_header" style="background:none;">
	<div class="details">
		<?php 
		if ($_SESSION["communicats"]['ok']) 
		{ 
			?><p><strong style="font-size:16px"><?php
			if ($_SESSION["communicats"]['ok']) { echo $_SESSION["communicats"]['ok']; } unset($_SESSION["communicats"]['ok']);
			?></strong></p><?php
		} 
		else if ($brutto > $limits['order_limit'])
		{
			?><p><strong style="font-size:16px">Przekroczono limit wartości zamówienia. Limit jest równy <?php echo $limits['order_limit']; ?> zł.</strong></p><?php
		}
		else
		{
			?><p><strong>Koszyk</strong></p><?php
		}
		?>
	</div>
	<div class="clear"></div>
</div>

<div id="page_content">

<?php
$products_list = $basket->getProducts();

if (count($products_list))
{
	?>
	<table class="proteges_list">
	<tr>
		<th>MPK</th>
		<th>Produkt</th>
		<th>Index</th>
		<th style="width:70px;">Ilość</th>
		<th>JM</th>
		<th>Pal.</th>
		<th>Cena netto</th>
		<th>Wartość netto</th>
		<th>Wartość brutto</th>
		<th>Usuń</th>
	</tr>
	<?php
	$netto_sum = 0;
	$brutto_sum = 0;
		
	foreach ($products_list as $key => $product)
	{
		if ($key%2 == 0)
		{
			$class = 'bg_1';
		}
		else 
		{
			$class = 'bg_2';
		}
		?>
		<tr alt="<?php echo $key; ?>">
			<td class="mpk <?php echo $class; ?>">
				<?php if (count($products_list) > 1) { ?>
					<span style="position:relative; top:-4px;">
				<?php } ?>
					<input type="text" class="mpk" alt="<?php echo $key; ?>" name="mpk[<?php echo $key; ?>]" value="<?php echo $product['mpk']; ?>" />
				<?php if (count($products_list) > 1) { ?>
					</span> 
					<img class="copy_mpk" src="images/copy_icon.png" alt="" title="skopiuj nr MPK poniżej" />
				<?php } ?>
			</td>
			<td class="<?php echo $class; ?>" style="text-align:left"><?php echo $product['name']; ?></td>
			<td class="<?php echo $class; ?>"><?php echo $product['index']; ?></td>
			<td class="<?php echo $class; ?>">
			<div>
				<div class="count" style="float:left;">
				<p style="padding:9px 0px 0px 18px; width:15px;"><?php echo $product['count']; ?></p>
				</div>
				<div class="change_count" style="float:left; padding-left:2px;">
					<form method="POST" name="basket_up" action="">
						<input type="hidden" name="count_change" value="1" />
						<input type="hidden" name="id" value="<?php echo $product['product_id']; ?>" /> 
						<input type="image" src="images/element_6.gif" />
					</form>
					<form method="POST" name="basket_down" action="">
						<input type="hidden" name="count_change" value="-1" />
						<input type="hidden" name="id" value="<?php echo $product['product_id']; ?>" /> 
						<input type="image" src="images/element_7.gif" />
					</form>
				</div>
				<div class="clear"></div>
			</div>
			</td>
			<td class="<?php echo $class; ?>"><?php 
			switch (strtoupper($product['unit']))
			{
				case 'SZT': ?>sztuka<?php break;
				case 'KPL': ?>komplet<?php break;
				case 'KG': ?>kilogram<?php break;
				case 'OP': ?>opakowanie<?php break;
				case 'M2': ?>metr kwadratowy<?php break;
				case 'MB': ?>metr bieżący<?php break;
				case 'M3': ?>metr sześcienny<?php break;
				case 'PAR': ?>para<?php break;
				case 'STO': ?>sto sztuk<?php break;
			} 
			?></td>
			<td class="<?php echo $class; ?>"><?php echo $product['size']; ?></td>
			<td class="<?php echo $class; ?>"><?php echo $product['netto_price']; ?> zł</td>
			<td class="<?php echo $class; ?>"><?php echo $product['netto_value']; ?> zł</td>
			<td class="<?php echo $class; ?>"><?php echo $product['brutto_value']; ?> zł</td>
			<td class="<?php echo $class; ?>">
			<form method="POST" action="">
				<input type="hidden" name="delete_product" value="<?php echo $product['product_id']; ?>" /> 
				<input type="image" src="images/element_8.gif" />
			</form>
			</td>
		</tr>
		<?php
	}
	?>
	</table>
	
	<div>
		<div id="basket_info">
			<p>Łączna waga towaru: <?php echo $basket->getWeight(); ?> kg</p>
			<?php $netto = $basket->getNettoValue(); ?>
			<p>Wartość zamówienia netto: <strong><?php echo $netto; ?> zł</strong></p>
		</div>
		<div id="to_realization">
			<p class="button" style="width:137px; text-align:center; margin-left:20px;"><a href="./koszyk,wyczysc.html">WYCZYŚĆ KOSZYK</a></p>
			<p class="button" style="width:137px; text-align:center; margin-left:20px;"><a href="./koszyk,szablon.html">ZAPISZ SZABLON</a></p>
			<?php if ($brutto <= $limits['order_limit']) { ?>
			<p class="button" style="margin-bottom:5px; margin-left:20px;"><a href="./sciezki.html">PRZEJDŹ DALEJ</a></p>
			<?php } ?>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>

	<?php
}
else 
{
	?>Brak produktów w koszyku<?php
}
?>

<div>
<p class="button" style="padding-top:10px;"><a href="./">POWRÓT DO ZAKUPÓW</a></p>
<div class="clear"></div>
</div>

</div>

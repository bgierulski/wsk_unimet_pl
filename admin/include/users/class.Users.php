<?php
class Users extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_user_id;
	private $_new_password;
	private $_repeated_password;
	private $_active;
	//private $_merchant_id;
	
	private $_actual_password;
	
	
	private $_status;
	private $_login;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
	}
	
	public function generatePassword($user_id, $generate = true)
	{
		$password = $this->randomString(5);
		
		$result = $this->_db->query('SELECT SUBSTRING(MD5(RAND()),-20) as value') or $this->_db->raise_error();
		$key = mysql_fetch_array($result);
		
		$this->_db->query('UPDATE users SET password = MD5(CONCAT(\''.$key['value'].'\', \''.$password.'\')), `key` = \''.$key['value'].'\', comment = \''.$password.'\', active = 1 WHERE id = '.$user_id) or $this->_db->raise_error();
		/*
		mysql_free_result($result);
		$result = $this->_db->query('SELECT login, email FROM users WHERE id = '.$user_id) or $this->_db->raise_error();
		$user = mysql_fetch_array($result);
		
		if ($this->validateEmail($user['email']))
		{
			$message = '<p><img src="http://www.sklep.unimet.pl/images/logotype.png"></p>';
			$message .= '<p></p>';
			$message .= '<p>Witamy</p>';
			$message .= '<p></p>';
			$message .= '<p>Państwa konto w systemie b2b.unimet.pl zostało właśnie aktywowane.</p>';
			$message .= '<p></p>';
			$message .= '<p>Poniżej znajduje się login i hasło pozwalający na zalogowanie się do serwisu:</p>';
			$message .= '<p>Login: <strong>'.$user['login'].'</strong></p>';
			$message .= '<p>Hasło: <strong>'.$password.'</strong></p>';
			$message .= '<p></p>';
			$message .= '<p>W przypadku jakichkolwiek problemów prosimy kontakt z Działem Handlowym.</p>';
			$message .= '<p></p>';
			$message .= '<p></p>';
			$message .= '<p>Z poważaniem,<br />Dział Handlowy Unimet<br /><a href="http://www.unimet.pl">www.unimet.pl</a>, <a href="http://b2b.unimet.pl">www.b2b.unimet.pl</a></p>';
			
			$mimemail = new nomad_mimemail(); 
			
			$from		= "newsletter@unimet.pl";	 
			$to			= $user['email'];	 
			$subject	= "b2b.unimet.pl - aktywacja konta"; 
			$html		= '<html><body style="font-family:arial;">'.iconv("UTF-8", "ISO-8859-2", $message).'</body></html>';
			
			$mimemail->set_smtp_log(true); 
			$smtp_host	= "swarog.az.pl";
			$smtp_user	= "newsletter@unimet.pl";
			$smtp_pass	= "1hadesa2madmax";
			$mimemail->set_smtp_host($smtp_host);
			$mimemail->set_smtp_auth($smtp_user, $smtp_pass); 
			$mimemail->new_mail($from, $to, iconv("UTF-8", "ISO-8859-2", $subject), "", $html);
			
			if (true == $generate)
			{
				$mimemail->send();
			}
		}
		*/
	}
	
	public function getMerchants()
	{
		$result = $this->_db->query('SELECT id, name FROM merchants ORDER BY name ASC') or $this->_db->raise_error();
		return $result;
	}
	
	public function getUsers($key, $activity)
	{
		$query = 'SELECT users.id, login, name, email, active, users.comment, login_date, login_counter, IFNULL(count(orders.id),0) as orders_count, 
		IFNULL(SUM(brutto_value), 0.00) as brutto_sum FROM users LEFT JOIN orders ON users.id = orders.user_id WHERE users.status = 1 ';
		
		if ($activity < 2)
		{
			$query .= 'AND active = '.$activity.' ';
		}
		
		if (!empty($key))
		{
			$query .= 'AND (id_hermes LIKE \'%'.$key.'%\' OR login LIKE \'%'.$key.'%\' OR name LIKE \'%'.$key.'%\') ';
		}
		
		$query .= ' GROUP BY users.id ';
		
		if (!empty($key))
		{
			$query .= 'ORDER BY name';
		}

		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getUser($user_id)
	{
		$result = $this->_db->query('SELECT active, merchant_id FROM users WHERE id = '.$user_id) or $this->_db->raise_error();
		$row = mysql_fetch_array($result);
		
		$values['active'] = $row['active'];
		$values['merchant_id'] = $row['merchant_id'];
		
		$this->setSession("values", $values);
	}
	
	public function getLogin($user_id)
	{
		$result = $this->_db->query('SELECT login FROM users WHERE id = '.$user_id) or $this->_db->raise_error();
		$row = mysql_fetch_array($result);
		
		return $row['login'];
	}
	
	public function changeActivity()
	{
		$result = $this->_db->query('UPDATE users SET active = '.$this->_active.' WHERE id = '.$this->_user_id) or $this->_db->raise_error();
	
		if ($this->_active == 1)
		{
			//$this->generatePassword($this->_user_id, false);
		}
		
		if ($result)
		{
			$communicats['ok'] = 'Status użytkownika został zmieniony';
		}
		else 
		{
			$communicats['error'] = 'Zmiana statusu użytkownika nie powiodła się';
		}
			
		$this->setSession("communicats", $communicats);
	}
	
	public function deleteUser($user_id)
	{
		$result = $this->_db->query('DELETE FROM users WHERE id = \''.$user_id.'\'') or $this->_db->raise_error();
		
		if ($result)
		{
			$communicats['ok'] = 'Użytkownik został usunięty';
		}
		else 
		{
			$communicats['error'] = 'Usunięcie użytkownika nie powiodło się.';
		}
			
		$this->setSession("communicats", $communicats);
	}
	
	public function validateUser()
	{
		$this->clear();
		
		$this->_user_id = (int)$_POST['user_id']; 
		$this->_new_password = trim(mysql_escape_string($_POST['new_password']));
		$this->_repeated_password = trim(mysql_escape_string($_POST['repeated_password']));
		//$this->_active = (int)$_POST['active']; 
		//$this->_merchant_id = (int)$_POST['merchant_id']; 
		
		if (strcmp($this->_new_password, $this->_repeated_password))
		{
			$this->_errors++;
			$errors['new_password'] = 'Podane hasła nie są identyczne';
		}
		
		if ($this->_errors)
		{
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
		else
		{
			//$result = $this->_db->query('SELECT active FROM users WHERE id = '.$this->_user_id) or $this->_db->raise_error();
			//$row = mysql_fetch_array($result);
			
			//$query = 'UPDATE users SET active = '.$this->_active;
			
			if ($this->_new_password != '')
			{
				$controll = $this->_db->query('SELECT SUBSTRING(MD5(RAND()),-20) as value') or $this->_db->raise_error();
				$key = mysql_fetch_array($controll);
				
				$query .= 'UPDATE users SET password = MD5(CONCAT(\''.$key['value'].'\', \''.$this->_new_password.'\')), `key` = \''.$key['value'].'\' WHERE id = '.$this->_user_id;
				$result = $this->_db->query($query) or $this->_db->raise_error();
			}
			
			/*
			if ($row['active'] == 0 && $this->_active == 1)
			{
				$active_change = true;
				$this->generatePassword($this->_user_id);
			}
			*/
			if ($result)
			{
				if (true == $active_change)
				{
					$communicats['ok'] = 'Status użytkownika został zmieniony';
				}
				else
				{
					$communicats['ok'] = 'Hasło zostało zmienione';
				}
				
				$this->setSession("communicats", $communicats);
				header("Location: ../../users/list");
			}
			else 
			{
				$communicats['error'] = 'Zmiana hasła nie powiodła się';
				$this->setSession("communicats", $communicats);
			}
		}
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}
?>
<?php
require("class/class.Barcode.php");

$bar = new BARCODE();
	
$bar->setSymblogy('EAN-13');
$bar->setHeight(30);
$bar->setScale(1.5);
$bar->setHexColor('#666666', '#ffffff');
  	
$return = $bar->genBarCode($_GET['code'], 'png', '');	
	
if( false == $return)
{
	$bar->error(true);
}
?>
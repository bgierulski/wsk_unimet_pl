<?php
class Products extends Validators 
{
	private $_db;
	private $_user;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		$this->_user = $this->getSession('b2b_user');
	}
	
	public function exportSavings($year)
	{
		$excel = new PHPExcel();
		
		$excel->setActiveSheetIndex(0);
		
		$excel->getActiveSheet()->setCellValue('B2', 'Termin wygenerowania zestawienia:');
		$excel->getActiveSheet()->setCellValue('C2', date('Y-m-d H:i'));
		$excel->getActiveSheet()->getStyle('C2')->getFont()->setBold(true);
		
		$excel->getActiveSheet()->setCellValue('B3', 'Zestawienie wygenerował:');
		$excel->getActiveSheet()->setCellValue('C3', $this->_user['login']);
		$excel->getActiveSheet()->getStyle('C3')->getFont()->setBold(true);
		
		if ((int)$year)
		{
			$excel->getActiveSheet()->setCellValue('B4', 'Raport za rok:');
			$excel->getActiveSheet()->setCellValue('C4', (int)$year);
			$excel->getActiveSheet()->getStyle('C4')->getFont()->setBold(true);
		}
		
		$excel->getActiveSheet()->getStyle('B2:C2')->applyFromArray(
			array('borders' => array(
				'top' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')), 
				'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')),
				'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9'))
			)
		));
		
		$change = (((int)$year) ? 1 : 0);
		
		if (!(int)$year)
		{
			$excel->getActiveSheet()->getStyle('B3:C3')->applyFromArray(
				array('borders' => array(
					'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')), 
					'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')),
					'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9'))
				)
			));
		}
		else
		{
			$excel->getActiveSheet()->getStyle('B3:C3')->applyFromArray(
				array('borders' => array(
					'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')),
					'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9'))
				)
			));
			
			$excel->getActiveSheet()->getStyle('B4:C4')->applyFromArray(
				array(
				'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
					),
				'borders' => array(
					'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')), 
					'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')),
					'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9'))
				)
			));
		}
		
		$query = 'SELECT order_savings.order_id, orders.number, order_savings.product_id, order_savings.product_name, order_savings.product_price, 
		order_savings.replacement_id, order_savings.replacement_name, order_savings.replacement_price, order_savings.savings, order_savings.time FROM 
		order_savings JOIN orders ON orders.id = order_savings.order_id ';
		
		if ((int)$year)
		{
			$query .= 'WHERE DATE_FORMAT(time, \'%Y\') = '.(int)$year;
		}
		
		$query .= ' ORDER BY `time` ASC';
		
		$data = $this->_db->query($query) or $this->_db->raise_error();
		
		if (mysql_num_rows($data))
		{
			$excel->getActiveSheet()->setCellValue('B'.(5 + $change), "Nr zamówienia");
			$excel->getActiveSheet()->setCellValue('C'.(5 + $change), "Nr wewnętrzny");
			$excel->getActiveSheet()->setCellValue('D'.(5 + $change), "Identyfikator produktu");
			$excel->getActiveSheet()->setCellValue('E'.(5 + $change), "Nazwa produktu");
			$excel->getActiveSheet()->setCellValue('F'.(5 + $change), "Cena netto produktu (zł)");
			$excel->getActiveSheet()->setCellValue('G'.(5 + $change), "Identyfikator zamiennika");
			$excel->getActiveSheet()->setCellValue('H'.(5 + $change), "Nazwa zamiennika");
			$excel->getActiveSheet()->setCellValue('I'.(5 + $change), "Cena netto zamiennika (zł)");
			$excel->getActiveSheet()->setCellValue('J'.(5 + $change), "Oszczędności (zł)");
			$excel->getActiveSheet()->setCellValue('K'.(5 + $change), "Data zamówienia");
			
			$excel->getActiveSheet()->getStyle('B'.(5 + $change).':K'.(5 + $change))->applyFromArray(
				array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				),
				'font' => array(
					'bold' => true
				),
				'borders' => array(
					'top' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')), 
					'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
					'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
					'bottom' => array('style' => PHPExcel_Style_Border::BORDER_DASHED, 'color' => array('argb' => 'ffbe3d38'))
				)
			));
			
			$counter = $change;
			$savings = 0;
			
			while($product = mysql_fetch_array($data)) 
		   	{
		   		$counter++;
		   		
		   		$excel->getActiveSheet()->setCellValue('B'.(5 + $counter), $this->formatId($product['order_id'], $product['query_id']));
				$excel->getActiveSheet()->setCellValue('C'.(5 + $counter), $product['number']);
				$excel->getActiveSheet()->setCellValue('D'.(5 + $counter), $product['product_id']);
				$excel->getActiveSheet()->setCellValue('E'.(5 + $counter), $product['product_name']);
				$excel->getActiveSheet()->setCellValue('F'.(5 + $counter), $product['product_price']);
				$excel->getActiveSheet()->setCellValue('G'.(5 + $counter), $product['replacement_id']);
				$excel->getActiveSheet()->setCellValue('H'.(5 + $counter), $product['replacement_name']);
				$excel->getActiveSheet()->setCellValue('I'.(5 + $counter), $product['replacement_price']);
				$excel->getActiveSheet()->setCellValue('J'.(5 + $counter), $product['savings']);
				$excel->getActiveSheet()->setCellValue('K'.(5 + $counter), date('Y-m-d H:i', strtotime($product['time'])));
				
				$excel->getActiveSheet()->getStyle('B'.(5 + $counter).':K'.(5 + $counter))->applyFromArray(
					array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					),
					'borders' => array(
						'bottom' => array('style' => PHPExcel_Style_Border::BORDER_DASHED, 'color' => array('argb' => 'ffbe3d38')), 
						'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
						'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38'))
					)
				));
				
				$savings += $product['savings'];
		   	}
		   	
		   	$excel->getActiveSheet()->setCellValue('J'.(6 + $counter), $savings);
			$excel->getActiveSheet()->getStyle('J'.(6 + $counter))->applyFromArray(array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				),
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('argb'=>'ffffebea')), 
				'font' => array(
					'bold' => true)
				)
			);
			
			$excel->getActiveSheet()->getStyle('B'.(6 + $counter).':K'.(6 + $counter))->applyFromArray(
				array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				),
				'borders' => array(
					'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')), 
					'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
					'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38'))
				)
			));
		}
		else
		{
			$excel->getActiveSheet()->setCellValue('B'.(5 + $change), "brak wyników spełniających kryterium wyszukiwania");
			$excel->getActiveSheet()->mergeCells('B'.(5 + $change).':K'.(5 + $change));
			
			$excel->getActiveSheet()->getStyle('B'.(5 + $change).':K'.(5 + $change))->applyFromArray(
				array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				),
				'borders' => array(
					'top' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
					'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
					'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
					'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38'))
				)
			));
		}
		
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		
		$xls_writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		
		header('Content-Type: application/vnd.ms-excel');	
		header('Content-Disposition: attachment; filename="b2b_raport_oszczędności_'.date('Y_m_d').'.xls"');
		header('Cache-Control: private, max-age=0, must-revalidate');
		header('Pragma: public');
		ini_set('zlib.output_compression','0');
		
		$xls_writer->save('php://output');
	}
	
	public function getSavingsYears()
	{
		$query = 'SELECT DISTINCT DATE_FORMAT(time, \'%Y\') as year FROM order_savings ORDER BY year ASC';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $result;
	}
	
	public function exportSummary($sorts, $types, $like, $columns)
	{
		$excel = new PHPExcel();
		
		$excel->setActiveSheetIndex(0);
		
		$excel->getActiveSheet()->setCellValue('B2', 'Termin wygenerowania zestawienia:');
		$excel->getActiveSheet()->setCellValue('C2', date('Y-m-d H:i'));
		$excel->getActiveSheet()->getStyle('C2')->getFont()->setBold(true);
		
		$excel->getActiveSheet()->setCellValue('B3', 'Zestawienie wygenerował:');
		$excel->getActiveSheet()->setCellValue('C3', $this->_user['login']);
		$excel->getActiveSheet()->getStyle('C3')->getFont()->setBold(true);
		
		$excel->getActiveSheet()->getStyle('B2:C2')->applyFromArray(
			array('borders' => array(
				'top' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')), 
				'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')),
				'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9'))
			)
		));
		
		$excel->getActiveSheet()->getStyle('B3:C3')->applyFromArray(
			array('borders' => array(
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')), 
				'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')),
				'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9'))
			)
		));
		
		if (is_array($types) && !empty($types) && is_array($like) && !empty($like))
		{
			$excel->getActiveSheet()->setCellValue('B5', 'Ustawione filtry: ');
			$excel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
			
			$excel->getActiveSheet()->setCellValue('B6', 'Kolumna');
			$excel->getActiveSheet()->setCellValue('C6', 'Szukana fraza');
			$excel->getActiveSheet()->setCellValue('D6', 'Rodzaj zawężenia');
			
			$excel->getActiveSheet()->getStyle('B6')->getFont()->setBold(true);
			$excel->getActiveSheet()->getStyle('C6')->getFont()->setBold(true);
			$excel->getActiveSheet()->getStyle('D6')->getFont()->setBold(true);
			
			$excel->getActiveSheet()->getStyle('B6:D6')->applyFromArray(
				array('borders' => array(
					'top' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')), 
					'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')),
					'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9'))
				)
			));
			
			$counter = 7;
			
			foreach ($types as $key => $type)
			{
				switch ($key)
				{
					case "order_informations.fv": $column = 'FV'; break;
					case "order_informations.wm": $column = 'WZ'; break;
					case "orders.add_date": $column = 'Data złożenia'; break;
					case "orders.status": $column = 'Status zamówienia'; break;
					case "order_details.order_id": $column = 'Nr zamówienia'; break;
					case "orders.number": $column = 'Nr wewnętrzny'; break;
					case "order_details.query_id": $column = 'Nr zapytania'; break;
					case "contractor_login": $column = 'Zamawiający'; break;
					case "wsk_budget.department": $column = 'Wydział'; break;
					case "wsk_mpk.mpk": $column = 'Numer MPK'; break;
					case "order_details.name": $column = 'Nazwa produktu'; break;
					case "order_details.symbol": $column = 'Indeks produktu'; break;
					case "order_details.producer": $column = 'Producent'; break;
					case "order_details.amount": $column = 'Ilość zamówionych produktów'; break;
					case "order_details.tax": $column = 'Podatek VAT (%)'; break;
					case "order_details.netto_price": $column = 'Cena netto (zł)'; break;
				}
				
				$value = $like[$key];
				
				if (!empty($value))
				{
					$excel->getActiveSheet()->setCellValue('B'.$counter, $column);
					
					if (in_array($key, array("order_details.netto_price", "order_details.tax", "order_details.amount")))
					{
						$value = (float)str_replace(',', '.', $value);
					}
					
					if ($key == "orders.status")
					{
						switch ($value)
						{
							case 1: $value = 'oczekujące w Unimet'; break;
							case 2: $value = 'w trakcie realizacji'; break;
							case 3: $value = 'w drodze'; break;
							case 4: $value = 'archiwum'; break;
							case 5: $value = 'odrzucone'; break;
							case 6: $value = 'u analityka'; break;
							case 7: $value = 'u kierownika'; break;
							case 8: $value = 'weryfikacja w Unimet'; break;
							case 9: $value = 'u zamawiającego'; break;
							case 10: $value = 'u administratora'; break;
						}
					}
					
					$excel->getActiveSheet()->setCellValue('C'.$counter, $value);
					
					switch ((int)$type)
					{
						case 3: $comparison = 'równy'; break;
						case 2: $comparison = 'większy'; break;
						case -1: $comparison = 'mniejszy'; break;
						default: $comparison = 'zawiera'; break;
					}
					
					$excel->getActiveSheet()->setCellValue('D'.$counter, $comparison);
					
					$excel->getActiveSheet()->getStyle('B'.$counter.':D'.$counter)->applyFromArray(
						array('borders' => array(
							'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')),
							'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9'))
						)
					));
					
					$counter;
				}
			}
		}
		
		$excel->getActiveSheet()->getStyle('B'.$counter.':D'.$counter)->applyFromArray(
			array('borders' => array(
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9'))
			)
		));
		
		$counter++;
		$counter++;
		
		if (is_array($columns) && !empty($columns))
		{
			$alphabet = array('B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T');
			$data_sum = array();
			$indeks = 0;
			
			if ((int)$columns['orders.status'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Status zamówienia");
				$indeks++;
			}
			if ((int)$columns['order_details.order_id'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Nr zamówienia");
				$indeks++;
			}
			if ((int)$columns['orders.number'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Nr wewnętrzny");
				$indeks++;
			}
			if ((int)$columns['order_details.query_id'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Nr zapytania");
				$indeks++;
			}
			if ((int)$columns['contractor_login'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Zamawiający");
				$indeks++;
			}
			if ((int)$columns['wsk_budget.department'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Wydział");
				$indeks++;
			}
			if ((int)$columns['wsk_mpk.mpk'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "MPK");
				$indeks++;
			}
			if ((int)$columns['order_details.name'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Nazwa produktu");
				$indeks++;
			}
			if ((int)$columns['order_details.symbol'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Indeks produktu");
				$indeks++;
			}
			if ((int)$columns['order_details.producer'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Producent");
				$indeks++;
			}
			if ((int)$columns['order_details.amount'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Ilość");
				$indeks++;
			}
			if ((int)$columns['order_details.tax'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "VAT (%)");
				$indeks++;
			}
			if ((int)$columns['order_details.tax_value'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Wartość VAT (zł)");
				$data_sum[$indeks] = "=";
				$indeks++;
			}
			if ((int)$columns['order_details.netto_price'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Cena netto (zł)");
				$data_sum[$indeks] = "=";
				$indeks++;
			}
			if ((int)$columns['order_details.netto_value'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Wartość netto (zł)");
				$data_sum[$indeks] = "=";
				$indeks++;
			}
			if ((int)$columns['order_details.brutto_value'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Wartość brutto (zł)");
				$data_sum[$indeks] = "=";
				$indeks++;
			}
			if ((int)$columns['orders.add_date'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Data złożenia");
				$indeks++;
			}
			if ((int)$columns['order_informations.fv'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "FV");
				$indeks++;
			}
			if ((int)$columns['order_informations.wm'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "WZ");
				$indeks++;
			}
			
			$excel->getActiveSheet()->getStyle('B'.($counter).':'.$alphabet[($indeks-1)].($counter))->applyFromArray(
				array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				),
				'font' => array(
					'bold' => true
				),
				'borders' => array(
					'top' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')), 
					'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
					'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
					'bottom' => array('style' => PHPExcel_Style_Border::BORDER_DASHED, 'color' => array('argb' => 'ffbe3d38'))
				)
			));
			
			$counter++;
			
			$data = $this->getSummary($sorts, $types, $like);
			
			if (mysql_num_rows($data))
			{
				while($product = mysql_fetch_array($data)) 
		   		{
		   			switch ($product['status'])
			       	{
			       		case 1: $product['status'] = 'oczekujące w Unimet'; break;
			       		case 2: $product['status'] = 'w trakcie realizacji'; break;
			       		case 3: $product['status'] = 'w drodze'; break;
			       		case 4: $product['status'] = 'archiwum'; break;
			       		case 5: $product['status'] = 'odrzucone'; break;
			       		case 6: $product['status'] = 'u analityka'; break;
			       		case 7: $product['status'] = 'u kierownika'; break;
			       		case 8: $product['status'] = 'weryfikacja w Unimet'; break;
			       		case 9: $product['status'] = 'u zamawiającego'; break;
			       		case 10: $product['status'] = 'u administratora'; break;
			       	}
			       	
			   		$indeks = 0;
				
					if ((int)$columns['orders.status'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['status']);
						$indeks++;
					}
					if ((int)$columns['order_details.order_id'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $this->formatId($product['order_id'], $product['query_id']));
						$indeks++;
					}
					if ((int)$columns['orders.number'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['number']);
						$indeks++;
					}
					if ((int)$columns['order_details.query_id'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, (($product['query_id'] > 0) ? $this->formatId($product['query_id'], $product['query_id']) : ""));
						$indeks++;
					}
					if ((int)$columns['contractor_login'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['contractor_login']);
						$indeks++;
					}
					if ((int)$columns['wsk_budget.department'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['department']);
						$indeks++;
					}
					if ((int)$columns['wsk_mpk.mpk'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['mpk']);
						$indeks++;
					}
					if ((int)$columns['order_details.name'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['product']);
						$indeks++;
					}
					if ((int)$columns['order_details.symbol'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['symbol']);
						$indeks++;
					}
					if ((int)$columns['order_details.producer'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['producer']);
						$indeks++;
					}
					if ((int)$columns['order_details.amount'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, str_replace(".", ",", (((int)$product['amount'] == $product['amount']) ? (int)$product['amount'] : $product['amount'])));
						$indeks++;
					}
					if ((int)$columns['order_details.tax'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, str_replace(".", ",", $product['tax']));
						$indeks++;
					}
					if ((int)$columns['order_details.tax_value'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, sprintf("%0.2f", ($product['tax'] * $product['netto_price'] / 100)));
						$data_sum[$indeks] .= $alphabet[$indeks].$counter.'+';
						$indeks++;
					}
					if ((int)$columns['order_details.netto_price'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['netto_price']);
						$data_sum[$indeks] .= $alphabet[$indeks].$counter.'+';
						$indeks++;
					}
					if ((int)$columns['order_details.netto_value'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, sprintf("%0.2f", ($product['netto_price'] * $product['amount'])));
						$data_sum[$indeks] .= $alphabet[$indeks].$counter.'+';
						$indeks++;
					}
					if ((int)$columns['order_details.brutto_value'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, sprintf("%0.2f", ($product['brutto_price'] * $product['amount'])));
						$data_sum[$indeks] .= $alphabet[$indeks].$counter.'+';
						$indeks++;
					}
		   			if ((int)$columns['orders.add_date'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['add_date']);
						$indeks++;
					}
					if ((int)$columns['order_informations.fv'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['fv']);
						$indeks++;
					}
					if ((int)$columns['order_informations.wm'] == 1)
					{
						$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['wm']);
						$indeks++;
					}
			       	
					$excel->getActiveSheet()->getStyle($alphabet[0].$counter.':'.$alphabet[($indeks-1)].$counter)->applyFromArray(
						array(
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
						),
						'borders' => array(
							'bottom' => array('style' => PHPExcel_Style_Border::BORDER_DASHED, 'color' => array('argb' => 'ffbe3d38')), 
							'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
							'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38'))
						)
					));
					
					$counter++;
		   		}
		   		
		   		foreach ($data_sum as $key => $value)
		   		{
		   			$excel->getActiveSheet()->setCellValue($alphabet[$key].$counter, substr($value, 0, -1));
					$excel->getActiveSheet()->getStyle($alphabet[$key].$counter)->applyFromArray(array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('argb'=>'ffffebea')), 
						'font' => array(
							'bold' => true)
						)
					);
		   		}

		   		$excel->getActiveSheet()->getStyle($alphabet[0].$counter.':'.$alphabet[$indeks-1].$counter)->applyFromArray(
					array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					),
					'borders' => array(
						'bottom' => array('style' => PHPExcel_Style_Border::BORDER_DASHED, 'color' => array('argb' => 'ffbe3d38')), 
						'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
						'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38'))
					)
				));
		   		
		   		$excel->getActiveSheet()->getStyle($alphabet[0].$counter.':'.$alphabet[$indeks-1].$counter)->applyFromArray(
					array('borders' => array(
						'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38'))
					)
				));
			}
			else
			{
				$excel->getActiveSheet()->setCellValue('B'.$counter, "brak wyników spełniających kryterium wyszukiwania");
				$excel->getActiveSheet()->mergeCells('B'.$counter.':'.$alphabet[($indeks-1)].$counter);
				
				$excel->getActiveSheet()->getStyle('B'.$counter.':'.$alphabet[($indeks-1)].$counter)->applyFromArray(
					array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					),
					'borders' => array(
						'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
						'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
						'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38'))
					)
				));
			}
		}
		
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);

		$xls_writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		
		header('Content-Type: application/vnd.ms-excel');	
		header('Content-Disposition: attachment; filename="b2b_zestawienie_'.date('Y_m_d').'.xls"');
		header('Cache-Control: private, max-age=0, must-revalidate');
		header('Pragma: public');
		ini_set('zlib.output_compression','0');
		
		$xls_writer->save('php://output');
	}
	
	public function getSummary($sorts, $types, $like)
	{
		$query = 'SELECT orders.status, orders.add_date, order_details.order_id, orders.number, order_details.name as product, order_details.symbol, (order_details.netto_price * order_details.amount) as netto_value, (order_details.brutto_price * order_details.amount) as brutto_value, order_details.netto_price, 
		order_details.tax, order_details.producer, order_details.amount, order_details.brutto_price, order_details.query_id, order_informations.fv, order_informations.wm, wsk_mpk.mpk, contractor_login, 
		analyst_login, manager_login, receiving_login, wsk_budget.department FROM orders JOIN order_details ON order_details.order_id = orders.id JOIN 
		wsk_mpk ON orders.id = wsk_mpk.order_id LEFT JOIN wsk_budget ON wsk_mpk.budget = wsk_budget.id LEFT JOIN order_informations ON orders.id = 
		order_informations.order_id WHERE orders.status != 11 ';
		
		$counter = 0;
		
		if (is_array($types) && !empty($types) && is_array($like) && !empty($like))
		{
			foreach ($types as $key => $type)
			{
				$value = $like[$key];
				
				if ( (!empty($value)) || ((int)$type == 4 && ( (!empty($like["orders.add_date_from"]) && !empty($like["orders.add_date_to"]) && 
				date("Y-m-d H:i:s", strtotime($like["orders.add_date_from"])) <= date("Y-m-d H:i:s", strtotime($like["orders.add_date_to"]))) || 
				(!empty($like["orders.add_date_from"]) && empty($like["orders.add_date_to"])) || (empty($like["orders.add_date_from"]) && 
				!empty($like["orders.add_date_to"])) )) )
				{
					if (in_array($key, array("order_details.netto_price", "order_details.tax", "order_details.amount")))
					{
						$value = (float)str_replace(',', '.', $value);
					}
					
					$query .= ' AND '.(((int)$type == 4) ? '(' : '').$key;
					
					switch ((int)$type)
					{
						case 3: 
							$query .= ' = \''.mysql_real_escape_string($value).'\''; 
							break;
							
						case 2: 
							$query .= ' > \''.mysql_real_escape_string($value).'\''; 
							break;
							
						case -1: 
							$query .= ' < \''.mysql_real_escape_string($value).'\''; 
							break;
							
						case 4:
							
							if (!empty($like["orders.add_date_from"]) && !empty($like["orders.add_date_to"]))
							{
								$query .= ' BETWEEN \''.trim(mysql_real_escape_string($like["orders.add_date_from"])).'\' 
								AND \''.trim(mysql_real_escape_string($like["orders.add_date_to"])).'\')'; 
							}
							else if (!empty($like["orders.add_date_from"]))
							{
								$query .= ' >= \''.trim(mysql_real_escape_string($like["orders.add_date_from"])).'\')'; 
							}
							else if (!empty($like["orders.add_date_to"]))
							{
								$query .= '<= \''.trim(mysql_real_escape_string($like["orders.add_date_to"])).'\')'; 
							}
							
							break;
							
						default: 
							$part = 0;
		
							if (in_array($key, array('order_details.query_id', 'order_details.order_id')))
							{
								$substrings = explode('/', $value);
								$substrings_count = count($substrings);
													
								if (!strcmp(strtolower(substr($value, -1)), "z"))
								{
									switch ($substrings_count)
									{
										case 3:
											$value = '2'.sprintf("%03d", $substrings[1]).sprintf("%06d", $substrings[0]);
											break;
																
										case 2:
											$part = 2;
											$value = '2'.sprintf("%03d", $substrings[0]);
											break;
									}
								}
								else
								{
									switch ($substrings_count)
									{
										case 2:
											$value = '2'.sprintf("%03d", $substrings[1]).sprintf("%06d", $substrings[0]);
											break;
									}
								}
							}
							
							switch ($part)
							{
								case 1: 
									$query .= ' LIKE \'%'.mysql_real_escape_string($value).'\''; 
									break;
									
								case 2: 
									$query .= ' LIKE \''.mysql_real_escape_string($value).'%\''; 
									break;
									
								default: 
									$query .= ' LIKE \'%'.mysql_real_escape_string($value).'%\''; 
									break;
							}
							
							break;
					}
					
					$counter++;
				}
			}
		}
		
		if (empty($like['wsk_budget.department']) || !isset($like['wsk_budget.department']))
		{
			$users = new Users();
			
			$budget_list = $users->getBudgets( ($this->_user['admin'] == 1) ? true : false );
			
			if (is_array($budget_list) && !empty($budget_list))
			{
				$query .= ' AND wsk_budget.department IN (';
				
				foreach ((array)$budget_list as $budget)
				{
					$query .= '\''.mysql_real_escape_string($budget['department']).'\',';
				}
				
				$query = substr($query, 0, -1).')';
				
			}	
		}
		
		$query .= ' ORDER BY ';	
		
		$default_order = true;
		
		if (is_array($sorts) && !empty($sorts))
		{
			foreach ($sorts as $key => $value)
			{
				if (!empty($value))
				{
					if (true == $default_order)
					{
						$default_order = false;
					}
					
					$query .= $key.' '.$value.', ';
				}
			}
		}
		
		if (true == $default_order)
		{
			$query .= 'add_date DESC';
		}
		else
		{
			$query = substr($query, 0, -2);
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $result;
	}
	
	public function checkUserType()
	{
		if (true === $this->getSession("b2b_full_offer"))
		{
			return false;
		}
		
		$result = $this->_db->query('SELECT individual, what_with_new FROM groups_tree_user WHERE user_id = '.(int)$this->_user['parent_id']) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			mysql_free_result($result);
			
			if ($row['individual'] == 1)
			{
				return true;
			}
		}
		
		return false;
	}
	
	public function getFavoriteProducts()
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT DISTINCT products.id_hermes, products.symbol, product_versions.name FROM products_status JOIN favorite_products ON 
			products_status.id = favorite_products.product_id AND favorite_products.user_id = products_status.user JOIN products ON products_status.id = 
			products.id_hermes JOIN product_versions ON products.id_hermes = product_versions.product_id WHERE products_status.status = 1 AND 
			products_status.user = '.$this->_user['parent_id'].' ORDER BY product_versions.name ASC';
		}
		else
		{
			$query = 'SELECT DISTINCT products.id_hermes, products.symbol, product_versions.name FROM products JOIN favorite_products ON products.id_hermes = 
			favorite_products.product_id JOIN product_versions ON products.id_hermes = product_versions.product_id WHERE favorite_products.user_id = 
			'.$this->_user['id'].' ORDER BY product_versions.name ASC';
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $result;
	}
	
	public function getConcession($product_id)
	{
		$result = $this->_db->query('SELECT IFNULL(concessions.name, \'\') as name FROM products JOIN concessions ON products.concession_id = concessions.id WHERE products.id_hermes = '.(int)$product_id) 
		or $this->_db->raise_error();
		
		$row = mysql_fetch_array($result);
		return $row['name'];
	}
	
	public function addFavoriteProducts($actives)
	{
		if (is_array($actives))
		{	
			if ($this->_user['id'])
			{
				$query = 'INSERT INTO favorite_products VALUES ';
				
				foreach ($actives as $product_id)
				{
					$query .= '(\'\', '.$this->_user['id'].', '.$product_id.'),';
				}
				
				$query = substr($query, 0, -1);
				$result = $this->_db->query($query) or $this->_db->raise_error();
				
				if ($result)
				{
					$comunicats['ok'] = 'Produkty zostały dodane do ulubionych';
					$this->setSession("communicats", $comunicats);
				}
			}
		}
	}
	
	public function getSubgroup($product_id)
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT groups_tree.id, groups_tree.name FROM products_status JOIN products ON products_status.id = products.id_hermes AND products_status.status = 1 AND 
			products_status.user = '.$this->_user['parent_id'].' JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE id_hermes = '.$product_id;
		}
		else
		{
			$query = 'SELECT groups_tree.id, groups_tree.name FROM products JOIN groups_tree ON products.subgroup_id = groups_tree.id 
			WHERE id_hermes = '.$product_id;
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		return mysql_fetch_array($result);
	}
	
	public function getPromotions()
	{
		$promotions = array();
		
		$result = $this->_db->query('SELECT id, restriction, `range`, range_detail FROM promotions WHERE space IN (0,1) AND start_date <= \''.date('Y-m-d').'\' 
		AND end_date >= \''.date('Y-m-d').'\'') or $this->_db->raise_error();
		
		while($row = mysql_fetch_array($result)) 
   		{
       		switch ($row['restriction'])
       		{
       			case 0: // wszyscy użytkownicy
       				
       				switch ($row['range'])
       				{
       					case 2: // grupy hermesowskie (logo)
       						$list = $this->_db->query('SELECT id_hermes FROM products JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE subgroup_id = '.$row['range_detail']) or $this->_db->raise_error();
       						
       						while($element = mysql_fetch_array($list)) 
   							{
   								array_push($promotions, $element['id_hermes']);
   							}
       						break;
       						
       					case 3: // producent
       						$list = $this->_db->query('SELECT id_hermes FROM products JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE producer_id = '.$row['range_detail']) or $this->_db->raise_error();
       						
       						while($element = mysql_fetch_array($list)) 
   							{
   								array_push($promotions, $element['id_hermes']);
   							}
       						break;
       						
       					default: // produkt
       						array_push($promotions, $row['range_detail']);
       						break;
       				}
       				
       				break;
       				
       			case 1: // wskazani użytkownicy
       				
       				switch ($row['range'])
       				{
       					case 2: // grupy hermesowskie (logo)
       						
       						$list = $this->_db->query('SELECT id_hermes FROM products JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE subgroup_id = '.$row['range_detail'].' AND '.(int)$this->_user['id_hermes'].' IN (SELECT user_id FROM promotion_details WHERE promotion_id = '.$row['id'].')') or $this->_db->raise_error();
       						
       						while($element = mysql_fetch_array($list)) 
   							{
   								array_push($promotions, $element['id_hermes']);
   							}
       						break;
       						
       					case 3: // producent
       						
       						$list = $this->_db->query('SELECT id_hermes FROM products JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE producer_id = '.$row['range_detail'].' AND '.(int)$this->_user['id_hermes'].' IN (SELECT user_id FROM promotion_details WHERE promotion_id = '.$row['id'].')') or $this->_db->raise_error();

       						while($element = mysql_fetch_array($list)) 
   							{
   								array_push($promotions, $element['id_hermes']);
   							}
       						break;
       						
       					default: // produkt
							$list = $this->_db->query('SELECT id_hermes FROM products JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE id_hermes = '.$row['range_detail'].' AND '.(int)$this->_user['id_hermes'].' IN (SELECT user_id FROM promotion_details WHERE promotion_id = '.$row['id'].')') or $this->_db->raise_error();
       						
							$element = mysql_fetch_array($list);
							array_push($promotions, $element['id_hermes']);
							
							break;
       				}
       				
       				break;
       				
       			default: // wszyscy użytkownicy z wyjątkami

       				switch ($row['range'])
       				{
       					case 2: // grupy hermesowskie (logo)
       						$list = $this->_db->query('SELECT id_hermes FROM products JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE subgroup_id = '.$row['range_detail'].' AND '.(int)$this->_user['id_hermes'].' NOT IN (SELECT user_id FROM promotion_details WHERE promotion_id = '.$row['id'].')') or $this->_db->raise_error();
       						
       						while($element = mysql_fetch_array($list)) 
   							{
   								array_push($promotions, $element['id_hermes']);
   							}
       						break;
       						
       					case 3: // producent
       						$list = $this->_db->query('SELECT id_hermes FROM products JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE producer_id = '.(int)$row['range_detail'].' AND '.(int)$this->_user['id_hermes'].' NOT IN (SELECT user_id FROM promotion_details WHERE promotion_id = '.$row['id'].')') or $this->_db->raise_error();
       						
       						while($element = mysql_fetch_array($list)) 
   							{
   								array_push($promotions, $element['id_hermes']);
   							}
       						break;
       						
       					default: // produkt
							$list = $this->_db->query('SELECT id_hermes FROM products JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE id_hermes = '.(int)$row['range_detail'].' AND '.(int)$this->_user['id_hermes'].' NOT IN (SELECT user_id FROM promotion_details WHERE promotion_id = '.$row['id'].')') or $this->_db->raise_error();
       						
							$element = mysql_fetch_array($list);
							array_push($promotions, $element['id_hermes']);
							
							break;
       				}
       				
       				break;
       		}
   		}
		
		return $promotions;
	}
	
	public function getProductId($symbol)
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT id_hermes FROM products_status JOIN products ON products_status.id = products.id_hermes AND products_status.status = 1 AND 
			products_status.user = '.$this->_user['parent_id'].' AND symbol = \''.mysql_real_escape_string($symbol).'\'';
		}
		else
		{
			$query = 'SELECT id_hermes FROM products WHERE symbol = \''.mysql_real_escape_string($symbol).'\'';
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			return $row['id_hermes'];
		}
		
		return 0;
	}
	
	public function getProducts($group_id, $sort, $producer_id, $visibility, $search = array())
	{		
		if (true == $this->checkUserType())
		{
			$query = 'SELECT groups_tree.id FROM products_status JOIN groups_tree ON products_status.subgroup = groups_tree.comment WHERE 
			products_status.status = 1 AND products_status.user = '.$this->_user['parent_id'].' AND comment LIKE concat((SELECT comment FROM groups_tree 
			WHERE id = '.$group_id.'),\'%\')';
			$result = $this->_db->query($query) or $this->_db->raise_error();
			
			$query = 'SELECT products.id_hermes, products_status.status as product_status, (SELECT IFNULL(symbol, \'\') FROM product_replacements WHERE product_id = products.id_hermes LIMIT 1) as 
			replacement, subgroup_id, groups_tree.name as subgroup_name, symbol, product_versions.name, product_versions.description, weight, pallet, unit, 
			producers.name as producer_name, IFNULL(DATEDIFF(END , NOW()), \'-1\') AS news, IFNULL(products_availability.status, 0) as status FROM 
			products_status JOIN producers_status ON products_status.producers = producers_status.name AND products_status.status = producers_status.status 
			AND products_status.user = producers_status.user JOIN products ON products_status.id = products.id_hermes AND products_status.user = '.$this->_user['parent_id'].' AND 
			products_status.status = 1 JOIN product_versions ON products.id_hermes = product_versions.product_id AND version_id = 1 JOIN product_details ON 
			products.id_hermes = product_details.product_id JOIN producers ON products.producer_id = producers.id JOIN groups_tree ON products.subgroup_id = 
			groups_tree.id LEFT JOIN newses ON products.id_hermes = newses.product_id LEFT JOIN products_availability ON products.id_hermes = 
			products_availability.id_hermes WHERE products.subgroup_id > 0 AND products.subgroup_id IN (';
		}
		else
		{
			$query = 'SELECT id FROM groups_tree WHERE comment LIKE concat((SELECT comment FROM groups_tree WHERE id = '.$group_id.'),\'%\')';
			$result = $this->_db->query($query) or $this->_db->raise_error();
			
			$query = 'SELECT products.id_hermes, products_status.status as product_status, (SELECT IFNULL(symbol, \'\') FROM product_replacements WHERE product_id = products.id_hermes LIMIT 1) as replacement, 
			subgroup_id, groups_tree.name as subgroup_name, symbol, product_versions.name, product_versions.description, weight, pallet, unit, 
			producers.name as producer_name, IFNULL(DATEDIFF(END , NOW()), \'-1\') AS news, IFNULL(products_availability.status, 0) as status 
			FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id AND version_id = 1 JOIN product_details ON 
			products.id_hermes = product_details.product_id JOIN producers ON products.producer_id = producers.id JOIN groups_tree ON 
			products.subgroup_id = groups_tree.id LEFT JOIN newses ON products.id_hermes = newses.product_id LEFT JOIN products_availability ON 
			products.id_hermes = products_availability.id_hermes LEFT JOIN products_status ON products.id_hermes = products_status.id AND products_status.user = '.(int)$this->_user['parent_id'].' WHERE products.subgroup_id > 0 AND products.subgroup_id IN (';
			
		}
		
		while($row = mysql_fetch_array($result)) 
	   	{
	       	$query .= $row['id'].',';
	   	}
			
		$query .= '0) ';
		
		if ($producer_id)
		{
			$query .= ' AND producer_id = '.$producer_id;
		}
		
		if ($visibility)
		{
			$query .= ' AND amount > 0';
		}
		
		if (is_array($search) && !empty($search))
		{
			$query .= ' AND product_versions.name LIKE \'%'.mysql_real_escape_string($search[0]).'%\' AND symbol LIKE \'%'.mysql_real_escape_string($search[1]).'%\' 
			AND producers.name LIKE \'%'.mysql_real_escape_string($search[2]).'%\'';
		}
		
		$query .= ' ORDER BY producers.order ASC,';
		
		$sort_details = explode('_', $sort);
		
		switch ($sort_details[0])
		{
			case 'subgroup': $query .= ' groups_tree.name '.$sort_details[1]; break;
			case 'producer': $query .= ' producers.name '.$sort_details[1]; break;
			case 'price': $query .= ' netto_6 '.$sort_details[1]; break;
			default: $query .= ' symbol ASC'; break;
		}

		$this->_db->query("INSERT INTO `sql` (`sql`, `time`) VALUES ('".mysql_real_escape_string($query)."', '".(microtime(true)-$actual_time)."')") or $this->_db->raise_error();
		
		$result = $this->_db->query($query.' LIMIT 650') or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getNewsesCount($visibility, $producer_id)
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT id_hermes FROM products_status JOIN products ON products_status.id = products.id_hermes JOIN product_versions ON products.id_hermes = product_versions.product_id AND 
			version_id = 1 JOIN newses ON products.id_hermes = newses.product_id JOIN groups_tree ON products.subgroup_id = groups_tree.id 
			WHERE products_status.status = 1 AND products_status.user = '.$this->_user['parent_id'].' AND IFNULL(DATEDIFF(end, NOW()), \'-1\') >= 0';
		}
		else
		{
			$query = 'SELECT id_hermes FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id AND 
			version_id = 1 JOIN newses ON products.id_hermes = newses.product_id JOIN groups_tree ON products.subgroup_id = groups_tree.id 
			WHERE IFNULL(DATEDIFF(end, NOW()), \'-1\') >= 0';
		}
			
		if ($visibility)
		{
			$query .= ' AND amount > 0';
		}
			
		if ($producer_id)
		{
			$query .= ' AND producer_id = '.$producer_id;
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		return mysql_num_rows($result);
	}
	
	public function getNewses($sort, $producer_id, $visibility, $subgroup_id = 0)
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT products.id_hermes, (SELECT count(product_id) FROM product_replacements WHERE product_id = products.id_hermes) as replacement, 
			subgroup_id, groups_tree.name as subgroup_name, symbol, product_versions.description, product_versions.name, weight, pallet, unit, producers.name 
			as producer_name, IFNULL(DATEDIFF(END , NOW()), \'-1\') AS news, IFNULL(products_availability.status, 0) as status FROM products_status JOIN 
			products ON products_status.id = products.id_hermes AND products_status.status = 1 AND products_status.user = '.$this->_user['parent_id'].' JOIN 
			product_versions ON products.id_hermes = product_versions.product_id AND version_id = 1 JOIN product_details ON products.id_hermes = 
			product_details.product_id JOIN producers ON products.producer_id = producers.id JOIN groups_tree ON products.subgroup_id = groups_tree.id 
			JOIN newses ON products.id_hermes = newses.product_id LEFT JOIN products_availability ON products.id_hermes = products_availability.id_hermes 
			WHERE DATEDIFF(end, NOW()) >= 0';
		}
		else
		{
			$query = 'SELECT products.id_hermes, (SELECT count(product_id) FROM product_replacements WHERE product_id = products.id_hermes) as replacement, 
			subgroup_id, groups_tree.name as subgroup_name, symbol, product_versions.description, product_versions.name, weight, pallet, unit, producers.name 
			as producer_name, IFNULL(DATEDIFF(END , NOW()), \'-1\') AS news, IFNULL(products_availability.status, 0) as status FROM products JOIN 
			product_versions ON products.id_hermes = product_versions.product_id AND version_id = 1 JOIN product_details ON products.id_hermes = 
			product_details.product_id JOIN producers ON products.producer_id = producers.id JOIN groups_tree ON products.subgroup_id = groups_tree.id 
			JOIN newses ON products.id_hermes = newses.product_id LEFT JOIN products_availability ON products.id_hermes = products_availability.id_hermes 
			WHERE DATEDIFF(end, NOW()) >= 0';
		}
		
		if ($producer_id)
		{
			$query .= ' AND producer_id = '.$producer_id;
		}
		
		if ($visibility)
		{
			$query .= ' AND amount > 0';
		}
		
		if ($subgroup_id)
		{
			$query .= ' AND subgroup_id = '.$subgroup_id;
		}
		
		$query .= ' ORDER BY';
		
		$sort_details = explode('_', $sort);
		
		switch ($sort_details[0])
		{
			case 'subgroup': $query .= ' groups_tree.name '.$sort_details[1]; break;
			case 'producer': $query .= ' producers.name '.$sort_details[1]; break;
			case 'price': $query .= ' netto_6 '.$sort_details[1]; break;
			default: $query .= ' symbol ASC'; break;
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getPromotionProducts($sort, $producer_id, $visibility, $promotions)
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT products.id_hermes, (SELECT count(product_id) FROM product_replacements WHERE product_id = products.id_hermes) as replacement, 
			subgroup_id, groups_tree.name as subgroup_name, symbol, product_versions.description, product_versions.name, weight, pallet, unit, producers.name 
			as producer_name, IFNULL(DATEDIFF(END , NOW()), \'-1\') AS news, IFNULL(products_availability.status, 0) as status FROM products_status JOIN 
			products ON products_status.id = products.id_hermes AND products_status.status = 1 AND products_status.user = '.$this->_user['parent_id'].' JOIN 
			product_versions ON products.id_hermes = product_versions.product_id AND version_id = 1 JOIN product_details ON products.id_hermes = 
			product_details.product_id JOIN producers ON products.producer_id = producers.id JOIN groups_tree ON products.subgroup_id = groups_tree.id 
			LEFT JOIN newses ON products.id_hermes = newses.product_id LEFT JOIN products_availability ON products.id_hermes = products_availability.id_hermes 
			WHERE products.id_hermes IN (0,';
		}
		else
		{
			$query = 'SELECT products.id_hermes, (SELECT count(product_id) FROM product_replacements WHERE product_id = products.id_hermes) as replacement, 
			subgroup_id, groups_tree.name as subgroup_name, symbol, product_versions.description, product_versions.name, weight, pallet, unit, producers.name 
			as producer_name, IFNULL(DATEDIFF(END , NOW()), \'-1\') AS news, IFNULL(products_availability.status, 0) as status FROM products JOIN 
			product_versions ON products.id_hermes = product_versions.product_id AND version_id = 1 JOIN product_details ON products.id_hermes = 
			product_details.product_id JOIN producers ON products.producer_id = producers.id JOIN groups_tree ON products.subgroup_id = groups_tree.id 
			LEFT JOIN newses ON products.id_hermes = newses.product_id LEFT JOIN products_availability ON products.id_hermes = products_availability.id_hermes 
			WHERE products.id_hermes IN (0,';
		}
		
		foreach ($promotions as $promotion)
		{
			$query .= (int)$promotion.',';
		}
		
		$query = substr($query, 0, -1);
		$query .= ')';
		
		if ($producer_id)
		{
			$query .= ' AND producer_id = '.$producer_id;
		}
		
		if ($visibility)
		{
			$query .= ' AND amount > 0';
		}
		
		$query .= ' ORDER BY';
		
		$sort_details = explode('_', $sort);
		
		switch ($sort_details[0])
		{
			case 'subgroup': $query .= ' groups_tree.name '.$sort_details[1]; break;
			case 'producer': $query .= ' producers.name '.$sort_details[1]; break;
			case 'price': $query .= ' netto_6 '.$sort_details[1]; break;
			default: $query .= ' symbol ASC'; break;
		}

		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getHeaderProducts()
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT products.id_hermes, product_versions.name, producers.name as producer_name FROM products_status JOIN products ON 
			products_status.id = products.id_hermes AND products_status.status = 1 AND products_status.user = '.$this->_user['parent_id'].' JOIN header_products 
			ON products.id_hermes = header_products.product_id JOIN product_versions ON products.id_hermes = product_versions.product_id AND version_id = 1 
			JOIN producers ON products.producer_id = producers.id ORDER BY product_versions.name ASC';
		}
		else
		{
			$query = 'SELECT products.id_hermes, product_versions.name, producers.name as producer_name FROM header_products JOIN products ON 
			header_products.product_id = products.id_hermes JOIN product_versions ON products.id_hermes = product_versions.product_id AND version_id = 1 JOIN 
			producers ON products.producer_id = producers.id ORDER BY product_versions.name ASC';
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $result;
	}
	
	public function getProduct($product_id)
	{		
		if (true == $this->checkUserType())
		{
			$query = 'SELECT subgroup_id, products_status.status as product_status, groups_tree.name as subgroup_name, producer_id, producers.name as producer_name, product_versions.name, symbol, 
			unit, unit_detail, product_versions.description, tax, products.id_hermes, weight, pallet, ean, IFNULL(products_availability.status, 0) as status 
			FROM products_status JOIN products ON products_status.id = products.id_hermes AND products_status.status = 1 AND 
			products_status.user = '.$this->_user['parent_id'].' JOIN product_versions ON products.id_hermes = product_versions.product_id JOIN groups_tree ON 
			products.subgroup_id = groups_tree.id JOIN producers ON products.producer_id = producers.id LEFT JOIN products_availability ON 
			products.id_hermes = products_availability.id_hermes WHERE products.id_hermes = '.$product_id;
		}
		else
		{
			$query = 'SELECT subgroup_id, products_status.status as product_status, groups_tree.name as subgroup_name, producer_id, producers.name as producer_name, product_versions.name, symbol, 
			unit, unit_detail, product_versions.description, tax, products.id_hermes, weight, pallet, ean, IFNULL(products_availability.status, 0) as status 
			FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id JOIN groups_tree ON products.subgroup_id = groups_tree.id 
			JOIN producers ON products.producer_id = producers.id LEFT JOIN products_availability ON products.id_hermes = products_availability.id_hermes 
			LEFT JOIN products_status ON products.id_hermes = products_status.id AND products_status.user = '.(int)$this->_user['parent_id'].' WHERE products.id_hermes = '.$product_id;
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return mysql_fetch_array($result);
	}
	
	public function getReplacement($product_id)
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT products.id_hermes, product_versions.name, products.symbol FROM products_status JOIN products ON products_status.id = 
			products.id_hermes AND products_status.status = 1 AND products_status.user = '.$this->_user['parent_id'].' JOIN product_replacements ON products.symbol 
			= product_replacements.symbol JOIN product_versions ON products.id_hermes = product_versions.product_id AND version_id = 1 WHERE 
			product_replacements.product_id = '.$product_id;
		}
		else
		{
			$query = 'SELECT products.id_hermes, product_versions.name, products.symbol FROM product_replacements JOIN products ON 
			product_replacements.symbol = products.symbol JOIN product_versions ON products.id_hermes = product_versions.product_id AND version_id = 1 
			WHERE product_replacements.product_id = '.$product_id;
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		$row = mysql_fetch_array($result);
		
		return $row;
	}
	
	public function getFrequentlyPurchased($subgroup_id)
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT products.id_hermes, product_versions.name, products.symbol FROM products_status JOIN products ON products_status.id = 
			products.id_hermes AND products_status.status = 1 AND products_status.user = '.$this->_user['parent_id'].' JOIN product_versions ON products.id_hermes = 
			product_versions.product_id WHERE subgroup_id = '.$subgroup_id.' ORDER BY amount DESC LIMIT 10';
		}
		else
		{
			$query = 'SELECT products.id_hermes, product_versions.name, products.symbol FROM products JOIN product_versions ON products.id_hermes = 
			product_versions.product_id WHERE subgroup_id = '.$subgroup_id.' ORDER BY amount DESC LIMIT 10';
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		return $result;
	}
	
	public function getAdditionalPicuters($product_id)
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT product_galleries.id FROM product_galleries JOIN products_status ON product_galleries.product_id = products_status.id WHERE 
			products_status.status = 1 AND products_status.user = '.$this->_user['parent_id'].' AND product_id = '.$product_id.' ORDER BY `order` ASC';
		}
		else
		{
			$query = 'SELECT id FROM product_galleries WHERE product_id = '.$product_id.' ORDER BY `order` ASC';
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $result;
	}
	
	public function getFiles($product_id)
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT product_files.id, product_files.name FROM product_files JOIN products_status ON product_files.product_id = products_status.id 
			WHERE products_status.status = 1 AND products_status.user = '.$this->_user['parent_id'].' AND product_id = '.$product_id;
		}
		else
		{
			$query = 'SELECT id, name FROM product_files WHERE product_id = '.$product_id;
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $result;
	}
	
	public function getDescription($product_id)
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT product_versions.description FROM products_status JOIN product_versions ON products_status.id = product_versions.product_id 
			WHERE products_status.status = 1 AND products_status.user = '.$this->_user['parent_id'].' AND product_id = '.$product_id;
		}
		else
		{
			$query = 'SELECT description FROM product_versions WHERE product_id = '.$product_id;
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		return mysql_fetch_array($result);
	}
	
	public function setStatus($product_id, $status)
	{
		$this->_db->query('INSERT INTO products_availability VALUES ('.$product_id.', '.$status.', \''.date('Y-m-d H:i:s').'\')') or $this->_db->raise_error();
	}
	
	public function clearStatuses()
	{
		$this->_db->query('DELETE FROM products_availability WHERE TIMESTAMPADD(HOUR,3,time) < current_timestamp') or $this->_db->raise_error();
	}
	
	public function getProductsSum($total = false)
	{
		if (true == $this->checkUserType() || $total == true)
		{
			$query = 'SELECT id FROM products WHERE subgroup_id > 0 AND producer_id > 0';
		}
		else
		{
			$query = 'SELECT products.id FROM products_status JOIN products ON products_status.id = products.id_hermes 
			WHERE user = '.$this->_user['parent_id'].' AND status = 1';
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return mysql_num_rows($result);
	}
	
	public function getAllProducts($comment, $producer_id)	
	{
		$query = 'SELECT products.id_hermes, product_versions.name FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id 
		WHERE producer_id = '.$producer_id.' AND products.comment LIKE \''.mysql_real_escape_string($comment).'%\' ORDER BY product_versions.name ASC';

		$result = $this->_db->query($query) or $this->_db->raise_error();
			
		if (mysql_num_rows($result))
		{
			return $this->_db->mysql_fetch_all($result);
		}
	}
	
	public function setIndividualSettings($range_id, $status, $value)
	{
		$query = 'SELECT user_id FROM groups_tree_user WHERE user_id = '.(int)$this->_user['parent_id'];
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		if (!mysql_num_rows($result))
		{
			$this->_db->query('INSERT INTO groups_tree_user VALUES ('.(int)$this->_user['parent_id'].', 1, 1)') or $this->_db->raise_error();
		}
		
		mysql_free_result($result);
	
		switch ($range_id)
		{
			case 1:
				$query = 'INSERT INTO products_status (`user`, `id`, `subgroup`, `producers`, `status`) SELECT '.(int)$this->_user['parent_id'].' as user, products.id_hermes, products.comment, 
				producers.name, '.$status.' as status FROM products, producers WHERE products.producer_id = producers.id AND products.comment LIKE \''.mysql_real_escape_string($value).'%\' ON 
				DUPLICATE KEY UPDATE status = '.$status;
				break;
				
			case 2:
				$query = 'INSERT INTO products_status (`user`, `id`, `subgroup`, `producers`, `status`) SELECT '.(int)$this->_user['parent_id'].' as user, products.id_hermes, products.comment, 
				producers.name, '.$status.' as status FROM products, producers WHERE products.producer_id = producers.id AND producers.id = \''.(int)$value.'\' ON DUPLICATE KEY UPDATE status = '.$status;
				break;
				
			case 3:
				$query = 'INSERT INTO products_status (`user`, `id`, `subgroup`, `producers`, `status`) SELECT '.(int)$this->_user['parent_id'].' as user, products.id_hermes, products.comment, 
				producers.name, '.$status.' as status FROM products, producers WHERE products.producer_id = producers.id AND products.id_hermes = \''.(int)$value.'\' ON DUPLICATE KEY UPDATE status = '.$status;
				break;
		}
		

		$this->_db->query($query) or $this->_db->raise_error();
		$result = $this->_db->query('SELECT id FROM individual_changes WHERE user_id = '.$this->_user['parent_id']) or $this->_db->raise_error();
			
		if (!mysql_num_rows($result))
		{
			$this->_db->query('INSERT INTO individual_changes VALUES (\'\', '.$this->_user['parent_id'].')') or $this->_db->raise_error();
		}
			
		return $query;
	}
	
	public function confirmIndividualSettings()
	{
		$this->_db->query('DELETE FROM individual_changes WHERE user_id = '.$this->_user['parent_id']) or $this->_db->raise_error();
		
		$query = 'UPDATE producers_status SET status = 1 WHERE name IN (SELECT DISTINCT(producers) FROM products_status WHERE status = 1) AND 
		user = '.$this->_user['parent_id'];
		$this->_db->query($query) or $this->_db->raise_error();
		$data .= $query."\n";
		
		$query = 'UPDATE groups_tree_status SET status = 1 WHERE comment IN (SELECT comment FROM groups_tree as mt WHERE (SELECT count(id) FROM 
		products_status WHERE subgroup LIKE CONCAT(mt.comment, \'%\'))>=1) AND user = '.$this->_user['parent_id'];
		$this->_db->query($query) or $this->_db->raise_error();
		$data .= $query."\n".CACHE_DIR."\n";
		
		if ($handle = opendir(CACHE_DIR)) 
		{	
			while (false !== ($file = readdir($handle))) 
			{ 
				if (!in_array($file, array('.', '..')))
				{
					$file_details = explode('.', $file);
					$data .= $file_details[0]."\n";
					if (substr($file_details[0], 0, 10) == "individual") 
					{ 
						unlink(CACHE_DIR.$file);
					}
				}
			}
				
			closedir($handle); 
		}
		
		return $data;
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}

$products = new Products();
?>
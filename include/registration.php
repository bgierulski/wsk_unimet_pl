<?php 
$page = $pages->getPage((int)$param1);
?>

<div id="page_content" style="padding:10px 0px 10px 0px;">
<?php echo str_replace(array('\"', "\'"), array('"', "'"), $page['content']); ?>
</div>

<div id="form">
	<?php if (isset($_SESSION["communicats"]['error'])) { ?>
	<div class="element its_error">
	<label style="width:250px;">&nbsp;</label>
	<span>
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
	</span>
	</div>
	<?php } else if (isset($_SESSION["communicats"]['ok'])) { ?>
	<div class="element its_ok">
	<label style="width:250x;">&nbsp;</label>
	<span>
	<?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?>
	</span>
	</div>
	<?php } ?>	
					
	<form action="" method="post" name="registration">
			
	<input type="hidden" name="operation" value="registration" />
	<input type="hidden" name="id_hermes" value="<?php echo $_SESSION["b2b_user"]['id_hermes']; ?>" />
	<input type="hidden" name="parent_id" value="<?php echo $_SESSION["b2b_user"]['parent_id']; ?>" />
	<input type="hidden" name="user_id" value="<?php echo (int)$user_id; ?>" />
		
	<?php if ($_SESSION["errors"]['name'] != '') { ?>
	<div class="element error">
	<label style="width:250px;">&nbsp;</label><?php echo $_SESSION["errors"]['name']; ?></div>
	<?php } ?>
	<div class="element">
	<label style="width:250px;">Imię i nazwisko <strong class="error">(*)</strong></label>
	<input name="name" value="<?php echo $forms->textControll($_SESSION["values"]['name']); ?>" type="text" />
	</div>
	
	<?php if ($_SESSION["errors"]['company'] != '') { ?>
	<div class="element error">
	<label style="width:250px;">&nbsp;</label><?php echo $_SESSION["errors"]['company']; ?></div>
	<?php } ?>
	<div class="element">
	<label style="width:250px;">Pełna nazwa firmy <strong class="error">(*)</strong></label>
	<input name="company" value="<?php echo $forms->textControll($_SESSION["values"]['company']); ?>" type="text" />
	</div>
				
	<?php if ($_SESSION["errors"]['street'] != '') { ?>
	<div class="element error">
	<label style="width:250px;">&nbsp;</label><?php echo $_SESSION["errors"]['street']; ?></div>
	<?php } ?>
	<?php 
	if (!isset($_SESSION["values"]['street'])) { $_SESSION["values"]['street'] = 'ul.'; }
	?>
	<div class="element">
	<label style="width:250px;">Ulica <strong class="error">(*)</strong></label>
	<input name="street" value="<?php echo $_SESSION["values"]['street']; ?>" type="text" />
	</div>
				
	<?php if ($_SESSION["errors"]['post_code'] != '') { ?>
	<div class="element error">
	<label style="width:250px;">&nbsp;</label><?php echo $_SESSION["errors"]['post_code']; ?></div>
	<?php } ?>
	<div class="element">
	<label style="width:250px;">Kod pocztowy <strong class="error">(*)</strong></label>
	<input name="post_code" value="<?php echo $_SESSION["values"]['post_code']; ?>" type="text" />
	</div>
				
	<?php if ($_SESSION["errors"]['city'] != '') { ?>
	<div class="element error">
	<label style="width:250px;">&nbsp;</label><?php echo $_SESSION["errors"]['city']; ?></div>
	<?php } ?>
	<div class="element">
	<label style="width:250px;">Miejscowość <strong class="error">(*)</strong></label>
	<input name="city" value="<?php echo $_SESSION["values"]['city']; ?>" type="text" />
	</div>
				
	<?php if ($_SESSION["errors"]['identificator'] != '') { ?>
	<div class="element error">
	<label style="width:250px;">&nbsp;</label><?php echo $_SESSION["errors"]['identificator']; ?></div>
	<?php } ?>
	<div class="element">
	<label style="width:250px;">NIP <strong class="error">(*)</strong></label>
	<input name="identificator" value="<?php echo $_SESSION["values"]['identificator']; ?>" type="text" />
	</div>
				
	<?php if ($_SESSION["errors"]['email'] != '') { ?>
	<div class="element error">
	<label style="width:250px;">&nbsp;</label><?php echo $_SESSION["errors"]['email']; ?></div>
	<?php } ?>
	<div class="element">
	<label style="width:250px;">E-mail <strong class="error">(*)</strong></label>
	<input name="email" value="<?php echo $_SESSION["values"]['email']; ?>" type="text" />
	</div>
					
	<?php if ($_SESSION["errors"]['phone'] != '') { ?>
	<div class="element error">
	<label style="width:250px;">&nbsp;</label><?php echo $_SESSION["errors"]['phone']; ?></div>
	<?php } ?>
	<div class="element">
	<label style="width:250px;">Telefon <strong class="error">(*)</strong></label>
	<input name="phone" value="<?php echo $_SESSION["values"]['phone']; ?>" type="text" />
	</div>
					
	<div class="element">
	<label style="width:250px;">Osoba do kontaktu</label>
	<input name="fax" value="<?php echo $_SESSION["values"]['contact']; ?>" type="text" />
	</div>
	
	<?php if ($_SESSION["errors"]['zgoda_id'] != '') { ?>
	<div class="element error">
	<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['zgoda_id']; ?></div>
	<?php } ?>
	<div class="element">
	<label style="width:250px;">Zgoda na przesyłanie ofery handlowej<strong class="error">(*)</strong></label>
	<select name="zgoda_id">
	<option value="0">Tak</option>
	<option value="tak" <?php if ($_SESSION["values"]['zgoda_id'] == 'Tak') echo 'selected="selected"'; ?>>Tak</option>
	<option value="nie" <?php if ($_SESSION["values"]['zgoda_id'] == 'Nie') echo 'selected="selected"'; ?>>Nie</option>
	</select>
	</div>
	
			
	<?php if ($_SESSION["errors"]['region_id'] != '') { ?>
	<div class="element error">
	<label style="width:250px;">&nbsp;</label><?php echo $_SESSION["errors"]['region_id']; ?></div>
	<?php } ?>
	<div class="element">
	<label style="width:250px;">Województwo <strong class="error">(*)</strong></label>
	<select name="region_id">
	<option value="0">-- wybierz --</option>
	<option value="dolnośląskie" <?php if ($_SESSION["values"]['region_id'] == 'dolnośląskie') echo 'selected="selected"'; ?>>dolnośląskie</option>
	<option value="kujawsko-pomorskie" <?php if ($_SESSION["values"]['region_id'] == 'kujawsko-pomorskie') echo 'selected="selected"'; ?>>kujawsko-pomorskie</option>
	<option value="lubelskie" <?php if ($_SESSION["values"]['region_id'] == 'lubelskie') echo 'selected="selected"'; ?>>lubelskie</option>
	<option value="lubuskie" <?php if ($_SESSION["values"]['region_id'] == 'lubuskie') echo 'selected="selected"'; ?>>lubuskie</option>
	<option value="łódzkie" <?php if ($_SESSION["values"]['region_id'] == 'łódzkie') echo 'selected="selected"'; ?>>łódzkie</option>
	<option value="mazowieckie" <?php if ($_SESSION["values"]['region_id'] == 'mazowieckie') echo 'selected="selected"'; ?>>mazowieckie</option>
	<option value="małopolskie" <?php if ($_SESSION["values"]['region_id'] == 'małopolskie') echo 'selected="selected"'; ?>>małopolskie</option>
	<option value="opolskie" <?php if ($_SESSION["values"]['region_id'] == 'opolskie') echo 'selected="selected"'; ?>>opolskie</option>
	<option value="podkarpackie" <?php if ($_SESSION["values"]['region_id'] == 'podkarpackie') echo 'selected="selected"'; ?>>podkarpackie</option>
	<option value="podlaskie" <?php if ($_SESSION["values"]['region_id'] == 'podlaskie') echo 'selected="selected"'; ?>>podlaskie</option>
	<option value="pomorskie" <?php if ($_SESSION["values"]['region_id'] == 'pomorskie') echo 'selected="selected"'; ?>>pomorskie</option>
	<option value="śląskie" <?php if ($_SESSION["values"]['region_id'] == 'śląskie') echo 'selected="selected"'; ?>>śląskie</option>
	<option value="świętokrzyskie" <?php if ($_SESSION["values"]['region_id'] == 'świętokrzyskie') echo 'selected="selected"'; ?>>świętokrzyskie</option>
	<option value="warminsko-mazurskie" <?php if ($_SESSION["values"]['region_id'] == 'warminsko-mazurskie') echo 'selected="selected"'; ?>>warminsko-mazurskie</option>
	<option value="wielkopolskie" <?php if ($_SESSION["values"]['region_id'] == 'wielkopolskie') echo 'selected="selected"'; ?>>wielkopolskie</option>
	<option value="zachodniopomorskie" <?php if ($_SESSION["values"]['region_id'] == 'zachodniopomorskie') echo 'selected="selected"'; ?>>zachodniopomorskie</option>
	</select>
	</div>
				
	<?php if ($_SESSION["errors"]['activity'] != '') { ?>
	<div class="element error">
	<label style="width:250px;">&nbsp;</label><?php echo $_SESSION["errors"]['activity']; ?></div>
	<?php } ?>
		
	<div class="element">
		<label style="width:250px;">Rodzaj działalności <strong class="error">(*)</strong></label>
		<div style="float:left;">
			<p><input name="activity" type="radio" value="sklep" style="width:auto;" <?php if ($_SESSION["values"]['activity'] == 'sklep') echo 'checked="checked"'; ?> /> sklep</p>
			<p><input name="activity" type="radio" value="hurtownia" style="width:auto;" <?php if ($_SESSION["values"]['activity'] == 'hurtownia') echo 'checked="checked"'; ?> /> hurtownia</p>
			<p><input name="activity" type="radio" value="zakład" style="width:auto;" <?php if ($_SESSION["values"]['activity'] == 'zakład') echo 'checked="checked"'; ?> /> zakład</p>
			<p><input name="activity" type="radio" value="inwestor" style="width:auto;" <?php if ($_SESSION["values"]['activity'] == 'inwestor') echo 'checked="checked"'; ?> /> inwestor</p>
			<p><input name="activity" type="radio" value="inny" style="width:auto;" <?php if ($_SESSION["values"]['activity'] == 'inny') echo 'checked="checked"'; ?> /> inny <input type="text" name="different_activity" value="<?php echo $_SESSION["values"]['different_activity']; ?>" style="width:255px;" /></p>
		</div>
		<div class="clear"></div>
	</div>
				
	<div class="element">
		<label style="width:250px;">Uwagi</label>
		<textarea name="comment" rows="0" cols="0" style="width:300px;"><?php echo $_SESSION["values"]['comment']; ?></textarea>
	</div>
	
	<div class="element">
		<label style="width:250px;">&nbsp;</label>
		<div class="button" style="padding-right:10px;">
		<a href="./" onclick="javascript:document.registration.submit(); return false;">WYŚLIJ WIADOMOŚĆ</a>
		</div>
		<div class="button" style="padding-right:10px;">
		<a href="./<?php echo $param1; ?>.html">WYCZYŚĆ FORMULARZ</a>
		</div>
		<div class="clear"></div>
	</div>
					
</form>
</div>
<?php
class Cashe
{
	var $connection;
	var $result;
	var $rows;
	 
	var $queries = 0;
	 
	var $cache_state =0;
	var $cache_file;
	var $cache_buffer;
	var $cache_ptr;
	var $cron = false;
	 
	public function sql_connect($host, $user, $pass, $db)
	{
		$this -> connection = mysql_connect($host, $user, $pass);
		mysql_select_db($db);
	}
	
	public function sql_cache($handle = 0)
	{
		if(is_string($handle))
		{
			if(file_exists(CACHE_DIR.$handle.'.xxx') && true !== $this->cron)
			{
				$this -> cache_state   = 1;
				$this -> cache_ptr      = 0;
				$this -> cache_buffer = unserialize(file_get_contents(CACHE_DIR.$handle.'.xxx'));
			}
			else
			{
				$this -> cache_state = 2;
				$this -> cache_buffer = array();
				$this -> cache_file = CACHE_DIR.$handle.'.xxx';
			}      
		}
		else
		{
		 	if($this -> cache_state == 2)
		 	{
				file_put_contents($this -> cache_file, serialize($this -> cache_buffer));
			}
			$this -> cache_state = 0;
		}
	}
	
	public function sql_cache_remove($handle)
	{
		if(file_exists(CACHE_DIR.$handle.'.xxx'))
		{
			unlink(CACHE_DIR.$handle.'.xxx');
		}
	}
	
	public function sql_query($query)
	{
		if($this->cache_state != 1)
		{
			$this->result = mysql_query($query);
			$this->queries++;
		            
			if(mysql_errno() != 0)
			{
				die('Error: '.mysql_error().'<br/>');
			}
			return 1;
		}
	}
	
	public function sql_fetch_array()
	{
		if($this -> cache_state == 1)
		{
			if(!isset($this -> cache_buffer[$this -> cache_ptr]))
			{
				return 0;
			}
			$this -> rows = $this -> cache_buffer[$this -> cache_ptr];
			$this -> cache_ptr++;
			return 1;
		}
		else
		{
			if($this -> rows = mysql_fetch_assoc($this -> result))
			{
				if($this -> cache_state == 2)
				{
					// Dodaj do cache
					$this -> cache_buffer[] = $this -> rows;
				}
				return 1;
			}
		}
		return 0;
	}
	
	public function sql_fetch_row()
	{
		if($this -> cache_state == 1)
		{
			// czy koniec bufora?
			if(!isset($this -> cache_buffer[$this -> cache_ptr]))
			{
				return 0;
			}
			// odczytaj z bufora
			$this -> rows = $this -> cache_buffer[$this -> cache_ptr];
			$this -> cache_ptr++;
			return 1;
		}
		else
		{
			if($this -> rows = mysql_fetch_row($this -> result))
			{
				if($this -> cache_state == 2)
				{
					// Jeśli tworzymy cache, musimy rekord dodatkowo zapisac w buforze
					$this -> cache_buffer[] = $this -> rows;
				}
				return 1;
			}
		}
		return 0;
	}
	 
	public function sql_close()
	{
		mysql_close($this->connection);
	}
}
?>
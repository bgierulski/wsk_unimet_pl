<?php
class Validators
{	
	public function formatId($id, $query_id = 0)
	{
		if ($id <= 0)
		{
			return $id;
		}
	
		$string = (string)$id;
		$prefix = (int)substr($string, 0, 4);
		
		return ($id - ($prefix * 1000000)).'/'.substr($prefix, 2, 3).(($query_id > 0) ? '/Z' : '');
	}

	protected function setSession($name, $value)
	{
		$_SESSION[$name] = $value;
	}
	
	protected function getSession($name)
	{
		return $_SESSION[$name];
	}
	
	public function textControll($value)
	{
		return str_replace(array('\"', '"', "\'"), array('&#34;', '&#34;', "'"), $value);
	}
	
	public function textClear($value)
	{
		return str_replace(array('\"', '&#34;', "\'"), array('"', '"', "'"), $value);
	}
	
	protected function validateHex($value)
	{
		return (!preg_match('/^#[0-9a-fA-F]{6}$/D', $value)) ? 0 : 1;
	}
	
	public function validateTextarea($value)
	{
		$signs = array('&nbsp;', '<br />', '<p>', '</p>');
		return (trim(str_replace($signs, '', nl2br($value))) == '') ? 0 : 1;
	}
	
	protected function checkImage($image)
	{
		return ($image['name'] == '' || ($image['name'] != '' && $image['type'] != 'image/gif' && $image['type'] != 'image/png' && 
		$image['type'] != 'image/x-png' && $image['type'] != 'image/jpeg' && $image['type'] != 'image/pjpeg')) ? 0 : 1;
	}
	
	public function validateEmail($value)
	{
		return (!preg_match('/^[a-zA-Z0-9\.\-\_]+\@[a-zA-Z0-9\.\-\_]+\.[a-z]{2,4}$/D', $value)) ? 0 : 1;
	}
	
	public function validateName($string)
	{
		$search_array = array('ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź', 'Ą', 'Ć', 'Ę', 'Ł', 'Ń', 'Ó', 'Ś', 'Ż', 'Ź', ' ', ',', '?');
		$replace_array = array('a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z', 'a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z', '_', '_', '_');
		return strtolower(str_replace($search_array, $replace_array, $string));
	}
	
	public function validatePrice($string)
	{
		return (!preg_match('/^[0-9,\.]*$/i', $string)) ? 0 : 1;
	}
	
	public function validateDate($month, $day, $year)
	{
		return ((@checkdate($month, $day, $year)) ? 1 : 0);
	}
	
	public function randomString($length)
	{
	  	$sings = '1234567890qwertyuiopasdfghjkklzxcvbnm';
		$string = '';
 		
	 	for ($i=0; $i<$length; $i++)
		{
			$string .= $sings[rand()%(strlen($sings))];
		}
		
		return $string;
	}
	
	public function clear()
	{
		$_SESSION['errors'] = null;
		unset($_SESSION['errors']);
		$_SESSION['values'] = null;
		unset($_SESSION['values']);
	}
}

$validators = new Validators();
?>
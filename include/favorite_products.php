<?php 
error_reporting(E_ALL ^ E_NOTICE);
session_start();

include('config.php');
include(BASE_DIR.'class/class.DataBase.php');
include(BASE_DIR.'class/class.Validators.php');
include(BASE_DIR.'class/class.Groups.php');
include(BASE_DIR.'class/class.Products.php');
include(BASE_DIR.'class/class.Prices.php');
include(BASE_DIR.'class/class.Users.php');
include(BASE_DIR.'class/class.Pages.php');
include(BASE_DIR.'class/class.Producers.php');
include(BASE_DIR.'class/class.ContactForm.php');
include(BASE_DIR.'class/class.Comments.php');
include(BASE_DIR.'class/class.Basket.php');
include(BASE_DIR.'class/class.Orders.php');
include(BASE_DIR.'class/class.SpecialOrders.php');
include(BASE_DIR.'class/class.Templates.php');
include(BASE_DIR.'class/class.Documents.php');

include(BASE_DIR.'basket_operations.php');

$db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<base href="<?php echo BASE_ADDRESS; ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="reply-to" content="Adres_e-mail" />
<meta name="author" content="Autor_dokumentu" />
<meta name="description" content="Opis" />
<link rel="stylesheet" href="css/b2b.css" type="text/css" />
<link rel="stylesheet" href="css/subpage.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.js"></script>
<script type="text/javascript" src="js/jquery.marquee.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.scrollTo.js"></script>

<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/operations.js"></script>
<script type="text/javascript" src="js/ajax.js"></script>

<link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.1.css" media="screen" />
<script type="text/javascript" src="js/fancybox/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.1.js"></script>

<!--[if lte IE 7]>
<style type="text/css">
.wrapper {position:relative;}
.cell {position:absolute; top:50%;}
.hack {position:relative; top:-50%;}
</style>
<![endif]-->

<title>B2B</title>
</head>

<body>

<?php 
$favorite_products = $products->getFavoriteProducts();
?>

<div id="product_content">

	<div id="product_menu">
		<p id="favorite_box" class="active">PRODUKTY ULUBIONE</p>
		<div class="clear"></div>
	</div>
	
	<div id="product_border">
		<div id="subpage_content">
		
		<?php 
		if (mysql_num_rows($favorite_products))
		{
			?>
			<input type="hidden" name="last_operation" value="<?php if (isset($_POST['operation'])) { echo './'; } ?>" />
			
			<form method="post" action="" name="to_basket">
			<input type="hidden" name="operation" value="to_basket" />
			
			<table cellspacing="0" cellpadding="0" class="product_table">
			<tr>
				<th style="border-bottom:1px solid #ebebeb; border-top:1px solid #ebebeb; padding:3px 0px 3px 0px;">&nbsp;</th>
				<th style="text-align:left; border-bottom:1px solid #ebebeb; border-top:1px solid #ebebeb; padding:3px 0px 3px 0px;">Nazwa produktu</th>
				<th style="text-align:left; border-bottom:1px solid #ebebeb; border-top:1px solid #ebebeb; padding:3px 0px 3px 0px;">Indeks</th>
				<th style="text-align:left; border-bottom:1px solid #ebebeb; border-top:1px solid #ebebeb; padding:3px 0px 3px 0px;">Cena</th>
				<th style="text-align:left; border-bottom:1px solid #ebebeb; border-top:1px solid #ebebeb; padding:3px 0px 3px 0px;">Ilość</th>
			</tr>
			<?php 
			while($row = mysql_fetch_array($favorite_products)) 
			{
				?>
				<tr>
				<td class="image_row">
					<span class="image"><a rel="gallery" href="<?php echo BASE_ADDRESS; ?>/include/image.php?id=<?php echo (int)$row['id_hermes']; ?>&w=500&h=500">
					<img src="<?php echo BASE_ADDRESS; ?>/include/image.php?id=<?php echo (int)$row['id_hermes']; ?>&w=50&h=50" alt="" />
					</a></span>
				</td>
				<td>
					<a href="#" onClick="javascript:pop_up(<?php echo $row['id_hermes']; ?>); return false;"><?php echo $row['name']; ?></a>
				</td>
				<td><?php echo $row['symbol']; ?></td>
				<td><?php echo $prices->getPrice($row['id_hermes'], 4); ?> zł netto</td>
				<td>
					<div>
					<span><input style="width:40px; text-align:right;" class="product_count" type="text" id="count_<?php echo $row['id_hermes']; ?>" name="counts[<?php echo $row['id_hermes']; ?>]" value="1" /></span> 
					<span><input class="product_checkbox" type="checkbox" id="active_<?php echo $row['id_hermes']; ?>" name="actives[]" value="<?php echo $row['id_hermes']; ?>" /></span>
					</div>
				</td>
				</tr>
				<?php
			}
			?>
			</table>

			<div class="action"> 
				<div style="padding:0; margin:0; margin-top:5px;">
					<p class="button" style="padding:0px 0px 0px 10px">
					<a href="./" id="select_all_favorites">ZAZNACZ WSZYSTKIE</a>
					</p>
					<p class="button" style="padding:0px 0px 0px 10px">
					<a href="./" id="delete_favorite_form_submit" id="delete_favorites">USUŃ PRODUKTY</a>
					</p>
					<p class="button" style="padding:0px 0px 0px 10px">
					<a href="./" id="to_basket_favorite_form_submit" id="add_to_basket_favorites">DODAJ DO ZAMÓWIENIA</a>
					</p>
				</div>
				<div class="clear"></div>
			</div>
			</form>
			<?php
		}
		else
		{
			?>Brak produktów<?php
		} 
		?>
		
		</div>
	</div>

</div>

</body>
</html>
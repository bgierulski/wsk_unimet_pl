<?php
class Comments extends Validators
{
	private $_db;
	private $_errors;
	
	private $_product_id;
	private $_ip;
	private $_nick;
	private $_comment;
	private $_www;
	
	public function __construct()
	{	
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		
		if ($_POST['operation'] == 'add_comment')
		{
			$this->_commentValidation();
		}
	}
	
	public function deleteComment($comment_id)
	{
		$result = $this->_db->query('SELECT id FROM product_comments WHERE id = '.$comment_id) 
		or $this->_db->raise_error();
		
		$this->_db->query('DELETE FROM product_comments WHERE id = '.$comment_id) 
		or $this->_db->raise_error();
		
		if ($this->_db->num_rows($result))
		{
			unset($_SESSION["communicats"]['error']);
			$comunicats['ok'] = 'komentarz został usunięty';
			$this->setSession("communicats", $comunicats);
		}
	}
	
	public function getComments($product_id)
	{
		$result = $this->_db->query('SELECT nick, comment, www FROM product_comments WHERE product_id = '.$product_id.' ORDER BY date DESC') or $this->_db->raise_error();
		
		return $result;
	}
	
	private function _commentValidation()
	{
		$this->clear();
		
		$this->_product_id = $values['product_id'] = (int)$_POST['product_id'];
		$this->_ip = $values['ip'] = trim(mysql_real_escape_string($_POST['ip']));
		$this->_nick = $values['nick'] = trim(mysql_real_escape_string(strip_tags($_POST['nick'])));
		$this->_comment = $values['comment'] = trim(mysql_real_escape_string(strip_tags($_POST['comment'])));
		$this->_www = $values['www'] = trim(mysql_real_escape_string(strip_tags($_POST['www'])));
				
		if ($this->_nick == '')
		{
			$this->_errors++;
			$errors['nick'] = 'Imię lub nick jest wymagany.';
		}
		
		if (str_replace('<br />', '', nl2br($this->_comment)) == '')
		{
			$this->_errors++;
			$errors['comment'] = 'Treść komentarza jest wymagana.';
		}
		
		if ((!empty($this->_ww) && $this->_ww != 'http://') && !$this->validateWWW($this->_www))
		{
			$this->_errors++;
			$errors['link'] = 'Przykład poprapwnego adresu: <strong>http://www.unimet.pl</strong>';
		}
		
		$this->setSession("values", $values);
		$this->setSession("errors", $errors);
	
		if (!$this->_errors)
		{
			$this->_addComment();
		}
	}
	
	private function _addComment()
	{		
		$this->clear();

		$this->_db->query('INSERT INTO product_comments VALUES (\'\', '.$this->_product_id.', \''.$this->_nick.'\', \''.$this->_comment.'\', 
		\''.$this->_www.'\', \''.date('Y-m-d').'\', \''.$this->_ip.'\')') or $this->_db->raise_error();
		
		$comment_id = $this->_db->insert_id();
		
		$this->_sendMessage($comment_id);
	}

	private function _sendMessage($comment_id)
	{
		$result = $this->_db->query('SELECT product_versions.name, subgroup_id, groups_tree.name as subgroup_name FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id 
		JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE id_hermes = '.$this->_product_id) or $this->_db->raise_error();
		
		$row = mysql_fetch_array($result);
		
		$message = '
		<p>Dodano komentarz do produktu <strong>'.$row['name'].'</strong> o następującej treści:</p>
		<p></p>
		<p>'.$this->_comment.'</p>
		<p></p>
		<p>Jeśli chcesz usunąć komentarz klinknij na link :  <a href="'.BASE_ADDRESS.'grupa_'.$this->validateName($row['subgroup_name'], 2).','.$row['subgroup_id'].',1,'.$comment_id.'.html">usuń komentarz</a></p>';
		
		$mimemail = new nomad_mimemail(); 
		
		$from		= "newsletter@unimet.pl";	  
		$html		= '<html><body>'.iconv("UTF-8", "ISO-8859-2", $message).'</body></html>';
		$subject    = 'www.b2b.unimet.pl - dodadano komentarz do produktu';
		
		$this->_db->free_result($result);
		$result = $this->_db->query('SELECT email FROM users WHERE email != \'\' AND id_hermes = 0 AND status = 0') 
		or $this->_db->raise_error();
		

		
		while($row = mysql_fetch_array($result)) 
   		{
   			if ($this->validateEmail($row['email']))
	   		{
		       	$to	= $row['email'];	
				
				$mimemail->set_smtp_log(true); 
				$smtp_host	= "swarog.az.pl";
				$smtp_user	= "newsletter@unimet.pl";
				$smtp_pass	= "1hadesa2madmax";
				$mimemail->set_smtp_host($smtp_host);
				$mimemail->set_smtp_auth($smtp_user, $smtp_pass); 
				$mimemail->new_mail($from, $to, iconv("UTF-8", "ISO-8859-2", $subject), "", $html);
				$mimemail->send(); 	
	   		}
   		}
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}

$comments = new Comments();
?>
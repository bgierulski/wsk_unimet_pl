<?php
class Orders extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_user = array();
	private $_products = array();
	
	private $_address;
	private $_message;
	private $_payment;
	private $_document;
	private $_internal;
	
	private $_product = array();
	private $_basket_products = array();
	private $_counter; // products in counter
	
	private $_children = array();
	private $_parents = array();
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		
		$this->_user = $this->getSession('b2b_user');
		$this->_products = $this->getSession("basket_products");
		
		if ($_POST['operation'] == 'send_order')
		{	
			$this->_validateOrder();
		}
	}

	public function getOrderYears()
	{
		$query = "SELECT DISTINCT DATE_FORMAT(`add_date`,'%Y') `year` FROM `orders` ORDER BY `add_date` DESC";

		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getBudgets()
	{
		$result = $this->_db->query('SELECT id, department FROM wsk_budget ORDER BY department ASC') or $this->_db->raise_error();
		return $result;
	}
	
	public function getBudgetDetails($budget_id)
	{
		$query = 'SELECT wsk_budget_history.id, wsk_budget_history.year, users.user_name, users.user_surname, old_value, new_value, wsk_budget_history.amount, wsk_budget_history.info, wsk_budget_history.date FROM wsk_budget JOIN wsk_budget_history ON wsk_budget.department = wsk_budget_history.department JOIN users ON wsk_budget_history.user = users.id WHERE wsk_budget.id = '.$budget_id.' ORDER BY wsk_budget_history.date DESC';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getOrderStatus($order_id)
	{
		$result = $this->_db->query('SELECT status FROM orders WHERE id = '.$order_id) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			return $row['status'];
		}
	}
	
	public function checkUnit($order_id, $symbol)
	{
		$query = 'SELECT order_details.unit, products.unit as default_unit FROM orders JOIN order_details ON orders.id = order_details.order_id LEFT JOIN products ON order_details.product_id = 
		products.id_hermes WHERE orders.id = '.$order_id.' AND order_details.symbol = \''.trim(mysql_real_escape_string($symbol)).'\'';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			return $row;
		}
	}
	
	public function getCommunicats($order_id)
	{
		$result = $this->_db->query('SELECT `communicat`, `time` FROM order_communicats WHERE order_id = '.$order_id.' ORDER BY `time` ASC') or $this->_db->raise_error();
		return $result;
	}
	
	public function getUsersNames()
	{
		$result = $this->_db->query('SELECT login, user_name, user_surname FROM users WHERE parent_id = 8338 OR status = 0') or $this->_db->raise_error();
		$data = array();
		
		if (mysql_num_rows($result))
		{
			while($row = mysql_fetch_array($result)) 
	   		{
	       			$data[$row['login']] = $row['user_name'].' '.$row['user_surname'];
	   		}
		}
		
		return $data;
	}
	
	public function getTraderComment($id)
	{
		$result = $this->_db->query('SELECT id, details FROM order_details WHERE id = '.$id) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			return $row;
		}
	}
	
	public function checkOrdersToReception()
	{
		$query = 'SELECT orders.id, orders.status, IF (order_informations.delivery_date IS NULL OR order_informations.delivery_date = \'1970-01-01\', 0, 
		DATEDIFF(current_date, order_informations.delivery_date)) as delivery_diff, wsk_mpk.contractor, wsk_mpk.receiving, wsk_mpk.receiving_deputy 
		FROM orders JOIN order_details ON orders.id = order_details.order_id JOIN users ON orders.user_id = users.id LEFT JOIN order_informations ON 
		orders.id = order_informations.order_id LEFT JOIN products ON order_details.product_id = products.id_hermes LEFT JOIN groups_tree ON 
		products.subgroup_id = groups_tree.id LEFT JOIN users_delivery ON users.id = users_delivery.user_id LEFT JOIN wsk_mpk ON orders.id = wsk_mpk.order_id 
		LEFT JOIN wsk_budget ON wsk_mpk.budget = wsk_budget.id AND wsk_budget.start <= \''.date('Y-m-d').'\' AND wsk_budget.end >= \''.date('Y-m-d').'\' 
		WHERE IF (order_informations.delivery_date IS NULL OR order_informations.delivery_date = \'1970-01-01\', 0, 
		DATEDIFF(current_date, order_informations.delivery_date)) >= 0 AND users.parent_id = 8338 AND orders.status IN (3) AND '.$this->_user['id'].' IN (wsk_mpk.receiving, wsk_mpk.receiving_deputy) AND '.$this->_user['id'].' > 0 
		ORDER BY orders.id ASC';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return ((mysql_num_rows($result)) ? true : false);
	}


	public function getLimits()
	{
		$result = $this->_db->query('SELECT `days`, order_limit FROM b2b_details WHERE id = 1') or $this->_db->raise_error();
		$row = mysql_fetch_array($result);
		return $row;
	}
	
	public function changeOrderAmount($id, $order_id, $amount)
	{
		$query = 'SELECT wsk_mpk.contractor, wsk_mpk.analyst, wsk_mpk.analyst_deputy, wsk_mpk.manager, wsk_mpk.manager_deputy, orders.status, order_details.amount, 
		order_details.netto_price, wsk_budget.id as budget_id, wsk_budget.budget, wsk_budget.department FROM order_details JOIN orders ON order_details.order_id = orders.id JOIN 
		wsk_mpk ON orders.id = wsk_mpk.order_id JOIN wsk_budget ON wsk_mpk.budget = wsk_budget.id WHERE orders.id = '.$order_id.' AND 
		order_details.id = '.$id.' AND orders.status IN (6,7,9,10)';

		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		if (mysql_num_rows($result) && (float)$amount > 0)
		{
			$order = mysql_fetch_array($result);
			mysql_free_result($result);
			
			if ((int)$this->_user['id'] > 0 && ($this->_user['admin'] == 1 ||  ($order['status'] == 9 && in_array((int)$this->_user['id'], 
			array($order['contractor']))) || ($order['status'] == 6 && in_array((int)$this->_user['id'], 
			array($order['analyst'], $order['analyst_deputy']))) || ($order['status'] == 7 && in_array((int)$this->_user['id'], array($order['manager'], 
			$order['manager_deputy'])))))
			{
				$data[0] = ((($order['budget'] + ($order['netto_price'] * ($order['amount'] - $amount))) >= 0) ? 1 : 0); 
				
				$query = 'UPDATE order_details SET amount = \''.$amount.'\' WHERE id = '.$id;
				$this->_db->query($query) or $this->_db->raise_error();
				
				$query = 'SELECT id, netto_price, brutto_price, amount, tax FROM order_details WHERE order_id = '.$order_id;
				$result = $this->_db->query($query) or $this->_db->raise_error();
				
				$netto_value = 0;
				$brutto_value = 0;
				
				while($row = mysql_fetch_array($result)) 
		   		{
		   			$tax = ((int)$row['tax'] * $row['netto_price'])/100;
		   		
		   			if ($row['id'] == $id)
		   			{
		   				$data[1] = ($row['netto_price'] * $row['amount']); 
		   				$data[2] = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']); 
		   			}
		   			
		       			$netto_value += ($row['netto_price'] * $row['amount']); 
		       			$brutto_value += sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']); 
		   		}
		   		
		   		$data[3] = $netto_value; 
		   		
		   		$query = 'UPDATE orders SET netto_value = \''.$netto_value.'\', brutto_value = \''.$brutto_value.'\' WHERE id = '.$order_id;
		   		$this->_db->query($query) or $this->_db->raise_error();
		   		
		   		$query = 'UPDATE wsk_mpk SET price_netto = \''.$netto_value.'\' WHERE order_id = '.$order_id;
		   		$this->_db->query($query) or $this->_db->raise_error();
		   		
		   		return $data;
			}
		}
	}
	
	public function checkMPK()
	{
		if (is_array($this->_products) && !empty($this->_products))
		{
			$counter = count($this->_products);
			
			foreach ($this->_products as $product)
			{
				if (!empty($product['mpk']))
				{
					$counter--;
				}
			}
		}
		
		if ($counter > 0)
		{
			$communicats['ok'] = 'Numer MPK jest wymagany.';
			$this->setSession("communicats", $communicats);
			header('Location: ./koszyk.html');
		}
	}
	
	public function getParents($user_id = 0)
	{
		$user_id = ((!$user_id) ? $this->_user['id'] : $user_id);
		
		$result = $this->_db->query('SELECT parent_id FROM user_tree WHERE user_id = '.$user_id) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			
			array_push($this->_parents, $row['parent_id']);
			
			$this->getParents($row['parent_id']);
		}
	}
	
	public function getStatus($id)
	{
		$query = "SELECT status FROM orders WHERE id = ".$id;
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_assoc($result);
			return $row['status'];
		}
		
		return 0;
	}
	
	public function getOrders($type, $all = false, $year = null)
	{			
		$this->_children = array();
		$this->_getChildren(array($this->_user['id']));
		
		$query = "SELECT id FROM users WHERE parent_id = 8338";
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		$typpp = $type;
		
		switch ($type)
		{
			case 1: $types = array(6); break;
			case 2: $types = array(7); break;
			case 3: $types = array(1,2); break;
			case 4: $types = array(3); break;
			case 5: $types = array(4); break;
			case 6: $types = array(5); break;
			case 7: $types = array(9); break;
			case 8: $types = array(10); break;
			case 9: $types = array(8); break;
			default: $types = array(6); break;
		}
		
		$query = 'SELECT orders.id, orders.status, number, orders.status, order_details.query_id, order_details.symbol, order_details.unit, products.unit as default_unit, 
		order_details.details, orders.comment as order_comment, 
		wsk_budget.department, wsk_budget.budget as budget_value, 
		wsk_budget_2012.department as department_2012, 0 as budget_value_2012, 
		wsk_budget_2013.department as department_2013, 0 as budget_value_2013, 
		wsk_budget_2014.department as department_2014, wsk_budget_2014.budget as budget_value_2014, 
		wsk_budget_2015.department as department_2015, wsk_budget_2015.budget as budget_value_2015, 
		wsk_budget_2016.department as department_2016, wsk_budget_2016.budget as budget_value_2016, 		
		groups_tree.name as subgroup_name, order_comments.information as merchant_information, 
		pallet, users.transport, users.name as user_name, users.name, users.post_code, users.city, users.street, users.home, users.flat, users_delivery.post_code 
		as delivery_post_code, users_delivery.city as delivery_city, users_delivery.street as delivery_street, users_delivery.home as delivery_home, 
		users_delivery.flat as delivery_flat, users.identificator, orders.id, orders.add_date, orders.update_date, orders.payment, orders.brutto_value, order_details.id as detail_id, order_details.name, 
		order_details.tax, order_details.amount, order_details.producer as producer_name, order_details.netto_price, order_details.brutto_price, document, orders.comment, wsk_mpk.mpk, 
		wsk_mpk.contractor, wsk_mpk.contractor_email, wsk_mpk.contractor_login, wsk_mpk.contractor_date, wsk_mpk.analyst, wsk_mpk.analyst_email, wsk_mpk.analyst_login, 
		wsk_mpk.analyst_date, wsk_mpk.analyst_deputy, wsk_mpk.administrator, wsk_mpk.administrator_email, wsk_mpk.administrator_login, wsk_mpk.administrator_date, wsk_mpk.manager, wsk_mpk.manager_email, wsk_mpk.manager_login, wsk_mpk.manager_date, wsk_mpk.manager_deputy, wsk_mpk.receiving, 
		wsk_mpk.receiving_email, wsk_mpk.receiving_login, wsk_mpk.receiving_date, wsk_mpk.receiving_deputy, wsk_mpk.analyst_login, wsk_mpk.manager_login, wsk_mpk.receiving_login, 
		wsk_mpk.unimet_representative, orders.netto_value, DATEDIFF(CURRENT_DATE, IF(orders.update_date = \'0000-00-00\', orders.add_date, orders.update_date)) as date_diff, order_informations.wz_document, order_informations.delivery_date, order_informations.fv, order_informations.wm, (SELECT email FROM users WHERE login = 
		wsk_mpk.unimet_representative AND status = 0) as unimet_email, order_replacements.id as replacement_element, IFNULL(order_replacements.product_id, 0) 
		as replacement_id, order_replacements.name as replacement_name, order_replacements.symbol as replacement_symbol, order_informations.reject_comment FROM orders JOIN order_details ON orders.id = order_details.order_id JOIN users ON 
		orders.user_id = users.id LEFT JOIN order_informations ON orders.id = order_informations.order_id LEFT JOIN order_comments ON orders.id = order_comments.order_id LEFT JOIN products ON order_details.product_id = 
		products.id_hermes LEFT JOIN groups_tree ON products.subgroup_id = groups_tree.id LEFT JOIN users_delivery ON users.id = users_delivery.user_id LEFT 
		JOIN wsk_mpk ON orders.id = wsk_mpk.order_id 
		LEFT JOIN wsk_budget ON wsk_mpk.budget = wsk_budget.id AND wsk_budget.start <= \''.date('Y-m-d').'\' AND wsk_budget.end >= \''.date('Y-m-d').'\' 
		LEFT JOIN wsk_budget_2012 ON wsk_mpk.budget = wsk_budget_2012.id AND wsk_budget_2012.start <= orders.add_date AND wsk_budget_2012.end >= orders.add_date 
		LEFT JOIN wsk_budget_2013 ON wsk_mpk.budget = wsk_budget_2013.id AND wsk_budget_2013.start <= orders.add_date AND wsk_budget_2013.end >= orders.add_date  
		LEFT JOIN wsk_budget_2014 ON wsk_mpk.budget = wsk_budget_2014.id AND wsk_budget_2014.start <= orders.add_date AND wsk_budget_2014.end >= orders.add_date  
		LEFT JOIN wsk_budget_2015 ON wsk_mpk.budget = wsk_budget_2015.id AND wsk_budget_2015.start <= orders.add_date AND wsk_budget_2015.end >= orders.add_date 
		LEFT JOIN wsk_budget_2016 ON wsk_mpk.budget = wsk_budget_2016.id AND wsk_budget_2016.start <= orders.add_date AND wsk_budget_2016.end >= orders.add_date  
		LEFT JOIN order_replacements ON order_details.id 
		= order_replacements.element_id WHERE ';

		if (!is_null($year))
		{
			$query .= 'orders.add_date >= \'' . $year . '-01-01\' AND orders.add_date < \'' . ($year + 1) . '-01-01\' AND ';
		}
		
		if (false == $all)
		{
			$query .= 'orders.status IN (';
			
			foreach ($types as $type)
			{
				$query .= $type.',';
			}
			
			$query = substr($query, 0, -1).') AND ';
		}
		
		$query .= 'orders.user_id IN (';
		
		while($row = mysql_fetch_array($result)) 
   		{
       		$query .= $row['id'].',';
   		}
		
		$query = substr($query, 0, -1).') ';
		
		$query .= 'ORDER BY orders.status ASC, orders.id DESC';

		

		$actual_time = microtime(true);
		
		$result = $this->_db->query($query) or $this->_db->raise_error();

		$this->_db->query("INSERT INTO `sql` (`sql`, `time`) VALUES ('".mysql_real_escape_string($query)."', '".(microtime(true)-$actual_time)."')") or $this->_db->raise_error();


		//echo $query;
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getOrder($order_id)
	{		
		$result = $this->_db->query('SELECT users.parent_id, users.merchant_id, order_details.query_id, order_details.unit, products.unit as default_unit, order_details.info as product_info, groups_tree.name as subgroup_name, 
		users.id_hermes, orders.id, number, orders.status, orders.comment as order_comment, wsk_budget.department, wsk_budget.budget as budget_value, pallet, 
		users.id as user_id, users.transport, users.name as user_name, users.name, users.post_code, users.city, users.street, users.home, users.flat, users_delivery.post_code 
		as delivery_post_code, users_delivery.city as delivery_city, users_delivery.street as delivery_street, users_delivery.home as delivery_home, 
		users_delivery.flat as delivery_flat, users.identificator, orders.add_date, orders.payment, orders.brutto_value, order_details.id as detail_id, order_details.name, 
		order_details.symbol, order_details.tax, order_details.description, order_details.amount, order_details.producer as producer_name, order_details.name as product_name, order_details.netto_price, order_details.brutto_price, document, 
		orders.comment, wsk_mpk.mpk, wsk_mpk.contractor, wsk_mpk.contractor_email, wsk_mpk.contractor_login, wsk_mpk.contractor_date, wsk_mpk.analyst, wsk_mpk.analyst_email, wsk_mpk.analyst_login, 
		wsk_mpk.analyst_date, wsk_mpk.analyst_deputy, wsk_mpk.administrator, wsk_mpk.administrator_email, wsk_mpk.administrator_login, wsk_mpk.administrator_date, wsk_mpk.manager, wsk_mpk.manager_email, wsk_mpk.manager_login, wsk_mpk.manager_date, wsk_mpk.manager_deputy, wsk_mpk.receiving, 
		wsk_mpk.receiving_email, wsk_mpk.receiving_login, wsk_mpk.receiving_date, wsk_mpk.receiving_deputy, wsk_mpk.analyst_login, wsk_mpk.manager_login, wsk_mpk.receiving_login, 
		wsk_mpk.unimet_representative, orders.netto_value, DATEDIFF(CURRENT_DATE, IF(orders.update_date = \'0000-00-00\', orders.add_date, orders.update_date)) as date_diff, order_informations.delivery_date, order_informations.fv, order_informations.wm, order_informations.reject_comment, (SELECT email FROM users WHERE login = 
		wsk_mpk.unimet_representative AND status = 0) as unimet_email FROM orders JOIN order_details ON orders.id = order_details.order_id JOIN users ON 
		orders.user_id = users.id LEFT JOIN order_informations ON orders.id = order_informations.order_id LEFT JOIN products ON order_details.product_id = 
		products.id_hermes LEFT JOIN groups_tree ON products.subgroup_id = groups_tree.id LEFT JOIN users_delivery ON users.id = users_delivery.user_id 
		LEFT JOIN wsk_mpk ON orders.id = wsk_mpk.order_id LEFT JOIN wsk_budget ON wsk_mpk.budget = wsk_budget.id WHERE orders.id = '.$order_id) or $this->_db->raise_error();
		
		$data = $this->_db->mysql_fetch_all($result);
		
		if (empty($data[0]['post_code']))
		{
			$query = 'SELECT users.name, users.post_code, users.city, users.street, users.home, users.flat, users_delivery.post_code as delivery_post_code, users.identificator, 
			users_delivery.city as delivery_city, users_delivery.street as delivery_street, users_delivery.home as delivery_home, users_delivery.flat as 
			delivery_flat FROM users LEFT JOIN users_delivery ON users.id = users_delivery.user_id WHERE users.id = (SELECT IFNULL(parent_id, 0) FROM 
			user_tree WHERE user_id = '.$data[0]['user_id'].')';
			
			$result = $this->_db->query($query) or $this->_db->raise_error();
			
			if (mysql_num_rows($result))
			{
				$row = mysql_fetch_array($result);
				
				foreach ($data as $key => $details)
				{
					$data[$key]['user_name'] = $row['name'];
					$data[$key]['name'] = $row['name'];
					$data[$key]['city'] = $row['city'];
					$data[$key]['post_code'] = $row['post_code'];
					$data[$key]['street'] = $row['street'];
					$data[$key]['home'] = $row['home'];
					$data[$key]['flat'] = $row['flat'];
					$data[$key]['delivery_post_code'] = $row['delivery_post_code'];
					$data[$key]['delivery_city'] = $row['delivery_city'];
					$data[$key]['delivery_street'] = $row['delivery_street'];
					$data[$key]['delivery_home'] = $row['delivery_home'];
					$data[$key]['delivery_flat'] = $row['delivery_flat'];
					$data[$key]['identificator'] = $row['identificator'];
				}
			}
		}
		
		return $data;
	}
	
	public function getAddress($user_id)
	{	
		$result = $this->_db->query('SELECT users.post_code, users.city, users.street, users.home, users.flat, IFNULL(users_delivery.post_code, \'\') as 
		delivery_post_code, IFNULL(users_delivery.city, \'\') as delivery_city, IFNULL(users_delivery.street, \'\') as delivery_street, 
		IFNULL(users_delivery.home, \'\') as delivery_home, IFNULL(users_delivery.flat, \'\') as delivery_flat FROM users LEFT JOIN users_delivery ON 
		users.id = users_delivery.user_id WHERE users.id = '.$user_id) or $this->_db->raise_error();
		
		return mysql_fetch_array($result);
	}
	
	private function _getChildren($parents)
	{
		$parent_id = array_pop($parents);
			
		$result = $this->_db->query('SELECT user_id FROM user_tree WHERE parent_id = '.$parent_id);
			
		while($row = mysql_fetch_array($result)) 
	   	{
	       	array_push($this->_children, $row['user_id']);
	       	array_push($parents, $row['user_id']);
	   	}
	   		
	   	if (!empty($parents))
	   	{
	   		$this->_getChildren($parents);
	   	}
	}
	
	public function approveOrder($order_id)
	{
		$this->_children = array();
		$this->_getChildren(array($this->_user['id']));
		
		$query = 'SELECT orders.id FROM orders JOIN users ON orders.user_id = users.id WHERE orders.id = '.$order_id.' AND 
		orders.user_id IN (0';
		
		foreach ($this->_children as $child_id)
		{
			$query .= ','.$child_id;
		}
		
		$query .= ')';

		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$this->_secretService($order_id, 1);
			$this->_db->query('UPDATE orders SET status = 1 WHERE status = 0 AND id = '.$order_id) or $this->_db->raise_error();
		}
	}
	
	private function _validateOrder()
	{
		$this->clear();
		
		$this->_address = $values['address'] = trim(mysql_real_escape_string($this->text($_POST['address'])));
		$this->_message = $values['message'] = trim(mysql_real_escape_string($_POST['message']));
		$this->_internal = $values['internal'] = trim(mysql_real_escape_string($_POST['internal']));
		$this->_payment = $values['payment'] = (int)$_POST['payment'];
		$this->_document = $values['document'] = (int)$_POST['document'];
		
		if (!$this->validateTextarea($this->_address))
		{
			$this->_errors++;
			$errors['address'] = 'Adres dostawy jest wymagany';
		}
		
		if ($this->_errors)
		{
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
		else 
		{
			$basket = new Basket();
			$netto_value = $basket->getNettoValue();
			
			$limits = $this->getLimits();
			
			if ($netto_value <= $limits['order_limit'])
			{
				$this->_saveOrder();
			}
		}
	}
	
	private function _saveOrder()
	{
		$basket = new Basket();
		$brutto_value = $basket->getBruttoValue();
		$netto_value = $basket->getNettoValue();
		$paths = $this->getSession("b2b_paths");
		
		unset($_SESSION["b2b_paths"]);
		
		if ((float)$brutto_value > 0)
		{
			switch ($this->_document)
			{
				case 1: $document = 'WZ'; break;
				case 2: $document = 'WZ'; break;
			}
			
			switch ($this->_payment)
			{
				case 1: $payment = 'według umowy'; break;
				case 2: $payment = 'według umowy'; break;
			}
			
			$mpks = array();		
	
			foreach ($this->_products as $product)
			{
				if (!in_array($product['mpk'], $mpks))
				{
					array_push($mpks, $product['mpk']);
				}
			}
			
			foreach ($mpks as $mpk)
			{
				$this->_db->query('INSERT INTO orders VALUES (\'\', \''.mysql_real_escape_string($this->_internal).'\', '.$this->_user['id'].', \'0\', \'0\', 8, \''.$document.'\', 
			\''.$payment.'\', \''.$this->_message.'\',  \''.date('Y-m-d H:i:s').'\', \'0000-00-00\')') or $this->_db->raise_error();
				
				$result = $this->_db->query('SELECT id FROM users WHERE id = '.$this->_user['id'].' AND ordering = 1') or $this->_db->raise_error();
				$order_id = mysql_insert_id();
				
				$this->_saveProducts($order_id, $mpk);
				//$this->_sendUserMessage();
				
				mysql_free_result($result);
				
				$result = $this->_db->query('SELECT DISTINCT netto_value, mpk FROM order_details JOIN orders ON order_details.order_id = orders.id WHERE order_details.order_id = '.$order_id) or $this->_db->raise_error();
				
				if (mysql_num_rows($result))
				{
					$order = mysql_fetch_array($result);
					mysql_free_result($result);
					
					foreach ($paths as $data)
					{
						$path = explode('@#@', $data);
						
						if ($path[0] == $order['mpk'])
						{
							$result = $this->_db->query('SELECT email, add_date, login FROM users WHERE id = '.$this->_user['id']) or $this->_db->raise_error();
							
							if (mysql_num_rows($result))
							{
								$contractor = mysql_fetch_array($result);
							}
							else
							{
								$contractor = array('email' => '', 'add_date' => '1970-01-01', 'login' => '1970-01-01');
							}
							
							mysql_free_result($result);
						
							$result = $this->_db->query('SELECT id, email, add_date, login FROM users WHERE id = '.$path[2]) or $this->_db->raise_error();
							
							if (mysql_num_rows($result))
							{
								$analyst = mysql_fetch_array($result);
							}
							else
							{
								$analyst = array('id' => 0, 'email' => '', 'add_date' => '1970-01-01', 'login' => '1970-01-01');
							}
							
							mysql_free_result($result);
							
							$result = $this->_db->query('SELECT id, email, add_date, login FROM users WHERE id = '.$path[3]) or $this->_db->raise_error();
						
							if (mysql_num_rows($result))
							{
								$manager = mysql_fetch_array($result);
							}
							else
							{
								$manager = array('id' => 0, 'email' => '', 'add_date' => '1970-01-01', 'login' => '1970-01-01');
							}
							
							mysql_free_result($result);
							
							$result = $this->_db->query('SELECT id, email, add_date, login FROM users WHERE id = '.$path[4]) or $this->_db->raise_error();
							
							if (mysql_num_rows($result))
							{
								$receiving = mysql_fetch_array($result);
							}
							else
							{
								$receiving = array('id' => 0, 'email' => '', 'add_date' => '1970-01-01', 'login' => '1970-01-01');
							}
							
							mysql_free_result($result);
							
							$query = 'INSERT INTO wsk_mpk VALUES (\'\', '.$order_id.', \''.mysql_real_escape_string($mpk).'\', '.(int)$path[1].', \''.$order['netto_value'].'\',
							'.$this->_user['id'].', \''.mysql_real_escape_string($contractor['email']).'\', \''.mysql_real_escape_string($contractor['login']).'\', 
							\''.date('Y-m-d').'\', 0, '.(int)$analyst['id'].', \''.mysql_real_escape_string($analyst['email']).'\', \''.mysql_real_escape_string($analyst['login']).'\', 
							\'1970-01-01\', 0, \'\', \'\', \'1970-01-0\', 0, '.(int)$manager['id'].', \''.mysql_real_escape_string($manager['email']).'\', 
							\''.mysql_real_escape_string($manager['login']).'\', \'1970-01-01\', 0, '.(int)$receiving['id'].', \''.mysql_real_escape_string($receiving['email']).'\', 
							\''.mysql_real_escape_string($receiving['login']).'\', \'1970-01-01\', 0, \'\')';
							
							$this->_db->query($query) or $this->_db->raise_error();
						}
					}
				}
				
				$this->saveOrderChangeMessage($order_id, 'oczekującym zamówieniu');
			}
			
			$_SESSION['basket_products'] = null;
			unset($_SESSION['basket_products']);
			
			if (!empty($_COOKIE["b2b_basket_key"]))
			{
				$this->_db->query('DELETE FROM basket WHERE `key` = \''.mysql_real_escape_string($_COOKIE["b2b_basket_key"]).'\'') or $this->_db->raise_error();
			}
			
			unset($_COOKIE["b2b_basket_key"]);
			
			header('Location: ./zlozonezamowienie.html');
		}
	}
	
	private function _saveProducts($order_id, $mpk)
	{
		$netto_sum = 0;
		$brutto_sum = 0;
	
		foreach ($this->_products as $product)
		{
			if (!strcmp($product['mpk'], $mpk))
			{
				$this->_db->query('INSERT INTO order_details VALUES (\'\', '.$order_id.', '.$product['product_id'].', \''.mysql_real_escape_string($product['name']).'\', \''.trim(mysql_real_escape_string($product['producer'])).'\',  
				\''.$product['index'].'\', 0, \'\', \''.$product['netto_price'].'\', '.$product['tax'].', \''.$product['brutto_price'].'\', \''.$product['count'].'\', 0, 
				\''.mysql_real_escape_string($product['mpk']).'\', \''.mysql_real_escape_string($product['info']).'\', '.$product['query_id'].', \'\', 1, 1)') or $this->_db->raise_error();
	
				$this->_db->query('UPDATE products SET sold_amount = `sold_amount` + \''.$product['count'].'\' WHERE id_hermes = '.$product['product_id']) or $this->_db->raise_error();
				
				$netto_sum += ($product['netto_price'] * $product['count']);
				$brutto_sum += ($product['count'] * $product['netto_price'] * (1+($product['tax'] / 100)));
			}
		}
		
		$this->_db->query('UPDATE orders SET `netto_value` = `netto_value` + \''.sprintf("%0.2f", $netto_sum).'\', `brutto_value` = `brutto_value` + \''.sprintf("%0.2f", $brutto_sum).'\' WHERE id = '.$order_id);
	}
	
	private function _sendUserMessage()
	{
		$message = '
		<p><img src="http://www.wsk.unimet.pl/images/logotype.png"></p>
		<p><strong>Użytkownik złożył zamówienie w serwisie wsk.unimet.pl</strong></p>
		<p>Login użytkownika: <strong>'.$this->_user['login'].'</strong></p>';
		
		$mimemail = new nomad_mimemail(); 
		
		$from		= "newsletter@unimet.pl";	 
		$html		= '<html><body>'.iconv("UTF-8", "ISO-8859-2", $message).'</body></html>';
		$subject    = 'wsk.unimet.pl - informacja o nowym zamówieniu';
		
		$to	= $this->_user['email'];
   			
   		$mimemail->set_smtp_log(true); 
		$smtp_host	= "swarog.az.pl";
		$smtp_user	= "newsletter@unimet.pl";
		$smtp_pass	= "1hadesa2madmax";
		$mimemail->set_smtp_host($smtp_host);
		$mimemail->set_smtp_auth($smtp_user, $smtp_pass); 
		$mimemail->new_mail($from, $to, iconv("UTF-8", "ISO-8859-2", $subject), "", $html);
		
		$mimemail->send(); 	
	}
	
	public function acceptOrder($id)
	{
		$result = $this->_db->query('SELECT orders.status, orders.netto_value, orders.add_date, IFNULL(order_details.query_id, 0) as query_id, wsk_mpk.budget FROM orders LEFT 
		JOIN order_details ON orders.id = order_details.order_id LEFT JOIN wsk_mpk ON orders.id = wsk_mpk.order_id WHERE orders.id = '.$id.' LIMIT 1') or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$order = mysql_fetch_array($result);
			mysql_free_result($result);
			
			if (in_array($order['status'], array(6,7,9,10)))
			{
				$query = 'SELECT id FROM wsk_mpk WHERE (';
				
				switch ($order['status'])
				{
					case 6:
						$query .= 'analyst = '.$this->_user['id'].' OR analyst_deputy = '.$this->_user['id'];
						break;
						
					case 7:
						$query .= 'manager = '.$this->_user['id'].' OR manager_deputy = '.$this->_user['id'];
						break;
						
					case 9:
						$query .= 'contractor = '.$this->_user['id'].' OR contractor_deputy = '.$this->_user['id'];
						break;
					
					case 10:
						$query .= 'administrator = '.$this->_user['id'];
						break;
				}
				
				$query .= ') AND order_id = '.$id;
				
				$result = $this->_db->query($query) or $this->_db->raise_error();
				
				if ((int)$this->_user['id'] > 0 && ($this->_user['admin'] == 1 || mysql_num_rows($result)))
				{
					$access = true;
					
					if (in_array($order['status'], array(6,9)))
					{
						$query = 'SELECT order_details.id, IFNULL(products.id_hermes, 0) as id_hermes, order_details.product_id, order_details.name as 
						product_name, order_details.netto_price, order_details.amount, IFNULL(order_replacements.product_id, 0) as replacement_id, 
						order_replacements.name as replacement_name, products.symbol as replacement_symbol, products.tax as replacement_tax FROM order_details 
						LEFT JOIN order_replacements ON order_details.id = order_replacements.element_id AND order_details.order_id = order_replacements.order_id 
						LEFT JOIN products ON order_replacements.product_id = products.id_hermes WHERE order_details.order_id = '.$id;
						
						$result = $this->_db->query($query) or $this->_db->raise_error();
						
						if (mysql_num_rows($result))
						{
							$prices = new Prices();
							
							while($row = mysql_fetch_array($result)) 
					   		{
					   			if ($row['replacement_id'] > 0 && $row['id_hermes'] > 0)
					   			{
					   				$replacement_netto = $prices->getPrice($row['replacement_id'], 4);
					   				
					   				$query = 'INSERT INTO order_savings VALUES (\'\', '.$id.', '.$row['product_id'].', \''.mysql_real_escape_string($row['product_name']).'\', 
					   				\''.mysql_real_escape_string($row['netto_price']).'\', '.$row['replacement_id'].', \''.mysql_real_escape_string($row['replacement_name']).'\', 
					   				\''.mysql_real_escape_string($replacement_netto).'\', \''.($row['netto_price'] - $replacement_netto).'\', \''.date('Y-m-d H:i:s').'\')';
						       		
						       		$this->_db->query($query) or $this->_db->raise_error();
						       		
						       		$query = 'UPDATE order_details SET product_id = '.$row['replacement_id'].', name = \''.mysql_real_escape_string($row['replacement_name']).'\', 
						       		symbol = \''.mysql_real_escape_string($row['replacement_symbol']).'\', netto_price = \''.mysql_real_escape_string($replacement_netto).'\', 
						       		tax = '.(int)$row['replacement_tax'].', brutto_price = \''.mysql_real_escape_string($prices->getPrice($row['replacement_id'], 1)).'\' 
						       		WHERE id = '.$row['id'];
						       		
						       		$this->_db->query($query) or $this->_db->raise_error();
						       		
						       		$query = 'DELETE FROM order_replacements WHERE order_id = '.$id.' AND element_id = '.$row['id'];
						       		
						       		$this->_db->query($query) or $this->_db->raise_error();
					   			}
					   		}
						}
					}
					else if ($order['status'] == 7)
					{
						$year_postfix = ((date('Y', strtotime($order['add_date'])) == date('Y')) ? '' : '_'. date('Y', strtotime($order['add_date'])));
						$order_day = date('Y-m-d', strtotime($order['add_date']));
						$order_year = (int)date('Y', strtotime($order['add_date']));

						$query = "SELECT * FROM wsk_budget" . $year_postfix . " 
						WHERE id = " . $order['budget'] . " AND start <= '" . $order_day . "' AND end >= '" . $order_day . "'";

						$result = $this->_db->query($query) or $this->_db->raise_error();
						
						if (mysql_num_rows($result))
						{
							$budget = mysql_fetch_array($result);
							mysql_free_result($result);
							
							if ($budget['budget'] >= $order['netto_value'])
							{
								$query = 'INSERT INTO wsk_budget_history VALUES (\'\', \''.mysql_real_escape_string($budget['department']).'\', '.$this->_user['id'].', 
								\''.mysql_real_escape_string($this->_user['login']).'\', \''.$budget['budget'].'\', \''.($budget['budget'] - $order['netto_value']).'\', 
								\''.((-1) * $order['netto_value']).'\', '.$id.', \'zmiana budżetu\', \''.date('Y-m-d H:i:s').'\', \''.$order_year.'\')';
								
								$this->_db->query($query) or $this->_db->raise_error();
								
								$query = "UPDATE wsk_budget" . $year_postfix . " 
								SET `used_budget` = `used_budget` + ('" . $order['netto_value'] . "'), `budget` = `budget` - '" . $order['netto_value']. "' 
								WHERE id = " . $budget['id'];
								
								$this->_db->query($query) or $this->_db->raise_error();
							}
							else
							{
								$access = false;
								$communicats['ok'] = 'wartość zamówienia przekracza ustalony budżet';
							}
						}
						else
						{
							$access = false;
							$communicats['ok'] = 'nie ustalono budżetu dla danego oddziału';
						}
					}
					
					if (true == $access)
					{
						if (in_array($order['status'], array(6,7,10)))
						{
							$query = 'UPDATE wsk_mpk SET ';
							
							switch ($order['status'])
							{
								case 6:  $query .= 'analyst_date = '; break;
								case 7:  $query .= 'manager_date = '; break;
								case 10: $query .= 'administrator = '.$this->_user['id'].', administrator_email = \''.mysql_real_escape_string($this->_user['email']).'\', administrator_login = \''.mysql_real_escape_string($this->_user['login']).'\', administrator_date = '; break;
							}
							
							$query .= '\''.date('Y-m-d').'\' WHERE order_id = '.$id;
							$this->_db->query($query) or $this->_db->raise_error();
						}
						

						//query id odpowiada za informcację o nr zapytania
						

// case 6: $params['param2'] = 'u_analityka'; break;
// 		case 7: $params['param2'] = 'u_kierownika'; break;
// 		case 1: $params['param2'] = 'w_unimet'; break;
// 		case 2: $params['param2'] = 'w_unimet'; break;
// 		case 3: $params['param2'] = 'w_drodze'; break;
// 		case 4: $params['param2'] = 'archiwum'; break;
// 		case 5: $params['param2'] = 'odrzucone'; break;
// 		case 9: $params['param2'] = 'u_zamawiajacego'; break;
// 		case 10: $params['param2'] = 'u_administratora'; break;
// 		case 8: $params['param2'] = 'u_zamawiajacego'; break;
					
						
						//BG tutaj zmiana statusu zamówienia rozbudowana o warunki finasowe 



						if($order['query_id'] <= 0){

							//procedujemy zamówienie

							switch ($order['status'])
							{
								case 6:  $status = (($order['netto_value'] < 1000) ? 1 : 7); break;
								case 7:  $status = 1; break;
								case 9:  $status = 6; break;
								case 10: $status = 7; break;
							}


						}
						else {

							//to jest zapytanie

							switch ($order['status'])
							{
								case 6:  $status = (($order['netto_value'] < 1000) ? 1 : 7); break;
								case 10: $status = 7; break;
								case 7:  $status = 1; break;
								case 9:  $status = 6; break;
							}


						}



















						$this->_secretService($id, $status);
						
						$query = 'UPDATE orders SET update_date = \''.date('Y-m-d').'\', status = '.$status.' WHERE id = '.$id;
						$this->_db->query($query) or $this->_db->raise_error();
						
						$this->saveOrderChangeMessage($id, 'zmianie statusu zamówienia');
						
						$communicats['ok'] = 'status zamówienia został zmieniony';
					}
					else 
					{
						$this->setSession("communicats", $communicats);
					}
					
					$this->setSession("communicats", $communicats);
				}
			}
		}
	}
	
	public function cancelOrder($id, $comment)
	{
		if ($this->validateTextarea($comment))
		{
			$result = $this->_db->query('SELECT orders.status, orders.netto_value, IFNULL(wsk_mpk.budget, 0) as budget FROM orders LEFT JOIN wsk_mpk ON orders.id = wsk_mpk.order_id 
			WHERE orders.id = '.$id) or $this->_db->raise_error();
			
			if (mysql_num_rows($result))
			{
				$order = mysql_fetch_array($result);
				mysql_free_result($result);
				
				if (in_array($order['status'], array(6,7,9,10)))
				{
					$query = 'SELECT id FROM wsk_mpk WHERE (';
					
					switch ($order['status'])
					{
						case 6:
							if (!empty($comment))
							{
								$comment = '\n\n--- KOMENTARZ ANALITYKA ---\n\n'.$comment;
							}
							
							$query .= 'analyst = '.$this->_user['id'].' OR analyst_deputy = '.$this->_user['id'];
							break;
							
						case 7:
							
							if (!empty($comment))
							{
								$comment = '\n\n--- KOMENTARZ KIEROWNIKA ---\n\n'.$comment;
							}
							
							$query .= 'manager = '.$this->_user['id'].' OR manager_deputy = '.$this->_user['id'];
							break;
							
						case 9:
							
							if (!empty($comment))
							{
								$comment = '\n\n--- KOMENTARZ ZAMAWIAJĄCEGO ---\n\n'.$comment;
							}
							
							$query .= 'contractor = '.$this->_user['id'].' OR contractor_deputy = '.$this->_user['id'];
							break;
							
						case 10:
							
							if (!empty($comment))
							{
								$comment = '\n\n--- KOMENTARZ ADMINISTRATORA ---\n\n'.$comment;
							}
							
							$query .= 'administrator = '.$this->_user['id'];
							break;
					}
					
					$query .= ') AND order_id = '.$id;
	
					$result = $this->_db->query($query) or $this->_db->raise_error();
					
					if ((int)$this->_user['id'] > 0 && ($this->_user['admin'] == 1 || mysql_num_rows($result)))
					{	

						if (in_array($order['status'], array(6,7,10)))
						{
							$this->_secretService($id, 5);
							
							$this->_db->query('UPDATE orders SET status = 5, `comment` = concat(`comment`,\''.trim(mysql_real_escape_string($comment)).'\'), update_date = \''.date('Y-m-d').'\' WHERE id = '.$id);
							
							$query = 'UPDATE wsk_mpk SET ';
								
							switch ($order['status'])
							{
								case 6: $query .= 'analyst_date = '; break;
								case 7: $query .= 'manager_date = '; break;
								case 10: $query .= 'administrator = '.$this->_user['id'].', administrator_email = \''.mysql_real_escape_string($this->_user['email']).'\', administrator_login = \''.mysql_real_escape_string($this->_user['login']).'\', administrator_date = '; break;
							}
								
							$query .= '\''.date('Y-m-d').'\' WHERE order_id = '.$id;
							$this->_db->query($query) or $this->_db->raise_error();

							$this->saveOrderChangeMessage($id, 'zmianie statusu zamówienia');
						}
						else
						{
							$this->_secretService($id, 8);
							$this->_db->query('UPDATE orders SET status = 8, `comment` = concat(`comment`,\''.$comment.'\'), update_date = \''.date('Y-m-d').'\' WHERE id = '.$id);
						}
						
						$communicats['ok'] = 'zamówienie zostało odrzucone.';
						$this->setSession("communicats", $communicats);
					}
				}
			}
		}
		else
		{
			$communicats['error'] = 'komentarz do zamówienia jest wymagany.';
			$this->setSession("communicats", $communicats);
		}
	}
	
	public function receiveOrder($id)
	{
		$result = $this->_db->query('SELECT receiving, receiving_deputy FROM wsk_mpk WHERE (receiving = '.$this->_user['id'].' OR 
		receiving_deputy = '.$this->_user['id'].') AND order_id = '.$id) or $this->_db->raise_error();
		
		if ((int)$this->_user['id'] > 0 && ($this->_user['admin'] == 1 || mysql_num_rows($result)))
		{
			$query = 'UPDATE wsk_mpk SET receiving_date = \''.date('Y-m-d').'\' WHERE order_id = '.$id;
			$this->_db->query($query) or $this->_db->raise_error();
			
			$this->_secretService($id, 4);
			$this->_db->query('UPDATE orders SET status = 4, update_date = \''.date('Y-m-d').'\' WHERE id = '.$id);
			$communicats['ok'] = 'zamówienie zostało przyjęte.';
			$this->setSession("communicats", $communicats);
			
			$this->saveOrderChangeMessage($id, 'zmianie statusu zamówienia');
		}
	}
	
	public function rejectReplacement($id)
	{
		$this->_db->query('DELETE FROM order_replacements WHERE id = '.$id) or $this->_db->raise_error();
	}
	
	public function saveOrderChangeMessage($id, $title)
	{
		$list = $this->getOrder($id);
		
		$mimemail = new nomad_mimemail(); 
		$emails = array();
		
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><img src="'.BASE_ADDRESS.'images/logotype_small.png" alt="" /></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">&nbsp;</p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><strong>Informacja o '.$title.'.</strong></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">&nbsp;</p>';
		
		switch ($list[0]['status'])
		{
			case 1:
				
				$title = 'nowym zamówieniu';
				
				$result = $this->_db->query('SELECT email FROM user_details JOIN users ON user_details.user_id = users.id WHERE user_details.merchant_id = '.$list[0]['merchant_id'].' AND email != \'\'') or $this->_db->raise_error();
				
				while($row = mysql_fetch_array($result)) 
   				{
   					if (!in_array($row['email'], $emails))
   					{
   						array_push($emails, $row['email']);
   					}
   				}
				
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$this->formatId($list[0]['id'], $list[0]['query_id']).'</strong>, które 
				zostało złożone w dniu <strong>'.(($list[0]['manager_date'] != "1970-01-01") ? $list[0]['manager_date'] : $list[0]['add_date']).'</strong> oczekuje na Twoją reakcję.</p>';
				
				break;
				
			case 4:
				array_push($emails, $list[0]['contractor_email']);
				
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$this->formatId($list[0]['id'], $list[0]['query_id']).'</strong>, które 
				zostało złożone w dniu <strong>'.$list[0]['menager_date'].'</strong> zostało odebrane.</p>';
				break;
			
			case 5:
				array_push($emails, $list[0]['contractor_email']);

				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$this->formatId($list[0]['id'], $list[0]['query_id']).'</strong>, które 
				zostało złożone w dniu <strong>'.$list[0]['contractor_date'].'</strong> zostało odrzucone.</p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><strong>Komentarz osoby odrzucającej</strong></p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['reject_comment'].'</p>';
				break;
			
			case 6:
				array_push($emails, $list[0]['analyst_email']);
				
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$this->formatId($list[0]['id'], $list[0]['query_id']).'</strong>, które 
				zostało złożone w dniu <strong>'.$list[0]['contractor_date'].'</strong> oczekuje na Twoją reakcję.</p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Aby przejść do zamówienia zaloguj się do <a href="http://wsk.unimet.pl">wsk.unimet.pl</a></p>';
				break;
				
			case 7:
				array_push($emails, $list[0]['manager_email']);
				
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$this->formatId($list[0]['id'], $list[0]['query_id']).'</strong>, które 
				zostało złożone w dniu <strong>'.$list[0]['contractor_date'].'</strong> oczekuje na Twoją reakcję.</p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Aby przejść do zamówienia zaloguj się do <a href="http://wsk.unimet.pl">wsk.unimet.pl</a></p>';
				break;
				
			case 10:

				$result = $this->_db->query('SELECT email FROM users WHERE status IN (1,2) AND admin = 1 AND email != \'\'') or $this->_db->raise_error();
				
				while($row = mysql_fetch_array($result)) 
   				{
   					if (!in_array($row['email'], $emails))
   					{
   						array_push($emails, $row['email']);
   					}
   				}
				
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$this->formatId($list[0]['id'], $list[0]['query_id']).'</strong>, które 
				zostało złożone w dniu <strong>'.$list[0]['contractor_date'].'</strong> oczekuje na Twoją reakcję.</p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Aby przejść do zamówienia zaloguj się do <a href="http://wsk.unimet.pl">wsk.unimet.pl</a></p>';
				break;
				break;
		}
		
		$user_names = $this->getUsersNames();
		
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">&nbsp;</p>';
		$message .= '<table cellspacing="0" cellpadding="0">';
		$message .= '<tr>';
			$message .= '<td style="border:'.((in_array($list[0]['status'], array(4,5))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">Zamawiający</span></p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$user_names[$list[0]['contractor_login']].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['contractor_email'].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($list[0]['contractor_date'] != "1970-01-01" && !empty($list[0]['contractor_date'])) ? $list[0]['contractor_date'] : "").'</p>';
			$message .= '</td>';
			
			if ($list[0]['query_id'] == 0) 
			{
				$message .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
				$message .= '<td style="border:1px solid #7597c5; text-align:center; padding:5px">';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">UNIMET - propozycja zamiennika</span></p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.((!empty($list[0]['unimet_representative'])) ? $user_names[$list[0]['unimet_representative']] : 'brak danych').'</p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['unimet_email'].'</p>';
				$message .= '</td>';
			}
			
			$message .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
			$message .= '<td style="border:'.((in_array($list[0]['status'], array(6))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">Analityk</span></p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$user_names[$list[0]['analyst_login']].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['analyst_email'].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($list[0]['analyst_date'] != "1970-01-01" && !empty($list[0]['analyst_date'])) ? $list[0]['analyst_date'] : "").'</p>';
			$message .= '</td>';
			$message .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
			
			if ($list[0]['query_id'] > 0)
			{
				$message .= '<td style="border:'.((in_array($list[0]['status'], array(10))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">Administrator</span></p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$user_names[$list[0]['administrator_login']].'</p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['administrator_email'].'</p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($list[0]['administrator_date'] != "1970-01-01" && !empty($list[0]['administrator_date'])) ? $list[0]['administrator_date'] : "").'</p>';
				$message .= '</td>';
				$message .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
			}
			
			$message .= '<td style="border:'.((in_array($list[0]['status'], array(7))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">Kierownik</span></p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$user_names[$list[0]['manager_login']].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['manager_email'].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($list[0]['manager_date'] != "1970-01-01" && !empty($list[0]['manager_date'])) ? $list[0]['manager_date'] : "").'</p>';
			$message .= '</td>';
			$message .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
			$message .= '<td style="border:'.((in_array($list[0]['status'], array(1))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">UNIMET</span></p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.((!empty($list[0]['unimet_representative'])) ? $user_names[$list[0]['unimet_representative']] : 'brak danych').'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['unimet_email'].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($list[0]['delivery_date'] != "1970-01-01" && !empty($list[0]['delivery_date'])) ? $list[0]['delivery_date'] : "").'</p>';
			$message .= '</td>';
			$message .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
			$message .= '<td style="border:'.((in_array($list[0]['status'], array(4))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">Odbierający</span></p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$user_names[$list[0]['receiving_login']].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['receiving_email'].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($list[0]['receiving_date'] != "1970-01-01" && !empty($list[0]['receiving_date'])) ? $list[0]['receiving_date'] : "").'</p>';
			$message .= '</td>';
		$message .= '</tr>';
		$message .= '</table>';
		
		$message .= '<div style="padding-top: 20px;">';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Wydział: <strong>'.$list[0]['department'].'</strong></p>';	
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Data złożenia zamówienia: <strong>'.$list[0]['add_date'].'</strong></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Data dostawy: <strong>'.(($list[0]['delivery_date'] != "1970-01-01" && !empty($list[0]['delivery_date'])) ? $list[0]['delivery_date'] : "brak danych").'</strong></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Forma płatności: <strong>'.$list[0]['payment'].'</strong></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Numer MPK: <strong>'.$list[0]['mpk'].'</strong></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Faktura VAT <strong>'.((!empty($list[0]['fv'])) ? $list[0]['fv'] : 'brak danych').'</strong></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">WZ: <strong>'.((!empty($list[0]['wm'])) ? $list[0]['wm'] : 'brak danych').'</strong></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Dokument <strong>'.$list[0]['document'].'</strong></p>';
		$message .= '</div>';
		
		$from = "newsletter@unimet.pl";	 
		$html = '<html xmlns="http://www.w3.org/1999/xhtml">';
		$html .= '<head>';
		$html .= '</head>';
		$html .= '<body>'.iconv("UTF-8", "ISO-8859-2", $message).'</body></html>';
		$subject = 'wsk.unimet.pl - informacja o '.$title;


		
   		$mimemail->set_smtp_log(true); 
		$smtp_host	= "swarog.az.pl";
		$smtp_user	= "newsletter@unimet.pl";
		$smtp_pass	= "1hadesa2madmax";
		$mimemail->set_smtp_host($smtp_host);
		$mimemail->set_smtp_auth($smtp_user, $smtp_pass); 
		
		foreach ($emails as $email)
		{
			if ($this->validateEmail($email))
			{
				$mimemail->new_mail($from, $email, iconv("UTF-8", "ISO-8859-2", $subject), "", $html);
				$mimemail->send(); 	
			}
		}
	}
	
	private function _secretService($order_id, $status)
	{
		$result = $this->_db->query('SELECT status FROM orders WHERE id = '.$order_id) or $this->_db->raise_error();	
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			mysql_free_result($result);
			
			$query = 'INSERT INTO secret_service VALUES (\'\', '.$order_id.', '.$row['status'].', '.$status.', '.$this->_user['id'].', \''.date('Y-m-d H:i:s').'\')';
			$this->_db->query($query) or $this->_db->raise_error();	
		}
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}

$orders = new Orders();
?>
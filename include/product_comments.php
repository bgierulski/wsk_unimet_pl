<div id="form" class="comment">
<form action="<?php echo substr(BASE_ADDRESS, 0, -1).$_SERVER['REQUEST_URI']; ?>" method="POST" name="add_comment">
	<input type="hidden" name="operation" value="add_comment" />
	<input type="hidden" name="product_id" value="<?php echo $product['id_hermes']; ?>" />
	<input type="hidden" name="ip" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>" />
			
	<?php if ($_SESSION["errors"]['nick'] != '') { ?>
	<div class="element error">
	<label>&nbsp;</label><?php echo $_SESSION["errors"]['nick']; ?></div>
	<?php } ?>
	
	<?php if (empty($_SESSION["values"]['nick'])) { $_SESSION["values"]['nick'] = $_SESSION["b2b_user"]['name']; } ?>
	
	<div class="element">
	<label>Nick <strong class="error">(*)</strong></label>
	<input name="nick" value="<?php echo $_SESSION["values"]['nick']; ?>" type="text" style="width:400px;" />
	</div>
				
	<?php if ($_SESSION["errors"]['comment'] != '') { ?>
	<div class="element error">
	<label>&nbsp;</label><?php echo $_SESSION["errors"]['comment']; ?></div>
	<?php } ?>
	<div class="element">
	<label>Opinia <strong class="error">(*)</strong></label>
	<textarea cols="" rows="" name="comment" style="width:400px;"><?php echo $_SESSION["values"]['comment']; ?></textarea>
	</div>
				
	<?php if ($_SESSION["errors"]['www'] != '') { ?>
	<div class="element error">
	<div><?php echo $_SESSION["errors"]['www']; ?></div>
	</div>
	<?php } ?>
	<div class="element">
	<label>WWW</label>
	
	<?php
	if (empty($_SESSION["values"]['www'])) { $_SESSION["values"]['www'] = 'http://'; }
	?>
	<input name="www" value="<?php echo $_SESSION["values"]['www']; ?>" type="text" style="width:400px;" />
	</div>
				
	<div class="element">
	<label>&nbsp;</label>
	<p class="button">
	<a id="add_comment_form_submit" href="./">ZAPISZ OPINIĘ</a>
	</p>
	</div>
</form>
</div>
			
<?php
$comments_list = $comments->getComments($product['id_hermes']);
			
while($comment = mysql_fetch_array($comments_list)) 
{
	?>
	 <div class="comment_content">
	 	<div class="comment_padding">
	    	<div class="nick">
	       	<?php echo $comment['nick']; ?>
	       	</div>
	       	<div class="link">
	       	<?php if (!empty($comment['www']) && $comment['www'] != 'http://') { ?>
	       	<a style="color:#e84f05;" href="<?php echo $comment['www']; ?>"><?php echo $comment['www']; ?></a>
	       	<?php } ?>
	       	</div>
	       	<div class="clear"></div>
	    </div>
	    <p class="comment_text"><?php echo $comment['comment']; ?></p>
	</div>
	<div class="clear"></div>
	<?php
}
?>
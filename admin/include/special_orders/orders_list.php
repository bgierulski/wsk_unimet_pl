<?php
$list = $special_orders->getSpecialOrders();
?>

<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
</div>
<?php } else if (isset($_SESSION["communicats"]['ok'])) { ?>
<div class="form_element ok">
	<?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?>
</div>
<?php } ?>

<table>
	<tr>
		<th>Użytkownik</th>
		<th>Data dodania</th>
		<th>Operacje</th>
	</tr>
	<?php
	if (is_array($list))
	{
		foreach ($list as $key => $row)
	   	{
			?>
		    <tr>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $special_orders->textControll($row['user_name']); ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['add_date']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>">
				<a href="./special_orders/edit/<?php echo $row['id']; ?>">edycja zamówienia</a> &nbsp;::&nbsp;
				<a href="./special_orders/delete/<?php echo $row['id']; ?>">usunięcie zamówienia</a>
				</td>
			</tr>
		    <?php
	   	}
	}
	?>
</table>

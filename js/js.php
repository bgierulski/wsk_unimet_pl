<?php
header("Content-Encoding: gzip");

$folder = $_GET['folder'];
$file = $_GET['file'];

include('../include/config.php');
$path = BASE_DIR.'../js/'.((!empty($folder)) ? $folder.'/' : '').$file;

if (file_exists($path))
{
	$fd = fopen($path, "r");
	echo gzencode(fread($fd, filesize($path)));
	$fd = fclose($fd);
}

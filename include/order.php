<div id="page_header">
	<div class="details">
		<p><strong>Realizacja zamówienia</strong></p>
	</div>
	<div class="clear"></div>
</div>

<div id="page_content">
<?php
if (isset($_SESSION["b2b_user"]['id']) && (int)$_SESSION["b2b_user"]['id'])
{
	$user = $orders->getAddress($_SESSION["b2b_user"]['id']);
	
	if (!isset($_SESSION["values"]['payment'])) 
	{ 
		$_SESSION["values"]['payment'] = (($_SESSION["b2b_user"]['payment_id'] == 1) ? 1 : 2); 
	}
	if (!isset($_SESSION["values"]['document'])) 
	{ 
		$_SESSION["values"]['document'] = 2; 
	}
	if (!isset($_SESSION["values"]['address']) || $_SESSION["values"]['address'] == '') 
	{ 
		if ($user['delivery_post_code'] != '')
		{
			$_SESSION["values"]['address'] = $user['delivery_post_code'].' '.$user['delivery_city']."\n";
		if ($user['delivery_flat'] != '') { $_SESSION["values"]['address'] .= '/'.$user['delivery_flat']; }
			$_SESSION["values"]['address'] .= $user['delivery_street'].' '.$user['delivery_home']."\n";
		}
		else
		{
			$_SESSION["values"]['address'] = $user['post_code'].' '.$user['city']."\n";
			if ($user['flat'] != '') { $_SESSION["values"]['address'] .= '/'.$user['flat']; }
			$_SESSION["values"]['address'] .= $user['street'].' '.$user['home']."\n";
		}
	}
	?>
	<div id="form">
	<form method="POST" action="<?php echo substr(BASE_ADDRESS, 0, -1).$_SERVER['REQUEST_URI']; ?>" name="send_order">
	<input type="hidden" name="operation" value="send_order" />
	
	<div id="order_form">
	
		<?php if ($_SESSION["errors"]['address'] != '') { ?>
		<div class="element error">
		<label style="width:225px">&nbsp;</label><?php echo $_SESSION["errors"]['address']; ?></div>
		<?php } ?>
			
		<input type="hidden" name="address" value="<?php echo $validators->textControll($_SESSION["values"]['address']); ?>" />
		
		<div class="element radio_element">
		<label style="width:225px">Forma płatności:</label>
			<div style="float:left;">
			<input type="hidden" name="payment" value="<?php echo $_SESSION["values"]['payment']; ?>" value="1" />
			<p><strong><?php echo (($_SESSION["values"]['payment'] == 1) ? 'według umowy' : 'według umowy'); ?></strong></p>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		
		<div class="element radio_element">
		<label style="width:225px">Dokument</label>
			<div style="float:left;">
			<input type="hidden" name="document" value="<?php echo $_SESSION["values"]['document']; ?>" value="2" />
			<p><strong><?php echo (($_SESSION["values"]['document'] == 2) ? 'WZ' : 'WZ'); ?></strong></p>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		
		<div class="element radio_element">
		<label style="width:225px">Numer wewnętrzny WSK (opcjonalnie)</label>
			<div style="float:left;">
			<p><input type="text" name="internal" value="" style="width:135px" /></p>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		
		<div class="element">
		<label style="width:225px">Uwagi do zamówienia</label>
		<textarea cols="" rows="" name="message"><?php echo $_SESSION["values"]['message']; ?></textarea>
		</div>
		<div class="clear"></div>
			
		<?php
		$weight = $basket->getWeight(); 
		$netto = $basket->getNettoValue();
		$size_info = $basket->checkSize();
		?>
			
		<div class="payment" id="payment_1" style="display:<?php if ($_SESSION["values"]['payment'] == 1) { echo 'block'; } else { echo 'none'; } ?>">
			
			<div class="element">
			<label style="width:225px">&nbsp;</label>
				<div style="float:left;">
				<p>Wartość zamówienia netto: <strong><?php echo $netto; ?> zł</strong></p>
				</div>
				<div class="clear"></div>
			</div>
	
		</div>
			
		<div class="payment"  id="payment_2" style="display:<?php if ($_SESSION["values"]['payment'] == 2) { echo 'block'; } else { echo 'none'; } ?>">
			
			<div class="element">
			<label style="width:225px">&nbsp;</label>
				<div style="float:left;">
				<p>Wartość zamówienia netto: <strong><?php echo $netto; ?> zł</strong></p>
				</div>
				<div class="clear"></div>
			</div>
			
		</div>
		
		<div class="element">
		<label style="width:225px">&nbsp;</label>
		<p class="button"><a id="send_order_button" style="cursor:pointer">ZAMAWIAM PRODUKTY</a></p>
		<div class="clear"></div>
		</div>
	
		<div class="clear"></div>
	</div>
	
	</form>
	</div>
	<?php
}
else 
{
	?>
	<p>Aby zrealizować zamówienie musisz być zalogowany. Jeśli posiadasz już konto zaloguj się do systemu.</p>
	<?php
}
?>
</div>
<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<label style="width:120px;">&nbsp;</label>
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
	<div class="clear"></div>
</div>
<?php } ?>

<?php
$concession_id = (int)$_GET['id']; 

if (!isset($_POST['add_concession']) && $action != 'edit')
{
	$concessions->clear();
}
if (!isset($_POST['save_changes']) && $action == 'edit')
{
	$concessions->getConcession($concession_id);
}
?>

<form method="POST" action="">

<input type="hidden" name="concession_id" value="<?php echo $concession_id; ?>" />
	
	<?php  if ($_SESSION["errors"]['guardian_name']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['guardian_name']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Imię i nazwisko</label>
		<input type="text" name="name" value="<?php echo $concessions->textControll($_SESSION["values"]['name']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['email']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['email']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">E-mail</label>
		<input type="text" name="email" value="<?php echo $concessions->textControll($_SESSION["values"]['email']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['phone']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['phone']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Telefon</label>
		<input type="text" name="phone" value="<?php echo $concessions->textControll($_SESSION["values"]['phone']); ?>" />
		<div class="clear"></div>
	</div>
	
	<div class="form_element">
		<label style="width:120px;">&nbsp;</label>
		<input id="submit" name="save_changes" value="Zapisz zmiany" type="submit" />
		<div class="clear"></div>
	</div>
</form> 
<?php
session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	
<head>
<base href="<?php echo BASE_ADDRESS; ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="Opis" />
<link rel="stylesheet" href="css/login.css" type="text/css" />
		
<title>::admin.aeb</title>
</head>
		
<body>

<div id="main_content">
	<p id="logo">
	<a href="http://www.aeb.pl"><img src="images/logo.gif" alt="" /></a>
	</p>
	
	<div id="login_content">
		<div id="description_box">
			<p><img src="images/security.png" alt="" /></p>
			<div>
				<p>Witamy w aeb.cms</p>
				<p>Aby uzyskać dostęp do konsoli administracyjnej proszę wprowadź poprawne hasło i login i nacisnij Loguj.</p>
			</div>
		</div>
		<div id="form_content">
			<div id="form_background">
				<form action="./authentication/login" method="POST">
					<div class="form_element">
						<label>Login:</label>
						<input type="text" name="login" value="" />
						<div class="clear"></div>
					</div>
					<div class="form_element">
						<label>Hasło:</label>
						<input type="password" name="password" value="" />
					</div>
					<div class="form_element">
						<label>&nbsp;</label>
						<input class="submit" type="submit" name="log_in" value="Zaloguj" />
					</div>
				</form>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

</body>
</html>
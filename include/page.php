<?php
switch ($page_type)
{
	case 'kontakt': $page_id = 12; break;
	case 'profit': $page_id = 2; break;
	case 'gazetki': $page_id = 11; break;
	case 'cenniki': $page_id = 16; break;
}

$page = $pages->getPage($page_id);
?>
<div id="page_header" style="background:none;">
	<div class="details">
		<p><strong><?php echo $page['title']; ?></strong></p>
	</div>
	<div class="clear"></div>
</div>
	
<div id="page_content">
<?php 
if ($page_id == 12)
{
	?><div style="float:left; width:420px"><?php
}
echo str_replace(array('\"', "\'"), array('"', "'"), $page['content']);
if ($page_id == 12)
{
	?></div><div style="float:left;"><?php
	include(BASE_DIR.'contact_form.php');
	?></div><div class="clear"></div><?php
}
?>
</div>
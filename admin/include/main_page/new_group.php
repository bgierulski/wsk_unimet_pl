<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<label style="width:120px;">&nbsp;</label>
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
	<div class="clear"></div>
</div>
<?php } ?>

<?php
$group_id = (int)$_GET['id']; 

if (!isset($_POST['add_group']) && $action != 'edit')
{
	$main_page->clear();
}
if (!isset($_POST['save_changes']) && $action == 'edit')
{
	$main_page->getGroup($group_id);
}
?>

<form method="POST" action="" enctype="multipart/form-data">

	<?php if ($action == 'edit') { ?>
	
	<input type="hidden" name="group_id" value="<?php echo $group_id; ?>" />
	
	<div class="form_element">
		<input type="hidden" name="name" value="<?php echo $_SESSION["values"]['name']; ?>" />
		<label style="width:120px;">&nbsp;</label><?php echo $_SESSION["values"]['name']; ?>
		<div class="clear"></div>
	</div>
	
	<?php } else { ?>
	
	<?php if ($_SESSION["errors"]['group_id']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['group_id']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	
	<?php 
	$list = $main_page->getGroups();
	?>
	
	<div class="form_element">
		<label style="width:120px;">Grupa</label>
		<select name="group_id">
		<option value="0">-- wybierz grupę --</option>
		<?php 
		while($element = mysql_fetch_array($list)) 
   		{
       		?><option value="<?php echo $element['id']; ?>" <?php if ($element['id'] == $_SESSION["values"]['group_id']) echo 'selected="selected"'; ?>>
       		<?php 
       		switch ($element['level'])
       		{
       			case 3:
       				echo '&nbsp;&nbsp;&nbsp;&nbsp;|&#8212;';
       				break;
       				
       			case 2:
       				echo '|&#8212;';
       				break;
       				
       			default:
       				break;
       		}
       		echo $element['name']; 
       		?>
       		</option><?php
   		}
		?>
		</select>
		<div class="clear"></div>
	</div>
	
	<?php } ?>
	
	<?php if ($_SESSION["values"]['image']) { ?>
	<div class="form_element">
		<label style="width:120px;">&nbsp;</label>
		<img src="<?php echo BASE_ADDRESS; ?>../images/groups/<?php echo $_SESSION["values"]['image']; ?>" alt="" />
		<div class="clear"></div>
	</div>
	<?php } if ($_SESSION["errors"]['image']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['image']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Zdjęcie</label>
		<input style="width:200px;" name="image" type="file" />
		<div class="clear"></div>
	</div>
	
	<div class="form_element">
		<label style="width:120px;">&nbsp;</label>
		<?php if ($action == 'edit') { ?>
		<input id="submit" name="save_changes" value="Zapisz zmiany" type="submit" /> 
		<?php } else { ?>
		<input id="submit" name="add_group" value="Dodaj grupę" type="submit" /> 
		<?php } ?>
		<div class="clear"></div>
	</div>
</form> 
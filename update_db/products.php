<?php
$result = $db->query('SELECT change_id FROM `update`') or $db->raise_error();
$row = mysql_fetch_assoc($result);
$last_change_id = $row['change_id'];

$result = ibase_query('SELECT DISTINCT mat_art.ID, zz_uni_produkty_zmiany.ID as CHANGE_ID FROM zz_uni_produkty_zmiany JOIN mat_art 
ON zz_uni_produkty_zmiany.INDEKS = mat_art.INDEKS WHERE zz_uni_produkty_zmiany.TYP = \'OP\' AND mat_art.B2B = \'T\' AND mat_art.SEL 
= \'WZR\' AND mat_art.UKRYJ != \'T\' AND zz_uni_produkty_zmiany.ID >= '.$last_change_id.' ORDER BY zz_uni_produkty_zmiany.ID ASC');

$change_id = 0;
$ids = array();

while($row = ibase_fetch_assoc($result)) 
{
	$ids[] = $row['ID'];
	$change_id = $row['CHANGE_ID'];
}

// pobieram informacje dotyczace wersji jezykowych
$result = $db->query('SELECT id, version FROM versions') or $db->raise_error();

$versions = array();
	
while($row = mysql_fetch_array($result)) 
{
	$versions[$row['version']] = (int)$row['id'];
}

mysql_free_result($result);
	
// pobieram informacje dotyczoce produktow
$result = $db->query('SELECT sold_amount, id_hermes, upd_id FROM products') or $db->raise_error();
	
$products = array();
	
while($row = mysql_fetch_array($result)) 
{
	$products[$row['id_hermes']] = array($row['sold_amount'], $row['upd_id'], 0);
}

// daty dla nowosci
$result = $db->query('SELECT DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL 30 DAY), \'%Y%m%d\') as end') or $db->raise_error();
$dates = mysql_fetch_array($result);



$result = ibase_query('SELECT DISTINCT LOGO from mat_art WHERE B2B = \'T\' AND SEL = \'WZR\'');	

while($row = ibase_fetch_assoc($result)) 
{
	$logos_query .= '(\'\', \''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO']))).'\'),';
	$logos_counter++; 
}

if ($logos_counter)
{
	$db->query('TRUNCATE TABLE logo_groups') or $db->raise_error();
	$db->query(substr('INSERT INTO logo_groups VALUES '.$logos_query, 0, -1)) or $db->raise_error();
}

$result = $db->query('SELECT id, name FROM logo_groups') or $db->raise_error();
$logos = array();

while($row = mysql_fetch_assoc($result)) 
{
	$logos[$row['name']] = $row['id'];
}

$query = "SELECT mat_art.INDEKS, mat_art.B2B, mat_art.UPD_ID, mat_art.KONCESJA, mat_alt.INDEKS2,
(SELECT count(sel) FROM mat_art LEFT JOIN mat_cha ON mat_art.ID = mat_cha.POZ WHERE indeks = mat_alt.INDEKS2 AND sel = 'WZR' AND B2B = 'T') 
as KONTROLA, ARTYKUL, mat_art.INDEKS, mat_art.LOGO, mat_prd.OPIS, ID, PRODUCENT, mat_art.C_NETTO1, mat_art.C_NETTO2, mat_art.C_NETTO3, mat_art.C_NETTO4, 
mat_art.C_NETTO5, mat_art.C_NETTO6, JM, OPK, CENA_MIN, NA_ZAMOW, HTML, art_htm.STATUS, VAT, ILOSC, PRM_WART, PRM_TYP, PRM_OGOLNE, WAGA, 
KRESKI, JAKOSC_C, DATA_PZAK from mat_art LEFT JOIN mat_cha ON mat_art.ID = mat_cha.POZ LEFT JOIN mat_alt ON mat_art.INDEKS = mat_alt.INDEKS1 
LEFT JOIN art_htm ON mat_art.INDEKS = art_htm.INDEKS LEFT JOIN mat_prd ON mat_art.PRODUCENT = mat_prd.LOGO WHERE B2B = 'T' AND SEL = 'WZR' AND mat_art.UKRYJ != 'T'
union all 
SELECT mat_art.INDEKS, mat_art.B2B, mat_art.UPD_ID, mat_art.KONCESJA, mat_alt.INDEKS2, (SELECT count(sel) FROM mat_art LEFT JOIN mat_cha ON mat_art.ID = mat_cha.POZ WHERE indeks = mat_alt.INDEKS2 AND sel = 'WZR')
as KONTROLA, ARTYKUL, mat_art.INDEKS, mat_art.LOGO, mat_prd.OPIS, ID, PRODUCENT, mat_art.C_NETTO1, mat_art.C_NETTO2, mat_art.C_NETTO3, mat_art.C_NETTO4, 
mat_art.C_NETTO5, mat_art.C_NETTO6, JM, OPK, CENA_MIN, NA_ZAMOW, HTML, art_htm.STATUS, VAT, ILOSC, PRM_WART, PRM_TYP, PRM_OGOLNE, WAGA, 
KRESKI, JAKOSC_C, DATA_PZAK
FROM mat_rab JOIN mat_art ON mat_rab.LOGO = mat_art.INDEKS
LEFT JOIN mat_cha ON mat_art.ID = mat_cha.POZ
LEFT JOIN mat_alt ON mat_art.INDEKS = mat_alt.INDEKS1 
LEFT JOIN art_htm ON mat_art.INDEKS = art_htm.INDEKS
LEFT JOIN mat_prd ON mat_art.PRODUCENT = mat_prd.LOGO
WHERE SEL = 'WZR' AND mat_art.UKRYJ != 'T' AND mat_rab.kon = 8338 AND mat_rab.rodzajlogo = 1 AND mat_art.SEL = 'WZR' AND B2B != 'T'";

$result = ibase_query($query);	
	
$signs = array('&nbsp;', '<br />', '<p>', '</p>');

$elements = array();

$db->query('TRUNCATE TABLE product_replacements') or $db->raise_error();

while($row = ibase_fetch_assoc($result)) 
{
	$ibase_producer = iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['PRODUCENT'])));
	$ibase_logo = iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO'])));
	$ibase_opis = iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['OPIS'])));
	
	$unit = trim(iconv("windows-1250", "UTF-8", mysql_escape_string($row['JM'])));
	
	if ($row['KONTROLA'] > 0)
	{
		$replacements_query .= '(\'\', '.(int)$row['ID'].', \''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['INDEKS2']))).'\'),';
		$replacements_counter++;
	}
	
	if (!in_array((int)$row['ID'], $elements))
	{
		$netto1 = (float)mysql_escape_string(str_replace(',', '.', $row['C_NETTO1']));
		$netto2 = (float)mysql_escape_string(str_replace(',', '.', $row['C_NETTO2']));
		$netto3 = (float)mysql_escape_string(str_replace(',', '.', $row['C_NETTO3']));
		$netto4 = (float)mysql_escape_string(str_replace(',', '.', $row['C_NETTO4']));
		$netto5 = (float)mysql_escape_string(str_replace(',', '.', $row['C_NETTO5']));
		$netto6 = (float)mysql_escape_string(str_replace(',', '.', $row['C_NETTO6']));
		$cena_min = (float)mysql_escape_string(str_replace(',', '.', $row['CENA_MIN']));
		
		if (!is_array($products[(int)$row['ID']])) // nowy produkt
		{
			$products_query = '(\'\', '.(int)$row['ID'].', '.(($row['B2B'] == 'T') ? 1 : 0).', 0, \''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['INDEKS']))).'\', ';
			$products_query .= (($ibase_producer[strlen($ibase_producer)-1] == '*' && $ibase_opis != '') ? $producer_ids[$ibase_opis] : $producer_ids[$default_producer]).', ';
			$products_query .= (int)$logos[iconv("windows-1250", "UTF-8", mysql_escape_string($row['LOGO']))].', \''.$unit.'\', \''.(($unit == 'STO') ? (((int)$row['OPK']) ? (int)$row['OPK'] : 1) : 0).'\', '.(($row['NA_ZAMOW'] == 'T') ? 1 : 0).', ';
			$products_query .= (int)$row['VAT'].', '.(int)$row['ILOSC'].', \''.(int)$products[$row['ID']][0].'\', '.(int)$row['PRM_WART'].', '.(int)$row['PRM_TYP'].', ';
			$products_query .= (int)$row['PRM_OGOLNE'].', '.(($row['HIT_C'] == 'T') ? 1 : 0).', \''.mysql_escape_string($row['WAGA']).'\', ';
			$products_query .= (($row['PALETOWY_C'] == 'T') ? 1 : 0).', \''.(int)trim($row['KRESKI']).'\', '.(int)$row['JAKOSC_C'].', \''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO']))).'\', '.(int)$concession_ids[iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['KONCESJA'])))].', '.(int)$row['UPD_ID'].'),';
				
			$product_versions_query = '('.(int)$row['ID'].', '.$versions['pl'].', \''.iconv("windows-1250", "UTF-8", mysql_escape_string($row['ARTYKUL'])).'\', ';
				
			if ($row['STATUS'] == 'STD') 
			{
				$blob_id = $row['HTML']; 
				$blob_data = ibase_blob_info($blob_id);
		    	$blob_handler = ibase_blob_open($blob_id);
		    	$description = iconv("windows-1250", "UTF-8", str_replace(array('&nbsp;', '&amp;'), array(' ', '&'), ibase_blob_get($blob_handler, $blob_data[0])));
		    	ibase_blob_close($blob_handler);	
		    	$product_versions_query .= '\''.mysql_escape_string($description).'\'),';
			}
			else
			{
				$product_versions_query .= '\'\'),';
			}
				
			$product_details_query = '(\'\', '.(int)$row['ID'].', \''.$netto1.'\', \''.$netto2.'\', \''.$netto3.'\', ';
			$product_details_query .= '\''.$netto4.'\', \''.$netto5.'\', \''.$netto6.'\', \''.$cena_min.'\', 1),';
					
			$actual_date = date('Y-m-d H:i:s');
					
			if (diff_date(date('Y-m-d H:i:s', strtotime($row['DATA_PZAK'])), $actual_date) < 30)
			{
				$rows = $db->query('SELECT product_id FROM newses WHERE product_id = '.(int)$row['ID']) or $db->raise_error();
						
				if (!$db->num_rows($rows))
				{
					$newses_query .= '('.(int)$row['ID'].', \''.$dates['end'].'\'),';
					$newses_counter++;
				}
			}
			
			$db->query(substr('INSERT INTO products VALUES '.$products_query, 0, -1)) or $db->raise_error();
			$db->query(substr('INSERT INTO product_versions VALUES '.$product_versions_query, 0, -1)) or $db->raise_error();
			$db->query(substr('INSERT INTO product_details VALUES '.$product_details_query, 0, -1)) or $db->raise_error();
			
			// operacje na drzewach indywidualnych
			$producer_name = (($ibase_producer[strlen($ibase_producer)-1] == '*' && $ibase_opis != '') ? $ibase_opis : $default_producer);
			
			$data = $db->query('SELECT user_id, what_with_new FROM groups_tree_user WHERE individual = 1') or $db->raise_error();
		
			if (mysql_num_rows($data))
			{
				$group_details = explode('-', iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO']))));
				$group_comments = array();
				
				if (is_array($group_details))
				{
					while($element = mysql_fetch_array($data)) 
		   			{
		   				$query = 'INSERT INTO products_status (`user`, `id`, `subgroup`, `producers`, `status`) VALUES ('.$element['user_id'].', 
		   				'.(int)$row['ID'].', \''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO']))).'\', 
		   				\''.mysql_escape_string($producer_name).'\', '.$element['what_with_new'].') ON DUPLICATE KEY UPDATE status = '.$element['what_with_new'];
		   				
		   				$db->query($query) or $db->raise_error();
		   			}
				}
			}
			
			mysql_free_result($data);
			// koniec operacji na drzewach indywidualnych
		}
		else if ((int)$row['UPD_ID'] > (int)$products[(int)$row['ID']][1] || in_array((int)$row['ID'], $ids)) // uaktualniony produkt
		{
			$products_query = 'UPDATE products SET b2b = '.(($row['B2B'] == 'T') ? 1 : 0).', symbol = \''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['INDEKS']))).'\', 
			producer_id = '.(($ibase_producer[strlen($ibase_producer)-1] == '*' && $ibase_opis != '') ? $producer_ids[$ibase_opis] : $producer_ids[$default_producer]).', 
			logo_group_id = '.(int)$logos[iconv("windows-1250", "UTF-8", mysql_escape_string($row['LOGO']))].', unit = \''.$unit.'\', unit_detail = 
			\''.(($unit == 'STO') ? (((int)$row['OPK']) ? (int)$row['OPK'] : 1) : 0).'\', on_order = '.(($row['NA_ZAMOW'] == 'T') ? 1 : 0).', 
			tax = '.(int)$row['VAT'].', amount = '.(int)$row['ILOSC'].', sold_amount = '.(int)$products[$row['ID']][0].', prm_wart = '.(int)$row['PRM_WART'].', 
			prm_typ = '.(int)$row['PRM_TYP'].', prm_ogolne = '.(int)$row['PRM_OGOLNE'].', hit = '.(($row['HIT_C'] == 'T') ? 1 : 0).', weight = 
			\''.mysql_escape_string($row['WAGA']).'\', pallet = '.(($row['PALETOWY_C'] == 'T') ? 1 : 0).', ean = \''.trim($row['KRESKI']).'\', 
			quality = '.(int)$row['JAKOSC_C'].', comment = \''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO']))).'\', 
			concession_id = '.(int)$concession_ids[iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['KONCESJA'])))].', 
			upd_id = '.(int)$row['UPD_ID'].' WHERE id_hermes = '.(int)$row['ID'];
			
			$product_versions_query = 'UPDATE product_versions SET name = \''.iconv("windows-1250", "UTF-8", mysql_escape_string($row['ARTYKUL'])).'\' ';
			
			if ($row['STATUS'] == 'STD') 
			{
				$blob_id = $row['HTML']; 
				$blob_data = ibase_blob_info($blob_id);
		    	$blob_handler = ibase_blob_open($blob_id);
		    	$description = iconv("windows-1250", "UTF-8", str_replace(array('&nbsp;', '&amp;'), array(' ', '&'), ibase_blob_get($blob_handler, $blob_data[0])));
		    	ibase_blob_close($blob_handler);	
		    	$product_versions_query .= ', description = \''.mysql_escape_string($description).'\' ';
			}

			$product_versions_query .= 'WHERE version_id = '.$versions['pl'].' AND product_id = '.(int)$row['ID'];
			
			$product_details_query = 'UPDATE product_details SET netto_1 = \''.$netto1.'\', netto_2 = \''.$netto2.'\', netto_3 = \''.$netto3.'\', netto_4 = 
			\''.$netto4.'\', netto_5 = \''.$netto5.'\', netto_6 = \''.$netto6.'\', netto_minium = \''.$cena_min.'\' WHERE product_id = '.(int)$row['ID'];
			
			$db->query($products_query) or $db->raise_error();
			$db->query($product_versions_query) or $db->raise_error();
			$db->query($product_details_query) or $db->raise_error();
			
			// operacje na drzewach indywidualnych
			$producer_name = (($ibase_producer[strlen($ibase_producer)-1] == '*' && $ibase_opis != '') ? $ibase_opis : $default_producer);
			
			$db->query('UPDATE products_status SET subgroup = \''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO']))).'\', producers = \''.mysql_escape_string($producer_name).'\' WHERE id = '.(int)$row['ID']) or $db->raise_error();
			// koniec operacji na drzewach indywidualnych
		}
		
		$products[(int)$row['ID']][2] = 1;
	}
	
	array_push($elements, (int)$row['ID']);
}

// usuwanie produktów, które zostały wycofane ze sprzedarzy
if (is_array($products))
{
	foreach ($products as $key => $product)
	{
		if ($product[2] == 0)
		{
			$db->query('DELETE FROM products WHERE id_hermes = '.(int)$key) or $db->raise_error();
			$db->query('DELETE FROM product_details WHERE product_id = '.(int)$key) or $db->raise_error();
			$db->query('DELETE FROM product_versions WHERE product_id = '.(int)$key) or $db->raise_error();
			$db->query('DELETE FROM product_files WHERE product_id = '.(int)$key) or $db->raise_error();
			$db->query('DELETE FROM product_galleries WHERE product_id = '.(int)$key) or $db->raise_error();
			$db->query('DELETE FROM product_replacements WHERE product_id = '.(int)$key) or $db->raise_error();
			
			// operacje na drzewach indywidualnych
			$db->query('DELETE FROM products_status WHERE id = '.(int)$key) or $db->raise_error();
			// koniec operacji na drzewach indywidualnych
		}
	}
}

if ($newses_counter)
{
	$db->query(substr('INSERT INTO newses VALUES '.$newses_query, 0, -1)) or $db->raise_error();
}
	
if ($replacements_counter)
{
	$db->query(substr('INSERT INTO product_replacements VALUES '.$replacements_query, 0, -1)) or $db->raise_error();
}

$query = "SELECT DISTINCT mat_art.INDEKS, mat_art.B2B, mat_art.UPD_ID, mat_art.KONCESJA, mat_alt.INDEKS2, (SELECT count(sel) FROM mat_art LEFT JOIN mat_cha ON mat_art.ID = mat_cha.POZ WHERE indeks = mat_alt.INDEKS2 AND sel = 'WZR')
as KONTROLA, ARTYKUL, mat_art.INDEKS, mat_art.LOGO, mat_prd.OPIS, ID, PRODUCENT, mat_art.C_NETTO1, mat_art.C_NETTO2, mat_art.C_NETTO3, mat_art.C_NETTO4, 
mat_art.C_NETTO5, mat_art.C_NETTO6, JM, OPK, CENA_MIN, NA_ZAMOW, HTML, art_htm.STATUS, VAT, ILOSC, PRM_WART, PRM_TYP, PRM_OGOLNE, WAGA, 
KRESKI, JAKOSC_C, DATA_PZAK
FROM mat_rab JOIN mat_art ON mat_rab.LOGO = mat_art.INDEKS
LEFT JOIN mat_cha ON mat_art.ID = mat_cha.POZ
LEFT JOIN mat_alt ON mat_art.INDEKS = mat_alt.INDEKS1 
LEFT JOIN art_htm ON mat_art.INDEKS = art_htm.INDEKS
LEFT JOIN mat_prd ON mat_art.PRODUCENT = mat_prd.LOGO
WHERE SEL = 'WZR' AND mat_art.UKRYJ != 'T' AND mat_rab.kon = 8338 AND mat_rab.rodzajlogo = 1 AND mat_art.SEL = 'WZR'";

$counter = 0;

$result = ibase_query($query);

$query = "DELETE products_status FROM products_status INNER JOIN users ON products_status.user = users.id AND users.id_hermes = 8338";
$db->query($query) or $db->raise_error();

$query = "SELECT id FROM users WHERE id_hermes = parent_id AND parent_id = 8338"; 
$data = $db->query($query) or $db->raise_error();

$user_ids = array();

while($row = mysql_fetch_assoc($data)) 
{
	$user_ids[] = $row['id'];
}

$product_ids = array();

while($row = ibase_fetch_assoc($result)) 
{
	if (!in_array((int)$row['ID'], $product_ids))
	{
		foreach ((array)$user_ids as $user_id)
		{
			$query = "INSERT INTO `products_status` (`user`, `id`, `subgroup`, `producers`, `status`)
			SELECT " . $user_id . " `user`, `p`.`id_hermes`, `gt`.`comment`, `pr`.`name`, 1 `status`
			FROM `products` `p` JOIN `groups_tree` `gt` ON `p`.`subgroup_id` = `gt`.`id` 
			JOIN `producers` `pr` ON `p`.`producer_id` = `pr`.`id` WHERE `p`.`id_hermes` = " . (int)$row['ID'];

			$db->query($query) or $db->raise_error();
		}
	}

	$product_ids[] = (int)$row['ID'];
}

$db->query('UPDATE products JOIN groups_tree ON groups_tree.comment = products.comment SET products.subgroup_id = groups_tree.id') or $db->raise_error();
$db->query('UPDATE `update` SET change_id = '.$change_id) or $db->raise_error();
?>
<?php
if (isset($_POST['budget_id']))
{
	$_SESSION['budget_id'] = (int)$_POST['budget_id'];
}
else if (!isset($_SESSION['budget_id']))
{
	$_SESSION['budget_id'] = 1;
}

$list = $orders->getBudgetDetails($_SESSION['budget_id']);
$entries_count = count($list);

$on_page = 50;
	
$pages_count = ceil($entries_count/$on_page);
	
if ((int)$_GET['id'] > 0 && (int)$_GET['id'] <= $pages_count)
{
	$page_number = (int)$_GET['id'];
}
else 
{
	$page_number = 1;
}
	
$first_entry = $page_number * $on_page - $on_page;
$last_entry = $page_number * $on_page - 1;

if ("178.219.17.138" == $_SERVER['REMOTE_ADDR'])
{
	$orders->budgetChanges();
}
?>

<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
</div>
<?php } else if (isset($_SESSION["communicats"]['ok'])) { ?>
<div class="form_element ok">
	<?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?>
</div>
<?php } ?>

<?php
$budgets_list = $orders->getBudgets();
?>

<div>
	<div style="float:left;">
		<form method="post" action="" name="budget">
		<select name="budget_id" style="position:relative; top:3px;" onchange="javascript: document.budget.submit();">
		<?php
		if (mysql_num_rows($budgets_list))
		{
			while($element = mysql_fetch_array($budgets_list)) 
	   		{
	       			?><option value="<?php echo $element['id']; ?>" <?php if ($_SESSION['budget_id'] == $element['id']) { echo 'selected="selected"'; } ?>><?php echo $element['department']; ?></option><?php
	   		}
		}
		?>
		</select>
		</form>
	</div>
	<div style="float:right;">
		<?php 
		if ($pages_count > 1)
		{
			?>
			<div id="subpages" style="text-align:right;">
			<?php 
			for ($i=1; $i<=$pages_count; $i++)
			{
				if ($page_number == $i) { ?><strong><?php }
				?><a href="./orders/budgets/<?php echo $i; ?>" style="color:#1178f3; text-decoration:none;"><?php echo $i; ?></a> <?php
				if ($page_number == $i) { ?></strong><?php }
			
				if ($i < $pages_count)
				{
					?><strong style="color:#ccccff;">|</strong> <?php
				}
			}
			?>
			</div>
			<?php
		}
		?>
	</div>
	<div class="clear"></div>
</div>

<table class="orders_list" style="padding:10px 0px 10px 0px;">
<tr>
	<th>Budżet</th>
	<th>Data</th>
	<th>Informacja</th>
	<th>Imię i nazwisko</th>
	<th>Wartość początkowa</th>
	<th>Zmiana</th>
	<th>Wartość końcowa</th>
</tr>
<?php
if (is_array($list) && !empty($list))
{
	foreach ($list as $key => $row)
	{
		if ($key >= $first_entry && $key <= $last_entry)
		{
			?>
			<tr>
				<td class="bg_<?php echo ($key%2)+1; ?>" style="width:9%;"><?php echo $row['year']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>" style="width:9%;"><?php echo date('Y-m-d H:i', strtotime($row['date'])); ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>" style="width:5%;"><?php echo $row['info']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>" style="width:9%;"><?php echo $row['user_name'].' '.$row['user_surname']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>" style="width:9%;"><?php echo $row['old_value']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>" style="width:8%;"><?php echo (($row['amount'] > 0) ? "+" : "").$row['amount']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>" style="width:8%;"><?php echo $row['new_value']; ?></td>
			</tr>
			<?php
		}
	}
}
?>
</table>
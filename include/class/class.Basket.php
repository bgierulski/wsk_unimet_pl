<?php
class Basket extends Validators
{
	private $_db;

	private $_product = array();
	private $_basket_products;
	private $_counter; // licznik produktów w koszyku
	private $_user;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		
		$this->_basket_products = $_SESSION["basket_products"]; // zawartoąść koszyka
		$this->_counter = count($this->_basket_products); // ilość typów produktów w koszyku
		$this->_user = $this->getSession("b2b_user");
		
		if (true !== $_SESSION["b2b_full_offer"])
		{
			if ($_POST['operation'] == 'to_basket')
			{					
				if (!isset($_POST["counts"]))
				{
					$this->_getProduct((int)$_POST['product_id'], (float)str_replace(',', '.', $_POST['count']));
				}
				else 
				{
					$actives = $_POST["actives"];
					$counts = $_POST["counts"];
					
					if (is_array($actives))
					{
						foreach ($actives as $product_id)
						{
							$this->_getProduct($product_id, (float)str_replace(',', '.', $counts[$product_id]));
						}
					}
				}
			}
			else if ($_POST['operation'] == 'delete_favorite')
			{
				$this->_deleteFavorites($_POST["actives"]);
			}
		}
		
		if (is_array($this->_basket_products) && !empty($this->_basket_products))
		{
			$this->_saveBasket();
		}
	}
	
	private function _deleteFavorites($ids)
	{
		$user = $this->getSession('b2b_user');
		
		if (is_array($ids) && !empty($ids))
		{
			$query = 'DELETE FROM favorite_products WHERE user_id = '.$user['id'].' AND product_id IN (0';
			
			foreach ($ids as $id)
			{
				$query .= ','.(int)$id;
			}
			
			$query .= ')';
			
			$this->_db->query($query) or $this->_db->raise_error();
		}
	}
	
	private function _saveBasket()
	{
		if (isset($_COOKIE["b2b_basket_key"]))
		{
			$key = $_COOKIE["b2b_basket_key"];
			
			if (strlen($key) == 20)
			{
				$this->_db->query('DELETE FROM basket WHERE `key` = \''.mysql_real_escape_string($key).'\'') or $this->_db->raise_error();
				
				foreach ($this->_basket_products as $product)
				{
					$query = 'INSERT INTO basket VALUES (\'\', \''.mysql_real_escape_string($key).'\', '.$product['product_id'].', \''.mysql_real_escape_string($product['subgroup']).'\', 
					\''.mysql_real_escape_string($product['name']).'\', \''.$product['index'].'\', '.$product['tax'].', \''.str_replace(',', '.', $product['weight']).'\', 
					\''.$product['size'].'\', \''.$product['unit'].'\', \''.str_replace(',', '.', $product['count']).'\', 
					\''.str_replace(',', '.', $product['netto_price']).'\', \''.str_replace(',', '.', $product['netto_value']).'\', 
					\''.str_replace(',', '.', $product['brutto_price']).'\', \''.str_replace(',', '.', $product['brutto_value']).'\', \''.mysql_real_escape_string($product['mpk']).'\', 
					\''.mysql_real_escape_string($product['info']).'\', '.(int)$product['query_id'].')';
					
					$this->_db->query($query) or $this->_db->raise_error();
				}
			}
		}
	}
	
	private function _getProduct($product_id, $count)
	{
		// wybór informacji o produkcie z bazy danych
		$result = $this->_db->query('SELECT groups_tree.name as subgroup_name, producers.name as producer_name, product_versions.name, symbol, unit, tax, 
		weight, pallet FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id JOIN groups_tree ON products.subgroup_id = 
		groups_tree.id JOIN producers ON products.producer_id = producers.id WHERE id_hermes = '.$product_id) or $this->_db->raise_error();
			
		$product = mysql_fetch_array($result);
		
		$this->_product['product_id'] = $product_id;
		$this->_product['subgroup'] = $product['subgroup_name'];
		$this->_product['producer'] = $product['producer_name'];
		$this->_product['name'] = $product['name'];
		$this->_product['index'] = $product['symbol'];
		$this->_product['tax'] = $product['tax'];
		$this->_product['weight'] = $product['weight'];
		$this->_product['size'] = (($product['pallet'] == 1) ? 'T' : 'N');
		$this->_product['unit'] = strtolower($product['unit']);
		$this->_product['count'] = $count;
			
		$prices = new Prices();
			
		$this->_product['netto_price'] = $prices->getPrice($product_id, 4);
		$this->_product['netto_value'] = sprintf("%0.2f", $this->_product['netto_price']*$this->_product['count']);
		$this->_product['brutto_price'] = $prices->getPrice($product_id, 1);
		$this->_product['brutto_value'] = sprintf("%0.2f", $this->_product['brutto_price']*$this->_product['count']);
		$this->_product['mpk'] = "";
		$this->_product['info'] = "";
		$this->_product['query_id'] = 0;
			
		$this->_addProduct();
	}
	
	public function _addProduct()
	{ 
		// dodanie produktu do koszyka
		if (!empty($this->_product)) 
		{
			if ($this->_basket_products)
			{
				foreach ($this->_basket_products as $key => $product)
				{
					// jeśli taki produkt jest już dodany do koszyka
					if ($product['product_id'] == $this->_product['product_id'])
					{						
						$this->_basket_products[$key]['count'] += $this->_product['count']; 
						$this->_basket_products[$key]['brutto_value'] = sprintf("%0.2f", $this->_basket_products[$key]['brutto_price'] * $this->_basket_products[$key]['count']);
						
						$this->setSession("basket_products", $this->_basket_products);
						return;
					}
				}
			}
			
			$this->_basket_products[$this->_counter] = $this->_product;
			$this->_counter = count($this->_basket_products); // ilość typów produktów w koszyku
			$this->setSession("wsk_order_change", $this->_product['product_id']);
			
			$this->setSession("basket_products", $this->_basket_products);
		}
	}
	
	public function changeCount($product_id, $change)
	{
		foreach ($this->_basket_products as $key => $product)
		{
			if ($product['product_id'] == $product_id && ( $product['count'] + $change) > 0)
			{
				$this->_basket_products[$key]['count'] = $product['count'] + ($change);
				$this->_basket_products[$key]['netto_value'] = sprintf("%0.2f", $product['netto_value'] + ($change * $product['netto_price']));
				$this->_basket_products[$key]['brutto_value'] = sprintf("%0.2f", $product['brutto_value'] + ($change * $product['brutto_price']));
			}
		}
		
		$this->setSession("basket_products", $this->_basket_products);
	}
	
	public function deleteProducts($product_id) // usunięcie produktu z koszyka
	{
		foreach ($this->_basket_products as $key => $product)
		{
			if ($product['product_id'] == $product_id)
			{
				unset($this->_basket_products[$key]);
			}
		}
		
		$this->setSession("basket_products", $this->_basket_products);
	}
	
	public function getBruttoValue()
	{
		$price = 0;
		
		if ($this->_basket_products)
		{
			foreach ($this->_basket_products as $product)
			{
				$price += $product['brutto_value'];
			}
		}
		
		return sprintf("%0.2f", $price);
	}
	
	public function getNettoValue()
	{
		$price = 0;
		
		if ($this->_basket_products)
		{
			foreach ($this->_basket_products as $product)
			{
				$price += $product['netto_value'];
			}
		}
		
		return sprintf("%0.2f", $price);
	}

	public function getWeight()
	{
		if ($this->_basket_products)
		{
			foreach ($this->_basket_products as $product)
			{
				$weight += $product['count'] * $product['weight'];
			}
		}
		
		return sprintf("%0.2f", $weight);
	}
	
	public function checkSize()
	{
		$pallets = 0;
		
		if ($this->_basket_products)
		{
			foreach ($this->_basket_products as $product)
			{
				if ($product['size'] == 'T')
				{
					$pallets ++;
				}
			}
		}
		
		return $pallets;
	}
	
	public function clearBasket($key)
	{
		$this->_db->query('DELETE FROM basket WHERE `key` = \''.mysql_real_escape_string($key).'\'') or $this->_db->raise_error();
	}
	
	public function getProducts() // pobranie zawartoĹci koszyka
	{
		return $this->_basket_products;
	}
	
	public function setMPK($value, $key)
	{
		$this->_basket_products = $this->getSession("basket_products");
		
		if (is_array($this->_basket_products))
		{
			$this->_basket_products[$key]['mpk'] = $value;
		}
		
		$this->setSession("basket_products", $this->_basket_products);
	}
	
	public function getMpkPaths($mpks = array())
	{
		if (is_array($this->_basket_products) && empty($mpks))
		{
			$mpks = array();		
	
			foreach ($this->_basket_products as $product)
			{
				if (!in_array($product['mpk'], $mpks))
				{
					array_push($mpks, $product['mpk']);
				}
			}
		}
		
		if (is_array($mpks) && !empty($mpks))
		{
			$data = array();
			
			foreach ($mpks as $mpk)
			{
				$query = 'SELECT id, budget FROM wsk_mpk_matrix_main WHERE active = 1 AND mpk LIKE \''.mysql_real_escape_string($mpk).'\'';
				$result = $this->_db->query($query) or $this->_db->raise_error();
				
				if (mysql_num_rows($result))
				{
					$row = mysql_fetch_array($result);
					
					if (!isset($data[$mpk]))
   					{
	   					$data[$mpk] = array(
							"mpk" => $mpk,
							"budget" => $row['budget'], 
							"analyst" => array(), 
							"manager" => array(),
							"receiving" => array()
						);
   					}
					
					$query = 'SELECT analyst as id FROM wsk_mpk_matrix_analyst WHERE active = 1 AND id = '.$row['id'];
					$result = $this->_db->query($query) or $this->_db->raise_error();
					
					while($analyst = mysql_fetch_array($result)) 
   					{
   						if (!in_array($analyst['id'], $data[$mpk]["analyst"]))
   						{
   							array_push($data[$mpk]["analyst"], $analyst['id']);
   						}
   					}
   					
   					mysql_free_result($result);
   					$analysts = $data[$mpk]["analyst"];
   					
   					foreach ($analysts as $analyst_id)
   					{
   						$query = 'SELECT user_deputy as id FROM wsk_mpk_matrix_deputys WHERE user_id = '.$analyst_id.' AND mpk_id = '.$row['id'].' AND position = 1';
   						$result = $this->_db->query($query) or $this->_db->raise_error();
   						
	   					while($analyst = mysql_fetch_array($result)) 
	   					{
	   						if (!in_array($analyst['id'], $data[$mpk]["analyst"]))
	   						{
	   							array_push($data[$mpk]["analyst"], $analyst['id']);
	   						}
	   					}
   					}
   					
					$query = 'SELECT manager as id FROM wsk_mpk_matrix_manager WHERE active = 1 AND id = '.$row['id'];
					$result = $this->_db->query($query) or $this->_db->raise_error();
					
					while($manager = mysql_fetch_array($result)) 
   					{
   						if (!in_array($manager['id'], $data[$mpk]["manager"]))
   						{
   							array_push($data[$mpk]["manager"], $manager['id']);
   						}
   					}
   					
   					mysql_free_result($result);
   					$managers = $data[$mpk]["manager"];
   					
   					foreach ($managers as $manager_id)
   					{
   						$query = 'SELECT user_deputy as id FROM wsk_mpk_matrix_deputys WHERE user_id = '.$manager_id.' AND mpk_id = '.$row['id'].' AND position = 2';
   						$result = $this->_db->query($query) or $this->_db->raise_error();
   						
	   					while($manager = mysql_fetch_array($result)) 
	   					{
	   						if (!in_array($manager['id'], $data[$mpk]["manager"]))
	   						{
	   							array_push($data[$mpk]["manager"], $manager['id']);
	   						}
	   					}
   					}
   					
					$query = 'SELECT receiving as id FROM wsk_mpk_matrix_receiving WHERE active = 1 AND id = '.$row['id'];
					$result = $this->_db->query($query) or $this->_db->raise_error();
					
					while($receiving = mysql_fetch_array($result)) 
   					{
   						if (!in_array($receiving['id'], $data[$mpk]["receiving"]))
   						{
   							array_push($data[$mpk]["receiving"], $receiving['id']);
   						}
   					}
   					
   					mysql_free_result($result);
   					$receivings = $data[$mpk]["receiving"];
   					
   					foreach ($receivings as $receiving_id)
   					{
   						$query = 'SELECT user_deputy as id FROM wsk_mpk_matrix_deputys WHERE user_id = '.$receiving_id.' AND mpk_id = '.$row['id'].' AND position = 3';
   						$result = $this->_db->query($query) or $this->_db->raise_error();
   						
	   					while($receiving = mysql_fetch_array($result)) 
	   					{
	   						if (!in_array($receiving['id'], $data[$mpk]["receiving"]))
	   						{
	   							array_push($data[$mpk]["receiving"], $receiving['id']);
	   						}
	   					}
   					}
				}
				else
				{
					$data[$mpk] = array(
						"mpk" => $mpk,
						"budget" => 0, 
						"analyst" => array(), 
						"manager" => array(),
						"receiving" => array()
					);
				}
				
				/*
				$query = 'SELECT IFNULL(budget,0) as budget, IFNULL(contractor,0) as contractor, IFNULL(analyst1,0) as analyst1, IFNULL(analyst2,0) as 
				analyst2, IFNULL(manager1,0) as manager1, IFNULL(manager2,0) as manager2, IFNULL(receiving1,0) as receiving1, IFNULL(receiving2,0) as 
				receiving2 FROM wsk_mpk_matrix WHERE mpk LIKE \''.mysql_real_escape_string($mpk).'\' AND contractor = '.$this->_user['id'];
				
				$result = $this->_db->query($query) or $this->_db->raise_error();
				
				if (!mysql_num_rows($result))
				{
					mysql_free_result($result);
					
					$query = 'SELECT IFNULL(budget,0) as budget, IFNULL(contractor,0) as contractor, IFNULL(analyst1,0) as analyst1, IFNULL(analyst2,0) as 
					analyst2, IFNULL(manager1,0) as manager1, IFNULL(manager2,0) as manager2, IFNULL(receiving1,0) as receiving1, IFNULL(receiving2,0) as 
					receiving2 FROM wsk_mpk_matrix WHERE mpk LIKE \''.mysql_real_escape_string($mpk).'\'';
					
					$result = $this->_db->query($query) or $this->_db->raise_error();
				}
			
				if (mysql_num_rows($result))
				{
					while($row = mysql_fetch_array($result)) 
   					{
   						if (!isset($data[$mpk]))
   						{
	   						$data[$mpk] = array(
								"mpk" => $mpk,
								"budget" => $row['budget'], 
								"analyst" => array(), 
								"manager" => array(),
								"receiving" => array()
							);
   						}
   						
   						if ($row['analyst1'] > 0 && !in_array($row['analyst1'], $data[$mpk]["analyst"]))
   						{
   							array_push($data[$mpk]["analyst"], $row['analyst1']);
   						}
   						
   						if ($row['analyst2'] > 0 && !in_array($row['analyst2'], $data[$mpk]["analyst"]))
   						{
   							array_push($data[$mpk]["analyst"], $row['analyst2']);
   						}
   						
   						if ($row['manager1'] > 0 && !in_array($row['manager1'], $data[$mpk]["manager"]))
   						{
   							array_push($data[$mpk]["manager"], $row['manager1']);
   						}
   						
   						if ($row['manager2'] > 0 && !in_array($row['manager2'], $data[$mpk]["manager"]))
   						{
   							array_push($data[$mpk]["manager"], $row['manager2']);
   						}
   						
   						if ($row['receiving1'] > 0 && !in_array($row['receiving1'], $data[$mpk]["receiving"]))
   						{
   							array_push($data[$mpk]["receiving"], $row['receiving1']);
   						}
   						
   						if ($row['receiving2'] > 0 && !in_array($row['receiving2'], $data[$mpk]["receiving"]))
   						{
   							array_push($data[$mpk]["receiving"], $row['receiving2']);
   						}
   					}
				}
				else
				{
					$data[$mpk] = array(
						"mpk" => $mpk,
						"budget" => 0, 
						"analyst" => array(), 
						"manager" => array(),
						"receiving" => array()
					);
				}
				
				mysql_free_result($result);
				*/
			}
		}

		return $data;
	}
	
	public function savePaths()
	{
		$this->clear();
		$controll = true;
		$paths = array();
		
		foreach ($_POST["mpk"] as $key => $mpk)
		{
			if (empty($mpk) || (int)$_POST["budget"][$key] == 0 || (int)$_POST["analyst"][$key] == 0 || (int)$_POST["manager"][$key] == 0 || (int)$_POST["receiving"][$key] == 0)
			{
				$controll = false;
			}
			
			$values["budget"][] = (int)$_POST["budget"][$key];
			$values["analyst"][] = (int)$_POST["analyst"][$key];
			$values["manager"][] = (int)$_POST["manager"][$key];
			$values["receiving"][] = (int)$_POST["receiving"][$key];
		}

		if (true == $controll)
		{
			foreach ($_POST["mpk"] as $key => $mpk)
			{
				$result = $this->_db->query('SELECT id FROM wsk_mpk_matrix_main WHERE mpk LIKE \''.mysql_real_escape_string($mpk).'\'') or $this->_db->raise_error();
					
				if (mysql_num_rows($result))
				{
					$row = mysql_fetch_array($result);
					$mpk_id = $row['id'];
				}
				else
				{
					$this->_db->query('INSERT INTO wsk_mpk_matrix_main VALUES (\'\', \''.mysql_real_escape_string($mpk).'\', '.(int)$_POST["budget"][$key].', 0)') or $this->_db->raise_error();
					$mpk_id = mysql_insert_id();
				}
					
				mysql_free_result($result);
					
				$result = $this->_db->query('SELECT ida FROM wsk_mpk_matrix_contractor WHERE contractor = '.$this->_user['id'].' AND id = '.$mpk_id) or $this->_db->raise_error();
					
				if (!mysql_num_rows($result))
				{
					$this->_db->query('INSERT INTO wsk_mpk_matrix_contractor VALUES ('.$mpk_id.', '.$this->_user['id'].', 0, \'\')') or $this->_db->raise_error();
				}
					
				mysql_free_result($result);
					
				$result = $this->_db->query('SELECT analyst FROM wsk_mpk_matrix_analyst WHERE analyst = '.(int)$_POST["analyst"][$key].' AND id = '.$mpk_id) or $this->_db->raise_error();
					
				if (!mysql_num_rows($result))
				{
					$this->_db->query('INSERT INTO wsk_mpk_matrix_analyst VALUES ('.$mpk_id.', '.(int)$_POST["analyst"][$key].', 0, \'\')') or $this->_db->raise_error();
					$analist_id = mysql_insert_id();
				}
				else
				{
					$row = mysql_fetch_array($result);
					$analist_id = $row['analyst'];
				}
					
				mysql_free_result($result);
					
				$result = $this->_db->query('SELECT manager FROM wsk_mpk_matrix_manager WHERE manager = '.(int)$_POST["manager"][$key].' AND id = '.$mpk_id) or $this->_db->raise_error();
					
				if (!mysql_num_rows($result))
				{
					$this->_db->query('INSERT INTO wsk_mpk_matrix_manager VALUES ('.$mpk_id.', '.(int)$_POST["manager"][$key].', 0, \'\')') or $this->_db->raise_error();
					$manager_id = mysql_insert_id();
				}
				else
				{
					$row = mysql_fetch_array($result);
					$manager_id = $row['manager'];
				}
					
				mysql_free_result($result);
					
				$result = $this->_db->query('SELECT receiving FROM wsk_mpk_matrix_receiving WHERE receiving = '.(int)$_POST["receiving"][$key].' AND id = '.$mpk_id) or $this->_db->raise_error();
					
				if (!mysql_num_rows($result))
				{
					$this->_db->query('INSERT INTO wsk_mpk_matrix_receiving VALUES ('.$mpk_id.', '.(int)$_POST["receiving"][$key].', 0, \'\')') or $this->_db->raise_error();
					$receiving_id = mysql_insert_id();
				}
				else
				{
					$row = mysql_fetch_array($result);
					$receiving_id = $row['receiving'];
				}
					
				mysql_free_result($result);	
				
				array_push($paths, $mpk.'@#@'.(int)$_POST["budget"][$key].'@#@'.(int)$_POST["analyst"][$key].'@#@'.(int)$_POST["manager"][$key].'@#@'.(int)$_POST["receiving"][$key]);
			}
			
			$this->setSession("b2b_paths", $paths);
			header('Location: ./zamowienie.html');
		}
		else
		{
			$this->setSession("values", $values);
			
			$communicats['ok'] = 'ścieżki są niekompletne.';
			$this->setSession("path_communicats", $communicats);
		}
	}
}
?>
<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<label style="width:120px;">&nbsp;</label>
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
	<div class="clear"></div>
</div>
<?php } ?>

<?php
$merchant_id = (int)$_GET['id']; 

if (!isset($_POST['add_merchant']) && $action != 'edit')
{
	$merchants->clear();
}
if (!isset($_POST['save_changes']) && $action == 'edit')
{
	$merchants->getMerchant($merchant_id);
}
?>

<form method="POST" action="" enctype="multipart/form-data">

<input type="hidden" name="merchant_id" value="<?php echo $merchant_id; ?>" />
	
	<?php  if ($_SESSION["errors"]['merchant_name']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['merchant_name']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Handlowiec</label>
		<input type="text" name="merchant_name" value="<?php echo $merchants->textControll($_SESSION["values"]['merchant_name']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php if ($_SESSION["values"]['merchant_image']) { ?>
	<div class="form_element">
		<label style="width:120px;">&nbsp;</label>
		<img src="<?php echo BASE_ADDRESS; ?>../images/merchants/<?php echo $merchant_id.'/'.$_SESSION["values"]['merchant_image']; ?>" alt="" />
		<div class="clear"></div>
	</div>
	<?php } if ($_SESSION["errors"]['merchant_image']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['merchant_image']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Zdjęcie handlowca</label>
		<input style="width:200px;" name="merchant_image" type="file" />
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['merchant_email']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['merchant_email']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">E-mail handlowca</label>
		<input type="text" name="merchant_email" value="<?php echo $merchants->textControll($_SESSION["values"]['merchant_email']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['merchant_phone']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['merchant_phone']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Telefon handlowca</label>
		<input type="text" name="merchant_phone" value="<?php echo $merchants->textControll($_SESSION["values"]['merchant_phone']); ?>" />
		<div class="clear"></div>
	</div>
	
	<div class="form_element">
		<label style="width:120px;">Komentarz handlowca</label>
		<textarea name="merchant_description" cols="0" rows="0" style="width:200px; height:100px;"><?php echo $_SESSION["values"]['merchant_description']; ?></textarea>
		<div class="clear"></div>
	</div>
	
	<?php if ($_SESSION["values"]['guardian_image']) { ?>
	<div class="form_element">
		<label style="width:120px;">&nbsp;</label>
		<img src="<?php echo BASE_ADDRESS; ?>../images/merchants/<?php echo $merchant_id.'/'.$_SESSION["values"]['guardian_image']; ?>" alt="" />
		<div class="clear"></div>
	</div>
	<?php } if ($_SESSION["errors"]['guardian_image']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['guardian_image']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Zdjęcie opiekuna</label>
		<input style="width:200px;" name="guardian_image" type="file" />
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['guardian_name']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['guardian_name']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Opiekun</label>
		<input type="text" name="guardian_name" value="<?php echo $merchants->textControll($_SESSION["values"]['guardian_name']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['guardian_email']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['guardian_email']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">E-mail opiekuna</label>
		<input type="text" name="guardian_email" value="<?php echo $merchants->textControll($_SESSION["values"]['guardian_email']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['guardian_phone']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['guardian_phone']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Telefon opiekuna</label>
		<input type="text" name="guardian_phone" value="<?php echo $merchants->textControll($_SESSION["values"]['guardian_phone']); ?>" />
		<div class="clear"></div>
	</div>
	
	<div class="form_element">
		<label style="width:120px;">Komentarz opiekuna</label>
		<textarea name="guardian_description" cols="0" rows="0" style="width:200px; height:100px;"><?php echo $_SESSION["values"]['guardian_description']; ?></textarea>
		<div class="clear"></div>
	</div>
	
	<div class="form_element">
		<label style="width:120px;">&nbsp;</label>
		<input id="submit" name="save_changes" value="Zapisz zmiany" type="submit" />
		<div class="clear"></div>
	</div>
</form> 
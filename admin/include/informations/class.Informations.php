<?php
class Informations extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_information_id;
	private $_title;
	private $_start;
	private $_end;
	private $_content;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
	}
	
	public function deleteInformation($information_id)
	{
		$this->_db->query('DELETE FROM communicats WHERE id = '.$information_id) or $this->_db->raise_error();	
	}
	
	public function getInformations()
	{
		$result = $this->_db->query('SELECT id, title, `start`, `end`, start, end FROM communicats ORDER BY `start` DESC') or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getInformation($information_id)
	{
		$result = $this->_db->query('SELECT title, content, start, end FROM communicats WHERE id = '.$information_id) or $this->_db->raise_error();
		
		$row = mysql_fetch_array($result);
		
		$values['title'] = $row['title'];
		$values['start'] = $row['start'];
		$values['end'] = $row['end'];
		$values['content'] = str_replace('\"', '"', $row['content']);
		
		$this->setSession("values", $values);
	}
	
	public function validateInformation()
	{
		$this->clear();
		
		$this->_information_id = (int)$_POST['information_id'];
		$this->_title = $values['title'] = trim(mysql_escape_string($_POST['title']));
		$this->_content = $values['content'] = trim($_POST['content']);
		$this->_start = $values['start'] = trim($_POST['start']);
		$this->_end = $values['end'] = trim($_POST['end']);
		
		$start = explode('-', $this->_start);
		
		if (count($start) != 3)
		{
			$this->_errors++;
			$errors['start'] = 'Data jest niepoprawna';
		}
		else if (!$this->validateDate((int)$start[1], (int)$start[2], (int)$start[0]))
		{
			$this->_errors++;
			$errors['start'] = 'Data jest niepoprawna';
		}
		
		$end = explode('-', $this->_end);
		
		if (count($end) != 3)
		{
			$this->_errors++;
			$errors['end'] = 'Data jest niepoprawna';
		}
		else if (!$this->validateDate((int)$end[1], (int)$end[2], (int)$end[0]))
		{
			$this->_errors++;
			$errors['end'] = 'Data jest niepoprawna';
		}
		
		if ($this->validateDate((int)$start[1], (int)$start[2], (int)$start[0]) && $this->validateDate((int)$end[1], (int)$end[2], (int)$end[0]))
		{
			if ((int)date('U', strtotime($this->_end)) < (int)date('U', strtotime($this->_start)))
			{
				$this->_errors++;
				$errors['end'] = 'Data jest niepoprawna';
			}
		}
		
		if (empty($this->_title))
		{
			$this->_errors++;
			$errors['title'] = 'Tytuł jest wymagany';
		}
		
		if (!$this->validateTextarea($this->_content))
		{
			$this->_errors++;
			$errors['content'] = 'Treść jest wymagana';
		}
		
		if (!$this->_errors)
		{
			if (isset($_POST['add_information']))
			{
				$this->_addInformation();
			}
			else 
			{
				$this->_saveChanges();
			}
		}
		else 
		{
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
	}
	
	private function _addInformation()
	{
		$this->_db->query('INSERT INTO communicats VALUES (\'\', \''.$this->_title.'\', \''.mysql_escape_string($this->_content).'\', \''.$this->_start.'\', \''.$this->_end.'\')') or $this->_db->raise_error();
		
		$communicats['ok'] = 'Komunikat został dodany';
		$this->setSession("communicats", $communicats);
		header("Location: ../informations/list");
	}

	private function _saveChanges()
	{
		$this->_db->query('UPDATE communicats SET title = \''.$this->_title.'\', content = \''.mysql_escape_string($this->_content).'\', start = \''.$this->_start.'\', end = \''.$this->_end.'\' WHERE id = '.$this->_information_id) or $this->_db->raise_error();

		$communicats['ok'] = 'Komunikat został wyedytowany';
		$this->setSession("communicats", $communicats);
		header("Location: ../../informations/list");
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}
?>
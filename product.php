<?php 
error_reporting(E_ALL ^ E_NOTICE);
session_start();

include('include/config.php');
include ('include/nomad/nomad_mimemail.inc.php');
include('include/class/class.DataBase.php');
include('include/class/class.Validators.php');
include('include/class/class.Groups.php');
include('include/class/class.Products.php');
include('include/class/class.Prices.php');
include('include/class/class.Users.php');
include('include/class/class.Pages.php');
include('include/class/class.Producers.php');
include('include/class/class.ContactForm.php');
include('include/class/class.Comments.php');
include('include/class/class.Basket.php');
include('include/class/class.Orders.php');
include('include/class/class.SpecialOrders.php');
include('include/class/class.Templates.php');
include('include/class/class.Documents.php');

include('include/basket_operations.php');

$db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<base href="<?php echo BASE_ADDRESS; ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="reply-to" content="Adres_e-mail" />
<meta name="author" content="Autor_dokumentu" />
<meta name="description" content="Opis" />
<link rel="stylesheet" href="css/b2b.css" type="text/css" />
<link rel="stylesheet" href="css/subpage.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="admin/js/jquery-ui-1.8.17.custom.min.js"></script>	
<script type="text/javascript" src="js/jquery.marquee.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.scrollTo.js"></script>

<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/operations.js"></script>
<script type="text/javascript" src="js/ajax.js"></script>

<link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.1.css" media="screen" />
<script type="text/javascript" src="js/fancybox/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.1.js"></script>

<!--[if lte IE 7]>
<style type="text/css">
.wrapper {position:relative;}
.cell {position:absolute; top:50%;}
.hack {position:relative; top:-50%;}
</style>
<![endif]-->

<title>B2B</title>
</head>

<body>

<?php 
$product_id = (int)$_GET['id']; 
$product = $products->getProduct($product_id);
?>

<div id="product_content">

<div id="product_menu">
	<p id="description_box" class="active">OPIS PRODUKTU</p>
	<p id="ask_about_box">ZAPYTAJ O PRODUKT</p>
	<p id="comments_box">KOMENTARZE</p>
	<p id="newses_box">NOWOŚCI W KATEGORII</p>
	<p id="frequently_purchased_box">NAJCZĘŚCIEJ KUPOWANE</p>
	<div class="clear"></div>
</div>

<div id="product_border">
	<div id="subpage_content">
		<div id="product_left_column" style="display:block;">
			<p id="main_image"><span class="image"><a rel="gallery_<?php echo $product_id; ?>" href="<?php echo BASE_ADDRESS; ?>/include/image.php?id=<?php echo (int)$product['id_hermes']; ?>&w=500&h=500"><img src="<?php echo BASE_ADDRESS; ?>/include/image.php?id=<?php echo (int)$product['id_hermes']; ?>&w=170" alt="" border="0"></a></span></p>
				
			<?php
			$images = $products->getAdditionalPicuters($product_id);
				
			if (mysql_num_rows($images))
			{
				?><div><?php 
					
				while($image = mysql_fetch_array($images)) 
			   	{
			       	?><div class="gallery_element"><span class="image"><a rel="gallery_<?php echo $product_id; ?>" href="<?php echo BASE_ADDRESS; ?>/include/image.php?uid=<?php echo (int)$image['id']; ?>&w=500&h=500"><img src="<?php echo BASE_ADDRESS; ?>/include/image.php?uid=<?php echo (int)$image['id']; ?>&w=77" alt="" border="0"></a></span></div><?php
			   	}
			   		
			   	?><div class="clear"></div></div><?php
			}
				
			$files = $products->getFiles($product_id);
				
			if (mysql_num_rows($files))
			{
				?>
				<div class="files">
					<p><strong>Dodatkowe pliki do pobrania</strong></p>
					<ul>
					<?php
					while($file = mysql_fetch_array($files)) 
					{
						?><li><a target="blank" href="./download.php?id_1=<?php echo (int)$product['id_hermes']; ?>&id_2=<?php echo $file['id']; ?>"><?php echo $file['name']; ?></a></li><?php
					}
					?>
					</ul>
				</div>
				<?php
			}
			?>
		
		</div>
	</div>
	
	<div id="product_right_column" style="padding-left:10px;">
	<?php  ?>
		<div id="descritption_content" style="display:block;">
			<div class="description" style="width:400px;">
				
				<p><strong><?php echo $product['name']; ?></strong></p>
				<p><strong>Index:</strong> <?php echo $product['symbol']; ?></p>
				<p><strong>Producent:</strong> <?php echo $product['producer_name']; ?></p>
				
				<?php 
				if (true !== $_SESSION["b2b_full_offer"] || $product['product_status'] == 0)
				{
					?>
					<p style="padding:10px 0px 10px 0px;"><strong style="position:relative; top:-4px;">Dostępność:</strong> 
					<?php 
					if ($product['status'] > 0) 
					{ 
						switch ($product['status'])
						{
							case 1: ?><img src="images/mag/magaznyn_ziel2.png" alt="" /><?php break;
							case 2: ?><img src="images/mag/magaznyn_czer2.png" alt="" /><?php break;
							case 3: ?><img src="images/mag/magaznyn_nieb2.png" alt="" /><?php break;
							case 4: ?><img src="images/mag/magaznyn_czar2.png" alt="" /><?php break;
						}
					}
					else
					{
						?><img src="./magazyn.php?id=<?php echo $product['id_hermes']; ?>" alt="" /><?php
					}
					?>
					</p>
					<?php 
				}
				?>
				
				<?php if (true !== $_SESSION["b2b_full_offer"]) { ?>
					<div style="padding:5px 0px 5px 0px;">
					<?php
					$netto_price = $prices->getPrice($product['id_hermes'], 6);
					$final_netto_price = $prices->getPrice($product['id_hermes'], 4);
					
					$savings = ($netto_price - $final_netto_price);
					$discount = sprintf("%01.2f",($savings * 100/$netto_price));
					/*						
					?>
					<p style="font-size:15px; color:#000;"><strong>Cena katalogowa:</strong> <?php echo $netto_price; ?> zł netto</p>
					<p style="font-size:15px;" id="savings"><strong>Rabat:</strong> <?php echo $discount; ?> %</p>
					<?php */ ?>
					<p style="font-size:15px;" id="promotion_price"><strong>Twoja cena:</strong> <?php echo $final_netto_price; ?> zł netto</p>
					<?php /* ?>
					<p style="font-size:15px;">(<?php echo $prices->getPrice($product['id_hermes'], 1); ?> zł brutto, VAT <?php echo $product['tax']; ?>%)</p>
					<?php */ ?>
					</div>
				<?php } ?>
				
				<?php 
				if ($product['unit'] == 'STO') 
				{
					if (true !== $_SESSION["b2b_full_offer"]) 
					{
						?><p style="padding:0px 0px 0px 0px;"><strong>Cena za: </strong>100 sztuk</p><?php 
					} 
					?>
					<p style="padding:0px 0px 0px 0px;"><strong>Opakowanie zawiera:</strong> <?php echo (100 * $product['unit_detail']); ?> sztuk</p>
					<?php
				}
				?>
				
				<?php if ($product['weight'] > 0) { ?><p>Masa : <?php echo $product['weight']; ?> kg</p><?php } ?>
				<?php if ($product['pallet'] == 1) { ?><p>Towar wymaga transportu paletowego</p><?php } ?>
				
				<p style="padding:20px 0px 20px 0px;"><strong>Jednostka miary:</strong> <?php 
				switch ($product['unit'])
				{
					case 'SZT': ?>sztuka<?php break;
					case 'KPL': ?>komplet<?php break;
					case 'KG': ?>kilogram<?php break;
					case 'OP': ?>opakowanie<?php break;
					case 'M2': ?>metr kwadratowy<?php break;
					case 'MB': ?>metr bieżący<?php break;
					case 'M3': ?>metr sześcienny<?php break;
					case 'PAR': ?>para<?php break;
					case 'STO': ?>sto sztuk<?php break;
				}
				?></p>
				
			</div>
					
			<div id="product_to_basket">
			
				<p>
					<span><a href="./include/print.php?product_id=<?php echo $product['id_hermes']; ?>" target="blank">Drukuj</a> 
					<?php /* if (true !== $_SESSION["b2b_full_offer"]) { ?>
					( <input type="checkbox" name="with_price" value="1" /> z ceną )
					<?php }*/ ?>
					</span>
				</p>
			
				<?php if (trim($product['ean']) != '') { ?>
				<!-- <p style="padding:30px 0px 0px 0px;"><img src="include/barcode.php?code=<?php echo $product['ean']; ?>" alt="" /></p> -->
				<p style="padding:30px 0px 0px 0px; text-align:center;"><?php echo $product['ean']; ?></p>
				<?php } ?>
				
				<input type="hidden" name="last_operation" value="<?php if (isset($_POST['product_id'])) { echo './grupa,'.$product['subgroup_id'].',1.'.$product['producer_id'].'.html'; } ?>" />
				
				<form method="post" action="<?php echo substr(BASE_ADDRESS, 0, -1).$_SERVER['REQUEST_URI']; ?>" name="to_basket">
				<input type="hidden" name="operation" value="to_basket" />
				<input type="hidden" name="product_id" value="<?php echo $product['id_hermes']; ?>" />
				<div style="padding:30px 20px 0px 0px;">
					<?php if (true !== $_SESSION["b2b_full_offer"] || $product['product_status'] == 0) { ?>
					<div>
					Liczba 
					<?php 
					switch ($product['unit'])
					{
						case 'SZT': ?>sztuk<?php break;
						case 'KPL': ?>kompletów<?php break;
						case 'KG': ?>kilogramów<?php break;
						case 'OP': ?>opakowań<?php break;
						case 'M2': ?>metrów kwadr.<?php break;
						case 'MB': ?>metrów bierz.<?php break;
						case 'M3': ?>metrów sześc.<?php break;
						case 'PAR': ?>par<?php break;
						default: ?>sztuk<?php break;
					}
					?>
					:<span><input id="count" type="text" name="count" value="1" /></span></div>
					<?php } ?>
					<div class="clear"></div>
					<div>
						<p class="button" style="padding-top:5px;">
						<?php if (true !== $_SESSION["b2b_full_offer"] || $product['product_status'] == 0) { ?>
						<a id="product_to_basket_form_submit" style="cursor:pointer;"><?php if (true !== $_SESSION["b2b_full_offer"]) { ?>DO ZAMÓWIENIA<?php } else { ?>DO ZAPYTANIA<?php } ?></a>
						<?php } else { ?>
						<strong>Produkt dostępny<br />w ofercie zawężonej</strong>
						<?php } ?>
						</p>
					</div>
					<div class="clear"></div>
					<?php if (true !== $_SESSION["b2b_full_offer"]) { ?>
					<div>
						<p class="button favorite_product" style="padding-top:5px;">
						<a href="./" id="favorite_21959">DODAJ DO ULUBIONYCH</a>
						</p>
					</div>
					<div class="clear"></div>
					<?php } ?>
				</div>
				</form>
			</div>
					
			<div class="clear"></div>
			
			<?php if ($product['description'] != '') { ?>
				<div id="about_product">
				<p><strong>Opis produktu:</strong></p>
				<?php echo $product['description']; ?>
				</div>
			<?php } ?>
				
			<?php /* if (true !== $_SESSION["b2b_full_offer"]) { ?>
				<p style="padding-top:30px;"><strong>Kalkulator marży:</strong></p>
				<p>
					<input type="hidden" name="tax" value="<?php echo (($product['tax'] + 100)/100); ?>" />
					<?php $netto_price = $prices->getPrice($product['id_hermes'], 4); ?>
					<span id="user_netto"><strong><?php echo $netto_price; ?> zł</strong> (netto)</span> + <input type="text" name="percent" value="" maxlength="2" style="width:20px;" /> % = 
					<span id="customer_netto"><strong><?php echo $netto_price; ?> zł</strong> (netto)</span> [ 
					<span id="customer_brutto"><strong><?php echo sprintf("%01.2f", $netto_price*(($product['tax'] + 100)/100)); ?> zł</strong> (brutto)</span> ]
				</p>
			<?php } */ ?>
				
			<?php 
			$replacement = $products->getReplacement((int)$product['id_hermes']);
				
			if ((int)$replacement['id_hermes'])
			{
				?>
				<p style="padding-top:25px;"><strong>Lista zamienników:</strong></p>
				<table cellspacing="0" cellpadding="0" class="product_table">
				<tr>
					<th style="border-bottom:1px solid #ebebeb; border-top:1px solid #ebebeb; padding:3px 0px 3px 0px;">&nbsp;</th>
					<th style="text-align:left; border-bottom:1px solid #ebebeb; border-top:1px solid #ebebeb; padding:3px 0px 3px 0px;">Nazwa produktu</th>
					<th style="text-align:left; border-bottom:1px solid #ebebeb; border-top:1px solid #ebebeb; padding:3px 0px 3px 0px;">Indeks</th>
					<th style="text-align:right; border-bottom:1px solid #ebebeb; border-top:1px solid #ebebeb; padding:3px 0px 3px 0px;">Cena</th>
				</tr>
				<tr>
				<td class="image_row">
					<span class="image"><a rel="gallery" href="<?php echo BASE_ADDRESS; ?>/include/image.php?id=<?php echo (int)$replacement['id_hermes']; ?>&w=500&h=500">
					<img src="<?php echo BASE_ADDRESS; ?>/include/image.php?id=<?php echo (int)$replacement['id_hermes']; ?>&w=50&h=50" alt="" />
					</a></span>
				</td>
				<td>
					<a href="#" onClick="javascript:pop_up(<?php echo $replacement['id_hermes']; ?>); return false;"><?php echo $replacement['name']; ?></a>
				</td>
				<td><?php echo $replacement['symbol']; ?></td>
				<td style="text-align:right;"><?php echo $prices->getPrice($replacement['id_hermes'], 4); ?> zł netto</td>
				</tr>
				</table>
				<?php
			}
			?>
			
			<div class="clear"></div>
		</div>
		
		<input type="hidden" name="product_operation" value="<?php echo $_POST['operation']; ?>" />
				
		<div id="product_forms" style="width:780px;">
			<div id="question_content" style="display:none">
			<?php
			include('include/product_ask_about.php');
			?>
			</div>
			<div id="comments_content" style="display:none">
			<?php
			include('include/product_comments.php');
			?>
			</div>
			<div id="newses_content" style="display:none">
			<?php 
			$newses_list = $products->getNewses('', 0, 1, $product['subgroup_id']);
			
			if (is_array($newses_list))
			{
				?>
				<table cellspacing="0" cellpadding="0" class="product_table">
				<?php 
				
				foreach ($newses_list as $news)
				{
					?>
					<tr>
					<td class="image_row">
						<span class="image"><a rel="gallery" href="<?php echo BASE_ADDRESS; ?>/include/image.php?id=<?php echo (int)$news['id_hermes']; ?>&w=500&h=500">
						<img src="<?php echo BASE_ADDRESS; ?>/include/image.php?id=<?php echo (int)$news['id_hermes']; ?>&w=50&h=50" alt="" />
						</a></span>
					</td>
					<td>
						<a href="#" onClick="javascript:pop_up(<?php echo $news['id_hermes']; ?>); return false;"><?php echo $news['name']; ?></a>
					</td>
					</tr>
					<?php
				}
				
				?>
				</table>
				<?php
			}
			else
			{
				?>brak produktów<?php
			}
			?>
			</div>
			<div id="frequently_purchased_content" style="display:none">
			
			<?php 
			$frequently_purchased = $products->getFrequentlyPurchased($product['subgroup_id']);
			
			if (mysql_num_rows($frequently_purchased))
			{
				?>
				<table cellspacing="0" cellpadding="0" class="product_table">
				<tr>
					<th style="border-bottom:1px solid #ebebeb; border-top:1px solid #ebebeb; padding:3px 0px 3px 0px;">&nbsp;</th>
					<th style="text-align:left; border-bottom:1px solid #ebebeb; border-top:1px solid #ebebeb; padding:3px 0px 3px 0px;">Nazwa produktu</th>
					<th style="text-align:left; border-bottom:1px solid #ebebeb; border-top:1px solid #ebebeb; padding:3px 0px 3px 0px;">Indeks</th>
					<th style="text-align:right; border-bottom:1px solid #ebebeb; border-top:1px solid #ebebeb; padding:3px 0px 3px 0px;">Cena</th>
				</tr>
				<?php 
				while($row = mysql_fetch_array($frequently_purchased)) 
				{
					?>
					<tr>
					<td class="image_row">
						<span class="image"><a rel="gallery" href="<?php echo BASE_ADDRESS; ?>/include/image.php?id=<?php echo (int)$row['id_hermes']; ?>&w=500&h=500">
						<img src="<?php echo BASE_ADDRESS; ?>/include/image.php?id=<?php echo (int)$row['id_hermes']; ?>&w=50&h=50" alt="" />
						</a></span>
					</td>
					<td>
						<a href="#" onClick="javascript:pop_up(<?php echo $row['id_hermes']; ?>); return false;"><?php echo $row['name']; ?></a>
					</td>
					<td><?php echo $row['symbol']; ?></td>
					<td style="text-align:right;"><?php echo $prices->getPrice($row['id_hermes'], 4); ?> zł netto</td>
					</tr>
					<?php
				}
				?>
				</table>
				<?php
			}
			else
			{
				?>Brak produktów<?php
			}
			?>
			</div>
		</div>
	<?php ?>			
	</div>
			
	<div class="clear"></div>
	
	<p style="font-size:11px; padding-top:11px; text-align:center;">
	Mimo dołożenia wszelkich starań nie gwarantujemy, że publikowane dane techniczne i zdjęcia nie zawierają uchybień lub błędów,<br />
	które nie mogą jednak być podstawą do roszczeń.
	</p>
</div>
</div>

</body>
</html>
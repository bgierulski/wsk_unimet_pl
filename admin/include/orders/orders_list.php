<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
</div>
<?php } else if (isset($_SESSION["communicats"]['ok'])) { ?>
<div class="form_element ok">
	<?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?>
</div>
<?php } ?>

<?php 
switch ($action)
{
	case 'waiting':
		$waiting_list = $orders->getOrders(array(1,8));
		break;
	
	case 'ongoing':
		$ongoing_list = $orders->getOrders(array(2));
		break;
		
	case 'realized':
		$realized_years = $orders->getYears(array(3));
		
		if (!isset($_SESSION["b2b_orders"]['realized']) || empty($_SESSION["b2b_orders"]['realized'])) 
		{ 
			$_SESSION["b2b_orders"]['realized'] = date('Y');
		}
		
		$realized_list = $orders->getOrders(array(3), (int)$_SESSION["b2b_orders"]['realized']);
		break;
		
	case 'archiwum':
		$archium_years = $orders->getYears(array(4));
		
		if (!isset($_SESSION["b2b_orders"]['archives']) || empty($_SESSION["b2b_orders"]['archives'])) 
		{ 
			$_SESSION["b2b_orders"]['archives'] = date('Y');
		}
	
		$archium_list = $orders->getOrders(array(4), (int)$_SESSION["b2b_orders"]['archives']);
		break;
		
	case 'deleted':
		$deleted_years = $orders->getYears(array(5));
		
		if (!isset($_SESSION["b2b_orders"]['deleted']) || empty($_SESSION["b2b_orders"]['deleted'])) 
		{ 
			$_SESSION["b2b_orders"]['deleted'] = date('Y');
		}
	
		$deleted_list = $orders->getOrders(array(5), (int)$_SESSION["b2b_orders"]['deleted']);
		break;
		
	case 'return':
		$return_list = $orders->getOrders(array(20));
		break;
		
	default:
		$realized_years = $orders->getYears(array(3));
		$archium_years = $orders->getYears(array(4));
		$deleted_years = $orders->getYears(array(5));
		
		if (!isset($_SESSION["b2b_orders"]['realized']) || empty($_SESSION["b2b_orders"]['realized'])) 
		{ 
			$_SESSION["b2b_orders"]['realized'] = date('Y');
		}
		if (!isset($_SESSION["b2b_orders"]['archives']) || empty($_SESSION["b2b_orders"]['archives'])) 
		{ 
			$_SESSION["b2b_orders"]['archives'] = date('Y');
		}
		if (!isset($_SESSION["b2b_orders"]['deleted']) || empty($_SESSION["b2b_orders"]['deleted'])) 
		{ 
			$_SESSION["b2b_orders"]['deleted'] = date('Y');
		}
	
		$waiting_list = $orders->getOrders(array(1,8));
		$ongoing_list = $orders->getOrders(array(2));
		$realized_list = $orders->getOrders(array(3), (int)$_SESSION["b2b_orders"]['realized']);
		$archium_list = $orders->getOrders(array(4), (int)$_SESSION["b2b_orders"]['archives']);
		$deleted_list = $orders->getOrders(array(5), (int)$_SESSION["b2b_orders"]['deleted']);
		$return_list = $orders->getOrders(array(20));
		break;
}


?>
<div id="reject_comment">
<input type="hidden" name="order_id" value="0" />
<p><textarea name="reject_comment" cols="0" rows="0"></textarea></p>
<p><a href="./" class="submit"><strong>ODRZUĆ ZAMÓWIENIE</strong></a></p>
<span><img src="images/close_icon.png" alt="" class="close" /></span>
</div>
<?php

if (is_array($waiting_list) || $action == 'waiting')
{
	?>
	<table class="orders_list" style="padding-bottom:10px;">
	<tr>
		<td class="bg_1" colspan="11" style="text-align:left; font-weight:bold;">ZAPROPONUJ ZAMIENNIK</td>
	</tr>
	<tr>
		<th style="width:9%;">Data zamówienia</th>
		<th style="width:5%;">Nr zam.</th>
		<th style="width:5%;">Wg zapyt.</th>
		<th style="width:9%;">Login użytkwonika</th>
		<th style="width:9%;">Imię i nazwisko / nazwa firmy</th>
		<th style="width:9%;">Wydział</th>
		<th style="width:8%;">Wartość brutto</th>
		<th style="width:8%;">Opiekun</th>
		<th style="width:8%;">Data dostawy</th>
		<th style="width:8%;">MPK</th>
		<th style="width:30%;">Operacje</th>
	</tr>
	<?php
	if (is_array($waiting_list))
	{
		$counter = 0;
		
		foreach ($waiting_list as $key => $row)
		{
			if ($_SESSION["admin"]['narrow_orders'] == 0 || in_array($row['merchant_id'], $_SESSION["admin"]["merchants"]) && $row['id'] != $waiting_list[$key - 1]['id'])
			{
				if ($row['status'] == 8 && $row['query_id'] == 0 && !empty($row['department']))
				{
					?>
				    <tr>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['add_date']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $orders->formatId($row['id'], $row['query_id']); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo (($row['query_id'] > 0) ? $orders->formatId($row['query_id'], $row['query_id']) : ""); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['login']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $orders->textControll($row['user_name'].' '.$row['user_surname']); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['department']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo sprintf("%0.2f", $row['brutto_value']); ?> zł</td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['unimet_representative']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo (($row['delivery_date'] != "1970-01-01") ? $row['delivery_date'] : ""); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['mpk']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?> options" id="order_<?php echo $row['id']; ?>">
						<?php if ($_SESSION["admin"]['observer'] == 0) { ?>
							<?php if ($row['status'] == 1) { ?>
							<a href="./orders/<?php echo $controller; ?>/to_realization/<?php echo $row['id']; ?>">do realizacji</a> &nbsp;::&nbsp; 
							<?php } else if ($row['status'] == 8 && $row['query_id'] == 0) { ?>
								<?php if ($row['replacement_id'] > 0) { ?>
									<a href="./orders/<?php echo $controller; ?>/to_contractor/<?php echo $row['id']; ?>">do zamawiającego</a> &nbsp;::&nbsp; 
								<?php } else { ?>
									<a href="./orders/<?php echo $controller; ?>/to_analyst/<?php echo $row['id']; ?>">do analityka</a> &nbsp;::&nbsp; 
								<?php } ?>
							<?php } else { ?>
							<a href="./orders/<?php echo $controller; ?>/to_analyst/<?php echo $row['id']; ?>">do analityka</a> &nbsp;::&nbsp; 
							<?php } ?>
							<a href="./orders/<?php echo $controller; ?>/delete/<?php echo $row['id']; ?>" alt="<?php echo $row['id']; ?>" class="reject_order">odrzuć</a> &nbsp;::&nbsp; 
							<a href="./orders/edit/<?php echo $row['id']; ?>">edytuj</a> &nbsp;::&nbsp;  
						<?php } ?>
						<a target="blank" href="./orders/show/<?php echo $row['id']; ?>">podgląd</a> &nbsp;::&nbsp;
						<a target="blank" href="./orders/print/<?php echo $row['id']; ?>">wydruk</a>
						
						</td>
					</tr>
				    <?php
				    
				     $counter++;
				}
			}
		}
		/*
		?>
		<tr>
			<td class="bg_1" colspan="9">&nbsp;</td>
			<td class="bg_1" colspan="10" style="text-align:right; font-weight:bold; text-align:center;"><a href="./orders/waiting/to_realization">PRZENIEŚ WSZYSTKIE DO REALIZACJI</a></td>
		</tr>
		<?php
		*/
	}
	?>
	</table>
	
	<table class="orders_list" style="padding-bottom:10px;">
	<tr>
		<td class="bg_1" colspan="11" style="text-align:left; font-weight:bold;">ZAMÓWIENIA OCZEKUJĄCE</td>
	</tr>
	<tr>
		<th style="width:9%;">Data zamówienia</th>
		<th style="width:5%;">Nr zam.</th>
		<th style="width:5%;">Wg zapyt.</th>
		<th style="width:9%;">Login użytkwonika</th>
		<th style="width:9%;">Imię i nazwisko / nazwa firmy</th>
		<th style="width:9%;">Wydział</th>
		<th style="width:8%;">Wartość brutto</th>
		<th style="width:8%;">Opiekun</th>
		<th style="width:8%;">Data dostawy</th>
		<th style="width:8%;">MPK</th>
		<th style="width:30%;">Operacje</th>
	</tr>
	<?php
	if (is_array($waiting_list))
	{
		$counter = 0;
		
		foreach ($waiting_list as $key => $row)
		{
			if ($_SESSION["admin"]['narrow_orders'] == 0 || in_array($row['merchant_id'], $_SESSION["admin"]["merchants"]) && $row['id'] != $waiting_list[$key - 1]['id'])
			{
				if ($row['status'] == 1 && !empty($row['department']))
				{
					?>
				    <tr>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['add_date']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $orders->formatId($row['id'], $row['query_id']); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo (($row['query_id'] > 0) ? $orders->formatId($row['query_id'], $row['query_id']) : ""); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['login']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $orders->textControll($row['user_name'].' '.$row['user_surname']); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['department']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo sprintf("%0.2f", $row['brutto_value']); ?> zł</td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['unimet_representative']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo (($row['delivery_date'] != "1970-01-01") ? $row['delivery_date'] : ""); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['mpk']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?> options" id="order_<?php echo $row['id']; ?>">
						<?php if ($_SESSION["admin"]['observer'] == 0) { ?>
							<?php if ($row['status'] == 1) { ?>
							<a href="./orders/<?php echo $controller; ?>/to_realization/<?php echo $row['id']; ?>">do realizacji</a> &nbsp;::&nbsp; 
							<?php } else if ($row['status'] == 8 && $row['query_id'] == 0) { ?>
								<?php if ($row['replacement_id'] > 0) { ?>
									<a href="./orders/<?php echo $controller; ?>/to_contractor/<?php echo $row['id']; ?>">do zamawiającego</a> &nbsp;::&nbsp; 
								<?php } else { ?>
									<a href="./orders/<?php echo $controller; ?>/to_analyst/<?php echo $row['id']; ?>">do analityka</a> &nbsp;::&nbsp; 
								<?php } ?>
							<?php } else { ?>
								<a href="./orders/<?php echo $controller; ?>/to_analyst/<?php echo $row['id']; ?>">do analityka</a> &nbsp;::&nbsp; 
							<?php } ?>
							<a href="./orders/<?php echo $controller; ?>/delete/<?php echo $row['id']; ?>" alt="<?php echo $row['id']; ?>" class="reject_order">odrzuć</a> &nbsp;::&nbsp; 
							<a href="./orders/edit/<?php echo $row['id']; ?>">edytuj</a> &nbsp;::&nbsp; 
						<?php } ?>
						<a target="blank" href="./orders/show/<?php echo $row['id']; ?>">podgląd</a> &nbsp;::&nbsp;
						<a target="blank" href="./orders/print/<?php echo $row['id']; ?>">wydruk</a>
						</td>
					</tr>
				    <?php
				    
				    $counter++;
				}
			}
		}
		
		if ($_SESSION["admin"]['observer'] == 0) 
		{
			?>
			<tr>
				<td class="bg_1" colspan="10">&nbsp;</td>
				<td class="bg_1" colspan="11" style="text-align:right; font-weight:bold; text-align:center;"><a href="./orders/waiting/to_realization">PRZENIEŚ WSZYSTKIE DO REALIZACJI</a></td>
			</tr>
			<?php
		}
	}
	?>
	</table>
	<?php
}

if (is_array($ongoing_list) || $action == 'ongoing')
{
	?>
	<table class="orders_list" style="padding-bottom:10px;">
	<tr>
		<td class="bg_1" colspan="11" style="text-align:left; font-weight:bold;">ZAMÓWIENIA W TRAKCIE REALIZACJI</td>
	</tr>
	<tr>
		<th>&nbsp;</th>
		<th style="width:9%;">Data zamówienia</th>
		<th style="width:5%;">Nr zam.</th>
		<th style="width:5%;">Wg zapyt.</th>
		<th style="width:9%;">Login użytkwonika</th>
		<th style="width:9%;">Imię i nazwisko / nazwa firmy</th>
		<th style="width:9%;">Wydział</th>
		<th style="width:8%;">Wartość brutto</th>
		<th style="width:8%;">Opiekun</th>
		<th style="width:8%;">Data dostawy</th>
		<th style="width:8%;">MPK</th>
		<th style="width:30%;">Operacje</th>
	</tr>
	<?php
	if (is_array($ongoing_list))
	{
		$counter = 0;
		
		foreach ($ongoing_list as $key => $row)
		{
			if ($_SESSION["admin"]['narrow_orders'] == 0 || in_array($row['merchant_id'], $_SESSION["admin"]["merchants"]) && $row['id'] != $ongoing_list[$key - 1]['id'] && !empty($row['department']))
			{
				?>
				<tr>
					<td class="bg_<?php echo ($counter%2)+1; ?>"><input type="checkbox" name="hermes[]" value="1" id="hermes_<?php echo $row['id']; ?>" <?php if ($row['hermes'] == 1) { ?>checked="checked"<?php } ?> title="wczytanie do Hermesa" /></td>
					<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['add_date']; ?></td>
					<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $orders->formatId($row['id'], $row['query_id']); ?></td>
					<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo (($row['query_id'] > 0) ? $orders->formatId($row['query_id'], $row['query_id']) : ""); ?></td>
					<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['login']; ?></td>
					<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $orders->textControll($row['user_name'].' '.$row['user_surname']); ?></td>
					<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['department']; ?></td>
					<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo sprintf("%0.2f", $row['brutto_value']); ?> zł</td>
					<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['unimet_representative']; ?></td>
					<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo (($row['delivery_date'] != "1970-01-01") ? $row['delivery_date'] : ""); ?></td>
					<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['mpk']; ?></td>
					<td class="bg_<?php echo ($counter%2)+1; ?> options" id="order_<?php echo $row['id']; ?>">
					<?php if ($_SESSION["admin"]['observer'] == 0) { ?>
						<a href="./orders/<?php echo $controller; ?>/to_realized/<?php echo $row['id']; ?>">do zrealizowanych</a> &nbsp;::&nbsp; 
						<a href="./orders/edit/<?php echo $row['id']; ?>">edytuj</a> &nbsp;::&nbsp; 
					<?php } ?>
					<a target="blank" href="./orders/show/<?php echo $row['id']; ?>">podgląd</a> &nbsp;::&nbsp;
					<a target="blank" href="./orders/print/<?php echo $row['id']; ?>">wydruk</a>
					</td>
				</tr>
				<?php
				    
				$counter++;
			}
		}
	}
	?>
	</table>
	<?php
}

if (is_array($realized_list) || $action == 'realized')
{
	?>
	<table class="orders_list" style="padding-bottom:10px;">
	<tr>
		<td class="bg_1" colspan="10" style="text-align:left; font-weight:bold;">ZAMÓWIENIA ZREALIZOWANE (W DRODZE)
		<?php
		if (is_array($realized_years) && !empty($realized_years))
		{
			?><span>(<?php
		
			foreach ($realized_years as $key => $element)
			{
				?><a href="./" class="year realized <?php echo (($_SESSION["b2b_orders"]["realized"] == $element['year']) ? "active" : ""); ?>" alt="<?php echo $element['year']; ?>"><?php echo $element['year']; ?></a><?php echo (($element != end($realized_years)) ? ', ' : '');
			}
			
			?>)</span><?php
		}
		?>
		- <strong class="show">POKAŻ</strong></td>
	</tr>
	<tr style="display:none;">
		<th style="width:9%;">Data zamówienia</th>
		<th style="width:5%;">Nr zam.</th>
		<th style="width:9%;">Login użytkwonika</th>
		<th style="width:9%;">Imię i nazwisko / nazwa firmy</th>
		<th style="width:9%;">Wydział</th>
		<th style="width:8%;">Wartość brutto</th>
		<th style="width:8%;">Opiekun</th>
		<th style="width:8%;">Data dostawy</th>
		<th style="width:8%;">MPK</th>
		<th style="width:30%;">Operacje</th>
	</tr>
	<?php
	if (is_array($realized_list))
	{
		$counter = 0;
		
		foreach ($realized_list as $key => $row)
		{
			if ($_SESSION["admin"]['narrow_orders'] == 0 || in_array($row['merchant_id'], $_SESSION["admin"]["merchants"]) && $row['id'] != $realized_list[$key - 1]['id'] && !empty($row['department']))
			{
				if ($row['query_id'] == 0)
				{
					?>
				    	<tr style="display:none;">
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['add_date']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $orders->formatId($row['id'], $row['query_id']); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['login']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $orders->textControll($row['user_name'].' '.$row['user_surname']); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['department']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo sprintf("%0.2f", $row['brutto_value']); ?> zł</td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['unimet_representative']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo (($row['delivery_date'] != "1970-01-01") ? $row['delivery_date'] : ""); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['mpk']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?> options" id="order_<?php echo $row['id']; ?>">
						<?php if ($_SESSION["admin"]['observer'] == 0) { ?>
							<a href="./orders/<?php echo $controller; ?>/to_archiwum/<?php echo $row['id']; ?>">do archiwum</a> &nbsp;::&nbsp; 
							<a href="./orders/edit/<?php echo $row['id']; ?>">edytuj</a> &nbsp;::&nbsp; 
						<?php } ?>
						<a target="blank" href="./orders/show/<?php echo $row['id']; ?>">podgląd</a> &nbsp;::&nbsp;
						<a target="blank" href="./orders/print/<?php echo $row['id']; ?>">wydruk</a>
						<?php 
						if ($row['date_diff'] > 5)
						{
							?> &nbsp;::&nbsp; <a target="blank" href="./orders/edit/<?php echo $row['id']; ?>">czeka na odbiór od <?php echo $row['date_diff']; ?> dni</a><?php
						}
						?>
						</td>
					</tr>
				    <?php
				    
				    $counter++;
				}
			}
		}
	}
	?>
	</table>
	<?php
}

if (is_array($archium_list) || $action == 'archiwum')
{
	?>
	<table class="orders_list" style="padding-bottom:10px;">
	<tr>
		<td class="bg_1" colspan="10" style="text-align:left; font-weight:bold;">ZAMÓWIENIA PRZENIESIONE DO ARCHIWUM 
		<?php
		if (is_array($archium_years) && !empty($archium_years))
		{
			?><span>(<?php
		
			foreach ($archium_years as $key => $element)
			{
				?><a href="./" class="year archives <?php echo (($_SESSION["b2b_orders"]["archives"] == $element['year']) ? "active" : ""); ?>" alt="<?php echo $element['year']; ?>"><?php echo $element['year']; ?></a><?php echo (($element != end($archium_years)) ? ', ' : '');
			}
			
			?>)</span><?php
		}
		?>
		- <strong class="show">POKAŻ</strong></td>
	</tr>
	<tr style="display:none;">
		<th style="width:9%;">Data zamówienia</th>
		<th style="width:5%;">Nr zam.</th>
		<th style="width:9%;">Login użytkwonika</th>
		<th style="width:9%;">Imię i nazwisko / nazwa firmy</th>
		<th style="width:9%;">Wydział</th>
		<th style="width:8%;">Wartość brutto</th>
		<th style="width:8%;">Opiekun</th>
		<th style="width:8%;">Data dostawy</th>
		<th style="width:8%;">MPK</th>
		<th style="width:30%;">Operacje</th>
	</tr>
	<?php
	if (is_array($archium_list))
	{
		$counter = 0;
		
		foreach ($archium_list as $key => $row)
		{
			if ($_SESSION["admin"]['narrow_orders'] == 0 || in_array($row['merchant_id'], $_SESSION["admin"]["merchants"]) && $row['id'] != $archium_list[$key - 1]['id'] && !empty($row['department']))
			{
				if ($row['query_id'] == 0)
				{
					?>
				    	<tr style="display:none;">
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['add_date']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $orders->formatId($row['id'], $row['query_id']); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['login']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $orders->textControll($row['user_name'].' '.$row['user_surname']); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['department']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo sprintf("%0.2f", $row['brutto_value']); ?> zł</td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['unimet_representative']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo (($row['delivery_date'] != "1970-01-01") ? $row['delivery_date'] : ""); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['mpk']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?> options" id="order_<?php echo $row['id']; ?>">
						<?php if ($_SESSION["admin"]['observer'] == 0) { ?>
							<a href="./orders/edit/<?php echo $row['id']; ?>">edytuj</a> &nbsp;::&nbsp; 
						<?php } ?>
						<a target="blank" href="./orders/show/<?php echo $row['id']; ?>">podgląd</a> &nbsp;::&nbsp;
						<a target="blank" href="./orders/print/<?php echo $row['id']; ?>">wydruk</a>
						</td>
					</tr>
				    <?php
				    
				    $counter++;
				}
			}
		}
	}
	?>
	</table>
	<?php
}

if (is_array($deleted_list) || $action == 'deleted')
{
	?>
	<table class="orders_list" style="padding-bottom:10px;">
	<tr>
		<td class="bg_1" colspan="10" style="text-align:left; font-weight:bold;">ZAMÓWIENIA ODRZUCONE 
		<?php
		if (is_array($deleted_years) && !empty($deleted_years))
		{
			?><span>(<?php
		
			foreach ($deleted_years as $key => $element)
			{
				?><a href="./" class="year deleted <?php echo (($_SESSION["b2b_orders"]["deleted"] == $element['year']) ? "active" : ""); ?>" alt="<?php echo $element['year']; ?>"><?php echo $element['year']; ?></a><?php echo (($element != end($deleted_years)) ? ', ' : '');
			}
			
			?>)</span><?php
		}
		?>
		- <strong class="show">POKAŻ</strong></td>
	</tr>
	<tr style="display:none;">
		<th style="width:9%;">Data zamówienia</th>
		<th style="width:5%;">Nr zam.</th>
		<th style="width:9%;">Login użytkwonika</th>
		<th style="width:9%;">Imię i nazwisko / nazwa firmy</th>
		<th style="width:9%;">Wydział</th>
		<th style="width:8%;">Wartość brutto</th>
		<th style="width:8%;">Opiekun</th>
		<th style="width:8%;">Data dostawy</th>
		<th style="width:8%;">MPK</th>
		<th style="width:30%;">Operacje</th>
	</tr>
	<?php
	if (is_array($deleted_list))
	{
		$counter = 0;
		
		foreach ($deleted_list as $key => $row)
		{
			if ($_SESSION["admin"]['narrow_orders'] == 0 || in_array($row['merchant_id'], $_SESSION["admin"]["merchants"]) && $row['id'] != $deleted_list[$key - 1]['id'] && !empty($row['department']))
			{
				if ($row['query_id'] == 0)
				{
					?>
				    	<tr style="display:none;">
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['add_date']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $orders->formatId($row['id'], $row['query_id']); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['login']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $orders->textControll($row['user_name'].' '.$row['user_surname']); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['department']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo sprintf("%0.2f", $row['brutto_value']); ?> zł</td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['unimet_representative']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo (($row['delivery_date'] != "1970-01-01") ? $row['delivery_date'] : ""); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['mpk']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?> options" id="order_<?php echo $row['id']; ?>">
						<?php if ($_SESSION["admin"]['observer'] == 0) { ?>
							<a href="./orders/<?php echo $controller; ?>/to_archiwum/<?php echo $row['id']; ?>">do archiwum</a> &nbsp;::&nbsp; 
							<a href="./orders/edit/<?php echo $row['id']; ?>">edytuj</a> &nbsp;::&nbsp; 
						<?php } ?>
						<a target="blank" href="./orders/show/<?php echo $row['id']; ?>">podgląd</a> &nbsp;::&nbsp;
						<a target="blank" href="./orders/print/<?php echo $row['id']; ?>">wydruk</a>
						</td>
					</tr>
				    <?php
				    
				    $counter++;
				}
			}
		}
	}
	?>
	</table>
	<?php
}

if (is_array($return_list) || $action == 'return')
{
	?>
	<table class="orders_list" style="padding-bottom:10px;">
	<tr>
		<td class="bg_1" colspan="10" style="text-align:left; font-weight:bold;">ZWROTY</td>
	</tr>
	<tr>
		<th style="width:9%;">Data zamówienia</th>
		<th style="width:5%;">Nr zam.</th>
		<th style="width:9%;">Login użytkwonika</th>
		<th style="width:9%;">Imię i nazwisko / nazwa firmy</th>
		<th style="width:9%;">Wydział</th>
		<th style="width:8%;">Wartość brutto</th>
		<th style="width:8%;">Opiekun</th>
		<th style="width:8%;">Data dostawy</th>
		<th style="width:8%;">MPK</th>
		<th style="width:30%;">Operacje</th>
	</tr>
	<?php
	if (is_array($return_list))
	{
		$counter = 0;
		
		foreach ($return_list as $key => $row)
		{
			if ($_SESSION["admin"]['narrow_orders'] == 0 || in_array($row['merchant_id'], $_SESSION["admin"]["merchants"]) && $row['id'] != $return_list[$key - 1]['id'] && !empty($row['department']))
			{
				if ($row['query_id'] == 0)
				{
					?>
				    <tr>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['add_date']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $orders->formatId($row['id'], $row['query_id']); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['login']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $orders->textControll($row['user_name'].' '.$row['user_surname']); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['department']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo sprintf("%0.2f", $row['brutto_value']); ?> zł</td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['unimet_representative']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo (($row['delivery_date'] != "1970-01-01") ? $row['delivery_date'] : ""); ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?>"><?php echo $row['mpk']; ?></td>
						<td class="bg_<?php echo ($counter%2)+1; ?> options" id="order_<?php echo $row['id']; ?>">
						<?php if ($_SESSION["admin"]['observer'] == 0) { ?>
						<a href="./orders/<?php echo $controller; ?>/to_archiwum/<?php echo $row['id']; ?>">do archiwum</a> &nbsp;::&nbsp; 
						<a href="./orders/edit/<?php echo $row['id']; ?>">edytuj</a> &nbsp;::&nbsp; 
						<?php } ?>
						<a target="blank" href="./orders/show/<?php echo $row['id']; ?>">podgląd</a> &nbsp;::&nbsp;
						<a target="blank" href="./orders/print/<?php echo $row['id']; ?>">wydruk</a>
						</td>
					</tr>
				    <?php
				    
				    $counter++;
				}
			}
		}
	}
	?>
	</table>
	<?php
}
?>
<?php 	
$summary = $summary->getSummary($_POST["order"], $types_default, $like_default);
?>

<form method="post" action="" name="export">
<input type="hidden" name="order[orders.status]" value="<?php echo $_POST["order"]['orders.status']; ?>" />
<input type="hidden" name="order[order_details.order_id]" value="<?php echo $_POST["order"]['order_details.order_id']; ?>" />
<input type="hidden" name="order[orders.number]" value="<?php echo $_POST["order"]['orders.number]']; ?>" />
<input type="hidden" name="order[order_details.query_id]" value="<?php echo $_POST["order"]['order_details.query_id']; ?>" />
<input type="hidden" name="order[contractor_login]" value="<?php echo $_POST["order"]['contractor_login']; ?>" />
<input type="hidden" name="order[wsk_budget.department]" value="<?php echo $_POST["order"]['wsk_budget.department']; ?>" />
<input type="hidden" name="order[wsk_mpk.mpk]" value="<?php echo $_POST["order"]['wsk_mpk.mpk']; ?>" />
<input type="hidden" name="order[order_details.name]" value="<?php echo $_POST["order"]['order_details.name']; ?>" />
<input type="hidden" name="order[order_details.symbol]" value="<?php echo $_POST["order"]['order_details.symbol']; ?>" />
<input type="hidden" name="order[order_details.producer]" value="<?php echo $_POST["order"]['order_details.producer']; ?>" />
<input type="hidden" name="order[order_details.amount]" value="<?php echo $_POST["order"]['order_details.amount']; ?>" />
<input type="hidden" name="order[order_details.tax]" value="<?php echo $_POST["order"]['order_details.tax']; ?>" />
<input type="hidden" name="order[order_details.netto_price]" value="<?php echo $_POST["order"]['order_details.netto_price']; ?>" />
<input type="hidden" name="order[netto_value]" value="<?php echo $_POST["order"]['netto_value']; ?>" />
<input type="hidden" name="order[brutto_value]" value="<?php echo $_POST["order"]['brutto_value']; ?>" />
<input type="hidden" name="order[order_informations.fv]" value="<?php echo $_POST["order"]['order_informations.fv']; ?>" />
<input type="hidden" name="order[order_informations.wm]" value="<?php echo $_POST["order"]['order_informations.wm']; ?>" />
<input type="hidden" name="order[analyst_login]" value="<?php echo $_POST["order"]['analyst_login']; ?>" />
<input type="hidden" name="order[manager_login]" value="<?php echo $_POST["order"]['manager_login']; ?>" />
<input type="hidden" name="order[receiving_login]" value="<?php echo $_POST["order"]['receiving_login']; ?>" />
<input type="hidden" name="order[orders.add_date]" value="<?php echo $_POST["order"]['orders.add_date']; ?>" />
<input type="hidden" name="order[order_informations.fv]" value="<?php echo $_POST["order"]['order_informations.fv']; ?>" />
<input type="hidden" name="order[order_informations.wm]" value="<?php echo $_POST["order"]['order_informations.wm']; ?>" />

<div id="export_columns">
	<div>
	<p><input type="checkbox" name="columns[orders.status]" value="1" checked="checked" /> status</p>
	<p><input type="checkbox" name="columns[order_details.order_id]" value="1" checked="checked" /> nr zamówienia</p>
	<p><input type="checkbox" name="columns[orders.number]" value="1" checked="checked" /> nr wewnętrzny</p>
	<p><input type="checkbox" name="columns[order_details.query_id]" value="1" checked="checked" /> nr zapytania</p>
	<p><input type="checkbox" name="columns[order_informations.fv]" value="1" checked="checked" /> FV</p>
	<p><input type="checkbox" name="columns[order_informations.wm]" value="1" checked="checked" /> WZ</p>
	<p><input type="checkbox" name="columns[contractor_login]" value="1" checked="checked" /> zamawiający</p>
	<p><input type="checkbox" name="columns[wsk_budget.department]" value="1" checked="checked" /> wydział</p>
	<p><input type="checkbox" name="columns[wsk_mpk.mpk]" value="1" checked="checked" /> MPK</p>
	<p><input type="checkbox" name="columns[order_details.name]" value="1" checked="checked" /> nazwa produktu</p>
	<p><input type="checkbox" name="columns[order_details.symbol]" value="1" checked="checked" /> indeks produktu</p>
	<p><input type="checkbox" name="columns[order_details.producer]" value="1" checked="checked" /> producent</p>
	<p><input type="checkbox" name="columns[order_details.amount]" value="1" checked="checked" /> ilość</p>
	<p><input type="checkbox" name="columns[order_details.tax]" value="1" checked="checked" /> VAT</p>
	<p><input type="checkbox" name="columns[order_details.tax_value]" value="1" checked="checked" /> wartość VAT</p>
	<p><input type="checkbox" name="columns[order_details.netto_price]" value="1" checked="checked" /> cena netto</p>
	<p><input type="checkbox" name="columns[order_details.netto_value]" value="1" checked="checked" /> wartość netto</p>
	<p><input type="checkbox" name="columns[order_details.brutto_value]" value="1" checked="checked" /> wartość brutto</p>
	<p><input type="checkbox" name="columns[orders.add_date]" value="1" checked="checked" /> data złożenia</p>
	<p class="button"><a href="./summary/export" class="export_form_submit">WYEKSPORTUJ DANE</a></p>
	</div>
	<div class="clear"></div>
</div>
	
<div id="page_content">
	<div id="export_content">
	<div id="export_box">
	<table class="export" cellspacing="0" cellpadding="0">
	<tr>
		<th style="width:160px">
			<input type="hidden" name="types[orders.status]" value="0" />
			<select name="like[orders.status]">
			<option value="4" <?php if ((int)$_POST["like"]['orders.status'] == 4) echo 'selected="selected"'; ?>>archiwum</option>
			<option value="11" <?php if ((int)$_POST["like"]['orders.status'] == 11) echo 'selected="selected"'; ?>>nadesłane</option>
			<option value="1" <?php if ((int)$_POST["like"]['orders.status'] == 1) echo 'selected="selected"'; ?>>oczekujące w Unimet</option>
			<option value="5" <?php if ((int)$_POST["like"]['orders.status'] == 5) echo 'selected="selected"'; ?>>odrzucone</option>
			<option value="3" <?php if ((int)$_POST["like"]['orders.status'] == 3) echo 'selected="selected"'; ?>>w drodze</option>
			<option value="8" <?php if ((int)$_POST["like"]['orders.status'] == 8) echo 'selected="selected"'; ?>>weryfikacja w Unimet</option>
			<option value="0" <?php if ((int)$_POST["like"]['orders.status'] == 0) echo 'selected="selected"'; ?>>wszystkie</option>
			<option value="2" <?php if ((int)$_POST["like"]['orders.status'] == 2) echo 'selected="selected"'; ?>>w trakcie realizacji</option>
			<option value="6" <?php if ((int)$_POST["like"]['orders.status'] == 6) echo 'selected="selected"'; ?>>u analityka</option>
			<option value="10" <?php if ((int)$_POST["like"]['orders.status'] == 10) echo 'selected="selected"'; ?>>u administratora</option>
			<option value="7" <?php if ((int)$_POST["like"]['orders.status'] == 7) echo 'selected="selected"'; ?>>u kierownika</option>
			<option value="9" <?php if ((int)$_POST["like"]['orders.status'] == 9) echo 'selected="selected"'; ?>>u zamawiającego</option>
			</select>
		</th>
		<th style="width:160px">
			<select name="types[order_details.order_id]">
			<option value="0" <?php if ((int)$_POST["types"]['order_details.order_id'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['order_details.order_id'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[order_details.order_id]" style="width:50px" value="<?php echo $_POST["like"]['order_details.order_id']; ?>" /> 
		</th>
		<th style="width:200px">
			<select name="types[orders.number]">
			<option value="0" <?php if ((int)$_POST["types"]['orders.number'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="-1" <?php if ((int)$_POST["types"]['orders.number'] == -1) echo 'selected="selected"'; ?>>mniejszy niż</option>
			<option value="2" <?php if ((int)$_POST["types"]['orders.number'] == 2) echo 'selected="selected"'; ?>>większy niż</option>
			</select>
			<input type="text" name="like[orders.number]" style="width:50px" value="<?php echo $_POST["like"]['orders.number']; ?>" /> 
		</th>
		<th style="width:160px">
			<select name="types[order_details.query_id]">
			<option value="0" <?php if ((int)$_POST["types"]['order_details.query_id'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['order_details.query_id'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[order_details.query_id]" style="width:50px" value="<?php echo $_POST["like"]['order_details.query_id']; ?>" /> 
		</th>
		<th style="width:160px">
			<select name="types[order_informations.fv]">
			<option value="0" <?php if ((int)$_POST["types"]['order_informations.fv'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['order_informations.fv'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[order_informations.fv]" style="width:50px" value="<?php echo $_POST["like"]['order_informations.fv']; ?>" /> 
		</th>
		<th style="width:160px">
			<select name="types[order_informations.wm]">
			<option value="0" <?php if ((int)$_POST["types"]['order_informations.wm'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['order_informations.wm'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[order_informations.wm]" style="width:50px" value="<?php echo $_POST["like"]['order_informations.wm']; ?>" /> 
		</th>
		<th style="width:160px">
			<select name="types[contractor_login]">
			<option value="0" <?php if ((int)$_POST["types"]['contractor_login'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['contractor_login'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[contractor_login]" style="width:50px" value="<?php echo $_POST["like"]['contractor_login']; ?>" /> 
		</th>
		<th style="width:160px">
			<select name="types[wsk_budget.department]">
			<option value="0" <?php if ((int)$_POST["types"]['wsk_budget.department'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['wsk_budget.department'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[wsk_budget.department]" style="width:50px" value="<?php echo $_POST["like"]['wsk_budget.department']; ?>" /> 
		</th>
		<th style="width:170px">
			<select name="types[wsk_mpk.mpk]">
			<option value="0" <?php if ((int)$_POST["types"]['wsk_mpk.mpk'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['wsk_mpk.mpk'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[wsk_mpk.mpk]" style="width:50px" value="<?php echo $_POST["like"]['wsk_mpk.mpk']; ?>" /> 
		</th>
		<th style="width:300px">
			<select name="types[order_details.name]">
			<option value="0" <?php if ((int)$_POST["types"]['order_details.name'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['order_details.name'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[order_details.name]" style="width:150px" value="<?php echo $_POST["like"]['order_details.name']; ?>" /> 
			
		</th>
		<th style="width:160px">
			<select name="types[order_details.symbol]">
			<option value="0" <?php if ((int)$_POST["types"]['order_details.symbol'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['order_details.symbol'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[order_details.symbol]" style="width:50px" value="<?php echo $_POST["like"]['order_details.symbol']; ?>" /> 
		</th>
		<th style="width:160px">
			<select name="types[order_details.producer]">
			<option value="0" <?php if ((int)$_POST["types"]['order_details.producer'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['order_details.producer'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[order_details.producer]" style="width:50px" value="<?php echo $_POST["like"]['order_details.producer']; ?>" /> 
		</th>
		<th style="width:200px">
			<select name="types[order_details.amount]">
			<option value="3" <?php if ((int)$_POST["types"]['order_details.amount'] == 3) echo 'selected="selected"'; ?>>równy</option>
			<option value="-1" <?php if ((int)$_POST["types"]['order_details.amount'] == -1) echo 'selected="selected"'; ?>>mniejszy niż</option>
			<option value="2" <?php if ((int)$_POST["types"]['order_details.amount'] == 2) echo 'selected="selected"'; ?>>większy niż</option>
			</select>
			<input type="text" name="like[order_details.amount]" style="width:50px" value="<?php echo $_POST["like"]['order_details.amount']; ?>" /> 
		</th>
		<th style="width:200px">
			<select name="types[order_details.tax]">
			<option value="3" <?php if ((int)$_POST["types"]['order_details.tax'] == 3) echo 'selected="selected"'; ?>>równy</option>
			<option value="-1" <?php if ((int)$_POST["types"]['order_details.tax'] == -1) echo 'selected="selected"'; ?>>mniejszy niż</option>
			<option value="2" <?php if ((int)$_POST["types"]['order_details.tax'] == 2) echo 'selected="selected"'; ?>>większy niż</option>
			</select>
			<input type="text" name="like[order_details.tax]" style="width:50px" value="<?php echo $_POST["like"]['order_details.tax']; ?>" /> 
		</th>
		<th style="width:110px">&nbsp;</th>
		<th style="width:200px">
			<select name="types[order_details.netto_price]">
			<option value="3" <?php if ((int)$_POST["types"]['order_details.netto_price'] == 3) echo 'selected="selected"'; ?>>równy</option>
			<option value="-1" <?php if ((int)$_POST["types"]['order_details.netto_price'] == -1) echo 'selected="selected"'; ?>>mniejszy niż</option>
			<option value="2" <?php if ((int)$_POST["types"]['order_details.netto_price'] == 2) echo 'selected="selected"'; ?>>większy niż</option>
			</select>
			<input type="text" name="like[order_details.netto_price]" style="width:50px" value="<?php echo $_POST["like"]['order_details.netto_price']; ?>" /> 
		</th>
		<th style="width:160px">&nbsp;</th>
		<th style="width:160px">&nbsp;</th>
		<th class="last" style="width:290px">
			<input type="hidden" name="types[orders.add_date]" value="4" />
			od <input class="date" type="text" name="like[orders.add_date_from]" style="width:70px" value="<?php echo $_POST["like"]['orders.add_date_from']; ?>" /> 
			do <input class="date" type="text" name="like[orders.add_date_to]" style="width:70px" value="<?php echo $_POST["like"]['orders.add_date_to']; ?>" /> 
		</th>
	</tr>
	<tr>
		<th><p alt="orders.status">Status <img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="order_details.order_id">Nr zam. <img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="orders.number">Nr wew. <img class="asc" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="order_details.query_id">Nr zap. <img class="asc" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="order_informations.fv">FV <img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th title="zaznacz wszystkie"><p alt="order_informations.wm">WZ <input type="checkbox" name="check_all" value="1" class="check_all" <?php if ($_POST["check_all"] == 1 || !is_array($_POST["selected"])) { echo 'checked="checked"'; } ?> /> <img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="contractor_login">Zamawiający <img class="asc" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="wsk_budget.department">Wydział <img class="asc" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="wsk_mpk.mpk">MPK <img class="asc" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="order_details.name">Produkt <img class="asc" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="order_details.symbol">Indeks <img class="asc" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="order_details.producer">Producent <img class="asc" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="order_details.amount">Ilość <img class="asc" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="order_details.tax">VAT <img class="asc" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="order_details.netto_price">Wartość VAT <img class="asc" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="order_details.netto_price">Cena netto <img class="asc" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="netto_value">Wartość netto <img class="asc" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="brutto_value">Wartość brutto <img class="asc" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
		<th><p alt="orders.add_date">Data złożenia <img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /><img class="desc" src="images/arrow_desc_hidden.png" alt="" /></p></th>
	</tr>
	<?php 
	while($product = mysql_fetch_array($summary)) 
   	{
       	?>
	       	<tr>
	       	<td>
	       	<?php 
	       	switch ($product['status'])
	       	{
	       		case 1: ?>oczekujące w Unimet<?php break;
	       		case 2: ?>w trakcie realizacji<?php break;
	       		case 3: ?>w drodze<?php break;
	       		case 4: ?>archiwum<?php break;
	       		case 5: ?>odrzucone<?php break;
	       		case 6: ?>u analityka<?php break;
	       		case 7: ?>u kierownika<?php break;
	       		case 8: ?>weryfikacja w Unimet<?php break;
	       		case 9: ?>u zamawiającego<?php break;
	       		case 10: ?>u administratora<?php break;
	       	}
	       	?>
	       	</td>
			<td><p><?php echo $orders->formatId($product['order_id'], $product['query_id']); ?></p></td>
			<td><p><?php echo $product['number']; ?></p></td>
			<td><p><?php echo (($product['query_id'] > 0) ? $orders->formatId($product['query_id'], $product['query_id']) : ""); ?></p></td>
			<td><?php echo $product['fv']; ?></td>
			<td><p class="relative"><?php echo $product['wm']; ?> <input type="checkbox" name="selected[]" value="<?php echo $product['detail_id']; ?>"  <?php if (in_array($product['detail_id'], $selected_default) || !is_array($_POST["selected"])) { echo 'checked="checked"'; } ?> /></p></td>
			<td><?php echo $product['contractor_login']; ?></td>
			<td><?php echo $product['department']; ?></td>
			<td><?php echo $product['mpk']; ?></td>
			<td><?php echo $product['product']; ?></td>
			<td><?php echo $product['symbol']; ?></td>
			<td><?php echo $product['producer']; ?></td>
			<td><p><?php echo (((int)$product['amount'] == $product['amount']) ? (int)$product['amount'] : $product['amount']); ?></p></td>
			<td><p><?php echo $product['tax']; ?></p></td>
			<td><p><?php echo sprintf("%0.2f", ($product['tax'] * $product['netto_price'] / 100)); ?></p></td>
			<td><p><?php echo $product['netto_price']; ?></p></td>
			<td><p><?php echo sprintf("%0.2f", ($product['netto_price'] * $product['amount'])); ?></p></td>
			<td><p><?php echo sprintf("%0.2f", ($product['brutto_price'] * $product['amount'])); ?></p></td>
			<td class="last"><?php echo $product['add_date']; ?></td>
		</tr>
       	<?php
   	}
	?>
	</table>
	</div>
	</div>

</div>
</form>
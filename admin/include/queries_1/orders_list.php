<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
</div>
<?php } else if (isset($_SESSION["communicats"]['ok'])) { ?>
<div class="form_element ok">
	<?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?>
</div>
<?php } ?>

<?php
$list = $special_orders->getSpecialOrders();
?>

<?php if (!in_array($action, array('response' , 'archiwum'))) { ?>
<table style="padding-bottom:10px;">
	<tr>
		<td class="bg_1" colspan="6" style="text-align:left; font-weight:bold;">NOWE ZAPYTANIA</td>
	</tr>
	<tr>
		<th style="width:70px">ID</th>
		<th style="width:200px">Użytkownik</th>
		<th style="width:200px">Data dodania</th>
		<th style="width:200px">Wartość brutto (zł)</th>
		<th style="width:200px">Opiekun handlowy</th>
		<th>Operacje</th>
	</tr>
	<?php
	if (is_array($list))
	{
		foreach ($list as $key => $row)
	   	{
			?>
		    <tr>
		    	<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['id']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $special_orders->textControll($row['login']); ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo date('Y-m-d H:i', strtotime($row['add_date'])); ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo sprintf("%01.2f", $row['brutto_value']); ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['unimet_representative']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>">
				<a href="./queries/edit/<?php echo $row['id']; ?>">zarządzaj</a> &nbsp;::&nbsp; 
				<a href="./queries/confirm/<?php echo $row['id']; ?>">zatwierdź</a>
				</td>
			</tr>
		    <?php
	   	}
	}
	
	if (empty($list))
	{
		?><td class="bg_1" colspan="6">brak danych</td><?php
	}
	?>
</table>
<?php } ?>

<?php
$list = $special_orders->getSpecialOrders(array(1));
?>

<?php if (!in_array($action, array('new' , 'archiwum'))) { ?>
<table style="padding-bottom:10px;">
	<tr>
		<td class="bg_1" colspan="6" style="text-align:left; font-weight:bold;">ODPOWIEDZI</td>
	</tr>
	<tr>
		<th style="width:70px">ID</th>
		<th style="width:200px">Użytkownik</th>
		<th style="width:200px">Data dodania</th>
		<th style="width:200px">Wartość brutto (zł)</th>
		<th style="width:200px">Opiekun handlowy</th>
		<th>Operacje</th>
	</tr>
	<?php
	if (is_array($list))
	{
		foreach ($list as $key => $row)
	   	{
			?>
		    <tr>
		    	<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['id']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $special_orders->textControll($row['login']); ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo date('Y-m-d H:i', strtotime($row['add_date'])); ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo sprintf("%01.2f", $row['brutto_value']); ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['unimet_representative']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>">
				<a href="./queries/edit/<?php echo $row['id']; ?>">podgląd zapytania</a>
				</td>
			</tr>
		    <?php
	   	}
	}
	
	if (empty($list))
	{
		?><td class="bg_1" colspan="6">brak danych</td><?php
	}
	?>
</table>
<?php } ?>

<?php
$list = $special_orders->getSpecialOrders(array(2,3));
?>

<?php if (!in_array($action, array('new' , 'response'))) { ?>
<table style="padding-bottom:10px;">
	<tr>
		<td class="bg_1" colspan="6" style="text-align:left; font-weight:bold;">ARCHIWUM</td>
	</tr>
	<tr>
		<th style="width:70px">ID</th>
		<th style="width:200px">Użytkownik</th>
		<th style="width:200px">Data dodania</th>
		<th style="width:200px">Wartość brutto (zł)</th>
		<th style="width:200px">Opiekun handlowy</th>
		<th>Operacje</th>
	</tr>
	<?php
	if (is_array($list))
	{
		foreach ($list as $key => $row)
	   	{
			?>
		    <tr>
		    	<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['id']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $special_orders->textControll($row['login']); ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo date('Y-m-d H:i', strtotime($row['add_date'])); ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo sprintf("%01.2f", $row['brutto_value']); ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['unimet_representative']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>">
				<a href="./queries/edit/<?php echo $row['id']; ?>">podgląd zapytania</a>
				</td>
			</tr>
		    <?php
	   	}
	}
	
	if (empty($list))
	{
		?><td class="bg_1" colspan="6">brak danych</td><?php
	}
	?>
</table>
<?php } ?>

<?php
function d2w($digits)
{
	$jednosci = Array( 'zero', 'jeden', 'dwa', 'trzy', 'cztery', 'pięć', 'sześć', 'siedem', 'osiem', 'dziewięć' );
	$dziesiatki = Array( '', 'dziesięć', 'dwadzieścia', 'trzydzieści', 'czterdzieści', 'piećdziesiąt', 'sześćdziesiąt', 'siedemdziesiąt', 'osiemdziesiąt', 'dziewiećdziesiąt' );
	$setki = Array( '', 'sto', 'dwieście', 'trzysta', 'czterysta', 'piećset', 'sześćset', 'siedemset', 'osiemset', 'dziewiećset' );
	$nastki = Array( 'dziesieć', 'jedenaście', 'dwanaście', 'trzynaście', 'czternaście', 'piętnaście', 'szesnaście', 'siedemnaście', 'osiemnaście', 'dzięwietnaście' );
	$tysiace = Array( 'tysiąc', 'tysiące', 'tysięcy' );

 	$digits = (string) $digits;
	$digits = strrev( $digits );
	$i = strlen( $digits );
	$string = '';

 	if( $i > 5 && $digits[5] > 0 ) $string .= $setki[ $digits[5] ] . ' ';
	if( $i > 4 && $digits[4] > 1 ) $string .= $dziesiatki[ $digits[4] ] . ' ';
	elseif( $i > 3 && $digits[4] == 1 ) $string .= $nastki[$digits[3]] . ' ';
	if( $i > 3 && $digits[3] > 0 && $digits[4] != 1 ) $string .= $jednosci[ $digits[3] ] . ' ';

 	$tmpStr = substr( strrev( $digits ), 0, -3 );

	if( strlen( $tmpStr ) > 0 )
	{
		$tmpInt = (int) $tmpStr;
		
		if( $tmpInt == 1 ) $string .= $tysiace[0] . ' ';
		elseif( ( $tmpInt % 10 > 1 && $tmpInt % 10 < 5 ) && ( $tmpInt < 10 || $tmpInt > 20 ) ) $string .= $tysiace[1] . ' ';
		else $string .= $tysiace[2] . ' ';
	}

 
	if( $i > 2 && $digits[2] > 0 ) $string .= $setki[$digits[2]] . ' ';
	if( $i > 1 && $digits[1] > 1 ) $string .= $dziesiatki[$digits[1]] . ' ';
	elseif( $i > 0 && $digits[1] == 1 ) $string .= $nastki[$digits[0]] . ' ';
	if( $digits[0] > 0 && $digits[1] != 1 ) $string .= $jednosci[$digits[0]] . ' ';
	if( !$string ) $string = $jednosci[0];

	return $string;
}


function slownie($kwota)
{
	$zl = array("złotych", "złoty", "złote");
	$gr = array("groszy", "grosz", "grosze");
	
	$kwotaArr = explode( ',', $kwota );
	$ostZl = substr($kwotaArr[0], -1, 1);

	switch($ostZl)
	{
		case "0": $zlote = $zl[0]; break;
		case "1": $ost2Zl = substr($kwotaArr[0], -2, 2);

			if($kwotaArr[0] == "1")
			{
				$zlote = $zl[1];
			}
			elseif($ost2Zl == "01")
			{
				$zlote = $zl[0];
			}
			else
			{
				$zlote = $zl[0];
			}
			break;

 		case "2": $ost2Zl = substr($kwotaArr[0], -2, 2);

			if($ost2Zl == "12")
			{
				$zlote = $zl[0];
			}
			else
			{
				$zlote = $zl[2];
			}
			break;
		
		case "3": $ost2Zl = substr($kwotaArr[0], -2, 2);

			if($ost2Zl == "13")
			{
				$zlote = $zl[0];
			}
			else
			{
				$zlote = $zl[2];
			}

			break;

 		case "4": $ost2Zl = substr($kwotaArr[0], -2, 2);

			if($ost2Zl == "14")
			{
				$zlote = $zl[0];
			}
			else
			{
				$zlote = $zl[2];
			}
			break;

 		case "5": $zlote = $zl[0]; break;
		case "6": $zlote = $zl[0]; break;
		case "7": $zlote = $zl[0]; break;
		case "8": $zlote = $zl[0]; break;
		case "9": $zlote = $zl[0]; break;
	}
		
	$ostGr = substr($kwotaArr[1], -1, 1);

	switch($ostGr)
	{
		case "0": $grosze = $gr[0]; break;

 		case "1": $ost2Gr = substr($kwotaArr[1], -2, 2);

 			if($kwotaArr[0] == "1")
 			{
				$grosze = $gr[1];
			}
			elseif($ost2Gr == "01")
			{
				$grosze = $gr[1];
			}
			else
			{
				$grosze = $gr[0];
			}
			break;

 		case "2": $ost2Gr = substr($kwotaArr[1], -2, 2);

			if($ost2Gr == "12")
			{
				$grosze = $gr[0];
			}
			else
			{
				$grosze = $gr[2];
			}
			break;

 		case "3": $ost2Gr = substr($kwotaArr[1], -2, 2);

			if($ost2Gr == "13")
			{
				$grosze = $gr[0];
			}
			else
			{
				$grosze = $gr[2];
			}
			break;

 		case "4": $ost2Gr = substr($kwotaArr[1], -2, 2);

			if($ost2Gr == "14")
			{
				$grosze = $gr[0];
			}
			else
			{
				$grosze = $gr[2];
			}
			break;

 		case "5": $grosze = $gr[0]; break;
		case "6": $grosze = $gr[0]; break;
		case "7": $grosze = $gr[0]; break;
		case "8": $grosze = $gr[0]; break;
		case "9": $grosze = $gr[0]; break;
	}

 	return( d2w( $kwotaArr[0] ) . ' '.$zlote.' i ' . d2w( $kwotaArr[1] ) . $grosze );
}

$list = $orders->getOrder((int)$id);
$user_names = $orders->getUsersNames();
?>

<div style="padding:5px">
	<div>
		<div style="float:left">
			<p><strong style="text-decoration:underline; font-style:italic">Odbiorca</strong></p>
			<?php if ($list[0]['parent_id'] == 8338) { ?>
			<div style="float:left; padding:0px 10px 0px 0px"><img src="../images/wsk.jpg" alt="" /></div>
			<?php } ?>
			<div style="float:left; width:230px">
				<p><strong><?php echo $orders->textControll($list[0]['user_name']); ?></strong></p>
				<p><?php echo $list[0]['post_code'].' '.$list[0]['city']; ?></p>
				<p><?php echo $list[0]['street'].' '.$list[0]['home']; if ($list[0]['flat'] != '') echo '/'.$list[0]['flat']; ?></p>
				<p>NIP: <?php echo $list[0]['identificator']; ?></p>
				
				<?php if ($list[0]['delivery_post_code'] != '') { ?>
					<p>&nbsp;</p>
					<p><strong>Adres dostawy:</strong></p>
					<p><?php echo $list[0]['delivery_post_code'].' '.$list[0]['delivery_city']; ?></p>
					<p><?php echo $list[0]['delivery_street'].' '.$list[0]['delivery_home']; if ($list[0]['delivery_flat'] != '') echo '/'.$list[0]['delivery_flat']; ?></p>
				<?php } ?>
			</div>
			<div class="font-size:0px; clear:both"></div>
		</div>
		<div style="float:right">
			<p><strong style="text-decoration:underline; font-style:italic">Dostawca</strong></p>
			<div style="float:right">
				<p><strong>Unimet Sp. z o.o.</strong></p>
				<p>35-205 Rzeszów</p>
				<p>ul. Torowa 2</p>
				<p>NIP: 517-03-59-501</p>
				<p>017 230-66-54</p>
			</div>
			<div style="float:right; padding:0px 10px 0px 0px"><img src="../images/logotype.png" alt="" /></div>
			<div class="font-size:0px; clear:both"></div>
		</div>
		<div style="font-size:0px; clear:both"></div>
	</div>
	
	<div style="font-size:0px; clear:both"></div>
	
	<p style="padding-top:50px; font-size:18px"><strong>Zamówienie nr: <?php echo $orders->formatId(( ( (int)$_GET['param'] == 'c' ) ? (int)$id - 1 : (int)$id), $list[0]['query_id']); ?></strong></p>
	
	<?php if ($list[0]['parent_id'] == 8338) { ?>	
	<div class="order_informations" style="padding-top:20px">
		<div class="path">
		<div class="element">
			<p><span>Zamawiający</span></p>
			<p><?php echo $user_names[$list[0]['contractor_login']]; ?></p>
			<p><?php echo $user_names[$list[0]['contractor_login'].'_phone']; ?></p>
			<p><?php echo $list[0]['contractor_email']; ?></p>
			<p><?php echo $list[0]['contractor_date']; ?></p>
		</div>
		<div class="arrow">
		<strong>&#187;</strong>
		</div>
		<div class="element">
			<p><span>Analityk</span></p>
			<p><?php echo $user_names[$list[0]['analyst_login']]; ?></p>
			<p><?php echo $list[0]['analyst_email']; ?></p>
			<p><?php echo (($list[0]['analyst_date'] != "1970-01-01" && !empty($list[0]['analyst_date'])) ? $list[0]['analyst_date'] : ""); ?></p>
		</div>
		<div class="arrow">
		<strong>&#187;</strong>
		</div>
	
		<?php if ($list[0]['query_id'] > 0) { ?>
			
			<div class="element">
				<p><span>Administrator</span></p>
				<p><?php echo $user_names[$list[0]['administrator_login']]; ?></p>
				<p><?php echo $list[0]['administrator_email']; ?></p>
				<p><?php echo (($list[0]['administrator_date'] != "1970-01-01" && !empty($list[0]['administrator_date'])) ? $list[0]['administrator_date'] : ""); ?></p>
			</div>
			<div class="arrow">
			<strong>&#187;</strong>
			</div>
			
		<?php } ?>
	
		<div class="element">
			<p><span>Kierownik</span></p>
			<p><?php echo $user_names[$list[0]['manager_login']]; ?></p>
			<p><?php echo $list[0]['manager_email']; ?></p>
			<p><?php echo (($list[0]['manager_date'] != "1970-01-01" && !empty($list[0]['manager_date'])) ? $list[0]['manager_date'] : ""); ?></p>
		</div>
		<div class="arrow">
		<strong>&#187;</strong>
		</div>
		<div class="element">
			<p><span>UNIMET</span></p>
			<p><?php echo ((!empty($list[0]['unimet_representative'])) ? $user_names[$list[0]['unimet_representative']] : 'brak danych'); ?></p>
			<p><?php echo $list[0]['unimet_email']; ?></p>
			<p><?php echo (($list[0]['delivery_date'] != "1970-01-01" && !empty($list[0]['delivery_date'])) ? $list[0]['delivery_date'] : ""); ?></p>
		</div>
		<div class="arrow">
		<strong>&#187;</strong>
		</div>
		<div class="element">
			<p><span>Odbierający</span></p>
			<p><?php echo $user_names[$list[0]['receiving_login']]; ?></p>
			<p><?php echo $user_names[$list[0]['receiving_login'].'_phone']; ?></p>
			<p><?php echo $list[0]['receiving_email']; ?></p>
			<p><?php echo (($list[0]['receiving_date'] != "1970-01-01" && !empty($list[0]['receiving_date'])) ? $list[0]['receiving_date'] : ""); ?></p>
		</div>
		<div class="clear"></div>
	</div>
	
	<?php } ?>
	
	<table cellspacing="0" cellpadding="0" style="padding:20px 0px 30px 0px">
		<tr>
			<td style="border:1px solid #222; width:20px; border-bottom:none; border-right:none"><strong>LP.</strong></td>
			<td style="border:1px solid #222; border-bottom:none; border-right:none"><strong>TOWAR</strong></td>
			<td style="border:1px solid #222; width:100px; border-bottom:none; border-right:none"><strong>INDEKS</strong></td>
			<td style="border:1px solid #222; border-bottom:none; border-right:none"><strong>PRODUCENT</strong></td>
			<td style="border:1px solid #222; border-bottom:none; border-right:none"><strong>JEDN. MIARY</strong></td>
			<td style="border:1px solid #222; border-bottom:none; border-right:none"><strong>ILOŚĆ</strong></td>
			<td style="border:1px solid #222; border-bottom:none; border-right:none"><strong>CENA NETTO</strong></td>
			<td style="border:1px solid #222; border-bottom:none; border-right:none"><strong>WARTOŚĆ NETTO</strong></td>
			<td style="border:1px solid #222; border-bottom:none; border-right:none"><strong>STAWKA VAT</strong></td>
			<td style="border:1px solid #222; border-bottom:none; border-right:none"><strong>PODATEK VAT</strong></td>
			<td style="border:1px solid #222; border-bottom:none; border-right:none"><strong>WARTOŚĆ BRUTTO</strong></td>
			<td style="border:1px solid #222; border-bottom:none; border-right:none"><strong>MPK</strong></td>
			<td style="border:1px solid #222; border-bottom:none; border-right:none"><strong>WG. ZAPYT.</strong></td>
			<td style="border:1px solid #222; border-bottom:none;"><strong>KOMENTARZ</strong></td>
		</tr>
		<?php
		foreach ($list as $key => $row)
		{
			$tax = ((int)$row['tax']*$row['netto_price'])/100;
			$product_tax = sprintf("%0.2f", $tax*$row['amount']);
			$netto_value = sprintf("%0.2f", $row['netto_price']*$row['amount']);
			$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
			?>
			<tr>
				<td style="border:1px solid #222; border-bottom:none; border-right:none"><?php echo ($key + 1); ?></td>
				<td style="border:1px solid #222; border-bottom:none; border-right:none"><?php echo $row['product_name']; ?></td>
				<td style="border:1px solid #222; border-bottom:none; border-right:none"><?php echo $row['symbol']; ?></td>
				<td style="border:1px solid #222; border-bottom:none; border-right:none"><?php echo $row['producer_name']; ?></td>
				<td style="border:1px solid #222; border-bottom:none; border-right:none"><?php 
				if ((int)$row['unit'])
				{
					switch ($row['unit'])
					{
						case 1: ?>kg<?php break;
						case 2: ?>komplet<?php break;
						case 3: ?>metr kwadr.<?php break;
						case 4: ?>metr sześć.<?php break;
						case 5: ?>metr bierz.<?php break;
						case 6: ?>opakowanie<?php break;
						case 7: ?>para<?php break;
						case 8: ?>sztuka<?php break;
						case 9: ?>sto sztuk<?php break;
					}
				}
				else
				{
					switch ($row['default_unit'])
					{
						case 'SZT': ?>sztuka<?php break;
						case 'KPL': ?>komplet<?php break;
						case 'KG': ?>kg<?php break;
						case 'OP': ?>opakowanie<?php break;
						case 'M2': ?>metr kwadr.<?php break;
						case 'MB': ?>metr bierz.<?php break;
						case 'M3': ?>metr sześć.<?php break;
						case 'PAR': ?>para<?php break;
						case 'STO': ?>sto sztuk<?php break;
						default: ?>&nbsp;<?php break;
					}
				}
				?></td>
				<td style="border:1px solid #222; border-bottom:none; border-right:none"><?php echo (float)$row['amount']; ?></td>
				<td style="border:1px solid #222; border-bottom:none; border-right:none"><?php echo $row['netto_price']; ?> zł</td>
				<td style="border:1px solid #222; border-bottom:none; border-right:none"><?php echo $netto_value; ?> zł</td>
				<td style="border:1px solid #222; border-bottom:none; border-right:none"><?php echo $row['tax']; ?> %</td>
				<td style="border:1px solid #222; border-bottom:none; border-right:none"><?php echo $product_tax; ?> zł</td>
				<td style="border:1px solid #222; border-bottom:none; border-right:none"><?php echo $brutto_value; ?> zł</td>
				<td style="border:1px solid #222; border-bottom:none; border-right:none"><?php echo $row['mpk']; ?></td>
				<td style="border:1px solid #222; border-bottom:none; border-right:none"><?php echo (((int)$row['query_id']) ? $row['query_id'] : "&nbsp;"); ?></td>
				<td style="border:1px solid #222; border-bottom:none;"><?php echo $row['product_info']; ?></td>
			</tr>
			<?php
			
			$weight += $row['amount'] * $row['weight'];
			
			if ($row['pallet'] == 1)
			{
				$pallets ++;
			}
			
			$netto_sum += $netto_value;
			$tax_sum += $product_tax; 
		}
		
		if ($list[0]['payment'] == 'za pobraniem ( + 6zł)')
		{
			$payment = 1;
		}
		else 
		{
			$payment = 0;
		}
			
		$transport_brutto = sprintf("%0.2f", $prices->getTransportPrice((($list[0]['transport'] == '') ? $weight : 0), $list[0]['brutto_value'], $pallets, $payment, $list[0]['user_id']));
		$transport_netto  = sprintf("%0.2f", (100*$transport_brutto/123));
		$transport_tax = sprintf("%0.2f", (23*($transport_netto)/100));
		
		$total_sum = sprintf("%0.2f", ($list[0]['brutto_value'] + $transport_brutto));
		?>
		<tr>
			<td style="border:1px solid #222; border-right:none">&nbsp;</td>
			<td style="border:1px solid #222; border-right:none">&nbsp;</td>
			<td style="border:1px solid #222; border-right:none">&nbsp;</td>
			<td style="border:1px solid #222; border-right:none">&nbsp;</td>
			<td style="border:1px solid #222; border-right:none"><strong>RAZEM</strong></td>
			<td style="border:1px solid #222; border-right:none">&nbsp;</td>
			<td style="border:1px solid #222; border-right:none"><strong><?php echo $netto_sum + $transport_netto; ?> zł</strong></td>
			<td style="border:1px solid #222; border-right:none">&nbsp;</td>
			<td style="border:1px solid #222; border-right:none"><strong><?php echo $tax_sum + $transport_tax; ?> zł</strong></td>
			<td style="border:1px solid #222; border-right:none"><strong><?php echo $total_sum; ?> zł</strong></td>
			<td style="border:1px solid #222; border-right:none">&nbsp;</td>
			<td style="border:1px solid #222; border-right:none">&nbsp;</td>
			<td style="border:1px solid #222;">&nbsp;</td>
		</tr>
	</table>
	
	<div style="padding-top:20px">
		<?php if ($list[0]['parent_id'] == 8338) { ?>
		<p>Wydział: <strong><?php echo $list[0]['department']; ?></strong></p>	
		<?php } ?>
		<p>Data złożenia zamówienia: <strong><?php echo $list[0]['add_date']; ?></strong></p>
		<p>Data dostawy: <strong><?php echo (($list[0]['delivery_date'] != "1970-01-01" && !empty($list[0]['delivery_date'])) ? $list[0]['delivery_date'] : "brak danych"); ?></strong></p>
		<p>Forma płatności: <strong><?php echo (($list[0]['parent_id'] == 8338) ? 'według umowy' : $list[0]['payment']); ?></strong></p>
		<p>Wydział: <strong><?php echo $list[0]['department']; ?></strong></p>
		<p>Numer MPK: <strong><?php echo $list[0]['mpk']; ?></strong></p>
		<p>Faktura VAT <strong><?php echo ((!empty($list[0]['fv'])) ? $list[0]['fv'] : 'brak danych'); ?></strong></p>
		<p>WZ: <strong><?php echo ((!empty($list[0]['wm'])) ? $list[0]['wm'] : 'brak danych'); ?></strong></p>	
		<p>Dokument <strong><?php echo $list[0]['document']; ?></strong></p>
	</div>
	
	<?php if (!empty($list[0]['order_comment'])) { ?>
		<p>&nbsp;</p>
		<p><strong>Komentarz użytkownika do zamówienia: </strong></p>
		<p><?php echo nl2br($list[0]['order_comment']); ?></p>
	<?php } ?>
	
	<?php 
	$delivery_point = $orders->checkDeliveryPoint((int)$id);
		
	if (null != $delivery_point)
	{
		?><p><strong>Miejsce dostawy:</strong> <?php echo $delivery_point; ?></p><?php
	}
	?>
	
</div>
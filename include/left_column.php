<div class="box">
	<div class="title"><strong>WYSZUKIWARKA</strong></div>
	<div><input type="text" name="key" value="" autocomplete="off" /></div>
					
	<div id="search_results"><p><a href="./wyszukiwarka.html">wszystkie wyniki</a></p><div></div></div>
</div>
		
<div class="box <?php echo ((!in_array($page_type, array("", "grupa"))) ? "none" : ""); ?>">			
	<?php 
	$producers_list = $producers->getProducers();

	if ($page_type != 'grupa')
	{
		$_SESSION["b2b"]['producer_id'] = 0;
	}
	?>
					
	<div class="offer_producers">
	<select name="producer_id">
	<option value="0" class="group_0">-- wszyscy producenci --</option>
	<optgroup label="producenci">
	<?php
	$producers_array = $db->mysql_fetch_all($producers_list);
	
	if (is_array($producers_array))
	{
		foreach ($producers_array as $i => $producer)
	   	{
	   		if ($producer['id'] != $producers_array[$i-1]['id'])
	   		{
	       		?><option class="group_<?php echo (int)$producer['group_id']; ?>" value="<?php echo $producer['id']; ?>" <?php if ($_SESSION["b2b"]['producer_id'] == $producer['id']) echo 'selected="selected"'; ?>><?php echo trim($producer['name']); ?></option><?php
	   		}
	   	} 
	}
	?>
	</optgroup>
	</select>
	</div>
	
	<p class="title uncompressed"><strong>GRUPY</strong> <span id="products_sum"></span> <strong class="arrow" title="kliknij aby rozwinąć">&#8595;</strong></p>
					
	<div id="groups">
	</div>
				
</div>
<!-- odblokować -->
<div class="box no_margin" id="<?php echo ((true == $_SESSION["b2b_full_offer"]) ? "narrowed" : "full"); ?>_offer" style="display:none;">
	<div class="title compressed">
		<strong><?php echo ((true == $_SESSION["b2b_full_offer"]) ? "ZAWĘŻONA OFERTA" : "PEŁNA OFERTA UNIMET"); ?> 
		<span>[<?php echo $products->getProductsSum(($_SESSION["b2b_full_offer"] == true) ? false : true);?>]</span></strong>
	</div>
</div>
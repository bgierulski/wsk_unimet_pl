<?php
class ImageUpload
{
	protected $_image;
	private $_max_height;
	private $_max_width;
	private $_image_path;
	public $extension;
	//private $_sign = '../images/znak.png';
	
	public function getExtension($image)
	{
		$table_image = explode('.', $image);
		$this->extension = strtolower($table_image[count($table_image)-1]);
	}
	
	public function uploadImage($max_width, $max_height, $image_name, $image_path, $image, $operation, $option = 0)
    {
    	
		if ($image != 0)
		{
			$this->_image = $image;
		}
		
		$this->_max_height = $max_height;
		$this->_max_width = $max_width;
		$this->_image_path = $image_path;
		
		$main_image = BASE_DIR.'../../images/temp/'.$image_name.'.'.$this->extension;
		
		if(is_uploaded_file($this->_image['tmp_name']))
		{
			move_uploaded_file($this->_image['tmp_name'], $main_image); // upload pliku do tymczasowego katalogu - ten plik będzie usuwany
		}
		
		if ($option == 0)
		{
			$size = getimagesize($main_image); // wymiary wysłanego na serwer pliku
			
			if ($operation == 0)
			{
				$this->_changeDimensions($size[0], $size[1], $image_path.$image_name.'.'.$this->extension, $main_image);
			}
			else if ($operation == 1)
			{
				$width = $this->_max_width;
				
				if ($size[0] <= $this->_max_width)
				{
					$width = $size[0];
				}
				
				$this->_resizeImage($image_path.$image_name.'.'.$this->extension, 'width', $width, $main_image, 0);
			}
			else
			{
				$height = $this->_max_height;
				
				if ($size[1] <= $this->_max_height)
				{
					$height = $size[1];
				}
				
				$this->_resizeImage($image_path.$image_name.'.'.$this->extension, 'height', $height, $main_image, 0);
			}
		}
		else
		{			
			$this->_cropImage($image_path.$image_name.'.'.$this->extension, $max_width, $max_height, $main_image);
		}
		
		/* koniec generowania zdjęć */
	}	
	
	public function deleteImage($image_name)
	{
		$main_image = BASE_DIR.'../../images/temp/'.$image_name.'.'.$this->extension;
		if(file_exists($main_image))
		{
			unlink($main_image);
		}
	}
	
	private function _changeDimensions($width, $height, $path, $main_image)
	{
		if ($width <= $this->_max_width && $height <= $this->_max_height) // generowanie miniaturki
		{	
			
			$this->_resizeImage($path, 'width', $width, $main_image, 0);
		}
		else
		{
			if ($height > $this->_max_height)
			{
				$new_width = ceil($width*($this->_max_height/$height));
				if ($new_width > $this->_max_width)
				{
					$this->_resizeImage($path, 'width', $this->_max_width, $main_image, 0);
				}
				else
				{
					$this->_resizeImage($path, 'height', $this->_max_height, $main_image, 0);
				}
			}
			else
			{
				$new_height = ceil($height*($this->_max_width/$width));
				if ($new_height > $this->_max_height)
				{
					$this->_resizeImage($path, 'height', $this->_max_height, $main_image, 0);
				}
				else
				{
					$this->_resizeImage($path, 'width', $this->_max_width, $main_image, 0);
				}
			}
		}
	}
	
	private function _cropImage($new_path, $width, $height, $main_image)
	{
		$info = getimagesize($main_image);
		
		if ($info[0] <= $width && $info[1] <= $height)
		{
			switch($info[2])
			{
				case IMAGETYPE_JPEG:
				$model = imagecreatefromjpeg($main_image); 
				imagejpeg($model, $new_path, 100);
				break;
				
				case IMAGETYPE_GIF:
				$model = imagecreatefromgif($main_image);
				imagegif($model, $new_path, 100);
				break;
				
				case IMAGETYPE_PNG:
				$model = imagecreatefrompng($main_image);
				imagepng($model, $new_path);
				break;
			}
		}
		else
		{
			$originialProportion = $info[1]/$info[0];
			$newProportion = $height/$width;
			
			if($originialProportion <= $newProportion)
			{
				$tmpWidth = floor($info[1]/$newProportion);
				$tmpFreeSpace = $info[0] - $tmpWidth;
				$positionX = (int)floor($tmpFreeSpace/2);
				
				$tmpImage = $this->imageCopy(0, 0, $positionX, 0, $width, $height, $tmpWidth, $info[1], $main_image);
			}
			else
			{
				$tmpHeight = floor($info[0] * $newProportion);
				$tmpFreeSpace = $info[1] - $tmpHeight;
				$positionY = (int)floor($tmpFreeSpace/2);

				$tmpImage = $this->imageCopy(0, 0, 0, $positionY, $width, $height, $info[0], $tmpHeight, $main_image);
			}
			
			switch($info[2])
			{
				case IMAGETYPE_JPEG:
				imagejpeg($tmpImage, $new_path, 100);
				break;
				
				case IMAGETYPE_GIF:
				imagegif($tmpImage, $new_path, 100);
				break;
				
				case IMAGETYPE_PNG:
				imagepng($tmpImage, $new_path);
				break;
			}
	
			imagedestroy($tmpImage);
		}
	}
	
	private function imageCopy($dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h, $main_image)
	{
		$info = getimagesize($main_image);
		switch($info[2])
		{
			case IMAGETYPE_JPEG:
				$model = imagecreatefromjpeg($main_image); 
				break;
				
			case IMAGETYPE_GIF:
				$model = imagecreatefromgif($main_image);
				break;
				
			case IMAGETYPE_PNG:
				$model = imagecreatefrompng($main_image);
				break;
		}
		
		imageinterlace($model, 1);
		
		if(imageistruecolor($model))
		{
			$tmpImg = imagecreatetruecolor($dst_w, $dst_h);
		}
		else 
		{
			$tmpImg = imagecreate($dst_w, $dst_h);
		}
		
		imagecopyresampled($tmpImg, $model, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
		return $tmpImg;
	}

	private function _resizeImage($new_path, $parameter, $new_value, $main_image, $sign)
	{
		$info = getimagesize($main_image);
		
		switch($info[2])
		{
			case IMAGETYPE_JPEG:
				$model = imagecreatefromjpeg($main_image); 
				break;
				
			case IMAGETYPE_GIF:
				$model = imagecreatefromgif($main_image);
				break;
				
			case IMAGETYPE_PNG:
				$model = imagecreatefrompng($main_image);
				break;
		}
		
		if ($parameter == 'height')
		{
			$new_width = ceil($info[0]*($new_value/$info[1]));
			$new_height = $new_value;
		}
		elseif ($parameter == 'width')
		{
			$new_height = ceil($info[1]*($new_value/$info[0]));
			$new_width = $new_value;
		}
		
		$new_image = imagecreatetruecolor($new_width, $new_height);
		
		switch($info[2])
		{
			case IMAGETYPE_GIF:
				imagecolortransparent($new_image, imagecolorallocate($new_image, 0, 0, 0));
				imagealphablending($new_image, true);
				imagesavealpha($new_image, true);
				break;
				
			case IMAGETYPE_PNG:
				imagealphablending($new_image, false);
				imagesavealpha($new_image, true);
				break;
		}
		
		imagecopyresampled($new_image, $model, 0, 0, 0, 0, $new_width, $new_height, $info[0], $info[1]);
		
		switch($info[2])
		{
			case IMAGETYPE_JPEG:
				imagejpeg($new_image, $new_path, 100);
				break;
				
			case IMAGETYPE_GIF:
				imagegif($new_image, $new_path, 100);
				break;
				
			case IMAGETYPE_PNG:
				imagepng($new_image, $new_path);
				break;
		}
		
		imagedestroy($model);
		imagedestroy($new_image);
	}
}
?>
<?php
class Producers extends Validators 
{
	private $_db;
	private $_user;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		$this->_user = $this->getSession('b2b_user');
	}
	
	public function checkUserType()
	{
		if (true === $this->getSession("b2b_full_offer"))
		{
			return false;
		}
		
		$result = $this->_db->query('SELECT individual, what_with_new FROM groups_tree_user WHERE user_id = '.(int)$this->_user['parent_id']) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			mysql_free_result($result);
			
			if ($row['individual'] == 1)
			{
				return true;
			}
		}
		
		return false;
	}
	
	public function getProducers($group_id = 0)
	{
		if (true == $this->checkUserType())
		{
			if ($group_id)
			{
				$query = 'SELECT groups_tree.id FROM groups_tree_status JOIN groups_tree ON groups_tree_status.comment = groups_tree.comment WHERE groups_tree.comment LIKE 
				concat((SELECT comment FROM groups_tree WHERE id = '.$group_id.'),\'%\') AND groups_tree_status.status = 1 AND groups_tree_status.user 
				= '.$this->_user['parent_id'];
				
				$result = $this->_db->query($query) or $this->_db->raise_error();
			}
			
			$query = 'SELECT DISTINCT producers.id, producers.name FROM producers_status JOIN producers ON producers_status.name = producers.name JOIN 
			products_status ON producers_status.name = products_status.producers AND producers_status.status = products_status.status AND 
			producers_status.user = products_status.user JOIN products ON products_status.id = products.id_hermes WHERE subgroup_id > 0 AND 
			producers_status.status = 1 AND producers_status.user = '.$this->_user['parent_id'];
			
			if ($group_id)
			{
				$query .= ' AND products.subgroup_id IN (';
			
				while($row = mysql_fetch_array($result)) 
		   		{
		       		$query .= $row['id'].',';
		   		}
				
				$query .= '0)';
			}
			
			$query .= ' ORDER BY producers.order ASC, producers.name ASC';
			
			$result = $this->_db->query($query) or $this->_db->raise_error();
			return $result;
		}

		if ($group_id)
		{
			$query = 'SELECT id FROM groups_tree WHERE comment LIKE concat((SELECT comment FROM groups_tree WHERE id = '.$group_id.'),\'%\')';
			$result = $this->_db->query($query) or $this->_db->raise_error();
		}
		
		$query = 'SELECT DISTINCT producers.id, producers.name FROM products JOIN producers ON products.producer_id = producers.id WHERE subgroup_id > 0 ';
		
		if ($group_id)
		{
			$query .= ' AND products.subgroup_id IN (';
			
			while($row = mysql_fetch_array($result)) 
	   		{
	       		$query .= $row['id'].',';
	   		}
			
			$query .= '0)';
		}
		
		$query .= ' ORDER BY producers.order ASC, producers.name ASC';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $result;
	}
	
	public function getNewsesProducers()
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT DISTINCT producers.id, producers.name FROM producers_status JOIN producers ON producers_status.name = producers.name JOIN 
			products_status ON producers_status.name = products_status.producers AND producers_status.status = products_status.status AND producers_status.user = 
			products_status.user JOIN products ON products_status.id = products.id_hermes JOIN newses ON products.id_hermes = newses.product_id WHERE 
			DATEDIFF(end, NOW()) >= 0 AND producers_status.status = 1 AND producers_status.user = '.$this->_user['parent_id'];
		}
		else
		{
			$query = 'SELECT DISTINCT producers.id, producers.name FROM products JOIN newses ON products.id_hermes = newses.product_id JOIN producers ON 
			products.producer_id = producers.id JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE DATEDIFF(end, NOW()) >= 0';
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		return $result;
	}
	
	public function getPromotionsProducers($promotions)
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT DISTINCT producers.id, producers.name FROM producers_status JOIN producers ON producers_status.name = producers.name JOIN 
			products_status ON producers_status.name = products_status.producers AND producers_status.status = products_status.status AND producers_status.user = 
			products_status.user JOIN products ON products_status.id = products.id_hermes WHERE producers_status.status = 1 AND producers_status.user 
			= '.$this->_user['parent_id'].' AND id_hermes IN (0,';
		}
		else
		{
			$query = 'SELECT DISTINCT producers.id, producers.name FROM products JOIN producers ON 
			products.producer_id = producers.id WHERE id_hermes IN (0,';
		}
		
		foreach ($promotions as $promotion)
		{
			$query .= (int)$promotion.',';
		}
		
		$query = substr($query, 0, -1);
		$query .= ')';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $result;
	}
	
	public function getAllProducers()
	{
		$query = 'SELECT id, name FROM producers ORDER BY `order` ASC, name ASC';
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		return $result;
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}

$producers = new Producers();
?>
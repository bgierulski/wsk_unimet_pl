<?php
if (isset($_POST['users_activity']))
{
	$_SESSION['users_activity'] = (int)$_POST['users_activity'];
}
else if (!isset($_SESSION['users_activity']))
{
	$_SESSION['users_activity'] = 2;
}

$list = $users->getUsers(trim(mysql_escape_string($_POST['key'])), $_SESSION['users_activity']);
$users_count = count($list);

$on_page = ((!empty($_POST['key'])) ? 2000 : 50);
	
$pages_count = ceil($users_count/$on_page);
	
if ((int)$_GET['id'] > 0 && (int)$_GET['id'] <= $pages_count)
{
	$page_number = (int)$_GET['id'];
}
else 
{
	$page_number = 1;
}
	
$first_user = $page_number * $on_page - $on_page;
$last_user = $page_number * $on_page - 1;
?>

<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
</div>
<?php } else if (isset($_SESSION["communicats"]['ok'])) { ?>
<div class="form_element ok">
	<?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?>
</div>
<?php } ?>

<div>
	<div style="float:left;">
		<form method="post" action="">
		<input type="text" name="key" value="" style="width:200px;" /> 
		<input type="submit" name="search_users" value="Szukaj" />
		</form>
	</div>
	<div style="float:left; padding-left:5px;">
		<form method="post" action="" name="activity">
		<select name="users_activity" style="position:relative; top:3px;" onchange="javascript: document.activity.submit();">
		<option value="2">-- wszyscy --</option>
		<option value="0" <?php if ($_SESSION['users_activity'] == 0) echo 'selected="selected"'; ?>>nieaktywni</option>
		<option value="1" <?php if ($_SESSION['users_activity'] == 1) echo 'selected="selected"'; ?>>aktywni</option>
		</select>
		</form>
	</div>
	<div style="float:right;">
		<?php 
		if ($pages_count > 1)
		{
			?>
			<div id="subpages" style="text-align:right;">
			<?php 
			for ($i=1; $i<=$pages_count; $i++)
			{
				if ($page_number == $i) { ?><strong><?php }
				?><a href="./users/list/<?php echo $i; ?>" style="color:#1178f3; text-decoration:none;"><?php echo $i; ?></a> <?php
				if ($page_number == $i) { ?></strong><?php }
			
				if ($i < $pages_count)
				{
					?><strong style="color:#ccccff;">|</strong> <?php
				}
			}
			?>
			</div>
			<?php
		}
		?>
	</div>
	<div class="clear"></div>
</div>

<table>
	<tr>
		<th>Login</th>
		<th>Nazwa użytkownika</th>
		<th>E-mail</th>
		<th>Aktywność konta</th>
		<th>Ostatnie logowanie</th>
		<th>Ilość logowań</th>
		<th>Ilość zamówień</th>
		<th>Wartość zamówień</th>
		<th style="width:290px;">Operacje</th>
	</tr>
	<?php
	foreach ($list as $key => $row)
   	{
   		if ($key >= $first_user && $key <= $last_user)
		{
	       	?>
	       	<tr>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['login']; ?></th>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['name']; ?></th>
				<td class="bg_<?php echo ($key%2)+1; ?>"><a href="mailto:<?php echo $row['email']; ?>"><?php echo $row['email']; ?></a></th>
				<td class="bg_<?php echo ($key%2)+1; ?>">
				<?php echo (($row['active'] == 1) ? 'aktywny' : 'nieaktywny'); ?>
				</td>
				<td class="bg_<?php echo ($key%2)+1; ?>">
				<?php 
				if ($row['login_date'] != '0000-00-00 00:00:00')
				{
					echo date('Y-m-d H:i', strtotime($row['login_date']));
				}
				else
				{
					?>brak danych<?php
				} 
				?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><span title="od 07-02-2011"><?php echo $row['login_counter']; ?></span></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['orders_count']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['brutto_sum']; ?> zł</td>
				<td class="bg_<?php echo ($key%2)+1; ?>">
				<a class="acitavation_js" href="./users/activate/<?php echo $row['id']; ?>"><?php echo (($row['active'] == 0) ? 'aktywuj' : 'wygeneruj hasło'); ?></a> &nbsp;::&nbsp;
				<?php if (strlen($row['comment']) > 0) { ?>
				<a target="blank" href="./users/print/<?php echo $row['id']; ?>">pokaż hasło</a> &nbsp;::&nbsp;
				<?php } ?>
				<a class="usun" href="./users/delete/<?php echo $row['id']; ?>">usuń</a> &nbsp;::&nbsp; 
				<a href="./users/edit/<?php echo $row['id']; ?>">edytuj</a>
				</td>
			</tr>
	       	<?php
		}
   	}
	?>
</table>
<?php
class Accounts extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_user_id;
	private $_login;
	private $_email;
	private $_password;
	private $_observer;
	private $_repeated_password;
	private $_registration_messages;
	private $_narrow_orders;
	private $_merchants = array();
	private $_modules = array();
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
	}
	
	public function getMerchants()
	{
		$result = $this->_db->query('SELECT id, guardian_name as name FROM merchants ORDER BY name ASC') or $this->_db->raise_error();
		
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function deleteAccount($user_id)
	{
		$result = $this->_db->query('DELETE FROM users WHERE id = '.$user_id) or $this->_db->raise_error();
		$this->_db->query('DELETE FROM user_details WHERE user_id = '.$user_id) or $this->_db->raise_error();
		
		$communicats['ok'] = 'Konto użytkownika zostało usunięte.';
		$this->setSession("communicats", $communicats);
	}
	
	public function getAccounts()
	{
		$result = $this->_db->query('SELECT id, login, email FROM users WHERE status = 0 AND active = 1') or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}

	
	public function getAccount($user_id)
	{
		$result = $this->_db->query('SELECT login, email, registration_email, narrow_orders, observer FROM users WHERE id = '.$user_id) or $this->_db->raise_error();
		$row = mysql_fetch_array($result);
		
		$values['email'] = $row['email'];
		$values['login'] = $row['login'];
		$values['observer'] = $row['observer'];
		$values['registration_messages'] = $row['registration_email'];
		$values['narrow_orders'] = $row['narrow_orders'];
		
		mysql_free_result($result);
		
		$result = $this->_db->query('SELECT merchant_id FROM user_details WHERE user_id = '.$user_id) or $this->_db->raise_error();
		
		$values['merchants_count'] = mysql_num_rows($result);
		
		while($row = mysql_fetch_array($result)) 
   		{
       		$values["merchants"][] = $row['merchant_id'];
   		}
   		
   		mysql_free_result($result);
   		
   		$result = $this->_db->query('SELECT module FROM user_cms_modules WHERE user_id = '.$user_id) or $this->_db->raise_error();
   		
		while($row = mysql_fetch_array($result)) 
   		{
       		$values["modules"][] = $row['module'];
   		}
		
		$this->setSession("values", $values);
	}
	
	public function validateAccount()
	{
		$this->clear();
		
		$this->_user_id = (int)$_POST['user_id'];
		$this->_login = $values['login'] = trim(mysql_escape_string($_POST['login']));
		$this->_email = $values['email'] = trim(mysql_escape_string($_POST['email']));
		$this->_password = $values['password'] = trim(mysql_escape_string($_POST['password']));
		$this->_repeated_password = $values['repeated_password'] = trim(mysql_escape_string($_POST['repeated_password']));
		$this->_observer = $values['observer'] = (int)$_POST['observer'];
		$this->_registration_messages = $values['registration_messages'] = (int)$_POST['registration_messages'];
		$this->_narrow_orders = $values['narrow_orders'] = (int)$_POST['narrow_orders'];
		$this->_modules = $values["modules"] = $_POST["modules"];
		
		foreach ($_POST["merchants"] as $merchant_id)
		{
			if ((int)$merchant_id && !in_array((int)$merchant_id, $this->_merchants))
			{
				array_push($this->_merchants, (int)$merchant_id);
			}
		}
		
		$values['merchants_count'] = count($this->_merchants);
		$values["merchants"] = $this->_merchants;
		
		if (!empty($this->_email) && !$this->validateEmail($this->_email))
		{
			$this->_errors++;
			$errors['email'] = 'Adres e-mail jest niepoprawny';
		}
		
		if (empty($this->_login))
		{
			$this->_errors++;
			$errors['login'] = 'Login jest wymagany';
		}
		else 
		{
			$result = $this->_db->query('SELECT login FROM users WHERE login = \''.$this->_login.'\'') or $this->_db->raise_error();
			
			if ((isset($_POST['save_changes']) && mysql_num_rows($result)>1) || (isset($_POST['add_account']) && mysql_num_rows($result)>0))
			{
				$this->_errors++;
				$errors['login'] = 'Taki login już istnieje';
			}
		}
		
		if ((empty($this->_password) && !empty($this->_repeated_password)) || (!empty($this->_password) && empty($this->_repeated_password)))
		{
			$this->_errors++;
			$errors['password'] = 'Hasło jest wymagane';
		}
		else if (!empty($this->_password) && !empty($this->_repeated_password) && strcmp($this->_password, $this->_repeated_password))
		{
			$this->_errors++;
			$errors['password'] = 'Hasło nie są identyczne';
		}
		
		if (!$this->_errors)
		{
			if (isset($_POST['save_changes']))
			{
				$this->_saveChanges();
			}
			else 
			{
				$this->_addAccount();
			}
		}
		else 
		{
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
	}
	
	private function _addAccount()
	{
		$result = $this->_db->query('SELECT SUBSTRING(MD5(RAND()),-20) as value') or $this->_db->raise_error();
		$key = mysql_fetch_array($result);
		echo 'krok 1<br />';

		$this->_db->query('INSERT INTO users VALUES (\'\', 0, \''.$this->_login.'\', MD5(CONCAT(\''.$key['value'].'\', \''.$this->_password.'\')), 
		\''.$key['value'].'\', \'\', \'\', \'\', \'\', \''.$this->_email.'\', \'\', \'\', \'\', \'\', \'\', 0, \'\', \'\', \'\', \'\', \'\', 0, 0, 0, '.$this->_observer.', 0, 0, 0, 0, 0, 1, 0, \''.date('Y-m-d').'\', 
		\'0000-00-00 00:00:00\', \'\', 0, '.$this->_registration_messages.', '.$this->_narrow_orders.', 0)') or $this->_db->raise_error();
		echo 'krok 2<br />';
		$user_id = mysql_insert_id();
		
		if (count($this->_merchants))
		{
			$this->_saveMerchants($user_id);
		}
		
		$this->_setModules($user_id);
			
		$communicats['ok'] = 'Użytkownik został dodany.';
		$this->setSession("communicats", $communicats);
		header("Location: ../accounts/list");
	}
	
	private function _saveChanges()
	{
		$result = $this->_db->query('UPDATE users SET login = \''.$this->_login.'\', email = \''.$this->_email.'\', observer = '.$this->_observer.', registration_email = '.$this->_registration_messages.', narrow_orders = '.$this->_narrow_orders.' WHERE id = '.$this->_user_id) or $this->_db->raise_error();
		
		if (!empty($this->_password))
		{
			$controll = $this->_db->query('SELECT SUBSTRING(MD5(RAND()),-20) as value') or $this->_db->raise_error();
			$key = mysql_fetch_array($controll);
			
			$this->_db->query('UPDATE users SET password = MD5(CONCAT(\''.$key['value'].'\', \''.$this->_password.'\')), `key` = \''.$key['value'].'\' WHERE id = '.$this->_user_id) or $this->_db->raise_error();
		}
		
		$this->_db->query('DELETE FROM user_details WHERE user_id = '.$this->_user_id) or $this->_db->raise_error();
		
		if (count($this->_merchants))
		{
			$this->_saveMerchants($this->_user_id);
		}
		
		$this->_setModules($this->_user_id);
				
		if ($result)
		{
			$communicats['ok'] = 'Dane użytkownika zostały wyedytowane.';
			$this->setSession("communicats", $communicats);
			header("Location: ../../accounts/list");
		}
		else 
		{
			$communicats['error'] = 'Dodanie użytkownika nie powiodło się';
			$this->setSession("communicats", $communicats);
		}
	}
	
	private function _setModules($user_id)
	{
		$this->_db->query('DELETE FROM user_cms_modules WHERE user_id = '.$user_id) or $this->_db->raise_error();
		
		if (is_array($this->_modules))
		{
			foreach ($this->_modules as $module)
			{
				$this->_db->query('INSERT INTO user_cms_modules VALUES (\'\', '.$user_id.', \''.mysql_escape_string($module).'\')') or $this->_db->raise_error();
			}
		}
	}
	
	private function _saveMerchants($user_id)
	{
		$query = 'INSERT INTO user_details VALUES ';
		
		if (is_array($this->_merchants))
		{
			foreach ($this->_merchants as $merchant_id)
			{
				$query .= '(DEFAULT, '.$user_id.', '.$merchant_id.'),';
			}
			
			$query = substr($query, 0, -1);
			
			$this->_db->query($query) or $this->_db->raise_error();
		}
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}
?>
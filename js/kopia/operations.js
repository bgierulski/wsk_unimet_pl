var bar_width = 0;
var cookie_products = $.cookie('products');
var amount = new Array(0, 0, 0, false);
var change_controll = false;

$(document).ready(function() {
	
	$("span.image a").fancybox({'type' : 'image'});
	
	$("#send_order_button").click(function(){
		
		$("form[name='send_order']").submit();
		return false;
	});
	
	scrollGroups();
	productsAmountChange();
	
	if ($("input[name='last_operation']").val() != '' && $("input[name='last_operation']").val() != undefined)
	{
		goMain($("input[name='last_operation']").val());
	}
	
	$("table.proteges_list span.amount_input input").keyup(function(e){
		
		var value = parseInt($(this).val());
		var id = parseInt($(this).attr("alt").split("#")[0]);
		
		if (!isNaN(value) && value > 0 && !isNaN(id) && id > 0)
		{
			amount = new Array(parseInt($(this).attr("alt").split("#")[1]), id, value, true);
		}
	});
	
	$("table#communicats_list a.communicat_operation").click(function(){
		
		var operation = $(this).text();
		var id = parseInt($(this).attr("id").split("_")[2]);
		
		switch (operation)
		{
			case "rozwiń":
				$("table#communicats_list div.communicat_content").css("display", "none");
				$("table#communicats_list div.communicat_short").css("display", "block");
				$("table#communicats_list a.communicat_operation").text("rozwiń");
				$(this).text("zwiń");
				$("table#communicats_list div#short_"+id).css("display", "none");
				$("table#communicats_list div#content_"+id).css("display", "block");
				break;
				
			case "zwiń":
				$(this).text("rozwiń");
				$("table#communicats_list div#short_"+id).css("display", "block");
				$("table#communicats_list div#content_"+id).css("display", "none");
				break;
		}
		
		return false;
	});
	
	$("#add_to_order").live("click",function(){
				
		$.cookie('products', '', { expires: 1, path: '/'});
		$("form[name='to_basket']").submit(); 
		$("form[name='to_basket'] .product_count").val(1);
		$("form[name='to_basket'] .product_checkbox").attr("checked",false);
		return false;
	});
	
	$(".legend").live("mouseover",function(){
		
		var id = parseInt($(this).attr("id").split("_")[1]);
		$("#legend_details_"+id).css("display","block");
		
		if (id == 1)
		{
			var position = $(this).offset();
			$("#legend_details_"+id).css({"top":parseInt(position.top + 20)+"px", "left":parseInt(position.left)+"px"});
		}
	});
	
	$(".legend").live("mouseleave",function(){
		
		var id = parseInt($(this).attr("id").split("_")[1]);		
		$("#legend_details_"+id).css("display","none");
	});
	
	$("#page_content .main_group").click(function(){
		
		var level = parseInt($(this).attr("id").split("_")[1]);
		var group_id = parseInt($(this).attr("id").split("_")[2]);
		
		showGroup($("#groups a#info_"+level+"_"+group_id));
	});
	
	$("#page_content p.main_prev a").click(function(){
		
		var level = parseInt($(this).attr("id").split("_")[2]);
		var group_id = parseInt($(this).attr("id").split("_")[3]);
		
		if (level > 0)
		{
			showGroup($("#groups a#info_"+level+"_"+group_id));
		}
		else
		{
			$("#page_content .main_group").css("display", "none");
			$("#page_content .mail_level_1").css("display", "block");
		}
		
		return false;
	});
	
	/* percents */
	
	$("input[name='percent']").keyup(function(){
		
		var tax = parseFloat($("input[type='hidden'][name='tax']").val());
		var percent = ((isNaN(parseInt($(this).val()))) ? 0 : parseInt($(this).val()));
		var netto = parseFloat($("#user_netto strong").text());
		
		$("#customer_netto strong").text(round((netto + (percent*netto/100)), 2)+" zł");
		$("#customer_brutto strong").text(round(((netto + (percent*netto/100))*tax), 2)+" zł");

	});
	
	/* users */
	/*
	if ($("input[type='hidden'][name='login_date']").val() == '0000-00-00 00:00:00')
	{
		if (false == strstr(document.location.href, "/konto,edycja.html"))
		{
			if (confirm('Prosimy o weryfikację danych osobowych'))
			{
				document.location="./konto,edycja.html";
			}
		}
	}
	*/

	/* products */
	
	$('#search_results').mouseleave(function(){
		
		setTimeout(function(){
			$('#search_results').css("display","none");
		},  1000);
	});
	
	$("a#add_to_favorite").live("click",function(){
		$(".subgroup:last").ready(function() {
			
			$(this).find("input#product_operation").val('favorite_products');
			document.to_basket.submit(); 
			
		});
		
		return false;
	});
	
	$("a#add_to_basket").live("click",function(){
		$(".subgroup:last").ready(function() {
			
			document.to_basket.submit(); 
			
		});
		
		return false;
	});
	
	$("span#select_products").live("click",function(){
		$(".subgroup:last").ready(function() {
			$(this).find(".product_checkbox").attr("checked", true);
		});
	});
	
	$("span#unselect_products").live("click",function(){
		$(".subgroup:last").ready(function() {
			$(this).find(".product_checkbox").attr("checked", false);
		});
	});
	
	checkedProducts();
	
	$(".subgroup input[type='checkbox']").live("click",function(){
		
		if (cookie_products == null)
		{
			cookie_products = '';
		}
		
		var id = parseInt($(this).val());
		var value = parseInt($(".subgroup input[type='text'][id='count_"+id+"']").val());
			
		var cookie_table = cookie_products.split('##');
		var count = cookie_table.length;
				
		for (var i=1; i<count; i++)
		{
			if (cookie_table[i].split('#')[1] == id)
			{
				cookie_products = str_replace('##'+cookie_table[i], '', cookie_products);
			}
		}
		
		if ($(this).attr("checked") == true)
		{
			cookie_products += '##'+value+'#'+id;
		}
			
		$.cookie('products', cookie_products, { expires: 1, path: '/'});
	});
	
	$(".subgroup input[type='text']").live("keyup",function(){
		
		if (cookie_products == null)
		{
			cookie_products = '';
		}
			
		var id = parseInt($(this).attr("id").split("_")[1]);
		var value = parseInt($(this).val());
		
		var cookie_table = cookie_products.split('##');
		var count = cookie_table.length;
		
		for (var i=1; i<count; i++)
		{
			if (cookie_table[i].split('#')[1] == id)
			{
				cookie_products = str_replace('##'+cookie_table[i], '', cookie_products);
			}
		}
		
		$(".subgroup:last").ready(function() {
			if (value > 0)
			{
				$(this).find("input[id='active_"+id+"']").attr("checked", true);
				cookie_products += '##'+value+'#'+id;
			}
			else
			{
				$(this).find("input[id='active_"+id+"']").attr("checked", false);
			}
		});
		
		$.cookie('products', cookie_products, { expires: 1, path: '/'});
	});
	
	/* groups list height */
	
	
	contentHeight();
	
	$(window).resize(function(){
		
		contentHeight();
	});
	
	/* marquee element */
	
	$('#horizontal_list span').each(function(){
		
		bar_width += parseInt($(this).width()) + 18;
	});
	
	$("div#horizontal_list p").css({"width":bar_width+"px"});
	$("div#horizontal_list marquee").css("width", $("div#horizontal_list").width());
    
	$('div#horizontal_list marquee').marquee('pointer').mouseover(function () {
        $(this).trigger('stop');
    }).mouseout(function () {
        $(this).trigger('start');
    }).mousemove(function (event) {
        if ($(this).data('drag') == true) 
        {
            this.scrollLeft = $(this).data('scrollX') + ($(this).data('x') - event.clientX);
        }
    }).mousedown(function (event) {
        $(this).data('drag', true).data('x', event.clientX).data('scrollX', this.scrollLeft);
    }).mouseup(function () {
        $(this).data('drag', false);
    });
	
	/* horizontal menu */
	
	$('#menu_content li').mouseover(function(){
		   
		var id = $(this).attr("id").split('_');
		var top = 127;
		var left = 0;
		
		if (id[0] == 'menu')
		{
			if (window_width < 1240)
			{
				switch (parseInt(id[1]))
				{
					case 1: left = 100; break;
					case 2: left = 288; break;
				}
			}
			else
			{
				switch (parseInt(id[1]))
				{
					case 1: left = 112; break;
					case 2: left = 322; break;
				}
			}
		}
		
		$("ul#submenu_"+id[1]).css({"display":"block", "top":top+"px", "left":left+"px"});
	});

	$('#menu_content li').mouseleave(function(){
		
		var id = $(this).attr("id").split('_');
		
		if (id[0] == 'menu')
		{
			$("ul#submenu_"+id[1]).css({"display":"none"});
		}
	});
	
	// add comment
	$("#add_comment").click(function(){
			
		$(".comment").css("display", "block");
		$(".ask_about").css("display", "none");
		
		return false;
	});
		
	// ask about product
	$("#product_menu p").click(function(){

		switch ($(this).attr("id"))
		{
			case 'description_box': 
				$("#question_content, #comments_content, #newses_content, #frequently_purchased_content").css("display","none");
				$("#product_left_column, #descritption_content").css("display","block");
				break;
			case 'comments_box': 
				$("#question_content, #product_left_column, #descritption_content, #newses_content, #frequently_purchased_content").css("display","none");
				$("#comments_content").css("display","block");
				break;
			case 'ask_about_box':
				$("#comments_content, #product_left_column, #descritption_content, #newses_content, #frequently_purchased_content").css("display","none");
				$("#question_content").css("display","block");
				break;
			case 'newses_box':
				$("#comments_content, #product_left_column, #descritption_content, #question_content, #frequently_purchased_content").css("display","none");
				$("#newses_content").css("display","block");
				break;
			case 'frequently_purchased_box':
				$("#comments_content, #product_left_column, #descritption_content, #question_content, #newses_content").css("display","none");
				$("#frequently_purchased_content").css("display","block");
				break;
		}
		
		$("#product_menu p").removeClass("active");
		$(this).addClass("active");
	});
	
	if ($("input[name='product_operation']").val() != '' && $("input[name='product_operation']").val() != undefined)
	{
		$("#product_menu p").removeClass("active");
		
		switch ($("input[name='product_operation']").val())
		{
			case 'add_comment':
				$("#question_content, #product_left_column, #descritption_content, #newses_content, #frequently_purchased_content").css("display","none");
				$("#comments_content").css("display","block");
				$("p#comments_box").addClass("active");
				break;
			case 'ask_about':
				$("#comments_content, #product_left_column, #descritption_content, #newses_content, #frequently_purchased_content").css("display","none");
				$("#question_content").css("display","block");
				$("p#ask_about_box").addClass("active");
				break;
		}
	}
	
	// payment
	$("#order_form input[name='payment']").change(function(){
			
		var id = $(this).val();
		$(".payment").css('display', 'none');
		$("#payment_"+id).css('display', 'block');	
	});
	
	// archive
	$(".see_details").click(function(){
    		
    	var id = $(this).attr('id').split('_')[1];
    		
    	$(".order_details").hide();
    	$("#details_"+id).show();
    		
    	return false;	
    });
	
	// special orders
	$("#add_special_order").click(function(){
		
		var element = '<tr class="special_product">'+$("tr.special_product").html()+'</tr>';
		
		$("tr.special_product").parent().append(element);
		$("tr.special_product:last input").val("");
		
		return false;
	});
	
	$("img.delete_query_element").live("click",function(){
		
		var element = $(this).parent().parent();
		$(element).find("input").val("");
		$(element).css("display", "none");
	});

	// input autocomplete
	$("#login_content input[name='login']").click(function(){	
		
		if ($(this).val() == 'Login') $(this).val('');
	});
	
	$("#login_content input[name='login']").blur(function(){	
		
		if ($(this).val() == '') $(this).val('Login');
	});
	
	$("#login_content input[name='password']").click(function(){	
		
		if ($(this).val() == 'Hasło') $(this).val('');
	});
	
	$("#login_content input[name='password']").blur(function(){	
		
		if ($(this).val() == '') $(this).val('Hasło');
	});
	
	/* favorite */
	
	$("a#select_all_favorites").click(function(){
		
		if ($("#product_menu #favorite_box.active").length)
		{
			$("#product_border form[name='to_basket'] input[type='checkbox'][name='actives[]']").attr("checked", true);
		}
		return false;
	});	
	
	$("a.accept_order, a.reject_order").click(function(){
		
		var id = parseInt($(this).attr("alt"));
		
		if ($(this).hasClass("accept_order"))
		{
			$("form[name='query_operations_"+id+"'] input[name='query_operation']").val('accept');
		}
		else if ($(this).hasClass("reject_order"))
		{
			$("form[name='query_operations_"+id+"'] input[name='query_operation']").val('reject');
		}
		
		$("form[name='query_operations_"+id+"']").submit();
		return false;
	});
	
	$("a.next_order_step").click(function(){
		
		$("form[name='paths']").submit();
		return false;
	});
	
	$("table.proteges_list a.reject_order").click(function(){
		
		var id = parseInt($(this).attr("alt"));
		
		$("#reject_order_form input[type='hidden'][name='id']").val(id);
		$("#reject_order_form").css("display","block");
		return false;
	});
	
	$("table#permissions_table input[type='checkbox']").change(function(){
		
		if ($(this).parent().hasClass("first"))
		{
			if (true == $(this).attr("checked"))
			{
				$(this).parent().parent().find("input[type='checkbox']").attr("checked", true);
			}
			else
			{
				$(this).parent().parent().find("input[type='checkbox']").attr("checked", false);
			}
		}
		else
		{
			if (true == $(this).attr("checked"))
			{
				$(this).parent().parent().find("td.first input[type='checkbox']").attr("checked", true);
			}
			else
			{
				var id = parseInt($(this).parent().parent().find("td.first input[type='checkbox']").val());
				
				if ($("input[type='checkbox'][name='contractors["+id+"]']").attr("checked") == false && $("input[type='checkbox'][name='analysts["+id+"]']").attr("checked") == false && 
				$("input[type='checkbox'][name='managers["+id+"]']").attr("checked") == false && $("input[type='checkbox'][name='receivings["+id+"]']").attr("checked") == false)
				{
					$(this).parent().parent().find("td.first input[type='checkbox']").attr("checked", false);
				}
			}
		}
	});
	
	$("form[name='paths'] select[name='budget[]']").change(function(){
		
		var id = parseInt($(this).val());
		
		$("form[name='paths'] select[name='analyst[]'] option, form[name='paths'] select[name='manager[]'] option, form[name='paths'] select[name='receiving[]'] option").css("display","none");
		$("form[name='paths'] select[name='analyst[]'] option[value='0'], form[name='paths'] select[name='manager[]'] option[value='0'], form[name='paths'] select[name='receiving[]'] option[value='0']").css("display","block");
		$("form[name='paths'] select[name='analyst[]'] option[alt='"+id+"'], form[name='paths'] select[name='manager[]'] option[alt='"+id+"'], form[name='paths'] select[name='receiving[]'] option[alt='"+id+"']").css("display","block");
		
		$("form[name='paths'] select[name='analyst[]'] option:selected, form[name='paths'] select[name='manager[]'] option:selected, form[name='paths'] select[name='receiving[]'] option:selected").each(function(){
		
			if (parseInt($(this).attr("alt")) != id)
			{
				$(this).attr("selected", false);
			}
		});
	});
	
	
	$("a#user_form_submit").click(function(){
		$("form[name='user']").submit();
		return false;
	});
	
	$("a#application_form_submit").click(function(){
		$("form[name='application']").submit();
		return false;
	});
	
	$("a#reject_form_submit").click(function(){
		$("form[name='reject_order']").submit();
		return false;
	});
	
	$("a#contact_form_submit").click(function(){
		$("form[name='contact']").submit();
		return false;
	});
	
	$("a#account_form_submit").click(function(){
		$("form[name='account']").submit();
		return false;
	});
	
	$("a#search_to_basket_form_submit").live("click",function(){
		$('input#product_operation').val('favorite_products');
		$("form[name='to_basket']").submit();
		return false;
	});
	
	$("a#special_products_form_submit").click(function(){
		$("form[name='special_products']").submit();
		return false;
	});
	
	$("a#delete_favorite_form_submit").click(function(){
		$("form[name='to_basket'] input[name='operation']").val("delete_favorite");
		$("form[name='to_basket']").submit();
		return false;
	});
	
	$("a#to_basket_favorite_form_submit").click(function(){
		$("form[name='to_basket'] input[name='operation']").val("to_basket");
		$("form[name='to_basket']").submit();
		return false;
	});
	
	$("input#visibility_form_submit").live("click", function(event) { 
		$("form[name='visibility']").submit();
		return false;
	});
	
	$("select#sort_form_submit").live("click", function(event) { 
		
		if (true == change_controll)
		{
			change_controll = false;
			$("form[name='sort']").submit();
		}
		else
		{
			change_controll = true;
		}

		return false;
	});
	
	$("a#ask_about_form_submit").click(function(){
		$("form[name='ask_about']").submit();
		return false;
	});
	
	$("a#add_comment_form_submit").click(function(){
		$("form[name='add_comment']").submit();
		return false;
	});
	
	$("a#product_to_basket_form_submit").click(function(){
		$("form[name='to_basket']").submit();
		window.opener.location.reload();
		return false;
	});
	
	$("a.filter_form_submit").click(function(){
		$("form[name='export']").submit();
		return false;
	});
	
	$("table.export th img").click(function(){
		
		var element = $(this).parent();

		$(element).find("img:first").attr("src", "images/arrow_asc_hidden.png");
		$(element).find("img:last").attr("src", "images/arrow_desc_hidden.png");
		
		if ($(this).hasClass("asc"))
		{
			$(this).attr("src", "images/arrow_asc.png");
		}
		else
		{
			$(this).attr("src", "images/arrow_desc.png");
		}
		
		$("input[type='hidden'][name='order["+$(this).parent().attr("alt")+"]']").val($(this).attr("class"));
	});
});
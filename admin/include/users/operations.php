<?php
if (isset($_POST['save_changes']))
{
	$users->validateUser();
}
else if (isset($_POST['active']))
{
	$users->changeActivity();
}
else if ($action == 'delete')
{
	$users->deleteUser((int)$id);
}
else if ($action == 'import')
{
	include(BASE_DIR.'users/import.php');
}
else if ($action == 'activate')
{
	$users->generatePassword((int)$id);
}
?>
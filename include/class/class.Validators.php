<?php
class Validators
{
	public function clear()
	{
		$_SESSION['errors'] = null;
		unset($_SESSION['errors']);
		$_SESSION['values'] = null;
		unset($_SESSION['values']);
	}
	
	public function formatId($id, $query_id)
	{
		if ($id <= 0)
		{
			return $id;
		}
	
		$string = (string)$id;
		$prefix = (int)substr($string, 0, 4);
		
		return ($id - ($prefix * 1000000)).'/'.substr($prefix, 2, 3).(($query_id > 0) ? '/Z' : '');
	}
	
	public function randomString($length)
	{
	  	$sings = '1234567890qwertyuiopasdfghjkklzxcvbnm';
		$string = '';

	 	for ($i=0; $i<$length; $i++)
		{
			$string .= $sings[rand()%(strlen($sings))];
		}

		return $string;
	}
	
	public function textControll($value)
	{
		return str_replace(array('\"', '"', "\'"), array('&#34;', '&#34;', "'"), $value);
	}
	
	public function text($value)
	{	
		return str_replace(array('\"', '&#34;', "\'"), array('"', '"', "'"), $value);
	}
		
	protected function setSession($name, $value) // zapis do zmiennej sesyjnej
	{
		$_SESSION[$name] = $value;	
	}
	
	protected function getSession($name) // pobiera dane z sesji
	{
		return $_SESSION[$name];
	}
	
	public function ibaseToUtf($value)
	{
		return iconv('windows-1250', 'UTF-8', $value);
	}
	
	public function validateChar($value)
	{
		return (!in_array($value, array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'W', 'Y', 'Z'))) ? 0 : 1;
	}
	
	public function validateName($value, $type)
	{
		switch($type)
		{
			case 1:
				$search = array('A', 'Ą', 'B', 'C', 'Ć', 'D', 'E', 'Ę', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'Ł', 'M', 'N', 'Ń', 'O', 'Ó', 'P', 'R', 'S', 'Ś', 'T', 'U', 'X', 'W', 'Y', 'V', 'Z', 'Ż', 'Ź');
				$replace = array('a', 'ą', 'b', 'c', 'ć', 'd', 'e', 'ę', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'ł', 'm', 'n', 'ń', 'o', 'ó', 'p', 'r', 's', 'ś', 't', 'u', 'x', 'w', 'y', 'v', 'z', 'ż', 'ź');	
				return $value[0].str_replace($search, $replace, substr($value,1));
	
			case 2:
				$search = array(' ', 'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź', 'Ą', 'Ć', 'Ę', 'Ł', 'Ń', 'Ó', 'Ś', 'Ż', 'Ź', ',', '-', ':', "'", '"', '/', '\\', '(', ')', '*', '?', '+', '|', '!');
				$replace = array('_', 'a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z', 'A', 'C', 'E', 'L', 'N', 'O', 'S', 'Z', 'Z', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.');	
				return strtolower(str_replace($search, $replace, $value));
		}

	}
	
	public function strToLower($name, $type = 0)
	{
		$search = array('A', 'Ą', 'B', 'C', 'Ć', 'D', 'E', 'Ę', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'Ł', 'M', 'N', 'Ń', 'O', 'Ó', 'P', 'R', 'S', 'Ś', 'T', 'U', 'X', 'W', 'Y', 'V', 'Z', 'Ż', 'Ź');
		$replace = array('a', 'ą', 'b', 'c', 'ć', 'd', 'e', 'ę', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'ł', 'm', 'n', 'ń', 'o', 'ó', 'p', 'r', 's', 'ś', 't', 'u', 'x', 'w', 'y', 'v', 'z', 'ż', 'ź');

		$data = explode('|', $name);
		$name = '';
				
		foreach ($data as $key => $element)
		{
			if ($key%2 == 0)
			{
				$name .= str_replace($search, $replace, $element);
			}
			else
			{
				$name .= $element;
			}
		}
		
		switch ($type)
		{
			case 0: 
				
				$position = ((in_array(substr($name,0,2), array('ą', 'Ą', 'ć', 'Ć', 'ę', 'Ę', 'ł', 'Ł', 'ń', 'Ń', 'ó', 'Ó', 'ś', 'Ś', 'ż', 'Ż', 'ź', 'Ź'))) ? 2 : 1);	
				return str_replace($replace, $search, substr($name, 0, $position)).substr($name, $position, 255);
				break;
				
			case 1: 
				
				return str_replace($search, $replace, $name);
				break;
		}
	}
	
	protected function validatePostCode($value)
	{
		return (!preg_match('/^[0-9]{2}[\-][0-9]{3}$/D', $value)) ? 0 : 1;
	}
	
	protected function validateNIP($value)
	{
		return (!preg_match('/^[0-9][0-9\- ]+[0-9]$/', $value)) ? 0 : 1;
	}
	
	protected function validateEmail($value)
	{
		return (!preg_match('/^[a-zA-Z0-9\.\-\_]+\@[a-zA-Z0-9\.\-\_]+\.[a-z]{2,4}$/D', $value)) ? 0 : 1;
	}
	
	protected function validatePhone($value)
	{
		return (!preg_match('/^[0-9\(\)\- ]+$/', $value)) ? 0 : 1;
	} 
	
	public function validateTextarea($value)
	{
		$signs = array('&nbsp;', '<br />', '<p>', '</p>');
		return (trim(str_replace($signs, '', nl2br($value))) == '') ? 0 : 1; 
	}
	
	public function validateWWW($value)
	{
		$value = trim($value);
		return (!preg_match('/^https?:\/\/[a-z0-9-]+(\.[a-z0-9-]+)+/i', $value)) ? 0 : 1;
	}
	
	public function br2nl($value)
	{
		return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $value);
	} 
}

$validators = new Validators();
?>
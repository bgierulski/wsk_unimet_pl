<ul>
<?php
switch ($controller)
{
	case 'users':
		?>
		<li><a href="./users/list" <?php if (in_array($action, array('list', 'delete' , ''))) echo 'class="active"'; ?>>Lista użytkowników</a> <span>|</span></li>
		<li><a href="./users/import" <?php if ($action == 'import') echo 'class="active"'; ?>>Import użytkowników</a> <span>|</span></li>
		<li><a href="./include/users/export.php" <?php if ($action == 'export') echo 'class="active"'; ?> target="blank">Eksport danych</a></li>
		<?php
		break;
	case 'pages':
		?>
		<li><a href="./pages/list" <?php if (in_array($action, array('list', 'delete' , ''))) echo 'class="active"'; ?>>Lista podstron</a></li>
		<?php
		break;
	case 'producers':
		?>
		<li><a href="./producers/list" <?php if (in_array($action, array('list', 'delete' , ''))) echo 'class="active"'; ?>>Lista producentów</a> <span>|</span></li>
		<li><a href="./producers/new" <?php if ($action == 'new') echo 'class="active"'; ?>>Nowy producent</a></li>
		<?php
		break;
	case 'header_products':
		?>
		<li><a href="./header_products/list" <?php if (in_array($action, array('list', 'delete' , ''))) echo 'class="active"'; ?>>Lista produktów w nagłówku</a> <span>|</span></li>
		<li><a href="./header_products/new" <?php if ($action == 'new') echo 'class="active"'; ?>>Nowy produkt w nagłówku</a></li>
		<?php
		break;
	case 'orders':
		?>
		<li><a href="./orders/all" <?php if ($action == 'all') echo 'class="active"'; ?>>Wszystkie zamówienia</a> <span>|</span></li>
		<li><a href="./orders/waiting" <?php if ($action == 'waiting') echo 'class="active"'; ?>>Zamówienia oczekujące</a> <span>|</span></li>
		<li><a href="./orders/ongoing" <?php if ($action == 'ongoing') echo 'class="active"'; ?>>Zamówienia w trakcie realizacji</a> <span>|</span></li>
		<li><a href="./orders/realized" <?php if ($action == 'realized') echo 'class="active"'; ?>>Zamówienia zrealizowane</a> <span>|</span></li>
		<li><a href="./orders/deleted" <?php if ($action == 'deleted') echo 'class="active"'; ?>>Zamówienia odrzucone</a> <span>|</span></li>
		<li><a href="./orders/archiwum" <?php if ($action == 'archiwum') echo 'class="active"'; ?>>Archiwum</a> <span>|</span></li>
		<li><a href="./orders/return" <?php if ($action == 'return') echo 'class="active"'; ?>>Zwroty</a> <span>|</span></li>
		<li><a href="./orders/budgets" <?php if ($action == 'budgets') echo 'class="active"'; ?>>Budżety</a></li>
		<?php
		break;
	case 'queries':
		?>
		<li><a href="./queries/all" <?php if ($action == 'all') echo 'class="active"'; ?>>Wszystkie zapytania</a> <span>|</span></li>
		<li><a href="./queries/waiting" <?php if ($action == 'waiting') echo 'class="active"'; ?>>Zapytania oczekujące</a> <span>|</span></li>
		<li><a href="./queries/ongoing" <?php if ($action == 'ongoing') echo 'class="active"'; ?>>Zapytania w trakcie realizacji</a> <span>|</span></li>
		<li><a href="./queries/realized" <?php if ($action == 'realized') echo 'class="active"'; ?>>Zapytania zrealizowane</a> <span>|</span></li>
		<li><a href="./queries/deleted" <?php if ($action == 'deleted') echo 'class="active"'; ?>>Zapytania odrzucone</a> <span>|</span></li>
		<li><a href="./queries/archiwum" <?php if ($action == 'archiwum') echo 'class="active"'; ?>>Archiwum</a> <span>|</span></li>
		<li><a href="./queries/submitted" <?php if ($action == 'submitted') echo 'class="active"'; ?>>Zapytania nadesłane</a></li>
		<?php /* ?>
		<li><a href="./queries/list" <?php if (in_array($action, array('list', 'delete' , ''))) echo 'class="active"'; ?>>Wszystkie zapytania</a> <span>|</span></li>
		<li><a href="./queries/new" <?php if ($action == 'new') echo 'class="active"'; ?>>Nowe zapytania</a> <span>|</span></li>
		<li><a href="./queries/response" <?php if ($action == 'response') echo 'class="active"'; ?>>Odpowiedzi</a> <span>|</span></li>
		<li><a href="./queries/archiwum" <?php if ($action == 'archiwum') echo 'class="active"'; ?>>Archiwum</a></li>
		<?php */
		break;
	case 'accounts':
		?>
		<li><a href="./accounts/list" <?php if (in_array($action, array('list', 'delete' , ''))) echo 'class="active"'; ?>>Lista użytkowników</a> <span>|</span></li>
		<li><a href="./accounts/new" <?php if ($action == 'new') echo 'class="active"'; ?>>Nowy użytkownik</a></li>
		<?php
		break;
	case 'merchants':
		?>
		<li><a href="./merchants/list" <?php if (in_array($action, array('list', ''))) echo 'class="active"'; ?>>Lista handlowców</a></li>
		<?php
		break;
	case 'concessions':
		?>
		<li><a href="./concessions/list" <?php if (in_array($action, array('list', ''))) echo 'class="active"'; ?>>Lista pracowników</a></li>
		<?php
		break;
	case 'main_page':
		?>
		<li><a href="./main_page/list" <?php if (in_array($action, array('list', 'delete' , ''))) echo 'class="active"'; ?>>Lista grup na stronie głównej</a> <span>|</span></li>
		<li><a href="./main_page/new" <?php if ($action == 'new') echo 'class="active"'; ?>>Nowa grupa na stronie głównej</a></li>
		<?php
		break;
	case 'informations':
		?>
		<li><a href="./informations/list" <?php if (in_array($action, array('list', 'delete' , ''))) echo 'class="active"'; ?>>Lista komunikatów</a> <span>|</span></li>
		<li><a href="./informations/new" <?php if ($action == 'new') echo 'class="active"'; ?>>Nowy komunikat</a></li>
		<?php
		break;
	case 'restrictions':
		?>
		<li><a href="./restrictions/list" <?php if (in_array($action, array('list', 'delete' , ''))) echo 'class="active"'; ?>>Lista ograniczeń</a> <span>|</span></li>
		<li><a href="./restrictions/new" <?php if ($action == 'new') echo 'class="active"'; ?>>Nowe ograniczenie</a></li>
		<?php
		break;
	case 'summary':
		?>
		<li><a href="./summary" class="filter_form_submit">Filtruj</a> <span>|</span></li>
		<li><a href="./summary/export" target="blank" class="export_form_columns">Wybierz kolumny do eksportu</a></li>
		<?php
		break;
	case 'b2b':
		?>
		<li><a href="./b2b/list" <?php if (in_array($action, array('list', ''))) echo 'class="active"'; ?>>B2B - szczegóły</a></li>
		<?php
		break;
}
?>
</ul>
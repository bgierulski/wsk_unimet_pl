<?php
$list = $accounts->getAccounts();
?>

<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
</div>
<?php } else if (isset($_SESSION["communicats"]['ok'])) { ?>
<div class="form_element ok">
	<?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?>
</div>
<?php } ?>

<table>
	<tr>
		<th>Login</th>
		<th>Adres e-mail</th>
		<th>Operacje</th>
	</tr>
	<?php  		
	if (is_array($list))
	{
		foreach ($list as $key => $row)
	   	{
	       	?>
	       	<tr>
	       		<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['login']; ?></th>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['email']; ?></th>
				<td class="bg_<?php echo ($key%2)+1; ?>">
				<a href="./accounts/edit/<?php echo $row['id']; ?>">edytuj</a> :: 
				<a class="usun" href="./accounts/delete/<?php echo $row['id']; ?>">usun</a>
				</th>
			</tr>
	       	<?php
	   	}
	}
	?>
</table>

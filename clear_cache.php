<?php
include(dirname(__FILE__).'/include/config.php');

function diff_date($start, $end="NOW")
{
	$sdate = strtotime($start);
	$edate = strtotime($end);
	
	$time = $edate - $sdate;
	        
	$pday = ($edate - $sdate) / 86400;
	$preday = explode('.',$pday);
	
	return $preday[0];
}

$cashe_dir = BASE_DIR.'../sql_cashe/';
	
if ($handle = opendir($cashe_dir)) 
{	
	while (false !== ($file = readdir($handle))) 
	{ 
		if (!in_array($file, array('.', '..')))
		{
			$file_details = explode('.', $file);

			//if (diff_date($file_details[1]) > 0) 
			if (substr($file_details[0], 0, 10) == "individual") 
			{ 
				unlink($cashe_dir.$file);
			}
		}
	}
		
	closedir($handle); 
}
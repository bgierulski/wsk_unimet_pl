<?php
class Merchants extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_merchant_id;
	private $_merchant_name;
	private $_merchant_image;
	private $_merchant_email;
	private $_merchant_phone;
	private $_merchant_description;
	private $_guardian_image;
	private $_guardian_name;
	private $_guardian_email;
	private $_guardian_phone;
	private $_guardian_description;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
	}
	
	public function getMerchants()
	{
		$result = $this->_db->query('SELECT id, name, email, phone, guardian_name, guardian_email, guardian_phone FROM merchants ORDER BY id ASC') or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getMerchant($merchant_id)
	{
		$result = $this->_db->query('SELECT id, name, email, phone, description, guardian_name, guardian_email, guardian_phone, guardian_description FROM merchants WHERE id = '.$merchant_id) or $this->_db->raise_error();
		$row = mysql_fetch_array($result);
		
		$values['merchant_name'] = $row['name'];
		$values['merchant_email'] = $row['email'];
		$values['merchant_phone'] = $row['phone'];
		$values['merchant_description'] = $row['description'];
		$values['guardian_name'] = $row['guardian_name'];
		$values['guardian_email'] = $row['guardian_email'];
		$values['guardian_phone'] = $row['guardian_phone'];
		$values['guardian_description'] = $row['guardian_description'];
		
		$values['merchant_image'] = $this->getImage($merchant_id, 'merchant');
		$values['guardian_image'] = $this->getImage($merchant_id, 'guardian');
		
		$this->setSession("values", $values);
	}
	
	public function validateMerchant()
	{
		$this->clear();
		
		$this->_merchant_id = (int)$_POST['merchant_id'];
		$this->_merchant_name = $values['merchant_name'] = trim(mysql_escape_string($_POST['merchant_name']));
		$this->_merchant_image = $_FILES["merchant_image"];
		$this->_merchant_email = $values['merchant_email'] = trim(mysql_escape_string($_POST['merchant_email']));
		$this->_merchant_phone = $values['merchant_phone'] = trim(mysql_escape_string($_POST['merchant_phone']));
		$this->_merchant_description = $values['merchant_description'] = trim($_POST['merchant_description']);
		$this->_guardian_image = $_FILES["guardian_image"];
		$this->_guardian_name = $values['guardian_name'] = trim(mysql_escape_string($_POST['guardian_name']));
		$this->_guardian_email = $values['guardian_email'] = trim(mysql_escape_string($_POST['guardian_email']));
		$this->_guardian_phone = $values['guardian_phone'] = trim(mysql_escape_string($_POST['guardian_phone']));
		$this->_guardian_description = $values['guardian_description'] = trim($_POST['guardian_description']);
		
		if (empty($this->_merchant_name))
		{
			$this->_errors++;
			$errors['merchant_name'] = 'Imię i nazwisko handlowca jest wymagane';
		}
		
		if ($this->_merchant_image['name'] != '' && !$this->checkImage($this->_merchant_image))
		{
			$this->_errors++;
			$errors['merchant_image'] = 'Oczeklwane rozszerzenia pliku to: gif, jpg, png';
		}
		
		if ($this->_guardian_image['name'] != '' && !$this->checkImage($this->_guardian_image))
		{
			$this->_errors++;
			$errors['guardian_image'] = 'Oczeklwane rozszerzenia pliku to: gif, jpg, png';
		}
		
		if (empty($this->_merchant_email) || !$this->validateEmail($this->_merchant_email))
		{
			$this->_errors++;
			$errors['merchant_email'] = 'E-mail handlowca jest wymagany';
		}
		
		if (empty($this->_merchant_phone))
		{
			$this->_errors++;
			$errors['merchant_phone'] = 'Tytuł do handlowca jest wymagany';
		}
		
		if (empty($this->_guardian_name))
		{
			$this->_errors++;
			$errors['guardian_name'] = 'Imię i nazwisko opiekuna jest wymagane';
		}
		
		if (empty($this->_guardian_email) || !$this->validateEmail($this->_guardian_email))
		{
			$this->_errors++;
			$errors['guardian_email'] = 'E-mail opiekuna jest wymagany';
		}
		
		if (empty($this->_guardian_phone))
		{
			$this->_errors++;
			$errors['guardian_phone'] = 'Telefon do opiekuna jest wymagany';
		}
		
		if (!$this->_errors)
		{
			if (isset($_POST['add_merchant']))
			{
				$this->_addMerchant();
			}
			else 
			{
				$this->_saveChanges();
			}
		}
		else 
		{
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
		
	}
	
	private function _addMerchant()
	{		
	}
	
	private function _saveChanges()
	{
		$this->_db->query('UPDATE merchants SET name = \''.$this->_merchant_name.'\', email = \''.$this->_merchant_email.'\', 
		phone = \''.$this->_merchant_phone.'\', description = \''.mysql_escape_string($this->_merchant_description).'\', guardian_name = \''.$this->_guardian_name.'\', 
		guardian_email = \''.$this->_guardian_email.'\', guardian_phone = \''.$this->_guardian_phone.'\', guardian_description = 
		\''.mysql_escape_string($this->_guardian_description).'\' WHERE id = '.$this->_merchant_id) or $this->_db->raise_error();
		
		if ($this->_merchant_image['name'] != '')
		{
			$this->deleteImage($this->_merchant_id, 'merchant', 1);
			$this->_saveImage($this->_merchant_id, 'merchant');
		}
		
		if ($this->_guardian_image['name'] != '')
		{
			$this->deleteImage($this->_merchant_id, 'guardian', 1);
			$this->_saveImage($this->_merchant_id, 'guardian');
		}
		
		$communicats['ok'] = 'Dane handlowca zostały wyedytowane';
		$this->setSession("communicats", $communicats);
		header("Location: ../../merchants/list");
	}
	
	public function getImage($merchant_id, $type)
	{
		$result = $this->_db->query('SELECT '.$type.'_image FROM merchants WHERE id = '.$merchant_id) or $this->_db->raise_error();
		
		$row = mysql_fetch_array($result);
		return $row[$type.'_image'];
	}
	
	public function deleteImage($merchant_id, $type, $option = 0)
	{
		$image = $this->getImage($merchant_id, $type);
		$path = BASE_DIR.'../../images/merchants/'.$merchant_id.'/'.$image;
		
		if (file_exists($path))
		{
			@unlink($path);
		}
		
		$this->_db->query('UPDATE merchants SET '.$type.'_image = \'\' WHERE id  = '.$merchant_id);
		
		if (!$option)
		{
			header("Location: ./");
		}
	}
	
	private function _saveImage($merchant_id, $type)
	{
		if (!is_dir(BASE_DIR.'../../images/merchants/'.$merchant_id))
		{
			mkdir(BASE_DIR.'../../images/merchants/'.$merchant_id);
		}
		
		$image = new ImageUpload();
		
		$symbols = '1234567890qwertyuiopasdfghjkklzxcvbnm';
   		$name = '';

		for ($i=0; $i<10; $i++)
		{
			$name .= $symbols[rand()%(strlen($symbols))];
		}
		
		switch ($type)
		{
			case 'merchant': $file = $this->_merchant_image; break;
			case 'guardian': $file = $this->_guardian_image; break;
		}
		
		$image->getExtension($file['name']);
		
		
		
		$image->uploadImage(60, 60, $name, BASE_DIR.'../../images/merchants/'.$merchant_id.'/', $file, 0, 1);
		$image->deleteImage($name, 1);
			
		$this->_db->query('UPDATE merchants SET '.$type.'_image = \''.$name.'.'.$image->extension.'\' WHERE id = '.$merchant_id) or $this->_db->raise_error();
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}
?>
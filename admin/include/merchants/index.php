<div id="title_element">
	<p>
	<strong>
	<?php
	switch ($action)
	{
		case 'edit': ?>Edycja danych handlowca<?php break;
		default: ?>Lista handlowców<?php break;
	}
	?>
	</strong></p>
</div>

<div id="page_padding">
<?php
switch ($action)
{
	case 'edit': include('new_merchant.php'); break;
	default: include('merchants_list.php'); break;
}
?>
</div>
var window_width, window_height, cookie_traces;

function contentHeight()
{
	setTimeout(function(){	

		if (typeof (window.innerWidth) == 'number') 
		{
			window_width = window.innerWidth; 
			window_height = window.innerHeight;
		} 
		else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) 
		{
			window_width = window.document.documentElement.clientWidth; 
			window_height = window.document.documentElement.clientHeight;
		} 
		else if (document.body && (document.body.clientWidth || document.body.clientHeight)) 
		{
			window_width = document.body.clientWidth; 
			window_height = document.body.clientHeight;
		}

		if ($("#export_content").length)
		{
			var column_height = 45;
			
			$("#left_column .box").each(function(){
				column_height += $(this).height();
			});
			
			$("#export_content").css({"width":$("#page_padding").width()+"px", "height":(window_height - 290)+"px"});
			$("#export_content div:first").css({"width":$("#export_content table:first").width()});
		}
		
	},  100);
}

contentHeight();

$(function(){
	
	$("input.date").datepicker();
	
	contentHeight();
	
	$(window).resize(function(){
		
		contentHeight();
	});
	
	$(".usun").click(function(){
	
    	return confirm('Czy na pewno chcesz usunąć?');    	
	});
	
	$(".activation_js").click(function(){
		
    	return confirm('Czy na pewno chcesz aktywować konto?');    	
	});
	
	// obsługa formularzy
	$("select").change(function(){
		 	
		if ($(this).attr("name") == "type")
		{				
			if ($(this).attr("value") == 1)
			{
				$("#type_2").css("display", "none");
				$("#type_1").css("display", "block");
			}
			else if ($(this).attr("value") == 2)
			{
				$("#type_1").css("display", "none");
				$("#type_2").css("display", "block");
			}
		}
	});
	
	// dodanie pól do formularzy
    
    $(".add_merchant").live("click",function(){
		
    	var count;
    	
		$(".merchant strong.add_merchant").html('');
		$(".merchant:last").after('<div class="form_element merchant list">'+$(".merchant:last").html()+'</div>');
		$(".merchant:last strong.add_merchant").text('dodaj kolejnego doradcę');
			
		count = parseInt($("input[name='merchants_count']").val())+1;
						
		$("input[name='merchants_count']").attr('value', count);
		$(".merchant:last").attr('id', 'select_merchant_'+count);
		$(".merchant:last select").val(0);
		var counter = 0;
		
		$(".merchant").each(function(){ 
			
			if ($(this).css("display") == 'block')
			{
				counter++;
	
		   		if (counter < count)
		   		{
		   			$(this).find(".merchant_action").css("visibility", "visible");
		   		}
			}
		});
	});
	
	$(".merchant_action").live("click",function(){
		
		var id = 0;
		var count = 0;
		var number = $(this).parent().parent().attr("id").split("_")[2];
		
		$(".merchant").each(function(){ 
			
			id = parseInt($(this).attr("id").split('_')[2]);
			
			if (number == id)
			{
				$(this).css("display","none");
				$(this).html('');
			}
			
			if (id > number)
			{
				$(this).attr('id', 'select_merchant_'+(id-1));
			}
		});
		
		count = parseInt($("input[name='merchants_count']").val())-1;
		$("input[name='merchants_count']").attr('value', count);
	});
	
	$("#restriction_form select[name='type']").change(function(){ 
		
		$("#restriction_form #group_element, #restriction_form #producent_element, #restriction_form #product_element").css("display", "none");
		
		switch (parseInt($(this).val()))
		{
			case 1:
				$("#restriction_form #group_element").css("display", "block");
				$("#restriction_form #group_element input").val("");
				break;
				
			case 2:
				$("#restriction_form #producent_element").css("display", "block");
				$("#restriction_form #producent_element input").val("");
				break;
				
			case 3:
				$("#restriction_form #product_element").css("display", "block");
				$("#restriction_form #product_element input").val("");
				break;
			}
	});
	
	$("#restriction_form input[name='user']").keyup(function(){ 
		
		var key = $(this).val();
		
		if (key.length > 2)
		{
			$.ajax({
				
				type: "POST",
				url: "include/restrictions/xml.php?operation=find_users",
				data: {key: $(this).val(), operation:"find_users"},
				dataType: "xml",
			  
				success: function(xml) 
				{
					var data = '';
					
					$(xml).find('word').each(function()
					{
						data += $(this).text()+'###';
					});
									
					var table = data.split("###");
					
					$("#restriction_form input[name='user']").autocomplete({
						source: table
					});
				}
			});
		}
	});
	
	$("#restriction_form input[name='group']").keyup(function(){ 
		
		var key = $(this).val();
		
		if (key.length > 2)
		{
			$.ajax({
				
				type: "POST",
				url: "include/restrictions/xml.php?operation=find_groups",
				data: {key: $(this).val(), operation:"find_groups"},
				dataType: "xml",
			  
				success: function(xml) 
				{
					var data = '';
					
					$(xml).find('word').each(function()
					{
						data += $(this).text()+'###';
					});
									
					var table = data.split("###");
					
					$("#restriction_form input[name='group']").autocomplete({
						source: table
					});
				}
			});
		}
	});
	
	$("#restriction_form input[name='producer']").keyup(function(){ 
		
		var key = $(this).val();
		
		if (key.length > 2)
		{
			$.ajax({
				
				type: "POST",
				url: "include/restrictions/xml.php?operation=find_producers",
				data: {key: $(this).val(), operation:"find_producers"},
				dataType: "xml",
			  
				success: function(xml) 
				{
					var data = '';
					
					$(xml).find('word').each(function()
					{
						data += $(this).text()+'###';
					});
									
					var table = data.split("###");
					
					$("#restriction_form input[name='producer']").autocomplete({
						source: table
					});
				}
			});
		}
	});
	
	$("#restriction_form input[name='product']").keyup(function(){ 
		
		var key = $(this).val();
		
		if (key.length > 2)
		{
			$.ajax({
				
				type: "POST",
				url: "include/restrictions/xml.php?operation=find_products",
				data: {key: $(this).val(), operation:"find_products"},
				dataType: "xml",
			  
				success: function(xml) 
				{
					var data = '';
					
					$(xml).find('word').each(function()
					{
						data += $(this).text()+'###';
					});
									
					var table = data.split("###");
					
					$("#restriction_form input[name='product']").autocomplete({
						source: table
					});
				}
			});
		}
	});
	
	$("span#regroup_order").click(function(){ 
		$("input[type='hidden'][name='operation']").val('regroup_order');
		$("form[name='order']").submit();
	});
	
	$("#return_products").click(function(){ 
		$("input[type='hidden'][name='operation']").val('return_products');
		$("form[name='order']").submit();
		return false;
	});
	
	$("span#save_order").click(function(){ 
		$("input[type='hidden'][name='operation']").val('save_changes');
		$("form[name='order']").submit();
	});
	
	$("span#move_to_archive").click(function(){ 
		$("input[type='hidden'][name='operation']").val('move_to_archive');
		$("form[name='order']").submit();
	});
	
	$("td.order_replacement .add_replacement img").click(function(){
		
		var element_id = parseInt($(this).parent().parent().parent().find("input[name='ids[]']").val());

		$("#replacment_box input[name='element_id']").val(element_id);
		$("#replacment_box").css("display","block");
	});
	
	$("#replacment_box strong").click(function(){
		$("#replacment_box").css("display","none");
		return false;
	});
	
	$("#replacment_box input").keyup(function(){ 
		
		var key = $(this).val();
		
		if (key.length > 2)
		{
			$.ajax({
				
				type: "POST",
				url: "include/orders/xml.php?operation=find_replacments",
				data: {key: $(this).val(), operation:"find_replacments"},
				dataType: "xml",
			  
				success: function(xml) 
				{
					var data = '';
					
					$(xml).find('word').each(function()
					{
						data += $(this).text()+'###';
					});
									
					var table = data.split("###");

					$("#replacment_box input").autocomplete({
						source: table
					});
				}
			});
		}
	});
	
	$("#replacment_box img").click(function(){
		
		var element_id = parseInt($("#replacment_box input[type='hidden']").val());
		
		$.ajax({
			
			type: "POST",
			url: "include/orders/xml.php?operation=check_replacment",
			data: {name: $("#replacment_box input[type='text']").val(), operation:"check_replacment"},
			dataType: "xml",
		  
			success: function(xml) 
			{
				if ($(xml).find("id").length)
				{
					$("input[name='elements[]'][value='"+element_id+"']").parent().parent().find("td.order_replacement input").val($(xml).find("id").text());
					$("input[name='elements[]'][value='"+element_id+"']").parent().parent().find("td.order_replacement span").text($(xml).find("name").text());
					$("input[name='elements[]'][value='"+element_id+"']").parent().parent().find("td.order_replacement span").css("display","inline");
					$("input[name='elements[]'][value='"+element_id+"']").parent().parent().find("td.order_replacement strong.delete_replacement").css("display","inline");
				}
				
				$("#replacment_box").css("display","none");
			}
		});
	});
	
	$("td.order_replacement .delete_replacement img").click(function(){
		
		$(this).parent().parent().find("span").text("");
		$(this).parent().parent().find("input").val(0);
		$(this).parent().parent().find("strong.delete_replacement").css("display","none");
		$(this).parent().parent().find("span").css("display","none");
	});
	
	$("table.orders_list .options a").click(function(){
	
		var id = parseInt($(this).parent().attr("id").split("_")[1]);
		
		if (id > 0 && id != undefined)
		{
			cookie_traces = $.cookie('cookie_traces');
			cookie_traces += '.'+id;
			$.cookie('cookie_traces', cookie_traces, { expires: 1, path: '/'});
		}
	});
	
	if ($("table.orders_list").length)
	{
		cookie_traces = $.cookie('cookie_traces');
		
		if (cookie_traces != null)
		{
			var data = cookie_traces.split(".");
			
			for (var i=0; i<data.length; i++)
			{
				$("table.orders_list .options#order_"+data[i]).parent().find("td").removeClass("bg_3").addClass("bg_3");
			}
		}
	}
	
	$("form[name='order'] img.info_change").click(function(){
	
		var id = parseInt($(this).attr("id").split("_")[1]);
		var location = $(this).position();
		
		$.ajax({
			
			type: "POST",
			url: "include/orders/xml.php?operation=get_trader_comment",
			data: {id:id, operation:"get_trader_comment"},
			dataType: "xml",
		  
			success: function(xml) 
			{
				if ($(xml).find("id").length)
				{
					$("form[name='order'] div#trader_comment input[type='hidden'][name='detail_id']").val($(xml).find("id").text());
					$("form[name='order'] div#trader_comment textarea").val($(xml).find("comment").text());
					$("form[name='order'] div#trader_comment").css({"display":"block", "left":parseInt(location.left - 237)+"px", "top":parseInt(location.top + 15)+"px"});
				}
				
				
			}
		});
	});
	
	$("form[name='order'] div#trader_comment a.submit").click(function(){

		$.ajax({
			
			type: "POST",
			url: "include/orders/xml.php?operation=set_trader_comment",
			data: {id:parseInt($("form[name='order'] input[name='detail_id']").val()), comment:$("form[name='order'] textarea[name='trader_comment']").val(), operation:"set_trader_comment"},
			dataType: "xml",
		  
			success: function(xml) 
			{
				$("div#trader_comment").css("display", "none");
			}
		});

		return false;
	});
	
	$("form[name='order'] div#trader_comment img.close").click(function(){
		$("div#trader_comment").css("display", "none");
	});
	
	$(".orders_list strong.show, orders_list strong.hide").click(function(){
			
		if ($(this).hasClass("show"))
		{
			$(this).text('UKRYJ');
			$(this).removeClass("show").addClass("hide");
			$(this).parent().parent().parent().find("tr").css("display","table-row");
		}
		else
		{
			$(this).text('POKAŻ');
			$(this).removeClass("hide").addClass("show");
			$(this).parent().parent().parent().find("tr").css("display","none");
		}
		
		$(this).parent().parent().parent().find("tr:first").css("display","table-row");
	});
	
	$(".orders_list a.year").click(function(){
	
		var type, year = parseInt($(this).text());
		$(this).parent().find("a").removeClass("active");
		$(this).addClass("active");
		
		if ($(this).hasClass("realized"))
		{
			type = "realized";
		}
		else if ($(this).hasClass("archives"))
		{
			type = "archives";
		}
		else if ($(this).hasClass("deleted"))
		{
			type = "deleted";
		}
		else if ($(this).hasClass("submitted"))
		{
			type = "submitted";
		}
		
		$.ajax({
			
			type: "POST",
			url: "include/orders/xml.php?operation=set_year",
			data: {type:type,year:year, operation:"set_year"},
			dataType: "xml",
		  
			success: function(xml) 
			{
				window.location.reload();
			}
		});
		
		return false;
	});
	
	// export
	
	
	$("a.filter_form_submit").click(function(){
		$("form[name='export']").attr("action", "./summary");
		$("form[name='export']").submit();
		return false;
	});
	
	
	$("a.export_form_columns").click(function(){
		$("form[name='export']").attr("action", "./summary/export");
		$("#export_columns").css("display","block");
		return false;
	});
	
	$("a.export_form_submit").click(function(){
		$("#export_columns").css("display","none");
		$("form[name='export']").submit();
		return false;
	});
	
	$("table.export th img").click(function(){
		
		var element = $(this).parent();

		$(element).find("img:first").attr("src", "images/arrow_asc_hidden.png");
		$(element).find("img:last").attr("src", "images/arrow_desc_hidden.png");
		
		if ($(this).hasClass("asc"))
		{
			$(this).attr("src", "images/arrow_asc.png");
		}
		else
		{
			$(this).attr("src", "images/arrow_desc.png");
		}
		
		$("input[type='hidden'][name='order["+$(this).parent().attr("alt")+"]']").val($(this).attr("class"));
	});
	
	// reject order
	
	$("table.orders_list a.reject_order").click(function(){
	
		var id = parseInt($(this).attr("alt"));
		var location = $(this).position();
		
		$.ajax({
			
			type: "POST",
			url: "include/orders/xml.php?operation=get_reject_comment",
			data: {id:id, operation:"get_reject_comment"},
			dataType: "xml",
		  
			success: function(xml) 
			{
				if ($(xml).find("id").length)
				{
					$("div#reject_comment input[type='hidden'][name='order_id']").val($(xml).find("id").text());
					$("div#reject_comment").css({"display":"block", "left":parseInt(location.left - 0)+"px", "top":parseInt(location.top + 15)+"px"});
				}
				
				if ($(xml).find("comment").length)
				{
					$("div#reject_comment textarea").val($(xml).find("comment").text());
				}
			}
		});
		
		return false;
	});
	
	$("div#reject_comment a.submit").click(function(){

		var id = parseInt($("div#reject_comment input[name='order_id']").val());

		$.ajax({
			
			type: "POST",
			url: "include/orders/xml.php?operation=set_reject_comment",
			data: {id:id, comment:$("div#reject_comment textarea[name='reject_comment']").val(), operation:"set_reject_comment"},
			dataType: "xml",
		  
			success: function(xml) 
			{
				$("div#reject_comment").css("display", "none");
				
				if (parseInt($(xml).find("status").text()) == 1)
				{
					document.location="./orders/orders/delete/"+id;
				}
				else
				{
					alert('Powód odrzucenia jest wymagany');
				}
			}
		});

		return false;
	});
	
	$("div#reject_comment img.close").click(function(){
		$("div#reject_comment").css("display", "none");
	});
	
	$("#export_box input.check_all").change(function(){
		$("#export_box input[type='checkbox'][name='selected[]']").attr("checked", ((true == $(this).attr('checked')) ? true : false));
	});
	
	$("table.orders_list input[type='checkbox'][name='hermes[]']").change(function(){
	
		var id = parseInt($(this).attr("id").split("_")[1]);
	
		$.ajax({
			
			type: "POST",
			url: "include/orders/xml.php?operation=set_hermes",
			data: {id:id, status:(($(this).is(":checked")) ? 1 : 0), operation:"set_hermes"},
			dataType: "xml",
		  
			success: function(xml) 
			{	
			}
		});
	});
});
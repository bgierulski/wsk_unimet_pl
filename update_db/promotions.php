<?php
// pobieram informacje dotyczace producentow
$result = $db->query('SELECT id, name FROM producers') or $db->raise_error();

$producers = array();
$producer_ids = array();
	
while($row = mysql_fetch_array($result)) 
{
	$producers[] = $row['name'];
	$producer_ids[$row['name']] = (int)$row['id'];
}

$result = $db->query('SELECT id, comment FROM groups_tree') or $db->raise_error();
$logos = array();

while($row = mysql_fetch_assoc($result)) 
{
	$logos[$row['comment']] = $row['id'];
}


$result = ibase_query('SELECT prm_nag.ID, prm_nag.DATA_OD, prm_nag.DATA_DO, prm_nag.DOTYCZY, prm_nag.OGRAN, prm_rab.RODZAJLOGO, prm_rab.WARTOSC, prm_rab.TYP, 
prm_rab.WAR_OGOLNE, prm_rab.CENA, prm_rab.LOGO FROM prm_nag join prm_rab ON prm_nag.ID = prm_rab.PRM WHERE (OGRAN IS NULL OR OGRAN = 0 OR (OGRAN = 3 AND 
prm_nag.TYPY LIKE \'%ZM%\')) AND (prm_nag.MAGAZYNY IS NULL OR prm_nag.MAGAZYNY LIKE \'%CDM%\') AND prm_rab.RODZAJLOGO IN (0,1,2,3) AND prm_rab.TYP IN (1,2,3)');

$actual_date = (int)date('Ymd');
$db->query('TRUNCATE TABLE promotions') or $db->raise_error();
$db->query('TRUNCATE TABLE promotion_details') or $db->raise_error();

$promotions = array();

while($row = ibase_fetch_assoc($result)) 
{
	$ibase_producer = iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO'])));
	
	if ((int)$row['DATA_OD'] <= $actual_date && (int)$row['DATA_DO'] >= $actual_date && !((int)$row['RODZAJLOGO'] == 3 && substr($ibase_producer, -1) != '*'))
	{
		$query = 'INSERT INTO promotions VALUES ('.(int)$row['ID'].', \''.$row['DATA_OD'].'\', \''.$row['DATA_DO'].'\', '.(int)$row['DOTYCZY'].', '.(int)$row['OGRAN'].', 
		'.(int)$row['RODZAJLOGO'].', ';
			
		$promotion_controll = false;
		
		switch ((int)$row['RODZAJLOGO'])
		{
			case 2:
				
				$logo_id = $logos[iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO'])))];
				
				$effect = $db->query('SELECT products.id_hermes FROM products JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE products.subgroup_id = '.(int)$logo_id) or $db->raise_error();
				
				if (mysql_num_rows($effect))
				{
					$detail = $logos[iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO'])))];
					$promotion_controll = true;
				}
				
				break;
					
			case 3:
				
				$producer_result = ibase_query('SELECT OPIS FROM mat_prd WHERE mat_prd.LOGO = \''.$ibase_producer.'\'');
				$producer_row = ibase_fetch_assoc($producer_result);
				$ibase_producer = iconv("windows-1250", "UTF-8", trim(mysql_escape_string($producer_row['OPIS'])));
				
				$effect = $db->query('SELECT products.id_hermes FROM producers JOIN products ON producers.id = products.producer_id JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE producers.name = \''.$ibase_producer.'\'') or $db->raise_error();
					
				if (mysql_num_rows($effect))
				{
					$detail = $producer_ids[$ibase_producer];
					$promotion_controll = true;
				}
				
				break;
					
			default:
				$effect = $db->query('SELECT id_hermes FROM products WHERE symbol = \''.trim(mysql_escape_string($row['LOGO'])).'\' AND subgroup_id > 0') or $db->raise_error();
				
				if (mysql_num_rows($effect))
				{
					$product = mysql_fetch_array($effect);
					$detail = $product['id_hermes'];
					$promotion_controll = true; 
				}
				
				break;
		}
			
		$query .= '\''.(int)$detail.'\', \''.mysql_escape_string(str_replace(',', '.', $row['WARTOSC'])).'\', \''.(int)$row['TYP'].'\', \''.(int)$row['WAR_OGOLNE'].'\', \''.(int)$row['CENA'].'\')';
		
		if (true == $promotion_controll)
		{
			$db->query($query) or $db->raise_error();
		}
	}
}

$result = ibase_query('SELECT PRM, KON FROM prm_kon');

while($row = ibase_fetch_assoc($result)) 
{
	$db->query('INSERT INTO promotion_details VALUES (\'\', '.(int)$row['PRM'].', '.(int)$row['KON'].')') or $db->raise_error();
}

//obsluga indywidualnych rabatow klienta

$db->query('TRUNCATE TABLE mat_rab') or $db->raise_error();

$result = ibase_query('SELECT * FROM mat_rab WHERE mat_rab.RODZAJLOGO IN (0,1,2,3) AND mat_rab.TYP IN (0,2,4,5,8) AND KON < 1000000');

while($row = ibase_fetch_assoc($result)) 
{
		$sqlrab="";
		$i=0;
		$detail="";
		
		switch ((int)$row['RODZAJLOGO'])
		{
			case 2:
				
				$logo_id = $logos[iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO'])))];
				$effect = $db->query('SELECT products.id_hermes FROM products JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE products.subgroup_id = '.(int)$logo_id) or $db->raise_error();
				
				if (mysql_num_rows($effect))
				{
					$detail = $logos[iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO'])))]; 
				}
				
				break;
					
			case 3:
				
				$ibase_producer = iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO']))); 
				$producer_result = ibase_query('SELECT OPIS FROM mat_prd WHERE mat_prd.LOGO = \''.$ibase_producer.'\'');
				$producer_row = ibase_fetch_assoc($producer_result);
				$ibase_producer = iconv("windows-1250", "UTF-8", trim(mysql_escape_string($producer_row['OPIS'])));
				
				$effect = $db->query('SELECT products.id_hermes FROM producers JOIN products ON producers.id = products.producer_id JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE producers.name = \''.$ibase_producer.'\'') or $db->raise_error();
					
				if (mysql_num_rows($effect))
				{
					$detail = $producer_ids[$ibase_producer]; 
				}
				
				break;
					
			default:
				
				$effect = $db->query('SELECT id_hermes FROM products WHERE symbol = \''.trim(mysql_escape_string($row['LOGO'])).'\' AND subgroup_id > 0') or $db->raise_error();
				
				if (mysql_num_rows($effect))
				{
					$product = mysql_fetch_array($effect);
					$detail = $product['id_hermes']; 
				}
				
				break;
				
		} 
		
		if($detail<>''){
			
			$db->query("INSERT INTO mat_rab VALUES ( '".$row['KON']."',	'".$detail."', '".$row['WARTOSC']."', '".$row['TYP']."', '".$row['WAR_OGOLNE']."', '".$row['RODZAJLOGO']."', '".$row['CENA']."', '".$row['CECHA']."' )") or $db->raise_error();

		}	
}

$result = ibase_query('SELECT mat_odb.ID as KON, mat_rab.LOGO, mat_rab.WARTOSC, mat_rab.TYP, mat_rab.WAR_OGOLNE, mat_rab.RODZAJLOGO, mat_rab.CENA, mat_rab.CECHA FROM mat_rab JOIN mat_sie ON mat_rab.KON = mat_sie.ID JOIN mat_odb ON mat_sie.SIEC = mat_odb.SIEC WHERE mat_rab.RODZAJLOGO IN (0,1,2,3) AND mat_rab.TYP IN (0,2,4,5,8) AND mat_rab.KON >= 1000000');

while($row = ibase_fetch_assoc($result)) 
{
		$sqlrab="";
		$i=0;
		$detail="";
		
		switch ((int)$row['RODZAJLOGO'])
		{
			case 2:
				
				$logo_id = $logos[iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO'])))];
				$effect = $db->query('SELECT products.id_hermes FROM products JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE products.subgroup_id = '.(int)$logo_id) or $db->raise_error();
				
				if (mysql_num_rows($effect))
				{
					$detail = $logos[iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO'])))]; 
				}
				
				break;
					
			case 3:
				
				$ibase_producer = iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['LOGO']))); 
				$producer_result = ibase_query('SELECT OPIS FROM mat_prd WHERE mat_prd.LOGO = \''.$ibase_producer.'\'');
				$producer_row = ibase_fetch_assoc($producer_result);
				$ibase_producer = iconv("windows-1250", "UTF-8", trim(mysql_escape_string($producer_row['OPIS'])));
				
				$effect = $db->query('SELECT products.id_hermes FROM producers JOIN products ON producers.id = products.producer_id JOIN groups_tree ON products.subgroup_id = groups_tree.id WHERE producers.name = \''.$ibase_producer.'\'') or $db->raise_error();
					
				if (mysql_num_rows($effect))
				{
					$detail = $producer_ids[$ibase_producer]; 
				}
				
				break;
					
			default:
				
				$effect = $db->query('SELECT id_hermes FROM products WHERE symbol = \''.trim(mysql_escape_string($row['LOGO'])).'\' AND subgroup_id > 0') or $db->raise_error();
				
				if (mysql_num_rows($effect))
				{
					$product = mysql_fetch_array($effect);
					$detail = $product['id_hermes']; 
				}
				
				break;
				
		} 
		
		if($detail<>''){
			
			$db->query("INSERT INTO mat_rab VALUES ( '".$row['KON']."',	'".$detail."', '".$row['WARTOSC']."', '".$row['TYP']."', '".$row['WAR_OGOLNE']."', '".$row['RODZAJLOGO']."', '".$row['CENA']."', '".$row['CECHA']."' )") or $db->raise_error();

		}	
}
?>
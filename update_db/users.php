<?php
$result = ibase_query('SELECT ID, NAZWA, CD_NAZWA, SPOSOB, ADRES, TELEFON, FAX, IDENT, KONTO, ODBIERA, BLOKADA, E_MAIL, TRANSPORT, AKW, RABAT, GRUPA_CEN, BEZ_PROM, PLATNIK  from mat_odb 
WHERE E_MAIL NOT IN (\'\', \'BRAK\', \'brak\') AND IDENT != \'\' AND UKRYJ = \'N\'');

$fbd_users = '';
$counter = 0;

while($row = ibase_fetch_assoc($result)) 
{
	$users = $db->query('SELECT id_hermes FROM users WHERE id_hermes = '.(int)$row['ID']) or $db->raise_error();
		
	$address = iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['ADRES'])));
		
	$post_code = array();
	unset($code);
	unset($city);
	$street_controll = false;
		
	$post_code = explode('-', $address);
		
	if (preg_match('/^[0-9]{2}$/D', $post_code[0]) && preg_match('/^[0-9]{3}$/D', substr($post_code[1], 0, 3)))
	{
		$code = $post_code[0].'-'.substr($post_code[1], 0, 3);
	}

	$start = ((isset($code)) ? 7 : 0);
		
	for ($i=$start; $i<strlen($address); $i++)
	{
		if (in_array(substr($address, $i, 3), array(',UL')))
		{
			$street_controll = true;
		}
			
		unset($street);
		unset($number);
				
		if (true == $street_controll)
		{
			for ($j=($i+1); $j<strlen($address); $j++)
			{
				if ((int)$address[$j] && $j > ($i+6))
				{
					$number = trim(substr($address, $j, 30));

					$street_controll = false;
					$street = trim($street);
					break;
				}
						
				$street .= $address[$j];
			}	
		}
				
		if ((int)$address[$i] || in_array(substr($address, $i, 3), array(',UL')))
		{
			if (!isset($number))
			{
				$number = trim(substr($address, $i, 30));
			}
				
			if (!isset($street))
			{
				if (isset($code))
				{
					$street = $city;
				}
				else if (isset($city))
				{
					$street = $city;
					unset($city);
				}
			}
			
			break;
		}
				
		$city .= $address[$i];
	}
		
	unset($home);
	unset($flat);
		
	if (isset($number))
	{
		$number_control = explode('/', $number);
			
		if (count($number_control) > 1)
		{
			$home = $number_control[0];
			$flat = $number_control[1];
		}
		else
		{
			$home = $number;
		}
	}
	
	if ($counter == 20)
	{
		$fbd_users = substr($fbd_users, 0, -1);
		$fbd_users .= ') AND ';
		$counter = 0;
	}
	
	if (!$counter)
	{
		$fbd_users .= 'id_hermes NOT IN (';
	}
	
	$fbd_users .= (int)$row['ID'].',';
	
	$counter++;
	
	switch (iconv("windows-1250", "UTF-8", trim($row['SPOSOB'])))
	{
		case 'PRZELEW': $payment = 1; break;
		case 'GOTÓWKA': $payment = 2; break;
		case 'WG.DOKUMENTU KP': $payment = 3; break;
		case 'WG.KP': $payment = 4; break;
		case 'KOMPENSATA': $payment = 5; break;
		case 'PRZEDPŁATA': $payment = 6; break;
		case 'ZA POBRANIEM': $payment = 7; break;
		case 'KARTA': $payment = 8; break;
		default: $payment = 2; break;
	}
		
	if (!mysql_num_rows($users))
	{
		$identyfikator = trim(mysql_escape_string(str_replace(array(' ', '-'), array('', ''), $row['IDENT'])));
		$char = substr($identyfikator, -1);
		
		if ((int)$row['ID'] != (int)$row['PLATNIK'] && !(int)$char && $char != '-')
		{
			$subaccount_controll = $db->query('SELECT id FROM users WHERE id_hermes = '.(int)$row['PLATNIK']) or $db->raise_error();
				
			if (mysql_num_rows($subaccount_controll))
			{
				$parent = mysql_fetch_array($subaccount_controll);
				
				$generator = $db->query('SELECT SUBSTRING(MD5(RAND()),-20) as value') or $db->raise_error();
				$key = mysql_fetch_array($generator);
					
				$subaccount_query  = 'INSERT INTO users VALUES ';
				$subaccount_query .= '(\'\', '.(int)$row['ID'].', \''.$identyfikator.'\', ';
				$subaccount_query .= '\'\', \'\', \''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['NAZWA'])));
				$subaccount_query .= ((trim($row['CD_NAZWA']) != '') ? ', '.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['CD_NAZWA']))) : '').'\', ';
				$subaccount_query .= '\'\', \'\', \'\', \''.trim(mysql_escape_string(strtolower($row['E_MAIL']))).'\', \''.$code.'\', \''.$city.'\', \''.$street.'\', \''.$home.'\', ';
				$subaccount_query .= '\''.$flat.'\', 0, \''.trim(mysql_escape_string($row['TELEFON'])).'\', \''.trim(mysql_escape_string($row['FAX'])).'\', ';
				$subaccount_query .= '\''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['TRANSPORT']))).'\', \''.trim(mysql_escape_string($row['KONTO'])).'\', ';
				$subaccount_query .= '\''.trim(mysql_escape_string($row['IDENT'])).'\', 1, 1, 0, 0, '.(int)$row['PLATNIK'].', \''.(float)$row['RABAT'].'\', '.(int)$row['GRUPA_CEN'].', ';
				$subaccount_query .= (($row['BEZ_PROM'] == 'T') ? 0 : 1).', 1, 0, ';
				$subaccount_query .= (int)$row['AKW'].', \''.date('Y-m-d').'\', \'0000-00-00 00:00:00\', \'\', '.$payment.', 0, 0, 0)';
					
				$db->query($subaccount_query) or $db->raise_error();
				$user_id = mysql_insert_id();
				
				$db->query('INSERT INTO user_tree VALUES (\'\', '.$user_id.', '.$parent['id'].', 1)') or $db->raise_error();
			}
		}
		else if ((int)$row['ID'] == (int)$row['PLATNIK'])
		{
			$generator = $db->query('SELECT SUBSTRING(MD5(RAND()),-20) as value') or $db->raise_error();
			$key = mysql_fetch_array($generator);
				
			$users_query .= '(\'\', '.(int)$row['ID'].', \''.$identyfikator.'\', ';
			$users_query .= '\'\', \'\', \''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['NAZWA'])));
			$users_query .= ((trim($row['CD_NAZWA']) != '') ? ', '.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['CD_NAZWA']))) : '').'\', ';
			$users_query .= '\'\', \'\', \'\', \''.trim(mysql_escape_string(strtolower($row['E_MAIL']))).'\', \''.$code.'\', \''.$city.'\', \''.$street.'\', \''.$home.'\', ';
			$users_query .= '\''.$flat.'\', 0, \''.trim(mysql_escape_string($row['TELEFON'])).'\', \''.trim(mysql_escape_string($row['FAX'])).'\', ';
			$users_query .= '\''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['TRANSPORT']))).'\', \''.trim(mysql_escape_string($row['KONTO'])).'\', ';
			$users_query .= '\''.trim(mysql_escape_string($row['IDENT'])).'\', 1, 1, 0, 0, '.(int)$row['ID'].', \''.(float)$row['RABAT'].'\', '.(int)$row['GRUPA_CEN'].', ';
			$users_query .= (($row['BEZ_PROM'] == 'T') ? 0 : 1).', 1, 0, ';
			$users_query .= (int)$row['AKW'].', \''.date('Y-m-d').'\', \'0000-00-00 00:00:00\', \'\', '.$payment.', 0, 0, 0),';
				
			$users_counter++;
			
			if ($users_counter > 10)
			{
				$db->query(substr('INSERT INTO users VALUES '.$users_query, 0, -1)) or $db->raise_error();
				$users_query = '';
				$users_counter = 0;
			}
		}
	}
	else 
	{
		$db->query('UPDATE users SET name = \''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['NAZWA']))).((trim($row['CD_NAZWA']) != '') ? ', '.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['CD_NAZWA']))) : '').'\', 
		email = \''.trim(mysql_escape_string(strtolower($row['E_MAIL']))).'\', post_code = \''.$code.'\', city = \''.$city.'\', street = \''.$street.'\', 
		home = \''.$home.'\', flat = \''.$flat.'\', identificator = \''.trim(mysql_escape_string($row['IDENT'])).'\' WHERE id_hermes = '.(int)$row['ID']) or $db->raise_error();
		
		$db->query('UPDATE users SET merchant_id = '.(int)$row['AKW'].', discount = \''.(float)$row['RABAT'].'\', 
		price_group = '.(int)$row['GRUPA_CEN'].', promotions = '.(($row['BEZ_PROM'] == 'T') ? 0 : 1).', 
		payment_id = '.$payment.' WHERE parent_id = '.(int)$row['ID']) or $db->raise_error();
	}
}

if (!empty($fbd_users))
{
	if (substr($fbd_users, - 4) != ') AND ')
	{
		$fbd_users = substr($fbd_users, 0, -1);
		$fbd_users .= ')';
	}
	
	$query = "UPDATE users SET `active` = 0 WHERE ".$fbd_users." AND status = 1 AND login != 'demo'";
	$db->query($query) or $db->raise_error();
}

if ($users_counter)
{
	$db->query(substr('INSERT INTO users VALUES '.$users_query, 0, -1)) or $db->raise_error();
}
?>
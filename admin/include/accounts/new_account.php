<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<label style="width:230px;">&nbsp;</label>
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
	<div class="clear"></div>
</div>
<?php } ?>

<?php
$user_id = (int)$_GET['id']; 

if (!isset($_POST['add_account']) && $action != 'edit')
{
	$accounts->clear();
}
if (!isset($_POST['save_changes']) && $action == 'edit')
{
	$accounts->getAccount($user_id);
}
?>

<form method="POST" action="">

	<input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
	
	<?php  if ($_SESSION["errors"]['login']) { ?>
	<div class="form_element error">
		<label style="width:230px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['login']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:230px;">Login</label>
		<input type="text" name="login" value="<?php echo $accounts->textControll($_SESSION["values"]['login']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['email']) { ?>
	<div class="form_element error">
		<label style="width:230px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['email']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:230px;">Adres e-mail</label>
		<input type="text" name="email" value="<?php echo $accounts->textControll($_SESSION["values"]['email']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['password']) { ?>
	<div class="form_element error">
		<label style="width:230px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['password']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:230px;">Hasło <strong>*</strong></label>
		<input type="password" name="password" value="" />
		<div class="clear"></div>
	</div>
	
	<div class="form_element">
		<label style="width:230px;">Powtórzone hasło <strong>*</strong></label>
		<input type="password" name="repeated_password" value="" />
		<div class="clear"></div>
	</div>
	
	<div class="form_element">
		<label style="width:230px;">Obserwator</label>
		<input type="checkbox" name="observer" value="1" style="width:auto" <?php if ($_SESSION["values"]['observer'] == 1) { echo 'checked="checked"'; } ?> />
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['registration_messages']) { ?>
	<div class="form_element error">
		<label style="width:230px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['registration_messages']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:230px;">Wiadomości e-mail dotyczące rejestracji</label>
		<input type="checkbox" name="registration_messages" value="1" style="width:auto" <?php if ($_SESSION["values"]['registration_messages'] == 1) { echo 'checked="checked"'; } ?> />
		<div class="clear"></div>
	</div>
	
	<?php 
	$merchants_list = $accounts->getMerchants();
	
	if ($_SESSION["values"]['merchants_count'] == 0)
	{
		?>
		<div class="form_element merchant list" id="select_merchant_1">
			<label style="width:230px;">Wiadomości dotyczące zamówień</label>
			<p>
				<select name="merchants[]">
				<option value="0">-- wybierz doradcę --</option>
				<?php 
				if (is_array($merchants_list))
				{
					foreach ($merchants_list as $merchant)
					{
						?><option value="<?php echo $merchant['id']; ?>"><?php echo $merchant['name']; ?></option><?php
					}
				}
				?>
				</select>
				<strong class="add_merchant">dodaj kolejnego doradcę</strong> <span class="merchant_action">usuń doradcę</span>
			</p>
			<div class="clear"></div>
		</div>
		<?php
	}
	else 
	{
		for ($i=0; $i<$_SESSION["values"]['merchants_count']; $i++)
		{
			?>
			<div class="form_element merchant list" id="select_merchant_<?php echo ($i+1); ?>">
				<label style="width:230px;">Wiadomości dotyczące zamówień</label>
				<p>
					<select name="merchants[]">
					<option value="0">-- wybierz doradcę --</option>
					<?php 
					if (is_array($merchants_list))
					{
						foreach ($merchants_list as $merchant)
						{
							?><option value="<?php echo $merchant['id']; ?>" <?php if ($_SESSION["values"]["merchants"][$i] == $merchant['id']) echo 'selected="selected"'; ?>><?php echo $merchant['name']; ?></option><?php
						}
					}
					?>
					</select>
						
					<?php if (($i+1) == $_SESSION["values"]['merchants_count']) { ?>
					<strong class="add_merchant">dodaj kolejnego doradcę</strong> 
					<?php } ?>
						
					<span class="merchant_action" style="<?php if (($i+1) < $_SESSION["values"]['merchants_count']) { ?>visibility:visible;<?php } ?>">usuń doradcę</span>
				</p>
				<div class="clear"></div>
			</div>
			<?php
		}
	}
		
	if (!isset($_SESSION["values"]['merchants_count']) || $_SESSION["values"]['merchants_count'] == 0)
	{
		$_SESSION["values"]['merchants_count'] = 1;
	}
	?>
	
	<input type="hidden" name="merchants_count" value="<?php echo $_SESSION["values"]['merchants_count']; ?>" />
	
	<div class="form_element">
		<label style="width:230px;">Wyświetlaj zamówienia tylko dla wybranych akwizytorów</label>
		<input type="checkbox" name="narrow_orders" value="1" style="width:auto" <?php if ($_SESSION["values"]['narrow_orders'] == 1) { echo 'checked="checked"'; } ?> />
		<div class="clear"></div>
	</div>
	
	<?php 
	if (!is_array($_SESSION["values"]["modules"]) || empty($_SESSION["values"]["modules"]))
	{
		$_SESSION["values"]["modules"] = array();
	}
	?>
	
	<div class="form_element">
		<label style="width:230px;">Uprawnienia</label>
		<div style="float:left">
			<p><input type="checkbox" name="modules[]" value="header_products" <?php if (in_array("header_products", $_SESSION["values"]["modules"])) { echo 'checked="checked"'; } ?> style="width:auto; position:relative; top:2px;" />&nbsp; produkty w nagłówku strony</p>
			<p><input type="checkbox" name="modules[]" value="orders" <?php if (in_array("orders", $_SESSION["values"]["modules"])) { echo 'checked="checked"'; } ?> style="width:auto; position:relative; top:2px;" />&nbsp; zamówienia</p>
			<p><input type="checkbox" name="modules[]" value="queries" <?php if (in_array("queries", $_SESSION["values"]["modules"])) { echo 'checked="checked"'; } ?> style="width:auto; position:relative; top:2px;" />&nbsp; zapytania</p>
			<p><input type="checkbox" name="modules[]" value="summary" <?php if (in_array("summary", $_SESSION["values"]["modules"])) { echo 'checked="checked"'; } ?> style="width:auto; position:relative; top:2px;" />&nbsp; zestawienia</p>
			<p><input type="checkbox" name="modules[]" value="accounts" <?php if (in_array("accounts", $_SESSION["values"]["modules"])) { echo 'checked="checked"'; } ?> style="width:auto; position:relative; top:2px;" />&nbsp; konta użytkowników</p>
			<p><input type="checkbox" name="modules[]" value="users" <?php if (in_array("users", $_SESSION["values"]["modules"])) { echo 'checked="checked"'; } ?> style="width:auto; position:relative; top:2px;" />&nbsp; klienci</p>
			<p><input type="checkbox" name="modules[]" value="producers" <?php if (in_array("producers", $_SESSION["values"]["modules"])) { echo 'checked="checked"'; } ?> style="width:auto; position:relative; top:2px;" />&nbsp; producenci</p>
			<p><input type="checkbox" name="modules[]" value="pages" <?php if (in_array("pages", $_SESSION["values"]["modules"])) { echo 'checked="checked"'; } ?> style="width:auto; position:relative; top:2px;" />&nbsp; podstrony</p>
			<p><input type="checkbox" name="modules[]" value="merchants" <?php if (in_array("merchants", $_SESSION["values"]["modules"])) { echo 'checked="checked"'; } ?> style="width:auto; position:relative; top:2px;" />&nbsp; handlowcy</p>
			<p><input type="checkbox" name="modules[]" value="concessions" <?php if (in_array("concessions", $_SESSION["values"]["modules"])) { echo 'checked="checked"'; } ?> style="width:auto; position:relative; top:2px;" />&nbsp; branżyści</p>
			<p><input type="checkbox" name="modules[]" value="main_page" <?php if (in_array("main_page", $_SESSION["values"]["modules"])) { echo 'checked="checked"'; } ?> style="width:auto; position:relative; top:2px;" />&nbsp; grupy główne</p>
			<p><input type="checkbox" name="modules[]" value="informations" <?php if (in_array("informations", $_SESSION["values"]["modules"])) { echo 'checked="checked"'; } ?> style="width:auto; position:relative; top:2px;" />&nbsp; komunikaty</p>
			<p><input type="checkbox" name="modules[]" value="restrictions" <?php if (in_array("restrictions", $_SESSION["values"]["modules"])) { echo 'checked="checked"'; } ?> style="width:auto; position:relative; top:2px;" />&nbsp; ograniczenia</p>
			<p><input type="checkbox" name="modules[]" value="b2b" <?php if (in_array("b2b", $_SESSION["values"]["modules"])) { echo 'checked="checked"'; } ?> style="width:auto; position:relative; top:2px;" />&nbsp; B2B - szczegóły</p>
			<p><input type="checkbox" name="modules[]" value="groups" <?php if (in_array("groups", $_SESSION["values"]["modules"])) { echo 'checked="checked"'; } ?> style="width:auto; position:relative; top:2px;" />&nbsp; indywidualne menu użytkowników</p>
		</div>
		<div class="clear"></div>
	</div>
	
	<div class="form_element">
		<label style="width:230px;">&nbsp;</label>
		<?php if ($action == 'edit') { ?>
		<input id="submit" name="save_changes" value="Zapisz zmiany" type="submit" /> 
		<?php } else { ?>
		<input id="submit" name="add_account" value="Dodaj użytkownika" type="submit" /> 
		<?php } ?>
		<div class="clear"></div>
	</div>
</form> 

<p>
<strong>*</strong> - nie jest wymagane, jeśli hasło ma pozostać bez zmian
</p>
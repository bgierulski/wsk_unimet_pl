<?php 
if (in_array($page_type, array('kontakt', 'profit', 'gazetki', 'cenniki')))
{
	include(BASE_DIR.'page.php');
}
else
{
	switch($page_type)
	{
		case 'mapa': include(BASE_DIR.'page_map.php');  break;
		case 'dokumenty': include(BASE_DIR.'documents.php');  break;
		case 'zapytania': include(BASE_DIR.'special_orders.php');  break;
		case 'szablony': include(BASE_DIR.'templates.php'); break;
		case 'zamowienia': include(BASE_DIR.'archive.php');  break;
		case 'zlozonezamowienie': include(BASE_DIR.'complex_order.php');  break;
		case 'koszyk': include(BASE_DIR.'basket.php');  break;
		case 'sciezki': include(BASE_DIR.'paths.php');  break;
		case 'zamowienie': include(BASE_DIR.'order.php');  break;
		case 'konto': include(BASE_DIR.'account.php');  break;
		case 'zestawienia': include(BASE_DIR.'export.php'); break;
		case 'produkt': include(BASE_DIR.'product.php');  break;
		case 'promotions': include(BASE_DIR.'promotions.php');  break;
		case 'nowosci': $_SESSION["b2b"]['producer_id'] = 0; include(BASE_DIR.'newses.php');  break;
		case 'promocje': include(BASE_DIR.'promotions.php');  break;
		case 'grupa': include(BASE_DIR.'subgroup.php');  break;
		case 'wyszukiwarka': include(BASE_DIR.'search.php');  break;
		case 'informacje': include(BASE_DIR.'informations.php');  break;
		default: include(BASE_DIR.'main_page.php'); break;
	}
}
?>
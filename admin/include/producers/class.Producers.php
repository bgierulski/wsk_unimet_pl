<?php
class Producers extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_producer_id;
	private $_name;
	private $_short;
	private $_description;
	private $_logo;
	private $_link;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
	}
	
	public function deleteProducer($producer_id)
	{
		$this->deleteImage($producer_id, 1);
		
		$this->_db->query('DELETE FROM producers WHERE id = '.$producer_id) or $this->_db->raise_error();	
	}
	
	public function getProducers()
	{
		$result = $this->_db->query('SELECT id, name, short, www FROM producers ORDER BY name ASC') or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getProducer($producer_id)
	{
		$result = $this->_db->query('SELECT name, short, description, www FROM producers WHERE id = '.$producer_id) or $this->_db->raise_error();
		
		$row = mysql_fetch_array($result);
		
		$values['name'] = $row['name'];
		$values['short'] = $row['short'];
		$values['description'] = str_replace('\"', '"', $row['description']);
		$values['link'] = $row['www'];
		
		$values['logo'] = $this->getImage($producer_id);
		
		$this->setSession("values", $values);
	}
	
	public function deleteImage($producer_id, $option = 0)
	{
		$image = $this->getImage($producer_id);
		$path = BASE_DIR.'../../images/producers/'.$image;
		
		if (file_exists($path))
		{
			@unlink($path);
		}
		
		$this->_db->query('UPDATE producers SET logotype = \'\' WHERE id  = '.$producer_id);
		
		if (!$option)
		{
			header("Location: ./");
		}
	}
	
	public function getImage($producer_id)
	{
		$result = $this->_db->query('SELECT logotype FROM producers WHERE id = '.$producer_id) or $this->_db->raise_error();
		
		$row = mysql_fetch_array($result);
		return $row['logotype'];
	}
	
	public function validateProducer()
	{
		$this->clear();
		
		$this->_producer_id = (int)$_POST['producer_id'];
		$this->_name = $values['name'] = trim(mysql_escape_string($_POST['name']));
		$this->_short = $values['short'] = trim(mysql_escape_string($_POST['short']));
		$this->_description = $values['description'] = trim(mysql_escape_string($_POST['description']));
		$this->_link = $values['link'] = trim(mysql_escape_string($_POST['link']));
		$this->_logo = $_FILES["logo"];
		
		if (empty($this->_name))
		{
			$this->_errors++;
			$errors['name'] = 'Nazwa producenta jest wymagana';
		}
		
		if (empty($this->_short))
		{
			$this->_errors++;
			$errors['short'] = 'Skrócony opis producenta jest wymagany';
		}
		
		if ($this->_logo['logo'] != '' && !$this->checkImage($this->_logo))
		{
			$this->_errors++;
			$errors['logo'] = 'Oczeklwane rozszerzenia pliku to: gif, jpg, png';
		}
		
		if (!$this->_errors)
		{
			if (isset($_POST['add_producer']))
			{
				$this->_addProducer();
			}
			else 
			{
				$this->_saveChanges();
			}
		}
		else 
		{
			if (isset($_POST['save_changes']))
			{
				$values['logo'] = $this->getImage($this->_producer_id);
			}
			
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
	}
	
	private function _addProducer()
	{
		$result = $this->_db->query('INSERT INTO producers VALUES (\'\', \''.$this->_name.'\', \''.$this->_short.'\', \''.$this->_description.'\', \'\', 
		\''.(($this->_link == 'http://') ? '' : $this->_link).'\')') or $this->_db->raise_error();
		
		$producer_id = $this->_db->insert_id();
		
		if (!empty($this->_logo['name']))
		{
			$this->_saveImage($producer_id);
		}
		
		$communicats['ok'] = 'Producent został dodany';
		$this->setSession("communicats", $communicats);
		header("Location: ../producers/list");
	}

	private function _saveChanges()
	{
		$this->_db->query('UPDATE producers SET name = \''.$this->_name.'\', short = \''.$this->_short.'\', description = \''.$this->_description.'\', 
		www = \''.(($this->_link == 'http://') ? '' : $this->_link).'\' WHERE id = '.$this->_producer_id) or $this->_db->raise_error();
		
		if ($this->_logo['name'] != '')
		{
			$this->deleteImage($this->_producer_id);
			$this->_saveImage($this->_producer_id);
		}

		$communicats['ok'] = 'Producent został wyedytowany';
		$this->setSession("communicats", $communicats);
		header("Location: ../../producers/list");
	}
	
	private function _saveImage($producer_id)
	{
		$image = new ImageUpload();
		$image->getExtension($this->_logo['name']);
		
		$image->uploadImage(200, 170, $producer_id, BASE_DIR.'../../images/producers/', $this->_logo, 0);
		$image->deleteImage($producer_id, 1);
			
		$this->_db->query('UPDATE producers SET logotype = \''.$producer_id.'.'.$image->extension.'\' WHERE id = '.$producer_id) or $this->_db->raise_error();
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}
?>
<?php
class Prices extends Validators
{
	private $_db;
	private $_user;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		$this->_user = $this->getSession("b2b_user");
	}
	
	public function getPrice($id_hermes, $parameter)
	{
		// pobranie informacji o grupie cenowej, rabacie i dostępie do promocji dla klienta
		$query = 'SELECT discount, price_group, promotions, parent_id FROM users WHERE parent_id = (SELECT parent_id FROM users WHERE id = '.$this->_user['id'].')';
		
		$result = $this->_db->query($query) or die ('Wystąpł błąd, za utrudnienia przepraszamy');
		
		if (mysql_num_rows($result))
		{
			$user = mysql_fetch_array($result);
			mysql_free_result($result);
			
			if (!(int)$user['price_group'])
			{
				$user['price_group'] = 1;	
				$user['discount'] = 0;
			}
			
			$query = 'SELECT products.id,products.id_hermes, product_versions.name, symbol, netto_1, netto_2, netto_3, netto_4, netto_5, netto_6, netto_minium, 
			tax, producer_id, subgroup_id FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id AND version_id = 1 JOIN 
			product_details ON products.id_hermes = product_details.product_id WHERE id_hermes = '.$id_hermes.' ORDER BY netto_6 ASC LIMIT 1';
			
			// pobranie informacji o produkcie
			$result = $this->_db->query($query) or die ('Nie znaleziono produktu');
			
			if (mysql_num_rows($result))
			{
				$product = mysql_fetch_array($result);
				mysql_free_result($result);
				
				if ((float)$product['netto_2'] == 0)
				{
					$product['netto_2'] = $product['netto_1'];
				}
				if ((float)$product['netto_3'] == 0)
				{
					$product['netto_3'] = $product['netto_1'];
				}
				if ((float)$product['netto_4'] == 0)
				{
					$product['netto_4'] = $product['netto_1'];
				}
				if ((float)$product['netto_5'] == 0)
				{
					$product['netto_5'] = $product['netto_1'];
				}
				if ((float)$product['netto_6'] == 0)
				{
					$product['netto_6'] = $product['netto_1'];
				}
				
				$price_options = array(
					array(), // wyniki wyliczeń opartych jedynie na informacjach o grupie cenowej oraz rabacie przypisanym do klienta klienta
					array(), // wyniki wyliczeń opartych o dane z MAT_RAB
					array()  // wyniki wyliczeń opartych o dane z PRM_RAB i PRM_KON
				);
				
				$basic_price = $product['netto_'.$user['price_group']];
				
				// wyliczenia oparte jedynie na informacji o grupie cenowej oraz rabacie przypisanym do klienta klienta
				$netto_price = ($basic_price * (100 - $user['discount']) / 100);
 					
				$price_options[0][0] = $price_options[1][0] = $price_options[2][0] = $netto_price;
				$price_options[0][1] = $price_options[1][1] = $price_options[2][1] = $netto_price * (1+($product['tax'] / 100));
				
				// wyliczenia oparte o MAT_RAB
				$query = 'SELECT * FROM mat_rab WHERE KON = '.$user['parent_id'].' AND RODZAJLOGO IN (0,1,2,3) AND TYP IN (0,2,4,5,8) AND ((RODZAJLOGO 
				= 3 AND LOGO = '.$product['producer_id'].') OR (RODZAJLOGO = 2 AND LOGO = '.$product['subgroup_id'].') OR (LOGO = '.$id_hermes.')) ORDER BY 
				RODZAJLOGO ASC';
				
				$result = $this->_db->query($query) or die ('Nie znaleziono danych');
				
				if (mysql_num_rows($result))
				{
					$row = mysql_fetch_array($result);
					mysql_free_result($result);
					
					switch ($row['TYP'])
					{
						case 8: // zł rabatu od ceny brutto
							
							$price = (((int)$product['netto_'.$row['CENA']] > 0) ? $product['netto_'.$row['CENA']] : $product['netto_'.$user['price_group']]);
	
							$brutto_price = $price * (1+($product['tax'] / 100)) - $row['WARTOSC'];
							$netto_price = (100 * $brutto_price / (1+($product['tax'] / 100)));
							break;
							
						case 5: // cena zawsze z tej grupy
							$netto_price = ((!empty($product['netto_'.$row['WARTOSC']])) ? $product['netto_'.$row['WARTOSC']] : $product['netto_'.$user['price_group']]);
							$brutto_price = $netto_price * (1+($product['tax'] / 100));
							break;
							
						case 4: // zł cena sprzedaży netto
							$netto_price = $row['WARTOSC'];
							$brutto_price = $netto_price * (1+($product['tax'] / 100));
							break;
							
						case 2: // zł rabatu od ceny netto
							$price = (((int)$product['netto_'.$row['CENA']] > 0) ? $product['netto_'.$row['CENA']] : $product['netto_'.$user['price_group']]);
							
							$netto_price = $price - $row['WARTOSC'];
							$brutto_price = $netto_price * (1+($product['tax'] / 100));
							break;
							
						default: // % rabatu od ceny netto
							$price = (((int)$product['netto_'.$row['CENA']] > 0) ? $product['netto_'.$row['CENA']] : $product['netto_'.$user['price_group']]);

							$netto_price = $price - ($price * $row['WARTOSC'] / 100);
							$brutto_price = $netto_price * (1+($product['tax'] / 100));
							break;
					}
					
					$price_options[1][0] = $netto_price;
					$price_options[1][1] = $brutto_price;
				}
				
				// wyliczenia oparte o PRM_RAB
				
				$query = 'SELECT * FROM promotions WHERE start_date <= \''.date('Y-m-d').'\' AND end_date >= \''.date('Y-m-d').'\' AND `range` IN (0,1,2,3) AND 
				type IN (1,2,3) AND ((`range` = 3 AND range_detail = '.$product['producer_id'].') OR (`range` = 2 AND range_detail = '.$product['subgroup_id'].') 
				OR (range_detail = '.$id_hermes.')) ORDER BY `range` ASC';
				
				$result = $this->_db->query($query) or die ('Nie znaleziono danych');
				
				if (mysql_num_rows($result))
				{
					$row = mysql_fetch_array($result);
					mysql_free_result($result);
					
					$allow = true; 
					
					if (in_array($row['restriction'], array(1,2))) // sprawdzenie ograniczeń
					{
						$query = 'SELECT id FROM promotion_details WHERE promotion_id = '.$row['id'].' AND user_id = '.$user['parent_id'];
						
						$result = $this->_db->query($query) or die ('Nie znaleziono danych');
						
						if ((mysql_num_rows($result) && $row['restriction'] == 2) OR (!mysql_num_rows($result) && $row['restriction'] == 1))
						{
							$allow = false;
						}
					}
					
					if (true == $allow)
					{
						switch ($row['type'])
						{
							case 3: // zł cena netto
								$netto_price = $row['value'];
								$brutto_price = $netto_price * (1+($product['tax'] / 100));
								break;
								
							case 2: // zł od ceny netto 
								$price = (((int)$product['netto_'.$row['price']] > 0) ? $product['netto_'.$row['price']] : $product['netto_'.$user['price_group']]);
								
								$netto_price = $price - $row['value'];
								$brutto_price = $netto_price * (1+($product['tax'] / 100));
								break;
								
							default: // % od ceny netto
								$price = (((int)$product['netto_'.$row['price']] > 0) ? $product['netto_'.$row['price']] : $product['netto_'.$user['price_group']]);
								
								$netto_price = $price - ($price * $row['value'] / 100);
								$brutto_price = $netto_price * (1+($product['tax'] / 100));
								break;
						}
						
						$price_options[2][0] = $netto_price;
						$price_options[2][1] = $brutto_price;
					}
				}
				
				$final_key = 0;
				
				if ($user['promotions'] == 1 && (float)$price_options[1][0] == 0)
				{
					if ((float)$price_options[1][0] < (float)$price_options[$final_key][0])
					{
						$final_key = 1;
					}
					if ((float)$price_options[2][0] < (float)$price_options[$final_key][0])
					{
						$final_key = 2;
					}
				}
				else
				{
					$final_key = 1;
				}
				
				$final_netto_price = $price_options[$final_key][0];
				$final_brutto_price = $price_options[$final_key][1];
				
				if ($price_options[$final_key][0] > 0)
				{
					$final_netto_price = $price_options[$final_key][0];
					$final_brutto_price = $price_options[$final_key][1];
				}
				else
				{
					$final_netto_price = $product['netto_minium'];
					$final_brutto_price = $product['netto_minium'] * (1+($product['tax'] / 100));
				}
				
				switch ($parameter)
				{
					case 1: // cena ostateczna brutto
						return sprintf("%0.2f", $final_brutto_price);
						break;
						
					case 2: // cena katalogowa brutto
						return sprintf("%0.2f", ($product['netto_1'] * (1+($product['tax'] / 100))));
						break;
						
					case 3: // oszczędności
						
						if ($basic_price > 100)
						{
							return sprintf("%0.2f", ($final_brutto_price - ($product['netto_1'] * (1+($product['tax'] / 100))))).' zł brutto';
						}
						else
						{
							return sprintf("%01.0f", ceil(((($final_netto_price * 100) / $basic_price) - 100) * (-1))).' %';
						}
						
						break;
						
					case 4: // cena ostateczna netto
						return sprintf("%0.2f", $final_netto_price);
						break;
						
					case 5: //nokaut
						return sprintf("%0.2f", $final_brutto_price);
						break;
						
					case 6: // cena katalogowa netto
						return sprintf("%0.2f", $product['netto_1']);
						break;
				}
			}
		}
	}
	
	public function getTransportPrice($wagacalosc, $wartosccalosc, $paletowy, $pobranie)
	{
		if ($paletowy <= 0)
		{
			if ($wagacalosc < 10) 
			{
				if ($wartosccalosc < 500) 
				{
					$cenatransportu = 20.00;		
				}
				else 
				{
					$cenatransportu = 0.00;
				}
			}
			else if ($wagacalosc > 10 && $wagacalosc < 30)
			{
				if ($wartosccalosc < 500) 
				{
					$cenatransportu = 24.00;		
				}
				else 
				{
					$cenatransportu = 0.00;
				}
			} 
			else if ($wagacalosc > 30)
			{
				$cenapaletowego = 1*110; 
				$cenatransportu = $cenapaletowego;
			}
		
		
			if ($pobranie == 1 && $wartosccalosc < 500) 
			{
				$cenatransportu = $cenatransportu+6;
			}
	 
		}
		else 
		{
			$cenapaletowego = $paletowy*110; 
			$cenatransportu = $cenapaletowego;
		
			if ($pobranie == 1) 
			{
				$cenatransportu = $cenatransportu+6;
			}
		}
	 
		return $cenatransportu;
	}
	
	public function getProducts() // pobranie zawartoĹci koszyka
	{
		return $this->_basket_products;
	}
}

$prices =  new Prices();
?>
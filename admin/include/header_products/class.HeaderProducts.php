<?php
class HeaderProducts extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_product_id;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
	}
	
	public function getHeaderProducts()
	{
		$result = $this->_db->query('SELECT products.id_hermes, product_versions.name FROM header_products JOIN products ON header_products.product_id = 
		products.id_hermes JOIN product_versions ON products.id_hermes = product_versions.product_id AND version_id = 1 ORDER BY product_versions.name ASC') 
		or $this->_db->raise_error();		
		
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getProducts()
	{
		$result = $this->_db->query('SELECT products.id_hermes, product_versions.name, symbol FROM products JOIN product_versions ON products.id_hermes = 
		product_versions.product_id WHERE version_id = 1 AND id_hermes NOT IN (SELECT product_id FROM header_products) AND subgroup_id > 0 ORDER BY symbol ASC') or $this->_db->raise_error();
		
		return $result;
	}
	
	public function deleteProduct($product_id)
	{
		$this->_db->query('DELETE FROM header_products WHERE product_id = '.$product_id) or $this->_db->raise_error();
	
		$communicats['ok'] = 'Produkt został usunięty z nagłówka';
		$this->setSession("communicats", $communicats);
	}
	
	public function addProduct()
	{
		$this->clear();
		
		$this->_product_id = (int)$_POST['product_id'];
		
		if (!$this->_product_id)
		{
			$this->_errors++;
			$errors['product_id'] = 'Produkt jest wymagany';
		}
		
		if (!$this->_errors)
		{
			$this->_db->query('INSERT INTO header_products VALUES ('.$this->_product_id.')') or $this->_db->raise_error();
			header("Location: ../header_products/list");
		}
		else 
		{
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}
?>
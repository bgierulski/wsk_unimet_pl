<?php
class Forms extends Validators
{
	private $_db;
	private $_errors;
	
	private $_title;
	private $_phone;
	private $_message;

	private $_id;
	private $_name;
	private $_company;
	private $_email;
	
	private $_street;
	private $_post_code;
	private $_city;
	private $_identificator;
	private $_contact;
	private $_region_id;
	private $_activity;
	private $_different_activity;
	private $_comment;
	
	public function __construct()
	{	
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		
		switch ($_POST['operation'])
		{
			case 'login':
				$this->_login(trim(mysql_real_escape_string($_POST['login'])), trim(mysql_real_escape_string($_POST['password'])));
				break;
				
			case 'page_question':
				$this->_formContactValidation();
				break;
				
			case 'password_question':
				$this->_formPasswordValidation();
				break;
				
			case 'registration':
				$this->_formRegistrationValidation();
				break;
		}
	}
	
	private function _login($login, $password)
	{
		$login = trim(str_replace(array('-'), array(''), $login));
		$changed_passsword = str_rot13(md5(str_rot13($password)));
		$user = $this->_db->query('SELECT users.id, id_hermes, name, user_name, user_surname, IFNULL(user_tree.parent_id, 0) as parent_id, user_tree.level, status, login, password, email, login_date, invoices, ordering, admin, promotions, price_group, discount, payment_id, merchant_id, observer FROM users LEFT JOIN user_tree ON users.id = user_tree.user_id WHERE (MD5(CONCAT(`key`, \''.$password.'\')) = password OR comment = \''.$changed_passsword.'\') AND login = \''.$login.'\' AND active = 1') or $this->_db->raise_error();
		
		if ($this->_db->num_rows($user))
		{
			$row = mysql_fetch_array($user);
			
			if (empty($user['password']))
			{
				$result = $this->_db->query('SELECT SUBSTRING(MD5(RAND()),-20) as value') or $this->_db->raise_error();
				$key = mysql_fetch_array($result);
				
				$this->_db->query('UPDATE users SET password = MD5(CONCAT(\''.$key['value'].'\', \''.$password.'\')), `key` = \''.$key['value'].'\' WHERE id = '.$row['id']) or $this->_db->raise_error();
			}
			
			$result = $this->_db->query('UPDATE users SET login_date = \''.date('Y-m-d H:i:s').'\', `login_counter` = `login_counter` + 1 WHERE id = '.$row['id'].' AND id_hermes = '.$row['id_hermes']) or $this->_db->raise_error();
			
			$values['id'] = $row['id'];
			$values['id_hermes'] = $row['id_hermes'];
			$values['login'] = $row['login'];
			$values['email'] = $row['email'];
			$values['login_date'] = $row['login_date'];
			$values['invoices'] = $row['invoices'];
			$values['ordering'] = $row['ordering'];
			$values['admin'] = $row['admin'];
			$values['observer'] = $row['observer'];
			$values['transport'] = $row['transport'];
			$values['promotions'] = $row['promotions'];
			$values['name'] = $row['name'];
			$values['user_name'] = $row['user_name'];
			$values['user_surname'] = $row['user_surname'];
			$values['price_group'] = $row['price_group'];
			$values['discount'] = $row['discount'];
			$values['status'] = $row['status'];
			$values['payment_id'] = $row['payment_id'];
			$values['merchant_id'] = $row['merchant_id'];
			
			if ($row['level'] > 1)
			{
				$result = $this->_db->query('SELECT parent_id, level FROM user_tree WHERE user_id = '.$row['parent_id']) or $this->_db->raise_error();
				
				if (mysql_num_rows($result))
				{
					$row = mysql_fetch_array($result);
					mysql_free_result($result);
					
					if ($row['level'] > 1)
					{
						$result = $this->_db->query('SELECT parent_id, level FROM user_tree WHERE user_id = '.$row['parent_id']) or $this->_db->raise_error();
						
						if (mysql_num_rows($result))
						{
							$row = mysql_fetch_array($result);
							mysql_free_result($result);
							
							if ($row['level'] > 1)
							{
								$result = $this->_db->query('SELECT parent_id, level FROM user_tree WHERE user_id = '.$row['parent_id']) or $this->_db->raise_error();
								
								if (mysql_num_rows($result))
								{
									$row = mysql_fetch_array($result);
									mysql_free_result($result);
								}
							}
						}
					}
				}
			}
			
			$values['parent_id'] = (($row['parent_id'] > 0) ? $row['parent_id'] : $row['id']);
			
			$this->setSession("b2b_user", $values);
			
			$this->_checkBasket();
			
			header("Location: ../zamowienia.html");
		}
	}
	
	private function _checkBasket()
	{
		if (isset($_COOKIE["b2b_basket_key"]))
		{
			$key = $_COOKIE["b2b_basket_key"];
			
			if (strlen($key) == 20)
			{
				$result = $this->_db->query('SELECT product_id, subgroup, name, `index`, tax, weight, size, unit, count, netto_price, netto_value, brutto_price, brutto_value, mpk, info, query_id FROM basket WHERE `key` = \''.mysql_real_escape_string($key).'\' ORDER BY name ASC') or $this->_db->raise_error();
			
				$basket_products = array();
				
				while($row = mysql_fetch_array($result)) 
		   		{
		       		array_push($basket_products, array(
		       			'product_id' => $row['product_id'], 
		       			'subgroup' => $row['subgroup'], 
		       			'name' => $row['name'], 
		       			'index' => $row['index'], 
		       			'tax' => $row['tax'], 
		       			'weight' => $row['weight'], 
		       			'size' => $row['size'], 
		       			'unit' => $row['unit'], 
		       			'count' => $row['count'], 
		       			'netto_price' => $row['netto_price'], 
		       			'netto_value' => $row['netto_value'], 
		       			'brutto_price' => $row['brutto_price'], 
		       			'brutto_value' => $row['brutto_value'], 
		       			'mpk' => $row['mpk'], 
		       			'info' => $row['info'], 
		       			'query_id' => $row['query_id']
		       		));
		   		}
		   		
		   		$this->_db->query('DELETE FROM basket WHERE `key` = \''.mysql_real_escape_string($key).'\'') or $this->_db->raise_error();
		   		$_SESSION["basket_products"] = $basket_products;
			}
		}
		
		setcookie("b2b_basket_key", date('U').$this->randomString(10), time()+604800, '/');
	}
	
	private function _formRegistrationValidation()
	{
		$this->clear();
		
		$this->_name = $values['name'] = trim(strip_tags($_POST['name']));
		$this->_company = $values['company'] = trim(strip_tags($_POST['company']));
		$this->_street = $values['street'] = trim(strip_tags($_POST['street']));
		$this->_post_code = $values['post_code'] = trim(strip_tags($_POST['post_code']));
		$this->_city = $values['city'] = trim(strip_tags($_POST['city']));
		$this->_identificator = $values['identificator'] = trim(strip_tags($_POST['identificator']));
		$this->_email = $values['email'] = trim(strip_tags($_POST['email']));
		$this->_phone = $values['phone'] = trim(strip_tags($_POST['phone']));
		$this->_contact = $values['contact'] = trim(strip_tags($_POST['contact']));
		$this->_region_id = $values['region_id'] = trim(strip_tags($_POST['region_id']));
		$this->_activity = $values['activity'] = trim(strip_tags($_POST['activity']));
		$this->_different_activity = $values['different_activity'] = trim(strip_tags($_POST['different_activity']));
		$this->_comment = $values['comment'] = trim(strip_tags($_POST['comment']));
		
		if ($this->_name == '')
		{
			$this->_errors++;
			$errors['name'] = 'Imię i nazwisko jest wymagane';
		}
		
		if ($this->_company == '')
		{
			$this->_errors++;
			$errors['company'] = 'Nazwa firmy jest wymagana';
		}
		
		if (in_array($this->_street, array('', 'ul.')))
		{
			$this->_errors++;
			$errors['street'] = 'Ulica lub miejscowość jest wymagana';
		}
		
		if (!$this->validatePostCode($this->_post_code))
		{
			$this->_errors++;
			$errors['post_code'] = 'Kod pocztowy jest niepoprawny';
		}
		
		if ($this->_city == '')
		{
			$this->_errors++;
			$errors['city'] = 'Poczta jest wymagana';
		}
		
		if ($this->_identificator != '' && !$this->validateNIP($this->_identificator))
		{
			$this->_errors++;
			$errors['identificator'] = 'Numer NIP jest niepoprawny';
		}
		
		if (!$this->validateEmail($this->_email))
		{
			$this->_errors++;
			$errors['email'] = 'E-mail jest niepoprawny';
		}
		
		if ($this->_phone == '')
		{
			$this->_errors++;
			$errors['phone'] = 'Numer telefonu jest wymagany';
		}
		
		if ($this->_region_id == '0')
		{
			$this->_errors++;
			$errors['region_id'] = 'Województwo jest wymagane';
		}
		
		if ($this->_activity == '' || ($this->_activity == 'inny' && empty($this->_different_activity)))
		{
			$this->_errors++;
			$errors['activity'] = 'Typ działalności jest wymagany.';
		}
		
		if (!$this->_errors)
		{
			$this->_sendRegistrationMessage();
		}
		else 
		{
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
	}
	
	private function _formPasswordValidation()
	{
		$this->clear();
		
		$this->_email = $values['email'] = trim(strip_tags($_POST['email']));
		
		if ($this->_email == '' || !$this->validateEmail($this->_email))
		{
			$this->_errors++;
			$errors['email'] = 'Adres e-mail jest wymagany.';
		}
		
		if (!$this->_errors)
		{
			$this->_sendPasswordMessage($this->_email);
		}
		else 
		{
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
	}
	
	private function _formContactValidation()
	{
		$this->clear();
		
		$this->_title = $values['title'] = trim($_POST['title']);
		$this->_email = $values['email'] = trim(strip_tags($_POST['email']));
		$this->_phone = $values['phone'] = trim(strip_tags($_POST['phone']));
		$this->_message = $values['message'] = trim(strip_tags(stripslashes($_POST['message'])));
			
		if ($this->_title == '')
		{
			$this->_errors++;
			$errors['title'] = 'Temat wiadomości jest wymagany.';
		}
		
		if ($this->_email == '' || !$this->validateEmail($this->_email))
		{
			$this->_errors++;
			$errors['email'] = 'Adres e-mail jest wymagany.';
		}
		
		if (!empty($this->_phone) && !$this->validatePhone($this->_phone))
		{
			$this->_errors++;
			$errors['phone'] = 'Numer telefonu jest niepoprawny.';
		}
		
		if (str_replace('<br />', '', nl2br($this->_message)) == '')
		{
			$this->_errors++;
			$errors['message'] = 'Treść wiadomości jest wymagana.';
		}
	
		if (!$this->_errors)
		{
			$this->_sendContactMessage();
		}
		else 
		{
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
	}
	
	private function _sendPasswordMessage($email)
	{
		$this->clear();
		
		$symbols = '1234567890qwertyuiopasdfghjkklzxcvbnm';
   		$password = '';

		for ($i=0; $i<5; $i++)
		{
			$password .= $symbols[rand()%(strlen($symbols))];
		} 
		
		$controll = $this->_db->query('SELECT SUBSTRING(MD5(RAND()),-20) as value') or $this->_db->raise_error();
		$key = mysql_fetch_array($controll);
			
		$this->_db->query('UPDATE users SET password = MD5(CONCAT(\''.$key['value'].'\', \''.$password.'\')), `key` = \''.$key['value'].'\', comment = \''.$password.'\' WHERE email = \''.$this->_email.'\'') or $this->_db->raise_error();
	
		$message = '<p>Twoje hasło w serwisie b2b.unimet.pl/wsknew zostalo zmienione</p>';
		$message .= '<p>Nowe hasło to: <strong>'.$password.'</strong></p>';
		$message .= '<p>Możesz je zmienić po zalogowaniu się do serwisu</p>';
		
		$this->_send('Przypomnienie hasła w serwisie b2b.unimet.pl/wsknew', $message, $email);
	}
	
	private function _sendRegistrationMessage()
	{
		$this->clear();
		
		$message = '<p><b>Imię i nazwisko:</b>  '.$this->_name.'</p>';
		$message .= '<p><b>Nazwa firmy:</b>  '.$this->_company.'</p>';
		$message .= '<p><b>Ulica:</b>  '.$this->_street.'</p>';
		$message .= '<p><b>Kod pocztowy:</b>  '.$this->_post_code.'</p>';
		$message .= '<p><b>Miejscowość:</b>  '.$this->_city.'</p>';
		$message .= '<p><b>NIP:</b>  '.$this->_identificator.'</p>';
		$message .= '<p><b>E-mail:</b>  '.$this->_email.'</p>';
		$message .= '<p><b>Telefon:</b>  '.$this->_phone.'</p>';
		if (!empty($this->_contact))
		{
		$message .= '<p><b>Osoba do kontaktu:</b> '.$this->_contact.'</p>';
		}
		$message .= '<p><b>Województwo:</b> '.$this->_region_id.'</p>';
		$message .= '<p><b>Rodzaj działalności:</b><br /> '.(($this->_activity != 'inny') ? $this->_activity : $this->_different_activity).'</p>';
		if (!empty($this->_comment))
		{
			$message .= '<p><b>Uwagi:</b><br /> '.$this->_comment.'</p>';
		}
		
		$this->_send('Zgłoszenie do rejestracji', $message);
	}
	
	private function _sendContactMessage()
	{		
		$this->clear();
		
		$message = '<p><b>Wiadomość:</b><br /> '.$this->_message.'</p>';
		$message .= '<p><b>E-mail:</b> '.$this->_email.'</p>';
		if (!empty($this->_phone))
		{
			$message .= '<p><b>Telefon:</b> '.$this->_phone.'</p>';
		}
		
		$this->_send($this->_title, $message);
	}	
	
	private function _send($title, $message, $email = '')
	{
		$mimemail = new nomad_mimemail(); 
		
		$from		= "newsletter@unimet.pl";	 
		$subject	= $title; 
		$html		= '<html><body>'.iconv("UTF-8", "ISO-8859-2", $message).'</body></html>';
		
		$mimemail->set_smtp_log(true); 
		$smtp_host	= "swarog.az.pl";
		$smtp_user	= "newsletter@unimet.pl";
		$smtp_pass	= "1hadesa2madmax";
		$mimemail->set_smtp_host($smtp_host);
		$mimemail->set_smtp_auth($smtp_user, $smtp_pass); 
		
		if ($email == '')
		{
			$result = $this->_db->query('SELECT email, registration_email FROM users WHERE email != \'\' AND id_hermes = 0 AND status = 0') 
			or $this->_db->raise_error();
			
			while($row = mysql_fetch_array($result)) 
	   		{
	   			if ($this->validateEmail($row['email']))
	   			{
		       		$to	= $row['email'];	
				
					$mimemail->new_mail($from, $to, iconv("UTF-8", "ISO-8859-2", $subject), "", $html);
					
					if ($title != 'Zgłoszenie do rejestracji' || $row['registration_email'] == 1)
					{
						if ($mimemail->send())
						{
							unset($_SESSION["communicats"]['error']);
							$comunicats['ok'] = 'Wiadomość została wysłana';
						}
						else
						{
							unset($_SESSION["communicats"]['ok']);
							$comunicats['error'] = 'Wysłanie wiadomości nie powiodło się.';
						}	
					}
	   			}
	   		}
		}
		else
		{
			$mimemail->new_mail($from, $email, iconv("UTF-8", "ISO-8859-2", $subject), "", $html);
			
			if ($mimemail->send())
			{
				unset($_SESSION["communicats"]['error']);
				$comunicats['ok'] = 'Wiadomość została wysłana';
			}
			else
			{
				unset($_SESSION["communicats"]['ok']);
				$comunicats['error'] = 'Wysłanie wiadomości nie powiodło się.';
			}	
		}
		
		$this->setSession("communicats", $comunicats);
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}

$forms = new Forms();
?>
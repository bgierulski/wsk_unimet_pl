<?php
class Pages extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_page_id;
	private $_title;
	private $_content;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
	}
	
	public function getPages()
	{
		$result = $this->_db->query('SELECT id, title FROM pages ORDER BY id ASC') or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getPage($page_id)
	{
		$result = $this->_db->query('SELECT id, title, content FROM pages WHERE id = '.$page_id) or $this->_db->raise_error();
		
		$row = mysql_fetch_array($result);
		
		$values['title'] = $row['title'];
		$values['content'] = str_replace('\"', '"', $row['content']);
		
		$this->setSession("values", $values);
	}
	
	public function validatePage()
	{
		$this->clear();
		
		$this->_page_id = (int)$_POST['page_id'];
		$this->_title = $values['title'] = trim(mysql_escape_string($_POST['title']));
		$this->_content = $values['content'] = trim(mysql_escape_string($_POST['content']));
		
		if (empty($this->_title))
		{
			$this->_errors++;
			$errors['title'] = 'Tytuł podstrony jest wymagany';
		}
		
		if (!$this->validateTextarea($this->_content) && !in_array($this->_page_id, array(10)))
		{
			$this->_errors++;
			$errors['description'] = 'Treść podstrony jest wymagana';
		}
		
		if (!$this->_errors)
		{
			if (isset($_POST['add_page']))
			{
				$this->_addPage();
			}
			else 
			{
				$this->_saveChanges();
			}
		}
		else 
		{

			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
		
	}
	
	private function _addPage()
	{		
	}
	
	private function _saveChanges()
	{
		$this->_db->query('UPDATE pages SET title = \''.$this->_title.'\', content = \''.$this->_content.'\' WHERE id = '.$this->_page_id) 
		or $this->_db->raise_error();
		
		$communicats['ok'] = 'Podstrona została wyedytowana';
		$this->setSession("communicats", $communicats);
		header("Location: ../../pages/list");
		
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}
?>
<?php
if ($controller != 'authentication' && $action != 'logout')
{
	$path = BASE_DIR.$controller.'/index.php';
	
	if (file_exists($path))
	{
		include($path);
	}
	else 
	{
		include('default/index.php');
	}
}
?>
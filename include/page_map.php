<div id="page_header">
	<div class="details">
		<p><strong>Mapa strony</strong></p>
	</div>
	<div class="clear"></div>
</div>

<div id="page_content">
<ul>
	<li><a href="./">Oferta</a></li>
	<?php if ($_SESSION["b2b_user"]['id']) { ?>
	<li>Zamówienia
		<ul>
			<li><a href="./szablony.html">Szablony zamówień</a></li>
			<li><a href="./zamowienia,podopieczni.html">Zamówienia wymagajace zatwierdzenia</a></li>
			<li><a href="./zamowienia,oczekujace.html">Zamówienia oczekujace</a></li>
			<li><a href="./zamowienia,realizowane.html">Zamówienia w trakcie realizacji</a></li>
			<li><a href="./zamowienia,zrealizowane.html">Zamówienia zrealizowane</a></li>
			<li><a href="./zamowienia,archiwalne.html">Zamówienia archiwalne</a></li>
		</ul>
	</li>
	<li><a href="./zapytania,lista.html">Zapytania</a></li>
	<?php } ?>
	<?php if ($_SESSION["b2b_user"]['invoices']) { ?>
	<li><a href="./dokumenty.html">Dokumenty</a>
		<ul>
			<li><a href="./dokumenty.html">Faktury VAT</a></li>
			<li><a href="./dokumenty,do_zaplaty.html">Faktury VAT do zaplaty</a></li>
			<li><a href="./dokumenty,po_terminie.html">Faktury VAT po terminie</a></li>
		</ul>
	</li>
	<?php } ?>
	<li><a href="./nowosci.html">Nowości</a></li>
	<li><a href="./promocje.html">Promocje</a></li>
	<?php if ($_SESSION["b2b_user"]['id']) { ?>
	<li><a href="./konto,edycja.html">Zarzązanie kontem</a></li>
	<?php } ?>
	<li><a href="./kontakt.html">Kontakt</a></li>
	<li><a href="./profit.html">Program Profit</a></li>
</ul>
</div>
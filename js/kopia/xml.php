<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start();

header("Content-Encoding: gzip");
header ('Content-Type: application/xml; charset=utf-8');
ob_start();

include('../include/config.php');
include(BASE_DIR.'class/class.DataBase.php');
include(BASE_DIR.'class/class.Validators.php');
include(BASE_DIR.'class/class.Groups.php');
include(BASE_DIR.'class/class.Products.php');
include(BASE_DIR.'class/class.Producers.php');
include(BASE_DIR.'class/class.Prices.php');
include(BASE_DIR.'class/class.Cashe.php');
include(BASE_DIR.'class/class.Users.php');
include(BASE_DIR.'class/class.Informations.php');
include(BASE_DIR.'class/class.Basket.php');
include(BASE_DIR.'class/class.Orders.php');
			
$db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
$validators = new Validators();
$basket = new Basket();

$sql = new Cashe;
$sql->sql_connect(HOST, LOGIN, PASSWORD, DATABASE);

$_SESSION["b2b_user"]['id_hermes'] = ((isset($_SESSION["b2b_user"]['id_hermes'])) ? $_SESSION["b2b_user"]['id_hermes'] : (int)$_POST['login_id']);

$xml = '<response>';

$operation = $_POST['operation'];

switch ($operation)
{
	case "get_mpk_paths":
		
		$xml .= '<html><![CDATA[
		<table class="proteges_list">
		<tr>
		<th style="width:150px;">MPK</th>
		<th>Budżet</th>
		<th>Analityk</th>
		<th>Kierownik</th>
		<th>Odbiorca</th>
		</tr>';
		
		if (is_array($_POST["data"]) && !empty($_POST["data"]))
		{
			$mpk_list = $basket->getMpkPaths($_POST["data"]);
			$budgets_list = $users->getBudgets();
			$users_list = $users->getUsersList();
			
			$counter = 0;
			
			foreach ($mpk_list as $key => $row)
			{
				if ($counter%2 == 0)
				{
					$class = 'bg_1';
				}
				else 
				{
					$class = 'bg_2';
				}
				
				$xml .= '<tr>
					<td class="'.$class.'">'.$row['mpk'].' <input type="hidden" name="mpk_keys[]" value="'.$row['mpk'].'" /></td>
					<td class="'.$class.'">
						<select name="budget[]" style="width:90%">';
				
						if ($row['budget'] == 0) 
						{
							$xml .= '<option value="0">-- wybierz --</option>';
						}
						
						foreach ($budgets_list as $budget)
						{
							if (($row['budget'] > 0 && $budget['id'] == $row['budget']) || $row['budget'] == 0)
							{
								$xml .= '<option value="'.$budget['id'].'" '.((($budget['id'] == $row['budget']) || (is_array($_SESSION["values"]["budget"]) && $budget['id'] == $_SESSION["values"]["budget"][$counter])) ? 'selected="selected"' : '').'>'.$budget['department'].'</option>';
							}
						}
						
						$xml .= '</select>
					</td>
					<td class="'.$class.'">
						<select name="analyst[]" style="width:90%">';
						
						if (count($row["analyst"]) > 1 || empty($row["analyst"])) 
						{ 
							$xml .= '<option value="0">-- wybierz --</option>';
						}
						
						foreach ($users_list as $user)
						{
							if ($user['analyst'] > 0 && (empty($row["analyst"]) || (!empty($row["analyst"]) && in_array($user['id'], $row["analyst"]))))
							{
								$xml .= '<option alt="'.$user['budget'].'" style="display:'.(($_SESSION["values"]["budget"][$counter] == $user['budget']) ? "block" : "none").'" value="'.$user['id'].'" '.((is_array($_SESSION["values"]["analyst"]) && $user['id'] == $_SESSION["values"]["analyst"][$counter] && $_SESSION["values"]["budget"][$counter] == $user['budget']) ? 'selected="selected"' : '').'>'.$user['login'].'</option>';
							}
						}
						
						$xml .= '</select>
					</td>
					<td class="'.$class.'">
						<select name="manager[]" style="width:90%">';
						
						if (count($row["manager"]) > 1 || empty($row["manager"])) 
						{
							$xml .= '<option value="0">-- wybierz --</option>';
						} 
						
						foreach ($users_list as $user)
						{
							if ($user['manager'] > 0 && (empty($row["manager"]) || (!empty($row["manager"]) && in_array($user['id'], $row["manager"]))))
							{
								$xml .= '<option alt="'.$user['budget'].'" style="display:'.(($_SESSION["values"]["budget"][$counter] == $user['budget']) ? "block" : "none").'" value="'.$user['id'].'" '.((is_array($_SESSION["values"]["manager"]) && $user['id'] == $_SESSION["values"]["manager"][$counter] && $_SESSION["values"]["budget"][$counter] == $user['budget']) ? 'selected="selected"' : '').'>'.$user['login'].'</option>';
							}
						}
						
						$xml .= '</select>
					</td>
					<td class="'.$class.'">
						<select name="receiving[]" style="width:90%">';
						
						if (count($row["receiving"]) > 1 || empty($row["receiving"])) 
						{ 
							$xml .= '<option value="0">-- wybierz --</option>';
						} 
						
						foreach ($users_list as $user)
						{
							if ($user['receiving'] > 0 && (empty($row["receiving"]) || (!empty($row["receiving"]) && in_array($user['id'], $row["receiving"]))))
							{
								$xml .= '<option alt="'.$user['budget'].'" style="display:'.(($_SESSION["values"]["budget"][$counter] == $user['budget']) ? "block" : "none").'" value="'.$user['id'].'" '.((is_array($_SESSION["values"]["receiving"]) && $user['id'] == $_SESSION["values"]["receiving"][$counter] && $_SESSION["values"]["budget"][$counter] == $user['budget']) ? 'selected="selected"' : '').'>'.$user['login'].'</option>';
							}
						}
						
						$xml .= '</select>
					</td>
				</tr>';
						
				$counter++;
			}
		}
		else
		{
			$xml .= '<tr><td class="bg_1" colspan="5">nie wybrano numeru mpk</td></tr>';
		}
		
		$xml .= '</table>]]></html>';
		
		break;
	
	case 'change_order_amount':
		$data = $orders->changeOrderAmount((int)$_POST['id'], (int)$_POST['order_id'], (float)$_POST['amount']);
		
		if (is_array($data) && !empty($data))
		{
			$xml .= '<controll>'.$data[0].'</controll>'."\n";
			$xml .= '<netto_value><![CDATA['.$data[1].']]></netto_value>'."\n";
			$xml .= '<brutto_value><![CDATA['.$data[2].']]></brutto_value>'."\n";
			$xml .= '<netto_sum><![CDATA['.$data[3].']]></netto_sum>'."\n";
		}
		break;
	
	case 'set_mpk':
		
		$basket->setMPK($_POST['value'], (int)$_POST['key']);
		
		break;
	
	case 'get_producers':
		
		$producers_list = $producers->getProducers();
		$producers_array = $db->mysql_fetch_all($producers_list);
		
		$xml .= '<options><![CDATA[';
		
		if (is_array($producers_array))
		{
			foreach ($producers_array as $i => $producer)
		   	{
		   		if ($producer['id'] != $producers_array[$i-1]['id'])
		   		{
		       		$xml .= '<option class="group_'.(int)$producer['group_id'].'" value="'.$producer['id'].'">'.trim($producer['name']).'</option>';
		   		}
		   	} 
		}
		
		$xml .= ']]></options>'."\n";
		
		break;
	
	case 'narrowed_offer':
		
		$_SESSION["b2b_full_offer"] = false;
		$_SESSION["b2b"]['producer_id'] = 0;
		
		$xml .= '<id><![CDATA[full_offer]]></id>'."\n";
		$xml .= '<title><![CDATA[PEŁNA OFERTA UNIMET]]></title>'."\n";
		$xml .= '<amount><![CDATA['.$products->getProductsSum(true).']]></amount>'."\n";
		break;
	
	case 'full_offer':
		
		$_SESSION["b2b_full_offer"] = true;
		$_SESSION["b2b"]['producer_id'] = 0;
		
		$xml .= '<id><![CDATA[narrowed_offer]]></id>'."\n";
		$xml .= '<title><![CDATA[ZAWĘŻONA OFERTA]]></title>'."\n";
		$xml .= '<amount><![CDATA['.$products->getProductsSum(false).']]></amount>'."\n";
		break;
	
	case 'check_comment':
		
		$id = (int)$_POST['id'];
		
		if ($id)
		{
			$informations->checkInformation($id);
		}
		
		break;
	
	case 'clear_basket':
		
		unset($_SESSION["basket_products"]);
		$basket->clearBasket($_COOKIE["b2b_basket_key"]);
		
		break;
	
	case 'check_basket':
		
		if (true != $_SESSION["b2b_user"]["basket_checked"])
		{
			$xml .= '<basket>'.((count($_SESSION["basket_products"])) ? 1 : 0).'</basket>';
			$_SESSION["b2b_user"]["basket_checked"] = true;
		}
		
		break;
	
	case 'get_request_uri':
		
		$xml .= '<request_uri><![CDATA['.substr(BASE_ADDRESS, 0, -9).$_SESSION['REQUEST_URI'].']]></request_uri>';
		
		break;
	
	case 'get_person':
		
		$person_type = $_POST['type'];
		$person_id = (int)$_POST['id'];
		$person = $users->getPerson($person_type, $person_id);
		
		$xml .= '<content><![CDATA[';
				
		$xml .= '<div><p><img src="images/';
		
		if (($person_type == 'merchant' && $person['merchant_image'] != '') || ($person_type == 'guardian' && $person['guardian_image'] != ''))
		{
			$xml .= 'merchants/'.$person_id.'/'.(($person_type == 'merchant') ? $person['merchant_image'] : $person['guardian_image']);
		}
		else
		{
			$xml .= 'element_10.png';
		}
			
		$xml .= '" alt="" /></p>';
		$xml .= '<div>';
		$xml .= '<p><strong>'.(($person_type == 'merchant') ? $person['name'] : $person['guardian_name']).'</strong></p>';
		
		if (($person_type == 'merchant' && $person['phone'] != '') || ($person_type == 'guardian' && $person['guardian_phone'] != ''))
		{
			$xml .= '<p>tel.: '.(($person_type == 'merchant') ? $person['phone'] : $person['guardian_phone']).'</p>';
		}
		if (($person_type == 'merchant' && $person['email'] != '') || ($person_type == 'guardian' && $person['guardian_email'] != ''))
		{
			$xml .= '<p>email: '.(($person_type == 'merchant') ? $person['email'] : $person['guardian_email']).'</p>';
		}
		$xml .= '</div><div class="clear"></div></div>';
		
		$xml .= '<div>'.(($person_type == 'merchant') ? nl2br($person['description']) : nl2br($person['description'])).'</div>';
		
		$xml .= ']]></content>';
		
		break;
	
	case 'get_description':
		
		$product = $products->getDescription((int)$_POST['product_id']);
		
		$description = strip_tags($product['description']);
		
		$xml .= '<description><![CDATA['.((strlen($description) > 300) ? substr($description, 0, 300).' ...' : $description).']]></description>';
		
		break;
	
	case 'get_products':
		
		$group_id = (int)$_POST['group_id'];
		$producer_id = (int)$_POST['producer_id'];
		
		$group_id = $groups->checkGroup($group_id, $producer_id);
		
		$xml .= '<content><![CDATA[';
		$xml .= '<span class="actual_group" id="actual_'.$group_id.'"></span>';
		
		$parents = $groups->getParents($group_id);
		$parents_count = count($parents) - 1;
		
		$xml .= '<div id="page_header">';
			$xml .= '<div class="details">';
				$xml .= '<p class="parents"><strong>';
				
				for ($i=$parents_count; $i>=0; $i--)
				{
					$group_name = $groups->getGroupName($parents[$i]);
					
					if ($i < $parents_count)
					{
						$xml .= '<a href="./grupa_'.$validators->validateName($group_name, 2).','.$parents[$i].(($_SESSION["b2b"]['producer_id']) ? ',1.'.$_SESSION["b2b"]['producer_id'] : '').'.html">';
					}
					$xml .= $validators->strToLower($group_name); 
					
					if ($i < $parents_count)
					{
						$xml .= '</a>';
					}
					
					if ($i > 0)
					{
						$xml .= '<span>&#187;</span>';
					}
				}
				
				$xml .= '</strong></p>';
				$xml .= '<p>Produktów w grupie: ';
				$xml .= $groups->getProductsCount($group_id, $producer_id, (int)$_SESSION['visibility'], $_POST["search"]); 
				$xml .= '<span class="comment"></span>';
				$xml .= '</p>';
			$xml .= '</div>';
			
			$xml .= '<div class="box">';
				$xml .= '<p><span id="select_products">zaznacz</span> / <span id="unselect_products">odznacz</span> wszystkie</p>';
			$xml .= '</div>';
			
			$xml .= '<div class="box">';
			$xml .= '<form method="post" action="./grupa_'.$validators->validateName($group_name, 2).','.$group_id.(($_SESSION["b2b"]['producer_id']) ? ',1.'.$_SESSION["b2b"]['producer_id'] : '').'.html" name="visibility">';
				$xml .= '<input type="hidden" name="visibility_change" value="" />';
				$xml .= '<p>Pokazuj tylko dostępne <input type="checkbox" name="visibility" id="visibility_form_submit" value="1" '.(($_SESSION['visibility'] == 1) ? 'checked="checked"' : '').' /></p>';
			$xml .= '</form>';
			$xml .= '</div>';
			
			$xml .= '<div class="box">';
			$xml .= '<form method="post" action="./grupa_'.$validators->validateName($group_name, 2).','.$group_id.(($_SESSION["b2b"]['producer_id']) ? ',1.'.$_SESSION["b2b"]['producer_id'] : '').'.html" name="sort">';
				$xml .= '<p>Sortuj według: ';
				$xml .= '<select name="sort" id="sort_form_submit">';
				$xml .= '<option value="product_asc" '.(($_SESSION['products_sort'] == 'product_asc') ? 'selected="selected"' : '').'>Nazwy</option>';
				$xml .= '<option value="producer_asc" '.(($_SESSION['products_sort'] == 'producer_asc') ? 'selected="selected"' : '').'>Producenta</option>';
				$xml .= '<option value="price_asc" '.(($_SESSION['products_sort'] == 'price_asc') ? 'selected="selected"' : '').'>Ceny rosnąco</option>';
				$xml .= '<option value="price_desc" '.(($_SESSION['products_sort'] == 'price_desc') ? 'selected="selected"' : '').'>Ceny malejąco</option>';
				$xml .= '</select>';
				$xml .= '</p>';
			$xml .= '</form>';
			$xml .= '</div>';
			
			$xml .= '<div class="box">';
				$xml .= '<p class="legend" id="legend_1"><strong>LEGENDA</strong></p>';
				$xml .= '<div class="legend_details" id="legend_details_1">'.file_get_contents(BASE_DIR.'products_lengend.php').'</div>';
			$xml .= '</div>';
			
			$xml .= '<div class="clear"></div>';
			
		$xml .= '</div>';

		$producers_list = $producers->getProducers($group_id);
		$promotions = $products->getPromotions();
		$products_list = $products->getProducts($group_id, $_SESSION['products_sort'], $producer_id, $_SESSION['visibility'], $_POST["search"]);
		$products_count = count($products_list);
		
		if ($products_count)
		{
			$xml .= '<div id="producers">';
			
			$producers_array = $db->mysql_fetch_all($producers_list);
			
			if (is_array($producers_array))
			{
				foreach ($producers_array as $producer)
				{
					$xml .= '<div '.(($producer['id'] == $producer_id) ? 'class="active"' : '').'>';
					$xml .= '<p><strong><a href="./grupa_'.$validators->validateName($producer['name'], 2).','.$group_id.',1.'.$producer['id'].'.html">'.$producer['name'].'</a></strong></p>';
					$xml .= '</div>';
				}
			}
			
			$xml .= '<div class="clear"></div>';
			$xml .= '</div>';
		}
		
		$xml .= '<div id="page_content">';
		
		if ($products_count)
		{
			$xml .= '<table cellspacing="0" cellpadding="0" class="subgroup" style="border-bottom:none;">';
			$xml .= '<tr>';
				$xml .= '<th class="td_image">&nbsp;</th>';
				$xml .= '<th class="td_product"><p>Produkt</p><p><input type="text" name="search[0]" value="" style="width:80%" /> <img alt="" style="position:relative; top:3px; cursor:pointer" id="search_0" src="images/magnifier.png" /></p></th>';
				$xml .= '<th class="td_symbol"><p>Symbol</p><p><input type="text" name="search[1]" value="" style="width:80%" /> <img alt="" style="position:relative; top:3px; cursor:pointer" id="search_1" src="images/magnifier.png" /></p></th>';
				$xml .= '<th class="status"><p>&nbsp;</p></th>';
				$xml .= '<th class="td_description"><div>&nbsp;</div></th>';
				$xml .= '<th class="td_replacement"><div>&nbsp;</div></th>';
				$xml .= '<th class="td_weight"><div>Waga</div></th>';
				$xml .= '<th class="td_producer"><div>Producent</div><div style="padding-left:5px"><input type="text" name="search[2]" value="" style="width:80%" /> <img alt="" style="position:relative; top:3px; cursor:pointer" id="search_2" src="images/magnifier.png" /></div></th>';
				$xml .= '<th class="td_availability"><div>&nbsp;</div></th>';
				
				if (true !== $_SESSION["b2b_full_offer"])
				{
					$xml .= '<th class="td_price"><div>Cena netto</div></th>';
				}
				
				$xml .= '<th class="td_amount"><div>Ilość</div></th>';
				$xml .= '<th class="td_unit"><div>JM.</div></th>';
			$xml .= '</tr>';
			$xml .= '</table>';
			
			$xml .= '<form method="POST" action="./grupa_'.$validators->validateName($group_name, 2).','.$group_id.(($_SESSION["b2b"]['producer_id']) ? ',1.'.$_SESSION["b2b"]['producer_id'] : '').'.html" name="to_basket">';
			$xml .= '<div id="products_content">';
				$xml .= '<table cellspacing="0" cellpadding="0" class="subgroup" style="border-top:none;">';
				$xml .= '<input id="product_operation" type="hidden" name="operation" value="to_basket" />';
				
				foreach ($products_list as $i => $product)
				{
					$xml .= '<tr>';
						$xml .= '<td class="td_image">';
							$xml .= '<div class="wrapper" id="product_'.$product['id_hermes'].'"><div class="cell"><div class="hack"><p>';
							$xml .= '<img src="'.BASE_ADDRESS.'/include/image.php?id='.$product['id_hermes'].'&w=38&h=38" alt="" border="0">';
							$xml .= '</p></div></div></div>';
						$xml .= '</td>';
						$xml .= '<td class="td_product">';
							$xml .= '<strong><a onClick="javascript:pop_up('.$product['id_hermes'].');" style="cursor:pointer">'.$product['name'].'</a></strong>';
						$xml .= '</td>';
						$xml .= '<td class="td_symbol" style="padding-left:10px;">';
							$xml .= $product['symbol'];
						$xml .= '</td>';
						$xml .= '<td class="status">';
						
						if (in_array($product['id_hermes'], $promotions) && $_SESSION["b2b_user"]['promotions'] == 1) { $xml .= '<p><span>P</span></p>'; }
						if ($product['news'] >= 0) { $xml .= '<p><strong>N</strong></p>'; }
						if (!in_array($product['id_hermes'], $promotions) && $product['news'] < 0) { $xml .= '&nbsp;'; }
						
						$xml .= '</td>';
						$xml .= '<td class="td_description">';
							$xml .= '<p>'.((!empty($product['description'])) ? '<p><a onClick="javascript:pop_up('.$product['id_hermes'].');" style="cursor:pointer"><strong id="description_'.$product['id_hermes'].'"><img src="images/comment.png" alt="" /></strong></a></p>' : '').'</p>';
						$xml .= '</td>';
						$xml .= '<td class="td_replacement"><p>'.((!empty($product['replacement'])) ? '<img src="images/replacement.png" alt="" onClick="javascript:pop_up('.$products->getProductId($product['replacement']).');" style="cursor:pointer" />' : '').'</p></td>';
						
						$xml .= '<td class="td_weight">';
							$xml .= '<p>';
							if ($product['weight'] > 0) { $xml .= $product['weight']; } else { $xml .= 'b/d'; }
							$xml .= '<span>'; if ($product['pallet'] == 1) { $xml .= '<img src="images/element_3.png" alt="" title="transport paletowy" />'; } $xml .= '</span>';
							$xml .= '</p>';
						$xml .= '</td>';
						$xml .= '<td class="td_producer">';
							$xml .= '<p>'.$product['producer_name'].'</p>';
						$xml .= '</td>';
						$xml .= '<td class="td_availability">';
							$xml .= '<p>';
							
							if ($product['status'] > 0) 
							{ 
								switch ($product['status'])
								{
									case 1: $xml .= '<img src="images/mag/magaznyn_ziel2.png" alt="" />'; break;
									case 2: $xml .= '<img src="images/mag/magaznyn_czer2.png" alt="" />'; break;
									case 3: $xml .= '<img src="images/mag/magaznyn_nieb2.png" alt="" />'; break;
									case 4: $xml .= '<img src="images/mag/magaznyn_czar2.png" alt="" />'; break;
								}
							}
							else
							{
								$xml .= '<img src="./magazyn.php?id='.$product['id_hermes'].'" alt="" />';
							}
							
							$xml .= '</p>';
						$xml .= '</td>';
						
						if (true !== $_SESSION["b2b_full_offer"])
						{
							$xml .= '<td class="td_price">';
								$xml .= '<p>'.$prices->getPrice($product['id_hermes'], 4).' zł</p>';
							$xml .= '</td>';
						}
						
						$xml .= '<td class="td_amount">';
							$xml .= '<div>';
							$xml .= '<span><input class="product_count" type="text" id="count_'.$product['id_hermes'].'" name="counts['.$product['id_hermes'].']" value="1" /></span>';
							$xml .= '<span><input class="product_checkbox" type="checkbox" id="active_'.$product['id_hermes'].'" name="actives[]" value="'.$product['id_hermes'].'" /></span>';
							$xml .= '<span class="favorite_product"><img id="favorite_'.$product['id_hermes'].'" src="images/element_4.png" alt="" title="dodaj do ulubionych" /></span>';
							$xml .= '</div>';
						$xml .= '</td>';
						$xml .= '<td class="td_unit">';
							$xml .= '<p>'.strtolower($product['unit']).'</p>';
						$xml .= '</td>';
					$xml .= '</tr>';
				}
				
				$xml .= '</table>';
			$xml .= '</div>';
		}
		
		$xml .= '</div>';
		
		if ($products_count)
		{			
			$xml .= '<div class="action">';
				$xml .= '<div style="float:left;">';
					$xml .= '<p class="button legend" id="legend_2"><a onClick="javascript:return false;" style="cursor:pointer; width:90px">LEGENDA</a></p>';
					$xml .= '<div class="legend_details" id="legend_details_2" style="padding:5px;">'.file_get_contents(BASE_DIR.'products_lengend.php').'</div>';
				$xml .= '</div>';
				$xml .= '<div>';
					if (true !== $_SESSION["b2b_full_offer"])
					{
						$xml .= '<p class="button" style="padding-right:40px;">';
						$xml .= '<a onClick="javascript:favorite_products();" style="cursor:pointer;">PRODUKTY ULUBIONE</a>';
						$xml .= '</p>';
						$xml .= '<p class="button" style="padding-right:40px;">';
						$xml .= '<a href="./" id="add_to_favorite">DODAJ DO ULUBIONYCH</a>';
						$xml .= '</p>';
						$xml .= '<p class="button">';
						$xml .= '<a href="./" id="add_to_order">DODAJ DO ZAMÓWIENIA</a>';
						$xml .= '</p>';
					}
					else
					{
						$xml .= '<p class="button">';
						$xml .= '<a href="./" id="add_to_order">DODAJ DO ZAPYTANIA</a>';
						$xml .= '</p>';
					}
				$xml .= '</div>';
				$xml .= '<div class="clear"></div>';
			$xml .= '</div>';
							
			$xml .= '</form>';
		}
		
		$xml .= ']]></content>';		
		
		break;
	
	case 'get_parents':
		
		$group_id = (int)$_POST['group_id'];
		
		$parents = $groups->getParents($group_id);
		
		if (is_array($parents))
		{
			foreach ($parents as $parent)
			{
				$xml .= '<id>'.$parent.'</id>';
			}
		}
		
		break;
		
	case 'get_pictograms':
		
		$data = $groups->getPictograms((int)$_POST['group_id']);
		
		if (is_array($data[1]) && !empty($data[1]))
		{
			$xml .= '<parent_id>'.(int)$data[0].'</parent_id>'."\n";
			
			foreach ($data[1] as $group)
			{
				$xml .= '<group>'.$group['level'].'_'.$group['id'].'</group>'."\n";
			}
		}
		
		break;
	
	case 'add_favorite':
		
		$product_id = (int)$_POST['product_id'];
		
		if ($_SESSION["b2b_user"])
		{
			$db->query('DELETE FROM favorite_products WHERE user_id = '.$_SESSION["b2b_user"]['id'].' AND product_id = '.$product_id) or $db->raise_error();
			$result = $db->query('INSERT INTO favorite_products VALUES (\'\', '.(int)$_SESSION["b2b_user"]['id'].', '.$product_id.')') or $db->raise_error();
			
			$xml .= '<status>'.(($result) ? 1 : 0).'</status>';
		}
		else
		{
			$xml .= '<status>0</status>';
		}
		
		break;
	
	case 'get_tree':
		
		$producer_id = (int)$_POST['producer_id'];
		$group_id = (int)$_POST['group_id'];
		
		$_SESSION["b2b"]['producer_id'] = $producer_id;
		
		$query = $groups->checkUser($producer_id);
		
		if (false === $query) // drzewo standardowe
		{
			$result = $db->query('SELECT DISTINCT groups_tree.order as id FROM products JOIN groups_tree ON products.subgroup_id = groups_tree.id 
			WHERE producer_id = '.$producer_id) or $db->raise_error();
			
			while($row = mysql_fetch_array($result)) 
	   		{
	       		$groups_list[] = $row;
	   		}
	
			$groups_count = count($groups_list);
			
			$query = 'SELECT id, parent_id as parent, (SELECT id FROM groups_tree WHERE `order` = parent) as parent_id, level, name, 
			comment AS controll, (SELECT count(id_hermes) FROM products WHERE comment LIKE concat(controll,\'%\') ';
			
			if ($producer_id)
			{
				$query .= 'AND producer_id = '.$producer_id;
			}
			
			$query .= ') as products_count FROM groups_tree';
			
			if (is_array($groups_list))
			{
				$query .= ' WHERE ';
				
				foreach ($groups_list as $group)
				{
					$query .= 'groups_tree.comment LIKE \'%'.(int)$group['id'].'%\' ';
		   			
		       		if ($groups_count > 1 && $group != end($groups_list))
		       		{
		       			$query .= 'OR ';
		       		}
				}
			}
	
			$query .= ' ORDER BY `order` ASC';
			
			if (is_array($groups_list))
			{
				$name = 'producer_'.$producer_id.'.'.date("Y-m-d", mktime(0,0,0,date("m"),date("d"),date("Y")));
				
				if (!file_exists(CACHE_DIR.$name.'.666'))
				{
					$name = 'producer_'.$producer_id.'.'.date("Y-m-d", mktime(0,0,0,date("m"),(date("d")-1),date("Y")));
				}
			}
			else
			{
				$name = 'tree.'.date("Y-m-d", mktime(0,0,0,date("m"),date("d"),date("Y")));
		
				if (!file_exists(CACHE_DIR.$name.'.666'))
				{
					$name = 'tree.'.date("Y-m-d", mktime(0,0,0,date("m"),(date("d")-1),date("Y")));
				}
			}
		}
		else
		{
			if ($producer_id > 0)
			{
				$name = 'individual_producer_'.$producer_id.'.'.date("Y-m-d", mktime(0,0,0,date("m"),date("d"),date("Y")));
			}
			else
			{
				$name = 'individual_tree.'.date("Y-m-d", mktime(0,0,0,date("m"),date("d"),date("Y")));
			}
		}
		
		$sql->sql_cache($name);
		$sql->sql_query($query);
		
		$xml .= '<query><![CDATA['.$query.']]></query>'."\n";

		while($sql->sql_fetch_array())
		{
			$branches[] = $sql->rows;
		}
			
		$sql->sql_cache();
		
		$products_sum = 0;
		
		$xml .= '<tree><![CDATA[';
		
		foreach ($branches as $i => $branch)
		{
			if ($branch['products_count'] > 0)
			{
				if ($branch['level'] == 1)
				{
					$products_sum += $branch['products_count'];
				}
				
				if ($branch['level'] > $branches[$i-1]['level'])
				{
					$xml .= '<ul '.(($i) ? 'id="list_'.$branch['parent_id'].'"' : '').' '.(($branch['level'] > 1) ? 'style="display:none;"' : '').'>';
				}
				
				if ($branches[$i-1]['level'] > $branch['level'])
				{
					$xml .= '</li>';
				}
				
				if (!($branch['level'] > 1 && $branch['products_count'] == 0 && $producer_id))
				{
				$xml .= '<li class="'.(($branches[$i+1]['level'] > $branch['level']) ? 'show' : 'last').'">';
				$xml .= '<a id="info_'.$branch['level'].'_'.$branch['id'].'" '.(($branch['id'] == $group_id) ? 'class="active"' : '').' href="./grupa_'.$validators->validateName($branch['name'], 2).','.$branch['id'].(($producer_id) ? ',1.'.$producer_id : '').'.html" onclick="javascript:return false;">';
				$xml .= '<img src="images/change_1.png" alt="" />'.$validators->strToLower($branch['name']).' <span>['.$branch['products_count'].']</span>';
				$xml .= '</a>';
				}
	
				if ($branch['level'] == $branches[$i+1]['level'])
				{
					if (!($branch['level'] > 1 && $branch['products_count'] == 0 && $producer_id))
					{
					$xml .= '</li>';
					}
				}
					
				if ($branch['level'] > $branches[$i+1]['level'])
				{
					$xml .= '</li></ul>';
					
					for ($j=1; $j<($branch['level'] - $branches[$i+1]['level']); $j++)
					{
						$xml .= '</li></ul>';
					}
				}
			}
		}
		
		$xml .= ']]></tree><sum>'.$products_sum.'</sum>';
		
		break;
		
	case 'search':
		
		$key = trim(mysql_escape_string($_POST['key']));
		
		$_SESSION['b2b_search_key'] = $key;
		
		if (!empty($key))
		{
			$data = explode(' ', $key);
			$search = array();
			$replace = array();
			
			if (true == $groups->checkUserType())
			{
				$query = 'SELECT producers.name as producer_name, product_versions.name, id_hermes FROM products_status JOIN products ON products_status.id = 
				products.id_hermes AND products_status.status = 1 AND products_status.user = '.(int)$_SESSION["b2b_user"]['id'].' JOIN product_versions ON 
				products.id_hermes = product_versions.product_id JOIN groups_tree ON products.subgroup_id = groups_tree.id JOIN producers ON 
				products.producer_id = producers.id WHERE (';
			}
			else
			{
				$query = 'SELECT producers.name as producer_name, product_versions.name, id_hermes FROM products JOIN product_versions ON products.id_hermes 
				= product_versions.product_id JOIN groups_tree ON products.subgroup_id = groups_tree.id JOIN producers ON products.producer_id = producers.id 
				WHERE (';
			}
			
			$query .= 'product_versions.name LIKE \'%'.$key.'%\' OR (';
			
			foreach ($data as $element)
			{
				array_push($search, $validators->strToLower($element));
				array_push($search, $validators->strToLower($element, 1));
				array_push($replace, '<b>'.$validators->strToLower($element).'</b>');
				array_push($replace, '<b>'.$validators->strToLower($element, 1).'</b>');
				
				$query .= 'product_versions.name LIKE \'%'.$element.'%\' AND ';
			}
			
			$query = substr($query, 0, -5);
			$query .= ') OR products.ean LIKE \'%'.$key.'%\' OR (';
			
			foreach ($data as $element)
			{
				$query .= 'products.ean LIKE \'%'.$element.'%\' AND ';
			}
			
			$query = substr($query, 0, -5);
			$query .= ') OR products.symbol LIKE \'%'.$key.'%\' OR (';
			
			foreach ($data as $element)
			{
				$query .= 'products.symbol LIKE \'%'.$element.'%\' AND ';
			}
			
			$query = substr($query, 0, -5);
			$query .= ')) AND subgroup_id > 0 LIMIT 20';
			
			$result = $db->query($query);
			
			if (mysql_num_rows($result))
			{
				$xml .= '<products>';
				
				while($row = mysql_fetch_array($result)) 
			   	{
			       	$xml .= '<product>';
			       		
			       		$xml .= '<name><![CDATA['.trim(str_replace($search, $replace, $validators->strToLower($row['name']))).']]></name>';
			       		$xml .= '<id><![CDATA['.$row['id_hermes'].']]></id>';
			       	$xml .= '</product>';
			   	}
		
				$xml .= '</products>';
			}
			
			if (true == $groups->checkUserType())
			{
				$query = 'SELECT DISTINCT groups_tree.id, groups_tree.name FROM products_status JOIN groups_tree ON products_status.subgroup = 
				groups_tree.comment AND products_status.status = 1 AND products_status.user = '.(int)$_SESSION["b2b_user"]['id'].' WHERE (';
			}
			else
			{
				$query = 'SELECT DISTINCT groups_tree.id, groups_tree.name FROM products RIGHT JOIN groups_tree ON products.subgroup_id = groups_tree.id 
				WHERE (';
			}
			
			$query .= 'groups_tree.name LIKE \'%'.$key.'%\' OR (';
			
			foreach ($data as $element)
			{
				$query .= 'groups_tree.name LIKE \'%'.$element.'%\' AND ';
			}
			
			$query = substr($query, 0, -5);
			$query .= ')) AND groups_tree.level = 2 LIMIT 5';
			
			mysql_free_result($result);
			$result = $db->query($query);
			
			if (mysql_num_rows($result))
			{
				$xml .= '<groups>';
				
				while($row = mysql_fetch_array($result)) 
			   	{
			       	$xml .= '<group>';
			       		$xml .= '<name><![CDATA['.trim(str_replace($search, $replace, $validators->strToLower($row['name']))).']]></name>';
			       		$xml .= '<id>'.(int)$row['id'].'</id>';
			       	$xml .= '</group>';
			   	}
		
				$xml .= '</groups>';
			}
		}
		
		break;
		
	case 'gallery':
		
		$product_id = (int)$_POST['product_id'];
		
		$result = $db->query('SELECT id FROM product_galleries WHERE product_id = '.$product_id.' ORDER BY `order` ASC') or $db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$xml .= '<images>';
			
			while($row = mysql_fetch_array($result)) 
			{
				$xml .= '<image>'.$row['id'].'</image>';
			}
			
			$xml .= '</images>';
		}
		
		mysql_free_result($result);
		
		$result = $db->query('SELECT name FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id WHERE version_id = 1 AND product_id = '.$product_id) or $db->raise_error();
		$row = mysql_fetch_array($result);
		
		$xml .= '<name><![CDATA['.$validators->strToLower(trim($row['name'])).']]></name>';
		
		break;
}

$xml .= '</response>';

echo gzencode($xml);
?>

function footerPosition()
{
	if (parseInt($("#content_center").height()) < parseInt($(window).height()))
	{
		$("#footer_content").css({"position":"absolute", "bottom":"0px", "left":"0px"});
	}
}

$(document).ready(function() {
	
	if (($.browser.mozilla && (parseFloat($.browser.version) < 1.9)) || ($.browser.msie && (parseFloat($.browser.version) < 7)) || ($.browser.opera && (parseFloat($.browser.version) < 9)))
	{
		var html = '<div style="margin:20px 0px 0px 0px; text-align:center;">';
		html += '<p>Wymagana jest nowsza wersja przegladarki niż ta, z której korzystasz.</p>';
		html += '<p style="margin:20px 0px 0px 0px; font-size:20px;"><a href="http://www.mozilla-europe.org/pl/firefox" style="color:#376cb0;">Mozilla Firefox</a></p>';
		html += '<p style="margin:10px 0px 0px 0px; font-size:20px;"><a href="http://www.opera.com/portal/choice/?language=pl" style="color:#376cb0;">Opera</a></p>';
		html += '<p style="margin:10px 0px 0px 0px; font-size:20px;"><a href="http://www.microsoft.com/poland/windows/internet-explorer" style="color:#376cb0;">Internet Explorer</a></p>';
		html += '</div>';
		
		$("#login_form").html(html);
	}
	
	footerPosition();
});
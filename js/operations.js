var bar_width = 0;
var cookie_products = $.cookie('products');
var amount = new Array(0, 0, 0, false);
var change_controll = false;

$.fn.hideOption = function() {
    this.each(function() {
        if ($(this).is('option') && (!$(this).parent().is('span'))) {
            $(this).wrap('<span>').hide();
        }
    });
};

$.fn.showOption = function() {
    this.each(function() {
        if (this.nodeName.toLowerCase() === 'option') {
            var p = $(this).parent(),
                o = this;
            $(o).show();
            $(p).replaceWith(o);
        } else {
            var opt = $('option', $(this));
            $(this).replaceWith(opt);
            opt.show();
        }
    });
};

function checkSelectOptions()
{
	/*
	$("form[name='paths'] select option, form[name='special_products'] select option").each(function(){
		
		if ($(this).hasClass("block"))
		{
			$(this).showOption();
		}
		else if ($(this).hasClass("none"))
		{
			$(this).hideOption();
		}
	});
	*/
}

$(document).ready(function() {
	
	$("span.image a").fancybox({'type' : 'image'});
	
	$("#send_order_button").click(function(){
		
		$("form[name='send_order']").submit();
		return false;
	});
	
	$("input.datepicker").datepicker();
	
	scrollGroups();
	productsAmountChange();
	
	if ($("input[name='last_operation']").val() != '' && $("input[name='last_operation']").val() != undefined)
	{
		goMain($("input[name='last_operation']").val());
	}
	
	jQuery(document).on("keyup", "table.proteges_list span.amount_input input", function(){
		
		var value = parseFloat(str_replace(',', '.', $(this).val()));
		var id = parseInt($(this).attr("alt").split("#")[0]);
		
		if (!isNaN(value) && value > 0 && !isNaN(id) && id > 0)
		{
			amount = new Array(parseInt($(this).attr("alt").split("#")[1]), id, value, true);
		}
	});
	
	$("table#communicats_list a.communicat_operation").click(function(){
		
		var operation = $(this).text();
		var id = parseInt($(this).attr("id").split("_")[2]);
		
		switch (operation)
		{
			case "rozwiń":
				$("table#communicats_list div.communicat_content").css("display", "none");
				$("table#communicats_list div.communicat_short").css("display", "block");
				$("table#communicats_list a.communicat_operation").text("rozwiń");
				$(this).text("zwiń");
				$("table#communicats_list div#short_"+id).css("display", "none");
				$("table#communicats_list div#content_"+id).css("display", "block");
				break;
				
			case "zwiń":
				$(this).text("rozwiń");
				$("table#communicats_list div#short_"+id).css("display", "block");
				$("table#communicats_list div#content_"+id).css("display", "none");
				break;
		}
		
		return false;
	});
	
	jQuery(document).on("click", "#add_to_order", function(){
				
		$.cookie('products', '', { expires: 1, path: '/'});
		$("form[name='to_basket']").submit(); 
		$("form[name='to_basket'] .product_count").val(1);
		$("form[name='to_basket'] .product_checkbox").attr("checked",false);
		return false;
	});
	
	jQuery(document).on("mouseover", ".legend", function(){
		
		var id = parseInt($(this).attr("id").split("_")[1]);
		$("#legend_details_"+id).css("display","block");
		
		if (id == 1)
		{
			var position = $(this).offset();
			$("#legend_details_"+id).css({"top":parseInt(position.top + 20)+"px", "left":parseInt(position.left)+"px"});
		}
	});
	
	jQuery(document).on("mouseleave", ".legend", function(){
		
		var id = parseInt($(this).attr("id").split("_")[1]);		
		$("#legend_details_"+id).css("display","none");
	});
	
	$("#page_content .main_group").click(function(){
		
		var level = parseInt($(this).attr("id").split("_")[1]);
		var group_id = parseInt($(this).attr("id").split("_")[2]);
		
		showGroup($("#groups a#info_"+level+"_"+group_id));
	});
	
	$("#page_content p.main_prev a").click(function(){
		
		var level = parseInt($(this).attr("id").split("_")[2]);
		var group_id = parseInt($(this).attr("id").split("_")[3]);
		
		if (level > 0)
		{
			showGroup($("#groups a#info_"+level+"_"+group_id));
		}
		else
		{
			$("#page_content .main_group").css("display", "none");
			$("#page_content .mail_level_1").css("display", "block");
		}
		
		return false;
	});
	
	/* percents */
	
	$("input[name='percent']").keyup(function(){
		
		var tax = parseFloat($("input[type='hidden'][name='tax']").val());
		var percent = ((isNaN(parseInt($(this).val()))) ? 0 : parseInt($(this).val()));
		var netto = parseFloat($("#user_netto strong").text());
		
		$("#customer_netto strong").text(round((netto + (percent*netto/100)), 2)+" zł");
		$("#customer_brutto strong").text(round(((netto + (percent*netto/100))*tax), 2)+" zł");

	});
	
	/* users */
	/*
	if ($("input[type='hidden'][name='login_date']").val() == '0000-00-00 00:00:00')
	{
		if (false == strstr(document.location.href, "/konto,edycja.html"))
		{
			if (confirm('Prosimy o weryfikację danych osobowych'))
			{
				document.location="./konto,edycja.html";
			}
		}
	}
	*/

	/* products */
	
	$('#search_results').mouseleave(function(){
		
		setTimeout(function(){
			$('#search_results').css("display","none");
		},  1000);
	});
	
	jQuery(document).on("click", "a#add_to_favorite", function(){
		$(".subgroup:last").ready(function() {
			
			$(this).find("input#product_operation").val('favorite_products');
			document.to_basket.submit(); 
			
		});
		
		return false;
	});
	
	jQuery(document).on("click", "a#add_to_basket", function(){
		$(".subgroup:last").ready(function() {
			
			document.to_basket.submit(); 
			
		});
		
		return false;
	});
	
	jQuery(document).on("click", "span#select_products", function(){
		$(".subgroup:last").ready(function() {
			$(this).find(".product_checkbox").attr("checked", true);
		});
	});
	
	jQuery(document).on("click", "span#unselect_products", function(){
		$(".subgroup:last").ready(function() {
			$(this).find(".product_checkbox").attr("checked", false);
		});
	});
	
	checkedProducts();
	
	jQuery(document).on("click", ".subgroup input[type='checkbox']", function(){
		
		if (cookie_products == null)
		{
			cookie_products = '';
		}
		
		var id = parseInt($(this).val());
		var value = parseInt($(".subgroup input[type='text'][id='count_"+id+"']").val());
			
		var cookie_table = cookie_products.split('##');
		var count = cookie_table.length;
				
		for (var i=1; i<count; i++)
		{
			if (cookie_table[i].split('#')[1] == id)
			{
				cookie_products = str_replace('##'+cookie_table[i], '', cookie_products);
			}
		}
		
		if ($(this).attr("checked") == true)
		{
			cookie_products += '##'+value+'#'+id;
		}
			
		$.cookie('products', cookie_products, { expires: 1, path: '/'});
	});
	
	jQuery(document).on("keyup", ".subgroup input[type='text']", function(){
		
		if (cookie_products == null)
		{
			cookie_products = '';
		}
			
		var id = parseInt($(this).attr("id").split("_")[1]);
		var value = parseInt($(this).val());
		
		var cookie_table = cookie_products.split('##');
		var count = cookie_table.length;
		
		for (var i=1; i<count; i++)
		{
			if (cookie_table[i].split('#')[1] == id)
			{
				cookie_products = str_replace('##'+cookie_table[i], '', cookie_products);
			}
		}
		
		$(".subgroup:last").ready(function() {
			if (value > 0)
			{
				$(this).find("input[id='active_"+id+"']").attr("checked", true);
				cookie_products += '##'+value+'#'+id;
			}
			else
			{
				$(this).find("input[id='active_"+id+"']").attr("checked", false);
			}
		});
		
		$.cookie('products', cookie_products, { expires: 1, path: '/'});
	});
	
	/* groups list height */
	
	
	contentHeight();
	
	$(window).resize(function(){
		
		contentHeight();
	});
	
	/* marquee element */
	
	$('#horizontal_list span').each(function(){
		
		bar_width += parseInt($(this).width()) + 18;
	});
	
	$("div#horizontal_list p").css({"width":bar_width+"px"});
	$("div#horizontal_list marquee").css("width", $("div#horizontal_list").width());
    
	$('div#horizontal_list marquee').marquee('pointer').mouseover(function () {
        $(this).trigger('stop');
    }).mouseout(function () {
        $(this).trigger('start');
    }).mousemove(function (event) {
        if ($(this).data('drag') == true) 
        {
            this.scrollLeft = $(this).data('scrollX') + ($(this).data('x') - event.clientX);
        }
    }).mousedown(function (event) {
        $(this).data('drag', true).data('x', event.clientX).data('scrollX', this.scrollLeft);
    }).mouseup(function () {
        $(this).data('drag', false);
    });
	
	/* horizontal menu */
	
	$('#menu_content li').mouseover(function(){
		  
		if ($(this).attr("id") != undefined && $(this).attr("id") != "")
		{
			var id = $(this).attr("id").split('_');
			var top = 127;
			var left = 0;
			
			if (id[0] == 'menu')
			{
				if (window_width < 1240)
				{
					switch (parseInt(id[1]))
					{
						case 1: left = 100; break;
						case 2: left = 288; break;
					}
				}
				else
				{
					switch (parseInt(id[1]))
					{
						case 1: left = 112; break;
						case 2: left = 322; break;
					}
				}
			}
			
			$("ul#submenu_"+id[1]).css({"display":"block", "top":top+"px", "left":left+"px"});
		}
	});

	$('#menu_content li').mouseleave(function(){
		
		if ($(this).attr("id") != undefined && $(this).attr("id") != "")
		{
			var id = $(this).attr("id").split('_');
			
			if (id[0] == 'menu')
			{
				$("ul#submenu_"+id[1]).css({"display":"none"});
			}
		}
	});
	
	// add comment
	$("#add_comment").click(function(){
			
		$(".comment").css("display", "block");
		$(".ask_about").css("display", "none");
		
		return false;
	});
		
	// ask about product
	$("#product_menu p").click(function(){

		switch ($(this).attr("id"))
		{
			case 'description_box': 
				$("#question_content, #comments_content, #newses_content, #frequently_purchased_content").css("display","none");
				$("#product_left_column, #descritption_content").css("display","block");
				break;
			case 'comments_box': 
				$("#question_content, #product_left_column, #descritption_content, #newses_content, #frequently_purchased_content").css("display","none");
				$("#comments_content").css("display","block");
				break;
			case 'ask_about_box':
				$("#comments_content, #product_left_column, #descritption_content, #newses_content, #frequently_purchased_content").css("display","none");
				$("#question_content").css("display","block");
				break;
			case 'newses_box':
				$("#comments_content, #product_left_column, #descritption_content, #question_content, #frequently_purchased_content").css("display","none");
				$("#newses_content").css("display","block");
				break;
			case 'frequently_purchased_box':
				$("#comments_content, #product_left_column, #descritption_content, #question_content, #newses_content").css("display","none");
				$("#frequently_purchased_content").css("display","block");
				break;
		}
		
		$("#product_menu p").removeClass("active");
		$(this).addClass("active");
	});
	
	if ($("input[name='product_operation']").val() != '' && $("input[name='product_operation']").val() != undefined)
	{
		$("#product_menu p").removeClass("active");
		
		switch ($("input[name='product_operation']").val())
		{
			case 'add_comment':
				$("#question_content, #product_left_column, #descritption_content, #newses_content, #frequently_purchased_content").css("display","none");
				$("#comments_content").css("display","block");
				$("p#comments_box").addClass("active");
				break;
			case 'ask_about':
				$("#comments_content, #product_left_column, #descritption_content, #newses_content, #frequently_purchased_content").css("display","none");
				$("#question_content").css("display","block");
				$("p#ask_about_box").addClass("active");
				break;
		}
	}
	
	// payment
	$("#order_form input[name='payment']").change(function(){
			
		var id = $(this).val();
		$(".payment").css('display', 'none');
		$("#payment_"+id).css('display', 'block');	
	});
	
	// archive
	jQuery(document).on("click", ".see_details", function(){
    		
    	var id = $(this).attr('id').split('_')[1];
    		
    	$(".order_details").hide();
    	$("#details_"+id).show();
    		
    	return false;	
    });
	
	// special orders
	$("#add_special_order").click(function(){
		
		var element = '<tr class="special_product">'+$("tr.special_product").html()+'</tr>';
		var unit_select = '<select name="units[]"><option value="0">-- wybierz --</option><option value="1">kilogram</option><option value="2">komplet</option><option value="3">m2</option>' + 
		'<option value="4">m3</option><option value="5">mb</option><option value="6">opakowanie</option><option value="7">para</option><option value="8">sztuka</option></select>';
		var producer_input = '<input type="text" name="producers[]" value="" />';
		
		$("tr.special_product").parent().append(element);
		$("tr.special_product:last input").val("");
		$("tr.special_product:last input[type='hidden'][name='units[]']").parent().html(unit_select);
		$("tr.special_product:last input[type='hidden'][name='producers[]']").parent().html(producer_input);
		
		return false;
	});

	// input autocomplete
	
	$("#login_content input[name='login']").click(function(){	
		
		if ($(this).val() == 'Login') $(this).val('');
	});
	
	$("#login_content input[name='login']").blur(function(){	
		
		if ($(this).val() == '') $(this).val('Login');
	});
	
	$("#login_content input[name='password']").click(function(){	
		
		if ($(this).val() == 'Hasło') $(this).val('');
	});
	
	$("#login_content input[name='password']").blur(function(){	
		
		if ($(this).val() == '') $(this).val('Hasło');
	});
	
	/* favorite */
	
	$("a#select_all_favorites").click(function(){
		
		if ($("#product_menu #favorite_box.active").length)
		{
			$("#product_border form[name='to_basket'] input[type='checkbox'][name='actives[]']").attr("checked", true);
		}
		return false;
	});	
	
	
	jQuery(document).on("click", "a.accept_order, a.reject_order", function(){
		
		var id = parseInt($(this).attr("alt"));
		
		if ($(this).hasClass("accept_order"))
		{
			$("form[name='query_operations_"+id+"'] input[name='query_operation']").val('accept');
		}
		else if ($(this).hasClass("reject_order"))
		{
			$("form[name='query_operations_"+id+"'] input[name='query_operation']").val('reject');
		}
		
		$("form[name='query_operations_"+id+"']").submit();
		return false;
	});
	
	$("a.next_order_step").click(function(){
		
		$("#during_the_verification").css("display","block");
		$("form[name='paths']").submit();
		return false;
	});
	
	$("#reject_order_form span.close").click(function(){
		$("#reject_order_form").css("display","none");
	});
	
	jQuery(document).on("click", "table.proteges_list a.reject_order", function(){
		
		var id = parseInt($(this).attr("alt"));
		
		$("#reject_order_form input[type='hidden'][name='id']").val(id);
		$("#reject_order_form").css("display","block");
		return false;
	});
	
	$("table#permissions_table input[type='checkbox']").change(function(){
		
		if ($(this).parent().hasClass("first"))
		{
			if (true == $(this).attr("checked"))
			{
				$(this).parent().parent().find("input[type='checkbox']").attr("checked", true);
			}
			else
			{
				$(this).parent().parent().find("input[type='checkbox']").attr("checked", false);
			}
		}
		else
		{
			if (true == $(this).attr("checked"))
			{
				$(this).parent().parent().find("td.first input[type='checkbox']").attr("checked", true);
			}
			else
			{
				var id = parseInt($(this).parent().parent().find("td.first input[type='checkbox']").val());
				
				if ($("input[type='checkbox'][name='contractors["+id+"]']").attr("checked") == false && $("input[type='checkbox'][name='analysts["+id+"]']").attr("checked") == false && 
				$("input[type='checkbox'][name='managers["+id+"]']").attr("checked") == false && $("input[type='checkbox'][name='receivings["+id+"]']").attr("checked") == false)
				{
					$(this).parent().parent().find("td.first input[type='checkbox']").attr("checked", false);
				}
			}
		}
	});
	
	checkSelectOptions();
	jQuery(document).on("change", "form[name='paths'] select[name='budget[]'], form[name='special_products'] select[name='budget[]']", function(){

		var id = parseInt($(this).val());
		var key = parseInt($(this).attr("id").split("_")[1]);
		/*
		$(this).parent().parent().find("select[name='analyst[]'] option").hideOption();
	
		$(this).parent().parent().find("select[name='analyst[]'] option[value='0']").showOption();
		$(this).parent().parent().find("select[name='analyst[]'] option[alt='"+id+"']").showOption();
			
		$(this).parent().parent().find("select[name='analyst[]'] option:selected").each(function(){
				
			if (parseInt($(this).attr("alt")) != id)
			{
				$(this).attr("selected", false);
			}
		});
			
		$(this).parent().parent().find("select[name='manager[]'] option").hideOption();
		$(this).parent().parent().find("select[name='manager[]'] option[value='0']").showOption();
		$(this).parent().parent().find("select[name='manager[]'] option[alt='"+id+"']").showOption();
			
		$(this).parent().parent().find("select[name='manager[]'] option:selected").each(function(){
				
			if (parseInt($(this).attr("alt")) != id)
			{
				$(this).attr("selected", false);
			}
		});
			
		$(this).parent().parent().find("select[name='receiving[]'] option").hideOption();
		$(this).parent().parent().find("select[name='receiving[]'] option[value='0']").showOption();
		$(this).parent().parent().find("select[name='receiving[]'] option[alt='"+id+"']").showOption();
		
		$(this).parent().parent().find("select[name='receiving[]'] option:selected").each(function(){
				
			if (parseInt($(this).attr("alt")) != id)
			{
				$(this).attr("selected", false);
			}
		});
		*/
		
		$.ajax({
			type: "POST",
			url: "js/xml.php?operation=get_budget_users",
			data: {id:id, operation:"get_budget_users"},
			dataType: "xml",
					  
			success: function(xml) 
			{
				$("select[name='budget[]'][id='row_"+key+"'] option[value='"+id+"']:selected").parent().parent().parent().find("select[name='analyst[]']").html("<option value='0'>-- wybierz --</option>");
				$("select[name='budget[]'][id='row_"+key+"'] option[value='"+id+"']:selected").parent().parent().parent().find("select[name='manager[]']").html("<option value='0'>-- wybierz --</option>");
				$("select[name='budget[]'][id='row_"+key+"'] option[value='"+id+"']:selected").parent().parent().parent().find("select[name='receiving[]']").html("<option value='0'>-- wybierz --</option>");
				
				if ($(xml).find("analist").length)
				{
					$(xml).find("analist").each(function(){
					$("select[name='budget[]'][id='row_"+key+"'] option[value='"+id+"']:selected").parent().parent().parent().find("select[name='analyst[]']").append("<option value='"+$(this).find("id").text()+"'>"+$(this).find("login").text()+"</option>");
					});
				}
					
				if ($(xml).find("manager").length)
				{
					$(xml).find("manager").each(function(){
					$("select[name='budget[]'][id='row_"+key+"'] option[value='"+id+"']:selected").parent().parent().parent().find("select[name='manager[]']").append("<option value='"+$(this).find("id").text()+"'>"+$(this).find("login").text()+"</option>");
					});
				}
					
				if ($(xml).find("receiving").length)
				{
					$(xml).find("receiving").each(function(){
					$("select[name='budget[]'][id='row_"+key+"'] option[value='"+id+"']:selected").parent().parent().parent().find("select[name='receiving[]']").append("<option value='"+$(this).find("id").text()+"'>"+$(this).find("login").text()+"</option>");
					});
				}
			}
		});
	});
	
	$("a#user_form_submit").click(function(){
		$("form[name='user']").submit();
		return false;
	});
	
	$("a#application_form_submit").click(function(){
		$("form[name='application']").submit();
		return false;
	});
	
	jQuery(document).on("click", "a#reject_form_submit", function(){

		$("form[name='reject_order']").submit();
		return false;
	});
	
	$("a#contact_form_submit").click(function(){
		$("form[name='contact']").submit();
		return false;
	});
	
	$("a#account_form_submit").click(function(){
		$("form[name='account']").submit();
		return false;
	});
	
	jQuery(document).on("click", "a#search_to_basket_form_submit", function(){
		$('input#product_operation').val('favorite_products');
		$("form[name='to_basket']").submit();
		return false;
	});
	
	$("a#back_special_order").click(function(){
		
		$("a#special_products_form_submit").text("USTAL ŚCIEŻKI");
		$("#back_special_order").parent().css("display","none");
		$("div#query_product_list").css("display","block");
		$("div#query_mpk_list").css("display","none");
		return false;
		
	});
	
	$("a#delete_favorite_form_submit").click(function(){
		$("form[name='to_basket'] input[name='operation']").val("delete_favorite");
		$("form[name='to_basket']").submit();
		return false;
	});
	
	$("a#to_basket_favorite_form_submit").click(function(){
		$("form[name='to_basket'] input[name='operation']").val("to_basket");
		$("form[name='to_basket']").submit();
		return false;
	});
	
	jQuery(document).on("change", "input#visibility_form_submit", function(){
		
		$("#products_content td.td_availability").parent().css("display","table-row");
		
		if ($(this).attr("checked") == "checked")
		{
			$("#products_content td.td_availability").parent().css("display","none");
			$("#products_content img[src='images/mag/magaznyn_ziel2.png']").parent().parent().parent().css("display","table-row");
		}
	});

	jQuery(document).on("click", "select#sort_form_submit", function(){
		
		if (true == change_controll)
		{
			change_controll = false;
			$("form[name='sort']").submit();
		}
		else
		{
			change_controll = true;
		}

		return false;
	});
	
	$("a#ask_about_form_submit").click(function(){
		$("form[name='ask_about']").submit();
		return false;
	});
	
	$("a#add_comment_form_submit").click(function(){
		$("form[name='add_comment']").submit();
		return false;
	});
	
	$("a#product_to_basket_form_submit").click(function(){
	
		$("form[name='to_basket']").submit();
		window.opener.location.reload();
		return false;
	});
	
	$("a.filter_form_submit").click(function(){
		$("form[name='export']").attr("action", "./zestawienia.html");
		$("form[name='export']").submit();
		return false;
	});
	
	$("a.export_form_columns").click(function(){
		$("form[name='export']").attr("action", "./zestawienia,eksport.html");
		$("#export_columns").css("display","block");
		return false;
	});
	
	$("a.export_form_submit").click(function(){
		$("#export_columns").css("display","none");
		$("form[name='export']").submit();
		return false;
	});
	
	$("a.savings_report").click(function(){
		$("form[name='export']").attr("action", "./zestawienia,oszczednosci.html");
		$("#savings_year").css("display","block");
		return false;
	});
	
	$("a.savings_form_submit").click(function(){
		$("#savings_year").css("display","none");
		$("form[name='export']").submit();
		return false;
	});
	
	$("table.export th img").click(function(){
		
		var element = $(this).parent();

		$(element).find("img:first").attr("src", "images/arrow_asc_hidden.png");
		$(element).find("img:last").attr("src", "images/arrow_desc_hidden.png");
		
		if ($(this).hasClass("asc"))
		{
			$(this).attr("src", "images/arrow_asc.png");
		}
		else
		{
			$(this).attr("src", "images/arrow_desc.png");
		}
		
		$("input[type='hidden'][name='order["+$(this).parent().attr("alt")+"]']").val($(this).attr("class"));
	});
	
	$(".proteges_list .copy_mpk").click(function(){
		
		var value = $(this).parent().find("input").val();
		var i = parseInt($(this).parent().parent().attr("alt"));
		
		if (value.length > 1)
		{	
			$.ajax({
				
				type: "POST",
				url: "js/xml.php?operation=set_mpk",
				data: {value:value, key:(i+1), operation:'set_mpk'},
				dataType: "xml",
			  
				success: function(xml) 
				{
				}
			});
		}
		
		$(".proteges_list tr[alt='"+(i+1)+"'] span").html('<input class="mpk" type="text" value="'+value+'" name="mpk['+(i+1)+']" alt="'+(i+1)+'">');
	});
	
	$("#budget_info").click(function(){
		return false;
	});
	
	$("#budget_info").mouseover(function(){
		
		$("#budget_box").css({
			"display":"block", 
			"top":($("#budget_info").offset().top + 19)+"px"
		});
	});
	
	$("#budget_info").mouseout(function(){
		$("#budget_box").css("display","none");
	});
	
	$("#user_functions").mouseover(function(){
		
		if ($("#user_functions_details").length)
		{
			$("#user_functions_details").css("display","block");
		}
	});
	
	$("#user_functions").mouseout(function(){
		
		if ($("#user_functions_details").length)
		{
			$("#user_functions_details").css("display","none");
		}
	});
	
	$("#protege_informations img.close").click(function(){
		
		$("#protege_informations").css("display","none");
		return false;
	});
	
	jQuery(document).on("click", "#range_1 a", function(){
		
		var data = $(this).attr("id").split('_');
		var level = parseInt(data[1]);
		var group_id = parseInt(data[2]);

		if ($("#details_"+group_id).css("display") == "block")
		{
			$("#details_"+group_id).css("display","none");
			$(this).parent().removeClass("hide").addClass("show");
		}
		else
		{
			$("#details_"+group_id).css("display","block");
			$(this).parent().removeClass("show").addClass("hide");
		}
		
		return false;
	});
	
	if ($("#individual_products_content").length)
	{
		if ($("#individual_products_content").css("display") == "block")
		{
			jQuery(document).on("click", "#page_header, #left_column, #top_content, #menu_content, #horizontal_list", function(){
				 	
				if (!confirm('Pamiętaj o zapisanu zmian. Czy zapisałęś zamiany ?'))
				{
					return false;
				} 
			});
		 }
	}
	
	$("#show_adjustment_amount").click(function(){
		$("#adjustment_amount").css("display","block");
	});
	
	jQuery(document).on("click", "#left_column .box.none", function(){
		
		$(this).removeClass("none");
		return false;
	});
	
	
	if ($("#page_content table.subgroup").length)
	{
		$("#page td.td_product").css("width", $("#page td.td_product:first").width()+"px");
		$("#page th.td_product").css("width", ($("#page td.td_product:first").width() + 20)+"px");
		$("#page td.td_symbol").css("width", $("#page td.td_symbol:first").width()+"px");
		$("#page th.td_symbol").css("width", ($("#page td.td_symbol:first").width() + 20)+"px");
	}
});
<?php
if ((int)$_SESSION["wsk_order_change"] > 0 || (int)$_SESSION["wsk_special_order_change"] > 0) 
{ 	
	$product_id = (((int)$_SESSION["wsk_order_change"] > 0) ? (int)$_SESSION["wsk_order_change"] : (int)$_SESSION["wsk_special_order_change"]);
	
	$product_info = $products->getProduct($product_id);
	
	?>
	<div id="<?php echo (((int)$_SESSION["wsk_order_change"] > 0) ? 'wsk_order_change' : 'wsk_special_order_change'); ?>" class="order_change">
		<p><strong>Dodano produkt do <?php echo (((int)$_SESSION["wsk_order_change"] > 0) ? 'zamówienia' : 'zapytania'); ?>:</strong></p>
		<div class="image"><img border="0" alt="" src="<?php echo BASE_ADDRESS; ?>include/image.php?id=<?php echo $product_id; ?>&amp;w=38&amp;h=38"></div>
		<div class="info">
			<p><?php echo $product_info['name']; ?></p>
			<p class="button"><a href="<?php echo BASE_ADDRESS.(((int)$_SESSION["wsk_order_change"] > 0) ? 'koszyk' : 'zapytania,nowe'); ?>.html">SZCZEGÓŁY</a></p>
		</div>
		<div class="clear"></div>
	</div>
	<?php
}

$_SESSION["wsk_special_order_change"] = 0;
$_SESSION["wsk_order_change"] = 0;
?>
<?php
class SpecialOrders extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_order_id;
	private $_prices = array();
	private $_taxes = array();
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
	}
	
	public function deleteOrder($order_id)
	{
		$this->_db->query('DELETE FROM special_order_details WHERE order_id = '.$order_id) or $this->_db->raise_error();
		$this->_db->query('DELETE FROM special_orders WHERE id = '.$order_id) or $this->_db->raise_error();
		
		$communicats['ok'] = 'Zamówienie zostało usunięte';
		$this->setSession("communicats", $communicats);
	}
	
	public function getSpecialOrders()
	{
		$result = $this->_db->query('SELECT special_orders.id, users.name as user_name, special_orders.add_date FROM special_orders JOIN users ON 
		special_orders.user_id = users.id ORDER BY special_orders.add_date') or $this->_db->raise_error();
		
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getSpecialOrder($order_id)
	{
		$result = $this->_db->query('SELECT name, symbol, special_orders.comment as order_comment, special_order_details.comment, amount, netto_price, tax FROM special_orders JOIN special_order_details ON special_orders.id = special_order_details.order_id 
		WHERE special_orders.id = '.$order_id) or $this->_db->raise_error();
		
		while($row = mysql_fetch_array($result)) 
   		{
       		$values["names"][] = $row['name'];
       		$values["symbols"][] = $row['symbol'];
       		$values["comments"][] = $row['comment'];
       		$values["amounts"][] = (float)$row['amount'];
       		$values["prices"][] = $row['netto_price'];
       		$values["taxes"][] = $row['tax'];
       		
       		$values['order_comment'] = $row['order_comment'];
   		}
   		
   		$values['products_count'] = count($values["names"]);
   		
   		$this->setSession("values", $values);
	}
	
	public function validateOrder()
	{
		$this->clear();
		
		$this->_order_id = (int)$_POST['order_id'];
		$this->_prices = $_POST["prices"];
		$this->_taxes = $_POST["taxes"];
		
		foreach ($this->_prices as $i => $price)
		{
			$values["prices"][] = sprintf("%01.2f", str_replace(',', '.', $this->_prices[$i]));
			$values["taxes"][] = (int)$this->_taxes[$i];
			
			if (!$this->validatePrice($this->_prices[$i]) || (float)str_replace(',', '.', $this->_prices[$i]) <= 0 || (int)$this->_taxes[$i] <= 0 || (int)$this->_taxes[$i] > 99)
			{
				$this->_errors++;
				$errors['order'] = 'Wartości są niepoprawne';
			}
		}
		
		$values['products_count'] = count($this->_prices);
		
		if ($this->_errors)
		{
			$result = $this->_db->query('SELECT name, symbol, special_order_details.comment, amount FROM special_orders JOIN special_order_details ON special_orders.id = special_order_details.order_id 
			WHERE special_orders.id = '.$this->_order_id) or $this->_db->raise_error();
			
			while($row = mysql_fetch_array($result)) 
	   		{
	       		$values["names"][] = $row['name'];
	       		$values["symbols"][] = $row['symbol'];
	       		$values["comments"][] = $row['comment'];
	       		$values["amounts"][] = $row['amount'];
	   		}
   		
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
		else
		{
			$this->_saveChanges();
		}
	}
	
	private function _saveChanges()
	{
		$result = $this->_db->query('SELECT name, symbol, special_order_details.comment, amount FROM special_orders JOIN special_order_details ON special_orders.id = special_order_details.order_id 
		WHERE special_orders.id = '.$this->_order_id) or $this->_db->raise_error();
		
		$products = $this->_db->mysql_fetch_all($result);
		
		foreach ($products as $i => $product)
		{
			$netto_price =  str_replace(',', '.', $this->_prices[$i]);
			
			$this->_db->query('UPDATE special_order_details SET netto_price = \''.$netto_price.'\', tax = '.(int)$this->_taxes[$i].', brutto_price = 
			\''.($netto_price + ($netto_price*(int)$this->_taxes[$i]/100)).'\' WHERE order_id = '.$this->_order_id.' AND name = \''.$product['name'].'\' 
			AND symbol = \''.$product['symbol'].'\' AND comment = \''.$product['comment'].'\' AND amount = \''.$product['amount'].'\'') or $this->_db->raise_error();
		}
		
		$communicats['ok'] = 'Zamówienie specjalne zostało wyedytowane';
		$this->setSession("communicats", $communicats);
		header("Location: ../../special_orders/list");
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}
?>
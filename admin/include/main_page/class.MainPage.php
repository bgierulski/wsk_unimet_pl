<?php
class MainPage extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_group_id;
	private $_image;
	private $_name;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
	}
	
	public function deleteGroup($group_id)
	{
		$this->deleteImage($group_id);
		
		$this->_db->query('DELETE FROM main_groups WHERE id = '.$group_id) or $this->_db->raise_error();
	}
	
	public function getMainGroups()
	{
		$result = $this->_db->query('SELECT main_groups.id, groups_tree.name, groups_tree.level FROM main_groups JOIN groups_tree ON main_groups.comment = groups_tree.comment ORDER BY `order` ASC') or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getGroups()
	{
		$result = $this->_db->query('SELECT id, name, level FROM groups_tree WHERE level < 4 AND comment NOT IN (SELECT comment FROM main_groups) ORDER BY `order` ASC') or $this->_db->raise_error();
		return $result;
	}
	
	public function getGroup($group_id)
	{
		$result = $this->_db->query('SELECT main_groups.id, name FROM main_groups JOIN groups_tree ON main_groups.comment = groups_tree.comment WHERE main_groups.id = '.$group_id) or $this->_db->raise_error();
		$row = mysql_fetch_array($result);
		
		$values['id'] = $row['id'];
		$values['name'] = $row['name'];
		$values['image'] = $this->getImage($group_id);
		
		$this->setSession("values", $values);
	}
	
	public function validateGroup()
	{
		$this->clear();
		
		$this->_group_id = $values['group_id'] = (int)$_POST['group_id'];
		$values['name'] = $_POST['name'];
		$this->_image = $_FILES["image"];
		
		if (!$this->_group_id)
		{
			$this->_errors++;
			$errors['group_id'] = 'Grupa jest wymagana';
		}
		
		if ($this->_image['name'] == '' && isset($_POST['add_group']))
		{
			$this->_errors++;
			$errors['image'] = 'Zdjęcie grupy jest wymagane';
		}
		else if (!$this->checkImage($this->_image) && $this->_image['name'] != '')
		{
			$this->_errors++;
			$errors['image'] = 'Oczekiwane rozszerzenia pliku to: gif, jpg, png';
		}
		
		if (!$this->_errors)
		{
			if (isset($_POST['add_group']))
			{
				$this->_addGroup();
			}
			else 
			{
				$this->_saveChanges();
			}
		}
		else 
		{
			$values['image'] = $this->getImage($this->_group_id);
			
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
	}
	
	private function _addGroup()
	{		
		$result = $this->_db->query('INSERT INTO main_groups VALUES (\'\', \'\', (SELECT comment FROM groups_tree WHERE id = '.$this->_group_id.'))') or $this->_db->raise_error();
	
		$result = $this->_db->query('SELECT IFNULL(MAX(id),0) AS new FROM main_groups') or $this->_db->raise_error();
		$row = mysql_fetch_array($result);
		
		$this->_saveImage($row['new']);
		
		$communicats['ok'] = 'Grupa została dodana do strony głównej';
		$this->setSession("communicats", $communicats);
		header("Location: ../main_page/list");
	}
	
	private function _saveChanges()
	{
		if ($this->_image['name'] != '')
		{
			$this->deleteImage($this->_group_id);
			$this->_saveImage($this->_group_id);
		}
		
		$communicats['ok'] = 'Informacje o grupie zostały wyedytowane';
		$this->setSession("communicats", $communicats);
		header("Location: ../../main_page/list");
	}
	
	public function getImage($group_id)
	{
		$result = $this->_db->query('SELECT image FROM main_groups WHERE id = '.$group_id) or $this->_db->raise_error();
		
		$row = mysql_fetch_array($result);
		return $row['image'];
	}
	
	public function deleteImage($group_id)
	{
		$image = $this->getImage($group_id);
		$path = BASE_DIR.'../../images/groups/'.$image;
		
		if (file_exists($path))
		{
			@unlink($path);
		}
		
		$this->_db->query('UPDATE main_groups SET image = \'\' WHERE id  = '.$group_id);
	}
	
	private function _saveImage($group_id)
	{
		$image = new ImageUpload();
		
		$symbols = '1234567890qwertyuiopasdfghjkklzxcvbnm';
   		$name = '';

		for ($i=0; $i<10; $i++)
		{
			$name .= $symbols[rand()%(strlen($symbols))];
		}
		
		$image->getExtension($this->_image['name']);
		
		$image->uploadImage(105, 105, $name, BASE_DIR.'../../images/groups/', $this->_image, 0);
		$image->deleteImage($name, 1);
			
		$this->_db->query('UPDATE main_groups SET image = \''.$name.'.'.$image->extension.'\' WHERE id = '.$group_id) or $this->_db->raise_error();
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}
?>
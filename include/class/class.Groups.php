<?php
class Groups extends Validators 
{
	private $_db;
	private $_tree;
	private $_user;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		$this->_user = $this->getSession('b2b_user');
	}
	
	public function checkUserType()
	{
		if (true === $this->getSession("b2b_full_offer"))
		{
			return false;
		}

		$result = $this->_db->query('SELECT individual, what_with_new FROM groups_tree_user WHERE user_id = '.(int)$this->_user['parent_id']) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			mysql_free_result($result);
			
			if ($row['individual'] == 1)
			{
				return true;
			}
		}
		
		return false;
	}
	
	public function getMainGroups()
	{
		$query = 'SELECT groups_tree.id, groups_tree.name, groups_tree.level, main_groups.image, 1 as amount FROM main_groups JOIN groups_tree ON main_groups.comment = 
		groups_tree.comment ORDER BY `order` ASC';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $result;
	}
	
	public function getPictograms($group_id)
	{
		$result = $this->_db->query('SELECT `order`, parent_id FROM groups_tree WHERE id = '.$group_id) or $this->_db->raise_error();

		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			mysql_free_result($result);
			
			if (true == $this->checkUserType())
			{
				$query = 'SELECT groups_tree.id, groups_tree.name, groups_tree.level, main_groups.image FROM groups_tree_status JOIN groups_tree ON 
				groups_tree_status.comment = groups_tree.comment JOIN main_groups ON groups_tree_status.comment = main_groups.comment WHERE 
				groups_tree.comment IN (SELECT comment FROM groups_tree WHERE parent_id = \''.$row['order'].'\' AND `order` != parent_id) AND 
				groups_tree_status.status = 1 AND groups_tree_status.user = '.$this->_user['parent_id'].' ORDER BY groups_tree.order ASC';
			}
			else
			{
				$query = 'SELECT groups_tree.id, groups_tree.level FROM main_groups JOIN groups_tree ON main_groups.comment = groups_tree.comment WHERE groups_tree.comment IN (SELECT comment FROM groups_tree WHERE parent_id = \''.$row['order'].'\' AND `order` != parent_id)';
			}
			
			$result = $this->_db->query($query) or $this->_db->raise_error();
		
			if (mysql_num_rows($result))
			{
				$list = $this->_db->mysql_fetch_all($result);
				mysql_free_result($result);
				
				$result = $this->_db->query('SELECT id FROM groups_tree WHERE `order` = \''.$row['parent_id'].'\'') or $this->_db->raise_error();
				$row = mysql_fetch_array($result);
				
				return array($row['id'], $list);
			}
		}
		
		return false;
	}
	
	public function getParents($group_id)
	{
		if (true == $this->checkUserType())
		{
			$result = $this->_db->query('SELECT groups_tree.id FROM groups_tree_status JOIN groups_tree ON groups_tree_status.comment = groups_tree.comment 
			WHERE status = 1 AND user = '.$this->_user['parent_id'].' AND groups_tree.id = '.$group_id) or $this->_db->raise_error();
			
			if (!mysql_num_rows($result))
			{
				$group_id = $this->checkGroup(0, 0);
			}
		}
		
		$result = $this->_db->query('SELECT id FROM groups_tree WHERE `order` = (SELECT parent_id FROM groups_tree WHERE id = '.$group_id.')') or $this->_db->raise_error();
		$result = $this->_db->query('SELECT parent_id, name, `order` FROM groups_tree WHERE id = '.$group_id) or $this->_db->raise_error();
		$group = mysql_fetch_array($result);
		
		$tree = array($group_id);
		$this->checkTree($tree, array($group['parent_id']));

		return $this->_tree;
	}
	
	public function checkUser($producer_id = 0)
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT groups_tree.id, groups_tree.level, groups_tree.name, groups_tree.order, groups_tree.parent_id as parent, (SELECT id FROM 
			groups_tree WHERE `order` = parent) as parent_id, groups_tree.comment, (SELECT count(products_status.id) FROM products_status JOIN 
			producers_status ON products_status.producers = producers_status.name AND products_status.user = producers_status.user AND 
			products_status.status = producers_status.status ';
				
			if ($producer_id > 0)
			{
				$query .= 'JOIN producers ON producers_status.name = producers.name AND producers.id = '.$producer_id;
			}
				
			$query .= ' WHERE subgroup LIKE concat(groups_tree.comment,\'%\') AND products_status.user = '.$this->_user['parent_id'].' AND products_status.status = 1) as 
			products_count FROM groups_tree_status JOIN groups_tree ON groups_tree_status.comment = groups_tree.comment WHERE groups_tree_status.user 
			= '.$this->_user['parent_id'].' AND groups_tree_status.status = 1 ORDER BY groups_tree.order ASC';

			return $query;
		}
		
		return false;
	}
	
	public function checkTree($tree, $parents)
	{
		$parent_id = array_pop($parents);
		$result = $this->_db->query('SELECT id FROM groups_tree WHERE `order` = '.$parent_id) or $this->_db->raise_error();
		
		$group = mysql_fetch_array($result);
		mysql_free_result($result);
		
		$result = $this->_db->query('SELECT  parent_id, name, `order` FROM groups_tree WHERE id = '.$group['id']) or $this->_db->raise_error();
		$parent = mysql_fetch_array($result);
		
		if (!in_array((int)$group['id'], $tree))
		{
			array_push($tree, (int)$group['id']);
		}
		
		if ($parent['parent_id'] != $parent['order'])
		{
			array_push($parents, $parent['parent_id']);
			$this->checkTree($tree, $parents);
		}
		else
		{
			$this->_tree = $tree;
		}
	}
	
	public function checkGroup($group_id, $producer_id)
	{
		if ($group_id > 0)
		{
			return $group_id;
		}
		
		if (true == $this->checkUserType())
		{
			$query = 'SELECT groups_tree.id FROM products_status JOIN producers ON products_status.producers = producers.name JOIN groups_tree ON 
			products_status.subgroup = groups_tree.comment WHERE user = '.$this->_user['parent_id'].' AND status = 1 ';
					
			if ($producer_id > 0)
			{
				$query .= ' AND producers.id = '.$producer_id;
			}
					
			$query .= ' LIMIT 1';
				
			$result = $this->_db->query($query) or $this->_db->raise_error();
					
			if (mysql_num_rows($result))
			{
				$row = mysql_fetch_array($result);
				return $row['id'];
			}
		}

		$query = 'SELECT subgroup_id FROM products WHERE subgroup_id > 0 ';
				
		if ($producer_id > 0)
		{
			$query .= ' AND producer_id = '.$producer_id;
		}
				
		$query .= ' LIMIT 1';
			
		$result = $this->_db->query($query) or $this->_db->raise_error();
					
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			return $row['subgroup_id'];
		}
	}

	public function getGroups($producer_id = 0)
	{
		$list_type = $_SESSION["b2b"]['list_type'];
		
		if ($producer_id)
		{
			$condition = ' WHERE producer_id = '.$producer_id;
		}
		
		$query = 'SELECT DISTINCT producers.id AS producer_id, producers.name AS producer_name, subgroup_id, groups_tree.name AS subgroup_name 
		FROM producers JOIN products ON producers.id = products.producer_id JOIN groups_tree ON products.subgroup_id = groups_tree.id '.$condition.' 
		ORDER BY producers.name ASC, groups_tree.name ASC';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getGroupName($group_id)
	{
		$result = $this->_db->query('SELECT name FROM groups_tree WHERE id = '.$group_id) or $this->_db->raise_error();
		$row = mysql_fetch_array($result);
		return $row['name'];
	}
	
	public function getProductsCount($group_id, $producer_id, $visibility, $search = array())
	{
		if (true == $this->checkUserType())
		{
			$query = 'SELECT products_status.id FROM products_status JOIN products ON products_status.id = products.id_hermes JOIN product_versions ON 
			products.id_hermes = product_versions.product_id JOIN producers ON products.producer_id = producers.id WHERE products.comment LIKE concat((SELECT 
			comment FROM groups_tree WHERE id = '.$group_id.'),\'%\') AND products_status.status = 1 AND products_status.user = '.$this->_user['parent_id'];
				
			if ($visibility)
			{
				$query .= ' AND amount > 0';
			}
			if ($producer_id)
			{
				$query .= ' AND producer_id = '.$producer_id;
			}	
			if (is_array($search) && !empty($search))
			{
				$query .= ' AND product_versions.name LIKE \'%'.mysql_real_escape_string($search[0]).'%\' AND symbol LIKE \'%'.mysql_real_escape_string($search[1]).'%\' 
				AND producers.name LIKE \'%'.mysql_real_escape_string($search[2]).'%\'';
			}
				
			$result = $this->_db->query($query) or $this->_db->raise_error();
			return mysql_num_rows($result);
		}

		$query = 'SELECT products.id FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id LEFT JOIN producers ON 
		products.producer_id = producers.id WHERE products.comment LIKE concat((SELECT comment FROM groups_tree WHERE id = '.$group_id.'),\'%\')';
		
		if ($visibility)
		{
			$query .= ' AND products.amount > 0';
		}
		if ($producer_id)
		{
			$query .= ' AND products.producer_id = '.$producer_id;
		}
		if (is_array($search) && !empty($search))
		{
			$query .= ' AND product_versions.name LIKE \'%'.mysql_real_escape_string($search[0]).'%\' AND symbol LIKE \'%'.mysql_real_escape_string($search[1]).'%\' 
			AND producers.name LIKE \'%'.mysql_real_escape_string($search[2]).'%\'';
		}

		$result = $this->_db->query($query) or $this->_db->raise_error();
	}
	
	public function getAllGroups()
	{
		$query = 'SELECT id, level, name, `order` FROM groups_tree ORDER BY `order` ASC';
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		return $result;
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}

$groups = new Groups();
?>
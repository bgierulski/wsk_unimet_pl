<?php
class Details extends Validators 
{
	private $_db;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
	}
	
	public function getDetails()
	{
		$result = $this->_db->query('SELECT title FROM b2b_details WHERE id = 1') or $this->_db->raise_error();
		$row = mysql_fetch_array($result);
		
		return $row;
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}

$b2b_details = new Details();
?>
<?php
$list = $restrictions->getRestrictions($_POST['key']);
$restrictions_count = count($list);

$on_page = ((!empty($_POST['key'])) ? 2000 : 50);
	
$pages_count = ceil($restrictions_count/$on_page);
	
if ((int)$_GET['id'] > 0 && (int)$_GET['id'] <= $pages_count)
{
	$page_number = (int)$_GET['id'];
}
else 
{
	$page_number = 1;
}
	
$first_restriction = $page_number * $on_page - $on_page;
$last_restriction = $page_number * $on_page - 1;
?>

<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["Informations"]['error']); ?>
</div>
<?php } else if (isset($_SESSION["communicats"]['ok'])) { ?>
<div class="form_element ok">
	<?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["Informations"]['ok']); ?>
</div>
<?php } ?>

<div>
	<div style="float:left;">
		<form method="post" action="">
		Wyszukaj użytkownika: <input type="text" name="key" value="" style="width:200px;" /> 
		<input type="submit" name="search_restrictions" value="Szukaj" />
		</form>
	</div>
	<div style="float:right;">
		<?php 
		if ($pages_count > 1)
		{
			?>
			<div id="subpages" style="text-align:right;">
			<?php 
			for ($i=1; $i<=$pages_count; $i++)
			{
				if ($page_number == $i) { ?><strong><?php }
				?><a href="./restrictions/list/<?php echo $i; ?>" style="color:#1178f3; text-decoration:none;"><?php echo $i; ?></a> <?php
				if ($page_number == $i) { ?></strong><?php }
			
				if ($i < $pages_count)
				{
					?><strong style="color:#ccccff;">|</strong> <?php
				}
			}
			?>
			</div>
			<?php
		}
		?>
	</div>
	<div class="clear"></div>
</div>

<table>
	<tr>
		<th>NIP</th>
		<th>Użytkownik</th>
		<th>Zakres ograniczenia</th>
		<th>Ograniczenie</th>
		<th>Operacje</th>
	</tr>
	<?php
	if (is_array($list))
	{
		foreach ($list as $key => $row)
	   	{
	   		if ($key >= $first_restriction && $key <= $last_restriction)
			{
				?>
			    <tr>
					<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['identificator']; ?></td>
					<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['name'].' ('.$row['login'].')'; ?></td>
					<td class="bg_<?php echo ($key%2)+1; ?>">
					<?php 
					switch ($row['element_type'])
					{
						case 2: ?>producent<?php break;
						case 1: ?>grupa<?php break;
						default: ?>produkt<?php break;
					}
					?>
					</td>
					<td class="bg_<?php echo ($key%2)+1; ?>">
					<?php echo $row['element_name']; ?>
					</td>
					<td class="bg_<?php echo ($key%2)+1; ?>">
					<a class="usun" href="./restrictions/delete/<?php echo $row['id']; ?>">usuń ograniczenie</a>
					</td>
				</tr>
			    <?php
			}
	   	}
	}
	?>
</table>

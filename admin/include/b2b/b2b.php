<?php
if (!isset($_POST['save_changes']))
{
	$details = $b2b_admin->getDetails();
}
?>

<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
</div>
<?php } else if (isset($_SESSION["communicats"]['ok'])) { ?>
<div class="form_element ok">
	<?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?>
</div>
<?php } ?>

<form method="POST" action="">
	
	<?php  if ($_SESSION["errors"]['title']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['title']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Tytuł strony</label>
		<input type="text" name="title" value="<?php echo $b2b_admin->textControll($_SESSION["values"]['title']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['marquee']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['marquee']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Tresc w pasku marquee</label>
		<textarea name="marquee" cols="0" rows="0" style="width:200px; height:100px;"><?php echo $b2b_admin->textControll($_SESSION["values"]['marquee']); ?></textarea>
		<div class="clear"></div>
	</div>
	
	<div class="form_element">
		<label style="width:120px;">&nbsp;</label>
		<input id="submit" name="save_changes" value="Zapisz zmiany" type="submit" /> 
		<div class="clear"></div>
	</div>
</form> 

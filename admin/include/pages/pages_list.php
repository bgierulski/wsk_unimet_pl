<?php
$list = $pages->getPages();
?>

<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
</div>
<?php } else if (isset($_SESSION["communicats"]['ok'])) { ?>
<div class="form_element ok">
	<?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?>
</div>
<?php } ?>

<table>
	<tr>
		<th>Tytuł podstrony</th>
		<th>Operacje</th>
	</tr>
	<?php
	if (is_array($list))
	{
		foreach ($list as $key => $row)
	   	{
			?>
		    <tr>
				<td class="bg_<?php echo ($key%2)+1; ?>">
				<?php 
				echo $row['title']; 
				if (in_array($row['id'], array(9,13))) { ?> - użytkownik nie jest zalogowany <?php } 
				if (in_array($row['id'], array(12,13,14,15,16,17))) { ?> (<strong>WSK</strong>)<?php } 
				?>
				</td>
				<td class="bg_<?php echo ($key%2)+1; ?>">
				<a href="./pages/edit/<?php echo $row['id']; ?>">edytuj podstronę</a>
				</td>
			</tr>
		    <?php
	   	}
	}
	?>
</table>

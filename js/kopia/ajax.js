var cookie_tree;
var gallery_content = '';

function productsAmountChange()
{
	if (true == amount[3])
	{
		amount[3] = false;
		var order_id = parseInt(amount[1]);
		var id = parseInt(amount[0]);
		
		$.ajax({
			
			type: "POST",
			url: "js/xml.php?operation=change_order_amount",
			data: {id:id, order_id:order_id, amount:amount[2], operation:'change_order_amount'},
			dataType: "xml",
		  
			success: function(xml) 
			{
				if ($(xml).find("controll").length)
				{
					switch(parseInt($(xml).find("controll").text()))
					{
						case 1:
							$("span.netto_value[alt='"+order_id+"']").parent().parent().find("a.accept_archive_order").css("display","inline");
							$("span.netto_value[alt='"+order_id+"']").parent().parent().find("strong.exceeded_budget").css("display","none");
							break;
							
						default:
							$("span.netto_value[alt='"+order_id+"']").parent().parent().find("a.accept_archive_order").css("display","none");
							$("span.netto_value[alt='"+order_id+"']").parent().parent().find("strong.exceeded_budget").css("display","inline");
							break;
					}
					
					$("span.netto_value[alt='"+order_id+"']").text($(xml).find("netto_sum").text());
					$("input[name='amount'][alt='"+order_id+"#"+id+"']").parent().parent().parent().find("span.netto_value").text($(xml).find("netto_value").text());
					$("input[name='amount'][alt='"+order_id+"#"+id+"']").parent().parent().parent().find("span.brutto_value").text($(xml).find("brutto_value").text());
				}
			}
		});
	}
	
	setTimeout(function(){
		productsAmountChange();
	},  2000);
}

function getProducts(group_id, producer_id)
{
	var search = new Array($("input[name='search[0]']").val(), $("input[name='search[1]']").val(), $("input[name='search[1]']").val());
	$("#page").html('');
	
	$.ajax({
		
		type: "POST",
		url: "js/xml.php?operation=get_products",
		data: {group_id:group_id, producer_id:producer_id, login_id:$("input[type='hidden'][name='login_id']").val(), search:search, operation:'get_products'},
		dataType: "xml",
		async:false,
		
		success: function(xml){
			
			$("#page").html($(xml).find("content").text());
			
			contentHeight();
			checkedProducts();
			
			$("input[name='search[0]']").val(search[0]);
			$("input[name='search[1]']").val(search[1]);
			$("input[name='search[2]']").val(search[2]);
		}
	});
}

function getParents(group_id, level)
{	
	$.ajax({
		
		type: "POST",
		url: "js/xml.php?operation=get_parents",
		data: {group_id: group_id, operation:'get_parents'},
		dataType: "xml",
		async:false,
		
		success: function(xml){
			
			cookie_tree = '';
			var operation;
			
			$("#groups").ready(function() {
				
				var tree = $(this);
				
				$("#groups li ul").css("display","none");
				$("#groups li ul").removeClass("active_branch").removeClass("active_family");
				$("#groups a").removeClass("a_branch").removeClass("a_family").removeClass("actual_group");
				
				$(tree).find('li').each(function(){
					
					if ($(this).find("a:first").attr("id") == 'info_'+level+"_"+group_id)
					{
						if ($(this).hasClass("hide"))
						{
							operation = 'hide';
							$(this).removeClass("hide").addClass("show");
							$(tree).find("ul#list_"+group_id).css("display","none");
						}
						else
						{
							operation = 'show';
							$(this).removeClass("show").addClass("hide");
						}
					}
					
					$(this).removeClass("hide").addClass("show");
				});
			});	
			
			var counter = 0;
			var ids = new Array();
			
			$(xml).find('id').each(function(){
				
				if (('hide' == operation && parseInt($(this).text()) != group_id) || 'show' == operation)
				{
					var element = $("ul#list_"+$(this).text()).parent();
					$(element).removeClass("show").addClass("hide");
					
					$("ul#list_"+$(this).text()).css("display","block");
					
					ids.push(parseInt($(this).text()));
					cookie_tree += '.'+parseInt($(this).text());
					
					counter++;
				}
			});
			
			var branches_count = counter - 2;
			
			counter = 0;
			
			for (var i=(ids.length - 1); i>=0; i--)
			{
				var element = $("ul#list_"+ids[i]).parent();
				
				if (counter < branches_count)
				{
					$("ul#list_"+ids[i]+" a").removeClass("a_family").removeClass("active_family").addClass("active_branch");
				}
				else
				{
					$("ul#list_"+ids[i]+" a").removeClass("a_family").removeClass("active_branch").addClass("active_family");
					
				}
				
				$(element).find("a:first").removeClass("active_branch").removeClass("active_family").addClass("a_family");
				
				counter++;
			}
			
			$("#groups a#info_"+level+"_"+group_id).addClass("actual_group");
			
			$.cookie('tree', cookie_tree, { expires: 1, path: '/'});
			/*
			if (level == 1 && $("#page_content #main_"+group_id).length)
			{
				if ('show' == operation)
				{
					$("#page_content .main_group").css("display","none");
					$("#page_content #main_"+group_id).css("display","block");
				}
				else
				{
					$("#page_content .main_group").css("display","block");
				}
			}
			*/
		}
	});
}

function getProducers()
{
	$.ajax({
		
		type: "POST",
		url: "js/xml.php?operation=get_producers",
		data: {operation:'get_producers'},
		dataType: "xml",
	  
		success: function(xml) 
		{
			$("div.offer_producers select[name='producer_id'] optgroup").html($(xml).find("options").text());
		}
	});
}

function getTree(producer_id, change)
{	
	var group_id = (($("span.actual_group").length) ? parseInt($("span.actual_group").attr("id").split('_')[1]) : 0);
	
	$.ajax({
		
		type: "POST",
		url: "js/xml.php?operation=get_tree",
		data: {producer_id:producer_id, group_id:group_id, operation:'get_tree'},
		dataType: "xml",
	  
		success: function(xml) 
		{
			$("#groups").html($(xml).find('tree').text());
			$("#products_sum").html('['+$(xml).find('sum').text()+']');
			
			cookie_tree = $.cookie('tree');
			
			if (cookie_tree != null)
			{
				var table = cookie_tree.split('.');
				
				var branches_count = table.length - 3;
				var counter = 0;
				var level = (table.length - 1);
				
				if (false == change)
				{
					for (var i=(table.length-1); i>0; i--)
					{
						$("#groups").find("#list_"+table[i]).css("display","block");
						$("#groups").find("#list_"+table[i]).parent().removeClass("show").addClass("hide");
						
						var element = $("ul#list_"+table[i]).parent();
						
						if (counter < branches_count)
						{
							$("ul#list_"+table[i]+" a").removeClass("a_family").removeClass("active_family").addClass("active_branch");
						}
						else
						{
							$("ul#list_"+table[i]+" a").removeClass("a_family").removeClass("active_branch").addClass("active_family");
							
						}
						
						$(element).find("a:first").removeClass("active_branch").removeClass("active_family").addClass("a_family");
						
						counter++;
					}
				}
				
				$("#groups a#info_"+level+"_"+table[1]).addClass("actual_group");
			}
		}
	});
	
	return false;
}

function clearBasket()
{
	$.ajax({
		
		type: "POST",
		url: "js/xml.php?operation=clear_basket",
		data: {operation:'clear_basket'},
		dataType: "xml",
		success: function(xml) 
		{
			window.opener.location.reload();
		}
	});
}

function showGroup(element)
{
	var data = $(element).attr("id").split('_');
	var level = parseInt(data[1]);
	var group_id = parseInt(data[2]);
	
	$("#groups a").removeClass("active");
	$(element).addClass("active");
	
	getParents(group_id, level);
	
	$.ajax({
		
		type: "POST",
		url: "js/xml.php?operation=get_pictograms",
		data: {group_id:group_id, operation:'get_pictograms'},
		dataType: "xml",
	  
		success: function(xml) 
		{
			$("div.main_group").css("display", "none");
			
			if ($(xml).find("group").length)
			{
				$(xml).find("group").each(function(){
					
					$("div#main_"+$(this).text()).css("display", "block");
				});
				
				$("#page_content p.main_prev a").attr("id", "main_prev_"+(level-1)+"_"+$(xml).find("parent_id").text());
				$("#page_content p.main_prev").css("display", "block");
			}
			else if (level == 1)
			{
				$("#page_content p.main_prev a").attr("id", "main_prev_0_0");
				$("#page_content p.main_prev").css("display", "block");
				$("div#main_1_"+group_id).css("display", "block");
			}
			else if (level > 1)
			{
				if (false == strstr(document.location.href, "grupa"))
				{
					document.location="./grupa,"+group_id+",1."+parseInt($("select[name='producer_id']").val())+".html";
				}
					
				var detail = $(element).attr("href").split(',')[2];
				var producer_id = parseInt(str_replace('1.', '', ((detail == undefined) ? '0' : detail)));
					
				getProducts(group_id, producer_id);
			}
		}
	});
}

var description_controll = 0;
var description_visibility = 0;
var product_description;

$(document).ready(function() {
	
	var tree_controll = false;
	var positions = new Array();
	var product_id = 0;
	var person_description = '';
	var person_id;
	
	$.ajax({
		
		type: "POST",
		url: "js/xml.php?operation=check_basket",
		data: {operation:'check_basket'},
		dataType: "xml",
		success: function(xml) 
		{
			if ($(xml).find("basket").length)
			{
				if (parseInt($(xml).find("basket").text()) == 1)
				{
					if (confirm('W Twoim koszyku znajdują się produkty, czy usunąć zawartość koszyka ?'))
					{
						clearBasket();
					}
					else
					{
						window.opener.location = './koszyk.html';
					}
				}
			}
		}
	});
	
	$(".communicat_box .close").click(function(){
		
		var id = parseInt($(this).parent().parent().attr("id").split("_")[1]);
		
		if ($("div#communicat_"+id).find("input[type='checkbox']").attr("checked") == true)
		{
			$.ajax({
				
				type: "POST",
				url: "js/xml.php?operation=check_comment",
				data: {id:id, operation:'check_comment'},
				dataType: "xml",
				  
				success: function(xml) 
				{
					$("#communicat_"+id).css("display", "none");
					$("#communicat_"+id).removeClass("communicat_box");
					$("div.communicat_box:first").css("display","block");
				}
			});
		}
		else
		{
			$("#communicat_"+id).css("display", "none");
			$("#communicat_"+id).removeClass("communicat_box");
			$("div.communicat_box:first").css("display","block");
		}
	});
	
	$(".person").mouseover(function(){
		
		var data = $(this).attr("id").split("_");
		var position = $(this).offset();
		
		$.ajax({
				
			type: "POST",
			url: "js/xml.php?operation=get_person",
			data: {type:data[0], id:parseInt(data[1]), operation:'get_person'},
			dataType: "xml",
			  
			success: function(xml) 
			{
				person_description = $(xml).find("content").text();
				person_id = parseInt(data[1]);
					
				$("#person_description #images").html(person_description);
				$("#person_description").css({"display":"block", "top":(parseInt(position.top)+85)+"px", "left":parseInt(position.left)+"px"});
				

			}
		});
	});
	
	$(".person").mouseleave(function(){
		
		setTimeout(function(){
			
			$("#person_description #images").html('');
			$("#person_description").css({"display":"none"});
			
		},  3000);
	});
	
	$("td.td_description strong").live("mousemove",function(){
		
		var product_id = parseInt($(this).attr("id").split("_")[1]);
		var position = $(this).offset();
		
		description_visibility = product_id;
		
		if (description_controll != product_id)
		{
			$.ajax({
				
				type: "POST",
				url: "js/xml.php?operation=get_description",
				data: {product_id:product_id, operation:'get_description'},
				dataType: "xml",
			  
				success: function(xml) 
				{
					description_controll = product_id;
					product_description = $(xml).find('description').text();
					
					$("#product_description").css({"display":"block", "top":(parseInt(position.top)-10)+"px", "left":(parseInt(position.left)+50)+"px"});
					$("#product_description #images").html(product_description);
					
					if (description_visibility != product_id)
					{
						$("#product_description").css("display","none");
					}
				}
			});
		}
		else
		{
			$("#product_description").css({"display":"block", "top":(parseInt(position.top)-10)+"px", "left":(parseInt(position.left)+50)+"px"});
			$("#product_description #images").html(product_description);
			
			if (description_visibility != product_id)
			{
				$("#product_description").css("display","none");
			}
		}
	});
	
	$("td.td_description strong").live("mouseleave",function(){
		
		description_visibility = 0;
		$("#product_description").css("display","none");
	});
	
	$("span.favorite_product img, p.favorite_product a").live("click",function(){
		
		var product_id = parseInt($(this).attr("id").split('_')[1]);
		var position = (($(this).attr("href") == undefined) ? false : true);
		
		$.ajax({
			
			type: "POST",
			url: "js/xml.php?operation=add_favorite",
			data: {product_id:product_id, operation:'add_favorite'},
			dataType: "xml",
		  
			success: function(xml) 
			{
				var comment = ((parseInt($(xml).find('status').text()) == 1) ? ' - produkt został dodany do ulubionych' : ' - dodanie produktu do ulubionych nie powiodło się');
				$("#page_header span.comment").text(comment);
				
				if (true == position)
				{
					alert('Produkt został dodany do ulubionych.');
				}
			}
		});
		
		return false;
	});
	
	$("select[name='producer_id']").change(function(){
		
		var producer_id = parseInt($("select.[name='producer_id']").val());
		var group_id = parseInt($(this).find("option[value='"+producer_id+"']").attr("class").split("_")[1]);
		
		getTree(producer_id, true);
		getProducts(group_id, producer_id);
		tree_controll = true;
	});
	
	if (false == tree_controll)
	{
		getTree(parseInt($("select[name='producer_id']").val()), false);
	}

	$("#groups a").live("click",function(){ 
		
		showGroup(this);
		/*
		var data = $(this).attr("id").split('_');
		var level = parseInt(data[1]);
		var group_id = parseInt(data[2]);
		
		$("#groups a").removeClass("active");
		$(this).addClass("active");
		
		getParents(group_id, level);
		
		$.ajax({
			
			type: "POST",
			url: "js/xml.php?operation=get_parent",
			data: {group_id:group_id, operation:'get_parent'},
			dataType: "xml",
		  
			success: function(xml) 
			{
				if ($(xml).find("group").length)
				{
					$("div.main_group").css("display", "none");
					
					$(xml).find("group").each(function(){
						
						$("div#main_"+$(this).text()).css("display", "block");
					});
				}
				else if (level > 1)
				{
					if (false == strstr(document.location.href, "grupa"))
					{
						document.location="./grupa,"+group_id+",1."+parseInt($("select[name='producer_id']").val())+".html";
					}
						
					var element = $(this).attr("href").split(',')[2];
					var producer_id = parseInt(str_replace('1.', '', ((element == undefined) ? '0' : element)));
						
					getProducts(group_id, producer_id);
				}
			}
		});
		*/
		return false;
	});
	
	$("input[name='key']").keyup(function(e){
		
		var key = $(this).val();
		
		$('#search_results').css("display","none");
		
		if (key.length >= 3)
		{	
			$("#search_results div").html("pobieram dane ..");
			$("#search_results p").css("visibility","hidden");
			$("#search_results").css("display","block");
			
			if (e.keyCode == 13) 
			{
				window.location.href = './wyszukiwarka.html';
			}
			else
			{	
				$.ajax({
					
					type: "POST",
					url: "js/xml.php?operation=search",
					data: {key: key, operation:'search'},
					dataType: "xml",
				  
					success: function(xml) 
					{
						var result = '';
						var products_count = parseInt($(xml).find('product').length);
						var groups_count = parseInt($(xml).find('group').length);
						
						if (products_count + groups_count > 0)
						{
							if (groups_count)
							{
								result += '<ul>';
								result += '<li class="title"><strong>Grupy produktowe</strong></li>';
								
								$(xml).find('group').each(function(){
									result += '<li><a href="#" onClick="javascript:getParents('+$(this).find("id").text()+', 2); getProducts('+$(this).find("id").text()+', 0); return false;">'+$(this).find("name").text()+'</a></li>';
								});
								
								result += '</ul>';
							}
							
							if (products_count)
							{
								result += '<ul>';
								result += '<li class="title"><strong>Produkty</strong></li>';
								
								$(xml).find('product').each(function(){
									result += '<li><a href="#" onClick="javascript:pop_up('+$(this).find("id").text()+'); return false;">'+$(this).find("name").text()+'</a></li>';
								});
								
								result += '</ul>';
							}
							
							$("#search_results p").css("visibility","visible");
						}
						else
						{
							result += 'brak wyników';
							$("#search_results p").css("visibility","hidden");
						}
						
						$("#search_results div").html(result);
					}
				});
			}
		}
	});
	
	/* gallery */
	
	$(".subgroup .wrapper").live("mouseover",function(){
		
		product_id = $(this).attr("id").split("_")[1];
		
		if (positions.length == 0)
		{
			setTimeout(function(){
				
				$.ajax({
					
					type: "POST",
					url: "js/xml.php?operation=gallery",
					data: {product_id:product_id, 'operation':'gallery'},
					dataType: "xml",
				  
					success: function(xml) 
					{
						gallery_content = '<div id="corner">';
						
						gallery_content += '<p><img src="images/element_5.png" alt="" /></p>';
						gallery_content += '</div>';
						gallery_content += '<div id="images">';
						gallery_content += '<div id="big_image">';
						
						gallery_content += '<div id="big_'+product_id+'" style="padding:10px;"><p onclick="javascript:pop_up('+product_id+'); return false;" style="cursor:pointer;">';
						gallery_content += '<img src="http://b2b.unimet.pl/include/image.php?id='+product_id+'&w=210&h=210" alt="" border="0">';
						gallery_content += '</p></div>';
						
						$(xml).find('image').each(function(){
							gallery_content += '<div id="big_'+$(this).text()+'" style="padding:10px; display:none;"><p>';
							gallery_content += '<img src="http://b2b.unimet.pl/include/image.php?uid='+$(this).text()+'&w=210&h=210" alt="" border="0">';
							gallery_content += '</p></div>';
						});
											
						gallery_content += '<p class="title"><strong onclick="javascript:pop_up('+product_id+'); return false;" style="cursor:pointer;">'+$(xml).find('name').text()+'</strong></p>';
										
						gallery_content += '</div>';
						gallery_content += '<div id="small_image">';
										
						gallery_content += '<div class="wrapper" id="small_'+product_id+'"><div class="cell"><div class="hack"><p>';
						gallery_content += '<img src="http://b2b.unimet.pl/include/image.php?id='+product_id+'&w=38&h=38" alt="" border="0">';
						gallery_content += '</p></div></div></div>';
						
						$(xml).find('image').each(function(){
							
							gallery_content += '<div class="wrapper" id="small_'+$(this).text()+'"><div class="cell"><div class="hack"><p>';
							gallery_content += '<img src="http://b2b.unimet.pl/include/image.php?uid='+$(this).text()+'&w=38&h=38" alt="" border="0">';
							gallery_content += '</p></div></div></div>';
						});
										
						gallery_content += '</div>';
						gallery_content += '<div class="clear"></div>';
									
						gallery_content += '</div>';
						gallery_content += '<div class="clear"></div>';
						
						$("body").ready(function() {
							
							var image = $(this).find(".subgroup .wrapper#product_"+product_id).offset();							
							$(this).find("#product_gallery").html(gallery_content);
							$(this).find("#product_gallery").css({"display":"block", "left":(image.left+42)+"px", "top":image.top+"px"});
							
							positions = new Array();
							
							positions.push(
							new Array(
								new Array(parseInt(image.top), (parseInt(image.top) + parseInt($(this).find(".subgroup .wrapper#product_"+product_id).height()))),
								new Array(parseInt(image.left), (parseInt(image.left) + parseInt($(this).find(".subgroup .wrapper#product_"+product_id).width())))
							),
							new Array(
								new Array(parseInt(image.top), (parseInt(image.top) + 280)),
								new Array(parseInt(image.left+42), (parseInt(image.left+42) + parseInt($(this).find("#product_gallery").width())))
							));

						});
					}
				});
				
			},  1000);
		}
	});
	
	$(document).mousemove(function(e){
		
		mouseX = e.pageX;
		mouseY = e.pageY;
       
		if (positions.length > 0)
		{
			if (!((mouseY >= positions[0][0][0] && mouseY <= positions[0][0][1] && mouseX >= positions[0][1][0] && mouseX <= positions[0][1][1]) ||
			(mouseY >= positions[1][0][0] && mouseY <= positions[1][0][1] && mouseX >= positions[1][1][0] && mouseX <= positions[1][1][1])))
			{
				$("#product_gallery").css("display","none");
				gallery_content = '';
				positions = new Array();
			}     
		}
	});
	
	$('#product_gallery div#small_image div.wrapper').live("mouseover",function(){
		
		var id = $(this).attr("id").split("_")[1];
		
		$("#product_gallery div#big_image div").css("display","none");
		$("#product_gallery div#big_image div#big_"+id).css("display","block");
	});
	
	$("div#full_offer, div#narrowed_offer").click(function(){
		
		if ($(this).find("div.compressed").length)
		{
			$.ajax({
				
				type: "POST",
				url: "js/xml.php?operation="+$(this).attr("id"),
				data: {operation:$(this).attr("id")},
				dataType: "xml",
			  
				success: function(xml) 
				{
					document.location = $("input[type='hidden'][name='base_address']").val();
					
					$("div.box div.compressed").parent().attr("id", $(xml).find("id").text());
					$("div.box div.compressed strong").html($(xml).find("title").text()+" <span>["+$(xml).find("amount").text()+"]</span>");
					
					getProducers();
					getTree(0, false);
				}
			});
		}
	});
	
	$("table.proteges_list input.mpk").keyup(function(e){
		
		var value = $(this).val();

		if (value.length > 1)
		{	
			$.ajax({
				
				type: "POST",
				url: "js/xml.php?operation=set_mpk",
				data: {value:value, key:parseInt($(this).attr("alt")), operation:'set_mpk'},
				dataType: "xml",
			  
				success: function(xml) 
				{
				}
			});
		}
	});
	
	$("input[name='search[0]'], input[name='search[1]'], input[name='search[2]']").live("keyup",function(e){
		
		var value = $(this).val();
		
		if (e.keyCode == 13)
		{
			var producer_id = parseInt($("select.[name='producer_id']").val());
			var group_id = parseInt($("#groups a.active").attr("id").split("_")[2]);
			
			getProducts(group_id, producer_id);
		}
	});
	
	$("img#search_0, img#search_1, img#search_2").live("click",function(){
		
		var producer_id = parseInt($("select.[name='producer_id']").val());
		var group_id = parseInt($("#groups a.active").attr("id").split("_")[2]);
		
		getProducts(group_id, producer_id);
	});
});

<div class="icons">
	<table>
	<tr>
		<td><p><a href="./kontakt.html"><img src="images/contact.png" alt="" /></a></p></td>
		<td><p><a href="./mapa.html"><img src="images/page_map.png" alt="" /></a></p></td>
		<td><p><a href="./"><img src="images/main_page.png" alt="" /></a></p></td>
		<td><p class="last"><a href="./informacje.html"><img src="images/informations_icon.png" alt="" /></a></p></td>
	</tr>
	</table>
</div>

<div>
<form action="" method="post" name="account">
<?php  if (!isset($_SESSION["b2b_user"]['login'])) { ?>
<input type="hidden" name="operation" value="user_login" />

<div id="login_box">

	<div style="padding:0; margin:0;">
		<span><input type="text" name="login" value="Login" /></span> 
		<span><input type="text" name="password" value="Hasło" /></span>
	</div>
	<div class="button">
	<a href="./" id="account_form_submit">ZALOGUJ</a>
	</div>
	<div class="clear"></div>

</div>
<?php } else { ?>
<input type="hidden" name="operation" value="user_logout" />
<div id="login_box">
	<div id="user_functions" style="position:relative;" title="zobacz informacje o funkcjach użytkownika">
		<p id="login_description" style="background:none;">Witaj</p>
		<p id="login"><?php echo $_SESSION["b2b_user"]['user_name'].' '.$_SESSION["b2b_user"]['user_surname']; ?></p>
		
		<?php 
		$functions_list = $users->getFunctions();
		
		if (is_array($functions_list) && !empty($functions_list))
		{
			?><div id="user_functions_details"><?php
			
			foreach ($functions_list as $function)
			{
				if ($function['contractor'] == 1)
				{
					?><p><strong><?php echo $function['department']; ?>:</strong> zamawiający</p><?php 
				}
				if ($function['analyst'] == 1)
				{
					?><p><strong><?php echo $function['department']; ?>:</strong> analityk</p><?php 
				}
				if ($function['manager'] == 1)
				{
					?><p><strong><?php echo $function['department']; ?>:</strong> kierownik</p><?php 
				}
				if ($function['receiving'] == 1)
				{
					?><p><strong><?php echo $function['department']; ?>:</strong> odbierający</p><?php 
				}
			}
			
			?></div><?php
		}
		?>
	</div>
	<div class="button">
	<a href="./" id="account_form_submit">WYLOGUJ</a>
	</div>
	<div class="clear"></div>
	<div class="button" id="budget_info">
	<a href="./">BUDŻET</a>
	</div>
	<div class="clear"></div>
</div>
<?php } ?>
</form>
</div>
<div class="clear"></div>

<div id="budget_box">

<?php 
$budget_list = $users->getBudgets();

if (is_array($budget_list) && !empty($budget_list))
{
	?>
	<table cellspacing="0" cellpadding="0">
	<tr><th class="first">Wydział</th><th>Budżet</th></tr>
	<?php 
	foreach ($budget_list as $budget)
	{
		if ($_SESSION["b2b_user"]['admin'] == 1 || $budget['contractor'] > 0 || $budget['analyst'] > 0 || $budget['manager'] > 0)
		{
			?><tr><td class="first"><?php echo $budget['department']; ?></td><td><?php echo $budget['budget']; ?> zł</td></tr><?php
		}
	}
	?>
	</table>
	<?php
}
else
{
	?>brak danych<?php
}
?>
</div>
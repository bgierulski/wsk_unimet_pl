<?php
class Orders extends Validators 
{
	private $_db;
	private $_user;
	private $_errors;
	
	private $_order_id;
	private $_delivery_date;
	private $_fv;
	private $_wm;
	private $_information;
	
	private $_names = array();
	private $_symbols = array();
	private $_producers = array();
	private $_units = array();
	private $_taxes = array();
	private $_replacements = array();
	private $_amounts = array();
	private $_ids = array();
	private $_prices = array();
	private $_operation;
	private $_elements = array();
	private $_details = array();
	private $_file;
	
	private $_controller;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		$this->_user = $this->getSession("admin");
		$this->_controller = $_GET['controller'];
	}
	
	public function correction()
	{
		/*
		$query = 'SELECT orders.id, orders.netto_value, orders.brutto_value, netto_price, tax, brutto_price, amount FROM order_details JOIN orders ON order_details.order_id = orders.id 
		ORDER BY order_details.order_id ASC, order_details.id ASC';
		
		$list = $this->_db->mysql_fetch_all($this->_db->query($query));
		
		if (is_array($list) && !empty($list))
		{
			foreach ($list as $key => $element)
			{
				if ($element['id'] != $list[$key-1]['id'])
				{
					$brutto_sum = 0;
					$netto_sum = 0;
				}
				
				$tax = ((int)$element['tax']*$element['netto_price'])/100;
				$netto_sum += ($element['netto_price'] * $element['amount']); 
				$brutto_sum += (($element['netto_price'] + $tax) * $element['amount']); 
				
				if ($element['id'] != $list[$key+1]['id'])
				{
					$query = 'UPDATE orders SET brutto_value = \''.sprintf("%0.2f", $brutto_sum).'\', netto_value = \''.sprintf("%0.2f", $netto_sum).'\' WHERE id = '.$element['id'];
					$this->_db->query($query) or $this->_db->raise_error();
				}
			}
		}
		*/
	}
	
	public function budgetChanges()
	{
		$query = 'SELECT wsk_budget.id as budget_id, wsk_budget_history.id, users.user_name, users.user_surname, old_value, new_value, wsk_budget_history.amount, wsk_budget_history.info, wsk_budget_history.date FROM 
		wsk_budget JOIN wsk_budget_history ON wsk_budget.department = wsk_budget_history.department JOIN users ON wsk_budget_history.user = users.id ORDER BY wsk_budget.id ASC, wsk_budget_history.date DESC';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			while($row = mysql_fetch_array($result)) 
	   		{
	   			unset($query);
	   		
	   			if (!in_array($row['info'], array("korekta budżetu", "zmiana budżetu")))
	   			{
	   				$query = 'UPDATE wsk_budget SET `full_budget` = `full_budget` + (\''.$row['amount'].'\') WHERE id = '.$row['budget_id'];
	   			}
	   			else
	   			{
	   				$query = 'UPDATE wsk_budget SET `used_budget` = `used_budget` - (\''.$row['amount'].'\') WHERE id = '.$row['budget_id'];
	   			}

	   			if (!empty($query))
	   			{
	   				//echo $query.'<br />';
	   				//$this->_db->query($query) or $this->_db->raise_error();
	   			}
	   		}
		}
	}
	
	public function setRejectComment($id, $comment)
	{
		$result = $this->_db->query('SELECT id FROM order_informations WHERE order_id = '.$id) or $this->_db->raise_error();
			
		if (mysql_num_rows($result))
		{
			$query = 'UPDATE order_informations SET reject_comment = \''.trim(mysql_escape_string($comment)).'\' WHERE order_id = '.$id;
		}
		else
		{
			$query = 'INSERT INTO order_informations VALUES (DEFAULT, '.$id.', \'\', \'\', \'\', \'\', \''.trim(mysql_escape_string($comment)).'\')';
		}
		
		$this->_db->query($query) or $this->_db->raise_error();
	}
	
	public function getRejectComment($id)
	{
		$result = $this->_db->query('SELECT order_id as id, reject_comment FROM order_informations WHERE order_id = '.$id) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			return $row;
		}
	}
	
	public function getUsersNames()
	{
		$result = $this->_db->query('SELECT login, user_name, user_surname, phone FROM users WHERE parent_id = 8338 OR status = 0') or $this->_db->raise_error();
		$data = array();
		
		if (mysql_num_rows($result))
		{
			while($row = mysql_fetch_array($result)) 
	   		{
	       			$data[$row['login']] = $row['user_name'].' '.$row['user_surname'];
	       			$data[$row['login'].'_phone'] = $row['phone'];
	   		}
		}
		
		return $data;
	}
	
	public function getYears($statuses)
	{
		$query = 'SELECT DISTINCT DATE_FORMAT(add_date, \'%Y\') as year FROM `orders` WHERE orders.status IN (';
		
		foreach ($statuses as $status)
		{
			$query .= $status.',';
		}
		
		$query = substr($query, 0, -1).') ORDER BY id ASC';
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			return $this->_db->mysql_fetch_all($result);
		}
	}
	
	public function setTraderComment($id, $comment)
	{
		$this->_db->query('UPDATE order_details SET details = \''.trim(mysql_escape_string($this->textControll($comment))).'\' WHERE id = '.$id) or $this->_db->raise_error();
	}
	
	public function getTraderComment($id)
	{
		$result = $this->_db->query('SELECT id, details FROM order_details WHERE id = '.$id) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			return $row;
		}
	}
	
	public function getBudgets()
	{
		$result = $this->_db->query('SELECT id, department FROM wsk_budget ORDER BY department ASC') or $this->_db->raise_error();
		return $result;
	}
	
	public function getBudgetDetails($budget_id)
	{
		$query = 'SELECT wsk_budget_history.id, wsk_budget_history.year, users.user_name, users.user_surname, old_value, new_value, wsk_budget_history.amount, wsk_budget_history.info, wsk_budget_history.date FROM wsk_budget JOIN wsk_budget_history ON wsk_budget.department = wsk_budget_history.department JOIN users ON wsk_budget_history.user = users.id WHERE wsk_budget.id = '.$budget_id.' ORDER BY wsk_budget_history.date DESC';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
		
	public function checkDeliveryPoint($order_id)
	{
		$result = $this->_db->query('SELECT users.delivery_point FROM wsk_mpk JOIN users ON wsk_mpk.receiving = users.id WHERE delivery_point != \'\' AND wsk_mpk.order_id = '.$order_id) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			return $row['delivery_point'];
		}
		
		return null;
	}
	
	public function getCommunicats($order_id)
	{
		$result = $this->_db->query('SELECT `communicat`, `time` FROM order_communicats WHERE order_id = '.$order_id.' ORDER BY `time` DESC') or $this->_db->raise_error();
		return $result;
	}
	
	public function setRepresentative($id)
	{
		if ($this->_user['observer'] == 0)
		{
			$result = $this->_db->query('SELECT id FROM wsk_mpk WHERE order_id = '.$id.' AND unimet_representative = \'\'') or $this->_db->raise_error();
			
			if (mysql_num_rows($result))
			{
				$row = mysql_fetch_array($result);
				
				$this->_db->query('UPDATE wsk_mpk SET unimet_representative = \''.mysql_escape_string($this->_user['login']).'\' WHERE order_id = '.$id) or $this->_db->raise_error();
			}
		}
	}
	
	public function getOrders($statuses, $year = 0)
	{		
		$query = 'SELECT orders.id, order_replacements.id as replacement_id, ';
		
		//if (in_array(8, $statuses) || in_array(4, $statuses) || in_array(5, $statuses) || in_array(6, $statuses) || in_array(11, $statuses))
		//{
			$query .= 'IFNULL((SELECT query_id FROM order_details WHERE order_id = orders.id LIMIT 1), 0) as query_id, ';
		//}
		
		$query .= 'orders.add_date, users.merchant_id, users.login, users.name, users.user_name, users.user_surname, users.email, orders.brutto_value, orders.status, orders.payment, department, 
		wsk_mpk.unimet_representative, order_informations.delivery_date, wsk_mpk.mpk, order_hermes.hermes, DATEDIFF(IF(orders.update_date = \'0000-00-00\', current_date, orders.update_date), 
		orders.add_date) as date_diff FROM orders JOIN users ON orders.user_id = users.id LEFT JOIN 
		order_informations ON orders.id = order_informations.order_id LEFT JOIN wsk_mpk ON orders.id = wsk_mpk.order_id LEFT JOIN order_replacements ON orders.id = order_replacements.order_id LEFT JOIN wsk_budget ON wsk_mpk.budget = wsk_budget.id LEFT JOIN order_hermes ON orders.id = order_hermes.order_id WHERE orders.status IN (';
		
		foreach ($statuses as $status)
		{
			$query .= $status.',';
		}
		
		$query = substr($query, 0, -1).')';
		
		if ($year > 0)
		{
			$query .= ' AND DATE_FORMAT(orders.add_date, \'%Y\') = '.$year;
		}
		
		if ($this->_user['narrow_orders'] == 1)
		{
			$query .= ' AND users.merchant_id IN (SELECT merchant_id FROM user_details WHERE user_id = '.$this->_user['id'].') ';
		}

		$query .= ' ORDER BY orders.add_date DESC';

		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getOrder($order_id)
	{		
		$result = $this->_db->query('SELECT users.parent_id, order_details.query_id, order_details.unit, products.unit as default_unit, order_details.info as product_info, groups_tree.name as subgroup_name, 
		users.id_hermes, orders.id, number, orders.status, orders.comment as order_comment, wsk_budget.department, wsk_budget.budget as budget_value, pallet, 
		users.id as user_id, users.transport, users.name as user_name, users.post_code, users.city, users.street, users.home, users.flat, users_delivery.post_code 
		as delivery_post_code, users_delivery.city as delivery_city, users_delivery.street as delivery_street, users_delivery.home as delivery_home, 
		users_delivery.flat as delivery_flat, users.identificator, orders.add_date, orders.payment, orders.brutto_value, order_details.producer as producer_name, order_details.id as detail_id, order_details.name as product_name, 
		order_details.symbol, order_details.tax, order_details.description, order_details.amount, order_details.netto_price, order_details.brutto_price, document, 
		orders.comment, wsk_mpk.mpk, wsk_mpk.contractor, wsk_mpk.contractor_email, wsk_mpk.contractor_login, wsk_mpk.contractor_date, wsk_mpk.analyst, wsk_mpk.analyst_email, wsk_mpk.analyst_login, 
		wsk_mpk.analyst_date, wsk_mpk.analyst_deputy, wsk_mpk.administrator, wsk_mpk.administrator_email, wsk_mpk.administrator_login, wsk_mpk.administrator_date, wsk_mpk.manager, wsk_mpk.manager_email, wsk_mpk.manager_login, wsk_mpk.manager_date, wsk_mpk.manager_deputy, wsk_mpk.receiving, 
		wsk_mpk.receiving_email, wsk_mpk.receiving_login, wsk_mpk.receiving_date, wsk_mpk.receiving_deputy, wsk_mpk.analyst_login, wsk_mpk.manager_login,  
		wsk_mpk.unimet_representative, orders.netto_value, DATEDIFF(IF(orders.update_date = \'0000-00-00\', current_date, orders.update_date), 
		orders.add_date) as date_diff, order_informations.delivery_date, order_informations.fv, order_informations.wm, order_informations.reject_comment, (SELECT email FROM users WHERE login = 
		wsk_mpk.unimet_representative AND status = 0) as unimet_email, IFNULL(order_replacements.product_id, 0) as replacement_id, IFNULL(order_replacements.name, \'\') as 
		replacement_name FROM orders JOIN order_details ON orders.id = order_details.order_id JOIN users ON orders.user_id = users.id LEFT JOIN 
		order_replacements ON order_details.id = order_replacements.element_id LEFT JOIN order_informations ON orders.id = order_informations.order_id LEFT 
		JOIN products ON order_details.product_id = products.id_hermes LEFT JOIN groups_tree ON products.subgroup_id = groups_tree.id LEFT JOIN 
		users_delivery ON users.id = users_delivery.user_id LEFT JOIN wsk_mpk ON orders.id = wsk_mpk.order_id LEFT JOIN wsk_budget ON wsk_mpk.budget = 
		wsk_budget.id WHERE orders.id = '.$order_id) or $this->_db->raise_error();
		
		$data = $this->_db->mysql_fetch_all($result);
		
		if (empty($data[0]['post_code']))
		{
			$query = 'SELECT users.id, users.name, users.post_code, users.city, users.street, users.home, users.flat, users_delivery.post_code as delivery_post_code, users.identificator, 
			users_delivery.city as delivery_city, users_delivery.street as delivery_street, users_delivery.home as delivery_home, users_delivery.flat as 
			delivery_flat FROM users LEFT JOIN users_delivery ON users.id = users_delivery.user_id WHERE users.id = (SELECT IFNULL(parent_id, 0) FROM 
			user_tree WHERE user_id = '.$data[0]['user_id'].')';
			
			$result = $this->_db->query($query) or $this->_db->raise_error();
		
			if (mysql_num_rows($result))
			{
				$row = mysql_fetch_array($result);
				mysql_free_result($result);
				
				if (empty($row['post_code']))
				{
					$query = 'SELECT users.name, users.post_code, users.city, users.street, users.home, users.flat, users_delivery.post_code as delivery_post_code, users.identificator, 
					users_delivery.city as delivery_city, users_delivery.street as delivery_street, users_delivery.home as delivery_home, users_delivery.flat as 
					delivery_flat FROM users LEFT JOIN users_delivery ON users.id = users_delivery.user_id WHERE users.id = (SELECT IFNULL(parent_id, 0) FROM 
					user_tree WHERE user_id = '.$row['id'].')';
			
					$result = $this->_db->query($query) or $this->_db->raise_error();
					
					if (mysql_num_rows($result))
					{
						$row = mysql_fetch_array($result);
						mysql_free_result($result);
					}
				}
				
				foreach ($data as $key => $details)
				{
					$data[$key]['user_name'] = $row['name'];
					$data[$key]['name'] = $row['name'];
					$data[$key]['city'] = $row['city'];
					$data[$key]['post_code'] = $row['post_code'];
					$data[$key]['street'] = $row['street'];
					$data[$key]['home'] = $row['home'];
					$data[$key]['flat'] = $row['flat'];
					$data[$key]['delivery_post_code'] = $row['delivery_post_code'];
					$data[$key]['delivery_city'] = $row['delivery_city'];
					$data[$key]['delivery_street'] = $row['delivery_street'];
					$data[$key]['delivery_home'] = $row['delivery_home'];
					$data[$key]['delivery_flat'] = $row['delivery_flat'];
					$data[$key]['identificator'] = $row['identificator'];
				}
			}
		}
		
		return $data;
	}
	
	public function moveAllTorealization()
	{
		$result = $this->_db->query('UPDATE orders SET status = 2 WHERE status = 1') or $this->_db->raise_error();
		
		if ($result)
		{
			$communicats['ok'] = 'Zamówienia oczekujące zostały przeniesione do realizacji';
			$this->setSession("communicats", $communicats);
		}
		
		header("Location: ".BASE_ADDRESS.$this->_controller);
		die();
	}
	
	public function checkReplacements()
	{
		$list = $this->getOrders(array(1,8));
		
		if (is_array($list) && !empty($list))
		{
			foreach ($list as $order)
			{
				if ($order['status'] == 8 && $order['query_id'] == 0 && !in_array(date('N'), array(1,6,7)))
				{
					$this->_db->query('UPDATE orders SET status = 9 WHERE id = '.$order['id'].' AND DATEDIFF(\''.date('Y-m-d').'\', add_date) > 0') or $this->_db->raise_error();
				}
			}
		}
	}
	
	public function changeStatus($order_id, $status)
	{
		$this->_secretService($order_id, $status);
		
		$controll = $this->_db->query('SELECT status FROM orders WHERE id = '.$order_id.' AND status NOT IN (8)') or $this->_db->raise_error();	
	
		$result = $this->_db->query('UPDATE orders SET status = '.$status.', update_date = \''.date('Y-m-d').'\' WHERE id = '.$order_id) or $this->_db->raise_error();
		
		if ($status == 5)
		{
			$data = $this->_db->query('SELECT product_id, amount FROM order_details WHERE order_id = '.$order_id) or $this->_db->raise_error();
			
			while($element = mysql_fetch_array($data)) 
	   		{
	       			$this->_db->query('UPDATE products SET sold_amount = `sold_amount` - '.$element['amount'].' WHERE id_hermes = '.$element['product_id']) or $this->_db->raise_error();
	   		}
	   		
	   		if (mysql_num_rows($controll))
	   		{
	   			$this->_releaseBudget($order_id);
	   		}
		}
		
		if (in_array($status, array(3,5,6)))
		{
			$this->_saveOrderChangeMessage($order_id, "zmianie statusu zamówienia");
		}
		
		if ($result)
		{
			$communicats['ok'] = 'Status zamówienia został zmieniony';
			$this->setSession("communicats", $communicats);
		}
		
		header("Location: ".BASE_ADDRESS.$this->_controller);
		die();
	}
	
	private function _saveOrderChangeMessage($id, $title)
	{
		$list = $this->getOrder($id);
		
		$mimemail = new nomad_mimemail(); 
		$emails = array();
		
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><img src="'.BASE_ADDRESS.'../images/logotype_small.png" alt="" /></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">&nbsp;</p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><strong>Informacja o '.$title.'.</strong></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">&nbsp;</p>';
		
		switch ($list[0]['status'])
		{
			case 3:
				array_push($emails, $list[0]['receiving_email']);
				
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$this->formatId($list[0]['id'], $list[0]['query_id']).'</strong>, które 
				zostało złożone w dniu <strong>'.$list[0]['menager_date'].'</strong> zostało wysłane.</p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Aby przejść do zamówienia zaloguj się do <a href="http://wsk.unimet.pl">www.wsk.unimet.pl</a></p>';
				break;
				
			case 5:
				array_push($emails, $list[0]['contractor_email']);
				
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$this->formatId($list[0]['id'], $list[0]['query_id']).'</strong>, które 
				zostało złożone w dniu <strong>'.$list[0]['menager_date'].'</strong> zostało odrzucone.</p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">&nbsp;</p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><strong>Komentarz osoby odrzucającej</strong></p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['reject_comment'].'</p>';
				break;
				
			case 6:
				array_push($emails, $list[0]['analyst_email']);
				
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$this->formatId($list[0]['id'], $list[0]['query_id']).'</strong>, które 
				zostało złożone w dniu <strong>'.$list[0]['contractor_date'].'</strong> oczekuje na Twoją reakcję.</p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Aby przejść do zamówienia zaloguj się do <a href="http://wsk.unimet.pl">wsk.unimet.pl</a></p>';
				break;
		}
		
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">&nbsp;</p>';
		$message .= '<table cellspacing="0" cellpadding="0">';
		$message .= '<tr>';
			$message .= '<td style="border:'.((in_array($list[0]['status'], array(4,5))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">Zamawiający</span></p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['contractor_login'].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['contractor_email'].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($list[0]['contractor_date'] != "1970-01-01" && !empty($list[0]['contractor_date'])) ? $list[0]['contractor_date'] : "").'</p>';
			$message .= '</td>';
			
			if ($list[0]['query_id'] == 0) 
			{
				$message .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
				$message .= '<td style="border:1px solid #7597c5; text-align:center; padding:5px">';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">UNIMET - propozycja zamiennika</span></p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.((!empty($list[0]['unimet_representative'])) ? $user_names[$list[0]['unimet_representative']] : 'brak danych').'</p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['unimet_email'].'</p>';
				$message .= '</td>';
			}
			
			$message .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
			$message .= '<td style="border:'.((in_array($list[0]['status'], array(6))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">Analityk</span></p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['analyst_login'].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['analyst_email'].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($list[0]['analyst_date'] != "1970-01-01" && !empty($list[0]['analyst_date'])) ? $list[0]['analyst_date'] : "").'</p>';
			$message .= '</td>';
			$message .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
			
			if ($list[0]['query_id'] > 0)
			{
				$message .= '<td style="border:'.((in_array($list[0]['status'], array(10))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">Aministrator</span></p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['administrator_login'].'</p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['administrator_email'].'</p>';
				$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($list[0]['administrator_date'] != "1970-01-01" && !empty($list[0]['administrator_date'])) ? $list[0]['administrator_date'] : "").'</p>';
				$message .= '</td>';
				$message .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
			}
			
			$message .= '<td style="border:'.((in_array($list[0]['status'], array(7))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">Kierownik</span></p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['manager_login'].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['manager_email'].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($list[0]['manager_date'] != "1970-01-01" && !empty($list[0]['manager_date'])) ? $list[0]['manager_date'] : "").'</p>';
			$message .= '</td>';
			$message .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
			$message .= '<td style="border:'.((in_array($list[0]['status'], array(1))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">UNIMET</span></p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.((!empty($list[0]['unimet_representative'])) ? $list[0]['unimet_representative'] : 'brak danych').'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['unimet_email'].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($list[0]['delivery_date'] != "1970-01-01" && !empty($list[0]['delivery_date'])) ? $list[0]['delivery_date'] : "").'</p>';
			$message .= '</td>';
			$message .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
			$message .= '<td style="border:'.((in_array($list[0]['status'], array(4))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">Odbierający</span></p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['receiving_login'].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$list[0]['receiving_email'].'</p>';
			$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($list[0]['receiving_date'] != "1970-01-01" && !empty($list[0]['receiving_date'])) ? $list[0]['receiving_date'] : "").'</p>';
			$message .= '</td>';
		$message .= '</tr>';
		$message .= '</table>';
		
		$message .= '<div style="padding-top: 20px;">';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Wydział: <strong>'.$list[0]['department'].'</strong></p>';	
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Data złożenia zamówienia: <strong>'.$list[0]['add_date'].'</strong></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Data dostawy: <strong>'.(($list[0]['delivery_date'] != "1970-01-01" && !empty($list[0]['delivery_date'])) ? $list[0]['delivery_date'] : "brak danych").'</strong></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Forma płatności: <strong>'.$list[0]['payment'].'</strong></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Numer MPK: <strong>'.$list[0]['mpk'].'</strong></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Faktura VAT <strong>'.((!empty($list[0]['fv'])) ? $list[0]['fv'] : 'brak danych').'</strong></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">WZ: <strong>'.((!empty($list[0]['wm'])) ? $list[0]['wm'] : 'brak danych').'</strong></p>';
		$message .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Dokument <strong>'.$list[0]['document'].'</strong></p>';
		$message .= '</div>';
		
		$from = "newsletter@unimet.pl";	 
		$html = '<html xmlns="http://www.w3.org/1999/xhtml">';
		$html .= '<head>';
		$html .= '</head>';
		$html .= '<body>'.iconv("UTF-8", "ISO-8859-2", $message).'</body></html>';
		$subject = 'b2b.unimet.pl - informacja o '.$title;
		
   		$mimemail->set_smtp_log(true); 
		$smtp_host	= "swarog.az.pl";
		$smtp_user	= "newsletter@unimet.pl";
		$smtp_pass	= "1hadesa2madmax";
		$mimemail->set_smtp_host($smtp_host);
		$mimemail->set_smtp_auth($smtp_user, $smtp_pass); 
		
		foreach ($emails as $email)
		{
			if ($this->validateEmail($email))
			{
				$mimemail->new_mail($from, $email, iconv("UTF-8", "ISO-8859-2", $subject), "", $html);
				$mimemail->send(); 	
			}
		}
	}
	
	public function getOrderInfo($id)
	{
		$result = $this->_db->query('SELECT fv, wm, delivery_date, wz_document, information FROM order_informations LEFT JOIN order_comments ON order_informations.order_id = order_comments.order_id WHERE order_informations.order_id = '.$id) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			
			$values['fv'] = $row['fv'];
			$values['wm'] = $row['wm'];
			$values['file'] = $row['wz_document'];
			$values['information'] = $this->textClear($row['information']);
			$values['delivery_date'] = (($row['delivery_date'] != "1970-01-01") ? $row['delivery_date'] : "");
		}
		
		$this->setSession("values", $values);
	}

	public function validateInformations()
	{
		$this->clear();
		
		$this->_order_id = (int)$_POST['order_id'];
		$this->_delivery_date = $values['delivery_date'] = trim(mysql_escape_string($_POST['delivery_date']));
		$this->_fv = $values['fv'] = trim(mysql_escape_string($_POST['fv']));
		$this->_wm = $values['wm'] = trim(mysql_escape_string($_POST['wm']));
		$this->_information = $values['information'] = trim(mysql_escape_string($this->textControll($_POST['information'])));
		$this->_operation = $_POST['operation'];
		$this->_names = $_POST["names"];
		$this->_symbols = $_POST["symbols"];
		$this->_producers = $_POST["producers"];
		$this->_units = $_POST["units"];
		$this->_taxes = $_POST["taxes"];
		$this->_replacements = $_POST["replacements"];
		$this->_amounts = $_POST["amounts"];
		$this->_ids = $_POST["ids"];
		$this->_prices = $_POST["prices"];
		$this->_elements = $_POST["elements"];
		$this->_details = $_POST["details"];
		$this->_file = $_FILES["file"];
		
		if (!empty($this->_delivery_date))
		{
			$date = explode('-', $this->_delivery_date);
			
			if (count($date) != 3)
			{
				$this->_errors++;
				$errors['delivery_date'] = 'Data jest niepoprawna';
			}
			else if (!$this->validateDate((int)$date[1], (int)$date[2], (int)$date[0]))
			{
				$this->_errors++;
				$errors['delivery_date'] = 'Data jest niepoprawna';
			}
			/*
			if ((int)date('U', strtotime($this->_delivery_date)) < (int)date('U', strtotime((string)date('Y-m-d'))))
			{
				$this->_errors++;
				$errors['delivery_date'] = 'Data jest niepoprawna';
			}
			*/
		}
		
		if ($this->_file['size'] > 10129932)
		{
			$this->_errors++;
			$errors['file'] = 'Rozmiar pliku nie może być większy niż 10 MB';
		}
		
		foreach ($this->_names as $name)
		{
			if (empty($name))
			{
				$this->_errors++;
				$errors['names'] = 'Nazwy produktów są wymagane';
			}
		}
		
		foreach ($this->_producers as $producer)
		{
			if (empty($producer))
			{
				$this->_errors++;
				$errors['producers'] = 'Producenci są wymagani';
			}
		}

		foreach ($this->_taxes as $tax)
		{
			if ((int)$tax <= 0)
			{
				$this->_errors++;
				$errors['taxes'] = 'Wartość VAT jest wymagana';
			}
		}
		
		foreach ($this->_prices as $price)
		{
			if ((float)sprintf("%0.2f", str_replace(',', '.', $price)) <= 0)
			{
				$this->_errors++;
				$errors['prices'] = 'Ceny są wymagane';
			}
		}

		if (!$this->_errors)
		{	
			$this->_setReplacements();
			
			if (is_array($this->_ids) && !empty($this->_ids) && is_array($this->_amounts) && !empty($this->_amounts) && is_array($this->_prices) && !empty($this->_prices) && is_array($this->_taxes) && !empty($this->_taxes))
			{
				if (count($this->_ids) == count($this->_amounts) && count($this->_amounts) == count($this->_prices))
				{
					$this->_changeProductAmounts();
				}
			}
			
			if (empty($this->_delivery_date))
			{
				$this->_delivery_date = '1970-01-01';
			}
			
			$result = $this->_db->query('SELECT id FROM order_informations WHERE order_id = '.$this->_order_id) or $this->_db->raise_error();
			
			if (mysql_num_rows($result))
			{
				$query = 'UPDATE order_informations SET fv = \''.mysql_escape_string($this->_fv).'\', wm = \''.mysql_escape_string($this->_wm).'\', 
				delivery_date = \''.mysql_escape_string($this->_delivery_date).'\' WHERE order_id = '.$this->_order_id;
			}
			else
			{
				$query = 'INSERT INTO order_informations VALUES (DEFAULT, '.$this->_order_id.', \''.mysql_escape_string($this->_fv).'\', 
				\''.mysql_escape_string($this->_wm).'\', \'\', \''.mysql_escape_string($this->_delivery_date).'\', \'\')';
			}
			
			$this->_db->query($query) or $this->_db->raise_error();
			
			$result = $this->_db->query('SELECT id FROM order_comments WHERE order_id = '.$this->_order_id) or $this->_db->raise_error();
			
			if (mysql_num_rows($result))
			{
				$query = 'UPDATE order_comments SET information = \''.$this->_information.'\' WHERE order_id = '.$this->_order_id;
			}
			else
			{
				$query = 'INSERT INTO order_comments VALUES (DEFAULT, '.$this->_order_id.', \''.$this->_information.'\')';
			}
			
			$this->_db->query($query) or $this->_db->raise_error();
			
			if (!empty($this->_file['name']))
			{
				if (is_uploaded_file($this->_file['tmp_name']))
				{
					$result = $this->_db->query('SELECT wz_document FROM order_informations WHERE order_id = '.$this->_order_id) or $this->_db->raise_error();
					$row = mysql_fetch_array($result);
					
					if (!empty($row['wz_document']) || is_file(BASE_DIR.'../../images/wz/'.$row['wz_document']))
					{
						unlink(BASE_DIR.'../../images/wz/'.$row['wz_document']);
					}
					
					$file_name = 'zamowienie_nr_'.$this->_order_id.'_'.$this->validateName($this->_file['name']);
					$path = BASE_DIR.'../../images/wz/'.$file_name;
					
					move_uploaded_file($this->_file['tmp_name'], $path);
					
					if (file_exists($path))
					{
						$this->_db->query('UPDATE order_informations SET wz_document = \''.mysql_escape_string($file_name).'\' WHERE order_id = '.$this->_order_id) 
						or $this->_db->raise_error();
					}
				}
			}

			if ($this->_operation == "move_to_archive")
			{
				$this->_secretService($this->_order_id, 4);
				$result = $this->_db->query('UPDATE orders SET status = 4, update_date = \''.date('Y-m-d').'\' WHERE id = '.$this->_order_id) or $this->_db->raise_error();
			}
			else if ($this->_operation == "regroup_order")
			{			
				$this->_regroupOrder();
			}
			else if ($this->_operation == "return_products")
			{
				$this->_returnProducts();
			}
			
			$ommunicats['ok'] = 'Dane zostały zapisane';
			$this->setSession("communicats", $communicats);
			header("Location: ../../".$this->_controller."/list");
		}
		else 
		{

			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
	}
	
	private function _releaseBudget($order_id)
	{
		$query = 'SELECT orders.status, orders.add_date, order_details.id, order_details.netto_price, order_details.brutto_price, order_details.tax, order_details.amount, 
		IFNULL(wsk_budget.id, 0) as budget_id, wsk_budget.department, wsk_budget.budget, 
		IFNULL(wsk_budget_2012.id, 0) as budget_id_2012, wsk_budget_2012.department as department_2012, 0 as budget_2012, 
		IFNULL(wsk_budget_2013.id, 0) as budget_id_2013, wsk_budget_2013.department as department_2013, 0 as budget_2013, 
		IFNULL(wsk_budget_2014.id, 0) as budget_id_2014, wsk_budget_2014.department as department_2014, wsk_budget_2014.budget as budget_2014, 
		IFNULL(wsk_budget_2015.id, 0) as budget_id_2015, wsk_budget_2015.department as department_2015, wsk_budget_2015.budget as budget_2015   
		FROM orders JOIN order_details ON orders.id = order_details.order_id 
		LEFT JOIN wsk_mpk ON order_details.order_id = wsk_mpk.order_id 
		LEFT JOIN wsk_budget ON wsk_mpk.budget = wsk_budget.id 
		LEFT JOIN wsk_budget_2012 ON wsk_mpk.budget = wsk_budget_2012.id 
		LEFT JOIN wsk_budget_2013 ON wsk_mpk.budget = wsk_budget_2013.id 
		LEFT JOIN wsk_budget_2014 ON wsk_mpk.budget = wsk_budget_2014.id 
		LEFT JOIN wsk_budget_2015 ON wsk_mpk.budget = wsk_budget_2015.id
		WHERE order_details.order_id = '.$order_id;

		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		$netto_value = 0;
		$brutto_value = 0;
		
		while($row = mysql_fetch_array($result)) 
		{
			$year_postfix = ((date('Y', strtotime($row['add_date'])) == date('Y')) ? '' : '_'. date('Y', strtotime($row['add_date'])));
			$order_day = date('Y-m-d', strtotime($row['add_date']));
			$order_year = (int)date('Y', strtotime($row['add_date']));

			$budget_id = (int)$row['budget_id' . $year_postfix];
			$department = $row['department' . $year_postfix];
			$budget_value = $row['budget' . $year_postfix];

			$status = $row['status'];
			$tax = ((int)$row['tax'] * $row['netto_price'])/100;
			
			$netto_sum += ($row['netto_price'] * $row['amount']); 
			$brutto_sum += (($row['netto_price'] + $tax) * $row['amount']); 
		}
		
		if ($netto_sum > 0 && (int)$budget_id > 0 && !in_array((int)$status, array(8)))
		{
			$query = 'INSERT INTO wsk_budget_history VALUES (\'\', \''.mysql_escape_string($department).'\', '.$this->_user['id'].', 
			\''.mysql_escape_string($this->_user['login']).'\', \''.$budget_value.'\', \''.($budget_value + $netto_sum).'\', 
			\''.($netto_sum).'\', '.$order_id.', \'korekta budżetu\', \''.date('Y-m-d H:i:s').'\', \''.$order_year.'\')';
			$this->_db->query($query) or $this->_db->raise_error();
							
			$query = "UPDATE wsk_budget" . $year_postfix . " SET `used_budget` = `used_budget` - ('" . $netto_sum . "'), 
			`budget` = `budget` + ('" . $netto_sum . "') WHERE id = " . $budget_id;	

			$this->_db->query($query) or $this->_db->raise_error();
		}
	}
	
	private function _returnProducts()
	{
		$controll = $this->_db->query('SELECT status FROM orders WHERE id = '.$this->_order_id.' AND status NOT IN (8)') or $this->_db->raise_error();	
	
		if (mysql_num_rows($controll))
		{
			$this->_releaseBudget($this->_order_id);
		}
		
		$query = 'UPDATE orders SET status = 20 WHERE id = '.$this->_order_id;
		$this->_db->query($query) or $this->_db->raise_error();
	}
	
	private function _regroupOrder()
	{
		$controll = false;
		
		if (is_array($this->_elements))
		{
			foreach ($this->_elements as $id)
			{
				$amount = (float)str_replace(',', '.', $this->_details[$id]);
				
				if ($amount > 0)
				{
					$result = $this->_db->query('SELECT id FROM order_details WHERE id = '.(int)$id.' AND order_id = '.$this->_order_id.' AND amount >= \''.$amount.'\'') or $this->_db->raise_error();
				
					if (mysql_num_rows($result))
					{
						$controll = true;
					}
					
					mysql_free_result($result);
				}
			}
		}
		
		if (true == $controll)
		{
			$query = 'INSERT INTO orders (id, number, user_id, netto_value, brutto_value, status, document, payment, comment, add_date, update_date) SELECT 
			\'\' as id, number, user_id, 0, 0, status, document, payment, comment, add_date, update_date FROM orders WHERE id = '.$this->_order_id;
			
			$this->_db->query($query) or $this->_db->raise_error();
			$order_id = mysql_insert_id();
			
			$query = 'INSERT INTO wsk_mpk (id, order_id, mpk, budget, price_netto, contractor, contractor_email, contractor_login, contractor_date, 
			contractor_deputy, analyst, analyst_email, analyst_login, analyst_date, analyst_deputy, manager, manager_email, manager_login, manager_date, 
			manager_deputy, receiving, receiving_email, receiving_login, receiving_date, receiving_deputy, unimet_representative) SELECT \'\' as id, '.$order_id.' 
			as order_id, mpk, budget, 0 as price_netto, contractor, contractor_email, contractor_login, contractor_date, contractor_deputy, analyst, analyst_email, 
			analyst_login, analyst_date, analyst_deputy, manager, manager_email, manager_login, manager_date, manager_deputy, receiving, receiving_email, 
			receiving_login, receiving_date, receiving_deputy, unimet_representative FROM wsk_mpk WHERE order_id = '.$this->_order_id;
			
			$this->_db->query($query) or $this->_db->raise_error();
			$wsk_mpk_id = mysql_insert_id();
			
			foreach ($this->_elements as $id)
			{
				$amount = (float)str_replace(',', '.', $this->_details[$id]);
				
				if ($amount > 0)
				{
					$result = $this->_db->query('SELECT id, amount FROM order_details WHERE id = '.(int)$id.' AND order_id = '.$this->_order_id.' AND amount >= \''.$amount.'\'') or $this->_db->raise_error();
				
					if (mysql_num_rows($result))
					{
						$row = mysql_fetch_array($result);
						
						if ($amount == $row['amount'])
						{
							$this->_db->query('UPDATE order_details SET order_id = '.$order_id.' WHERE id = '.$row['id']) or $this->_db->raise_error();
						}
						else
						{
							$query = 'INSERT INTO order_details (id, order_id, product_id, name, producer, symbol, unit, description, netto_price, tax, 
							brutto_price, amount, supplier_id, mpk, info, query_id, `details`, editability, availability) SELECT \'\' as id, '.$order_id.' 
							as order_id, product_id, name, producer, symbol, unit, description, netto_price, tax, brutto_price, \''.$amount.'\' as amount, 
							supplier_id, mpk, info, query_id, details, 1 as editability, 1 as availability FROM order_details WHERE id = '.$row['id'];
							
							$this->_db->query($query) or $this->_db->raise_error();
							
							$this->_db->query('UPDATE order_details SET `amount` = `amount` - \''.$amount.'\' WHERE id = '.$row['id']) or $this->_db->raise_error();
						}
					}
					
					mysql_free_result($result);
				}
			}
			
			$query = 'SELECT order_details.id, order_details.netto_price, order_details.brutto_price, order_details.tax, order_details.amount, IFNULL(wsk_budget.id, 
			0) as budget_id, wsk_budget.budget, wsk_budget.department FROM order_details LEFT JOIN wsk_mpk ON order_details.order_id = wsk_mpk.order_id LEFT JOIN 
			wsk_budget ON wsk_mpk.budget = wsk_budget.id WHERE order_details.order_id = '.$this->_order_id;
			$result = $this->_db->query($query) or $this->_db->raise_error();
			
			$netto_value = 0;
			$brutto_value = 0;
			
			while($row = mysql_fetch_array($result)) 
			{
				$tax = ((int)$row['tax'] * $row['netto_price'])/100;
				$netto_value += ($row['netto_price'] * $row['amount']); 
				$brutto_value += (($row['netto_price'] + $tax) * $row['amount']); 
			}
			
			mysql_free_result($result);
			
			$query = 'UPDATE orders SET netto_value = \''.$netto_value.'\', brutto_value = \''.$brutto_value.'\' WHERE id = '.$this->_order_id;
			$this->_db->query($query) or $this->_db->raise_error();
				
			$query = 'UPDATE wsk_mpk SET price_netto = \''.$netto_value.'\' WHERE order_id = '.$this->_order_id;
			$this->_db->query($query) or $this->_db->raise_error();
			
			$query = 'SELECT order_details.id, order_details.netto_price, order_details.brutto_price, order_details.tax, order_details.amount, IFNULL(wsk_budget.id, 
			0) as budget_id, wsk_budget.budget, wsk_budget.department FROM order_details LEFT JOIN wsk_mpk ON order_details.order_id = wsk_mpk.order_id LEFT JOIN 
			wsk_budget ON wsk_mpk.budget = wsk_budget.id WHERE order_details.order_id = '.$order_id;
			$result = $this->_db->query($query) or $this->_db->raise_error();
			
			$netto_value = 0;
			$brutto_value = 0;
			
			while($row = mysql_fetch_array($result)) 
			{
				$tax = ((int)$row['tax']*$row['netto_price'])/100;
				$netto_value += ($row['netto_price'] * $row['amount']); 
				$brutto_value += (($row['netto_price'] + $tax) * $row['amount']); 
			}
			
			mysql_free_result($result);
			
			$query = 'UPDATE orders SET netto_value = \''.$netto_value.'\', brutto_value = \''.$brutto_value.'\' WHERE id = '.$order_id;
			$this->_db->query($query) or $this->_db->raise_error();
				
			$query = 'UPDATE wsk_mpk SET price_netto = \''.$netto_value.'\' WHERE order_id = '.$order_id;
			$this->_db->query($query) or $this->_db->raise_error();
		}
	}
	
	private function _setReplacements()
	{
		$this->_db->query('DELETE FROM order_replacements WHERE order_id = '.$this->_order_id) or $this->_db->raise_error();
		
		foreach ($this->_ids as $key => $detail_id)
		{
			if ($this->_replacements[$key] > 0)
			{
				$result = $this->_db->query('SELECT product_versions.name, products.symbol FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id WHERE products.id_hermes = '.(int)$this->_replacements[$key]) or $this->_db->raise_error();
			
				if (mysql_num_rows($result))
				{
					$row = mysql_fetch_array($result);
					
					$query = 'INSERT INTO order_replacements VALUES (\'\', '.$this->_order_id.', '.$detail_id.', '.(int)$this->_replacements[$key].', 
					\''.trim(mysql_escape_string($row['name'])).'\', \''.trim(mysql_escape_string($row['symbol'])).'\')';
					$this->_db->query($query) or $this->_db->raise_error();
				}
				
				mysql_free_result($result);
			}
		}
	}
	
	private function _changeProductAmounts()
	{
		$query = 'SELECT id, name, symbol, netto_price, brutto_price, tax, amount FROM order_details WHERE order_id = '.$this->_order_id;
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		$old_sum = 0;
		$data = array();
				
		while($row = mysql_fetch_array($result)) 
		{
			$old_sum += ($row['netto_price'] * $row['amount']); 
			$data["taxes"][$row['id']] = $row['tax'];
			$data["amounts"][$row['id']] = $row['amount'];
			$data["names"][$row['id']] = $row['name'];
			$data["symbols"][$row['id']] = $row['symbol'];
		}
		
		mysql_free_result($result);
		
		if (is_array($this->_units) && !empty($this->_units))
		{
			foreach ($this->_ids as $key => $id)
			{
				if ((int)$this->_units[$key] > 0)
				{
					$this->_db->query('UPDATE order_details SET unit = '.(int)$this->_units[$key].' WHERE id = '.$id.' AND order_id = '.$this->_order_id) or $this->_db->raise_error();
				}
			}
		}
		
		foreach ($this->_ids as $key => $id)
		{
			$netto_price = sprintf("%0.2f", str_replace(',', '.', $this->_prices[$key]));
			$amount = (float)str_replace(',', '.', $this->_amounts[$key]);
			$tax = (int)$this->_taxes[$key];
			$name = $this->_names[$key];
			$symbol = $this->_symbols[$key];
			$producer = $this->_producers[$key];
			
			if ($netto_price > 0 && $amount >= 0)
			{
				$query = 'UPDATE order_details SET netto_price = \''.$netto_price.'\', tax = \''.$tax.'\', name = \''.mysql_escape_string($name).'\', producer = \''.mysql_escape_string($producer).'\', symbol = \''.mysql_escape_string($symbol).'\', brutto_price = \''.sprintf("%0.2f", ($netto_price + ($tax * $netto_price)/100)).'\', 
				amount = \''.$amount.'\' WHERE id = '.$id.' AND order_id = '.$this->_order_id;
				
				$this->_db->query($query) or $this->_db->raise_error();
				
				if ((float)$amount != (float)$data["amounts"][$id])
				{
					$query = 'INSERT INTO order_communicats VALUES (\'\', '.$this->_order_id.', \''.mysql_escape_string('użytkownik o loginie 
					<strong>'.$this->_user['login'].'</strong> zmienił ilość produktów o nazwie "'.$data["names"][$id].'" (<strong>'.$data["symbols"][$id].'</strong>) 
					z <strong>'.(((int)$data["amounts"][$id] == (float)$data["amounts"][$id]) ? (int)$data["amounts"][$id] : $data["amounts"][$id]).'</strong> 
					na <strong>'.(((int)$amount == (float)$amount) ? (int)$amount : $amount).'</strong>').'\', '.$this->_user['id'].', \''.date("Y-m-d H:i:s").'\')';
					
					$this->_db->query($query) or $this->_db->raise_error();
				}
			}
		}
		
		$query = 'SELECT orders.status, orders.add_date, order_details.id, order_details.netto_price, order_details.brutto_price, order_details.tax, order_details.amount, 
		IFNULL(wsk_budget.id, 0) as budget_id, wsk_budget.department, wsk_budget.budget, 
		IFNULL(wsk_budget_2012.id, 0) as budget_id_2012, wsk_budget_2012.department as department_2012, 0 as budget_2012, 
		IFNULL(wsk_budget_2013.id, 0) as budget_id_2013, wsk_budget_2013.department as department_2013, 0 as budget_2013, 
		IFNULL(wsk_budget_2014.id, 0) as budget_id_2014, wsk_budget_2014.department as department_2014, wsk_budget_2014.budget as budget_2014, 
		IFNULL(wsk_budget_2015.id, 0) as budget_id_2015, wsk_budget_2015.department as department_2015, wsk_budget_2015.budget as budget_2015   
		FROM orders JOIN order_details ON orders.id = order_details.order_id 
		LEFT JOIN wsk_mpk ON order_details.order_id = wsk_mpk.order_id 
		LEFT JOIN wsk_budget ON wsk_mpk.budget = wsk_budget.id 
		LEFT JOIN wsk_budget_2012 ON wsk_mpk.budget = wsk_budget_2012.id 
		LEFT JOIN wsk_budget_2013 ON wsk_mpk.budget = wsk_budget_2013.id 
		LEFT JOIN wsk_budget_2014 ON wsk_mpk.budget = wsk_budget_2014.id  
		LEFT JOIN wsk_budget_2015 ON wsk_mpk.budget = wsk_budget_2015.id 
		WHERE order_details.order_id = '.$this->_order_id;
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		$netto_value = 0;
		$brutto_value = 0;
		
		while($row = mysql_fetch_array($result)) 
		{
			$year_postfix = ((date('Y', strtotime($row['add_date'])) == date('Y')) ? '' : '_'. date('Y', strtotime($row['add_date'])));
			$order_day = date('Y-m-d', strtotime($row['add_date']));
			$order_year = (int)date('Y', strtotime($row['add_date']));

			$budget_id = (int)$row['budget_id' . $year_postfix];
			$department = $row['department' . $year_postfix];
			$budget_value = $row['budget' . $year_postfix];

			$status = $row['status'];
			$tax = ((int)$row['tax']*$row['netto_price'])/100;
			
			$netto_sum += ($row['netto_price'] * $row['amount']); 
			$brutto_sum += (($row['netto_price'] + $tax) * $row['amount']); 
		}
		
		$query = 'UPDATE orders SET netto_value = \''.$netto_sum.'\', brutto_value = \''.$brutto_sum.'\' WHERE id = '.$this->_order_id;
		$this->_db->query($query) or $this->_db->raise_error();
			
		$query = 'UPDATE wsk_mpk SET price_netto = \''.$netto_sum.'\' WHERE order_id = '.$this->_order_id;
		$this->_db->query($query) or $this->_db->raise_error();

		if ($old_sum - $netto_sum != 0 && (int)$budget_id > 0 && !in_array((int)$status, array(8)))
		{
			$query = 'INSERT INTO wsk_budget_history VALUES (\'\', \''.mysql_escape_string($department).'\', '.$this->_user['id'].', 
			\''.mysql_escape_string($this->_user['login']).'\', \''.$budget_value.'\', \''.($budget_value + ($old_sum - $netto_sum)).'\', 
			\''.($old_sum - $netto_sum).'\', '.$this->_order_id.', \'korekta budżetu\', \''.date('Y-m-d H:i:s').'\', \''.$order_year.'\')';
			$this->_db->query($query) or $this->_db->raise_error();
							
			$query = "UPDATE wsk_budget" . $year_postfix . " SET `used_budget` = `used_budget` - ('" . ($old_sum - $netto_sum) . "'), 
			`budget` = `budget` + ('" . ($old_sum - $netto_sum) . "') WHERE id = " . $budget_id;	
			$this->_db->query($query) or $this->_db->raise_error();
		}
	}
	
	public function findProducts($key)
	{
		$query = 'SELECT product_versions.name, products.symbol FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id WHERE 
		products.symbol LIKE \'%'.trim(mysql_escape_string($key)).'%\' AND producer_id > 0 AND subgroup_id > 0';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();	
		return $result;
	}
	
	public function getProduct($value)
	{
		$query = 'SELECT id_hermes, product_versions.name FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id WHERE 
		concat(products.symbol, \' (\', product_versions.name, \')\') = \''.mysql_escape_string($value).'\' AND producer_id > 0 AND subgroup_id > 0';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();	
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			return $row;
		}
	}
	
	private function _secretService($order_id, $status)
	{
		$result = $this->_db->query('SELECT status FROM orders WHERE id = '.$order_id) or $this->_db->raise_error();	
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			mysql_free_result($result);
			
			$query = 'INSERT INTO secret_service VALUES (\'\', '.$order_id.', '.$row['status'].', '.$status.', '.$this->_user['id'].', \''.date('Y-m-d H:i:s').'\')';
			$this->_db->query($query) or $this->_db->raise_error();	
		}
	}
	
	public function changeHermesSettings($order_id, $status)
	{
		$query = 'INSERT INTO order_hermes (`order_id`, `hermes`) VALUES ('.$order_id.', '.$status.') ON DUPLICATE KEY UPDATE hermes = '.$status;
		$this->_db->query($query) or $this->_db->raise_error();	
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}
?>
<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<label style="width:100px;">&nbsp;</label>
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
	<div class="clear"></div>
</div>
<?php } ?>

<?php
$order_id = (int)$_GET['id']; 

if (!isset($_POST['save_changes']) && in_array($action, array("edit","show")))
{
	$orders->getOrderInfo($order_id);
	$information = $_SESSION["values"]['information'];
}
?>

<form method="POST" action="" name="order" enctype="multipart/form-data">
<input type="hidden" name="order_id" value="<?php echo $order_id; ?>" />

<?php if (!in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>	
	<?php if ($_SESSION["errors"]['delivery_date']) { ?>
	<div class="form_element error">
		<label style="width:100px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['delivery_date']; ?>
		<div class="clear"></div>
	</div>
	<?php } ?>
	<div class="form_element">
		<label style="width:100px;">Data dostawy</label>
		<input type="text" name="delivery_date" class="date" value="<?php echo $orders->textControll($_SESSION["values"]['delivery_date']); ?>" />
		<div class="clear"></div>
	</div>
		
	<?php if ($_SESSION["errors"]['fv']) { ?>
	<div class="form_element error">
		<label style="width:100px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['fv']; ?>
		<div class="clear"></div>
	</div>
	<?php } ?>
	<div class="form_element">
		<label style="width:100px;">FV</label>
		<input type="text" name="fv" value="<?php echo $orders->textControll($_SESSION["values"]['fv']); ?>" />
		<div class="clear"></div>
	</div>
		
	<?php if ($_SESSION["errors"]['wm']) { ?>
	<div class="form_element error">
		<label style="width:100px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['wm']; ?>
		<div class="clear"></div>
	</div>
	<?php } ?>
	<div class="form_element">
		<label style="width:100px;">WZ</label>
		<input type="text" name="wm" value="<?php echo $orders->textControll($_SESSION["values"]['wm']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php if ($_SESSION["values"]['file']) { ?>
	<div class="form_element">
		<label style="width:100px;">&nbsp;</label>
		<a target="blank" href="<?php echo BASE_ADDRESS; ?>../images/wz/<?php echo $_SESSION["values"]['file']; ?>"><?php echo $_SESSION["values"]['file']; ?></a>
		<div class="clear"></div>
	</div>
	<?php } if ($_SESSION["errors"]['file']) { ?>
	<div class="form_element error">
		<label style="width:100px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['file']; ?>
		<div class="clear"></div>
	</div>
	<?php } ?>
	<div class="form_element">
		<label style="width:100px;">WZ - dokument</label>
		<input style="width:200px;" name="file" type="file" />
		<div class="clear"></div>
	</div>
	
	<div class="form_element">
		<label style="width:100px;">Komentarz handlowca</label>
		<textarea name="information" cols="0" rows="0" style="width:400px; height:100px;"><?php echo $_SESSION["values"]['information']; ?></textarea>
		<div class="clear"></div>
	</div>
<?php } ?>

<?php
$list = $orders->getOrder($order_id);
?>

<?php
if (in_array($action, array("show"))) { 
	$orders->clear();
}
	
?>

<div style="padding-left:100px; position:relative;">

<?php if (!in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>	
<p id="return_products">
<!--  
<input type="submit" name="return" value="Zwrot towaru" />
-->
<img src="images/returns.png" alt="" />
</p>
<?php } ?>

<p><strong>Zamawiający (<?php echo $orders->textControll($list[0]['user_name']); ?>)</strong></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table style="width:300px;">
	<tr>
		<td style="width:100px; text-align:left;"><strong>Nazwa firmy</strong></td>
		<td style="width:200px; text-align:left;"><?php echo $orders->textControll($list[0]['user_name']); ?></td>
	</tr>
	<tr>
		<td style="width:100px; text-align:left;"><strong>Adres:</strong></td>
		<td style="width:200px; text-align:left;">
		<?php 
		echo $list[0]['post_code'].' '.$list[0]['city'].'<br />'.$list[0]['street'].' '.$list[0]['home']; 
		if ($list[0]['flat'] != '') echo '/'.$list[0]['flat'];
		?></td>
	</tr>
	<tr>
		<td style="width:100px; text-align:left;"><strong>NIP</strong></td>
		<td style="width:200px; text-align:left;"><?php echo $list[0]['identificator']; ?></td>
	</tr>
</table>

<?php
$comment = $list[0]['comment'];
?>

<p>&nbsp;</p>
<p><strong>Zamówienie:</strong></p>

<?php if ($_SESSION["errors"]['names']) { ?>
<div class="form_element error">
	<?php echo $_SESSION["errors"]['names']; ?>
	<div class="clear"></div>
</div>
<?php } ?>

<?php if ($_SESSION["errors"]['producers']) { ?>
<div class="form_element error">
	<?php echo $_SESSION["errors"]['producers']; ?>
	<div class="clear"></div>
</div>
<?php } ?>

<?php if ($_SESSION["errors"]['taxes']) { ?>
<div class="form_element error">
	<?php echo $_SESSION["errors"]['taxes']; ?>
	<div class="clear"></div>
</div>
<?php } ?>

<?php if ($_SESSION["errors"]['prices']) { ?>
<div class="form_element error">
	<?php echo $_SESSION["errors"]['prices']; ?>
	<div class="clear"></div>
</div>
<?php } ?>

<div>
<table style="width:auto">
	<?php if (!in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>	
	<tr>
		<td colspan="<?php echo (($list[0]['status'] == 8) ? 8 : 7); ?>">&nbsp;</td>
		<td colspan="2"><span id="regroup_order"><img src="images/regroup_order.png" alt="" /></span></td>
	</tr>
	<?php } ?>
	<tr>
		<td style="border:1px solid #333;"><strong>Produkt (model)</strong></td>
		<td style="border:1px solid #333;"><strong>Opis</strong></td>
		<td style="border:1px solid #333; width:100px;"><strong>Indeks</strong></td>
		<td style="border:1px solid #333; width:100px;"><strong>Producent</strong></td>
		<?php if ($list[0]['status'] == 8 && $list[0]['query_id'] == 0 && !in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>
		<td style="border:1px solid #333;"><strong>Zamiennik</strong></td>
		<?php } ?>
		<?php //if ($list[0]['status'] == 8 && $list[0]['query_id'] > 0) { ?>
		<td style="border:1px solid #333;"><strong>Jedn. miary</strong></td>
		<?php //} ?>
		<td style="border:1px solid #333;"><strong>Ilość</strong></td>
		<td style="border:1px solid #333;"><strong>Cena netto</strong></td>
		<td style="border:1px solid #333;"><strong>Wartość netto</strong></td>
		<td style="border:1px solid #333;"><strong>Vat (%)</strong></td>
		<td style="border:1px solid #333;"><strong>Wart. brutto</strong></td>
		<td style="border:1px solid #333;"><strong title="komentarz handlowca">Kom. handl.</strong></td>
		<?php if (!in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>	
		<td></td>
		<td><strong>Ilość</strong></td>
		<?php } ?>
	</tr>
	<?php
	foreach ($list as $row)
	{
		$tax = ((int)$row['tax']*$row['netto_price'])/100;
		$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
		$netto_value = sprintf("%0.2f", $row['netto_price']*$row['amount']);
		?>
		<tr>
			<td style="border:1px solid #333;">
				<?php if ($list[0]['status'] == 8 && !in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>
				<input type="text" name="names[]" value="<?php echo $row['product_name']; ?>" style="width:300px;" />
				<?php } else { ?>
				<input type="hidden" name="names[]" value="<?php echo $row['product_name']; ?>" /> <?php echo $row['product_name']; ?>
				<?php } ?>
			</td>
			<td style="border:1px solid #333;"><?php echo $row['description']; ?></td>
			<td style="border:1px solid #333;">
				<?php if ($list[0]['status'] == 8 && !in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>
				<input type="text" name="symbols[]" value="<?php echo $row['symbol']; ?>" />
				<?php } else { ?>
				<input type="hidden" name="symbols[]" value="<?php echo $row['symbol']; ?>" /> <?php echo $row['symbol']; ?>
				<?php } ?>
			</td>
			<td style="border:1px solid #333;">
				<?php if (in_array($list[0]['status'], array(1,8)) && !in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>
				<input type="text" name="producers[]" value="<?php echo $row['producer_name']; ?>" />
				<?php } else { ?>
				<input type="hidden" name="producers[]" value="<?php echo $row['producer_name']; ?>" /> <?php echo $row['producer_name']; ?>
				<?php } ?>
			</td>
			<?php if ($list[0]['status'] == 8 && $list[0]['query_id'] == 0 && !in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>
			<td class="order_replacement" style="border:1px solid #333;">
				<input type="hidden" name="replacements[]" value="<?php echo $row['replacement_id']; ?>" />
				<strong class="add_replacement"><img src="images/replacement_icon.png" title="wybierz zamiennik" /></strong> 
				<span style="display:<?php echo (($row['replacement_id'] > 0) ? "inline" : "none"); ?>"><?php echo $row['replacement_name']; ?></span> 
				<strong class="delete_replacement" style="display:<?php echo (($row['replacement_id'] > 0) ? "inline" : "none"); ?>"><img src="images/delete_icon.png" title="usuń zamiennik" /></strong> 
			</td>
			<?php } ?>
			<?php //if ($list[0]['status'] == 8 && $list[0]['query_id'] > 0) { ?>
			<td style="border:1px solid #333;">
			<select name="units[]">
			<option value="0">-- wybierz --</option>
			<option value="1" <?php if ($row['unit'] == 1 || $row['default_unit'] == 'KG') { echo 'selected="selected"'; } ?>>kilogram</option>
			<option value="2" <?php if ($row['unit'] == 2 || $row['default_unit'] == 'KPL') { echo 'selected="selected"'; } ?>>komplet</option>
			<option value="3" <?php if ($row['unit'] == 3 || $row['default_unit'] == 'M2') { echo 'selected="selected"'; } ?>>m2</option>
			<option value="4" <?php if ($row['unit'] == 4 || $row['default_unit'] == 'M3') { echo 'selected="selected"'; } ?>>m3</option>
			<option value="5" <?php if ($row['unit'] == 5 || $row['default_unit'] == 'MB') { echo 'selected="selected"'; } ?>>mb</option>
			<option value="6" <?php if ($row['unit'] == 6 || $row['default_unit'] == 'OP') { echo 'selected="selected"'; } ?>>opakowanie</option>
			<option value="7" <?php if ($row['unit'] == 7 || $row['default_unit'] == 'PAR') { echo 'selected="selected"'; } ?>>para</option>
			<option value="9" <?php if ($row['unit'] == 9 || $row['default_unit'] == 'STO') { echo 'selected="selected"'; } ?>>sto sztuk</option>
			<option value="8" <?php if ($row['unit'] == 8 || $row['default_unit'] == 'SZT') { echo 'selected="selected"'; } ?>>sztuka</option>
			</select>
			</td>
			<?php //} ?>
			<td style="border:1px solid #333;">
				<?php if (in_array($list[0]['status'], array(1,8)) && !in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>
				<input type="text" name="amounts[]" value="<?php echo (float)$row['amount']; ?>" style="width:30px; text-align:right" />
				<?php } else { ?>
				<input type="hidden" name="amounts[]" value="<?php echo (float)$row['amount']; ?>" /> <?php echo (float)$row['amount']; ?>
				<?php } ?>
				<input type="hidden" name="ids[]" value="<?php echo $row['detail_id']; ?>" />
			</td>
			<td style="border:1px solid #333;">
				<?php if (in_array($list[0]['status'], array(1,8)) && !in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>
				<input type="text" name="prices[]" value="<?php echo $row['netto_price']; ?>" style="width:60px; text-align:right" /> zł
				<?php } else { ?>
				<input type="hidden" name="prices[]" value="<?php echo $row['netto_price']; ?>" /> <?php echo $row['netto_price']; ?>
				<?php } ?>
			<td style="border:1px solid #333;"><?php echo $netto_value; ?> zł</td>
			<td style="border:1px solid #333;">
				<?php if (in_array($list[0]['status'], array(1,8)) && !in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>
				<select name="taxes[]" style="width:60px; text-align:right;">
				<option value="23" <?php if ($row['tax'] == 23) { echo 'selected="selected"'; } ?>>23</option>
				<option value="5" <?php if ($row['tax'] == 5) { echo 'selected="selected"'; } ?>>5</option>
				<option value="8" <?php if ($row['tax'] == 8) { echo 'selected="selected"'; } ?>>8</option>
				</select>
				<?php } else { ?>
				<input type="hidden" name="taxes[]" value="<?php echo $row['tax']; ?>" /> <?php echo $row['tax']; ?>
				<?php } ?>
			</td>
			<td style="border:1px solid #333;"><?php echo $brutto_value; ?> zł</td>
			<td style="border:1px solid #333;"><img src="images/informations_icon.png" alt="" title="Kliknij aby dodać/zmienić komentarz" id="element_<?php echo $row['detail_id']; ?>" class="info_change" style="cursor:pointer;" /></td>
			<?php if (!in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>	
			<td><input type="checkbox" name="elements[]" value="<?php echo $row['detail_id']; ?>" /></td>
			<td><input type="text" name="details[<?php echo $row['detail_id']; ?>]" value="<?php echo (float)$row['amount']; ?>" style="width:80px; text-align:right" /></td>
			<?php } ?>
		</tr>
		<?php		
		$weight += $row['amount'] * $row['weight'];
		
		if ($row['pallet'] == 1)
		{
			$pallets ++;
		}
		
		$netto_sum += $netto_value;
		$brutto_sum +=  $brutto_value; 
	}
	
	if ($list[0]['payment'] == 'za pobraniem ( + 6zł)')
	{
		$payment = 1;
	}
	else 
	{
		$payment = 0;
	}
	
	if (empty($list[0]['mpk']))
	{
		$transport_brutto = sprintf("%0.2f", $prices->getTransportPrice((($list[0]['transport'] == '') ? $weight : 0), $list[0]['brutto_value'], $pallets, $payment, $list[0]['user_id']));
		$transport_netto  = sprintf("%0.2f", (100*$transport_brutto/123));
		?>
		<tr>
			<td style="border:1px solid #333;">Transport</td>
			<td style="border:1px solid #333;">Transport</td>
			<td style="border:1px solid #333;">1</td>
			<td style="border:1px solid #333;"><?php echo $transport_netto; ?> zł</td>
			<td style="border:1px solid #333;"><?php echo $transport_netto; ?> zł</td>
			<td style="border:1px solid #333;">23</td>
			<td style="border:1px solid #333;"><?php echo $transport_brutto; ?> zł</td>
		</tr>
	<?php } ?>
	
	<tr>
		<td style="border:1px solid #333;">&nbsp;</td>
		<td style="border:1px solid #333;">&nbsp;</td>
		<td style="border:1px solid #333;">&nbsp;</td>
		<td style="border:1px solid #333;">&nbsp;</td>
		<?php if ($list[0]['status'] == 8 && $list[0]['query_id'] == 0 && !in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>
		<td style="border:1px solid #333;">&nbsp;</td>
		<?php } ?> 
		<?php //if ($list[0]['status'] == 8 && $list[0]['query_id'] > 0) { ?>
		<td style="border:1px solid #333;">&nbsp;</td>
		<?php //} ?>
		<td style="border:1px solid #333;"><strong>RAZEM</strong></td>
		<td style="border:1px solid #333;">&nbsp;</td>
		<td style="border:1px solid #333;"><strong><?php echo sprintf("%0.2f", $netto_sum + $transport_netto); ?> zł</strong></td>
		<td style="border:1px solid #333;"><strong>&nbsp;</strong></td>
		<td style="border:1px solid #333;"><strong><?php echo sprintf("%0.2f", $brutto_sum + $transport_brutto); ?> zł</strong></td>
		<td style="border:none;">&nbsp;</td>
		<?php if (!in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>	
		<td style="border:none;">&nbsp;</td>
		<td style="border:none;">&nbsp;</td>
		<?php } ?>
	</tr>
</table>

<div id="trader_comment">
<input type="hidden" name="detail_id" value="0" />
<p><textarea name="trader_comment" cols="0" rows="0"></textarea></p>
<p><a href="./" class="submit"><strong>ZAPISZ KOMENTARZ</strong></a></p>
<span><img src="images/close_icon.png" alt="" class="close" /></span>
</div>

</div>

<p>&nbsp;</p>

<?php if (!empty($list[0]['mpk'])) { ?>
<div class="order_informations">
	<div class="path">
	<div class="element">
		<p><span>Zamawiający</span></p>
		<p><?php echo $user_names[$list[0]['contractor_login']]; ?></p>
		<p><?php echo $user_names[$list[0]['contractor_login'].'_phone']; ?></p>
		<p><?php echo $list[0]['contractor_email']; ?></p>
		<p><?php echo $list[0]['contractor_date']; ?></p>
	</div>
	<div class="arrow">
	<strong>&#187;</strong>
	</div>
	<div class="element">
		<p><span>Analityk</span></p>
		<p><?php echo $user_names[$list[0]['analyst_login']]; ?></p>
		<p><?php echo $list[0]['analyst_email']; ?></p>
		<p><?php echo (($list[0]['analyst_date'] != "1970-01-01" && !empty($list[0]['analyst_date'])) ? $list[0]['analyst_date'] : ""); ?></p>
	</div>
	<div class="arrow">
	<strong>&#187;</strong>
	</div>
	
	<?php if ($list[0]['query_id'] > 0) { ?>
		
		<div class="element">
			<p><span>Administrator</span></p>
			<p><?php echo $user_names[$list[0]['administrator_login']]; ?></p>
			<p><?php echo $list[0]['administrator_email']; ?></p>
			<p><?php echo (($list[0]['administrator_date'] != "1970-01-01" && !empty($list[0]['administrator_date'])) ? $list[0]['administrator_date'] : ""); ?></p>
		</div>
		<div class="arrow">
		<strong>&#187;</strong>
		</div>
		
	<?php } ?>
	
	<div class="element">
		<p><span>Kierownik</span></p>
		<p><?php echo $user_names[$list[0]['manager_login']]; ?></p>
		<p><?php echo $list[0]['manager_email']; ?></p>
		<p><?php echo (($list[0]['manager_date'] != "1970-01-01" && !empty($list[0]['manager_date'])) ? $list[0]['manager_date'] : ""); ?></p>
	</div>
	<div class="arrow">
	<strong>&#187;</strong>
	</div>
	<div class="element">
		<p><span>UNIMET</span></p>
		<p><?php echo ((!empty($list[0]['unimet_representative'])) ? $user_names[$list[0]['unimet_representative']] : 'brak danych'); ?></p>
		<p><?php echo $list[0]['unimet_email']; ?></p>
		<p><?php echo (($list[0]['delivery_date'] != "1970-01-01" && !empty($list[0]['delivery_date'])) ? $list[0]['delivery_date'] : ""); ?></p>
	</div>
	<div class="arrow">
	<strong>&#187;</strong>
	</div>
	<div class="element">
		<p><span>Odbierający</span></p>
		<p><?php echo $user_names[$row['receiving_login']]; ?></p>
		<p><?php echo $user_names[$list[0]['receiving_login'].'_phone']; ?></p>
		<p><?php echo $row['receiving_email']; ?></p>
		<p><?php echo (($list[0]['receiving_date'] != "1970-01-01" && !empty($list[0]['receiving_date'])) ? $list[0]['receiving_date'] : ""); ?></p>
	</div>
	<div class="clear"></div>
</div>
<p>&nbsp;</p>
<?php } ?>

<p><strong>Wydział: </strong> <?php echo $list[0]['department']; ?></p>
<p><strong>Data złożenia zamówienia: </strong> <?php echo $list[0]['add_date']; ?></p>
<p><strong>Data dostawy: </strong> <?php echo (($list[0]['delivery_date'] != "1970-01-01" && !empty($list[0]['delivery_date'])) ? $list[0]['delivery_date'] : "brak danych"); ?></p>
<p><strong>Forma płatności: </strong> <?php echo $list[0]['payment']; ?></p>
<p><strong>Numer MPK: </strong> <?php echo $list[0]['mpk']; ?></p>
<p><strong>Faktura VAT: </strong> <?php echo ((!empty($list[0]['fv'])) ? $list[0]['fv'] : 'brak danych'); ?></p>
<p><strong>WZ: </strong> <?php echo ((!empty($list[0]['wm'])) ? $list[0]['wm'] : 'brak danych'); ?></p>
<p>&nbsp;</p>

<?php if (!empty($information) && (in_array($action, array("show")) || $_SESSION["admin"]['observer'] == 1)) { ?>
	<p><strong>Komentarz handlowca do zamówienia: </strong></p>
	<p><?php echo nl2br($information); ?></p>
	<p>&nbsp;</p>
<?php } ?>

<?php if (!empty($comment)) { ?>
	<p><strong>Komentarz użytkownika do zamówienia: </strong></p>
	<p><?php echo nl2br($comment); ?></p>
	<p>&nbsp;</p>
<?php } ?>

<?php 
$delivery_point = $orders->checkDeliveryPoint($order_id);
		
if (null != $delivery_point)
{
	?><p><strong>Miejsce dostawy:</strong> <?php echo $delivery_point; ?></p><p>&nbsp;</p><?php
}
?>

<p><strong>Adres dostawy: </strong><br /> 
<?php 
if ($list[0]['delivery_post_code'] != '')
{
	echo $list[0]['delivery_post_code'].' '.$list[0]['delivery_city'].'<br />'.$list[0]['delivery_street'].' '.$list[0]['delivery_home']; 
	if ($list[0]['delivery_flat'] != '') echo '/'.$list[0]['delivery_flat'];
}
else
{
	echo $list[0]['post_code'].' '.$list[0]['city'].'<br />'.$list[0]['street'].' '.$list[0]['home']; 
	if ($list[0]['flat'] != '') echo '/'.$list[0]['flat'];
}
?></p>
<p>&nbsp;</p>
<p><strong>Dokument: </strong> <?php echo $list[0]['document']; ?></p>
</div>

<?php if (!in_array($action, array("show")) && $_SESSION["admin"]['observer'] == 0) { ?>	
	<input type="hidden" name="operation" value="save_changes" />
	
	<div class="form_element" style="padding:10px 0px 0px 0px">
		<span id="save_order"><img src="images/save_changes.png" alt="" /></span>
		<?php if ($list[0]['status'] == 3 && $list[0]['date_diff'] > 5) { ?>
		<span id="move_to_archive"><img src="images/move_to_archive.png" alt="" /></span>
		<?php } ?>
		<div class="clear"></div>
	</div>
<?php } ?>
</form> 


<div style="padding:10px 0px 0px 0px">
<?php 
$communicats_list = $orders->getCommunicats($order_id);

while($communicat = mysql_fetch_array($communicats_list)) 
{
	?><p><strong>[<?php echo $communicat['time']; ?>]</strong> - <?php echo $communicat['communicat']; ?></p><?php
}
?>
</div>

<div id="replacment_box">
<input type="hidden" name="element_id" value="0" />
<div>
	<div>
		<p><input type="text" name="key" value="" /></p>
		<p><img src="images/save_replacement.png" alt="" /></p>
	</div>
	<strong><a href="./" style="">zamknij</a></strong>
</div>
</div>
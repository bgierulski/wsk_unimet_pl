<?php
class Authentication extends Validators 
{
	private $_db;
	private $_user;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
	}
	
	public function login($login, $password)
	{
		
		$user = $this->_db->query('SELECT id, login, narrow_orders, observer FROM users WHERE login = \''.$login.'\' AND MD5(CONCAT(`key`, \''.$password.'\')) = password AND status = 0 AND active = 1') or $this->_db->raise_error();
		
		
		if ($this->_db->num_rows($user))
		{
			$this->_user = mysql_fetch_array($user);	
			$this->_user['controll'] = 1;	
			$this->_user["merchants"] = array();	
			$this->_user["modules"] = array();	
			
			$result = $this->_db->query('SELECT merchant_id FROM user_details WHERE user_id = '.$this->_user['id']);
			
			while($row = mysql_fetch_array($result)) 
	   		{
	   			array_push($this->_user["merchants"], $row['merchant_id']);
	   		}
	   		
	   		mysql_free_result($result);
	   		$result = $this->_db->query('SELECT module FROM user_cms_modules WHERE user_id = '.$this->_user['id']);
			
			while($row = mysql_fetch_array($result)) 
	   		{
	   			array_push($this->_user["modules"], $row['module']);
	   		}
	   		
	   		mysql_free_result($result);
			
			$this->setSession("admin", $this->_user);
			header("Location: ".BASE_ADDRESS);
		}
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}
?>
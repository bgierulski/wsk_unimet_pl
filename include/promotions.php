<?php 
if (isset($_POST['sort']))
{
	$_SESSION['products_sort'] = $_POST['sort'];
}

if (isset($_POST['visibility_change']))
{
	$_SESSION['visibility'] = (int)$_POST['visibility'];
}

$controll = explode('.', $_GET['param2']);
$producer_id = (((int)$controll[1]) ? $controll[1] : 0);

$promotions = $products->getPromotions();

$products_list = $products->getPromotionProducts($_SESSION['products_sort'], $producer_id, $_SESSION['visibility'], $promotions);
$products_count = count($products_list);
?>

<div id="page_header">
	<div class="details">
		<p><strong>Promocje</strong></p>
		<p>Produktów oznaczonych jako promocje: <?php echo $products_count; ?> <span class="comment"></span></p>
	</div>
	<div class="box">
		<p><span id="select_products">zaznacz</span> / <span id="unselect_products">odznacz</span> wszystkie</p>
	</div>
	<div class="box">
	<form method="post" action="<?php echo substr(BASE_ADDRESS, 0, -1).$_SERVER['REQUEST_URI']; ?>" name="visibility">
		<input type="hidden" name="visibility_change" value="" />
		<p>Pokazuj tylko dostępne <input type="checkbox" name="visibility" onchange="javascript:document.visibility.submit();" value="1" <?php if ($_SESSION['visibility'] == 1) { echo 'checked="checked"'; } ?> /></p>
	</form>
	</div>
	<div class="box">
	<form method="post" action="<?php echo substr(BASE_ADDRESS, 0, -1).$_SERVER['REQUEST_URI']; ?>" name="sort">
		<p>Sortuj według: 
		<select name="sort" onchange="javascript:document.sort.submit();">
		<option value="product_asc" <?php if ($_SESSION['products_sort'] == 'product_asc') { echo 'selected="selected"'; } ?>>Nazwy</option>
		<option value="producer_asc" <?php if ($_SESSION['products_sort'] == 'producer_asc') { echo 'selected="selected"'; } ?>>Producenta</option>
		<option value="price_asc" <?php if ($_SESSION['products_sort'] == 'price_asc') { echo 'selected="selected"'; } ?>>Ceny rosnąco</option>
		<option value="price_desc" <?php if ($_SESSION['products_sort'] == 'price_desc') { echo 'selected="selected"'; } ?>>Ceny malejąco</option>
		</select>
		</p>
	</form>
	</div>
	<div class="box">
		<p class="legend" id="legend_1"><strong>LEGENDA</strong></p> 
		<div class="legend_details" id="legend_details_1"><?php include(BASE_DIR.'products_lengend.php'); ?></div>
	</div>
	<div class="clear"></div>
</div>

<?php 
$producers_list = $producers->getPromotionsProducers($promotions);

if ($products_count)
{
	?>
	<div id="producers">
		<?php 
		while($producer = mysql_fetch_array($producers_list)) 
	   	{
	       	?>
	       	<div <?php if ($producer['id'] == $producer_id) { echo 'class="active"'; } ?>>
			<p><strong><a href="./promocje<?php echo ',1.'.$producer['id']; ?>.html"><?php echo $producer['name']; ?></a></strong></p>
			</div>
	       	<?php
	   	}
		?>
		<div class="clear"></div>
	</div>
	<?php 
}
?>
<div id="page_content">
<?php 
if ($products_count)
{
	?>
	<table cellspacing="0" cellpadding="0" class="subgroup" style="border-bottom:none;">
	<tr>
		<th class="td_image">&nbsp;</th>
		<th class="td_product"><p>Produkt</p></th>
		<th class="td_symbol"><p>Symbol</p></th> 
		<th class="status"><p>&nbsp;</p></th>
		<th class="td_description"><div>&nbsp;</div></th>
		<th class="td_replacement">&nbsp;</th>
		<th class="td_weight"><div>Waga</div></th>
		<th class="td_producer"><div>Producent</div></th>
		<th class="td_availability"><div>&nbsp;</div></th>
		<th class="td_price"><div>Cena netto</div></th>
		<th class="td_amount"><div>Ilość</div></th>
		<th class="td_unit"><div>JM.</div></th>
	</tr>
	</table>
	<div id="products_content">
		<table cellspacing="0" cellpadding="0" class="subgroup" style="border-top:none;">
		<form method="POST" action="<?php echo substr(BASE_ADDRESS, 0, -1).$_SERVER['REQUEST_URI']; ?>" name="to_basket">
		<input id="product_operation" type="hidden" name="operation" value="to_basket" />
		<?php 
		
		foreach ($products_list as $i => $product)
		{
			?>
			<tr>
				<td class="td_image">
					<div class="wrapper" id="product_<?php echo $product['id_hermes']; ?>"><div class="cell"><div class="hack"><p>
					<img src="<?php echo BASE_ADDRESS; ?>/include/image.php?id=<?php echo (int)$product['id_hermes']; ?>&w=38&h=38" alt="" border="0">
					</p></div></div></div>
				</td>
				<td class="td_product">
					<strong><a href="#" onClick="javascript:pop_up(<?php echo $product['id_hermes']; ?>); return false;"><?php echo $product['name']; ?></a></strong>
				</td>
				<td class="td_symbol">
					<?php echo $product['symbol']; ?>
				</td>
				<td class="status">
				<?php 
				if (in_array($product['id_hermes'], $promotions) && $_SESSION["b2b_user"]['promotions'] == 1) { ?><p><span>P</span></p><?php }
				if ($product['news'] >= 0) { ?><p><strong>N</strong></p><?php }
				if (!in_array($product['id_hermes'], $promotions) && $product['news'] < 0) { ?>&nbsp;<?php }
				?>
				</td>
				<td class="td_description">
					<?php if (!empty($product['description'])) { ?>
					<p><a href="#" onClick="javascript:pop_up(<?php echo $product['id_hermes']; ?>); return false;"><strong id="description_<?php echo $product['id_hermes']; ?>"><img src="images/comment.png" alt="" /></strong></a></p>
					<?php } ?>
				</td>
				<td class="td_replacement"><p><?php if ($product['replacement'] > 0) { ?><img src="images/replacement.png" alt="" /><?php } else { ?>&nbsp;<?php } ?></p></td>
				<td class="td_weight">
					<p>
					<span><?php if ($product['weight'] > 0) { echo $product['weight']; } else { echo 'b/d'; } ?></span> 
					<span><?php if ($product['pallet'] == 1) { ?><img src="images/element_3.png" alt="" title="transport paletowy" /><?php } ?></span>
					</p>
				</td>
				<td class="td_producer">
					<p><?php echo $product['producer_name']; ?></p>
				</td>
				<td class="td_availability">
					<p>
					<?php 
					if ($product['status'] > 0) 
					{ 
						switch ($product['status'])
						{
							case 1: ?><img src="images/mag/magaznyn_ziel2.png" alt="" /><?php break;
							case 2: ?><img src="images/mag/magaznyn_czer2.png" alt="" /><?php break;
							case 3: ?><img src="images/mag/magaznyn_nieb2.png" alt="" /><?php break;
							case 4: ?><img src="images/mag/magaznyn_czar2.png" alt="" /><?php break;
						}
					}
					else
					{
						?><img src="./magazyn.php?id=<?php echo $product['id_hermes']; ?>" alt="" /><?php
					}
					?>
					</p>
				</td>
				<td class="td_price">
					<p><?php echo $prices->getPrice($product['id_hermes'], 4); ?> zł</p>
				</td>
				<td class="td_amount">
					<div>
					<span><input class="product_count" type="text" id="count_<?php echo $product['id_hermes']; ?>" name="counts[<?php echo $product['id_hermes']; ?>]" value="1" /></span> 
					<span><input class="product_checkbox" type="checkbox" id="active_<?php echo $product['id_hermes']; ?>" name="actives[]" value="<?php echo $product['id_hermes']; ?>" /></span>
					<span class="favorite_product"><img id="favorite_<?php echo $product['id_hermes']; ?>" src="images/element_4.png" alt="" title="dodaj do ulubionych" /></span>
					</div>
				</td>
				<td class="td_unit">
					<p><?php echo strtolower($product['unit']); ?></p>
				</td>
			</tr>
			<?php
		}
		?>
		</table>
	</div>
	<?php 
}
?>

</div>

<?php 
if ($products_count)
{
	?>				
	<div class="action">
		<div style="float:left;">
			<p class="button legend" id="legend_2">
			<a href="./" onClick="javascript:return false;">LEGENDA</a>
			</p>
			<div class="legend_details" id="legend_details_2" style="padding:5px;"><?php include(BASE_DIR.'products_lengend.php'); ?></div>
		</div>
		<div>
			<p class="button" style="padding-right:40px;">
			<a href="./" onClick="javascript:favorite_products(); return false;">PRODUKTY ULUBIONE</a>
			</p>
			<p class="button" style="padding-right:40px;">
			<a href="./" onClick="javascript:$('input#product_operation').val('favorite_products'); document.to_basket.submit(); return false;">DODAJ DO ULUBIONYCH</a>
			</p>
			<p class="button">
			<a href="./" id="add_to_order">DODAJ DO ZAMÓWIENIA</a>
			</p>
		</div>
		<div class="clear"></div>
	</div>
	</form>
	<?php 
}
?>

<?php
include('../config.php');
include(BASE_DIR.'default/class.DataBase.php');
include(BASE_DIR.'default/class.Validators.php');

$validators = new Validators();

$result = $db->query('SELECT users.id, login, name, email, active, users.comment, login_date, login_counter, IFNULL(count(orders.id),0) as 
orders_count, IFNULL(SUM(brutto_value), 0.00) as brutto_sum FROM users LEFT JOIN orders ON users.id = orders.user_id WHERE users.status = 1 
GROUP BY users.id');

$data = array();
$counter = 0;
while($row = mysql_fetch_array($result)) 
{
	if ($validators->validateEmail($row['email']))
	{
		array_push($data, array(
			'email' => $row['email'], 
			'login_date' => $row['login_date'], 
			'login_counter' => $row['login_counter'], 
			'orders_count' => $row['orders_count'], 
			'brutto_sum' => $row['brutto_sum']
		));
		
		$counter++;
	}
}

header("Content-type: application/vnd.ms-excel");  
header("Content-disposition: attachment; filename=adresy_email.csv");  
header("Pragma: no-cache"); 

$csv = '';

if (is_array($data))
{
	foreach ($data as $user)
	{
		$csv .= $user['email'].';'.$user['login_date'].';'.$user['login_counter'].';'.$user['orders_count'].';'.$user['brutto_sum']."\n";
	}
}

echo $csv;
?>
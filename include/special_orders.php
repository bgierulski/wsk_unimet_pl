<?php 
$param = $_GET['param1'];
$action = $_GET['param2']; 
$id = (int)$param3;

if (isset($_POST['query_operation']) && (int)$_POST['id'] > 0)
{
	switch ($_POST['query_operation'])
	{
		case 'accept':
			$special_orders->acceptOrder((int)$_POST['id'], $_POST['comment']);
			break;
				
		case 'reject':
			$special_orders->rejectOrder((int)$_POST['id'], $_POST['comment']);
			break;
	}
}
?>

<div id="page_header">
	<div class="details">
		<p><strong>Zapytania</strong> <?php if ($_SESSION["communicats"]['ok']) echo ' - '; echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?></p>
	</div>
	<?php if ($_SESSION["b2b_user"]['id']) { ?>
	<div class="box">
		<p><strong><a href="./zapytania,lista.html" <?php if ($param == 'zapytania' && $action == 'lista') { echo 'class="active"'; } ?>>Lista zapytań</a></strong></p>
	</div>
	<div class="box">
		<p><strong><a href="./zapytania,nowe.html" <?php if ($param == 'zapytania' && $action == 'nowy') { echo 'class="active"'; } ?>>Nowe zapytanie</a></strong></p>
	</div>
	<?php } ?>
	<div class="clear"></div>
</div>

<?php 
if ($_SESSION["b2b_user"]['id'])
{
	?>
	<div id="page_content">
	
	<div id="during_the_verification">
	<div>
		<p>Twoje zamówienie jest w trakcie wstępnej weryfikacji.</p>
		<p><strong>Proszę czekać</strong></p>
		<p><img src="images/loading_transparent.gif" alt="" /></p>
	</div>
	</div>
	
	<div id="special_order" style="display:<?php if ($param == 'zapytania' && $action == 'nowe') { echo 'block'; } else { echo 'none'; } ?>">
		
		<form method="post" action="<?php echo substr(BASE_ADDRESS, 0, -1).$_SERVER['REQUEST_URI']; ?>" name="special_products">
		<input type="hidden" name="operation" value="add_special_order" />
		
		<div id="form">
			<?php 
			if (empty($_POST))
			{
				$special_orders->regroupSpecialOrder();
			}
			?> 
		
			<?php if (isset($_SESSION["errors"]['amounts'])) { ?>
			<div class="element error">
			<label style="width:115px;">&nbsp;</label><?php echo $_SESSION["errors"]['amounts']; ?>
			</div>
			<?php } if (isset($_SESSION["errors"]['names'])) { ?>
			<div class="element error">
			<label style="width:115px;">&nbsp;</label><?php echo $_SESSION["errors"]['names']; ?>
			</div>
			<?php } if (isset($_SESSION["errors"]['units'])) { ?>
			<div class="element error">
			<label style="width:115px;">&nbsp;</label><?php echo $_SESSION["errors"]['units']; ?>
			</div>
			<?php } if (isset($_SESSION["errors"]['mpk'])) { ?>
			<div class="element error">
			<label style="width:115px;">&nbsp;</label><?php echo $_SESSION["errors"]['mpk']; ?>
			</div>
			<?php } if (isset($_SESSION["errors"]['paths'])) { ?>
			<div class="element error">
			<label style="width:115px;">&nbsp;</label><?php echo $_SESSION["errors"]['paths']; ?>
			</div>
			<?php } ?>
		
			<div class="element">
			<label style="width:115px;">Typ płatności:</label>
			<input type="hidden" name="payment" value="przedpłata / przelew" /> <strong>przedpłata / przelew</strong>
			</div>
			
			<div class="element">
			<label style="width:115px;">Typ dokumentu:</label>
			<input type="hidden" name="document" value="faktura" /> <strong>faktura</strong>
			</div>
			
			<?php if (isset($_SESSION["errors"]['comment'])) { ?>
			<div class="element error">
			<label style="width:115px;">&nbsp;</label><?php echo $_SESSION["errors"]['comment']; ?>
			</div>
			<?php } ?>
			
			<div class="element">
			<label style="width:115px;">Komentarz <strong class="error">(*)</strong></label>
			<textarea name="comment" cols="0" rows="0" style="width:298px; height:100px"><?php echo $_SESSION["values"]['comment']; ?></textarea>
			<?php /* ?>
			<input type="text" name="comment" value="<?php echo $_SESSION["values"]['comment']; ?>" />
			<?php */ ?>
			</div>
		</div>
		
		<div id="query_product_list" style="display:<?php echo (($_SESSION["values"]['step'] == 2) ? "none" : "block"); ?>">
			<table class="proteges_list">
			<tr>
				<th style="width:150px;">MPK</th>
				<th style="width:300px;">Produkt</th>
				<th style="width:150px;">Symbol produktu</th>
				<th style="width:150px;">Producent</th>
				<th>Opis dodatkowy</th>
				<th style="width:100px;">Jedn. miary</th>
				<th style="width:100px;">Ilość</th>
				<th style="width:15px; background:none">&nbsp;</th>
			</tr>
			<?php 
			if (!$_SESSION["values"]['products_count'])
			{
				?>
				<tr class="special_product">
					<input type="hidden" name="ids[]" value="" />
					<td><input type="text" name="mpk[]" value="" /></td>
					<td><input type="text" name="names[]" value="" /></td>
					<td><input type="text" name="symbols[]" value="" /></td>
					<td><input type="text" name="producers[]" value="" /></td>
					<td><input type="text" name="comments[]" value="" /></td>
					<td>
					<select name="units[]">
					<option value="0">-- wybierz --</option>
					<option value="1">kilogram</option>
					<option value="2">komplet</option>
					<option value="3">m2</option>
					<option value="4">m3</option>
					<option value="5">mb</option>
					<option value="6">opakowanie</option>
					<option value="7">para</option>
					<option value="9">sto sztuk</option>
					<option value="8">sztuka</option>
					</select>
					</td>
					<td><input type="text" name="amounts[]" value="" /></td>
					<td><img class="delete_query_element" src="images/element_8.gif" alt="usuń" title="usuń" /></td>
				</tr>
				<?php 
			}
			else
			{
				for ($i=0; $i<$_SESSION["values"]['products_count']; $i++)
				{
					?>
					<tr class="special_product">
						<input type="hidden" name="ids[]" value="<?php echo $_SESSION["values"]["ids"][$i]; ?>" />
						<td><input type="text" name="mpk[]" value="<?php echo $_SESSION["values"]["mpk"][$i]; ?>" /></td>
						<td><input type="text" name="names[]" value="<?php echo $_SESSION["values"]["names"][$i]; ?>" /></td>
						<td><input type="text" name="symbols[]" value="<?php echo $_SESSION["values"]["symbols"][$i]; ?>" /></td>
						<td>
						<?php if ((int)$_SESSION["values"]["ids"][$i] == 0) { ?>
							<input type="text" name="producers[]" value="<?php echo $_SESSION["values"]["producers"][$i]; ?>" />
						<?php } else { ?>
							<input type="hidden" name="producers[]" value="<?php echo $_SESSION["values"]["producers"][$i]; ?>" />
							<?php echo $_SESSION["values"]["producers"][$i]; ?>
						<?php } ?>
						</td>
						<td><input type="text" name="comments[]" value="<?php echo $_SESSION["values"]["comments"][$i]; ?>" /></td>
						<td>
						<?php if ((int)$_SESSION["values"]["ids"][$i] == 0) { ?>
							<select name="units[]">
							<option value="0">-- wybierz --</option>
							<option value="1" <?php if ($_SESSION["values"]["units"][$i] == 1) echo 'selected="selected"'; ?>>kilogram</option>
							<option value="2" <?php if ($_SESSION["values"]["units"][$i] == 2) echo 'selected="selected"'; ?>>komplet</option>
							<option value="3" <?php if ($_SESSION["values"]["units"][$i] == 3) echo 'selected="selected"'; ?>>m2</option>
							<option value="4" <?php if ($_SESSION["values"]["units"][$i] == 4) echo 'selected="selected"'; ?>>m3</option>
							<option value="5" <?php if ($_SESSION["values"]["units"][$i] == 5) echo 'selected="selected"'; ?>>mb</option>
							<option value="6" <?php if ($_SESSION["values"]["units"][$i] == 6) echo 'selected="selected"'; ?>>opakowanie</option>
							<option value="7" <?php if ($_SESSION["values"]["units"][$i] == 7) echo 'selected="selected"'; ?>>para</option>
							<option value="9" <?php if ($_SESSION["values"]["units"][$i] == 9) echo 'selected="selected"'; ?>>sto sztuk</option>
							<option value="8" <?php if ($_SESSION["values"]["units"][$i] == 8) echo 'selected="selected"'; ?>>sztuka</option>
							</select>
						<?php } else { ?>
							<input type="hidden" name="units[]" value="<?php echo $_SESSION["values"]["units"][$i]; ?>" />
							<?php
							switch ($_SESSION["values"]["units"][$i])
							{
								case 8: ?>sztuka<?php break;
								case 2: ?>komplet<?php break;
								case 1: ?>kilogram<?php break;
								case 6: ?>opakowanie<?php break;
								case 3: ?>metr kwadr.<?php break;
								case 5: ?>metr bierz.<?php break;
								case 4: ?>metr sześć.<?php break;
								case 7: ?>para<?php break;
								default: ?>&nbsp;<?php break;
							}
							?>
						<?php } ?>
						</td>
						<td><input type="text" name="amounts[]" value="<?php echo $_SESSION["values"]["amounts"][$i]; ?>" /></td>
						<td><img class="delete_query_element" src="images/element_8.gif" alt="usuń" title="usuń" /></td>
					</tr>
					<?php
				}
			}
			?>
			</table>
			
			<div>
				<div class="button" style="float:left;"><a href="./" class="full_offer">POWRÓT TO PEŁNEJ OFERTY UNIMET</a></div>
				<div class="button" style="float:right;"><a href="./" id="add_special_order">DODAJ KOLEJNY PRODUKT SPOZA OFERTY UNIMET</a></div>
				<div class="clear"></div>
			</div>
		</div>
			
		<div id="query_mpk_list"  style="display:<?php echo (($_SESSION["values"]['step'] == 2) ? "block" : "none"); ?>">
			<table class="proteges_list">
			<tr>
				<th style="width:150px;">MPK</th>
				<th>Budżet</th>
				<th>Analityk</th>
				<th>Kierownik</th>
				<th>Odbiorca</th>
			</tr>
			<?php 
			if (is_array($_SESSION["values"]["mpk_keys"]) && !empty($_SESSION["values"]["mpk_keys"]))
			{
				$mpk_list = $basket->getMpkPaths($_SESSION["values"]["mpk_keys"]);
				$budgets_list = $users->getBudgets();
				$users_list = $users->getUsersList();
			}

			$counter = 0;
			
			foreach ($mpk_list as $key => $row)
			{
				if ($counter%2 == 0)
				{
					$class = 'bg_1';
				}
				else 
				{
					$class = 'bg_2';
				}
				?>
				<tr>
					<td class="<?php echo $class; ?>"><?php echo $row['mpk']; ?> <input type="hidden" name="mpk_keys[]" value="<?php echo $row['mpk']; ?>" /></td>
					<td class="<?php echo $class; ?>">
						<select name="budget[]" style="width:90%" id="row_<?php echo $counter; ?>">
						<?php if ($row['budget'] == 0) {?>
						<option value="0">-- wybierz --</option>
						<?php } ?>
						<?php 
						foreach ($budgets_list as $budget)
						{
							if (($row['budget'] > 0 && $budget['id'] == $row['budget']) || $row['budget'] == 0)
							{
								?><option value="<?php echo $budget['id']; ?>" <?php if (($budget['id'] == $row['budget']) || (is_array($_SESSION["values"]["budget"]) && $budget['id'] == $_SESSION["values"]["budget"][$counter])) { echo 'selected="selected"'; } ?>><?php echo $budget['department']; ?></option><?php
							}
						}
						?>
						</select>
					</td>
					
					<td class="<?php echo $class; ?>">
						<select name="analyst[]" style="width:90%">
						<?php if (count($row["analyst"]) > 1 || empty($row["analyst"])) { ?>
						<option value="0">-- wybierz --</option>
						<?php } ?>
						<?php 
						foreach ($users_list as $user)
						{
							if ($_SESSION["values"]["budget"][$counter] == $user['budget'] && $user['budget'] > 0)
							{
								if ($user['analyst'] > 0 && (empty($row["analyst"]) || (!empty($row["analyst"]) && in_array($user['id'], $row["analyst"]))))
								{
									?><option lt="<?php echo $user['budget']; ?>" value="<?php echo $user['id']; ?>" <?php if (is_array($_SESSION["values"]["analyst"]) && $user['id'] == $_SESSION["values"]["analyst"][$counter]) { echo 'selected="selected"'; } ?>><?php echo $user['login']; ?></option><?php
								}
							}
						}
						?>
						</select>
					</td>
					<td class="<?php echo $class; ?>">
						<select name="manager[]" style="width:90%">
						<?php if (count($row["manager"]) > 1 || empty($row["manager"])) { ?>
						<option value="0">-- wybierz --</option>
						<?php } ?>
						<?php 
						foreach ($users_list as $user)
						{
							if ($_SESSION["values"]["budget"][$counter] == $user['budget'] && $user['budget'] > 0)
							{
							if ($user['manager'] > 0 && (empty($row["manager"]) || (!empty($row["manager"]) && in_array($user['id'], $row["manager"]))))
							{
								?><option alt="<?php echo $user['budget']; ?>" value="<?php echo $user['id']; ?>" <?php if (is_array($_SESSION["values"]["manager"]) && $user['id'] == $_SESSION["values"]["manager"][$counter]) { echo 'selected="selected"'; } ?>><?php echo $user['login']; ?></option><?php
							}
							}
						}
						?>
						</select>
					</td>
					<td class="<?php echo $class; ?>">
						<select name="receiving[]" style="width:90%">
						<?php if (count($row["receiving"]) > 1 || empty($row["receiving"])) { ?>
						<option value="0">-- wybierz --</option>
						<?php } ?>
						<?php 
						foreach ($users_list as $user)
						{
							if ($_SESSION["values"]["budget"][$counter] == $user['budget'] && $user['budget'] > 0)
							{
							if ($user['receiving'] > 0 && (empty($row["receiving"]) || (!empty($row["receiving"]) && in_array($user['id'], $row["receiving"]))))
							{
								?><option alt="<?php echo $user['budget']; ?>" value="<?php echo $user['id']; ?>" <?php if (is_array($_SESSION["values"]["receiving"]) && $user['id'] == $_SESSION["values"]["receiving"][$counter]) { echo 'selected="selected"'; } ?>><?php echo $user['login']; ?></option><?php
							}
							}
						}
						?>
						</select>
					</td>
				</tr>
				<?php
				
				$counter++;
			}
			?>
			</table>
		</div>
			
		<div>
			<div class="button" style="float:left; display:<?php echo (($_SESSION["values"]['step'] == 2) ? "block" : "none"); ?>; width:153px; text-align:center; margin-top:5px;"><a href="./" id="back_special_order">WSTECZ</a></div>
			<?php  ?>
			<div class="button" style="float:right; width:153px; text-align:center; margin-top:5px;"><a href="./" id="special_products_form_submit"><?php echo (($_SESSION["values"]['step'] == 2) ? "PRZEKAŻ DALEJ" : "USTAL ŚCIEŻKI"); ?></a></div>
			<?php  ?>
			<div class="clear"></div>
		</div>
		</form>
	
	</div>
	
	<div id="special_orders" style="display:<?php if ($param == 'zapytania' && in_array($action, array('lista','zatwierdz','odrzuc','zamow'))) { echo 'block'; } else { echo 'none'; } ?>">

	<?php
	$year_list = array();
	$years = $special_orders->getSpecialOrderYears();
	
	foreach ((array)$years as $row)
	{
		$year_list[] = $row[0];
	}

	if (in_array((int)$_GET['param3'], $year_list))
	{
		$actual_year = (int)$_GET['param3'];
	}
	else
	{
		$actual_year = $year_list[0];
	}

	$list = $special_orders->getSpecialOrders($actual_year);
	$orders_count = count($list);
	
	if ($orders_count)
	{
		?>
		<p class="query_title">
			<span><strong>Złożone w Unimet</strong></span>
		</p>
		<table class="proteges_list">
		<tr>
			<th style="width:100px">ID</th>
			<th style="width:200px">Data dodania</th>
			<th style="width:200px">Wartość brutto</th>
			<th style="width:200px">Opiekun handlowy</th>
			<th>Operacje</th>
		</tr>
		<?php 
		$counter = 0;
		
		foreach ($list as $i => $row)
		{
			if ($_SESSION["b2b_user"]['id'] == $row['user_id'] && $row['status'] == 8)
			{
				if ($row['id'] != $list[$i-1]['id'])
				{
					$details = '
					<table cellspacing="0" cellpadding="0" style="width:100%;">
					<tr>
						<th><strong>MPK</strong></th>
						<th><strong>Nazwa produktu</strong></th>
						<th><strong>Producent</strong></th>
						<th><strong>Opis</strong></th>
						<th><strong>Jedn. miary</strong></th>
						<th><strong>Ilość</strong></th>
						<th><strong>Cena netto</strong></th>
						<th><strong>Wartość netto</strong></th>
						<th><strong>Wartość brutto</strong></th>
					</tr>';
					
					$netto_sum  = 0;
					$tax_sum = 0;
				}
				
				$tax = ((int)$row['tax']*$row['netto_price'])/100;
				$product_tax = sprintf("%0.2f", $tax*$row['amount']);
				$netto_value = sprintf("%0.2f", ($row['netto_price'] * $row['amount']));
				$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
				
				$details .= '<tr>';
				$details .= '<td>'.$row['mpk'].'</td>';
				$details .= '<td>'.$row['name'].'</td>';
				$details .= '<td>'.$row['producer'].'</td>';
				$details .= '<td>'.$row['comment'].'</td>';
				$details .= '<td>';
				
				switch ($row['unit'])
				{
					case 1: $details .= 'kilogram'; break;
					case 2: $details .= 'komplet'; break;
					case 3: $details .= 'm2'; break;
					case 4: $details .= 'm3'; break;
					case 5: $details .= 'mb'; break;
					case 6: $details .= 'opakowanie'; break;
					case 7: $details .= 'para'; break;
					case 8: $details .= 'sztuka'; break;
				}
				
				$details .= '</td>';
				$details .= '<td>'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'</td>';
				$details .= '<td>'.$row['netto_price'].' zł</td>';
				$details .= '<td>'.$netto_value.' zł</td>';
				$details .= '<td>'.$brutto_value.' zł</td>';
				$details .= '</tr>';
					
				$netto_sum += $netto_value;
				$tax_sum += $product_tax; 
				
				if ($row['id'] != $list[$i+1]['id'])
				{
					$details .= '<tr><td><strong>Komentarz</strong></td><td colspan="7" style="text-align:left">'.nl2br($row['order_comment']).'</td></tr>';
					
					$details .= '</table>';
					?>
					<tr>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo $orders->formatId($row['id'], $row['query_id']); ?></td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo date('Y-m-d H:i', strtotime($row['add_date'])); ?></td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo sprintf("%0.2f", ($netto_sum + $tax_sum)); ?> zł</td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo $row['unimet_representative']; ?></td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>">
							<p>
							<a href="#" class="see_details" id="see_<?php echo $row['id']; ?>">zobacz szczegóły</a>
							</p>
						</td>
					</tr>
					<tr class="order_details" id="details_<?php echo $row['id']; ?>" style="display:none;">
					<td colspan="5"><?php echo $details; ?></td>
					</tr>
					<?php
				}
				
				$counter++;
			}
		}
	
		if (!$counter)
		{
			?>
			<tr><td colspan="5" class="bg_<?php echo (($counter%2)+1); ?>">brak danych</td></tr>
			<?php
		}
		?>
		</table>
		<?php /* ?>
		<p class="query_title"><strong>U Administratora</strong></p>
		<table class="proteges_list">
		<tr>
			<th style="width:100px">ID</th>
			<th style="width:200px">Data</th>
			<th style="width:200px">Wartość brutto</th>
			<th style="width:200px">Opiekun handlowy</th>
			<th>Operacje</th>
		</tr>
		<?php 
		$counter = 0;
		
		foreach ($list as $i => $row)
		{
			if ($_SESSION["b2b_user"]['admin'] == 1 && $row['status'] == 1)
			{
				if ($row['id'] != $list[$i-1]['id'])
				{
					$details = '
					<table cellspacing="0" cellpadding="0" style="width:100%;">
					<tr>
						<th><strong>MPK</strong></td>
						<th><strong>Nazwa produktu</strong></td>
						<th><strong>Ilość</strong></td>
						<th><strong>Cena netto</strong></td>
						<th><strong>Wartość netto</strong></td>
						<th><strong>Wartość brutto</strong></td>
					</tr>';
					
					$netto_sum  = 0;
					$tax_sum = 0;
				}
				
				$tax = ((int)$row['tax']*$row['netto_price'])/100;
				$product_tax = sprintf("%0.2f", $tax*$row['amount']);
				$netto_value = sprintf("%0.2f", ($row['netto_price'] * $row['amount']));
				$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
				
				$details .= '<tr>';
				$details .= '<td>'.$row['mpk'].'</td>';
				$details .= '<td>'.$row['name'].'</td>';
				$details .= '<td>'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'</td>';
				$details .= '<td>'.$row['netto_price'].' zł</td>';
				$details .= '<td>'.$netto_value.' zł</td>';
				$details .= '<td>'.$brutto_value.' zł</td>';
				$details .= '</tr>';
					
				$netto_sum += $netto_value;
				$tax_sum += $product_tax; 
				
				if ($row['id'] != $list[$i+1]['id'])
				{
					$details .= '<tr><td><strong>Komentarz</strong></td><td colspan="5" style="text-align:left">'.nl2br($row['order_comment']).'</td></tr>';
					
					$details .= '</table>';
					?>
					<tr>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo $row['id']; ?></td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo date('Y-m-d H:i', strtotime($row['add_date'])); ?></td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo sprintf("%0.2f", ($netto_sum + $tax_sum)); ?> zł</td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo $row['unimet_representative']; ?></td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>">
							<form method="post" action="<?php echo substr(BASE_ADDRESS, 0, -1).$_SERVER['REQUEST_URI']; ?>" name="query_operations_<?php echo $row['id']; ?>">
							<p style="text-align:left"><strong>Komentarz: </strong></p>
							<p><textarea cols="0" rows="0" name="comment" style="height:30px; width:100%"></textarea></p>
							<input type="hidden" name="id" value="<?php echo $row['id']; ?>" />
							<input type="hidden" name="query_operation" value="" />
							</form>
							<p>
							<a href="#" class="see_details" id="see_<?php echo $row['id']; ?>">zobacz szczegóły</a><span style="padding:0px 5px 0px 5px;">|</span> 
							<a class="accept_order" alt="<?php echo $row['id']; ?>" href="./zapytania,zatwierdz,<?php echo $row['id']; ?>.html">zatwierdź</a> <span style="padding:0px 5px 0px 5px;">|</span> 
							<a class="reject_order" alt="<?php echo $row['id']; ?>" href="./zapytania,odrzuc,<?php echo $row['id']; ?>.html">odrzuć</a>
							</p>
						</td>
					</tr>
					<tr class="order_details" id="details_<?php echo $row['id']; ?>" style="display:none;">
					<td colspan="5"><?php echo $details; ?></td>
					</tr>
					<?php
				}
				
				$counter++;
			}
		}
	
		if (!$counter)
		{
			?>
			<tr><td colspan="5" class="bg_<?php echo (($counter%2)+1); ?>">brak danych</td></tr>
			<?php
		}
		?>
		</table>
		
		<p class="query_title"><strong>Odpowiedzi</strong></p>
		<table class="proteges_list">
		<tr>
			<th style="width:100px">ID</th>
			<th style="width:200px">Data dodania</th>
			<th style="width:200px">Wartość brutto</th>
			<th style="width:200px">Opiekun handlowy</th>
			<th>Operacje</th>
		</tr>
		<?php 
		$counter = 0;
		
		foreach ($list as $i => $row)
		{
			if ($_SESSION["b2b_user"]['id'] == $row['user_id'] && $row['status'] == 2)
			{
				if ($row['id'] != $list[$i-1]['id'])
				{
					$details = '
					<table cellspacing="0" cellpadding="0" style="width:100%;">
					<tr>
						<th><strong>MPK</strong></td>
						<th><strong>Nazwa produktu</strong></td>
						<th><strong>Ilość</strong></td>
						<th><strong>Cena netto</strong></td>
						<th><strong>Wartość netto</strong></td>
						<th><strong>Wartość brutto</strong></td>
					</tr>';
					
					$netto_sum  = 0;
					$tax_sum = 0;
				}
				
				$tax = ((int)$row['tax']*$row['netto_price'])/100;
				$product_tax = sprintf("%0.2f", $tax*$row['amount']);
				$netto_value = sprintf("%0.2f", ($row['netto_price'] * $row['amount']));
				$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
				
				$details .= '<tr>';
				$details .= '<td>'.$row['mpk'].'</td>';
				$details .= '<td>'.$row['name'].'</td>';
				$details .= '<td>'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'</td>';
				$details .= '<td>'.$row['netto_price'].' zł</td>';
				$details .= '<td>'.$netto_value.' zł</td>';
				$details .= '<td>'.$brutto_value.' zł</td>';
				$details .= '</tr>';
					
				$netto_sum += $netto_value;
				$tax_sum += $product_tax; 
				
				if ($row['id'] != $list[$i+1]['id'])
				{
					$details .= '<tr><td><strong>Komentarz</strong></td><td colspan="5" style="text-align:left">'.nl2br($row['order_comment']).'</td></tr>';
					
					$details .= '</table>';
					?>
					<tr>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo $row['id']; ?></td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo date('Y-m-d H:i', strtotime($row['add_date'])); ?></td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo sprintf("%0.2f", ($netto_sum + $tax_sum)); ?> zł</td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo $row['unimet_representative']; ?></td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>">
							<p>
							<a href="#" class="see_details" id="see_<?php echo $row['id']; ?>">zobacz szczegóły</a> <span style="padding:0px 5px 0px 5px;">|</span> 
							<a href="./zapytania,zamow,<?php echo $row['id']; ?>.html">przenieś do koszyka</a>
							</p>
						</td>
					</tr>
					<tr class="order_details" id="details_<?php echo $row['id']; ?>" style="display:none;">
					<td colspan="5"><?php echo $details; ?></td>
					</tr>
					<?php
				}
				
				$counter++;
			}
		}
		
		if (!$counter)
		{
			?>
			<tr><td colspan="5" class="bg_<?php echo (($counter%2)+1); ?>">brak danych</td></tr>
			<?php
		}
		?>
		</table>
		<?php */ ?>
		<p class="query_title">
			<span><strong>Archiwum</strong></span>
			<?php
			foreach ((array)$year_list as $year)
			{
				?>
				<span class="year">
					<a href="/zapytania,lista,<?php echo $year; ?>.html" <?php echo (($year == $actual_year) ? 'class="active"' : ''); ?>>
					<strong><?php echo $year; ?></strong>
					</a>
				</span>
				<?php 
			} 
			?>
			<span class="clear"></span>
		</p>
		<table class="proteges_list">
		<tr>
			<th style="width:100px">ID</th>
			<th style="width:200px">Data dodania</th>
			<th style="width:200px">Wartość brutto</th>
			<th style="width:200px">Opiekun handlowy</th>
			<th>Operacje</th>
		</tr>
		<?php 
		$counter = 0;		

		foreach ($list as $i => $row)
		{
			if (($_SESSION["b2b_user"]['id'] == $row['user_id'] || $_SESSION["b2b_user"]['admin'] == 1 || $_SESSION["b2b_user"]['observer'] == 1) && in_array($row['status'], array(4,5)))
			{
				if ($row['id'] != $list[$i-1]['id'])
				{
					$details = '
					<table cellspacing="0" cellpadding="0" style="width:100%;">
					<tr>
						<th><strong>MPK</strong></th>
						<th><strong>Nazwa produktu</strong></th>
						<th><strong>Producent</strong></th>
						<th><strong>Opis</strong></th>
						<th><strong>Jedn. miary</strong></th>
						<th><strong>Ilość</strong></th>
						<th><strong>Cena netto</strong></th>
						<th><strong>Wartość netto</strong></th>
						<th><strong>Wartość brutto</strong></th>
					</tr>';
					
					$netto_sum  = 0;
					$tax_sum = 0;
				}
				
				$tax = ((int)$row['tax']*$row['netto_price'])/100;
				$product_tax = sprintf("%0.2f", $tax*$row['amount']);
				$netto_value = sprintf("%0.2f", ($row['netto_price'] * $row['amount']));
				$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
				
				$details .= '<tr>';
				$details .= '<td>'.$row['mpk'].'</td>';
				$details .= '<td>'.$row['name'].'</td>';
				$details .= '<td>'.$row['producer'].'</td>';
				$details .= '<td>'.$row['comment'].'</td>';
				$details .= '<td>';
				
				switch ($row['unit'])
				{
					case 1: $details .= 'kilogram'; break;
					case 2: $details .= 'komplet'; break;
					case 3: $details .= 'm2'; break;
					case 4: $details .= 'm3'; break;
					case 5: $details .= 'mb'; break;
					case 6: $details .= 'opakowanie'; break;
					case 7: $details .= 'para'; break;
					case 8: $details .= 'sztuka'; break;
				}
				
				$details .= '</td>';
				$details .= '<td>'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'</td>';
				$details .= '<td>'.$row['netto_price'].' zł</td>';
				$details .= '<td>'.$netto_value.' zł</td>';
				$details .= '<td>'.$brutto_value.' zł</td>';
				$details .= '</tr>';
					
				$netto_sum += $netto_value;
				$tax_sum += $product_tax; 
				
				if ($row['id'] != $list[$i+1]['id'])
				{
					$details .= '<tr><td><strong>Komentarz</strong></td><td colspan="7" style="text-align:left">'.nl2br($row['order_comment']).'</td></tr>';
					
					$details .= '</table>';
					?>
					<tr>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo $orders->formatId($row['id'], $row['query_id']); ?></td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo date('Y-m-d H:i', strtotime($row['add_date'])); ?></td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo sprintf("%0.2f", ($netto_sum + $tax_sum)); ?> zł</td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>"><?php echo $row['unimet_representative']; ?></td>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="<?php if ($netto_sum) { echo 'background:#e6ecf5'; } ?>">
							<p>
							<a href="#" class="see_details" id="see_<?php echo $row['id']; ?>">zobacz szczegóły</a>
							</p>
						</td>
					</tr>
					<tr class="order_details" id="details_<?php echo $row['id']; ?>" style="display:none;">
					<td colspan="6"><?php echo $details; ?></td>
					</tr>
					<?php
				}
				
				$counter++;
			}
		}
		
		if (!$counter)
		{
			?>
			<tr><td colspan="5" class="bg_<?php echo (($counter%2)+1); ?>">brak danych</td></tr>
			<?php
		}
		?>
		</table>
		<?php
	}
	?>	

	</div>
	
	</div>
	<?php
}
?>

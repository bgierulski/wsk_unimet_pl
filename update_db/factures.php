<?php
$result = ibase_query('SELECT DATA, DOKUMENT, TERMIN, KON, ROZL, AKW, WINIEN, MA, WINIEN2, MA2, TYP FROM MAT_OEW WHERE TYP IN (\'FV\', \'KF\', \'RF\', \'KC\')');

$factures_counter = 0;

while($row = ibase_fetch_assoc($result)) 
{
	$factures_query .= '(\'\', '.(int)$row['KON'].', '.(int)$row['AKW'].', \''.strtoupper(iconv("windows-1250", "UTF-8", trim(mysql_escape_string($row['DOKUMENT'])))).'\', ';
	$factures_query .= '\''.date('Y-m-d', strtotime($row['DATA'])).'\', \''.date('Y-m-d', strtotime($row['TERMIN'])).'\', ';
	$factures_query .= '\''.((float)$row['WINIEN'] + (float)$row['WINIEN2'] - (float)$row['MA'] - (float)$row['MA2']).'\', '.(($row['ROZL'] == 'T') ? 1 : 0).', ';
	$factures_query .= '\''.$row['TYP'].'\'),';
	
	if ($factures_counter == 0)
	{
		$db->query('TRUNCATE TABLE factures') or $db->raise_error();
	}
		
	if ($factures_counter > 500)
	{
		$db->query(substr('INSERT INTO factures VALUES '.$factures_query, 0, -1)) or $db->raise_error();
		$factures_query = '';
			
		$factures_counter = 0;
	}
	
	$factures_counter++;
}

if ($factures_counter)
{
	if (!empty($factures_query))
	{
		$db->query(substr('INSERT INTO factures VALUES '.$factures_query, 0, -1)) or $db->raise_error();
	}
}
?>
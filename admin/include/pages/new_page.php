<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<label style="width:120px;">&nbsp;</label>
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
	<div class="clear"></div>
</div>
<?php } ?>

<?php
$page_id = (int)$_GET['id']; 

if (!isset($_POST['add_page']) && $action != 'edit')
{
	$pages->clear();
}
if (!isset($_POST['save_changes']) && $action == 'edit')
{
	$pages->getPage($page_id);
}
?>

<form method="POST" action="">

<input type="hidden" name="page_id" value="<?php echo $page_id; ?>" />
	
	<?php  if ($_SESSION["errors"]['title']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['title']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Tytuł podstrony</label>
		<input type="text" name="title" value="<?php echo $pages->textControll($_SESSION["values"]['title']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['description']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['description']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Treść podstrony</label>
		<div style="float:left;">
			<?php
			$oFCKeditor = new FCKeditor('content');
			$oFCKeditor->BasePath = './fckeditor/';
			$oFCKeditor->Value = $_SESSION["values"]['content'];
			$oFCKeditor->Width = 1000;
			$oFCKeditor->Height = 800;
			$oFCKeditor->ToolbarSet = 'Page';
			$oFCKeditor->Create();
			?>
		</div>
		<div class="clear"></div>
	</div>
	
	<div class="form_element">
		<label style="width:120px;">&nbsp;</label>
		<?php if ($action == 'edit') { ?>
		<input id="submit" name="save_changes" value="Zapisz zmiany" type="submit" /> 
		<?php } else { ?>
		<input id="submit" name="add_page" value="Dodaj podstrone" type="submit" /> 
		<?php } ?>
		<div class="clear"></div>
	</div>
</form> 
<div id="page_header">
	<div class="details">
		<p><strong>Szablony zamówień</strong></p>
	</div>
	<div class="clear"></div>
</div>

<div id="page_content">
<div id="form">
<?php 
if (isset($_SESSION["communicats"]['error'])) 
{ 
	?><div class="element its_error"><span><?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?></span></div><?php 
} 
else if (isset($_SESSION["communicats"]['ok'])) 
{ 
	?><div class="element its_ok"><span><?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?></span></div><?php 
}
?>
</div>
<?php

$templates_list = $templates->getTemplates();
$templates_count = count($templates_list);
$counter = 0;

if ($templates_count)
{
	?>
	<table class="proteges_list">
	<tr>
		<th>Data dodania</th>
		<th>Dostępne operacje</th>
	</tr>
	<?php
	foreach ($templates_list as $i => $template)
	{
		if ($template['template_id'] != $templates_list[$i-1]['template_id'])
		{
			$details = '
			<table cellspacing="0" cellpadding="0" style="width:100%;">
			<tr>
				<th><strong>Nazwa produktu</strong></td>
				<th><strong>Ilość</strong></td>
			</tr>';
		}
		
		$details .= '<tr>';
		$details .= '<td>'.$template['name'].'</td>';
		$details .= '<td>'.$template['amount'].'</td>';
		$details .= '</tr>';
		
		if ($template['template_id'] != $templates_list[$i+1]['template_id'])
		{
			$details .= '</table>';
			
			?>
			<tr>
				<td class="bg_<?php echo (($counter%2)+1); ?>"><?php echo $template['add_date']; ?></td>
				<td class="bg_<?php echo (($counter%2)+1); ?>">
				<a href="#" class="see_details" id="see_<?php echo ($counter+1); ?>">zobacz szczegóły</a> <span style="padding:0px 5px 0px 5px;">|</span>
				<a href="./szablony,wczytaj,<?php echo $template['template_id']; ?>.html">wczytaj szablon</a> <span style="padding:0px 5px 0px 5px;">|</span> 
				<a href="./szablony,usun,<?php echo $template['template_id']; ?>.html">usuń szablon</a>
				</td>
			</tr>
			<tr class="order_details" id="details_<?php echo ($counter+1); ?>" style="display:none;">
			<td colspan="2"><?php echo $details; ?></td>
			</tr>
			<?php
			
			$counter++;
		}
	}
	?>
	</table>
	<?php
}
?>
</div>
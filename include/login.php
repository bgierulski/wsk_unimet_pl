<div id="page_header" style="background:none;">
	<div class="details">
		<p><strong></strong> <?php if ($_SESSION["communicats"]['ok']) echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?></p>
	</div>
	<div class="clear"></div>
</div>

<div id="page_content" style="padding:10px 0px 10px 0px;">

	<?php 
	$page = $pages->getPage(4);
	?>

	<div style="width:398px; margin:0 auto;"><?php echo str_replace(array('\"', "\'"), array('"', "'"), $page['content']); ?></div>

	<div id="login_form">
		
		<noscript>
				<div style="display:none;">
		</noscript>
		
		<div id="description_box">
			<p><img src="../admin/images/security.png" alt="" /></p>
			<div>
				<p>Aby uzyskać dostęp proszę wprowadź poprawne hasło i login i nacisnij Loguj.</p>
			</div>
		</div>
		<div id="form_content">
			<div id="form_background">
			
			
				<form action="<?php echo BASE_ADDRESS; ?>login/index.php" method="POST" target="b2b" onSubmit="window.open('', 'b2b', 'width='+screen.availWidth-10+',height='+screen.availHeight-55+',status=yes,resizable=yes,scrollbars=yes');">
					<div class="form_element">
						<label>Login:</label>

						<input type="text" name="login" value="" />
						<div class="clear"></div>
					</div>
					<div class="form_element">
						<label>Hasło:</label>
						<input type="password" name="password" value="" />
					</div>
					<div class="form_element">

						<label>&nbsp;</label>
						<input type="hidden" name="operation" value="login" />
						<input class="submit" type="submit" name="log_in" value="Zaloguj" />
					</div>
				</form>
				
				
				
			</div>
		</div>
		<div class="clear"></div>
		
		<noscript>
			</div>
			<div style="padding:10px;">
				<span style="font-size:16px;">Przepraszamy ale Twoja przeglądarka nie spełnia wymagań technicznych. <Br><br>Prosimy o kontakt z naszą pomocą techniczną : <br>
				<b>Maciej Wojdyło
				<br>e-mail: wskadmin@unimet.pl, <br>tel: 17 230 66 75
				</span>
			</div>
		</noscript>
	
	</div>
</div>
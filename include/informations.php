<div id="page_header" style="background:none;">
	<div class="details">
		<p><strong>Komunikaty</strong></p>
	</div>
	<div class="clear"></div>
</div>

<div id="page_content">

<?php 
$informations_list = $informations->getInformations(1);

if (is_array($informations_list) && !empty($informations_list))
{
	?>
	<table id="communicats_list">
	<tr>
		<th>Temat komunikatu</th>
		<th>Operacje</th>
	</tr>
	<?php 
	foreach ($informations_list as $i => $information)
	{
		?>
		<tr>
			<td class="bg_<?php echo (($i%2)+1); ?>">
			<div class="communicat_short" id="short_<?php echo $information['id'] ?>"><?php echo $information['title']; ?></div>
			<div class="communicat_content" id="content_<?php echo $information['id'] ?>"><?php echo $informations->text($information['content']); ?></div>
			</td>
			<td class="bg_<?php echo (($i%2)+1); ?>" style="vertical-align:top"><a class="communicat_operation" id="communicat_operation_<?php echo $information['id'] ?>" href="./informacje.html">rozwiń</a></td>
		</tr>
		<?php
	}
	?>
	</table>
	<?php 
}
else
{
	?>Brak komunikatów<?php
}
?>


</div>

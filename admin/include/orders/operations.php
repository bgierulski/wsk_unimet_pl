<?php
$orders->checkReplacements();

if (in_array($_POST['operation'], array("regroup_order", "save_changes", "return_products", "move_to_archive")))
{
	$orders->validateInformations();
}
else if ($id == 'to_contractor')
{
	$orders->changeStatus((int)$param, 9);
}
else if ($id == 'to_analyst')
{
	$orders->changeStatus((int)$param, 6);
}
else if ($id == 'to_realization')
{
	$orders->changeStatus((int)$param, 2);
}
else if ($id == 'to_realized')
{
	$orders->changeStatus((int)$param, 3);
}
else if ($id == 'to_archiwum')
{
	$orders->changeStatus((int)$param, 4);
}
else if ($id == 'delete')
{
	$orders->changeStatus((int)$param, 5);
}
else if ($action == 'waiting' && $id == 'to_realization')
{
	$orders->moveAllTorealization();
}
?>
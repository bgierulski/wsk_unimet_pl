<?php
$list = $concessions->getConcessions();
?>

<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
</div>
<?php } else if (isset($_SESSION["communicats"]['ok'])) { ?>
<div class="form_element ok">
	<?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?>
</div>
<?php } ?>

<table>
	<tr>
		<th>Imię i nazwisko</th>
		<th>E-mail</th>
		<th>Telefon</th>
		<th>Operacje</th>
	</tr>
	<?php
	if (is_array($list))
	{
		foreach ($list as $key => $row)
	   	{
			?>
		    <tr>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['name']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo ((!empty($row['email'])) ? $row['email'] : '&nbsp;'); ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo ((!empty($row['phone'])) ? $row['phone'] : '&nbsp;'); ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>">
				<a href="./concessions/edit/<?php echo $row['id']; ?>">edytuj dane</a>
				</td>
			</tr>
		    <?php
	   	}
	}
	?>
</table>

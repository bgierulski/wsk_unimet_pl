<?php
class Pages extends Validators 
{
	private $_db;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
	}
	
	public function getMarqee()
	{
		$result = $this->_db->query('SELECT marquee FROM b2b_details WHERE id = 1') or $this->_db->raise_error();
		$row = mysql_fetch_array($result);
		
		return str_replace('/\<br(\s*)?\/?\>/i', " ", $row['marquee']);
	}
	
	public function getPages()
	{
		$query = 'SELECT id, title FROM pages WHERE id IN (6,13,15)';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getPage($page_id)
	{
		if (in_array($page_id, array(3,4)))
		{
			switch ($page_id)
			{
				case 3: $page_id = 14; break;
				case 4: $page_id = 15; break;
			}
		}
		
		$result = $this->_db->query('SELECT title, content FROM pages WHERE id = '.$page_id) or $this->_db->raise_error();
		$row = mysql_fetch_array($result);
		
		return $row;
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}

$pages = new Pages();
?>
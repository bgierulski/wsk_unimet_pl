<div id="form" class="ask_about">

<form action="<?php echo substr(BASE_ADDRESS, 0, -1).$_SERVER['REQUEST_URI']; ?>" method="POST" name="ask_about">
	<input type="hidden" name="operation" value="ask_about" />
	<input type="hidden" name="id" value="<?php echo $product['id_hermes']; ?>" />
	
	<?php if (isset($_SESSION["communicats"]['error'])) { ?>
	<div class="element its_error">
	<label style="width:95px;">&nbsp;</label>
	<span>
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
	</span>
	</div>
	<?php } else if (isset($_SESSION["communicats"]['ok'])) { ?>
	<div class="element its_ok">
	<label style="width:95px;">&nbsp;</label>
	<span>
	<?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?>
	</span>
	</div>
	<?php } ?>
	
	<?php 
	if ($_SESSION["values"]['product_name'] == '')
	{
		$_SESSION["values"]['product_name'] = $product['name'];
	}
	
	$guardian_name = $users->getGuardian();
	$concession_name = $products->getConcession($product['id_hermes']);
	
	if (!empty($guardian_name))
	{
		?>
		<div class="element">
		<label style="width:95px;">&nbsp;</label>Wiadomość do: <?php echo $guardian_name; ?>
		<div class="clear"></div>
		</div>
		<?php 
	}
	
	if ($_SESSION["errors"]['product_name'] != '') { ?>
	<div class="element error">
	<label style="width:95px;">&nbsp;</label><?php echo $_SESSION["errors"]['product_name']; ?></div>
	<?php } ?>
	<div class="element">
	<label style="width:95px;">Produkt <strong class="error">(*)</strong></label>
	<input name="name" value="<?php echo $_SESSION["values"]['product_name']; ?>" type="text" style="width:400px;" readonly="true" />
	<div class="clear"></div>
	</div>
	
	<?php if ($_SESSION["errors"]['product_email'] != '') { ?>
	<div class="element error">
	<label style="width:95px;">&nbsp;</label><?php echo $_SESSION["errors"]['product_email']; ?></div>
	<?php } ?>
	
	<?php if (empty($_SESSION["values"]['product_email'])) { $_SESSION["values"]['product_email'] = $_SESSION["b2b_user"]['email']; } ?>
	
	<div class="element">
	<label style="width:95px;">Twój e-mail <strong class="error">(*)</strong></label>
	<input name="email" value="<?php echo $_SESSION["values"]['product_email']; ?>" type="text" style="width:400px;" />
	<div class="clear"></div>
	</div>
	
	<?php if ($_SESSION["errors"]['product_message'] != '') { ?>
	<div class="element error">
	<label style="width:95px;">&nbsp;</label><?php echo $_SESSION["errors"]['product_message']; ?></div>
	<?php } ?>
	<div class="element">
	<label style="width:95px;">Wiadomość <strong class="error">(*)</strong></label>
	<textarea cols="" rows="" name="message" style="width:400px;"><?php echo $_SESSION["values"]['product_message']; ?></textarea>
	<div class="clear"></div>
	</div>
	
	<div class="element">
	<label style="width:95px;">&nbsp;</label>
	<p class="button">
	<a href="./" id="ask_about_form_submit">WYŚLIJ WIADOMOŚĆ</a>
	</p>
	</div>
	
</form>
</div>
var window_width;

function  round(n, k) 
{
	var factor = Math.pow(10, k);
	return Math.round(n*factor)/factor;
}

function goMain(url)
{
	$.ajax({
		
		type: "POST",
		url: "js/xml.php",
		data: {operation:'get_request_uri'},
		dataType: "xml",
		
		success: function(xml){
			
			if ($(xml).find("request_uri").length)
			{
				window.opener.location = $(xml).find("request_uri").text();
			}
			else
			{
				window.opener.location.reload();
			}
		}
	});
}

function pop_up(product_id)
{
	var windowWidth = 850;
	var windowHeight = 630;
	var windowUri = 'product.php?id='+parseInt(product_id);
	
	var centerWidth = (window.screen.width - windowWidth) / 2;
    var centerHeight = (window.screen.height - windowHeight) / 2;

    newWindow = window.open(windowUri, 'Galeria'+product_id, 'scrollbars=1,resizable=1,width=' + windowWidth + 
        ',height=' + windowHeight + 
        ',left=' + centerWidth + 
        ',top=' + centerHeight);

    newWindow.focus();
    
    return false;
}

function favorite_products()
{
	var windowWidth = 850;
	var windowHeight = 630;
	var windowUri = 'include/favorite_products.php';
	
	var centerWidth = (window.screen.width - windowWidth) / 2;
    var centerHeight = (window.screen.height - windowHeight) / 2;
    
    newWindow = window.open(windowUri, 'Produkty_ulubione', 'scrollbars=1,resizable=1,width=' + windowWidth + 
            ',height=' + windowHeight + 
            ',left=' + centerWidth + 
            ',top=' + centerHeight);

    newWindow.focus();
    
    return false;
}

function contentHeight()
{
	setTimeout(function(){	
		var min_height = parseInt($(window).height()) - parseInt($("#top_content").height()) - parseInt($("#menu_content").height()) - parseInt($("#horizontal_list").height()) - 42;
		
		$("body").ready(function() {
			
			$(this).find("#main_content").css("min-height", min_height+"px");
			$(this).find("#groups").css("height", (min_height - 184)+"px");
			
			if ($.browser.msie && $.browser.version == 7)
			{
				$(this).find("#products_content").css("height", (min_height - parseInt($("#producers").height()) - 72)+"px");
			}
			else
			{
				$(this).find("#products_content").css("height", (min_height - parseInt($("#producers").height()) - 106)+"px");
			}
			
			if (parseInt($(this).find("#products_content table").height()) > parseInt($(this).find("#products_content").height()))
			{
				$(this).find(".subgroup:first").css("padding-right","19px");
			}
		});
		
		window_width = parseInt($(window).width());
		
		if (window_width < 1400)
		{
			$("#above_1024").css("display", "none");
			$("#below_1024").css({"display":"block"});
			$("#menu_content a").css("font-size", "11px");
			$("#page_content .subgroup").css("font-size", "11px");
			$("#logotype img").attr("src", "images/logotype_small.png");
			$(".person").css({"font-size":"9px"});
		}
		else
		{
			$("#below_1024").css("display","none");
			$("#above_1024").css("display","block");
			$("#menu_content a").css("font-size", "13px");
			$("#page_content .subgroup").css("font-size", "13px");
			$("#logotype img").attr("src", "images/logotype.png");
			$(".person").css("font-size", "13px");
		}
		
		if ($("#export_content").length)
		{
			var column_height = min_height - 40;

			$("#export_content").css({"width":$("#page_content").width()+"px", "height":column_height+"px"});
			$("#export_content div:first").css({"width":$("#export_content table:first").width()});
		}
		
		if ($("#budget_content").length)
		{
			$("#budget_content iframe").css({"height": (min_height - 40) + "px"});
		}
		
		if ($(".archive_content").length)
		{
			$(".archive_content").css({"width":"100%", "min-height":"200px"});
		}
		
	},  100);
}

function str_replace(f, r, s) // funkcja zamienia elementy ciągu
{
    var ra = r instanceof Array, sa = s instanceof Array, l = (f = [].concat(f)).length, r = [].concat(r), i = (s = [].concat(s)).length;

    while(j = 0, i--)
    while(s[i] = s[i].split(f[j]).join(ra ? r[j] || "" : r[0]), ++j < l);
    return sa ? s : s[0];
}

function strstr(haystack, needle, bool) {
    // Finds first occurrence of a string within another  
    // 
    // version: 1006.1915
    // discuss at: http://phpjs.org/functions/strstr    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   bugfixed by: Onno Marsman
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

    var pos = 0;
    
    haystack += '';
    pos = haystack.indexOf(needle);    
    if (pos == -1) 
    {
        return false;
    } 
    else
    {
        if (bool)
        {
            return haystack.substr(0, pos);        
        } 
        else
        {
            return haystack.slice(pos);
        }
    }
}

function scrollGroups()
{	
	if ($("#groups a.active").length)
	{
		var position = $("#groups a.active").parent().offset();
		
		$("#groups").animate({scrollTop:(parseInt(position.top) - 288)}, 'slow'); 
	}
}

function checkedProducts()
{
	if (cookie_products != null)
	{
		var cookie_table = cookie_products.split('##');
		var count = cookie_table.length;
			
		$(".subgroup:last").ready(function() {
			for (var i=1; i<count; i++)
			{
				var element = cookie_table[i].split('#');
					
				$(this).find("input[id='count_"+element[1]+"']").val(element[0]);
				$(this).find("input[id='active_"+element[1]+"']").attr("checked", true);
			}
		});
	}
}

function in_array(needle, haystack)
{
	for(var i=0; i<haystack.length; i++)
	{
		if(needle == haystack[i])
		{
			return true;
		}
	}

	return false;
}
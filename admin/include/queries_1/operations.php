<?php
include(BASE_DIR.'queries/class.SpecialOrders.php');
$special_orders = new SpecialOrders();

if (isset($_POST['save_changes']))
{
	$special_orders->validateOrder();
}
else if ($action == 'delete')
{
	$special_orders->deleteOrder((int)$id);
}
else if ($action == 'confirm')
{
	$special_orders->confirmOrder((int)$id);
}
?>
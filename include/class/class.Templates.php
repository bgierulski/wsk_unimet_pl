<?php
class Templates extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_user = array();

	private $_product = array();
	private $_basket_products = array();
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		
		$this->_user = $this->getSession('b2b_user');
		$this->_basket_products = $this->getSession("basket_products");
		
		if ($_GET['param1'] == 'szablony' && (int)$_GET['param3'])
		{
			switch ($_GET['param2'])
			{
				case 'wczytaj':
					$this->getTemplate((int)$_GET['param3']);
					break;
					
				case 'usun':
					$this->deleteTemplate((int)$_GET['param3']);
					break;
			}
		}
	}
	
	public function deleteTemplate($template_id)
	{
		$result = $this->_db->query('SELECT id FROM templates WHERE id = '.$template_id.' AND user_id = '.(int)$this->_user['id']) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$this->_db->query('DELETE FROM templates WHERE id = '.$template_id) or $this->_db->raise_error();
			
			$communicats['ok'] = 'Szablon został usunięty.';
			$this->setSession("communicats", $communicats);
		}
	}
	
	public function getTemplate($template_id)
	{
		$template = $this->_db->query('SELECT product_id, template_details.amount FROM templates JOIN template_details ON templates.id = template_details.template_id JOIN products ON template_details.product_id = products.id_hermes WHERE template_id = '.$template_id.' AND user_id = '.$this->_user['id']) or $this->_db->raise_error();
		
		while($row = mysql_fetch_array($template)) 
   		{
   			$added = false;	
   		
   			$result = $this->_db->query('SELECT product_versions.product_id, groups_tree.name as subgroup_name, producers.name as producer_name, product_versions.name, symbol, unit, 
       		tax, weight, pallet FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id JOIN groups_tree ON 
       		products.subgroup_id = groups_tree.id JOIN producers ON products.producer_id = producers.id WHERE products.id_hermes = '.$row['product_id']) 
       		or $this->_db->raise_error();
       		
       		$product = mysql_fetch_array($result);	
   		
   			foreach ($this->_basket_products as $key => $element)
			{
				if ($product['product_id'] == $element['product_id'])
				{
					$this->_basket_products[$key]['count'] += $row['amount']; 
					$this->_basket_products[$key]['brutto_value'] = sprintf("%0.2f", $this->_basket_products[$key]['brutto_price'] * $this->_basket_products[$key]['count']);
					
					$added = true;
				}
			}
       		
			if (false === $added)
			{
				$this->_product['product_id'] = $row['product_id'];
				$this->_product['subgroup'] = $product['subgroup_name'];
				$this->_product['name'] = $product['name'];
				$this->_product['index'] = $product['symbol'];
				$this->_product['tax'] = $product['tax'];
				$this->_product['weight'] = $product['wieght'];
				$this->_product['size'] = (($product['pallet'] == 1) ? 'T' : 'N');
				$this->_product['unit'] = strtolower($product['unit']);
				
				$this->_product['count'] = $row['amount'];
					
				$prices = new Prices();
					
				$this->_product['netto_price'] = $prices->getPrice($row['product_id'], 4);
				$this->_product['netto_value'] = sprintf("%0.2f", $this->_product['netto_price']*$this->_product['count']);
				$this->_product['brutto_price'] = $prices->getPrice($row['product_id'], 1);
				$this->_product['brutto_value'] = sprintf("%0.2f", $this->_product['brutto_price']*$this->_product['count']);
				
				// add product to basket
				if (!empty($this->_product)) 
				{
					$this->_basket_products[count($this->_basket_products)] = $this->_product;
				}
			}
   		}
   		
   		$this->setSession("basket_products", $this->_basket_products);
	}
	
	public function getTemplates()
	{
		$result = $this->_db->query('SELECT template_id, add_date, name, amount FROM templates JOIN template_details ON templates.id = 
		template_details.template_id JOIN product_versions ON template_details.product_id = product_versions.product_id WHERE user_id = '.
		$this->_user['id'].' ORDER BY add_date DESC, template_id DESC') or $this->_db->raise_error();
		
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function saveTemplate()
	{
		if (is_array($this->_basket_products))
		{
			$result = $this->_db->query('INSERT INTO templates VALUES (\'\', '.$this->_user['id'].', \''.date('Y-m-d').'\')') or $this->_db->raise_error();
				
			$template_id = mysql_insert_id();
			
			foreach ($this->_basket_products as $product)
			{
				$result = $this->_db->query('SELECT id FROM products WHERE id_hermes = '.(int)$product['product_id']) or $this->_db->raise_error();
				
				if (mysql_num_rows($result))
				{
					$this->_db->query('INSERT INTO template_details VALUES (\'\', '.$template_id.', '.$product['product_id'].', '.$product['count'].')') or $this->_db->raise_error();
				}
				
				mysql_free_result($result);
			}
			
			if ($result)
			{
				$communicats['ok'] = 'Szablon został zapisany.';
				$this->setSession("communicats", $communicats);
			}
		}
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}

$templates = new Templates();
?>
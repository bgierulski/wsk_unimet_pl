<?php
$list = $producers->getProducers();
?>

<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
</div>
<?php } else if (isset($_SESSION["communicats"]['ok'])) { ?>
<div class="form_element ok">
	<?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?>
</div>
<?php } ?>

<table>
	<tr>
		<th>Nazwa producenta</th>
		<th>Skrócony opis</th>
		<th>Adres www</th>
		<th>Operacje</th>
	</tr>
	<?php
	if (is_array($list))
	{
		foreach ($list as $key => $row)
	   	{
			?>
		    <tr>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['name']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['short']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php 
				if (!empty($row['www'])) 
				{
					echo '<a href="'.$row['www'].'" target="blank">'.$row['www'].'</a>';
				}
				?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>">
				<a class="usun" href="./producers/delete/<?php echo $row['id']; ?>">usuń producenta</a> &nbsp;::&nbsp;
				<a href="./producers/edit/<?php echo $row['id']; ?>">edytuj producenta</a>
				</td>
			</tr>
		    <?php
	   	}
	}
	?>
</table>

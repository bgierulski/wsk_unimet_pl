<?php
session_start();

header ('Content-Type: application/xml; charset=utf-8');

include(dirname(__FILE__).'/../config.php');
include(BASE_DIR.'default/class.DataBase.php');
include(BASE_DIR.'default/class.Validators.php');
include(BASE_DIR.'orders/class.Orders.php');

$orders = new Orders();

$operation = trim($_POST['operation']);

?>
<response>
<?php 
switch ($operation)
{
	case "find_replacments": 
		
		$words = $orders->findProducts($_POST['key']); 
		
		if (mysql_num_rows($words))
		{
			while($word = mysql_fetch_array($words)) 
	   		{
	       			$xml .= '<word><![CDATA['.$word['symbol'].' ('.$word['name'].')]]></word>';
	   		}
		}
		
		break;
		
	case "check_replacment":
		
		$data = $orders->getProduct($_POST['name']);
		
		if (is_array($data))
		{
			$xml .= '<id>'.$data['id_hermes'].'</id>'."\n";		
			$xml .= '<name><![CDATA['.$data['name'].']]></name>'."\n";		
		}
		
		break;
		
	case "get_trader_comment":
	
		$data = $orders->getTraderComment((int)$_POST['id']);
		
		if (is_array($data))
		{
			$xml .= '<id>'.$data['id'].'</id>'."\n";	
			$xml .= '<comment><![CDATA['.$orders->textClear($data['details']).']]></comment>'."\n";
		}
	
		break;
		
	case "set_trader_comment":
	
		$orders->setTraderComment((int)$_POST['id'], $_POST['comment']);
	
		break;
		
	case "set_year":
		$_SESSION["b2b_orders"][$_POST['type']] = (int)$_POST['year'];
		break;
		
	case "get_reject_comment":
	
		$id = (int)$_POST['id'];
	
		$data = $orders->getRejectComment($id);

		$xml .= '<id>'.$id.'</id>'."\n";	

		if (is_array($data))
		{
			$xml .= '<comment><![CDATA['.$orders->textClear($data['reject_comment']).']]></comment>'."\n";
		}
	
		break;
		
	case "set_reject_comment":
	
		if ($orders->validateTextarea($_POST['comment']))
		{
			$orders->setRejectComment((int)$_POST['id'], $_POST['comment']);
			$xml .= '<status>1</status>'."\n";	
		}
		else
		{
			$xml .= '<status>0</status>'."\n";	
		}
		break;
		
	case "set_hermes":
		$orders->changeHermesSettings((int)$_POST['id'], (int)$_POST['status']);
		break;
}

echo $xml;
?>

</response>
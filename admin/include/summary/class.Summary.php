<?php
class Summary extends Validators 
{
	private $_db;
	private $_errors;
	private $_user;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		$this->_user = $this->getSession("admin");
	}
	
	public function exportSummary($sorts, $types, $like, $columns, $selected)
	{
		$excel = new PHPExcel();
		
		$excel->setActiveSheetIndex(0);
		
		$excel->getActiveSheet()->setCellValue('B2', 'Termin wygenerowania zestawienia:');
		$excel->getActiveSheet()->setCellValue('C2', date('Y-m-d H:i'));
		$excel->getActiveSheet()->getStyle('C2')->getFont()->setBold(true);
		
		$excel->getActiveSheet()->setCellValue('B3', 'Zestawienie wygenerował:');
		$excel->getActiveSheet()->setCellValue('C3', $this->_user['login']);
		$excel->getActiveSheet()->getStyle('C3')->getFont()->setBold(true);
		
		$excel->getActiveSheet()->getStyle('B2:C2')->applyFromArray(
			array('borders' => array(
				'top' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')), 
				'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')),
				'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9'))
			)
		));
		
		$excel->getActiveSheet()->getStyle('B3:C3')->applyFromArray(
			array('borders' => array(
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')), 
				'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')),
				'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9'))
			)
		));
		
		if (is_array($types) && !empty($types) && is_array($like) && !empty($like))
		{
			$excel->getActiveSheet()->setCellValue('B5', 'Ustawione filtry: ');
			$excel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
			
			$excel->getActiveSheet()->setCellValue('B6', 'Kolumna');
			$excel->getActiveSheet()->setCellValue('C6', 'Szukana fraza');
			$excel->getActiveSheet()->setCellValue('D6', 'Rodzaj zawężenia');
			
			$excel->getActiveSheet()->getStyle('B6')->getFont()->setBold(true);
			$excel->getActiveSheet()->getStyle('C6')->getFont()->setBold(true);
			$excel->getActiveSheet()->getStyle('D6')->getFont()->setBold(true);
			
			$excel->getActiveSheet()->getStyle('B6:D6')->applyFromArray(
				array('borders' => array(
					'top' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')), 
					'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')),
					'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9'))
				)
			));
			
			$counter = 7;
			
			foreach ($types as $key => $type)
			{
				switch ($key)
				{
					case "order_informations.fv": $column = 'FV'; break;
					case "order_informations.wm": $column = 'WZ'; break;
					case "orders.add_date": $column = 'Data złożenia'; break;
					case "orders.status": $column = 'Status zamówienia'; break;
					case "order_details.order_id": $column = 'Nr zamówienia'; break;
					case "orders.number": $column = 'Nr wewnętrzny'; break;
					case "order_details.query_id": $column = 'Nr zapytania'; break;
					case "contractor_login": $column = 'Zamawiający'; break;
					case "wsk_budget.department": $column = 'Wydział'; break;
					case "wsk_mpk.mpk": $column = 'Numer MPK'; break;
					case "order_details.name": $column = 'Nazwa produktu'; break;
					case "order_details.symbol": $column = 'Indeks produktu'; break;
					case "order_details.producer": $column = 'Producent'; break;
					case "order_details.amount": $column = 'Ilość zamówionych produktów'; break;
					case "order_details.tax": $column = 'Podatek VAT (%)'; break;
					case "order_details.netto_price": $column = 'Cena netto (zł)'; break;
				}
				
				$value = $like[$key];
				
				if (!empty($value))
				{
					$excel->getActiveSheet()->setCellValue('B'.$counter, $column);
					
					if (in_array($key, array("order_details.netto_price", "order_details.tax", "order_details.amount")))
					{
						$value = (float)str_replace(',', '.', $value);
					}
					
					if ($key == "orders.status")
					{
						switch ($value)
						{
							case 1: $value = 'oczekujące w Unimet'; break;
							case 2: $value = 'w trakcie realizacji'; break;
							case 3: $value = 'w drodze'; break;
							case 4: $value = 'archiwum'; break;
							case 5: $value = 'odrzucone'; break;
							case 6: $value = 'u analityka'; break;
							case 7: $value = 'u kierownika'; break;
							case 8: $value = 'weryfikacja w Unimet'; break;
							case 9: $value = 'u zamawiającego'; break;
							case 10: $value = 'u administratora'; break;
						}
					}
					
					$excel->getActiveSheet()->setCellValue('C'.$counter, $value);
					
					switch ((int)$type)
					{
						case 3: $comparison = 'równy'; break;
						case 2: $comparison = 'większy'; break;
						case -1: $comparison = 'mniejszy'; break;
						default: $comparison = 'zawiera'; break;
					}
					
					$excel->getActiveSheet()->setCellValue('D'.$counter, $comparison);
					
					$excel->getActiveSheet()->getStyle('B'.$counter.':D'.$counter)->applyFromArray(
						array('borders' => array(
							'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9')),
							'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9'))
						)
					));
					
					$counter;
				}
			}
		}
		
		$excel->getActiveSheet()->getStyle('B'.$counter.':D'.$counter)->applyFromArray(
			array('borders' => array(
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ff0753a9'))
			)
		));
		
		$counter++;
		$counter++;
		
		if (is_array($columns) && !empty($columns))
		{
			$alphabet = array('B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T');
			$data_sum = array();
			$indeks = 0;
			
			if ((int)$columns['orders.status'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Status zamówienia");
				$indeks++;
			}
			if ((int)$columns['order_details.order_id'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Nr zamówienia");
				$indeks++;
			}
			if ((int)$columns['orders.number'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Nr wewnętrzny");
				$indeks++;
			}
			if ((int)$columns['order_details.query_id'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Nr zapytania");
				$indeks++;
			}
			if ((int)$columns['order_informations.fv'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "FV");
				$indeks++;
			}
			if ((int)$columns['order_informations.wm'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "WZ");
				$indeks++;
			}
			if ((int)$columns['contractor_login'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Zamawiający");
				$indeks++;
			}
			if ((int)$columns['wsk_budget.department'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Wydział");
				$indeks++;
			}
			if ((int)$columns['wsk_mpk.mpk'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "MPK");
				$indeks++;
			}
			if ((int)$columns['order_details.name'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Nazwa produktu");
				$indeks++;
			}
			if ((int)$columns['order_details.symbol'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Indeks produktu");
				$indeks++;
			}
			if ((int)$columns['order_details.producer'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Producent");
				$indeks++;
			}
			if ((int)$columns['order_details.amount'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Ilość");
				$indeks++;
			}
			if ((int)$columns['order_details.tax'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "VAT (%)");
				$indeks++;
			}
			if ((int)$columns['order_details.tax_value'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Wartość VAT (zł)");
				$data_sum[$indeks] = "=";
				$indeks++;
			}
			if ((int)$columns['order_details.netto_price'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Cena netto (zł)");
				$data_sum[$indeks] = "=";
				$indeks++;
			}
			if ((int)$columns['order_details.netto_value'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Wartość netto (zł)");
				$data_sum[$indeks] = "=";
				$indeks++;
			}
			if ((int)$columns['order_details.brutto_value'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Wartość brutto (zł)");
				$data_sum[$indeks] = "=";
				$indeks++;
			}
			if ((int)$columns['orders.add_date'] == 1)
			{
				$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, "Data złożenia");
				$indeks++;
			}
			
			$excel->getActiveSheet()->getStyle('B'.($counter).':'.$alphabet[($indeks-1)].($counter))->applyFromArray(
				array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				),
				'font' => array(
					'bold' => true
				),
				'borders' => array(
					'top' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')), 
					'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
					'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
					'bottom' => array('style' => PHPExcel_Style_Border::BORDER_DASHED, 'color' => array('argb' => 'ffbe3d38'))
				)
			));
			
			$counter++;
			
			$data = $this->getSummary($sorts, $types, $like);
			
			if (mysql_num_rows($data))
			{
				while($product = mysql_fetch_array($data)) 
		   		{
		   			if (in_array($product['detail_id'], $selected))
		   			{
			   			switch ($product['status'])
					       	{
					       		case 1: $product['status'] = 'oczekujące w Unimet'; break;
					       		case 2: $product['status'] = 'w trakcie realizacji'; break;
					       		case 3: $product['status'] = 'w drodze'; break;
					       		case 4: $product['status'] = 'archiwum'; break;
					       		case 5: $product['status'] = 'odrzucone'; break;
					       		case 6: $product['status'] = 'u analityka'; break;
					       		case 7: $product['status'] = 'u kierownika'; break;
					       		case 8: $product['status'] = 'weryfikacja w Unimet'; break;
					       		case 9: $product['status'] = 'u zamawiającego'; break;
					       		case 10: $product['status'] = 'u administratora'; break;
					       	}
				       	
				   		$indeks = 0;
					
						if ((int)$columns['orders.status'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['status']);
							$indeks++;
						}
						if ((int)$columns['order_details.order_id'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $this->formatId($product['order_id'], $product['query_id']));
							$indeks++;
						}
						if ((int)$columns['orders.number'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['number']);
							$indeks++;
						}
						if ((int)$columns['order_details.query_id'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, (($product['query_id'] > 0) ? $this->formatId($product['query_id'], $product['query_id']) : ""));
							$indeks++;
						}
						if ((int)$columns['order_informations.fv'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['fv']);
							$indeks++;
						}
						if ((int)$columns['order_informations.wm'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['wm']);
							$indeks++;
						}
						if ((int)$columns['contractor_login'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['contractor_login']);
							$indeks++;
						}
						if ((int)$columns['wsk_budget.department'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['department']);
							$indeks++;
						}
						if ((int)$columns['wsk_mpk.mpk'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['mpk']);
							$indeks++;
						}
						if ((int)$columns['order_details.name'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['product']);
							$indeks++;
						}
						if ((int)$columns['order_details.symbol'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['symbol']);
							$indeks++;
						}
						if ((int)$columns['order_details.producer'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['producer']);
							$indeks++;
						}
						if ((int)$columns['order_details.amount'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, str_replace(".", ",", (((int)$product['amount'] == $product['amount']) ? (int)$product['amount'] : $product['amount'])));
							$indeks++;
						}
						if ((int)$columns['order_details.tax'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, str_replace(".", ",", $product['tax']));
							$indeks++;
						}
						if ((int)$columns['order_details.tax_value'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, sprintf("%0.2f", ($product['tax'] * $product['netto_price'] / 100)));
							$data_sum[$indeks] .= $alphabet[$indeks].$counter.'+';
							$indeks++;
						}
						if ((int)$columns['order_details.netto_price'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['netto_price']);
							$data_sum[$indeks] .= $alphabet[$indeks].$counter.'+';
							$indeks++;
						}
						if ((int)$columns['order_details.netto_value'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, sprintf("%0.2f", ($product['netto_price'] * $product['amount'])));
							$data_sum[$indeks] .= $alphabet[$indeks].$counter.'+';
							$indeks++;
						}
						if ((int)$columns['order_details.brutto_value'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, sprintf("%0.2f", ($product['brutto_price'] * $product['amount'])));
							$data_sum[$indeks] .= $alphabet[$indeks].$counter.'+';
							$indeks++;
						}
			   			if ((int)$columns['orders.add_date'] == 1)
						{
							$excel->getActiveSheet()->setCellValue($alphabet[$indeks].$counter, $product['add_date']);
							$indeks++;
						}
				       	
						$excel->getActiveSheet()->getStyle($alphabet[0].$counter.':'.$alphabet[($indeks-1)].$counter)->applyFromArray(
							array(
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
							),
							'borders' => array(
								'bottom' => array('style' => PHPExcel_Style_Border::BORDER_DASHED, 'color' => array('argb' => 'ffbe3d38')), 
								'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
								'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38'))
							)
						));
						
						$counter++;
					}
		   		}
		   		
		   		foreach ($data_sum as $key => $value)
		   		{
		   			$excel->getActiveSheet()->setCellValue($alphabet[$key].$counter, substr($value, 0, -1));
					$excel->getActiveSheet()->getStyle($alphabet[$key].$counter)->applyFromArray(array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('argb'=>'ffffebea')), 
						'font' => array(
							'bold' => true)
						)
					);
		   		}

		   		$excel->getActiveSheet()->getStyle($alphabet[0].$counter.':'.$alphabet[$indeks-1].$counter)->applyFromArray(
					array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					),
					'borders' => array(
						'bottom' => array('style' => PHPExcel_Style_Border::BORDER_DASHED, 'color' => array('argb' => 'ffbe3d38')), 
						'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
						'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38'))
					)
				));
		   		
		   		$excel->getActiveSheet()->getStyle($alphabet[0].$counter.':'.$alphabet[$indeks-1].$counter)->applyFromArray(
					array('borders' => array(
						'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38'))
					)
				));
			}
			else
			{
				$excel->getActiveSheet()->setCellValue('B'.$counter, "brak wyników spełniających kryterium wyszukiwania");
				$excel->getActiveSheet()->mergeCells('B'.$counter.':'.$alphabet[($indeks-1)].$counter);
				
				$excel->getActiveSheet()->getStyle('B'.$counter.':'.$alphabet[($indeks-1)].$counter)->applyFromArray(
					array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
					),
					'borders' => array(
						'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
						'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38')),
						'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('argb' => 'ffbe3d38'))
					)
				));
			}
		}
		
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
	
		$xls_writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		
		header('Content-Type: application/vnd.ms-excel');	
		header('Content-Disposition: attachment; filename="b2b_zestawienie_'.date('Y_m_d').'.xls"');
		header('Cache-Control: private, max-age=0, must-revalidate');
		header('Pragma: public');
		ini_set('zlib.output_compression','0');
		
		$xls_writer->save('php://output');
	}
	
	public function getSummary($sorts, $types, $like)
	{
		$query = 'SELECT orders.status, orders.add_date, order_details.order_id, order_details.id as detail_id, orders.number, order_details.name as product, order_details.symbol, order_details.producer, (order_details.netto_price * 
		order_details.amount) as netto_value, (order_details.brutto_price * order_details.amount) as brutto_value, order_details.netto_price, 
		order_details.tax, order_details.amount, order_details.brutto_price, order_details.query_id, order_informations.fv, order_informations.wm, wsk_mpk.mpk, contractor_login, 
		analyst_login, manager_login, receiving_login, wsk_budget.department FROM orders JOIN order_details ON order_details.order_id = orders.id JOIN 
		wsk_mpk ON orders.id = wsk_mpk.order_id LEFT JOIN wsk_budget ON wsk_mpk.budget = wsk_budget.id LEFT JOIN order_informations ON orders.id = 
		order_informations.order_id WHERE 1 = 1 ';
		
		$counter = 0;
		
		if (is_array($types) && !empty($types) && is_array($like) && !empty($like))
		{
			foreach ($types as $key => $type)
			{
				$value = $like[$key];
				
				if ( (!empty($value)) || ((int)$type == 4 && ( (!empty($like["orders.add_date_from"]) && !empty($like["orders.add_date_to"]) && 
				date("Y-m-d H:i:s", strtotime($like["orders.add_date_from"])) <= date("Y-m-d H:i:s", strtotime($like["orders.add_date_to"]))) || 
				(!empty($like["orders.add_date_from"]) && empty($like["orders.add_date_to"])) || (empty($like["orders.add_date_from"]) && 
				!empty($like["orders.add_date_to"])) )) )
				{
					if (in_array($key, array("order_details.netto_price", "order_details.tax", "order_details.amount")))
					{
						$value = (float)str_replace(',', '.', $value);
					}
					
					$query .= ' AND '.(((int)$type == 4) ? '(' : '').$key;
					
					switch ((int)$type)
					{
						case 3: 
							$query .= ' = \''.mysql_escape_string($value).'\''; 
							break;
						
						case 2: 
							$query .= ' > \''.mysql_escape_string($value).'\''; 
							break;
							
						case -1: 
							$query .= ' < \''.mysql_escape_string($value).'\''; 
							break;
							
						case 4:
							
							if (!empty($like["orders.add_date_from"]) && !empty($like["orders.add_date_to"]))
							{
								$query .= ' BETWEEN \''.trim(mysql_escape_string($like["orders.add_date_from"])).'\' 
								AND \''.trim(mysql_escape_string($like["orders.add_date_to"])).'\')'; 
							}
							else if (!empty($like["orders.add_date_from"]))
							{
								$query .= ' >= \''.trim(mysql_escape_string($like["orders.add_date_from"])).'\')'; 
							}
							else if (!empty($like["orders.add_date_to"]))
							{
								$query .= '<= \''.trim(mysql_escape_string($like["orders.add_date_to"])).'\')'; 
							}
							
							break;
							
						default: 
							$part = 0;
		
							if (in_array($key, array('order_details.query_id', 'order_details.order_id')))
							{
								$substrings = explode('/', $value);
								$substrings_count = count($substrings);
													
								if (!strcmp(strtolower(substr($value, -1)), "z"))
								{
									switch ($substrings_count)
									{
										case 3:
											$value = '2'.sprintf("%03d", $substrings[1]).sprintf("%06d", $substrings[0]);
											break;
																
										case 2:
											$part = 2;
											$value = '2'.sprintf("%03d", $substrings[0]);
											break;
									}
								}
								else
								{
									switch ($substrings_count)
									{
										case 2:
											$value = '2'.sprintf("%03d", $substrings[1]).sprintf("%06d", $substrings[0]);
											break;
									}
								}
							}
							
							switch ($part)
							{
								case 1: 
									$query .= ' LIKE \'%'.mysql_escape_string($value).'\''; 
									break;
									
								case 2: 
									$query .= ' LIKE \''.mysql_escape_string($value).'%\''; 
									break;
									
								default: 
									$query .= ' LIKE \'%'.mysql_escape_string($value).'%\''; 
									break;
							}
							
						break;
					}
					
					$counter++;
				}
			}
		}
		
		$query .= ' ORDER BY ';	
		
		$default_order = true;
		
		if (is_array($sorts) && !empty($sorts))
		{
			foreach ($sorts as $key => $value)
			{
				if (!empty($value))
				{
					if (true == $default_order)
					{
						$default_order = false;
					}
					
					$query .= $key.' '.$value.', ';
				}
			}
		}
		
		if (true == $default_order)
		{
			$query .= 'add_date DESC';
		}
		else
		{
			$query = substr($query, 0, -2);
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $result;
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}
?>
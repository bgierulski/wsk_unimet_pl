<?php 
set_time_limit(60);

error_reporting(E_ALL ^ E_NOTICE);
header("Content-type: text/html; charset=utf-8"); 

include(dirname(__FILE__).'/../include/config.php');
include(BASE_DIR.'/class/class.DataBase.php');
include(BASE_DIR.'/class/class.Validators.php');
include(BASE_DIR.'nomad/nomad_mimemail.inc.php');

$db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
$validators = new Validators();

$mimemail = new nomad_mimemail(); 

$db->query('DELETE FROM order_reminders WHERE `date` < \''.date('Y-m-d').'\'') or $db->raise_error();

$result = $db->query('SELECT login, user_name, user_surname FROM users WHERE parent_id = 8338 OR status = 0') or $this->_db->raise_error();
$user_names = array();
		
if (mysql_num_rows($result))
{
	while($row = mysql_fetch_array($result)) 
	{
		$user_names[$row['login']] = $row['user_name'].' '.$row['user_surname'];
	}
}

$result = $db->query('SELECT id FROM users WHERE parent_id = 8338') or $db->raise_error();

if (mysql_num_rows($result))
{
	$ids = array();

	while($row = mysql_fetch_array($result)) 
   	{
   		array_push($ids, $row['id']);
   	}
	
	$query = 'SELECT orders.user_id, order_informations.delivery_date, IF (order_informations.delivery_date IS NULL OR order_informations.delivery_date = \'1970-01-01\', 0, 
	DATEDIFF(current_date, order_informations.delivery_date)) as delivery_diff, orders.id, number, users.parent_id, orders.status, order_details.query_id, orders.comment as order_comment, wsk_budget.department, wsk_budget.budget as 
	budget_value, groups_tree.name as subgroup_name, pallet, users.transport, users.name as user_name, users.name, users.post_code, users.city, users.street, 
	users.home, users.flat, users_delivery.post_code as delivery_post_code, users_delivery.city as delivery_city, users_delivery.street as delivery_street, 
	users_delivery.home as delivery_home, users_delivery.flat as delivery_flat, users.identificator, orders.add_date, orders.payment, orders.brutto_value, 
	order_details.id as detail_id, order_details.name, order_details.tax, order_details.amount, order_details.netto_price, order_details.brutto_price, 
	document, orders.comment, wsk_mpk.mpk, wsk_mpk.contractor, wsk_mpk.contractor_email, wsk_mpk.contractor_login, wsk_mpk.contractor_date, wsk_mpk.analyst, 
	wsk_mpk.analyst_email, wsk_mpk.analyst_login, wsk_mpk.analyst_date, wsk_mpk.analyst_deputy, wsk_mpk.administrator, wsk_mpk.administrator_email, 
	wsk_mpk.administrator_login, wsk_mpk.administrator_date, wsk_mpk.manager, wsk_mpk.manager_email, wsk_mpk.manager_login, wsk_mpk.manager_date, 
	wsk_mpk.manager_deputy, wsk_mpk.receiving, wsk_mpk.receiving_email, wsk_mpk.receiving_login, wsk_mpk.receiving_date, wsk_mpk.receiving_deputy, 
	wsk_mpk.unimet_representative, orders.netto_value, DATEDIFF(IF(orders.update_date = \'0000-00-00\', current_date, orders.update_date), orders.add_date) 
	as date_diff, order_informations.delivery_date, order_informations.fv, order_informations.wm, (SELECT email FROM users WHERE login = 
	wsk_mpk.unimet_representative AND status = 0) as unimet_email, users.merchant_id FROM orders JOIN order_details ON orders.id = order_details.order_id 
	JOIN users ON orders.user_id = users.id LEFT JOIN order_informations ON orders.id = order_informations.order_id LEFT JOIN products ON 
	order_details.product_id = products.id_hermes LEFT JOIN groups_tree ON products.subgroup_id = groups_tree.id LEFT JOIN users_delivery ON users.id = 
	users_delivery.user_id LEFT JOIN wsk_mpk ON orders.id = wsk_mpk.order_id LEFT JOIN wsk_budget ON wsk_mpk.budget = wsk_budget.id AND wsk_budget.start 
	<= \''.date('Y-m-d').'\' AND wsk_budget.end >= \''.date('Y-m-d').'\' WHERE DATEDIFF(IF(orders.update_date = \'0000-00-00\', current_date, orders.update_date), 
	orders.add_date) >= 3 AND users.parent_id = 8338 AND orders.status IN (1,3,6,7,9,10) ORDER BY orders.id ASC';
   
   	mysql_free_result($result);
    $result = $db->query($query) or $db->raise_error();
    
    $list = $db->mysql_fetch_all($result);
    mysql_free_result($result);
    
    $counter = 0;
    
    if (is_array($list))
    {
    	foreach ($list as $key => $order)
    	{
    		if ($order['id'] != $list[$key-1]['id'] && $counter < 10)
    		{
    			$result = $db->query('SELECT id FROM order_reminders WHERE order_id = '.$order['id'].' AND `date` = \''.date('Y-m-d').'\'') or $db->raise_error();
    			
    			if (!mysql_num_rows($result))
    			{
	    			$emails = array();
	    			
	    			$title = 'zamówieniu oczekującym na Twoją reakcję';
	    			
	    			$message_start = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><img src="'.BASE_ADDRESS.'images/logotype_small.png" alt="" /></p>';
					$message_start .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">&nbsp;</p>';
					$message_start .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><strong>Informacja o '.$title.'.</strong></p>';
					$message_start .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">&nbsp;</p>';
					
					switch ($order['status'])
					{
						case 1:
							
							if (empty($order['unimet_email']))
							{
							$result = $db->query('SELECT email FROM user_details JOIN users ON user_details.user_id = users.id 
							WHERE user_details.merchant_id = '.$order['merchant_id'].' AND email != \'\'') or $db->raise_error();
							//$message_content = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">';
							
							while($row = mysql_fetch_array($result)) 
			   				{
			   					if (!in_array($row['email'], $emails))
			   					{
			   						array_push($emails, $row['email']);
			   					}
			   					//$message_content .= $row['email'].', ';
			   				}
			   				}
			   				else
			   				{
			   					array_push($emails, $order['unimet_email']);
			   				}
							//$message_content .= ' </p>';
							$message_content = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$validators->formatId($order['id'], $order['query_id']).'</strong>, 
							które zostało złożone w dniu <strong>'.(($order['manager_date'] != "1970-01-01") ? $order['manager_date'] : $order['add_date']).'</strong> 
							oczekuje na Twoją reakcję <strong>'.$order['date_diff'].'</strong> dni.</p>';
							
							break;
							
						case 3:
							array_push($emails, $order['receiving_email']);
							
							//$message_content = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$order['contractor_email'].'</p>';
							$message_content = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$validators->formatId($order['id'], $order['query_id']).'</strong>, które 
							zostało złożone w dniu <strong>'.$order['menager_date'].'</strong> oczekuje na Twoją reakcję <strong>'.$order['date_diff'].'</strong> 
							dni.</p><p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Aby przejść do zamówienia zaloguj się do 
							<a href="http://wsk.unimet.pl">wsk.unimet.pl</a></p>';
							break;
							
						case 6:
							array_push($emails, $order['analyst_email']);
							
							//$message_content = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$order['analyst_email'].'</p>';
							$message_content = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$validators->formatId($order['id'], $order['query_id']).'</strong>, 
							które zostało złożone w dniu <strong>'.$order['contractor_date'].'</strong> oczekuje na Twoją reakcję <strong>'.$order['date_diff'].'</strong> 
							dni.</p><p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Aby przejść do zamówienia zaloguj się do 
							<a href="http://wsk.unimet.pl">wsk.unimet.pl</a></p>';
							break;
							
						case 7:
							array_push($emails, $order['manager_email']);
							
							//$message_content = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$order['manager_email'].'</p>';
							$message_content = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$validators->formatId($order['id'], $order['query_id']).'</strong>, 
							które zostało złożone w dniu <strong>'.$order['contractor_date'].'</strong> oczekuje na Twoją reakcję <strong>'.$order['date_diff'].'</strong> 
							dni.</p><p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Aby przejść do zamówienia zaloguj się do 
							<a href="http://wsk.unimet.pl">wsk.unimet.pl</a></p>';
							break;
							
						case 9:
							array_push($emails, $order['contractor_email']);
							
							//$message_content = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$order['contractor_email'].'</p>';
							$message_content = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$validators->formatId($order['id'], $order['query_id']).'</strong>, 
							które zostało złożone w dniu <strong>'.$order['contractor_date'].'</strong> oczekuje na Twoją reakcję <strong>'.$order['date_diff'].'</strong> 
							dni.</p><p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Aby przejść do zamówienia zaloguj się do 
							<a href="http://wsk.unimet.pl">wsk.unimet.pl</a></p>';
							break;
							
						case 10:
							//array_push($emails, 'krzysztof.kura@gmail.com');
							
							$result = $db->query('SELECT email FROM users WHERE status IN (1,2) AND admin = 1 AND email != \'\'') or $db->raise_error();
							
							//$message_content = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">';
							
							while($row = mysql_fetch_array($result)) 
			   				{
			   					if (!in_array($row['email'], $emails))
			   					{
			   						array_push($emails, $row['email']);
			   					}
			   					//$message_content .= $row['email'].', ';
			   				}
							//$message_content .= ' </p>';
							$message_content = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$validators->formatId($order['id'], $order['query_id']).'</strong>, 
							które zostło złożone w dniu <strong>'.$order['contractor_date'].'</strong> oczekuje na Twoją reakcję <strong>'.$order['date_diff'].'</strong> 
							dni.</p><p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Aby przejść do zamówienia zaloguj się do 
							<a href="http://wsk.unimet.pl">wsk.unimet.pl</a></p>';
							break;
					}
					
					$message_end = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">&nbsp;</p>';
					$message_end .= '<table cellspacing="0" cellpadding="0">';
					$message_end .= '<tr>';
						$message_end .= '<td style="border:'.((in_array($order['status'], array(9))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">Zamawiający</span></p>';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$user_names[$order['contractor_login']].'</p>';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$order['contractor_email'].'</p>';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($order['contractor_date'] != "1970-01-01" && !empty($order['contractor_date'])) ? $order['contractor_date'] : "").'</p>';
						$message_end .= '</td>';
						
						if ($list[0]['query_id'] == 0) 
						{
							$message_end .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
							$message_end .= '<td style="border:1px solid #7597c5; text-align:center; padding:5px">';
							$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">UNIMET - propozycja zamiennika</span></p>';
							$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.((!empty($order['unimet_representative'])) ? $user_names[$order['unimet_representative']] : 'brak danych').'</p>';
							$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$order['unimet_email'].'</p>';
							$message_end .= '</td>';
						}
						
						$message_end .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
						$message_end .= '<td style="border:'.((in_array($order['status'], array(6))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">Analityk</span></p>';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$user_names[$order['analyst_login']].'</p>';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$order['analyst_email'].'</p>';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($order['analyst_date'] != "1970-01-01" && !empty($order['analyst_date'])) ? $order['analyst_date'] : "").'</p>';
						$message_end .= '</td>';
						
						if ($order['query_id'] > 0)
						{
							$message_end .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
							$message_end .= '<td style="border:'.((in_array($order['status'], array(10))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
							$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">Administrator</span></p>';
							$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$user_names[$order['administrator_login']].'</p>';
							$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$order['administrator_email'].'</p>';
							$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($order['administrator_date'] != "1970-01-01" && !empty($order['administrator_date'])) ? $order['administrator_date'] : "").'</p>';
							$message_end .= '</td>';
						}
						
						$message_end .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
						$message_end .= '<td style="border:'.((in_array($order['status'], array(7))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">Kierownik</span></p>';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$user_names[$order['manager_login']].'</p>';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$order['manager_email'].'</p>';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($order['manager_date'] != "1970-01-01" && !empty($order['manager_date'])) ? $order['manager_date'] : "").'</p>';
						$message_end .= '</td>';
						$message_end .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
						$message_end .= '<td style="border:'.((in_array($order['status'], array(1))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">UNIMET</span></p>';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.((!empty($order['unimet_representative'])) ? $user_names[$order['unimet_representative']] : 'brak danych').'</p>';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$order['unimet_email'].'</p>';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($order['delivery_date'] != "1970-01-01" && !empty($order['delivery_date'])) ? $order['delivery_date'] : "").'</p>';
						$message_end .= '</td>';
						$message_end .= '<td style="font-size:23px; padding:0px 10px 0px 10px"><strong>&#187;</strong></td>';
						$message_end .= '<td style="border:'.((in_array($order['status'], array(3))) ? '3px solid #bd3d37' : '1px solid #7597c5').'; text-align:center; padding:5px">';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><span style="text-decoration:underline">Odbierający</span></p>';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$user_names[$order['receiving_login']].'</p>';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$order['receiving_email'].'</p>';
						$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.(($order['receiving_date'] != "1970-01-01" && !empty($order['receiving_date'])) ? $order['receiving_date'] : "").'</p>';
						$message_end .= '</td>';
					$message_end .= '</tr>';
					$message_end .= '</table>';
					
					$message_end .= '<div style="padding-top: 20px;">';
					$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Wydział: <strong>'.$order['department'].'</strong></p>';	
					$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Data złożenia zamówienia: <strong>'.$order['add_date'].'</strong></p>';
					$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Data dostawy: <strong>'.(($order['delivery_date'] != "1970-01-01" && !empty($order['delivery_date'])) ? $order['delivery_date'] : "brak danych").'</strong></p>';
					$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Forma płatności: <strong>'.$order['payment'].'</strong></p>';
					$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Numer MPK: <strong>'.$order['mpk'].'</strong></p>';
					$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Faktura VAT <strong>'.((!empty($order['fv'])) ? $order['fv'] : 'brak danych').'</strong></p>';
					$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">WZ: <strong>'.((!empty($order['wm'])) ? $order['wm'] : 'brak danych').'</strong></p>';
					$message_end .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Dokument <strong>'.$order['document'].'</strong></p>';
					$message_end .= '</div>';
					
					$from = "newsletter@unimet.pl";	 
					$html = '<html xmlns="http://www.w3.org/1999/xhtml"><body>'.iconv("UTF-8", "ISO-8859-2", $message_start.$message_content.$message_end).'</body></html>';
					$subject = 'wsk.unimet.pl - przypomnienie o zamówieniu oczekującym na reakcję';
					
					$mimemail->set_smtp_log(true); 
					$smtp_host	= "swarog.az.pl";
					$smtp_user	= "newsletter@unimet.pl";
					$smtp_pass	= "1hadesa2madmax";
					$mimemail->set_smtp_host($smtp_host);
					$mimemail->set_smtp_auth($smtp_user, $smtp_pass); 
					
					//$email = 'krzysztof.kura@gmail.com';
					
					if (is_array($emails) && !empty($emails))
					{
						foreach ($emails as $email)
						{
							$mimemail->new_mail($from, $email, iconv("UTF-8", "ISO-8859-2", $subject), "", $html);

							if ($mimemail->send())
							{
								$db->query('INSERT INTO order_reminders VALUES (\'\', '.$order['id'].', \''.date('Y-m-d').'\')') or $db->raise_error();
								$counter++;
							}
						}
					}
					
					
					if ($order['status'] == 3 && $order['delivery_diff'] >= 3)
					{
						$result = $db->query('SELECT merchants.guardian_email as email FROM users JOIN merchants ON users.merchant_id = merchants.id 
						WHERE users.id = '.$order['user_id']) or $db->raise_error();
						
						if (mysql_num_rows($result))
						{
							$row =  mysql_fetch_array($result);
							
							$message_start = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><img src="'.BASE_ADDRESS.'images/logotype_small.png" alt="" /></p>';
							$message_start .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">&nbsp;</p>';
							$message_start .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;"><strong>Informacja o braku reakcji odbierającego.</strong></p>';
							$message_start .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">&nbsp;</p>';
							
							$emails = array();
							array_push($emails, $row['email']);
							//array_push($emails, 'krzysztof.kura@gmail.com');
							
							$message_content = '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">'.$row['email'].'</p>';
							$message_content .= '<p style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;">Zamówienie o numerze <strong>'.$validators->formatId($order['id'], $order['query_id']).'</strong>  
							czeka na potwierdzenie odbioru w WSK od <strong>'.$order['delivery_diff'].'</strong> dni.</p>';
							
							$from = "newsletter@unimet.pl";	 
							$html = '<html xmlns="http://www.w3.org/1999/xhtml"><body>'.iconv("UTF-8", "ISO-8859-2", $message_start.$message_content.$message_end).'</body></html>';
							$subject = 'wsk.unimet.pl - powiadomienie o braku reakcji odbierającego';
							
							$mimemail->set_smtp_log(true); 
							$smtp_host	= "swarog.az.pl";
							$smtp_user	= "newsletter@unimet.pl";
							$smtp_pass	= "1hadesa2madmax";
							$mimemail->set_smtp_host($smtp_host);
							$mimemail->set_smtp_auth($smtp_user, $smtp_pass); 
							
							$email = $row['email'];
							//$email = 'krzysztof.kura@gmail.com';
							
							$mimemail->new_mail($from, $email, iconv("UTF-8", "ISO-8859-2", $subject), "", $html);
						}
					}
    			}
				
    		}
    	}
    }
}
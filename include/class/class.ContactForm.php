<?php
class Forms extends Validators
{
	private $_db;
	private $_errors;
	
	private $_title;
	private $_phone;
	private $_message;

	private $_id;
	private $_name;
	private $_email;
	private $_user;
	
	public function __construct()
	{	
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		
		$this->_user = $this->getSession("b2b_user");
		
		switch ($_POST['operation'])
		{
			case 'page_question':
				$this->_formContactValidation();
				break;
				
			case 'ask_about':
				$this->_questionValidation();
				break;
		}
	}
	
	private function _questionValidation()
	{
		$this->clear();
		
		$this->_id = $values['id'] = (int)$_POST['id'];
		$this->_name = $values['product_name'] = trim(strip_tags($_POST['name']));
		$this->_email = $values['product_email'] = trim(strip_tags($_POST['email']));
		$this->_message = $values['product_message'] = trim(strip_tags(stripslashes($_POST['message'])));
		
		if ($this->_name == '')
		{
			$this->_errors++;
			$errors['product_name'] = 'Nazwa produktu jest wymagana.';
		}
		
		if (str_replace('<br />', '', nl2br($this->_message)) == '')
		{
			$this->_errors++;
			$errors['product_message'] = 'Treść wiadomości jest wymagana.';
		}
		
		if ($this->_email == '')
		{
			$this->_errors++;
			$errors['product_email'] = 'Adres e-mail jest wymagany.';
		}
		else if (!$this->validateEmail($this->_email))
		{
			$this->_errors++;
			$errors['product_email'] = 'Adres e-mail jest niepoprawny.';
		}
		
		if (!$this->_errors)
		{
			$this->_sendQuestion();
		}
		else 
		{
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
	}
	
	private function _formContactValidation()
	{
		$this->clear();
		
		$this->_title = $values['title'] = trim($_POST['title']);
		$this->_email = $values['email'] = trim(strip_tags($_POST['email']));
		$this->_phone = $values['phone'] = trim(strip_tags($_POST['phone']));
		$this->_message = $values['message'] = trim(strip_tags(stripslashes($_POST['message'])));
			
		if ($this->_title == '')
		{
			$this->_errors++;
			$errors['title'] = 'Temat wiadomości jest wymagany.';
		}
		
		if ($this->_email == '' || !$this->validateEmail($this->_email))
		{
			$this->_errors++;
			$errors['email'] = 'Adres e-mail jest wymagany.';
		}
		
		if (!empty($this->_phone) && !$this->validatePhone($this->_phone))
		{
			$this->_errors++;
			$errors['phone'] = 'Numer telefonu jest niepoprawny.';
		}
		
		if (str_replace('<br />', '', nl2br($this->_message)) == '')
		{
			$this->_errors++;
			$errors['message'] = 'Treść wiadomości jest wymagana.';
		}
	
		if (!$this->_errors)
		{
			$this->_sendContactMessage();
		}
		else 
		{
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
	}
	
	private function _sendQuestion()
	{
		$this->clear();
		
		$message .= '<p><b>Produkt:</b> '.$this->_name.'</p>';
		$message .= '<p><b>Wiadomość:</b><br /> '.$this->_message.'</p>';
		$message .= '<p><b>E-mail zwrotny:</b> '.$this->_email.'</p>';
	
		$result = $this->_db->query('SELECT merchants.guardian_email as email FROM users JOIN merchants ON users.merchant_id = merchants.id WHERE users.id = '.$this->_user['id']) 
		or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			
			if ($this->validateEmail($row['email']))
			{
				$this->_send('wsk.unimet.pl - zapytanie o produkt', $message, $row['email']);
			}
		}
		
		mysql_free_result($result);
		$result = $this->_db->query('SELECT concessions.email FROM products JOIN concessions ON products.concession_id = concessions.id WHERE products.id_hermes = '.$this->_id) 
		or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			
			if ($this->validateEmail($row['email']))
			{
				$this->_send('wsk.unimet.pl - zapytanie o produkt', $message, $row['email']);
			}
		}
	}
	
	private function _sendContactMessage()
	{		
		$this->clear();
		
		$message = '<p><b>Wiadomość:</b><br /> '.$this->_message.'</p>';
		$message .= '<p><b>E-mail:</b> '.$this->_email.'</p>';
		if (!empty($this->_phone))
		{
			$message .= '<p><b>Telefon:</b> '.$this->_phone.'</p>';
		}
		
		$this->_send('wsk.unimet.pl - '.$this->_title, $message);
	}	
	
	private function _send($title, $message, $email = '')
	{
		$mimemail = new nomad_mimemail(); 
		
		$from		= "newsletter@unimet.pl";	 
		$subject	= $title; 
		$html		= '<html><body>'.iconv("UTF-8", "ISO-8859-2", $message).'</body></html>';
		
		if (empty($email))
		{
			$result = $this->_db->query('SELECT email FROM users WHERE email != \'\' AND id_hermes = 0 AND status = 0') 
			or $this->_db->raise_error();
			
			while($row = mysql_fetch_array($result)) 
	   		{
	   			if ($this->validateEmail($row['email']))
	   			{
		       		$to	= $row['email'];	
				
					$mimemail->set_smtp_log(true); 
					$smtp_host	= "swarog.az.pl";
					$smtp_user	= "newsletter@unimet.pl";
					$smtp_pass	= "1hadesa2madmax";
					$mimemail->set_smtp_host($smtp_host);
					$mimemail->set_smtp_auth($smtp_user, $smtp_pass); 
					$mimemail->new_mail($from, $to, iconv("UTF-8", "ISO-8859-2", $subject), "", $html);
					
					if ($mimemail->send())
					{
						unset($_SESSION["communicats"]['error']);
						$comunicats['ok'] = 'Wiadomość została wysłana';
					}
					else
					{
						unset($_SESSION["communicats"]['ok']);
						$comunicats['error'] = 'Wysłanie wiadomości nie powiodło się.';
					}
	   			}
	   		}
		}
	   	else
	   	{
			$mimemail->set_smtp_log(true); 
			$smtp_host	= "swarog.az.pl";
			$smtp_user	= "newsletter@unimet.pl";
			$smtp_pass	= "1hadesa2madmax";
			$mimemail->set_smtp_host($smtp_host);
			$mimemail->set_smtp_auth($smtp_user, $smtp_pass); 
			$mimemail->new_mail($from, $email, iconv("UTF-8", "ISO-8859-2", $subject), "", $html);
				
			if ($mimemail->send())
			{
				unset($_SESSION["communicats"]['error']);
				$comunicats['ok'] = 'Wiadomość została wysłana';
			}
			else
			{
				unset($_SESSION["communicats"]['ok']);
				$comunicats['error'] = 'Wysłanie wiadomości nie powiodło się.';
			}
	   	}
		
		$this->setSession("communicats", $comunicats);
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}

$forms = new Forms();
?>
<?php
$result = ibase_query('SELECT mat_art.ID as ID_PRD, fbx_plk.ID, fbx_plk.PLIK FROM fbx_plk JOIN mat_art ON fbx_plk.LOGO = mat_art.INDEKS WHERE fbx_plk.WERSJA 
= 1 AND fbx_plk.TYP IN (\'JPEG\', \'JPG\', \'GIF\', \'PNG\', \'jpeg\', \'jpg\', \'gif\', \'png\', \'TIF\', \'tif\') AND fbx_plk.LOGO_SPEC = \'WZR\' AND 
fbx_plk.LOGO_TYP = \'ART\' ORDER BY mat_art.ID, fbx_plk.ID ASC'); 

$db->query('TRUNCATE TABLE product_galleries') or $this->_db->raise_error();

$data = array('id_prd' => 0, 'plik' => "");
$galleries_counter = 0;
$order = 0;

while($row = ibase_fetch_assoc($result)) 
{
	if ($data['id_prd'] != $row['ID_PRD'] || $data['plik'] != $row['PLIK'])
	{
		if ($data['id_prd'] != $row['ID_PRD'])
		{
			$order = 0;
		}
				
		$data = array('id_prd' => $row['ID_PRD'], 'plik' => $row['PLIK']);
					
		if ($order)
		{
			$galleries_query .= '('.$row['ID'].', '.$row['ID_PRD'].', '.$order.'),';
			$galleries_counter++;
		}
		
		$order++;
					
		if ($galleries_counter > 1000)
		{
			$db->query('INSERT INTO product_galleries VALUES '.substr($galleries_query, 0, -1)) or $db->raise_error();
			$galleries_query = "";
			$galleries_counter = 0;
		}
	}
}

if ($galleries_counter)
{
	if (!empty($galleries_query))
	{
		$db->query(substr('INSERT INTO product_galleries VALUES '.$galleries_query, 0, -1)) or $db->raise_error();
	}
}

/*
$result = ibase_query('SELECT NAZWA, UPDID, img_art.POZ, img_art.LP FROM img_art WHERE img_art.TYP IN (\'JPEG\', 
\'JPG\', \'GIF\', \'PNG\', \'jpeg\', \'jpg\', \'gif\', \'png\', \'TIF\', \'tif\') ORDER BY img_art.POZ, img_art.LP ASC'); 

while($row = ibase_fetch_assoc($result)) 
{
	$images[] = $row;
}

$galleries_counter = 0;

foreach ($images as $i => $image)
{
	if ($image['POZ'] != $images[$i-1]['POZ'])
	{
		$order = 0;
	}
	
	if ($order)
	{
		$galleries_query .= '('.(int)$image['UPDID'].', '.(int)$image['POZ'].', '.$order.'),';
		$galleries_counter++; 
	}
	
	$order++;
	
	if ($galleries_counter == 0)
	{
		$db->query('TRUNCATE TABLE product_galleries') or $db->raise_error();
	}
	
	if ($galleries_counter > 1000)
	{
		$db->query(substr('INSERT INTO product_galleries VALUES '.$galleries_query, 0, -1)) or $db->raise_error();
		$galleries_query = '';
		$galleries_counter = 0;
	}
			
	
}

if ($galleries_counter)
{
	if (!empty($galleries_query))
	{
		$db->query(substr('INSERT INTO product_galleries VALUES '.$galleries_query, 0, -1)) or $db->raise_error();
	}
}
*/
?>
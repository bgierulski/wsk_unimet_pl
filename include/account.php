<?php
$param = $_GET['param2'];
$action = $_GET['param3']; 
$id = (int)$param4;

if ($param == 'aktywacja' && $_SESSION["b2b_user"]['admin'] == 1)
{
	$users->activateAccount(mysql_real_escape_string($action));
}

if ($action == 'usun' && $id && true == $users->checkLinks() && $_SESSION["b2b_user"]['admin'] == 1)
{
	$users->deleteUser($_SESSION["b2b_user"]['id'], $id);
}
?>

<div id="page_header">
	<div class="details">
		<p><strong>Zarządzanie kontem użytkownika</strong> <?php if ($_SESSION["communicats"]['ok']) echo ' - '; echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?></p>
	</div>
	<?php if ($_SESSION["b2b_user"]['id']) { ?>
		<?php if ($_SESSION["b2b_user"]['admin'] == 1) { ?>
		<?php /* ?>
		<div class="box">
			<p><strong><a href="./konto,analizy,lista.html" <?php if ($param == 'analizy') { echo 'class="active"'; } ?>>Analizy</a></strong></p>
		</div>
		<div class="box">
			<p><strong><a href="./konto,sciezki,lista.html" <?php if ($param == 'sciezki') { echo 'class="active"'; } ?>>Ścieżki</a></strong></p>
		</div>
		<?php */ ?>
		<?php if ($_SESSION["b2b_user"]['id'] == 7342) { ?>
		<div class="box">
			<p><strong><a href="./konto,produkty,lista.html" <?php if ($param == 'produkty') { echo 'class="active"'; } ?>>Indywidualne produkty</a></strong></p>
		</div>
		<?php } ?>
		<?php if ($_SESSION["b2b_user"]['admin'] == 1) { ?>
		<div class="box">
			<p><strong><a href="./konto,budzety_szczegoly.html" <?php if ($param == 'budzety_szczegoly') { echo 'class="active"'; } ?>>Szczegóły budżetów</a></strong></p>
		</div>
		<?php } ?>
		<div class="box">
			<p><strong><a href="./konto,budzety,lista.html" <?php if ($param == 'budzety') { echo 'class="active"'; } ?>>Podsumowanie budżetów</a></strong></p>
		</div>
		<div class="box">
			<p><strong><a href="./konto,podopieczni,nowy.html" <?php if ($param == 'podopieczni' && $action == 'nowy') { echo 'class="active"'; } ?>>Nowe subkonto</a></strong></p>
		</div>
		<?php } ?>
		<?php if (true == $users->checkLinks()) { ?>
		<div class="box">
			<p><strong><a href="./konto,podopieczni,lista.html" <?php if ($param == 'podopieczni' && $action == 'lista') { echo 'class="active"'; } ?>>Subkonta</a></strong></p>
		</div>
		<?php } ?>
		<div class="box">
			<p><strong><a href="./konto,edycja.html" <?php if ($param == 'edycja') { echo 'class="active"'; } ?>>Edycja danych osobowych</a></strong></p>
		</div>
	<?php } ?>
	<div class="clear"></div>
</div>
<?php

if ($_SESSION["b2b_user"]['id'])
{
	?>
	<div id="page_content">
	
		<div id="individual_products_content" style="display:<?php echo (($param == 'produkty') ? "block" : "none"); ?>">
		<?php include(BASE_DIR.'individual_products.php'); ?>
		</div>
	
		<div id="account_content" style="display:<?php if ($param == 'edycja' || ($action == 'nowy' && $_SESSION["b2b_user"]['admin'] == 1) || ($action == 'edycja' && $id && true == $users->checkLinks()) && $_SESSION["b2b_user"]['admin'] == 1) { echo 'block'; } else { echo 'none'; } ?>">
		
			<div id="form">
				<?php if (isset($_SESSION["communicats"]['error'])) { ?>
				<div class="element its_error">
				<label style="width:113px;">&nbsp;</label>
				<span>
				<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
				</span>
				</div>
				<?php } else if (isset($_SESSION["communicats"]['ok'])) { ?>
				<div class="element its_ok">
				<label style="width:113px;">&nbsp;</label>
				<span>
				<?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["communicats"]['ok']); ?>
				</span>
				</div>
				<?php }
				 
				if (!isset($_POST['operation']))
				{
					if (!in_array('edycja', array($param, $action)))
					{
						$forms->clear();
					}
					else if ($param == 'edycja')
					{
						$users->getUser();
						$user_id = $_SESSION["b2b_user"]['id'];
					}
					else if ($action == 'edycja' && $id)
					{
						$users->getUser($id);
						$user_id = $id;
					}
					
					if ($param == "podopieczni" && $action == "nowy")
					{
						$_SESSION["values"]["permissions"] = $users->getUserPermissions((int)$_SESSION["b2b_user"]['id']);
					}
				}

				if (!isset($_SESSION["values"]["permissions"]) || empty($_SESSION["values"]["permissions"]))
				{
					$_SESSION["values"]["permissions"] = $users->getUserPermissions((int)$user_id);
				}
				?>	
				
				<form action="" method="post" name="user">
			
				<input type="hidden" name="operation" value="account" />
				<input type="hidden" name="user_id" value="<?php echo (int)$user_id; ?>" />
				<input type="hidden" name="status" value="<?php echo ((($action == 'edycja' && $id) || $action == 'nowy' || $param == 'edycja') ? 2 : 1); ?>" />
				
				<?php if ($_SESSION["errors"]['parent_id'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['parent_id']; ?></div>
				<?php } ?>
				
				<input type="hidden" name="parent_id" value="<?php echo (($action != 'nowy') ? $_SESSION["values"]['parent_id'] : $_SESSION["b2b_user"]['id']); ?>" />
				
				<?php if (($action == 'edycja' && $id) || $action == 'nowy') { ?>	
					<?php if ($_SESSION["errors"]['login'] != '') { ?>
					<div class="element error">
					<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['login']; ?></div>
					<?php } ?>
					<div class="element">
					<label style="width:140px;">Login <strong class="error">(*)</strong></label>
					<input name="login" value="<?php echo $_SESSION["values"]['login']; ?>" type="text" />
					</div>
				<?php } else { ?>
					<input type="hidden" name="login" value="<?php echo $_SESSION["values"]['login']; ?>" />
				<?php } ?>
						
				<?php if ($_SESSION["errors"]['password'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['password']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Hasło <strong class="error" style="color:#034b97;">(*)</strong></label>
				<input name="password" autocomplete="off" value="<?php echo $_SESSION["values"]['password']; ?>" type="password" />
				<?php if (($action == 'edycja' && $id) || $action == 'nowy') { ?>
				<span style="color:#034b97;">- nie wymagane podczas edycji danych</span>
				<?php } ?>
				</div>
					
				<?php if ($_SESSION["errors"]['repeated_password'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['repeated_password']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Powtórzone hasło <strong class="error" style="color:#034b97;">(*)</strong></label>
				<input name="repeated_password" autocomplete="off" value="<?php echo $_SESSION["values"]['repeated_password']; ?>" type="password" />
				<?php if (($action == 'edycja' && $id) || $action == 'nowy') { ?>
				<span style="color:#034b97;">- nie wymagane podczas edycji danych</span>
				<?php } ?>
				</div>
				
				<?php if ($_SESSION["errors"]['user_name'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['user_name']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Imię <strong class="error">(*)</strong></label>
				<input name="user_name" value="<?php echo $_SESSION["values"]['user_name']; ?>" type="text" />
				</div>
					
				<?php if ($_SESSION["errors"]['user_surname'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['user_surname']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Nazwisko <strong class="error">(*)</strong></label>
				<input name="user_surname" value="<?php echo $_SESSION["values"]['user_surname']; ?>" type="text" />
				</div>
					
				<?php if ($_SESSION["errors"]['delivery_point'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['delivery_point']; ?></div>
				<?php } ?>
				<div class="element" style="display:<?php echo ((true === $users->checkReceiving((int)$user_id)) ? "block" : "none"); ?>;">
				<label style="width:140px;">Miejsce dostawy <strong class="error">(*)</strong></label>
				<input name="delivery_point" value="<?php echo $_SESSION["values"]['delivery_point']; ?>" type="text" />
				</div>
				
				<?php if (($action == 'edycja' && $id && $_SESSION["b2b_user"]['admin'] == 1) || ($action == 'nowy' && $_SESSION["b2b_user"]['admin'] == 1)) { ?>
				
					<?php if ($_SESSION["errors"]['identificator'] != '') { ?>
					<div class="element error">
					<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['identificator']; ?></div>
					<?php } ?>
					<div class="element">
					<label style="width:140px;">NIP jeśli firma</label>
					<input name="identificator" value="<?php echo $_SESSION["values"]['identificator']; ?>" type="text" />
					</div>
				
					<?php if ($_SESSION["errors"]['email'] != '') { ?>
					<div class="element error">
					<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['email']; ?></div>
					<?php } ?>
					<div class="element">
					<label style="width:140px;">E-mail <strong class="error">(*)</strong></label>
					<input name="email" value="<?php echo $_SESSION["values"]['email']; ?>" type="text" />
					</div>
						
					<?php if ($_SESSION["errors"]['phone'] != '') { ?>
					<div class="element error">
					<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['phone']; ?></div>
					<?php } ?>
					<div class="element">
					<label style="width:140px;">Telefon <strong class="error">(*)</strong></label>
					<input name="phone" value="<?php echo $_SESSION["values"]['phone']; ?>" type="text" />
					</div>
						
					<?php if ($_SESSION["errors"]['fax'] != '') { ?>
					<div class="element error">
					<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['fax']; ?></div>
					<?php } ?>
					<div class="element">
					<label style="width:140px;">Fax</label>
					<input name="fax" value="<?php echo $_SESSION["values"]['fax']; ?>" type="text" />
					</div>
				
					<?php if ($_SESSION["errors"]['ordering'] != '') { ?>
					<div class="element error">
					<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['ordering']; ?></div>
					<?php } ?>
					
					<input type="hidden" name="ordering" value="1" />
					
					<?php /*if ($_SESSION["b2b_user"]['ordering'] == 1) { ?>
					<div class="element">
					<label style="width:140px;">Składanie zamówień</label>
					<input class="checkbox" type="checkbox" name="ordering" value="1" <?php if ($_SESSION["values"]['ordering'] == 1) echo 'checked="checked"'; ?> />
					</div>
					<?php } else { ?>
					<input type="hidden" name="ordering" value="0" />
					<?php }*/ ?>
						
					<?php if ($_SESSION["errors"]['invoices'] != '') { ?>
					<div class="element error">
					<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['invoices']; ?></div>
					<?php } ?>
					
					<?php if ($_SESSION["b2b_user"]['invoices'] == 1) { ?>
					<div class="element">
					<label style="width:140px;">Podgląd faktur</label>
					<input class="checkbox" type="checkbox" name="invoices" value="1" <?php if ($_SESSION["values"]['invoices'] == 1) echo 'checked="checked"'; ?> />
					</div>
					<?php } else { ?>
					<input type="hidden" name="invoices" value="0" />
					<?php } ?>
					
					<?php if ($_SESSION["errors"]['admin'] != '') { ?>
					<div class="element error">
					<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['admin']; ?></div>
					<?php } ?>
					
					<?php if ($_SESSION["b2b_user"]['admin'] == 1) { ?>
					<div class="element">
					<label style="width:140px;">Administrator</label>
					<input class="checkbox" type="checkbox" name="admin" value="1" <?php if ($_SESSION["values"]['admin'] == 1) echo 'checked="checked"'; ?> />
					</div>
					<?php } else { ?>
					<input type="hidden" name="admin" value="0" />
					<?php } ?>
					
					<div class="element">
					<label style="width:140px;">Obserwator</label>
					<input class="checkbox" type="checkbox" name="observer" value="1" <?php if ($_SESSION["values"]['observer'] == 1) echo 'checked="checked"'; ?> />
					</div>
						
					<?php if ($_SESSION["errors"]['active'] != '') { ?>
					<div class="element error">
					<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['active']; ?></div>
					<?php } ?>
					<div class="element">
					<label style="width:140px;">&nbsp;</label>
					<select name="active">
					<option value="0">konto nieaktywne</option>
					<option value="1" <?php if ($_SESSION["values"]['active'] == 1) echo 'selected="selected"'; ?>>konto aktywne</option>
					</select>
					</div>
				<?php } else { ?>
					<input type="hidden" name="identificator" value="<?php echo $_SESSION["values"]['identificator']; ?>" />
					<input type="hidden" name="email" value="<?php echo $_SESSION["values"]['email']; ?>" />
					<input type="hidden" name="phone" value="<?php echo $_SESSION["values"]['phone']; ?>" />
					<input type="hidden" name="fax" value="<?php echo $_SESSION["values"]['fax']; ?>" />
					<input type="hidden" name="observer" value="<?php echo $_SESSION["values"]['observer']; ?>" />
				<?php } ?>
				
				<?php if (($action == 'edycja' && $id && $_SESSION["b2b_user"]['admin'] == 1) || ($action == 'nowy' && $_SESSION["b2b_user"]['admin'] == 1) || $_SESSION["b2b_user"]['id'] == $_SESSION["b2b_user"]['parent_id'] || $_SESSION["b2b_user"]['admin'] == 1) { ?>
				<div>
					<label style="width:140px;">Uprawnienia</label>
					<table id="permissions_table" cellspacing="0" cellpadding="0">
					<tr>
						<th class="first">&nbsp;</th>
						<th>Zamawiający</th>
						<th>Analityk</th>
						<th>Kierownik</th>
						<th>Odbierający</th>
					</tr>
					<?php 
					if (is_array($_SESSION["values"]["permissions"]))
					{
						foreach ($_SESSION["values"]["permissions"] as $key => $permission)
						{
							?>
							<tr>
								<td class="first <?php echo (($permission == end($_SESSION["values"]["permissions"])) ? "last" : ""); ?>">
								<input type="hidden" name="ids[<?php echo $key ?>]" value="<?php echo $permission['department']; ?>" /><input type="checkbox" name="budgets[]" value="<?php echo $key; ?>"  <?php if ($permission['contractor'] + $permission['analyst'] + $permission['manager'] + $permission['receiving'] > 0 && !(!isset($_POST['operation']) && $param == "podopieczni" && $action == "nowy")) echo 'checked="checked"'; ?> /> <strong><?php echo $permission['department']; ?></strong></td>
								<td <?php echo (($permission == end($_SESSION["values"]["permissions"])) ? 'class="last"' : ''); ?>><input type="checkbox" name="contractors[<?php echo $key; ?>]" value="1" <?php if ($permission['contractor'] == 1 && !(!isset($_POST['operation']) && $param == "podopieczni" && $action == "nowy")) echo 'checked="checked"'; ?> /></td>
								<td <?php echo (($permission == end($_SESSION["values"]["permissions"])) ? 'class="last"' : ''); ?>><input type="checkbox" name="analysts[<?php echo $key; ?>]" value="1" <?php if ($permission['analyst'] == 1 && !(!isset($_POST['operation']) && $param == "podopieczni" && $action == "nowy")) echo 'checked="checked"'; ?> /></td>
								<td <?php echo (($permission == end($_SESSION["values"]["permissions"])) ? 'class="last"' : ''); ?>><input type="checkbox" name="managers[<?php echo $key; ?>]" value="1" <?php if ($permission['manager'] == 1 && !(!isset($_POST['operation']) && $param == "podopieczni" && $action == "nowy")) echo 'checked="checked"'; ?> /></td>
								<td <?php echo (($permission == end($_SESSION["values"]["permissions"])) ? 'class="last"' : ''); ?>><input type="checkbox" name="receivings[<?php echo $key; ?>]" value="1" <?php if ($permission['receiving'] == 1 && !(!isset($_POST['operation']) && $param == "podopieczni" && $action == "nowy")) echo 'checked="checked"'; ?> /></td>
							</tr>
							<?php
						}
					}
					?>
					</table>
				</div>
				<?php } ?>
				
				<div class="element">
				<label style="width:140px;">&nbsp;</label>
				<div class="button" style="padding-right:10px;">
				<a href="./" id="user_form_submit"><?php if ($action == 'nowy') { echo 'DODAJ UŻYTKOWNIKA'; } else { echo 'ZAPISZ ZMIANY'; } ?></a>
				</div>
				<div class="clear"></div>
				</div>
				
			</form>
			
			<?php if (!(($action == 'edycja' && $id) || $action == 'nowy') && $_SESSION["b2b_user"]['status'] == 1) { ?>
			
			<form action="" method="post" name="application">
		
				<input type="hidden" name="operation" value="application" />
				<input type="hidden" name="id_hermes" value="<?php echo $_SESSION["b2b_user"]['id_hermes']; ?>" />
				
				<div class="element">
				<label style="width:140px;">&nbsp;</label>
				<div class="clear"></div>
				</div>
		
				<?php if ($_SESSION["errors"]['name'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['name']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Firma <strong class="error">(*)</strong></label>
				<input name="name" value="<?php echo $users->textControll($_SESSION["values"]['name']); ?>" type="text" />
				</div>
					
				<?php if ($_SESSION["errors"]['email'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['email']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">E-mail <strong class="error">(*)</strong></label>
				<input name="email" value="<?php echo $_SESSION["values"]['email']; ?>" type="text" />
				</div>
					
				<?php if ($_SESSION["errors"]['phone'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['phone']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Telefon <strong class="error">(*)</strong></label>
				<input name="phone" value="<?php echo $_SESSION["values"]['phone']; ?>" type="text" />
				</div>
					
				<?php if ($_SESSION["errors"]['fax'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['fax']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Fax</label>
				<input name="fax" value="<?php echo $_SESSION["values"]['fax']; ?>" type="text" />
				</div>
				
				<div class="element">
				<label style="width:140px;">&nbsp;</label>
				<strong>Twoje dane do paragony / faktury</strong>
				</div>
				
				<?php if ($_SESSION["errors"]['post_code'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['post_code']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Kod pocztowy <strong class="error">(*)</strong></label>
				<input name="post_code" value="<?php echo $_SESSION["values"]['post_code']; ?>" type="text" />
				</div>
					
				<?php if ($_SESSION["errors"]['city'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['city']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Miejscowość <strong class="error">(*)</strong></label>
				<input name="city" value="<?php echo $_SESSION["values"]['city']; ?>" type="text" />
				</div>
					
				<?php if ($_SESSION["errors"]['street'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['street']; ?></div>
				<?php } ?>
				<?php 
				if (!isset($_SESSION["values"]['street'])) { $_SESSION["values"]['street'] = 'ul.'; }
				?>
				<div class="element">
				<label style="width:140px;">Ulica <strong class="error">(*)</strong></label>
				<input name="street" value="<?php echo $_SESSION["values"]['street']; ?>" type="text" />
				</div>
				
				<?php if ($_SESSION["errors"]['home'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['home']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Nr budynku <strong class="error">(*)</strong></label>
				<input name="home" value="<?php echo $_SESSION["values"]['home']; ?>" type="text" />
				</div>
					
				<?php if ($_SESSION["errors"]['flat'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['flat']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Nr lokalu</label>
				<input name="flat" value="<?php echo $_SESSION["values"]['flat']; ?>" type="text" />
				</div>
				
				<?php /* ?>
				<?php if ($_SESSION["errors"]['region_id'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['region_id']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Województwo <strong class="error">(*)</strong></label>
				<select name="region_id">
				<option value="0">-- wybierz --</option>
				<option value="1" <?php if ($_SESSION["values"]['region_id'] == 1) echo 'selected="selected"'; ?>>dolnośląskie</option>
				<option value="2" <?php if ($_SESSION["values"]['region_id'] == 2) echo 'selected="selected"'; ?>>kujawsko-pomorskie</option>
				<option value="3" <?php if ($_SESSION["values"]['region_id'] == 3) echo 'selected="selected"'; ?>>lubelskie</option>
				<option value="4" <?php if ($_SESSION["values"]['region_id'] == 4) echo 'selected="selected"'; ?>>lubuskie</option>
				<option value="5" <?php if ($_SESSION["values"]['region_id'] == 5) echo 'selected="selected"'; ?>>łódzkie</option>
				<option value="6" <?php if ($_SESSION["values"]['region_id'] == 6) echo 'selected="selected"'; ?>>mazowieckie</option>
				<option value="7" <?php if ($_SESSION["values"]['region_id'] == 7) echo 'selected="selected"'; ?>>małopolskie</option>
				<option value="8" <?php if ($_SESSION["values"]['region_id'] == 8) echo 'selected="selected"'; ?>>opolskie</option>
				<option value="9" <?php if ($_SESSION["values"]['region_id'] == 9) echo 'selected="selected"'; ?>>podkarpackie</option>
				<option value="10" <?php if ($_SESSION["values"]['region_id'] == 10) echo 'selected="selected"'; ?>>podlaskie</option>
				<option value="11" <?php if ($_SESSION["values"]['region_id'] == 11) echo 'selected="selected"'; ?>>pomorskie</option>
				<option value="12" <?php if ($_SESSION["values"]['region_id'] == 12) echo 'selected="selected"'; ?>>śląskie</option>
				<option value="13" <?php if ($_SESSION["values"]['region_id'] == 13) echo 'selected="selected"'; ?>>świętokrzyskie</option>
				<option value="14" <?php if ($_SESSION["values"]['region_id'] == 14) echo 'selected="selected"'; ?>>warminsko-mazurskie</option>
				<option value="15" <?php if ($_SESSION["values"]['region_id'] == 15) echo 'selected="selected"'; ?>>wielkopolskie</option>
				<option value="16" <?php if ($_SESSION["values"]['region_id'] == 16) echo 'selected="selected"'; ?>>zachodniopomorskie</option>
				</select>
				</div>
				<?php */ ?>
				
				<?php if ($_SESSION["errors"]['identificator'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['identificator']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">NIP</label>
				<input name="identificator" value="<?php echo $_SESSION["values"]['identificator']; ?>" type="text" />
				</div>
				
				<?php /* ?>
				
				<div class="element">
				<label style="width:140px;">&nbsp;</label>
				<strong>Adres dostawy (pozostaw bez zmian jeśli taki sam)</strong>
				</div>
					
				<?php if ($_SESSION["errors"]['delivery_post_code'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['delivery_post_code']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Kod pocztowy</label>
				<input name="delivery_post_code" value="<?php echo $_SESSION["values"]['delivery_post_code']; ?>" type="text" />
				</div>
					
				<?php if ($_SESSION["errors"]['delivery_city'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['delivery_city']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Miejscowość</label>
				<input name="delivery_city" value="<?php echo $_SESSION["values"]['delivery_city']; ?>" type="text" />
				</div>
					
				<?php if ($_SESSION["errors"]['delivery_street'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['delivery_street']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Ulica</label>
				<input name="delivery_street" value="<?php echo $_SESSION["values"]['delivery_street']; ?>" type="text" />
				</div>
				
				<?php if ($_SESSION["errors"]['delivery_home'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['delivery_home']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Nr domu</label>
				<input name="delivery_home" value="<?php echo $_SESSION["values"]['delivery_home']; ?>" type="text" />
				</div>
					
				<?php if ($_SESSION["errors"]['delivery_flat'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['delivery_flat']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Nr mieszkania</label>
				<input name="delivery_flat" value="<?php echo $_SESSION["values"]['delivery_flat']; ?>" type="text" />
				</div>
					
				<?php if ($_SESSION["errors"]['delivery_region_id'] != '') { ?>
				<div class="element error">
				<label style="width:140px;">&nbsp;</label><?php echo $_SESSION["errors"]['delivery_region_id']; ?></div>
				<?php } ?>
				<div class="element">
				<label style="width:140px;">Województwo</label>
				<select name="delivery_region_id" style="width:200px;">
				<option value="0">-- wybierz --</option>
				<option value="1" <?php if ($_SESSION["values"]['delivery_region_id'] == 1) echo 'selected="selected"'; ?>>dolnośląskie</option>
				<option value="2" <?php if ($_SESSION["values"]['delivery_region_id'] == 2) echo 'selected="selected"'; ?>>kujawsko-pomorskie</option>
				<option value="3" <?php if ($_SESSION["values"]['delivery_region_id'] == 3) echo 'selected="selected"'; ?>>lubelskie</option>
				<option value="4" <?php if ($_SESSION["values"]['delivery_region_id'] == 4) echo 'selected="selected"'; ?>>lubuskie</option>
				<option value="5" <?php if ($_SESSION["values"]['delivery_region_id'] == 5) echo 'selected="selected"'; ?>>łódzkie</option>
				<option value="6" <?php if ($_SESSION["values"]['delivery_region_id'] == 6) echo 'selected="selected"'; ?>>mazowieckie</option>
				<option value="7" <?php if ($_SESSION["values"]['delivery_region_id'] == 7) echo 'selected="selected"'; ?>>małopolskie</option>
				<option value="8" <?php if ($_SESSION["values"]['delivery_region_id'] == 8) echo 'selected="selected"'; ?>>opolskie</option>
				<option value="9" <?php if ($_SESSION["values"]['delivery_region_id'] == 9) echo 'selected="selected"'; ?>>podkarpackie</option>
				<option value="10" <?php if ($_SESSION["values"]['delivery_region_id'] == 10) echo 'selected="selected"'; ?>>podlaskie</option>
				<option value="11" <?php if ($_SESSION["values"]['delivery_region_id'] == 11) echo 'selected="selected"'; ?>>pomorskie</option>
				<option value="12" <?php if ($_SESSION["values"]['delivery_region_id'] == 12) echo 'selected="selected"'; ?>>śląskie</option>
				<option value="13" <?php if ($_SESSION["values"]['delivery_region_id'] == 13) echo 'selected="selected"'; ?>>świętokrzyskie</option>
				<option value="14" <?php if ($_SESSION["values"]['delivery_region_id'] == 14) echo 'selected="selected"'; ?>>warminsko-mazurskie</option>
				<option value="15" <?php if ($_SESSION["values"]['delivery_region_id'] == 15) echo 'selected="selected"'; ?>>wielkopolskie</option>
				<option value="16" <?php if ($_SESSION["values"]['delivery_region_id'] == 16) echo 'selected="selected"'; ?>>zachodniopomorskie</option>
				</select>
				</div>
				
				<?php */ ?>
					
				<?php if ($action != 'nowy') { ?>
					<div class="element">
					<label style="width:140px;">&nbsp;</label>
					<div class="button" style="padding-right:10px;">
					<a href="./" id="application_form_submit">ZGŁOŚ DO ZMIANY</a>
					</div>
					<div class="clear"></div>
					</div>
				<?php } ?>
					
			</form>
			
			<?php } ?>
			</div>
		
		</div>
	
		<div id="proteges_content" style="display:<?php if ($param == 'podopieczni' && in_array($action, array('lista','usun')) && true == $users->checkLinks()) { echo 'block'; } else { echo 'none'; } ?>">
		<?php 
		$proteges = $users->getUserPermissions();
		?>
		
		<div id="protege_informations">
			<div></div>
			<span><img class="close" src="images/close_icon.png" alt="" title="zamknij" /></span>
		</div>
		
		<table class="proteges_list" cellspacing="0" cellpadding="0">
		<tr>
			<th style="text-align:left">Login</th>
			<th>Telefon</th>
			<th>E-mail</th>
			<th>Podgląd faktur</th>
			<th>Informacje</th>
			<?php if ($_SESSION["b2b_user"]['admin'] == 1) { ?>
			<th>Operacje</th>
			<?php } ?>
		</tr>
		<?php 
		if (is_array($proteges["departments"]) && !empty($proteges["departments"]))
		{
			$counter = 0;
				
			foreach ($proteges["departments"] as $i => $department)
			{
				?><tr><td class="bg_1<?php //echo (($counter%2)+1); ?>" style="text-align:left; background:#ccdceb;" colspan="6"><strong><?php echo $i; ?></strong></td></tr><?php
				$counter++;
					
				if (is_array($department["managers"]) && !empty($department["managers"]))
				{
					?><tr><td class="bg_1<?php //echo (($counter%2)+1); ?>" style="text-align:left" colspan="6">&nbsp;&nbsp;&nbsp;<strong>Kierownicy</strong></td></tr><?php
					$counter++;
						
					foreach ($department["managers"] as $protege)
					{
						if (!empty($protege['login']))
						{
							?>
							<tr>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>" style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $protege['login']; ?></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><?php echo $protege['phone']; ?></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><a href="mailto:<?php echo $protege['email']; ?>"><?php echo $protege['email']; ?></a></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><?php echo (($protege['invoices'] == 1) ? 'T' : 'N'); ?></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><a href="./" class="user_info" alt="<?php echo $protege['id']; ?>">zobacz</a></td>
								<?php if ($_SESSION["b2b_user"]['admin'] == 1) { ?>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>>
								<a href="./konto,podopieczni,usun,<?php echo $protege['id']; ?>.html">usuń</a> :: <a href="./konto,podopieczni,edycja,<?php echo $protege['id']; ?>.html">edytuj</a>
								</td>
								<?php } ?>
							</tr>
							<?php
								
							$counter++;
						}
					}
				}
					
				if (is_array($department["analysts"]) && !empty($department["analysts"]))
				{
					?><tr><td class="bg_1<?php //echo (($counter%2)+1); ?>" style="text-align:left" colspan="6">&nbsp;&nbsp;&nbsp;<strong>Analitycy</strong></td></tr><?php
					$counter++;
						
					foreach ($department["analysts"] as $protege)
					{
						if (!empty($protege['login']))
						{
							?>
							<tr>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>" style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $protege['login']; ?></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><?php echo $protege['phone']; ?></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><a href="mailto:<?php echo $protege['email']; ?>"><?php echo $protege['email']; ?></a></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><?php echo (($protege['invoices'] == 1) ? 'T' : 'N'); ?></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><a href="./" class="user_info" alt="<?php echo $protege['id']; ?>">zobacz</a></td>
								<?php if ($_SESSION["b2b_user"]['admin'] == 1) { ?>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>">
								<a href="./konto,podopieczni,usun,<?php echo $protege['id']; ?>.html">usuń</a> :: <a href="./konto,podopieczni,edycja,<?php echo $protege['id']; ?>.html">edytuj</a>
								</td>
								<?php } ?>
							</tr>
							<?php
								
							$counter++;
						}
					}
				}
					
				if (is_array($department["contractors"]) && !empty($department["contractors"]))
				{
					?><tr><td class="bg_1<?php //echo (($counter%2)+1); ?>" style="text-align:left" colspan="6">&nbsp;&nbsp;&nbsp;<strong>Zamawiający</strong></td></tr><?php
					$counter++;
						
					foreach ($department["contractors"] as $protege)
					{
						if (!empty($protege['login']))
						{
							?>
							<tr>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>" style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $protege['login']; ?></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><?php echo $protege['phone']; ?></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><a href="mailto:<?php echo $protege['email']; ?>"><?php echo $protege['email']; ?></a></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><?php echo (($protege['invoices'] == 1) ? 'T' : 'N'); ?></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><a href="./" class="user_info" alt="<?php echo $protege['id']; ?>">zobacz</a></td>
								<?php if ($_SESSION["b2b_user"]['admin'] == 1) { ?>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>">
								<a href="./konto,podopieczni,usun,<?php echo $protege['id']; ?>.html">usuń</a> :: <a href="./konto,podopieczni,edycja,<?php echo $protege['id']; ?>.html">edytuj</a>
								</td>
								<?php } ?>
							</tr>
							<?php
								
							$counter++;
						}
					}
				}
					
				if (is_array($department["receivings"]) && !empty($department["receivings"]))
				{
					?><tr><td class="bg_1<?php //echo (($counter%2)+1); ?>" style="text-align:left" colspan="6">&nbsp;&nbsp;&nbsp;<strong>Odbierający</strong></td></tr><?php
					$counter++;
						
					foreach ($department["receivings"] as $protege)
					{
						if (!empty($protege['login']))
						{
							?>
							<tr>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>" style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $protege['login']; ?></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><?php echo $protege['phone']; ?></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><a href="mailto:<?php echo $protege['email']; ?>"><?php echo $protege['email']; ?></a></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><?php echo (($protege['invoices'] == 1) ? 'T' : 'N'); ?></td>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>"><a href="./" class="user_info" alt="<?php echo $protege['id']; ?>">zobacz</a></td>
								<?php if ($_SESSION["b2b_user"]['admin'] == 1) { ?>
								<td class="bg_0<?php //echo (($counter%2)+1); ?>">
								<a href="./konto,podopieczni,usun,<?php echo $protege['id']; ?>.html">usuń</a> :: <a href="./konto,podopieczni,edycja,<?php echo $protege['id']; ?>.html">edytuj</a>
								</td>
								<?php } ?>
							</tr>
							<?php
								
							$counter++;
						}
					}
				}
			}
				
			if (is_array($proteges["anothers"]) && !empty($proteges["anothers"]))
			{
				?><tr><td class="bg_1<?php //echo (($counter%2)+1); ?>" style="text-align:left; background:#ccdceb;" colspan="6"><strong>Użytkownicy niepowiązani z działami</strong></td></tr><?php
				$counter++;
					
				foreach ($proteges["anothers"] as $protege)
				{
					if (!empty($protege['login']))
					{
						?>
						<tr>
							<td class="bg_0<?php //echo (($counter%2)+1); ?>" style="text-align:left">&nbsp;&nbsp;&nbsp;<?php echo $protege['login']; ?></td>
							<td class="bg_0<?php //echo (($counter%2)+1); ?>"><?php echo $protege['phone']; ?></td>
							<td class="bg_0<?php //echo (($counter%2)+1); ?>"><a href="mailto:<?php echo $protege['email']; ?>"><?php echo $protege['email']; ?></a></td>
							<td class="bg_0<?php //echo (($counter%2)+1); ?>"><?php echo (($protege['invoices'] == 1) ? 'T' : 'N'); ?></td>
							<td class="bg_0<?php //echo (($counter%2)+1); ?>"><a href="./" class="user_info" alt="<?php echo $protege['id']; ?>">zobacz</a></td>
							<?php if ($_SESSION["b2b_user"]['admin'] == 1) { ?>
							<td class="bg_0<?php //echo (($counter%2)+1); ?>">
							<a href="./konto,podopieczni,usun,<?php echo $protege['id']; ?>.html">usuń</a> :: <a href="./konto,podopieczni,edycja,<?php echo $protege['id']; ?>.html">edytuj</a>
							</td>
							<?php } ?>
						</tr>
						<?php
							
						$counter++;
					}
				}
			}
		}
		?>
		</table>
		<?php
		/*
		}
		else
		{
			$proteges = $users->getProteges($_SESSION["b2b_user"]['parent_id']);
			?>
			
			<table class="proteges_list" cellspacing="0" cellpadding="0">
			<tr>
				<th style="text-align:left">Login</th>
				<th>Telefon</th>
				<th>E-mail</th>
				<!-- <th>Składanie zamówień</th> -->
				<th>Podgląd faktur</th>
				<?php if ($_SESSION["b2b_user"]['admin'] == 1) { ?>
				<th>Operacje</th>
				<?php } ?>
			</tr>
			<?php 
			if (is_array($proteges))
			{
				$counter = 0;
				
				foreach ($proteges as $i => $protege)
				{
					?>
					<tr>
						<td class="bg_<?php echo (($counter%2)+1); ?>" style="text-align:left"><?php echo $protege['login']; ?></td>
						<td class="bg_<?php echo (($counter%2)+1); ?>"><?php echo $protege['phone']; ?></td>
						<td class="bg_<?php echo (($counter%2)+1); ?>"><a href="mailto:<?php echo $protege['email']; ?>"><?php echo $protege['email']; ?></a></td>
						<td class="bg_<?php echo (($counter%2)+1); ?>"><?php echo (($protege['invoices'] == 1) ? 'T' : 'N'); ?></td>
						<?php if ($_SESSION["b2b_user"]['admin'] == 1) { ?>
						<td class="bg_<?php echo (($counter%2)+1); ?>">
						<a href="./konto,podopieczni,usun,<?php echo $protege['id']; ?>.html">usuń</a> :: <a href="./konto,podopieczni,edycja,<?php echo $protege['id']; ?>.html">edytuj</a>
						</td>
						<?php } ?>
					</tr>
					<?php
					
					$counter++;
					
					unset($list);
					$list = $users->getProteges($protege['id']);
					
					if (is_array($list))
					{
						foreach ($list as $protege)
						{
							?>
							<tr>
								<td class="bg_<?php echo (($counter%2)+1); ?>" style="text-align:left">&nbsp;-&nbsp;<?php echo $protege['login']; ?></td>
								<td class="bg_<?php echo (($counter%2)+1); ?>"><?php echo $protege['phone']; ?></td>
								<td class="bg_<?php echo (($counter%2)+1); ?>"><a href="mailto:<?php echo $protege['email']; ?>"><?php echo $protege['email']; ?></a></td>
								<td class="bg_<?php echo (($counter%2)+1); ?>"><?php echo (($protege['invoices'] == 1) ? 'T' : 'N'); ?></td>
								<?php if ($_SESSION["b2b_user"]['admin'] == 1) { ?>
								<td class="bg_<?php echo (($counter%2)+1); ?>">
								<a href="./konto,podopieczni,usun,<?php echo $protege['id']; ?>.html">usuń</a> :: <a href="./konto,podopieczni,edycja,<?php echo $protege['id']; ?>.html">edytuj</a>
								</td>
								<?php } ?>
							</tr>
							<?php
							
							$counter++;
						}	
					}
				}
			}
			?>
			</table>
			<?php 
		}*/
		?>
		
		</div>
		
		<div id="budget_content" style="display:<?php if ($param == 'budzety' && in_array($action, array('lista','usun')) && true == $users->checkLinks()) { echo 'block'; } else { echo 'none'; } ?>">
		
			<iframe src="admin/bartek/budget/index.php" style="width:100%;height:500px;border:0px solid #ffffff;">
		
			</iframe>
		
		
		</div>
		
		<div id="budget_details_content" style="display:<?php if ($param == 'budzety_szczegoly' && true == $users->checkLinks()) { echo 'block'; } else { echo 'none'; } ?>">
		
			<?php
			if (isset($_POST['budget_id']))
			{
				$_SESSION['budget_details_id'] = (int)$_POST['budget_id'];
			}
			else if (!isset($_SESSION['budget_details_id']))
			{
				$_SESSION['budget_details_id'] = 1;
			}
			
			$list = $orders->getBudgetDetails($_SESSION['budget_details_id']);
			$entries_count = count($list);

			$on_page = 50;
				
			$pages_count = ceil($entries_count/$on_page);
				
			if ((int)$_GET['param3'] > 0 && (int)$_GET['param3'] <= $pages_count)
			{
				$page_number = (int)$_GET['param3'];
			}
			else 
			{
				$page_number = 1;
			}
				
			$first_entry = $page_number * $on_page - $on_page;
			$last_entry = $page_number * $on_page - 1;
			?>
			
			<?php
			$budgets_list = $orders->getBudgets();
			?>

			<div>
				<div style="float:left;">
					<form method="post" action="" name="budget">
					<select name="budget_id" style="position:relative; top:3px;" onchange="javascript: document.budget.submit();">
					<?php
					if (mysql_num_rows($budgets_list))
					{
						while($element = mysql_fetch_array($budgets_list)) 
						{
								?><option value="<?php echo $element['id']; ?>" <?php if ($_SESSION['budget_details_id'] == $element['id']) { echo 'selected="selected"'; } ?>><?php echo $element['department']; ?></option><?php
						}
					}
					?>
					</select>
					</form>
				</div>
				<div style="float:right;">
					<?php 
					if ($pages_count > 1)
					{
						?>
						<div id="subpages" style="text-align:right;">
						<?php 
						for ($i=1; $i<=$pages_count; $i++)
						{
							if ($page_number == $i) { ?><strong><?php }
							?><a href="./konto,budzety_szczegoly,<?php echo $i; ?>.html" style="color:#1178f3; text-decoration:none;"><?php echo $i; ?></a> <?php
							if ($page_number == $i) { ?></strong><?php }
						
							if ($i < $pages_count)
							{
								?><strong style="color:#ccccff;">|</strong> <?php
							}
						}
						?>
						</div>
						<?php
					}
					?>
				</div>
				<div class="clear"></div>
			</div>
			
			<table class="orders_list" style="padding:10px 0px 10px 0px;">
			<tr>
				<th>Budżet</th>
				<th>Data</th>
				<th>Informacja</th>
				<th>Imię i nazwisko</th>
				<th>Wartość początkowa</th>
				<th>Zmiana</th>
				<th>Wartość końcowa</th>
			</tr>
			<?php
			if (is_array($list) && !empty($list))
			{
				foreach ($list as $key => $row)
				{
					if ($key >= $first_entry && $key <= $last_entry)
					{
						?>
						<tr>
							<td class="bg_<?php echo ($key%2)+1; ?>" style="width:9%;"><?php echo (((int)$row['year'] > 0) ? $row['year'] : date('Y', strtotime($row['date']))); ?></td>
							<td class="bg_<?php echo ($key%2)+1; ?>" style="width:9%;"><?php echo date('Y-m-d H:i', strtotime($row['date'])); ?></td>
							<td class="bg_<?php echo ($key%2)+1; ?>" style="width:5%;"><?php echo $row['info']; ?></td>
							<td class="bg_<?php echo ($key%2)+1; ?>" style="width:9%;"><?php echo $row['user_name'].' '.$row['user_surname']; ?></td>
							<td class="bg_<?php echo ($key%2)+1; ?>" style="width:9%;"><?php echo $row['old_value']; ?></td>
							<td class="bg_<?php echo ($key%2)+1; ?>" style="width:8%;"><?php echo (($row['amount'] > 0) ? "+" : "").$row['amount']; ?></td>
							<td class="bg_<?php echo ($key%2)+1; ?>" style="width:8%;"><?php echo $row['new_value']; ?></td>
						</tr>
						<?php
					}
				}
			}
			?>
			</table>
		
		</div>
		
		
		
		
		<div id="paths_content" style="display:<?php if ($param == 'sciezki' && in_array($action, array('lista','usun')) && true == $users->checkLinks()) { echo 'block'; } else { echo 'none'; } ?>">
		
			<iframe src="admin/bartek/mpk/index.php" style="width:900px;height:500px;border:0px solid #ffffff;" scrolling="yes">
		
			</iframe>
		
		
		</div>
		
		<div id="analysis_content" style="display:<?php if ($param == 'analizy' && in_array($action, array('lista','usun')) && true == $users->checkLinks()) { echo 'block'; } else { echo 'none'; } ?>">
		treść w przygotowaniu
		</div>
	
	</div>
	<?php 
}
?>
<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<label style="width:120px;">&nbsp;</label>
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
	<div class="clear"></div>
</div>
<?php } ?>

<?php
$order_id = (int)$_GET['id']; 

if (!isset($_POST['save_changes']) && $action == 'edit')
{
	$order = $special_orders->getSpecialOrder($order_id);
}
?>

<form method="POST" action="">

<?php  if ($_SESSION["errors"]['order']) { ?>
<div class="form_element error">
	<?php echo $_SESSION["errors"]['order']; ?>
	<div class="clear"></div>
</div>
<?php } ?>

<input type="hidden" name="order_id" value="<?php echo $order_id; ?>" />

<?php if ($_SESSION["values"]['order_comment'] != '') { ?>
<div style="padding:5px 0px 5px 0px;"><?php echo $_SESSION["values"]['order_comment']; ?></div>
<?php } ?>

<table style="width: 100%;" cellpadding="0" cellspacing="0">
<tr>
	<th><strong>Nazwa produktu</strong></th>
	<th><strong>Symbol produktu</strong></th>
	<th><strong>Komentarz</strong></th>
	<th><strong>Ilość</strong></th>
	<th><strong>Cena netto</strong></th>
	<th><strong>VAT</strong></th>
</tr>
<?php 
for ($i=0; $i<$_SESSION["values"]['products_count']; $i++)
{
	?>
	<tr>
		<td class="bg_<?php echo (($i%2)+1); ?>"><?php echo $_SESSION["values"]["names"][$i]; ?></td>
		<td class="bg_<?php echo (($i%2)+1); ?>"><?php echo $_SESSION["values"]["symbols"][$i]; ?></td>
		<td class="bg_<?php echo (($i%2)+1); ?>"><?php echo $_SESSION["values"]["comments"][$i]; ?></td>
		<td class="bg_<?php echo (($i%2)+1); ?>"><?php echo $_SESSION["values"]["amounts"][$i]; ?></td>
		<td class="bg_<?php echo (($i%2)+1); ?>"><input type="text" name="prices[]" value="<?php echo $_SESSION["values"]["prices"][$i]; ?>" style="padding:0px 2px 0px 2px; width:100%;" /></td>
		<td class="bg_<?php echo (($i%2)+1); ?>"><input type="text" name="taxes[]" value="<?php echo $_SESSION["values"]["taxes"][$i]; ?>" style="padding:0px 2px 0px 2px; width:100%;" /></td>
	</tr>
	<?php
}
?>
</table>
	
<div class="form_element" style="margin-top:10px;">
	<input id="submit" name="save_changes" value="Zapisz zmiany" type="submit" /> 
	<div class="clear"></div>
</div>

</form> 
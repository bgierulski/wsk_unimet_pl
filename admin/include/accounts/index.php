<div id="title_element">
	<p>
	<strong>
	<?php
	switch ($action)
	{
		case 'new': ?>Dodawanie nowego użytkownika<?php break;
		case 'edit': ?>Edycja danych użytkownika<?php break;
		default: ?>Lista użytkowników<?php break;
	}
	?>
	</strong></p>
</div>

<div id="page_padding">
<?php
switch ($action)
{
	case 'new': include('new_account.php'); break;
	case 'edit': include('new_account.php'); break;
	default: include('accounts_list.php'); break;
}
?>
</div>
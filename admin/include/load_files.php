<?php
include('include/config.php');
include('fckeditor/fckeditor.php');
include(BASE_DIR.'../../include/nomad/nomad_mimemail.inc.php');
include(BASE_DIR.'default/class.DataBase.php');
include(BASE_DIR.'default/class.Validators.php');
include(BASE_DIR.'default/class.ImageUpload.php');
include(BASE_DIR.'default/class.Prices.php');
include(BASE_DIR.'authentication/class.Auth.php');
include(BASE_DIR.'users/class.Users.php');
include(BASE_DIR.'orders/class.Orders.php');

$auth = new Authentication();
$users = new Users();
$orders = new Orders();
$prices =  new Prices();

$resuources = array('authentication', 'orders', 'users', 'newses', 'accounts', 'header_products', 'producers', 'pages', 'special_orders', 'queries', 'merchants', 'concessions', 'b2b', 'main_page', 'informations', 'restrictions', 'summary');

if (in_array($controller, $resuources))
{
	include(BASE_DIR.$controller.'/operations.php');
}
?>
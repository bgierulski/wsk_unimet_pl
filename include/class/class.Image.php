<?php
class Image
{
	private $_path;
	private $_id;
	private $_type;
	private $_width;
	private $_height;
	
	public function __construct($path, $width, $height, $id, $type)
	{
		$this->_path = $path;
		$this->_id = $id;
		$this->_type = $type;
		$this->_width = $width;
		$this->_height = $height;
		
		$size = getimagesize($this->_path);
		
		if ($this->_width >= 0 && $this->_height >= 0 && file_exists($path))
		{
			if ($this->_width && $this->_height)
			{
				$this->_changeDimensions();
			}
			else
			{
				$size = getimagesize($path);
				
				if ($this->_width)
				{
					$this->_resizeImage('width', (($size[0] <= $this->_width) ? $size[0] : $this->_width));
				}
				else
				{
					$this->_resizeImage('height', (($size[1] <= $this->_height) ? $size[1] : $this->_height));
				}
			}
		}
	}
	
	private function _changeDimensions()
	{
		
		$size = getimagesize($this->_path);
		
		if ($this->_width >= $size[0] && $this->_height >= $size[1]) // generowanie miniaturki
		{	
			$this->_resizeImage('width', $size[0]);
		}
		else
		{
			if ($size[1] > $this->_height)
			{

				$new_width = ceil($size[0]*($this->_height/$size[1]));
				
				if ($new_width > $this->_width)
				{
					$this->_resizeImage('width', (($size[0] <= $this->_width) ? $size[0] : $this->_width));
				}
				else
				{
					$this->_resizeImage('height', (($size[1] <= $this->_height) ? $size[1] : $this->_height));
				}
			}
			else
			{
				$new_height = ceil($size[1]*($this->_width/$size[0]));
				if ($new_height > $this->_height)
				{
					$this->_resizeImage('height', (($size[1] <= $this->_height) ? $size[1] : $this->_height));
				}
				else
				{
					$this->_resizeImage('width', (($size[0] <= $this->_width) ? $size[0] : $this->_width));
				}
			}
		}
	}
	
	private function _resizeImage($parameter, $dimension)
	{
		if (file_exists($this->_path))
		{
			$info = getimagesize($this->_path);
			
			switch($info[2])
			{
				case IMAGETYPE_JPEG:
					$model = imagecreatefromjpeg($this->_path); 
					break;
					
				case IMAGETYPE_GIF:
					$model = imagecreatefromgif($this->_path);
					break;
					
				case IMAGETYPE_PNG:
					$model = imagecreatefrompng($this->_path);
					break;
			}
			
			if ($parameter == 'height')
			{
				$new_width = ceil($info[0]*($dimension/$info[1]));
				$new_height = $dimension;
			}
			elseif ($parameter == 'width')
			{
				$new_height = ceil($info[1]*($dimension/$info[0]));
				$new_width = $dimension;
			}
			
			$new_image = imagecreatetruecolor($new_width, $new_height);
			
			switch($info[2])
			{
				case IMAGETYPE_GIF:
					imagecolortransparent($new_image, imagecolorallocate($new_image, 0, 0, 0));
					imagealphablending($new_image, true);
					imagesavealpha($new_image, true);
					break;
					
				case IMAGETYPE_PNG:
					imagealphablending($new_image, false);
					imagesavealpha($new_image, true);
					break;
			}
			
			imagecopyresampled($new_image, $model, 0, 0, 0, 0, $new_width, $new_height, $info[0], $info[1]);
			
			$data = explode('/', $this->_path);
			$file_name = $data[count($data)-1];
			
			switch($info[2])
			{
				case IMAGETYPE_JPEG:
					
					return imagejpeg($new_image, (($file_name != 'no_image.png' && $this->_id) ? BASE_DIR.'/../../cashe/images/'.$this->_width.'/'.$this->_id.(($this->_type) ? '_1' : '').'.jpg' : ''), 100);
					break;
					
				case IMAGETYPE_GIF:
					
					return imagegif($new_image, (($file_name != 'no_image.png' && $this->_id) ? BASE_DIR.'/../../cashe/images/'.$this->_width.'/'.$this->_id.(($this->_type) ? '_1' : '').'.jpg' : ''), 100);
					break;
					
				case IMAGETYPE_PNG:
					
					return imagepng($new_image, (($file_name != 'no_image.png' && $this->_id) ? BASE_DIR.'/../../cashe/images/'.$this->_width.'/'.$this->_id.(($this->_type) ? '_1' : '').'.jpg' : ''));
					break;
			}
			
			imagedestroy($model);
			imagedestroy($new_image);
			
		}
	}
}
?>
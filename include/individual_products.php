<div class="form">
	
	<div class="element">
	<label style="width:110px;">Zakres zmian <strong class="error">(*)</strong></label>
	<select name="range">
	<option value="1">grupy</option>
	<option value="2">producenci</option>
	<option value="3">produkty</option>
	</select>
	</div>
	
	<div class="range_details block" id="range_1">
	
		<label style="width:110px;">Grupy</label>
		<div class="details"><p class="loading"><img src="images/loading_transparent.gif" alt="" /></p></div>
		<div class="clear"></div>

	</div>
	
	<div class="range_details" id="range_2">
	
		<label style="width:110px;">Producenci</label>
		<div class="details"></div>
		<div class="clear"></div>

	</div>
	
	<div class="range_details" id="range_3">
	
		<?php 
		$groups_list = $groups->getAllGroups();
		?>
	
		<div class="element">
		<label style="width:110px;">Grupa <strong class="error">(*)</strong></label>
		<select name="subgroup">
		<option value="0">-- dowolna --</option>
		<?php 
		while($group = mysql_fetch_array($groups_list)) 
		{
			?><option value="<?php echo $group['id']; ?>"><?php 
			
			for ($i=1; $i < $group['level']; $i++)
			{
				echo '&nbsp;&nbsp;';
			}
			
			if ($group['level'] > 1)
			{
				echo '-&nbsp;';
			}
			
			echo $group['name']; 
			
			?></option><?php
		}
		?>
		</select>
		</div>
		
		<?php 
		$producers_list = $producers->getAllProducers();
		?>
		
		<div class="element">
		<label style="width:110px;">Producent <strong class="error">(*)</strong></label>
		<select name="producer">
		<option value="0">-- dowolny --</option>
		<?php 
		while($producer = mysql_fetch_array($producers_list)) 
		{
			?><option value="<?php echo $producer['id']; ?>"><?php echo $producer['name']; ?></option><?php
		}
		?>
		</select>
		</div>
		
		<div class="element details">
			<label style="width:110px;">Produkty</label>
			<div style="float:left;"></div>
			<div class="clear"></div>
		</div>
	
	</div>
	
	<div class="element">
		<label style="width:110px;">&nbsp;</label>
		<div class="button" style="padding-right:10px;">
			<a href="./" id="individual_products_submit">ZAPISZ ZMIANY</a>
		</div>
		<div class="clear"></div>
	</div>
	
</div>
<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);

$controller = $_GET['controller'];
$action = $_GET['action'];
$id = $_GET['id'];
$param = $_GET['param'];
/*
if (!isset($_SESSION['hide']))
{
	header('Content-Type: text/html; charset=utf-8');
	
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
	<head>
	</head>
	<body>
	<p>WSK.UNIMET.PL jest w trakcie modernizacji. Aplikacja będzie dostępna od godziny 09:30.</p>
	</body>
	</html>
	<?php

	die();
}
*/

include('include/load_files.php');

if (!is_array($_SESSION["admin"]["modules"]))
{
	$_SESSION["admin"]["modules"] = array();
}

if (!in_array($controller, $_SESSION["admin"]["modules"]) && !in_array($controller, array("", "authentication")))
{
	header("Location: ".BASE_ADDRESS);
}

if ($controller == 'authentication')
{
	if ($action == 'login')
	{
		include('include/'.$controller.'/'.$action.'.php');
	}
	else if ($action == 'logout')
	{
		include('include/'.$controller.'/'.$action.'.php');
	}
}
else if (isset($_SESSION["admin"]) && $_SESSION["admin"]['controll'] == 1)
{
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	
	<head>
	<base href="<?php echo BASE_ADDRESS; ?>" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="Opis" />
	<link rel="stylesheet" href="css/admin.css" type="text/css" />
	<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.custom.css" rel="stylesheet" />
	<!-- <link rel="stylesheet" href="js/autocomplete/jquery.autocomplete.css" type="text/css" /> -->
	
	<script type="text/javascript" src="../js/jquery-1.4.js"></script>	
	<script type="text/javascript" src="js/jquery-ui-1.8.17.custom.min.js"></script>	
	<script type="text/javascript" src="js/jquery.cookie.js"></script>	
	<!-- <script type="text/javascript" src="js/autocomplete/jquery.autocomplete.js"></script> -->
	<script type="text/javascript" src="js/functions.js"></script>		
	<script type="text/javascript" src="js/advajax.js"></script>
	<script type="text/javascript" src="js/fu.js"></script>

	<title><?php echo DATABASE; ?></title>
	</head>
		
	<body <?php if ($action == 'print' && $id) { ?>onload="window.print();"<?php } ?>>
	
	<?php if (!in_array($action, array('print'))) { ?>
		
	<div>
		<p id="logo"><img src="../images/logotype.png" alt="" /></p>
		
		<div id="main_content">
			<div id="main_menu">
				<div id="controllers">
					<ul>
						<li><a href="./" <?php if ($controller == '') echo 'class="active"'; ?>>Strona&nbsp;główna</a></li>
						<?php if (in_array("header_products", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./header_products" <?php if ($controller == 'header_products') echo 'class="active"'; ?>>Produkty&nbsp;w&nbsp;nagłówku&nbsp;strony</a></li>
						<?php } /*if (in_array("special_orders", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./special_orders" <?php if ($controller == 'special_orders') echo 'class="active"'; ?>>Zamówienia&nbsp;specjalne</a></li>
						<?php }*/ if (in_array("orders", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./orders" <?php if ($controller == 'orders') echo 'class="active"'; ?>>Zamówienia</a></li>
						<?php } if (in_array("queries", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./queries" <?php if ($controller == 'queries') echo 'class="active"'; ?>>Zapytania</a></li>
						<?php } if (in_array("summary", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./summary" <?php if ($controller == 'summary') echo 'class="active"'; ?>>Zestawienia</a></li>
						<?php } if (in_array("accounts", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./accounts" <?php if ($controller == 'accounts') echo 'class="active"'; ?>>Konta&nbsp;użytkowników</a></li>
						<?php } if (in_array("users", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./users/list/1" <?php if ($controller == 'users') echo 'class="active"'; ?>>Klienci</a></li>
						<?php } if (in_array("producers", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./producers" <?php if ($controller == 'producers') echo 'class="active"'; ?>>Producenci</a></li>
						<?php } if (in_array("pages", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./pages" <?php if ($controller == 'pages') echo 'class="active"'; ?>>Podstrony</a></li>
						<?php } if (in_array("merchants", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./merchants" <?php if ($controller == 'merchants') echo 'class="active"'; ?>>Handlowcy</a></li>
						<?php } if (in_array("concessions", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./concessions" <?php if ($controller == 'concessions') echo 'class="active"'; ?>>Branżyści</a></li>
						<?php } if (in_array("main_page", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./main_page" <?php if ($controller == 'main_page') echo 'class="active"'; ?>>Grupy&nbsp;główne</a></li>
						<?php } if (in_array("informations", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./informations" <?php if ($controller == 'informations') echo 'class="active"'; ?>>Komunikaty</a></li>
						<?php } if (in_array("restrictions", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./restrictions" <?php if ($controller == 'restrictions') echo 'class="active"'; ?>>Ograniczenia</a></li>
						<?php } if (in_array("b2b", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./b2b" <?php if ($controller == 'b2b') echo 'class="active"'; ?>>B2B&nbsp;-&nbsp;szczegóły</a></li>
						<?php } if (in_array("groups", $_SESSION["admin"]["modules"])) { ?>
						<li><a href="./groups" <?php if ($controller == 'groups') echo 'class="active"'; ?>>Indywidualne&nbsp;menu&nbsp;użytkowników</a></li>
						<?php } ?>
						<li><a href="./authentication/logout">Wyloguj</a></li>
					</ul>
				</div>
				<!--  
				<div id="logout">
					<ul>
						<li><a href="./authentication/logout">Wyloguj</a></li>
					</ul>
				</div>
				-->
				<div class="clear"></div>
			</div>
			<div id="main_submenu">
			<?php
			include('include/default/menu.php');
			?>
			</div>
			
			<div id="page_content">
			<?php
			include('include/load_page.php');
			?>
			</div>
			
		</div>
	</div>
	
	<?php } else { ?>
	
	<?php
	if (in_array($controller, array("orders", "queries")))
	{
		if (in_array($action, array("print")))
		{
			$orders->setRepresentative((int)$id);
		}
		
		switch ($action)
		{
			case 'show': include('include/orders/show_order.php'); break;
			case 'print': include('include/orders/print_order.php'); break;
		}
	}
	else if ($controller == 'users')
	{
		switch ($action)
		{
			case 'print': include('include/users/print_message.php'); break;
		}
	}
	?>
	
	<?php } ?>
	
	</body>
	</html>
	<?php
}
else 
{
	header("Location: ".BASE_ADDRESS."authentication/login");
}
?>
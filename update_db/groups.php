<?php
$result = ibase_query('SELECT LOGO, OPIS, NALEZY FROM MAT_GRP ORDER BY LOGO ASC');

$tree = array();
$groups_list = '';
$query_counter = 0;

while($row = ibase_fetch_assoc($result)) 
{
	$tree[] = $row;
}

if (is_array($tree))
{
	$query = 'INSERT INTO groups_tree VALUES ';
	
	foreach ($tree as $i => $branch)
	{
		if ((int)trim($branch['LOGO']) > 0 && substr(trim($branch['LOGO']), 0, 1) != '0' && (!strcmp((string)(int)trim($branch['LOGO']), trim($branch['LOGO'])) || trim($branch['NALEZY']) != ''))
		{
			$prievious = explode('-', trim($tree[$i-1]['LOGO']));
			$elements = explode('-', trim($branch['LOGO']));
			$level = count($elements);
			
			$order = (int)$elements[0];
			unset($elements[0]);
			
			if (count($elements)) 
			{
				$order .= '.';
			}
			
			if ((!preg_match('/^[0-9]*$/i', $elements[count($elements)]) && preg_match('/^[0-9]*$/i', $prievious[count($prievious)-1])) || (strlen(trim($tree[$i-1]['LOGO'])) == strlen(trim($branch['LOGO'])) && $elements[count($elements)-1] != $prievious[count($prievious)-2]))
			{
				$counter = 1;
			}
			
			foreach ($elements as $i => $element)
			{
				if (preg_match('/^[0-9]*$/i', $element))
				{
					$order .= str_pad((int)$element, 2, 0, STR_PAD_LEFT);
				}
				else
				{
					$order .= str_pad($counter, 2, 0, STR_PAD_LEFT);
				}
			}
			
			$counter++;
		
			unset($prievious);
			unset($elements);
			
			// parent
			$elements = explode('-', trim($branch['NALEZY']));
			
			$parent = (int)$elements[0];
			unset($elements[0]);
			
			if (count($elements)) 
			{
				$parent .= '.';
			}
			
			foreach ($elements as $i => $element)
			{
				$parent .= str_pad((int)$element, 2, 0, STR_PAD_LEFT);
			}
			
			if ($query_counter == 200)
			{
				$groups_list = substr($groups_list, 0, -4);
				$groups_list .= ') AND ';
				$query_counter = 0;
			}
			
			if ($query_counter == 0)
			{
				$groups_list .= 'id NOT IN (SELECT id FROM groups_tree WHERE ';
			}
			
			$groups_list .= '(level = '.$level.' AND parent_id = \''.(((float)$parent > 0) ? (float)$parent : (float)$order).'\' AND comment = \''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($branch['LOGO']))).'\') OR ';
			
			$query_counter++;
			
			$data = $db->query('SELECT id FROM groups_tree WHERE level = '.$level.' AND parent_id = \''.(((float)$parent > 0) ? (float)$parent : (float)$order).'\' AND comment = \''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($branch['LOGO']))).'\'') or $db->raise_error();
			
			if (mysql_num_rows($data))
			{
				$subgroup = mysql_fetch_array($data);
				
				$db->query('UPDATE groups_tree SET name = \''.iconv("windows-1250", "UTF-8", mysql_escape_string(trim(str_replace(trim($branch['LOGO']), '', $branch['OPIS'])))).'\' WHERE id = '.$subgroup['id']) or $db->raise_error();
			}
			else
			{
				$query .= '(\'\', '.$level.', \''.iconv("windows-1250", "UTF-8", mysql_escape_string(trim(str_replace(trim($branch['LOGO']), '', $branch['OPIS'])))).'\', \''.(float)$order.'\', \''.(((float)$parent > 0) ? (float)$parent : (float)$order).'\', \''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($branch['LOGO']))).'\'),';
				
				// operacje na drzewach indywidualnych
				$result = $db->query('SELECT user_id, what_with_new FROM groups_tree_user WHERE individual = 1') or $db->raise_error();
				
				if (mysql_num_rows($result))
				{
					while($element = mysql_fetch_array($result)) 
		   			{
		   				$subquery = 'INSERT INTO groups_tree_status (`user`, `comment`, `status`) VALUES ('.$element['user_id'].', 
		   				\''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($branch['LOGO']))).'\', '.$element['what_with_new'].') ON DUPLICATE KEY UPDATE status = '.$element['what_with_new'];
		   				
		   				$db->query($subquery) or $db->raise_error();
		   			}
				}
				
				mysql_free_result($result);
				// koniec operacji na drzewach indywidualnych
			}
			
		}
	}
	
	if (strlen($query) > 33)
	{
		$db->query(substr($query, 0, -1)) or $db->raise_error();
	}
	
	if (substr($groups_list, - 3) != ' OR')
	{
		$groups_list = substr($groups_list, 0, -3);
		$groups_list .= ')';
	}
}

$result = $db->query('SELECT `order` FROM groups_tree WHERE '.$groups_list) or $db->raise_error();

function deleteChildren($db, $children)
{
	$order = array_pop($children);
	
	if (!is_null($order))
	{
		$result = $db->query('SELECT `order` FROM groups_tree WHERE parent_id = '.$order.' OR `order` = \''.$order.'\'') or $db->raise_error();
		
		if (mysql_num_rows($result))
		{
			while($row = mysql_fetch_array($result)) 
		   	{
		       	array_push($children, $row['kolejnosc']);
		   	}
		}
		
		$db->query('DELETE FROM groups_tree WHERE `order` = '.$order) or $db->raise_error();
		
		// operacje na drzewach indywidualnych
		$result = $db->query('SELECT comment FROM groups_tree WHERE `order` = \''.$order.'\'') or $db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			$db->query('DELETE FROM groups_tree_status WHERE comment LIKE \''.mysql_escape_string($row['comment']).'%\'') or $db->raise_error();
			$db->query('DELETE FROM products_status WHERE subgroup LIKE \''.mysql_escape_string($row['comment']).'%\'') or $db->raise_error();
		}
		// koniec operacji na drzewach indywidualnych
		
		if (count($children))
		{
			deleteChildren($db, $children);
		}
	}
}

if (mysql_num_rows($result))
{
	while($row = mysql_fetch_array($result)) 
   	{
       	deleteChildren($db, array($row['order']));
   	}
}

$db->query('UPDATE products JOIN groups_tree ON groups_tree.comment = products.comment SET products.subgroup_id = groups_tree.id') or $db->raise_error();
?>
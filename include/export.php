<?php if ('bartosz_welc' != $_SESSION["b2b_user"]['login']) { ?>

<div id="page_header" style="background:none;">
	<div class="details">
		<p><strong>Zestawienia danych</strong></p>
	</div>
	<div class="clear"></div>
</div>

<form method="post" action="" name="export">
<input type="hidden" name="order[orders.status]" value="<?php echo $_POST["order"]['orders.status']; ?>" />
<input type="hidden" name="order[order_details.order_id]" value="<?php echo $_POST["order"]['order_details.order_id']; ?>" />
<input type="hidden" name="order[orders.number]" value="<?php echo $_POST["order"]['orders.number]']; ?>" />
<input type="hidden" name="order[order_details.query_id]" value="<?php echo $_POST["order"]['order_details.query_id']; ?>" />
<input type="hidden" name="order[contractor_login]" value="<?php echo $_POST["order"]['contractor_login']; ?>" />
<input type="hidden" name="order[wsk_budget.department]" value="<?php echo $_POST["order"]['wsk_budget.department']; ?>" />
<input type="hidden" name="order[wsk_mpk.mpk]" value="<?php echo $_POST["order"]['wsk_mpk.mpk']; ?>" />
<input type="hidden" name="order[order_details.name]" value="<?php echo $_POST["order"]['order_details.name']; ?>" />
<input type="hidden" name="order[order_details.symbol]" value="<?php echo $_POST["order"]['order_details.symbol']; ?>" />
<input type="hidden" name="order[order_details.producer]" value="<?php echo $_POST["order"]['order_details.producer']; ?>" />
<input type="hidden" name="order[order_details.amount]" value="<?php echo $_POST["order"]['order_details.amount']; ?>" />
<input type="hidden" name="order[order_details.tax]" value="<?php echo $_POST["order"]['order_details.tax']; ?>" />
<input type="hidden" name="order[order_details.netto_price]" value="<?php echo $_POST["order"]['order_details.netto_price']; ?>" />
<input type="hidden" name="order[netto_value]" value="<?php echo $_POST["order"]['netto_value']; ?>" />
<input type="hidden" name="order[brutto_value]" value="<?php echo $_POST["order"]['brutto_value']; ?>" />
<input type="hidden" name="order[order_informations.fv]" value="<?php echo $_POST["order"]['order_informations.fv']; ?>" />
<input type="hidden" name="order[order_informations.wm]" value="<?php echo $_POST["order"]['order_informations.wm']; ?>" />
<input type="hidden" name="order[analyst_login]" value="<?php echo $_POST["order"]['analyst_login']; ?>" />
<input type="hidden" name="order[manager_login]" value="<?php echo $_POST["order"]['manager_login']; ?>" />
<input type="hidden" name="order[receiving_login]" value="<?php echo $_POST["order"]['receiving_login']; ?>" />
<input type="hidden" name="order[orders.add_date]" value="<?php echo $_POST["order"]['orders.add_date']; ?>" />
<input type="hidden" name="order[order_informations.fv]" value="<?php echo $_POST["order"]['order_informations.fv']; ?>" />
<input type="hidden" name="order[order_informations.wm]" value="<?php echo $_POST["order"]['order_informations.wm']; ?>" />

<div style="padding:0px 0px 5px 0px">
	<p class="button" style="float:left; margin:0px 10px 0px 0px"><a href="./zestawienia.html" class="filter_form_submit">FILTRUJ</a></p>
	<p class="button" style="float:left; margin:0px 10px 0px 0px"><a href="./zestawienia,eksport.html" target="blank" class="export_form_columns">WYBIERZ KOLUMNY DO EKSPORTU</a></p>
	<p class="button" style="float:right; margin:0px 5px 0px 0px"><a href="./zestawienia.html" class="filter_form_submit">FILTRUJ</a></p>
	<p class="button" style="float:right; margin:0px 10px 0px 0px"><a href="./zestawienia,eksport.html" target="blank" class="export_form_columns">WYBIERZ KOLUMNY DO EKSPORTU</a></p>
	<p class="button" style="float:right; margin:0px 10px 0px 0px"><a href="./zestawienia,oszczednosci.html" target="blank" class="savings_report">RAPORT OSZCZĘDNOŚCI</a></p>
	<div class="clear"></div>
</div>

<div id="savings_year">
	<div>
	<?php 
	$savings_info = $products->getSavingsYears();
	?>
	<p>
	<select name="year">
	<option value="0">-- wybierz rok --</option>
	<?php 
	while($row = mysql_fetch_array($savings_info)) 
	{
		?><option value="<?php echo $row['year']; ?>"><?php echo $row['year']; ?></option><?php
	}
	?>
	</select>
	</p>
	<p class="button"><a href="./zestawienia,oszczednosci.html" class="savings_form_submit">WYEKSPORTUJ DANE</a></p>
	</div>
	<div class="clear"></div>
</div>

<?php 
$budget_list = $users->getBudgets( ($_SESSION["b2b_user"]['admin'] == 1) ? true : false );
?>

<div id="export_columns">
	<div>
	<p><input type="checkbox" name="columns[orders.status]" value="1" checked="checked" /> status</p>
	<p><input type="checkbox" name="columns[order_details.order_id]" value="1" checked="checked" /> nr zamówienia</p>
	<p><input type="checkbox" name="columns[orders.number]" value="1" checked="checked" /> nr wewnętrzny</p>
	<p><input type="checkbox" name="columns[order_details.query_id]" value="1" checked="checked" /> nr zapytania</p>
	<p><input type="checkbox" name="columns[contractor_login]" value="1" checked="checked" /> zamawiający</p>
	<p><input type="checkbox" name="columns[wsk_budget.department]" value="1" checked="checked" /> wydział</p>
	<p><input type="checkbox" name="columns[wsk_mpk.mpk]" value="1" checked="checked" /> MPK</p>
	<p><input type="checkbox" name="columns[order_details.name]" value="1" checked="checked" /> nazwa produktu</p>
	<p><input type="checkbox" name="columns[order_details.symbol]" value="1" checked="checked" /> indeks produktu</p>
	<p><input type="checkbox" name="columns[order_details.producer]" value="1" checked="checked" /> producent</p>
	<p><input type="checkbox" name="columns[order_details.amount]" value="1" checked="checked" /> ilość</p>
	<p><input type="checkbox" name="columns[order_details.tax]" value="1" checked="checked" /> VAT</p>
	<p><input type="checkbox" name="columns[order_details.tax_value]" value="1" checked="checked" /> wartość VAT</p>
	<p><input type="checkbox" name="columns[order_details.netto_price]" value="1" checked="checked" /> cena netto</p>
	<p><input type="checkbox" name="columns[order_details.netto_value]" value="1" checked="checked" /> wartość netto</p>
	<p><input type="checkbox" name="columns[order_details.brutto_value]" value="1" checked="checked" /> wartość brutto</p>
	<p><input type="checkbox" name="columns[orders.add_date]" value="1" checked="checked" /> data złożenia</p>
	<p><input type="checkbox" name="columns[order_informations.fv]" value="1" checked="checked" /> FV</p>
	<p><input type="checkbox" name="columns[order_informations.wm]" value="1" checked="checked" /> WZ</p>
	<p class="button"><a href="./zestawienia.html" class="export_form_submit">WYEKSPORTUJ DANE</a></p>
	</div>
	<div class="clear"></div>
</div>
	
<div id="page_content">
	<div id="export_content">
	<div id="export_box">
	<table class="export" cellspacing="0" cellpadding="0">
	<tr>
		<th style="width:160px">
			<input type="hidden" name="types[orders.status]" value="0" />
			<select name="like[orders.status]">
			<option value="4" <?php if ((int)$_POST["like"]['orders.status'] == 4) echo 'selected="selected"'; ?>>archiwum</option>
			<option value="1" <?php if ((int)$_POST["like"]['orders.status'] == 1) echo 'selected="selected"'; ?>>oczekujące w Unimet</option>
			<option value="5" <?php if ((int)$_POST["like"]['orders.status'] == 5) echo 'selected="selected"'; ?>>odrzucone</option>
			<option value="3" <?php if ((int)$_POST["like"]['orders.status'] == 3) echo 'selected="selected"'; ?>>w drodze</option>
			<option value="8" <?php if ((int)$_POST["like"]['orders.status'] == 8) echo 'selected="selected"'; ?>>weryfikacja w Unimet</option>
			<option value="0" <?php if ((int)$_POST["like"]['orders.status'] == 0) echo 'selected="selected"'; ?>>wszystkie</option>
			<option value="2" <?php if ((int)$_POST["like"]['orders.status'] == 2) echo 'selected="selected"'; ?>>w trakcie realizacji</option>
			<option value="6" <?php if ((int)$_POST["like"]['orders.status'] == 6) echo 'selected="selected"'; ?>>u analityka</option>
			<option value="10" <?php if ((int)$_POST["like"]['orders.status'] == 10) echo 'selected="selected"'; ?>>u administratora</option>
			<option value="7" <?php if ((int)$_POST["like"]['orders.status'] == 7) echo 'selected="selected"'; ?>>u kierownika</option>
			<option value="9" <?php if ((int)$_POST["like"]['orders.status'] == 9) echo 'selected="selected"'; ?>>u zamawiającego</option>
			</select>
		</th>
		<th style="width:160px">
			<select name="types[order_details.order_id]">
			<option value="0" <?php if ((int)$_POST["types"]['order_details.order_id'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['order_details.order_id'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[order_details.order_id]" style="width:35px" value="<?php echo $_POST["like"]['order_details.order_id']; ?>" /> 
		</th>
		<th style="width:160px">
			<select name="types[orders.number]">
			<option value="0" <?php if ((int)$_POST["types"]['orders.number'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="-1" <?php if ((int)$_POST["types"]['orders.number'] == -1) echo 'selected="selected"'; ?>>mniejszy niż</option>
			<option value="2" <?php if ((int)$_POST["types"]['orders.number'] == 2) echo 'selected="selected"'; ?>>większy niż</option>
			</select>
			<input type="text" name="like[orders.number]" style="width:35px" value="<?php echo $_POST["like"]['orders.number']; ?>" /> 
		</th>
		<th style="width:160px">
			<select name="types[order_details.query_id]">
			<option value="0" <?php if ((int)$_POST["types"]['order_details.query_id'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['order_details.query_id'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[order_details.query_id]" style="width:35px" value="<?php echo $_POST["like"]['order_details.query_id']; ?>" /> 
		</th>
		<th style="width:160px">
			<select name="types[contractor_login]">
			<option value="0" <?php if ((int)$_POST["types"]['contractor_login'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['contractor_login'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[contractor_login]" style="width:50px" value="<?php echo $_POST["like"]['contractor_login']; ?>" /> 
		</th>
		<th style="width:160px">
			<input type="hidden" name="types[wsk_budget.department]" value="0" />
			<select name="like[wsk_budget.department]">
			<option value="">wszystkie</option>
			<?php 
			foreach ((array)$budget_list as $budget)
			{
				?><option value="<?php echo $budget['department']; ?>" <?php if ($_POST["like"]['wsk_budget.department'] == $budget['department']) { echo 'selected="selected"'; } ?>><?php echo $budget['department']; ?></option><?php
			}
			?>
			</select>
		</th>
		<th style="width:160px">
			<select name="types[wsk_mpk.mpk]">
			<option value="0" <?php if ((int)$_POST["types"]['wsk_mpk.mpk'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['wsk_mpk.mpk'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[wsk_mpk.mpk]" style="width:50px" value="<?php echo $_POST["like"]['wsk_mpk.mpk']; ?>" /> 
		</th>
		<th style="width:300px">
			<select name="types[order_details.name]">
			<option value="0" <?php if ((int)$_POST["types"]['order_details.name'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['order_details.name'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[order_details.name]" style="width:130px" value="<?php echo $_POST["like"]['order_details.name']; ?>" /> 
			
		</th>
		<th style="width:160px">
			<select name="types[order_details.symbol]">
			<option value="0" <?php if ((int)$_POST["types"]['order_details.symbol'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['order_details.symbol'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[order_details.symbol]" style="width:50px" value="<?php echo $_POST["like"]['order_details.symbol']; ?>" /> 
		</th>
		<th style="width:160px">
			<select name="types[order_details.producer]">
			<option value="0" <?php if ((int)$_POST["types"]['order_details.producer'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['order_details.producer'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[order_details.producer]" style="width:50px" value="<?php echo $_POST["like"]['order_details.producer']; ?>" /> 
		</th>
		<th style="width:160px">
			<select name="types[order_details.amount]">
			<option value="3" <?php if ((int)$_POST["types"]['order_details.amount'] == 3) echo 'selected="selected"'; ?>>równy</option>
			<option value="-1" <?php if ((int)$_POST["types"]['order_details.amount'] == -1) echo 'selected="selected"'; ?>>mniejszy niż</option>
			<option value="2" <?php if ((int)$_POST["types"]['order_details.amount'] == 2) echo 'selected="selected"'; ?>>większy niż</option>
			</select>
			<input type="text" name="like[order_details.amount]" style="width:35px" value="<?php echo $_POST["like"]['order_details.amount']; ?>" /> 
		</th>
		<th style="width:160px">
			<select name="types[order_details.tax]">
			<option value="3" <?php if ((int)$_POST["types"]['order_details.tax'] == 3) echo 'selected="selected"'; ?>>równy</option>
			<option value="-1" <?php if ((int)$_POST["types"]['order_details.tax'] == -1) echo 'selected="selected"'; ?>>mniejszy niż</option>
			<option value="2" <?php if ((int)$_POST["types"]['order_details.tax'] == 2) echo 'selected="selected"'; ?>>większy niż</option>
			</select>
			<input type="text" name="like[order_details.tax]" style="width:35px" value="<?php echo $_POST["like"]['order_details.tax']; ?>" /> 
		</th>
		<th style="width:110px">&nbsp;</th>
		<th style="width:160px">
			<select name="types[order_details.netto_price]">
			<option value="3" <?php if ((int)$_POST["types"]['order_details.netto_price'] == 3) echo 'selected="selected"'; ?>>równy</option>
			<option value="-1" <?php if ((int)$_POST["types"]['order_details.netto_price'] == -1) echo 'selected="selected"'; ?>>mniejszy niż</option>
			<option value="2" <?php if ((int)$_POST["types"]['order_details.netto_price'] == 2) echo 'selected="selected"'; ?>>większy niż</option>
			</select>
			<input type="text" name="like[order_details.netto_price]" style="width:35px" value="<?php echo $_POST["like"]['order_details.netto_price']; ?>" /> 
		</th>
		<th style="width:160px">&nbsp;</th>
		<th style="width:160px">&nbsp;</th>
		<th class="last" style="width:280px">
			<input type="hidden" name="types[orders.add_date]" value="4" />
			od <input class="datepicker" type="text" name="like[orders.add_date_from]" style="width:70px" value="<?php echo $_POST["like"]['orders.add_date_from']; ?>" /> 
			do <input class="datepicker" type="text" name="like[orders.add_date_to]" style="width:70px" value="<?php echo $_POST["like"]['orders.add_date_to']; ?>" /> 
		</th>
		<th class="last" style="width:160px">
			<select name="types[order_informations.fv]">
			<option value="0" <?php if ((int)$_POST["types"]['order_informations.fv'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['order_informations.fv'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[order_informations.fv]" style="width:50px" value="<?php echo $_POST["like"]['order_informations.fv']; ?>" /> 
		</th>
		<th class="last" style="width:160px">
			<select name="types[order_informations.wm]">
			<option value="0" <?php if ((int)$_POST["types"]['order_informations.wm'] == 0) echo 'selected="selected"'; ?>>zawiera</option>
			<option value="3" <?php if ((int)$_POST["types"]['order_informations.wm'] == 3) echo 'selected="selected"'; ?>>równy</option>
			</select>
			<input type="text" name="like[order_informations.wm]" style="width:50px" value="<?php echo $_POST["like"]['order_informations.wm']; ?>" /> 
		</th>
	</tr>
	<tr>
		<th>
			<div class="title">Status</div>
			<div class="arrows" alt="orders.status">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">Nr zam.</div>
			<div class="arrows" alt="order_details.order_id">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">Nr wew.</div>
			<div class="arrows" alt="orders.number">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">Nr zap.</div>
			<div class="arrows" alt="order_details.query_id">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">Zamawiający</div>
			<div class="arrows" alt="contractor_login">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">Wydział</div>
			<div class="arrows" alt="wsk_budget.department">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">MPK</div>
			<div class="arrows" alt="wsk_mpk.mpk">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">Produkt</div>
			<div class="arrows" alt="order_details.name">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">Indeks</div>
			<div class="arrows" alt="order_details.symbol">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">Producent</div>
			<div class="arrows" alt="order_details.producer">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">Ilość</div>
			<div class="arrows" alt="order_details.amount">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">VAT</div>
			<div class="arrows" alt="order_details.tax">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">Wartość VAT</div>
			<div class="arrows" alt="order_details.netto_price">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">Cena netto</div>
			<div class="arrows" alt="order_details.netto_price">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">Wartość netto</div>
			<div class="arrows" alt="netto_value">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">Wartość brutto</div>
			<div class="arrows" alt="brutto_value">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">Data złożenia</div>
			<div class="arrows" alt="orders.add_date">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th>
			<div class="title">FV</div>
			<div class="arrows" alt="order_informations.fv">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
		<th class="last">
			<div class="title">WZ</div>
			<div class="arrows" alt="order_informations.wm">
				<p><img class="asc" alt="" src="images/arrow_asc_hidden.png" alt="" /></p>
				<p><img class="desc" alt="" src="images/arrow_desc_hidden.png" alt="" /></p>
			</div>
			<div class="clear"></div>
		</th>
	</tr>
	</table>
	
	<p id="params" style="display:none;"><?php echo serialize($_POST); ?></p>
	
	</div>
	</div>

</div>
</form>

<?php } ?>
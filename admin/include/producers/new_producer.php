<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<label style="width:120px;">&nbsp;</label>
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
	<div class="clear"></div>
</div>
<?php } ?>

<?php
$producer_id = (int)$_GET['id']; 

if (!isset($_POST['add_producer']) && $action != 'edit')
{
	$producers->clear();
}
if (!isset($_POST['save_changes']) && $action == 'edit')
{
	$producers->getProducer($producer_id);
}
?>

<form method="POST" action="" enctype="multipart/form-data">

<input type="hidden" name="type" value="<?php echo $_SESSION["values"]['type']; ?>" />
<input type="hidden" name="producer_id" value="<?php echo $producer_id; ?>" />
	
	<?php  if ($_SESSION["errors"]['name']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['name']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Nazwa producenta <strong>*</strong></label>
		<input type="text" name="name" value="<?php echo $producers->textControll($_SESSION["values"]['name']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['short']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['short']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Krótki opis <strong>*</strong></label>
		<input type="text" name="short" value="<?php echo $producers->textControll($_SESSION["values"]['short']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php if ($_SESSION["values"]['logo']) { ?>
	<div class="form_element">
		<label style="width:120px;">&nbsp;</label>
		<img src="<?php echo BASE_ADDRESS; ?>../images/producers/<?php echo $_SESSION["values"]['logo']; ?>" alt="" />
		<div class="clear"></div>
	</div>
	<?php } if ($_SESSION["errors"]['logo']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['logo']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Logo</label>
		<input style="width:200px;" name="logo" type="file" />
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['description']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['description']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Opis producenta</label>
		<div style="float:left;">
			<?php
			$oFCKeditor = new FCKeditor('description');
			$oFCKeditor->BasePath = './fckeditor/';
			$oFCKeditor->Value = $_SESSION["values"]['description'];
			$oFCKeditor->Width = 400;
			$oFCKeditor->Height = 200;
			$oFCKeditor->ToolbarSet = 'Box';
			$oFCKeditor->Create();
			?>
		</div>
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['link']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['link']; ?>
		<div class="clear"></div>
	</div>
	<?php  } 
		
	if (empty($_SESSION["values"]['link']))
	{
		$_SESSION["values"]['link'] = 'http://';
	}
	?>
	<div class="form_element">
		<label style="width:120px;">Strona www</label>
		<input type="text" name="link" value="<?php echo $producers->textControll($_SESSION["values"]['link']); ?>" />
		<div class="clear"></div>
	</div>
	
	<div class="form_element">
		<label style="width:120px;">&nbsp;</label>
		<?php if ($action == 'edit') { ?>
		<input id="submit" name="save_changes" value="Zapisz zmiany" type="submit" /> 
		<?php } else { ?>
		<input id="submit" name="add_producer" value="Dodaj producenta" type="submit" /> 
		<?php } ?>
		<div class="clear"></div>
	</div>
</form> 
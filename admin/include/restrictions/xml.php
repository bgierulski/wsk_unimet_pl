<?php
session_start();

header ('Content-Type: application/xml; charset=utf-8');

include(dirname(__FILE__).'/../config.php');
include(BASE_DIR.'default/class.DataBase.php');
include(BASE_DIR.'default/class.Validators.php');
include(BASE_DIR.'restrictions/class.Restrictions.php');

$restrictions = new Restrictions();

$operation = trim($_POST['operation']);

?>
<response>
<?php 
switch ($operation)
{
	case 'find_users': 
		
		$words = $restrictions->findUsers($_POST['key']); 
		
		if (mysql_num_rows($words))
		{
			while($word = mysql_fetch_array($words)) 
	   		{
	       		$xml .= '<word><![CDATA['.$word['name'].']]></word>';
	   		}
		}
		
		break;
	
	case 'find_products': 
		
		$words = $restrictions->findProducts($_POST['key']); 
		
		if (mysql_num_rows($words))
		{
			while($word = mysql_fetch_array($words)) 
	   		{
	       		$xml .= '<word><![CDATA['.$word['name'].']]></word>';
	   		}
		}
		
		break;
	
	case 'find_groups': 
		
		$words = $restrictions->findGroups($_POST['key']); 
		
		if (is_array($words)) 
		{
			foreach ($words as $word) 
			{
				$xml .= '<word><![CDATA['.$word.']]></word>';
			}
		}
		
		break;
	
	case 'find_producers':
		
		$words = $restrictions->findProducers($_POST['key']); 
		
		if (mysql_num_rows($words))
		{
			while($word = mysql_fetch_array($words)) 
	   		{
	       		$xml .= '<word><![CDATA['.$word['name'].']]></word>';
	   		}
		}
		
		break;
}

echo $xml;
?>

</response>
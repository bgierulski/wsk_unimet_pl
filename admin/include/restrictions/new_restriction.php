<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<label style="width:100px;">&nbsp;</label>
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
	<div class="clear"></div>
</div>
<?php } ?>

<?php
if (!isset($_POST['add_restriction']))
{
	$restrictions->clear();
}
?>

<form method="POST" action="" id="restriction_form">

	<?php if ($_SESSION["errors"]['user']) { ?>
	<div class="form_element error">
		<label style="width:100px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['user']; ?>
		<div class="clear"></div>
	</div>
	<?php } ?>
	<div class="form_element">
	<label style="width:100px;">Użytkownik *</label>
	<input type="text" name="user" value="<?php echo $_SESSION["values"]['user']; ?>" autocomplete="off" />
	<div class="clear"></div>
	</div> 

	<?php if ($_SESSION["errors"]['type']) { ?>
	<div class="form_element error">
		<label style="width:100px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['type']; ?>
		<div class="clear"></div>
	</div>
	<?php } ?>
	<div class="form_element">
		<label style="width:100px;">Zakres *</label>
		<select name="type" style="width:202px"> 
		<option value="0">-- wybierz --</option> 
		<option value="1" <?php if ($_SESSION["values"]['type'] == 1) { echo 'selected="selected"'; } ?>>grupa</option> 
		<option value="2" <?php if ($_SESSION["values"]['type'] == 2) { echo 'selected="selected"'; } ?>>producent</option> 
		<option value="3" <?php if ($_SESSION["values"]['type'] == 3) { echo 'selected="selected"'; } ?>>pojedyńczy produkt</option>
		</select>
		<div class="clear"></div>
	</div>
	
	<div id="group_element" style="display:<?php echo (($_SESSION["values"]['type'] == 1) ? "block" : "none") ?>">
		<?php if ($_SESSION["errors"]['group']) { ?>
		<div class="form_element error">
			<label style="width:100px;">&nbsp;</label>
			<?php echo $_SESSION["errors"]['group']; ?>
			<div class="clear"></div>
		</div>
		<?php } ?>
		<div class="form_element">
		<label style="width:100px;">Grupa *</label>
		<input type="text" name="group" autocomplete="off" value="<?php echo $_SESSION["values"]['group']; ?>" />
		<div class="clear"></div>
		</div> 
	</div>
		
	<div id="producent_element"  style="display:<?php echo (($_SESSION["values"]['type'] == 2) ? "block" : "none") ?>">
		<?php if ($_SESSION["errors"]['producer']) { ?>
		<div class="form_element error">
			<label style="width:100px;">&nbsp;</label>
			<?php echo $_SESSION["errors"]['producer']; ?>
			<div class="clear"></div>
		</div>
		<?php } ?>
		<div class="form_element">
		<label style="width:100px;">Producent *</label>
		<input type="text" name="producer" autocomplete="off" value="<?php echo $_SESSION["values"]['producer']; ?>" />
		<div class="clear"></div>
		</div> 
	</div>
	
	<div id="product_element"  style="display:<?php echo (($_SESSION["values"]['type'] == 3) ? "block" : "none") ?>">
		<?php if ($_SESSION["errors"]['product']) { ?>
		<div class="form_element error">
			<label style="width:100px;">&nbsp;</label>
			<?php echo $_SESSION["errors"]['product']; ?>
			<div class="clear"></div>
		</div>
		<?php } ?>
		<div class="form_element">
		<label style="width:100px;">Produkt *</label>
		<input type="text" name="product" value="<?php echo $_SESSION["values"]['product']; ?>" autocomplete="off" />
		<div class="clear"></div>
		</div> 
	</div>
	
	<div class="form_element">
		<label style="width:100px;">&nbsp;</label>
		<input id="submit" name="add_restriction" value="Dodaj ograniczenie" type="submit" /> 
		<div class="clear"></div>
	</div>
</form> 
<?php
$list = $informations->getInformations();
?>

<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["Informations"]['error']); ?>
</div>
<?php } else if (isset($_SESSION["communicats"]['ok'])) { ?>
<div class="form_element ok">
	<?php echo $_SESSION["communicats"]['ok']; unset($_SESSION["Informations"]['ok']); ?>
</div>
<?php } ?>

<table>
	<tr>
		<th>Ważny od</th>
		<th>Ważny do</th>
		<th>Tytuł</th>
		<th>Operacje</th>
	</tr>
	<?php
	if (is_array($list))
	{
		foreach ($list as $key => $row)
	   	{
			?>
		    <tr>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['start']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['end']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>"><?php echo $row['title']; ?></td>
				<td class="bg_<?php echo ($key%2)+1; ?>">
				<a class="usun" href="./informations/delete/<?php echo $row['id']; ?>">usuń komunikat</a> &nbsp;::&nbsp;
				<a href="./informations/edit/<?php echo $row['id']; ?>">edytuj komunikat</a>
				</td>
			</tr>
		    <?php
	   	}
	}
	?>
</table>

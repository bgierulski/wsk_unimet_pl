<?php if (isset($_SESSION["communicats"]['error'])) { ?>
<div class="form_element error">
	<label style="width:120px;">&nbsp;</label>
	<?php echo $_SESSION["communicats"]['error']; unset($_SESSION["communicats"]['error']); ?>
	<div class="clear"></div>
</div>
<?php } ?>

<?php
$information_id = (int)$_GET['id']; 

if (!isset($_POST['add_information']) && $action != 'edit')
{
	$informations->clear();
}
if (!isset($_POST['save_changes']) && $action == 'edit')
{
	$informations->getInformation($information_id);
}
?>

<form method="POST" action="">

<input type="hidden" name="information_id" value="<?php echo $information_id; ?>" />
	
	<?php if ($_SESSION["errors"]['title']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['title']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Tytuł <strong>*</strong></label>
		<input type="text" name="title" value="<?php echo $informations->textControll($_SESSION["values"]['title']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php if ($_SESSION["errors"]['start']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['start']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Ważny od <strong>*</strong></label>
		<input type="text" name="start" class="date" value="<?php echo $informations->textControll($_SESSION["values"]['start']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php if ($_SESSION["errors"]['end']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['end']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Ważny do <strong>*</strong></label>
		<input type="text" name="end" class="date" value="<?php echo $informations->textControll($_SESSION["values"]['end']); ?>" />
		<div class="clear"></div>
	</div>
	
	<?php  if ($_SESSION["errors"]['content']) { ?>
	<div class="form_element error">
		<label style="width:120px;">&nbsp;</label>
		<?php echo $_SESSION["errors"]['content']; ?>
		<div class="clear"></div>
	</div>
	<?php  } ?>
	<div class="form_element">
		<label style="width:120px;">Opis producenta</label>
		<div style="float:left;">
			<?php
			$oFCKeditor = new FCKeditor('content');
			$oFCKeditor->BasePath = './fckeditor/';
			$oFCKeditor->Value = $_SESSION["values"]['content'];
			$oFCKeditor->Width = 600;
			$oFCKeditor->Height = 300;
			$oFCKeditor->ToolbarSet = 'Box';
			$oFCKeditor->Create();
			?>
		</div>
		<div class="clear"></div>
	</div>
	
	<div class="form_element">
		<label style="width:120px;">&nbsp;</label>
		<?php if ($action == 'edit') { ?>
		<input id="submit" name="save_changes" value="Zapisz zmiany" type="submit" /> 
		<?php } else { ?>
		<input id="submit" name="add_information" value="Dodaj komunikat" type="submit" /> 
		<?php } ?>
		<div class="clear"></div>
	</div>
</form> 
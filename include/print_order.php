<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start();

include('config.php');
include(BASE_DIR.'class/class.DataBase.php');
include(BASE_DIR.'class/class.Validators.php');
include(BASE_DIR.'class/class.Prices.php');
include(BASE_DIR.'class/class.Orders.php');

$order_id = (int)$_GET['id'];
$key = mysql_real_escape_string($_GET['token']);

$db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="reply-to" content="Adres_e-mail" />
<meta name="author" content="Autor_dokumentu" />
<meta name="description" content="Opis" />

<title>UNIMET</title>
</head>

<body onload="window.print();">
<?php
$result = $db->query('SELECT user_id FROM orders JOIN users ON orders.user_id = users.id WHERE users.id = \''.$_SESSION["b2b_user"]['id'].'\' AND orders.id = '.$order_id.' AND `key` = \''.$key.'\'');

if ($db->num_rows($result))
{
	$list = $orders->getOrder($order_id);
	?>
	<table cellspacing="0" cellpadding="0">
		<tr>
			<td style="padding:2px 5px 2px 0px;"><strong>Nazwa</strong></td>
			<td style="padding:2px 5px 2px 5px; text-align:center;"><strong>Ilość</strong></td>
			<td style="padding:2px 5px 2px 5px; text-align:center;"><strong>Cena netto</strong></td>
			<td style="padding:2px 5px 2px 5px; text-align:center;"><strong>Wartość netto</strong></td>
			<td style="padding:2px 5px 2px 5px; text-align:center;"><strong>Wartość brutto</strong></td>
		</tr>
		<?php
		foreach ($list as $row)
		{
			$tax = ((int)$row['tax']*$row['netto_price'])/100;
			$product_tax = sprintf("%0.2f", $tax*$row['amount']);
			$netto_value = sprintf("%0.2f", ($row['netto_price'] * $row['amount']));
			$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
			?>
			<tr>
				<td style="padding:2px 5px 2px 0px;"><?php echo $row['name']; ?></td>
				<td style="padding:2px 5px 2px 5px; text-align:center;"><?php echo $row['amount']; ?></td>
				<td style="padding:2px 5px 2px 5px; text-align:center;"><?php echo $row['netto_price']; ?> zł</td>
				<td style="padding:2px 5px 2px 5px; text-align:center;"><?php echo $netto_value; ?> zł</td>
				<td style="padding:2px 5px 2px 5px; text-align:center;"><?php echo $brutto_value; ?> zł</td>
			</tr>
			<?php
			
			$weight += $row['amount'] * $row['weight'];
			
			if ($row['pallet'] == 1)
			{
				$pallets++;
			}
			
			$netto_sum += $netto_value;
			$tax_sum += $product_tax; 
		}
		
		if ($list[0]['payment'] == 'za pobraniem ( + 6zł)')
		{
			$payment = 1;
		}
		else 
		{
			$payment = 0;
		}
			
		$transport_brutto = sprintf("%0.2f", $prices->getTransportPrice($weight, $list[0]['brutto_value'], $pallets, $payment));
		$transport_netto  = sprintf("%0.2f", (100*$transport_brutto/122));
		$transport_tax = sprintf("%0.2f", (22*($transport_netto)/100));
		
		$total_sum = sprintf("%0.2f", ($list[0]['brutto_value'] + $transport_brutto));
		?>
		<tr>
			<td style="padding:2px 5px 2px 0px;">Transport</td>
			<td style="padding:2px 5px 2px 5px; text-align:center;"></td>
			<td style="padding:2px 5px 2px 5px; text-align:center;"><?php echo $transport_netto; ?> zł</td>
			<td style="padding:2px 5px 2px 5px; text-align:center;"><?php echo $transport_netto; ?> zł</td>
			<td style="padding:2px 5px 2px 5px; text-align:center;"><?php echo $transport_brutto; ?> zł</td>
		</tr>
	</table>
	
	<p><strong>Podsumowanie transakcji</strong><br />
	Numer zamówienia: <strong><?php echo $orders->formatId($list[0]['id']); ?></strong><br />
	Forma płatności: <strong><?php echo $list[0]['payment']; ?></strong><br />
	Kwota zamówienia: <strong><?php echo $total_sum; ?> PLN</strong><br />
	Data złożenia zamówienia: <strong><?php echo $list[0]['add_date']; ?></strong><br />
	Dokument: <strong><?php echo $list[0]['document']; ?></strong></p>
	
	<p><strong>Dane zamawiającego</strong><br />
	<strong>Użytkownik:</strong> <?php echo $orders->textControll($list[0]['user_name']); ?><br />
	<strong>Adres dostawy:</strong> <br /><?php 
	if ($list[0]['delivery_post_code'] != '')
	{
		echo $list[0]['delivery_post_code'].' '.$list[0]['delivery_city'].'<br />'.$list[0]['delivery_street'].' '.$list[0]['delivery_home']; 
		if ($list[0]['delivery_flat'] != '') echo '/'.$list[0]['delivery_flat'];
	}
	else
	{
		echo $list[0]['post_code'].' '.$list[0]['city'].'<br />'.$list[0]['street'].' '.$list[0]['home']; 
		if ($list[0]['flat'] != '') echo '/'.$list[0]['flat'];
	}
	?>
	</p>
	<?php
}
?>
</body>
</html>
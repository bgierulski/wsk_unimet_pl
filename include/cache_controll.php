<?php 
if ($handle = opendir(CACHE_DIR)) 
{
	$controll_date = date("Y-m-d", mktime(0,0,0,date("m"),(date("d")-1),date("Y")));
	
    while (false !== ($file = readdir($handle))) 
    { 
    	$file_table = explode('.', $file);
    	
    	if ($controll_date > date('Y-m-d', strtotime($file_table[1])) && !empty($file_table[1]) && $file_table[1] != "xxx")
    	{
    		@unlink(CACHE_DIR.$file);
    	}
    }

    closedir($handle); 
}
?>
<?php
class Concessions extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_concession_id;
	private $_name;
	private $_email;
	private $_phone;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
	}
	
	public function getConcessions()
	{
		$result = $this->_db->query('SELECT id, name, email, phone FROM concessions ORDER BY id ASC') or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getConcession($concession_id)
	{
		$result = $this->_db->query('SELECT id, name, phone, email FROM concessions WHERE id = '.$concession_id) or $this->_db->raise_error();
		$row = mysql_fetch_array($result);
		
		$values['name'] = $row['name'];
		$values['email'] = $row['email'];
		$values['phone'] = $row['phone'];
		
		$this->setSession("values", $values);
	}
	
	public function validateConcession()
	{
		$this->clear();
		
		$this->_concession_id = (int)$_POST['concession_id'];
		$this->_name = $values['name'] = trim(mysql_escape_string($_POST['name']));
		$this->_email = $values['email'] = trim(mysql_escape_string($_POST['email']));
		$this->_phone = $values['phone'] = trim(mysql_escape_string($_POST['phone']));
		
		if (empty($this->_name))
		{
			$this->_errors++;
			$errors['name'] = 'Imię i nazwisko jest wymagane';
		}
		
		if (empty($this->_email))
		{
			$this->_errors++;
			$errors['email'] = 'E-mail jest wymagany';
		}
		
		if (!$this->_errors)
		{
			if (isset($_POST['add_concession']))
			{
				$this->_addConcession();
			}
			else 
			{
				$this->_saveChanges();
			}
		}
		else 
		{
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
		
	}
	
	private function _addConcession()
	{		
	}
	
	private function _saveChanges()
	{
		$this->_db->query('UPDATE concessions SET name = \''.$this->_name.'\', phone = \''.$this->_phone.'\', email = \''.$this->_email.'\' 
		WHERE id = '.$this->_concession_id) or $this->_db->raise_error();
		
		$communicats['ok'] = 'Dane zostały wyedytowane';
		$this->setSession("communicats", $communicats);
		header("Location: ../../concessions/list");
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}
?>
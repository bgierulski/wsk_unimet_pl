<div id="person_description">
	<div id="images">
	</div>
	<div class="clear"></div>
</div>

<?php 
$persons = $users->getPersons((int)$_SESSION["b2b_user"]['id']);
?>

<div id="below_1024" style="float:right; display:none;">
<?php
if ($persons['name'] != '')
{
	?>
	<div class="person" id="merchant_<?php echo (int)$persons['id']; ?>" style="float:none; padding-top:5px;">
		<p><strong>Państwa Doradca Techniczno-Handlowy:</strong></p>
		<p><?php echo $users->textControll($persons['name']); ?></p>
	</div>
	<?php
}

if ($persons['guardian_name'] != '')
{
	?>
	<div class="person" id="guardian_<?php echo (int)$persons['id']; ?>" style="float:none; padding-top:5px;">
		<p><strong>Państwa Opiekun Handlowy:</strong></p>
		<p><?php echo $users->textControll($persons['guardian_name']); ?></p>
	</div>
	<?php
}
?>
</div>

<div id="above_1024">
<?php
if ($persons['name'] != '')
{
	?>
	<div class="person" id="merchant_<?php echo (int)$persons['id']; ?>">

		<div>
			<div class="image">
			<?php if (!empty($persons['merchant_image'])) { ?>
			<img src="<?php echo BASE_ADDRESS; ?>/include/person_image.php?id=<?php echo (int)$persons['id']; ?>&file=<?php echo $persons['merchant_image']; ?>&w=38&h=38" alt="" />
			<?php } else { ?>
			<img src="images/image_1.png" alt="" />
			<?php } ?>
			</div>
			<div>
				<p><strong>Państwa Doradca Techniczno-Handlowy:</strong></p>
				<p><?php echo $users->textControll($persons['name']); ?></p>
				<?php if (!empty($persons['phone'])) { ?><p>tel.: <?php echo $persons['phone']; ?></p><?php } ?>
				<?php if (!empty($persons['email'])) { ?><p><a href="mailto:<?php echo $persons['email']; ?>"><?php echo $persons['email']; ?></a></p><?php } ?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<?php
}

if ($persons['guardian_name'] != '')
{
	?>
	<div class="person" id="guardian_<?php echo (int)$persons['id']; ?>">
	
		<div>
			<div class="image">
			<?php if (!empty($persons['guardian_image'])) { ?>
			<img src="<?php echo BASE_ADDRESS; ?>/include/person_image.php?id=<?php echo (int)$persons['id']; ?>&file=<?php echo $persons['guardian_image']; ?>&w=38&h=38" alt="" />
			<?php } else { ?>
			<img src="images/image_1.png" alt="" />
			<?php } ?>
			</div>
			<div>
				<p><strong>Państwa Opiekun Handlowy:</strong></p>
				<p><?php echo $users->textControll($persons['guardian_name']); ?></p>
				<?php if (!empty($persons['guardian_phone'])) { ?><p>tel.: <?php echo $persons['guardian_phone']; ?></p><?php } ?>
				<?php if (!empty($persons['guardian_email'])) { ?><p><a href="mailto:<?php echo $persons['guardian_email']; ?>"><?php echo $persons['guardian_email']; ?></a></p><?php } ?>
			</div>
			<div class="clear"></div>
		</div>

	</div>
	<?php
}
?>
</div>

<div class="clear"></div>
<?php 
if ("178.219.17.138" == $_SERVER['REMOTE_ADDR'])
{
	$orders->correction();
}

if (in_array($action, array("edit", "show")))
{
	$orders->setRepresentative((int)$id);
}
?>

<div id="title_element">
	<p>
	<strong>
	<?php
	switch ($action)
	{
		case 'waiting': ?>Podgląd zamówień oczekujących na realizację<?php break;
		case 'ongoing': ?>Podgląd zamówień w trakcie realizacji<?php break;
		case 'realized': ?>Podgląd zamówień zrealizowanych<?php break;
		case 'archium': ?>Podgląd zamówień archiwalnych<?php break;
		case 'anulowane': ?>Podgląd zamówień odrzuconych<?php break;
		case 'return': ?>Zwrócone zamówienia<?php break;
		case 'edit': ?>Edycja danych<?php break;
		case 'budgets': ?>Szczegóły budżetów<?php break;
		default: ?>Podgląd wszystkich zamówień<?php break;
	}
	?>
	</strong></p>
</div>

<div id="page_padding">
<?php

$user_names = $orders->getUsersNames();

switch ($action)
{
	case 'check': include('edit_order.php'); break;
	case 'edit': include('edit_order.php'); break;
	case 'show': include('edit_order.php'); break;
	case 'budgets': include('budgets_list.php'); break;
	default: include('orders_list.php'); break;
}
?>
</div>
<?php 
if ($_GET['param2'] == 'szablon')
{
	$templates->saveTemplate();
}
?>

<div id="page_header" style="background:none;">
	<div class="details">
		<p><strong>MPK</strong> <?php if ($_SESSION["path_communicats"]['ok']) { echo ' - '.$_SESSION["path_communicats"]['ok']; } unset($_SESSION["path_communicats"]['ok']); ?></p>
	</div>
	<div class="clear"></div>
</div>

<div id="page_content">

<div id="during_the_verification">
<div>
	<p>Twoje zamówienie jest w trakcie wstępnej weryfikacji.</p>
	<p><strong>Proszę czekać</strong></p>
	<p><img src="images/loading_transparent.gif" alt="" /></p>
</div>
</div>

<?php
$mpk_list = $basket->getMpkPaths();
$budgets_list = $users->getBudgets();
$users_list = $users->getUsersList();

if (count($mpk_list))
{
	?>
	<form method="post" name="paths" action="./sciezki.html">
	<input type="hidden" name="operation" value="change_paths" />
	<table class="proteges_list">
	<tr>
		<th>MPK</th>
		<th>Budżet</th>
		<th>Analityk</th>
		<th>Kierownik</th>
		<th>Odbiorca</th>
	</tr>
	<?php
	$counter = 0;
	
	foreach ($mpk_list as $key => $row)
	{
		if ($counter%2 == 0)
		{
			$class = 'bg_1';
		}
		else 
		{
			$class = 'bg_2';
		}
		?>
		<tr>
			<td class="<?php echo $class; ?>"><?php echo $row['mpk']; ?> <input type="hidden" name="mpk[]" value="<?php echo $row['mpk']; ?>" /></td>
			<td class="<?php echo $class; ?>">
				<select name="budget[]" style="width:90%" id="row_<?php echo $counter; ?>">
				<?php if ($row['budget'] == 0) {?>
				<option value="0">-- wybierz --</option>
				<?php } ?>
				<?php 
				foreach ($budgets_list as $budget)
				{
					if (($row['budget'] > 0 && $budget['id'] == $row['budget']) || $row['budget'] == 0)
					{
						?><option value="<?php echo $budget['id']; ?>" <?php if (($budget['id'] == $row['budget']) || (is_array($_SESSION["values"]["budget"]) && $budget['id'] == $_SESSION["values"]["budget"][$counter])) { echo 'selected="selected"'; } ?>><?php echo $budget['department']; ?></option><?php
					}
				}
				?>
				</select>
			</td>
			
			<td class="<?php echo $class; ?>">
				<select name="analyst[]" style="width:90%">
				<?php if (count($row["analyst"]) > 1 || empty($row["analyst"])) { ?>
				<option value="0">-- wybierz --</option>
				<?php } ?>
				<?php 
				foreach ($users_list as $user)
				{
					if ($_SESSION["values"]["budget"][$counter] == $user['budget'])
					{
						if ($user['analyst'] > 0 && (empty($row["analyst"]) || (!empty($row["analyst"]) && in_array($user['id'], $row["analyst"]))))
						{
							?><option alt="<?php echo $user['budget']; ?>" value="<?php echo $user['id']; ?>" <?php if (is_array($_SESSION["values"]["analyst"]) && $user['id'] == $_SESSION["values"]["analyst"][$counter]) { echo 'selected="selected"'; } ?>><?php echo $user['login']; ?></option><?php
						}
					}
				}
				?>
				</select>
			</td>
			<td class="<?php echo $class; ?>">
				<select name="manager[]" style="width:90%">
				<?php if (count($row["manager"]) > 1 || empty($row["manager"])) { ?>
				<option value="0">-- wybierz --</option>
				<?php } ?>
				<?php 
				foreach ($users_list as $user)
				{
					if ($_SESSION["values"]["budget"][$counter] == $user['budget'])
					{
						if ($user['manager'] > 0 && (empty($row["manager"]) || (!empty($row["manager"]) && in_array($user['id'], $row["manager"]))))
						{
							?><option alt="<?php echo $user['budget']; ?>" value="<?php echo $user['id']; ?>" <?php if (is_array($_SESSION["values"]["manager"]) && $user['id'] == $_SESSION["values"]["manager"][$counter]) { echo 'selected="selected"'; } ?>><?php echo $user['login']; ?></option><?php
						}
					}
				}
				?>
				</select>
			</td>
			<td class="<?php echo $class; ?>">
				<select name="receiving[]" style="width:90%">
				<?php if (count($row["receiving"]) > 1 || empty($row["receiving"])) { ?>
				<option value="0">-- wybierz --</option>
				<?php } ?>
				<?php 
				foreach ($users_list as $user)
				{
					if ($_SESSION["values"]["budget"][$counter] == $user['budget'])
					{
						if ($user['receiving'] > 0 && (empty($row["receiving"]) || (!empty($row["receiving"]) && in_array($user['id'], $row["receiving"]))))
						{
							?><option alt="<?php echo $user['budget']; ?>" value="<?php echo $user['id']; ?>" <?php if (is_array($_SESSION["values"]["receiving"]) && $user['id'] == $_SESSION["values"]["receiving"][$counter]) { echo 'selected="selected"'; } ?>><?php echo $user['login']; ?></option><?php
						}
					}
				}
				?>
				</select>
			</td>
		</tr>
		<?php
		
		$counter++;
	}
	?>
	</table>
	</form>
	<?php
}

$products_list = $basket->getProducts();

if (count($products_list))
{
	?>
	<div>
		<div id="basket_info">
			<p>Łączna waga towaru: <?php echo $basket->getWeight(); ?> kg</p>
			<?php $netto = $basket->getNettoValue(); ?>
			<p>Wartość zamówienia netto: <strong><?php echo $netto; ?> zł</strong></p>
		</div>
		<div id="to_realization">
			<p class="button" style="margin-bottom:5px; margin-left:20px;"><a href="./zamowienie.html" class="next_order_step">REALIZUJ ZAMÓWIENIE</a></p>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
	<!--  	
	<p id="transport_info">* - Przy przesyłkach wielkogabarytowych i niestandardowych koszt transportu może ulec zmianie! </p> 
	-->
	<?php
}
else 
{
	?>Brak produktów w koszyku<?php
}
?>

<div>
<p class="button" style="padding-top:10px;"><a href="./koszyk.html">POWRÓT DO KOSZYKA</a></p>
<div class="clear"></div>
</div>

</div>

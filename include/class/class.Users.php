<?php
class Users extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_id_hermes;
	private $_parent_id;
	private $_user_id;
	private $_status;
	private $_login;
	private $_password;
	private $_repeated_password;
	private $_name;
	private $_user_name;
	private $_user_surname;
	private $_delivery_point;
	private $_email;
	private $_phone;
	private $_fax;
	private $_ordering;
	private $_invoices;
	private $_admin;
	private $_observer;
	private $_active;
	private $_post_code;
	private $_city;
	private $_street;
	private $_home;
	private $_flat;
	private $_region_id = 0;
	private $_identificator;
	private $_delivery_post_code;
	private $_delivery_city;
	private $_delivery_street;
	private $_delivery_home;
	private $_delivery_flat;
	private $_delivery_region_id;
	private $_top_id;
	private $_permissions = array();
	
	private $_user = array();
	private $_children = array();
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		
		$this->_user = $this->getSession("b2b_user");
		
		if ($_POST['operation'] == 'user_login')
		{
			$this->_login(trim(mysql_real_escape_string($_POST['login'])), trim(mysql_real_escape_string($_POST['password'])));
		}
		else if ($_POST['operation'] == 'user_logout')
		{
			$this->_logount();
		}
		else if ($_POST['operation'] == 'account')
		{
			$this->_validateAccount();
		}
		else if ($_POST['operation'] == 'application' && $this->_user['admin'] == 1)
		{
			$this->_validateApplication();
		}
		/*
		$user = $this->getSession("b2b_user");
		
		if ($user['id'] == 255)
		{
			$result = $this->_db->query('SELECT id, parent_id FROM users WHERE merchant_id = 0');
			
			while($row = mysql_fetch_array($result)) 
	   		{
	   			$this->_db->query('UPDATE users SET merchant_id = '.$this->getParent($row['id']).' WHERE id = '.$row['id']);
	   		}
		}
		*/
	}
	
	public function getCSV($path)
	{/*
		if (file_exists($path) && is_file($path))
		{
			$file_handle = fopen($path, "r");

			$rows = array();
			$counter = 0;
			
			while (($lines = fgets($file_handle, 4096)) !== false) 
			{
				$rows[$counter] = array();
				
				$line = explode(';', $lines);
							
				foreach ($line as $detail)
				{
					if ($detail[0] == '"') { $detail[0] = " "; }
					if ($detail[strlen($detail)-1] == '"') { $detail[strlen($detail)-1] = " "; } 
							
					array_push($rows[$counter], iconv("UTF-8", "UTF-8", trim($detail)));
				}
				
				$counter++;
			}
			
			$exceptions = array();
			
			if (is_array($rows) && !empty($rows))
			{
				foreach ($rows as $row)
				{
					if (!in_array($row[1], $exceptions))
					{
						$query = 'INSERT INTO users VALUES (\'\', \''.$row[0].'\', \''.mysql_real_escape_string($row[1]).'\', \''.mysql_real_escape_string($row[2]).'\', 
						\''.mysql_real_escape_string($row[3]).'\', \''.mysql_real_escape_string($row[4]).'\', \''.mysql_real_escape_string($row[5]).'\', \'\', \'\', \'\', 
						\'\', \'\', \'0\', \''.mysql_real_escape_string($row[11]).'\', \''.mysql_real_escape_string($row[12]).'\', \''.mysql_real_escape_string($row[13]).'\', 
						\''.mysql_real_escape_string($row[14]).'\', \''.mysql_real_escape_string($row[15]).'\', \''.mysql_real_escape_string($row[16]).'\', 
						\''.mysql_real_escape_string($row[17]).'\', \''.mysql_real_escape_string($row[18]).'\', \''.mysql_real_escape_string($row[19]).'\', 
						\''.mysql_real_escape_string($row[20]).'\', \''.mysql_real_escape_string($row[21]).'\', \''.mysql_real_escape_string($row[22]).'\', 
						\''.mysql_real_escape_string($row[23]).'\', \''.mysql_real_escape_string($row[24]).'\', \''.mysql_real_escape_string($row[25]).'\', 
						\''.date('Y-m-d').'\', \''.date('Y-m-d H:i:s').'\', \'unimet\', 0, 0, 0, 0)';
						
						$this->_db->query($query) or $this->_db->raise_error();
						$this->_db->query('INSERT INTO user_tree VALUES (\'\', '.mysql_insert_id().', 255, 1)') or $this->_db->raise_error();
					}
					
					array_push($exceptions, $row[1]);
				}
			}
		}*/
	}
	
	public function getFunctions()
	{
		$query = 'SELECT wsk_budget.department, contractor, analyst, manager, receiving FROM wsk_users_function JOIN wsk_budget ON wsk_users_function.budget 
		= wsk_budget.id AND user_id = '.$this->_user['id'].' WHERE contractor > 0 OR analyst > 0 OR manager > 0 OR receiving > 0 AND wsk_users_function.budget 
		> 0 ORDER BY wsk_budget.department ASC, contractor DESC, analyst DESC, manager DESC, receiving DESC';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result); 
	}
	
	public function checkReceiving($user_id)
	{
		$result = $this->_db->query('SELECT id FROM wsk_users_function WHERE user_id = '.$user_id.' AND receiving = 1') or $this->_db->raise_error();
		return ((mysql_num_rows($result)) ? true : false);
	}
	
	public function getUsersList()
	{
		$query = 'SELECT users.id, login, IFNULL(budget,0) as budget, IFNULL(contractor,0) as contractor, IFNULL(analyst,0) as analyst, IFNULL(manager,0) as 
		manager, IFNULL(receiving,0) as receiving FROM users LEFT JOIN wsk_users_function ON users.id = wsk_users_function.user_id WHERE active = 1 AND 
		status > 0 AND (users.id IN (SELECT user_id FROM user_tree WHERE parent_id = 255) OR users.id = 255) ORDER BY users.login ASC';
		
		$query = 'SELECT users.id, login, IFNULL(budget,0) as budget, IFNULL(contractor,0) as contractor, IFNULL(analyst,0) as analyst, IFNULL(manager,0) as 
		manager, IFNULL(receiving,0) as receiving FROM users LEFT JOIN wsk_users_function ON users.id = wsk_users_function.user_id WHERE active = 1 AND 
		status > 0 AND users.parent_id = 8338 ORDER BY users.login ASC';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getBudgets($all = false)
	{		
		if (true !== $all)
		{
			$query = 'SELECT wsk_budget.id, department, wsk_budget.budget, contractor, analyst, manager FROM wsk_budget JOIN wsk_users_function 
			ON wsk_budget.id = wsk_users_function.budget AND user_id = '.$this->_user['id'].' AND (contractor > 0 OR analyst > 0 OR manager > 0 
			OR receiving > 0) ORDER BY department ASC';
		}
		else 
		{
			$query = 'SELECT `department` FROM `wsk_budget` ORDER BY `department` ASC';
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function checkTopUser($user_id)
	{
		$result = $this->_db->query('SELECT parent_id FROM user_tree WHERE user_id = '.(int)$user_id) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			$this->checkTopUser($row['parent_id']);
		}
		else
		{
			$_SESSION["b2b_user"]['top_id'] = $user_id;
		}
	}
	
	public function getParent($user_id)
	{
		$result = $this->_db->query('SELECT parent_id, level FROM user_tree WHERE user_id = '.$user_id) or $this->_db->raise_error();
	
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			mysql_free_result($result);
			
			if ($row['level'] > 1)
			{
				$result = $this->_db->query('SELECT parent_id, level FROM user_tree WHERE user_id = '.$row['parent_id']) or $this->_db->raise_error();
				
				if (mysql_num_rows($result))
				{
					$row = mysql_fetch_array($result);
					mysql_free_result($result);
					
					if ($row['level'] > 1)
					{
						$result = $this->_db->query('SELECT parent_id, level FROM user_tree WHERE user_id = '.$row['parent_id']) or $this->_db->raise_error();
						
						if (mysql_num_rows($result))
						{
							$row = mysql_fetch_array($result);
							mysql_free_result($result);
							
							if ($row['level'] > 1)
							{
								$result = $this->_db->query('SELECT parent_id, level FROM user_tree WHERE user_id = '.$row['parent_id']) or $this->_db->raise_error();
								
								$row = mysql_fetch_array($result);
								mysql_free_result($result);
							}
						}
					}
				}
			}
			
			$result = $this->_db->query('SELECT id_hermes, merchant_id FROM users WHERE id = '.$row['parent_id']) or $this->_db->raise_error();
			$row = mysql_fetch_array($result);
			
			$this->_id_hermes = $row['id_hermes'];
			return $row['merchant_id'];
		}
		else
		{
			$this->_id_hermes = $this->_user['id_hermes'];
			return $this->_user['merchant_id'];
		}
	}
	
	public function getPerson($type, $id)
	{
		$result = $this->_db->query('SELECT * FROM merchants WHERE id = \''.$id.'\'');
		
		return mysql_fetch_array($result);
	}
	
	private function _validateApplication()
	{
		$this->clear();
		
		$this->_email = $values['email'] = trim(mysql_real_escape_string($_POST['email']));
		$this->_phone = $values['phone'] = trim(mysql_real_escape_string($_POST['phone']));
		$this->_fax = $values['fax'] = trim(mysql_real_escape_string($_POST['fax']));
		$this->_name = $values['name'] = trim(mysql_real_escape_string($_POST['name']));
		$this->_post_code = $values['post_code'] = trim(mysql_real_escape_string($_POST['post_code']));
		$this->_city = $values['city'] = trim(mysql_real_escape_string($_POST['city']));
		$this->_street = $values['street'] = trim(mysql_real_escape_string($_POST['street']));
		$this->_home = $values['home'] = trim(mysql_real_escape_string($_POST['home']));
		$this->_flat = $values['flat'] = trim(mysql_real_escape_string($_POST['flat']));
		$this->_identificator = $values['identificator'] = trim(mysql_real_escape_string($_POST['identificator']));
		
		/*
		$this->_region_id = $values['region_id'] = (int)$_POST['region_id'];
		$this->_delivery_post_code = $values['delivery_post_code'] = trim(mysql_real_escape_string($_POST['delivery_post_code']));
		$this->_delivery_city = $values['delivery_city'] = trim(mysql_real_escape_string($_POST['delivery_city']));
		$this->_delivery_street = $values['delivery_street'] = trim(mysql_real_escape_string($_POST['delivery_street']));
		$this->_delivery_home = $values['delivery_home'] = trim(mysql_real_escape_string($_POST['delivery_home']));
		$this->_delivery_flat = $values['delivery_flat'] = trim(mysql_real_escape_string($_POST['delivery_flat']));
		$this->_delivery_region_id = $values['delivery_region_id'] = (int)$_POST['delivery_region_id'];
		*/
		
		if (!$this->validateEmail($this->_email))
		{
			$this->_errors++;
			$errors['email'] = 'E-mail jest niepoprawny';
		}
		
		if ($this->_phone == '')
		{
			$this->_errors++;
			$errors['phone'] = 'Numer telefonu jest wymagany';
		}
		
		if ($this->_name == '')
		{
			$this->_errors++;
			$errors['name'] = 'Imię i nazwisko lub nazwa firmy jest wymagana';
		}
		
		if (!$this->validatePostCode($this->_post_code))
		{
			$this->_errors++;
			$errors['post_code'] = 'Kod pocztowy jest niepoprawny';
		}
		
		if ($this->_city == '')
		{
			$this->_errors++;
			$errors['city'] = 'Poczta jest wymagana';
		}
		
		if ($this->_street == '')
		{
			$this->_errors++;
			$errors['street'] = 'Ulica lub miejscowość jest wymagana';
		}
		
		if ($this->_home == '')
		{
			$this->_errors++;
			$errors['home'] = 'Numer domu jest wymagany';
		}
		
		if ($this->_identificator != '' && !$this->validateNIP($this->_identificator))
		{
			$this->_errors++;
			$errors['identificator'] = 'Numer NIP jest niepoprawny';
		}
		
		/*
		if (!$this->_region_id)
		{
			$this->_errors++;
			$errors['region_id'] = 'Województwo jest wymagane';
		}
		
		if ($this->_delivery_post_code != '' || $this->_delivery_city != '' || $this->_delivery_street != '' || $this->_delivery_home != '' || $this->_delivery_flat != '' || $this->_delivery_region_id > 0)
		{
			if (!$this->validatePostCode($this->_delivery_post_code))
			{
				$this->_errors++;
				$errors['delivery_post_code'] = 'Kod pocztowy jest niepoprawny';
			}
			
			if ($this->_delivery_city == '')
			{
				$this->_errors++;
				$errors['delivery_city'] = 'Poczta jest wymagana';
			}
			
			if ($this->_delivery_street == '')
			{
				$this->_errors++;
				$errors['delivery_street'] = 'Ulica lub miejscowość jest wymagana';
			}
			
			if ($this->_delivery_home == '')
			{
				$this->_errors++;
				$errors['delivery_home'] = 'Numer domu jest wymagany';
			}
			
			if (!$this->_delivery_region_id)
			{
				$this->_errors++;
				$errors['delivery_region_id'] = 'Województwo jest wymagane';
			}
		}
		*/
		
		if ($this->_errors)
		{
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
		else
		{	
			$this->_sendApplicationEmail();
		}
	}
	
	private function _validateAccount()
	{
		$this->clear();
		
		$this->_parent_id = $values['parent_id'] = (int)$_POST['parent_id'];
		$this->_user_id = $values['user_id'] = (int)$_POST['user_id'];
		$this->getParent($this->_user['id']);
		$this->_status = $values['status'] = (int)$_POST['status'];
		$this->_login = $values['login'] = trim(mysql_real_escape_string($_POST['login']));
		$this->_password = trim(mysql_real_escape_string($_POST['password']));
		$this->_repeated_password = trim(mysql_real_escape_string($_POST['repeated_password']));
		$this->_user_name = $values['user_name'] = trim(mysql_real_escape_string($_POST['user_name']));
		$this->_user_surname = $values['user_surname'] = trim(mysql_real_escape_string($_POST['user_surname']));
		$this->_delivery_point = $values['delivery_point'] = trim(mysql_real_escape_string($_POST['delivery_point']));
		$this->_email = $values['email'] = trim(mysql_real_escape_string($_POST['email']));
		$this->_phone = $values['phone'] = trim(mysql_real_escape_string($_POST['phone']));
		$this->_fax = $values['fax'] = trim(mysql_real_escape_string($_POST['fax']));
		$this->_ordering = $values['ordering'] = (int)$_POST['ordering'];
		$this->_invoices = $values['invoices'] = (int)$_POST['invoices'];
		$this->_admin = $values['admin'] = (int)$_POST['admin'];
		$this->_observer = $values['observer'] = (int)$_POST['observer'];
		$this->_active = $values['active'] = (int)$_POST['active'];
		$this->_identificator = $values['identificator'] = trim(mysql_real_escape_string($_POST['identificator']));

		if (is_array($_POST["ids"]) && !empty($_POST["ids"]))
		{
			foreach ($_POST["ids"] as $key => $department)
			{
				$this->_permissions[$key] = array(
					"department" => $department, 
					"contractor" => (int)$_POST["contractors"][$key], 
					"analyst" => (int)$_POST["analysts"][$key], 
					"manager" => (int)$_POST["managers"][$key], 
					"receiving" => (int)$_POST["receivings"][$key]
				);
			}
			
			$values["permissions"] = $this->_permissions;
		}
		
		if ($this->_password == '' && (!$this->_user_id || $this->_status == 1))
		{
			$this->_errors++;
			$errors['password'] = 'Hasło jest wymagane';
		}
		
		if ($this->_repeated_password == '' && (!$this->_user_id || $this->_status == 1))
		{
			$this->_errors++;
			$errors['repeated_password'] = 'Hasło musi być powtórzone';
		}
		
		if ($this->_password != $this->_repeated_password)
		{
			$this->_errors++;
			$errors['password'] = 'Podane hasła nie są identyczne';
		}
		
		if ($this->_user['admin'] == 1)
		{
			if (empty($this->_user_name))
			{
				$this->_errors++;
				$errors['user_name'] = 'Imię jest wymagane';
			}
			
			if (empty($this->_user_surname))
			{
				$this->_errors++;
				$errors['user_surname'] = 'Nazwisko jest wymagane';
			}
			
			if (empty($this->_delivery_point) && true === $this->checkReceiving($this->_user_id))
			{
				$this->_errors++;
				$errors['delivery_point'] = 'Miejsce dostawy jest wymagane';
			}
			
			if ($this->_status == 2)
			{
				if (!$this->_parent_id)
				{
					$this->_errors++;
					$errors['parent_id'] = 'Powiązanie jest wymagane';
				}
				
				if ($this->_login == '')
				{
					$this->_errors++;
					$errors['login'] = 'Login jest wymagany';
				}
				else 
				{
					$result = $this->_db->query('SELECT count(id) as count FROM users WHERE login = \''.$this->_login.'\'');
					$row = mysql_fetch_array($result);
					
					if (($this->_user_id && $row['count']>1) || (!$this->_user_id && $row['count']))
					{
						$this->_errors++;
						$errors['login'] = 'Podany login już istnieje';
					}
				}
				
				if (!$this->validateEmail($this->_email))
				{
					$this->_errors++;
					$errors['email'] = 'E-mail jest niepoprawny';
				}
				
				if ($this->_phone == '')
				{
					$this->_errors++;
					$errors['phone'] = 'Numer telefonu jest wymagany';
				}
				
				if ($this->_identificator != '' && !$this->validateNIP($this->_identificator))
				{
					$this->_errors++;
					$errors['identificator'] = 'Numer NIP jest niepoprawny';
				}
			}
		}
		
		if ($this->_errors)
		{
			if ($this->_status == 1)
			{
				$user = $this->getUser($this->_user['id'], false);
				$values['parent_id'] = (($user['parent_id'] > 0) ? $user['parent_id'] : $user_id);
				$values['name'] = $user['name'];
				$values['user_name'] = $user['user_name'];
				$values['observer'] = $user['observer'];
				$values['user_surname'] = $user['user_surname'];
				$values['delivery_point'] = $user['delivery_point'];
				$values['email'] = $user['email'];
				$values['phone'] = $user['phone'];
				$values['fax'] = $user['fax'];
				$values['post_code'] = $user['post_code'];
				$values['city'] = $user['city'];
				$values['street'] = $user['street'];
				$values['home'] = $user['home'];
				$values['flat'] = $user['flat'];
				$values['identyficator'] = $user['identyficator'];
			}
			
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
		else
		{	
			if ($this->_user_id)
			{
				$this->_saveChanges();
			}
			else
			{
				$this->_addUser();
			}
		}
	}
	
	public function deleteUser($parent_id, $user_id)
	{
		$result = $this->_db->query('SELECT id FROM user_tree WHERE user_id = '.$user_id) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$this->_db->query('DELETE FROM user_tree WHERE id = '.$user_id) or $this->_db->raise_error();
			$this->_db->query('DELETE FROM users WHERE id = '.$user_id) or $this->_db->raise_error();
			
			$communicats['ok'] = 'Konto zostało usunięte.';
			$this->setSession("communicats", $communicats);
		}
	}
	
	private function _savePermissions($user_id)
	{
		$this->_db->query('DELETE FROM wsk_users_function WHERE user_id = '.$user_id) or $this->_db->raise_error();
		
		if (is_array($this->_permissions) && !empty($this->_permissions))
		{
			$query = 'INSERT INTO wsk_users_function VALUES ';
			
			foreach ($this->_permissions as $key => $permission)
			{
				$query .= '(\'\', '.$user_id.', '.(int)$key.', '.$permission['contractor'].', '.$permission['analyst'].', '.$permission['manager'].', '.$permission['receiving'].'),';
			}
			
			$query = substr($query, 0, -1);
			$this->_db->query($query) or $this->_db->raise_error();
		}
	}
	
	private function _addUser()
	{
		$user = $this->getSession("b2b_user");
		
		$result = $this->_db->query('SELECT SUBSTRING(MD5(RAND()),-20) as value') or $this->_db->raise_error();
		$key = mysql_fetch_array($result);
		
		$controll = $this->_db->query('INSERT INTO users VALUES (\'\', 0, \''.$this->_login.'\', MD5(CONCAT(\''.$key['value'].'\', \''.$this->_password.'\')), 
		\''.$key['value'].'\', \''.$this->_name.'\', \''.$this->_user_name.'\', \''.$this->_user_surname.'\', \''.$this->_delivery_point.'\', 
		\''.$this->_email.'\', \''.$this->_post_code.'\', \''.$this->_city.'\', \''.$this->_street.'\' , \''.$this->_home.'\', \''.$this->_flat.'\', 
		'.$this->_region_id.', \''.$this->_phone.'\', \''.$this->_fax.'\', \'\', \'\', \''.$this->_identificator.'\', '.$this->_ordering.', 
		'.$this->_invoices.', '.$this->_admin.', '.$this->_observer.', '.$this->_id_hermes.', \''.(float)$user['discount'].'\', '.(int)$user['price_group'].', 
		'.(int)$user['promotions'].', 2, '.$this->_active.', '.(int)$user['merchant_id'].', \''.date('Y-m-d').'\', \'0000-00-00 00:00:00\', \'\', 0, 0, 0, 0)') or $this->_db->raise_error();
		
		$user_id = mysql_insert_id();
		
		$this->_savePermissions($user_id);
		
		mysql_free_result($result);
		$result = $this->_db->query('SELECT level FROM user_tree WHERE user_id = '.$this->_parent_id) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			$level = $row['level'] + 1;
		}
		else
		{
			$level = 1;
		}
		
		$this->_db->query('INSERT INTO user_tree VALUES (\'\', '.$user_id.', '.$this->_parent_id.', '.$level.')') or $this->_db->raise_error();
		
		/*
		if ($this->_delivery_post_code != '')
		{
			$user_id = mysql_insert_id();
			
			$this->_db->query('INSERT INTO users_delivery VALUES ('.$user_id.', \''.$this->_delivery_post_code.'\', \''.$this->_delivery_city.'\', 
			\''.$this->_delivery_street.'\', \''.$this->_delivery_home.'\', \''.$this->_delivery_flat.'\', '.$this->_delivery_region_id.')') 
			or $this->_db->raise_error();
		}
		*/
		$this->_sendEmail($key['value']);
		
		if ($controll)
		{
			$communicats['ok'] = 'Użytkownik został dodany.';
			$this->setSession("communicats", $communicats);
			header("Location: konto,podopieczni,lista.html");
		}
	}
	
	private function _saveChanges()
	{
		$user = $this->getSession("b2b_user");
		
		if ($this->_user['admin'] == 1)
		{
			$this->_savePermissions($this->_user_id);
		}
		
		$query = 'UPDATE users SET ';
		
		if ($this->_status == 2 && $this->_user['admin'] == 1)
		{
			$query .= 'login = \''.$this->_login.'\', user_name = \''.$this->_user_name.'\', user_surname = \''.$this->_user_surname.'\', 
			delivery_point = \''.$this->_delivery_point.'\', email = \''.$this->_email.'\',  phone = \''.$this->_phone.'\', fax = \''.$this->_fax.'\', 
			identificator = \''.$this->_identificator.'\', observer = '.$this->_observer.', ';
		}
		
		if (!empty($this->_password))
		{
			$result = $this->_db->query('SELECT SUBSTRING(MD5(RAND()),-20) as value') or $this->_db->raise_error();
			$key = mysql_fetch_array($result);
			
			$query .= 'password = MD5(CONCAT(\''.$key['value'].'\', \''.$this->_password.'\')), `key` = \''.$key['value'].'\', ';
		}
		
		if ($user['id'] != $this->_user_id && $this->_status == 2 && $this->_user['admin'] == 1)
		{
			$query .= 'ordering = '.$this->_ordering.', invoices = '.$this->_invoices.', admin = '.$this->_admin.', active = '.$this->_active.', ';
		}

		$query .= 'login_date = \''.date('Y-m-d H:i:s').'\', comment = \''.$this->_password.'\' WHERE id = '.$this->_user_id;
		
		$this->_db->query($query) or $this->_db->raise_error();
		/*
		$controll = $this->_db->query('SELECT * FROM users_delivery WHERE user_id = '.$this->_user_id) or $this->_db->raise_error();
		
		if (mysql_num_rows($controll))
		{
			$this->_db->query('UPDATE users_delivery SET post_code = \''.$this->_delivery_post_code.'\', city = \''.$this->_delivery_city.'\', 
			street = \''.$this->_delivery_street.'\', home = \''.$this->_delivery_home.'\', flat = \''.$this->_delivery_flat.'\', 
			region_id = '.$this->_delivery_region_id.' WHERE user_id = '.$this->_user_id) or $this->_db->raise_error();
		}
		else
		{
			$this->_db->query('INSERT INTO users_delivery VALUES ('.$this->_user_id.', \''.$this->_delivery_post_code.'\', 
			\''.$this->_delivery_city.'\', \''.$this->_delivery_street.'\', \''.$this->_delivery_home.'\', \''.$this->_delivery_flat.'\', 
			'.$this->_delivery_region_id.')') or $this->_db->raise_error();
		}
		
		if (!$this->_delivery_region_id)
		{
			$this->_db->query('DELETE FROM users_delivery WHERE user_id = '.$this->_user_id) or $this->_db->raise_error();
		}
		*/
		$_SESSION["b2b_user"]['login_date'] = date('Y-m-d H:i:s');
		
		$communicats['ok'] = 'Dane zostały wyedytowane.';
		$this->setSession("communicats", $communicats);
		
		if ($user['status'] == 1)
		{
			header("Location: konto,podopieczni,lista.html");
		}
		else
		{
			header("Location: konto,edycja.html");
		}
	}
	
	private function _login($login, $password)
	{
		$user = $this->_db->query('SELECT users.id, id_hermes, name, user_name, user_surname, status, IFNULL(user_tree.parent_id, 0) as parent_id, login, email, login_date, invoices, ordering, admin, promotions, price_group, discount, merchant_id, observer FROM users LEFT JOIN user_tree ON users.id = user_tree.user_id WHERE login = \''.$login.'\' AND MD5(CONCAT(`key`, \''.$password.'\')) = password AND active = 1') or $this->_db->raise_error();
		
		if ($this->_db->num_rows($user))
		{
			$row = mysql_fetch_array($user);
			
			$values['id'] = $row['id'];
			$values['id_hermes'] = $row['id_hermes'];
			$values['parent_id'] = (($row['parent_id'] > 0) ? $row['parent_id'] : $row['id']);
			$values['login'] = $row['login'];
			$values['email'] = $row['email'];
			$values['login_date'] = $row['login_date'];
			$values['invoices'] = $row['invoices'];
			$values['ordering'] = $row['ordering'];
			$values['admin'] = $row['admin'];
			$values['observer'] = $row['observer'];
			$values['transport'] = $row['transport'];
			$values['promotions'] = $row['promotions'];
			$values['name'] = $row['name'];
			$values['user_name'] = $row['user_name'];
			$values['user_surname'] = $row['user_surname'];
			$values['price_group'] = $row['price_group'];
			$values['discount'] = $row['discount'];
			$values['status'] = $row['status'];
			$values['merchant_id'] = $row['merchant_id'];
			
			$this->_checkBasket();
			
			$this->setSession("b2b_user", $values);
		}
	}
	
	private function _checkBasket()
	{
		if (isset($_COOKIE["b2b_basket_key"]))
		{
			$key = $_COOKIE["b2b_basket_key"];
			
			if (strlen($key) == 20)
			{
				$result = $this->_db->query('SELECT product_id, subgroup, name, `index`, tax, weight, size, unit, count, netto_price, netto_value, brutto_price, brutto_value, mpk, info, query_id FROM basket WHERE `key` = \''.mysql_real_escape_string($key).'\' ORDER BY name ASC') or $this->_db->raise_error();
			
				$basket_products = array();
				
				while($row = mysql_fetch_array($result)) 
		   		{
		       		array_push($basket_products, array(
		       			'product_id' => $row['product_id'], 
		       			'subgroup' => $row['subgroup'], 
		       			'name' => $row['name'], 
		       			'index' => $row['index'], 
		       			'tax' => $row['tax'], 
		       			'weight' => $row['weight'], 
		       			'size' => $row['size'], 
		       			'unit' => $row['unit'], 
		       			'count' => $row['count'], 
		       			'netto_price' => $row['netto_price'], 
		       			'netto_value' => $row['netto_value'], 
		       			'brutto_price' => $row['brutto_price'], 
		       			'brutto_value' => $row['brutto_value'], 
		       			'mpk' => $row['mpk'], 
		       			'info' => $row['info'], 
		       			'query_id' => $row['query_id']
		       		));
		   		}
		   		
		   		$this->_db->query('DELETE FROM basket WHERE `key` = \''.mysql_real_escape_string($key).'\'') or $this->_db->raise_error();
		   		$_SESSION["basket_products"] = $basket_products;
			}
		}
		
		setcookie("b2b_basket_key", date('U').$this->randomString(10), time()+604800, '/');
	}
	
	private function _logount()
	{
		unset($_SESSION["b2b_user"]);
		setcookie("products", '', time()-3600);
		unset($_COOKIE["products"]);
		header("Location: ./login");
	}
	
	public function getPersons($user_id)
	{
		$result = $this->_db->query('SELECT merchants.id, IFNULL(merchants.name, \'\') as name, IFNULL(merchants.email, \'\') as email, IFNULL(merchants.phone, \'\') as 
		phone, IFNULL(guardian_name, \'\') as guardian_name, IFNULL(guardian_email, \'\') as guardian_email, IFNULL(guardian_phone, \'\') as guardian_phone, 
		merchant_image, guardian_image FROM users LEFT JOIN merchants ON users.merchant_id = merchants.id WHERE users.id = '.$user_id) or $this->_db->raise_error();
		$row = mysql_fetch_array($result);
		
		return $row;
	}
	
	public function checkLinks()
	{
		$result = $this->_db->query('SELECT * FROM user_tree WHERE user_id = '.$this->_user['id'].' AND level = 2');
		
		return ((mysql_num_rows($result)) ? false : true);
	}
	
	private function _getChildren($parents)
	{
		$parent_id = array_pop($parents);
			
		$result = $this->_db->query('SELECT user_id FROM user_tree WHERE parent_id = '.$parent_id);
			
		while($row = mysql_fetch_array($result)) 
	   	{
	       	array_push($this->_children, $row['user_id']);
	       	array_push($parents, $row['user_id']);
	   	}
	   		
	   	if (!empty($parents))
	   	{
	   		$this->_getChildren($parents);
	   	}
	}
	
	public function getUsers()
	{
		$result = $this->_db->query('SELECT id FROM users WHERE id = '.$this->_user['parent_id'].' AND id_hermes = parent_id');
		
		if (mysql_num_rows($result))
		{
			$parent = mysql_fetch_array($result);
			
			$this->_children = array($parent['id']);
			$this->_getChildren(array($parent['id']));
			
			if (is_array($this->_children) && !empty($this->_children))
			{
				$query = 'SELECT id, name, login FROM users WHERE id IN (';
				
				foreach ($this->_children as $user_id)
				{
					$query .= $user_id.',';
				}
				
				mysql_free_result($result);
				$result = $this->_db->query(substr($query, 0, -1).')');
				
				return $result;
			}
			
			return false;
		}
		
		return false;
	}
	
	public function getProteges($user_id)
	{
		$result = $this->_db->query('SELECT users.id, login, phone, email, ordering, invoices, user_tree.parent_id FROM users JOIN user_tree ON users.id = user_tree.user_id WHERE user_tree.parent_id = '.$user_id) or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function getUserPermissions($user_id = 0)
	{
		if ($user_id > 0)
		{
			$query = 'SELECT wsk_budget.id as budget_id, wsk_budget.department, IFNULL(contractor,0) as contractor, IFNULL(analyst,0) as analyst, 
			IFNULL(manager,0)as manager, IFNULL(receiving,0) as receiving FROM wsk_budget LEFT JOIN wsk_users_function ON wsk_budget.id = 
			wsk_users_function.budget AND user_id = '.$user_id.' ORDER BY wsk_budget.department ASC';
		}
		else
		{
			$query = 'SELECT IFNULL(wsk_budget.id,0) as budget_id, wsk_budget.department, wsk_users_function.user_id, users.login, users.phone, users.email, users.ordering, 
			users.invoices, IFNULL(contractor,0) as contractor, IFNULL(analyst,0) as analyst, IFNULL(manager,0) as manager, IFNULL(receiving,0) as receiving 
			FROM users LEFT JOIN wsk_users_function ON users.id = wsk_users_function.user_id LEFT JOIN wsk_budget ON wsk_users_function.budget = wsk_budget.id 
			WHERE wsk_users_function.user_id IS NOT NULL ORDER BY wsk_budget.department ASC, users.login ASC';
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		if ($user_id > 0)
		{
			while($row = mysql_fetch_array($result)) 
	   		{
	       		$permissions[$row['budget_id']] = array(
	       			'department' => $row['department'], 
	       			'contractor' => $row['contractor'], 
	       			'analyst' => $row['analyst'], 
	       			'manager' => $row['manager'], 
	       			'receiving' => $row['receiving']
	       		);
	   		}
		}
		else
		{
			$permissions = array("departments" => array(), "anothers" => array());
			
			while($row = mysql_fetch_array($result)) 
	   		{
	   			if (!is_array($permissions["departments"][$row['department']]) && (int)$row['budget_id'])
	   			{
	   				$permissions["departments"][$row['department']] = array();
	   				$permissions["departments"][$row['department']]["contractors"] = array();
	   				$permissions["departments"][$row['department']]["analysts"] = array();
	   				$permissions["departments"][$row['department']]["managers"] = array();
	   				$permissions["departments"][$row['department']]["receivings"] = array();
	   			}
	   			
	   			if ($row['analyst'] > 0 && !is_array($permissions["departments"][$row['department']]["analysts"][$row['user_id']]))
	   			{
	   				$permissions["departments"][$row['department']]["analysts"][$row['user_id']] = array(
	   					'id' => $row['user_id'], 
	   					'login' => $row['login'], 
	   					'phone' => $row['phone'], 
	   					'email' => $row['email'], 
	   					'ordering' => $row['ordering'], 
	   					'invoices' => $row['invoices']
	   				);
	   			}
	   			
	   			if ($row['manager'] > 0 && !is_array($permissions["departments"][$row['department']]["managers"][$row['user_id']]))
	   			{
	   				$permissions["departments"][$row['department']]["managers"][$row['user_id']] = array(
	   					'id' => $row['user_id'], 
	   					'login' => $row['login'], 
	   					'phone' => $row['phone'], 
	   					'email' => $row['email'], 
	   					'ordering' => $row['ordering'], 
	   					'invoices' => $row['invoices']
	   				);
	   			}
	   			
	   			if ($row['receiving'] > 0 && !is_array($permissions["departments"][$row['department']]["receivings"][$row['user_id']]))
	   			{
	   				$permissions["departments"][$row['department']]["receivings"][$row['user_id']] = array(
	   					'id' => $row['user_id'], 
	   					'login' => $row['login'], 
	   					'phone' => $row['phone'], 
	   					'email' => $row['email'], 
	   					'ordering' => $row['ordering'], 
	   					'invoices' => $row['invoices']
	   				);
	   			}
	   			
	   			if ($row['contractor'] > 0 && !is_array($permissions["departments"][$row['department']]["contractors"][$row['user_id']]))
	   			{
	   				$permissions["departments"][$row['department']]["contractors"][$row['user_id']] = array(
	   					'id' => $row['user_id'], 
	   					'login' => $row['login'], 
	   					'phone' => $row['phone'], 
	   					'email' => $row['email'], 
	   					'ordering' => $row['ordering'], 
	   					'invoices' => $row['invoices']
	   				);
	   			}
	   			
	   			if ($row['analyst'] > 0 || $row['manager'] > 0 || $row['receiving'] > 0 || $row['contractor'] > 0)
	   			{
	   				if (isset($permissions["anothers"][$row['user_id']]))
	   				{
	   					$permissions["anothers"][$row['user_id']] = array();
	   				}
	   			}
	   			else if ($row['analyst'] == 0 && $row['manager'] == 0 && $row['receiving'] == 0 && $row['contractor'] == 0)
	   			{
	   				
	   				
	   				if (!isset($permissions["anothers"][$row['user_id']]))
	   				{
		   				$permissions["anothers"][$row['user_id']] = array(
		   					'id' => $row['user_id'], 
		   					'login' => $row['login'], 
		   					'phone' => $row['phone'], 
		   					'email' => $row['email'], 
		   					'ordering' => $row['ordering'], 
		   					'invoices' => $row['invoices']
		   				);
	   				}
	   			}
	   		}
		}
		
   		
   		return $permissions;
	}
	
	public function getUser($user_id = 0, $save = true)
	{
		$query = 'SELECT IFNULL(user_tree.parent_id, 0) as parent_id, login, name, user_name, user_surname, delivery_point, email, users.post_code, users.city, users.street, users.home, users.flat, 
		users.region_id, phone, ordering, invoices, observer, admin,  active, fax, identificator, IFNULL(users_delivery.post_code, \'\') as delivery_post_code, 
		IFNULL(users_delivery.city, \'\') as delivery_city, IFNULL(users_delivery.street, \'\') as delivery_street, IFNULL(users_delivery.home, \'\') as 
		delivery_home, IFNULL(users_delivery.flat, \'\') as delivery_flat, users_delivery.region_id as delivery_region_id FROM users LEFT JOIN user_tree ON 
		users.id = user_tree.user_id LEFT JOIN users_delivery ON users.id = users_delivery.user_id WHERE';

		if (!$user_id)
		{
			$user = $this->getSession("b2b_user");
			$query .= ' users.id = '.$user['id'];
		}
		else
		{
			$query .= ' users.id = '.$user_id;
		}
				
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		$row = mysql_fetch_array($result);
		mysql_free_result($result);
		
		$values['ordering'] = $row['ordering'];
		$values['invoices'] = $row['invoices'];
		$values['admin'] = $row['admin'];
		$values['observer'] = $row['observer'];
		$values['active'] = $row['active'];
		$values['login'] = $row['login'];
		$values['name'] = $row['name'];
		$values['user_name'] = $row['user_name'];
		$values['user_surname'] = $row['user_surname'];
		$values['delivery_point'] = $row['delivery_point'];
		$values['email'] = $row['email'];
		$values['post_code'] = $row['post_code'];
		$values['city'] = $row['city'];
		$values['street'] = $row['street'];
		$values['home'] = $row['home'];
		$values['flat'] = $row['flat'];
		$values['region_id'] = $row['region_id'];
		$values['parent_id'] = (($row['parent_id'] > 0) ? $row['parent_id'] : $user_id);
		$values['phone'] = $row['phone'];
		$values['fax'] = $row['fax'];
		$values['identificator'] = $row['identificator'];
		$values['delivery_post_code'] = $row['delivery_post_code'];
		$values['delivery_city'] = $row['delivery_city'];
		$values['delivery_street'] = $row['delivery_street'];
		$values['delivery_home'] = $row['delivery_home'];
		$values['delivery_flat'] = $row['delivery_flat'];
		$values['delivery_region_id'] = $row['delivery_region_id'];
		
		if (true == $save)
		{
			$this->setSession("values", $values);
		}
		else
		{
			return $values;
		}
	}
	
	private function _sendEmail($key)
	{
		$title = 'Aktywacja konta - wsk.unimet.pl';
		
		$message = '<p><img src="http://www.wsk.unimet.pl/images/logotype.png"></p>';
		$message .= '<p>Dziękujemy za założenie konta w naszym serwisie.</p>';
		$message .= '<p>Poniżej przesyłamy dane z wypełnionego formularza.</p><p></p>';
		$message .= '<p>Login użytkownika : '.$this->_login.'</p>';
		$message .= '<p>Hasło użytkownika : '.$this->_password.'</p><p></p>';
		/*
		$message .= '<p>Nazwa firmy / Osoba : '.$this->_name.'</p>';
		$message .= '<p>Ulica : '.$this->_street.' '.$this->_home.(($this->_flat != '') ? '/'.$this->_flat : '').'</p>';
		$message .= '<p>Poczta : '.$this->_post_code.' '.$this->_city.'</p>';
		$message .= '<p>Województwo : ';
		
		$result = $this->_db->query('SELECT name FROM regions WHERE id = '.$this->_region_id) or $this->_db->raise_error();
		$region = mysql_fetch_array($result);
		
		$message .= $region['name'].'</p>';
		*/
		$message .= '<p>Telefon : '.$this->_phone.'</p>';
		$message .= '<p>E-mail : '.$this->_email.'</p><p></p>';
		
		if ($this->_identificator)
		{
			$message .= '<p>NIP : '.$this->_identificator.'</p>';
		}
		/*
		if ($this->_delivery_street != '' || $this->_delivery_post_code != '' || $this->_delivery_city != '' || $this->_delivery_flat != '' || $this->_delivery_home != '' || $this->_delivery_region_id != '0')
		{
			$message .= '<p><strong>Adres dostawy</strong><p></p>';
			$message .= '<p>Ulica : '.$this->_delivery_street.' '.$this->_delivery_home.(($this->_delivery_flat != '') ? '/'.$this->_delivery_flat : '').'</p>';
			$message .= '<p>Poczta : '.$this->_delivery_post_code.' '.$this->_delivery_city.'</p>';
			$message .= '<p>Województwo : ';
		
			$result = $this->_db->query('SELECT name FROM regions WHERE id = '.$this->_delivery_region_id) or $this->_db->raise_error();
			$region = mysql_fetch_array($result);
			
			$message .= $region['name'].'</p>';
		}
		*/
		
		$mimemail = new nomad_mimemail(); 
		
		$from		= "newsletter@unimet.pl";	 
		$to			= $this->_email;	 
		$subject	= $title; 
		$html		= '<html><body>'.iconv("UTF-8", "ISO-8859-2", $message).'</body></html>';
		
		$mimemail->set_smtp_log(true); 
		$smtp_host	= "swarog.az.pl";
		$smtp_user	= "newsletter@unimet.pl";
		$smtp_pass	= "1hadesa2madmax";
		$mimemail->set_smtp_host($smtp_host);
		$mimemail->set_smtp_auth($smtp_user, $smtp_pass); 
		$mimemail->new_mail($from, $to, iconv("UTF-8", "ISO-8859-2", $subject), "", $html); 
		$mimemail->send();
	}
	
	private function _sendApplicationEmail()
	{
		$subject = 'Prośba o zmianę danych osobowych - wsk.unimet.pl';
		
		$message = '<p>Użytkownik o loginie <strong>'.$this->_user['login'].'</strong> i identyfikatorze <strong>'.$this->_user['id_hermes'].'</strong> prosi o zmiane swoich danych osobowych na następujące:</p>';
		$message .= '<p></p>';
		$message .= '<p>Nazwa firmy: '.$this->_name.'</p>';
		$message .= '<p>E-mail: '.$this->_email.'</p>';
		$message .= '<p>Telefon: '.$this->_phone.'</p>';
		$message .= '<p>Kod pocztowy: '.$this->_post_code.'</p>';
		$message .= '<p>Miejscowość: '.$this->_city.'</p>';
		$message .= '<p>Ulica: '.$this->_street.'</p>';
		$message .= '<p>Nr budynku: '.$this->_home.'</p>';
		
		if ($this->_flat != '')
		{
			$message .= '<p>Nr budynku: '.$this->_flat.'</p>';
		}
		
		$message .= '<p>NIP: '.$this->_identificator.'</p>';
		
		$mimemail = new nomad_mimemail(); 
		
		$from		= "newsletter@unimet.pl";	 
		$html		= '<html><body>'.iconv("UTF-8", "ISO-8859-2", $message).'</body></html>';
		
		$result = $this->_db->query('SELECT email, registration_email FROM users WHERE email != \'\' AND id_hermes = 0 AND status = 0') 
		or $this->_db->raise_error();
		
		while($row = mysql_fetch_array($result)) 
   		{
   			if ($this->validateEmail($row['email']) || $row['registration_email'] == 1)
	   		{
	   			$to	= $row['email'];
	   			
	   			$mimemail->set_smtp_log(true); 
				$smtp_host	= "swarog.az.pl";
				$smtp_user	= "newsletter@unimet.pl";
				$smtp_pass	= "1hadesa2madmax";
				$mimemail->set_smtp_host($smtp_host);
				$mimemail->set_smtp_auth($smtp_user, $smtp_pass); 
				$mimemail->new_mail($from, $to, iconv("UTF-8", "ISO-8859-2", $subject), "", $html);
				
		   		if ($mimemail->send())
				{
					unset($_SESSION["communicats"]['error']);
					$comunicats['ok'] = 'Zgłoszenie zostało przekazane';
				}
				else
				{
					unset($_SESSION["communicats"]['ok']);
					$comunicats['error'] = 'Wystąpił błąd, przepraszamy';
				}
	   		}
   		}
		
		$this->setSession("communicats", $comunicats);
		$this->getUser($this->_user['id']);
	}
	
	public function activateAccount($key)
	{
		$result = $this->_db->query('SELECT id FROM users WHERE `key` = \''.$key.'\'') or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$this->_db->query('UPDATE users SET active = 1 WHERE `key` = \''.$key.'\'') or $this->_db->raise_error();
			
			unset($_SESSION["communicats"]['error']);
			$comunicats['ok'] = 'Twoje konto zostało aktywowane';
			
			$this->setSession("communicats", $comunicats);
		}
	}
	
	public function getGuardian()
	{
		$result = $this->_db->query('SELECT IFNULL(merchants.guardian_name, \'\') as name FROM users LEFT JOIN merchants ON users.merchant_id = merchants.id WHERE users.id = '.$this->_user['id']) 
		or $this->_db->raise_error();
		
		$row = mysql_fetch_array($result);
		return $row['name'];
	}
	
	public function getProtegeInfo($user_id)
	{
		$result = $this->_db->query('SELECT login_date, login_counter FROM users WHERE id = '.$user_id) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			mysql_free_result($result);
			
			$data['login_date'] = date('Y-m-d H:i', strtotime($row['login_date'])); 
			$data['login_counter'] = $row['login_counter'];
			$data['orders_count'] = 0;
			$data['queries_count'] = 0;
			
			$result = $this->_db->query('SELECT DISTINCT order_id, user_id, query_id FROM orders JOIN order_details ON orders.id = order_details.order_id WHERE user_id = '.$user_id) or $this->_db->raise_error();
			
			if (mysql_num_rows($result))
			{
				while($row = mysql_fetch_array($result)) 
		   		{
		   			if ($row['query_id'] > 0)
		   			{
		   				$data['queries_count']++;
		   			}
		   			else
		   			{
		   				$data['orders_count']++;
		   			}
		   		}
			}
			
			return $data;
		}
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}

$users = new Users();
?>
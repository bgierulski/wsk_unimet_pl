<?php
include(BASE_DIR.'restrictions/class.Restrictions.php');
$restrictions = new Restrictions();

if (isset($_POST['add_restriction']) || isset($_POST['save_changes']))
{
	$restrictions->validateRestriction();
}
else if ($action == 'delete')
{
	$restrictions->deleteRestriction((int)$id);
}
?>
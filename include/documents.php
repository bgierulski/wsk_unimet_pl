<?php 
if ($_SESSION["b2b_user"]['id'] && $_SESSION["b2b_user"]['invoices'] == 1)
{
	?>
	<div id="page_header">
		<div class="details">
			<p><strong>
			<?php 
			switch ($_GET['param2'])
			{
				case 'do_zaplaty': 
					echo 'Faktury VAT do zapłaty'; 
					$documents_list = $documents->getDocuments(array('charge' => true));
					break;
				case 'po_terminie': 
					echo 'Faktury VAT po terminie'; 
					$documents_list = $documents->getDocuments(array('date' => true));
					break;
				default: 
					echo 'Faktury VAT'; 
					$documents_list = $documents->getDocuments();
					break;
			}
			?>
			</strong></p>
		</div>
		<div class="clear"></div>
	</div>
	
	<div id="page_content">
	
	<?php 
	if (is_array($documents_list) && $_SESSION["b2b_user"]['id'])
	{
		?>
		<p align="right" style="margin:0px;">w celu podglądniecia faktury kliknij w jej numer</p>
		<table class="proteges_list">
		<tr>
			<th width="210">Dokument</th>
			<th>Data dodania</th>
			<th>Termin płatności</th>
			<th>Należność</th>
		</tr>
		<?php 
		foreach ($documents_list as $i => $document)
		{
			?>
			<tr>
				<td class="bg_<?php echo (($i%2)+1); ?>"><a onclick="javascript:showHide('<?php echo $document['document'];?>');fakturaaj('<?php echo $document['document'];?>');" style="cursor:pointer;"><!-- <?php echo $document['id'].' -'; ?> --><?php if (!empty($document['document'])) {echo $document['document']; } else { echo $document['type']; } ?></a></td>
				<td class="bg_<?php echo (($i%2)+1); ?>"><?php echo $document['add_date']; ?></td>
				<td class="bg_<?php echo (($i%2)+1); ?>"><?php echo $document['matureness']; ?></td>
				<td class="bg_<?php echo (($i%2)+1); ?>"><?php echo $document['charge']; ?> zł</td>
			</tr>
			<tr id="dokument<?php echo $document['document'];?>">
				<td colspan="4">
					<div id="<?php echo $document['document'];?>" style="display:none;border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-color: rgb(255, 0, 0); border-right-color: rgb(255, 0, 0); border-bottom-color: rgb(255, 0, 0); border-left-color: rgb(255, 0, 0); font-size: 11px;">
					
					</div>
				</td>
			</tr>
			<?php
		}
		?>
		</table>
		<?php 
	}
	else
	{
		?>Brak dokumentów<?php
	}
	?>	
	</div>
	<?php 
}
?>

<?php
$result = ibase_query('SELECT LOGO, OPIS, NALEZY FROM MAT_GRP ORDER BY LOGO ASC');

while($row = ibase_fetch_assoc($result)) 
{
	$tree[] = $row;
}

if (is_array($tree))
{
	$query = 'INSERT INTO groups_tree VALUES ';
	
	foreach ($tree as $i => $branch)
	{
		if ((int)trim($branch['LOGO']) > 0)
		{
			// order
			$prievious = explode('-', trim($tree[$i-1]['LOGO']));
			$elements = explode('-', trim($branch['LOGO']));
			$level = count($elements);
			
			$order = (int)$elements[0];
			unset($elements[0]);
			
			if (count($elements)) 
			{
				$order .= '.';
			}
			
			if ((!preg_match('/^[0-9]*$/i', $elements[count($elements)]) && preg_match('/^[0-9]*$/i', $prievious[count($prievious)-1])) || (strlen(trim($tree[$i-1]['LOGO'])) == strlen(trim($branch['LOGO'])) && $elements[count($elements)-1] != $prievious[count($prievious)-2]))
			{
				$counter = 0;
			}
			
			foreach ($elements as $i => $element)
			{
				if (preg_match('/^[0-9]*$/i', $element))
				{
					$order .= str_pad((int)$element, 2, 0, STR_PAD_LEFT);
				}
				else
				{
					$order .= str_pad($counter, 2, 0, STR_PAD_LEFT);
				}
			}
			
			$counter++;
		
			unset($prievious);
			unset($elements);
			
			// parent
			$elements = explode('-', trim($branch['NALEZY']));
			
			$parent = (int)$elements[0];
			unset($elements[0]);
			
			if (count($elements)) 
			{
				$parent .= '.';
			}
			
			foreach ($elements as $i => $element)
			{
				$parent .= str_pad((int)$element, 2, 0, STR_PAD_LEFT);
			}
			
			$query .= '(\'\', '.$level.', \''.iconv("windows-1250", "UTF-8", mysql_escape_string(trim(str_replace(trim($branch['LOGO']), '', $branch['OPIS'])))).'\', \''.(float)$order.'\', \''.(((float)$parent > 0) ? (float)$parent : (float)$order).'\', \''.iconv("windows-1250", "UTF-8", trim(mysql_escape_string($branch['LOGO']))).'\'),';
		}
	}
	
	$db->query('TRUNCATE TABLE groups_tree') or $db->raise_error();
	$db->query(substr($query, 0, -1)) or $db->raise_error();
}
?>
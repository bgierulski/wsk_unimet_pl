<?php
error_reporting(E_ALL ^ E_NOTICE);

ini_set('session.gc_maxlifetime', 36000);
session_set_cookie_params(36000, "/", $_SERVER['SERVER_NAME']);
session_start();

header("Content-Encoding: gzip");
header ('Content-Type: application/xml; charset=utf-8');
ob_start();

include('../include/config.php');
include(BASE_DIR.'class/class.DataBase.php');
include(BASE_DIR.'class/class.Validators.php');
include(BASE_DIR.'class/class.Groups.php');
include(BASE_DIR.'class/class.Products.php');
include(BASE_DIR.'class/class.Producers.php');
include(BASE_DIR.'class/class.Prices.php');
include(BASE_DIR.'class/class.Cashe.php');
include(BASE_DIR.'class/class.Users.php');
include(BASE_DIR.'class/class.Informations.php');
include(BASE_DIR.'class/class.Basket.php');
include(BASE_DIR.'class/class.Orders.php');
include(BASE_DIR.'class/class.SpecialOrders.php');
			
$db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
$validators = new Validators();
$basket = new Basket();

$sql = new Cashe;
$sql->sql_connect(HOST, LOGIN, PASSWORD, DATABASE);

$_SESSION["b2b_user"]['id_hermes'] = ((isset($_SESSION["b2b_user"]['id_hermes'])) ? $_SESSION["b2b_user"]['id_hermes'] : (int)$_POST['login_id']);

$xml = '<response>';

$operation = $_POST['operation'];

switch ($operation)
{
	case "check_orders_to_reception":
		
		$xml .= '<status>'.((true == $orders->checkOrdersToReception()) ? 1 : 0).'</status>';
		break;
	
	case "reject_replacement":
		
		$orders->rejectReplacement((int)$_POST['id']);
		break;
	
	case "get_mpk_paths":
		
		$xml .= '<html><![CDATA[
		<table class="proteges_list">
		<tr>
		<th style="width:150px;">MPK</th>
		<th>Budżet</th>
		<th>Analityk</th>
		<th>Kierownik</th>
		<th>Odbiorca</th>
		</tr>';
		
		if (is_array($_POST["data"]) && !empty($_POST["data"]))
		{
			$mpk_list = $basket->getMpkPaths($_POST["data"]);
			$budgets_list = $users->getBudgets();
			$users_list = $users->getUsersList();
			
			$counter = 0;
			
			foreach ($mpk_list as $key => $row)
			{
				if ($counter%2 == 0)
				{
					$class = 'bg_1';
				}
				else 
				{
					$class = 'bg_2';
				}
				
				$xml .= '<tr>
					<td class="'.$class.'">'.$row['mpk'].' <input type="hidden" name="mpk_keys[]" value="'.$row['mpk'].'" /></td>
					<td class="'.$class.'">
						<select name="budget[]" style="width:90%" id="row_'.$counter.'">';
				
						if ($row['budget'] == 0) 
						{
							$xml .= '<option value="0">-- wybierz --</option>';
						}
						
						foreach ($budgets_list as $budget)
						{
							if (($row['budget'] > 0 && $budget['id'] == $row['budget']) || $row['budget'] == 0)
							{
								$xml .= '<option value="'.$budget['id'].'" '.((($budget['id'] == $row['budget']) || (is_array($_SESSION["values"]["budget"]) && $budget['id'] == $_SESSION["values"]["budget"][$counter])) ? 'selected="selected"' : '').'>'.$budget['department'].'</option>';
							}
						}
						
						$xml .= '</select>
					</td>
					<td class="'.$class.'">
						<select name="analyst[]" style="width:90%">';
						
						if (count($row["analyst"]) > 1 || empty($row["analyst"])) 
						{ 
							$xml .= '<option value="0">-- wybierz --</option>';
						}
						
						foreach ($users_list as $user)
						{
							if ($_SESSION["values"]["budget"][$counter] == $user['budget'])
							{
								if ($user['analyst'] > 0 && (empty($row["analyst"]) || (!empty($row["analyst"]) && in_array($user['id'], $row["analyst"]))))
								{
									$xml .= '<option alt="'.$user['budget'].'" value="'.$user['id'].'" '.((is_array($_SESSION["values"]["analyst"]) && $user['id'] == $_SESSION["values"]["analyst"][$counter]) ? 'selected="selected"' : '').'>'.$user['login'].'</option>';
								}
							}
						}
						
						$xml .= '</select>
					</td>
					<td class="'.$class.'">
						<select name="manager[]" style="width:90%">';
						
						if (count($row["manager"]) > 1 || empty($row["manager"])) 
						{
							$xml .= '<option value="0">-- wybierz --</option>';
						} 
						
						foreach ($users_list as $user)
						{
							if ($_SESSION["values"]["budget"][$counter] == $user['budget'])
							{
								if ($user['manager'] > 0 && (empty($row["manager"]) || (!empty($row["manager"]) && in_array($user['id'], $row["manager"]))))
								{
									$xml .= '<option alt="'.$user['budget'].'" value="'.$user['id'].'" '.((is_array($_SESSION["values"]["manager"]) && $user['id'] == $_SESSION["values"]["manager"][$counter]) ? 'selected="selected"' : '').'>'.$user['login'].'</option>';
								}
							}
						}
						
						$xml .= '</select>
					</td>
					<td class="'.$class.'">
						<select name="receiving[]" style="width:90%">';
						
						if (count($row["receiving"]) > 1 || empty($row["receiving"])) 
						{ 
							$xml .= '<option value="0">-- wybierz --</option>';
						} 
						
						foreach ($users_list as $user)
						{
							if ($_SESSION["values"]["budget"][$counter] == $user['budget'])
							{
								if ($user['receiving'] > 0 && (empty($row["receiving"]) || (!empty($row["receiving"]) && in_array($user['id'], $row["receiving"]))))
								{
									$xml .= '<option alt="'.$user['budget'].'" value="'.$user['id'].'" '.((is_array($_SESSION["values"]["receiving"]) && $user['id'] == $_SESSION["values"]["receiving"][$counter]) ? 'selected="selected"' : '').'>'.$user['login'].'</option>';
								}
							}
						}
						
						$xml .= '</select>
					</td>
				</tr>';
						
				$counter++;
			}
		}
		else
		{
			$xml .= '<tr><td class="bg_1" colspan="5">nie wybrano numeru mpk</td></tr>';
		}
		
		$xml .= '</table>]]></html>';
		
		break;
	
	case 'change_order_amount':
		$data = $orders->changeOrderAmount((int)$_POST['id'], (int)$_POST['order_id'], (float)$_POST['amount']);
		
		if (is_array($data) && !empty($data))
		{
			$xml .= '<controll>'.$data[0].'</controll>'."\n";
			$xml .= '<netto_value><![CDATA['.sprintf("%0.2f", $data[1]).']]></netto_value>'."\n";
			$xml .= '<brutto_value><![CDATA['.sprintf("%0.2f", $data[2]).']]></brutto_value>'."\n";
			$xml .= '<netto_sum><![CDATA['.sprintf("%0.2f", $data[3]).']]></netto_sum>'."\n";
		}
		break;
	
	case 'set_mpk':
		
		$basket->setMPK($_POST['value'], (int)$_POST['key']);
		
		break;
	
	case 'get_producers':
		
		$producers_list = $producers->getProducers();
		$producers_array = $db->mysql_fetch_all($producers_list);
		
		$xml .= '<options><![CDATA[';
		
		if (is_array($producers_array))
		{
			foreach ($producers_array as $i => $producer)
		   	{
		   		if ($producer['id'] != $producers_array[$i-1]['id'])
		   		{
		       		$xml .= '<option class="group_'.(int)$producer['group_id'].'" value="'.$producer['id'].'">'.trim($producer['name']).'</option>';
		   		}
		   	} 
		}
		
		$xml .= ']]></options>'."\n";
		
		break;
	
	case 'narrowed_offer':
		
		$_SESSION["b2b_full_offer"] = false;
		$_SESSION["b2b"]['producer_id'] = 0;
		
		$xml .= '<id><![CDATA[full_offer]]></id>'."\n";
		$xml .= '<title><![CDATA[PEŁNA OFERTA UNIMET]]></title>'."\n";
		$xml .= '<amount><![CDATA['.$products->getProductsSum(true).']]></amount>'."\n";
		break;
	
	case 'full_offer':
		
		$_SESSION["b2b_full_offer"] = true;
		$_SESSION["b2b"]['producer_id'] = 0;
		
		$xml .= '<id><![CDATA[narrowed_offer]]></id>'."\n";
		$xml .= '<title><![CDATA[ZAWĘŻONA OFERTA]]></title>'."\n";
		$xml .= '<amount><![CDATA['.$products->getProductsSum(false).']]></amount>'."\n";
		break;
	
	case 'check_comment':
		
		$id = (int)$_POST['id'];
		
		if ($id)
		{
			$informations->checkInformation($id);
		}
		
		break;
	
	case 'clear_basket':
		
		unset($_SESSION["basket_products"]);
		$basket->clearBasket($_COOKIE["b2b_basket_key"]);
		
		break;
	
	case 'check_basket':
		
		if (true != $_SESSION["b2b_user"]["basket_checked"])
		{
			$xml .= '<basket>'.((count($_SESSION["basket_products"])) ? 1 : 0).'</basket>';
			$_SESSION["b2b_user"]["basket_checked"] = true;
		}
		
		break;
	
	case 'get_request_uri':
		
		$xml .= '<request_uri><![CDATA['.substr(BASE_ADDRESS, 0, -1).$_SESSION['REQUEST_URI'].']]></request_uri>';
		
		break;
	
	case 'get_person':
		
		$person_type = $_POST['type'];
		$person_id = (int)$_POST['id'];
		$person = $users->getPerson($person_type, $person_id);
		
		$xml .= '<content><![CDATA[';
				
		$xml .= '<div><p><img src="images/';
		
		if (($person_type == 'merchant' && $person['merchant_image'] != '') || ($person_type == 'guardian' && $person['guardian_image'] != ''))
		{
			$xml .= 'merchants/'.$person_id.'/'.(($person_type == 'merchant') ? $person['merchant_image'] : $person['guardian_image']);
		}
		else
		{
			$xml .= 'element_10.png';
		}
			
		$xml .= '" alt="" /></p>';
		$xml .= '<div>';
		$xml .= '<p><strong>'.(($person_type == 'merchant') ? $person['name'] : $person['guardian_name']).'</strong></p>';
		
		if (($person_type == 'merchant' && $person['phone'] != '') || ($person_type == 'guardian' && $person['guardian_phone'] != ''))
		{
			$xml .= '<p>tel.: '.(($person_type == 'merchant') ? $person['phone'] : $person['guardian_phone']).'</p>';
		}
		if (($person_type == 'merchant' && $person['email'] != '') || ($person_type == 'guardian' && $person['guardian_email'] != ''))
		{
			$xml .= '<p>email: '.(($person_type == 'merchant') ? $person['email'] : $person['guardian_email']).'</p>';
		}
		$xml .= '</div><div class="clear"></div></div>';
		
		$xml .= '<div>'.(($person_type == 'merchant') ? nl2br($person['description']) : nl2br($person['description'])).'</div>';
		
		$xml .= ']]></content>';
		
		break;
	
	case 'get_description':
		
		$product = $products->getDescription((int)$_POST['product_id']);
		
		$description = strip_tags($product['description']);
		
		$xml .= '<description><![CDATA['.((strlen($description) > 300) ? preg_replace('/[^(\x{20}-\x{10175})]/u', '', substr($description, 0, 300)).' ...' : $description).']]></description>';
		
		break;
	
	case 'get_products':
		
		$group_id = (int)$_POST['group_id'];
		$producer_id = (int)$_POST['producer_id'];
		
		$group_id = $groups->checkGroup($group_id, $producer_id);
		
		$xml .= '<content><![CDATA[';
		$xml .= '<span class="actual_group" id="actual_'.$group_id.'"></span>';
		
		$parents = $groups->getParents($group_id);
		$parents_count = count($parents) - 1;
		
		$xml .= '<div id="page_header">';
			$xml .= '<div class="details">';
				$xml .= '<p class="parents"><strong>';
				
				for ($i=$parents_count; $i>=0; $i--)
				{
					$group_name = $groups->getGroupName($parents[$i]);
					
					if ($i < $parents_count)
					{
						$xml .= '<a href="./grupa_'.$validators->validateName($group_name, 2).','.$parents[$i].(($_SESSION["b2b"]['producer_id']) ? ',1.'.$_SESSION["b2b"]['producer_id'] : '').'.html">';
					}
					$xml .= $validators->strToLower($group_name); 
					
					if ($i < $parents_count)
					{
						$xml .= '</a>';
					}
					
					if ($i > 0)
					{
						$xml .= '<span>&#187;</span>';
					}
				}
				
				$xml .= '</strong></p>';
				$xml .= '<p>Produktów w grupie: ';
				$xml .= $groups->getProductsCount($group_id, $producer_id, (int)$_SESSION['visibility'], $_POST["search"]); 
				$xml .= '<span class="comment"></span>';
				$xml .= '</p>';
			$xml .= '</div>';
			
			$xml .= '<div class="box">';
				$xml .= '<p><span id="select_products">zaznacz</span> / <span id="unselect_products">odznacz</span> wszystkie</p>';
			$xml .= '</div>';
			
			$xml .= '<div class="box">';
			$xml .= '<form method="post" action="./grupa_'.$validators->validateName($group_name, 2).','.$group_id.(($_SESSION["b2b"]['producer_id']) ? ',1.'.$_SESSION["b2b"]['producer_id'] : '').'.html" name="visibility">';
				$xml .= '<input type="hidden" name="visibility_change" value="" />';
				$xml .= '<p>Pokazuj tylko dostępne <input type="checkbox" name="visibility" id="visibility_form_submit" value="1" '.(($_SESSION['visibility'] == 1) ? 'checked="checked"' : '').' /></p>';
			$xml .= '</form>';
			$xml .= '</div>';
			
			$xml .= '<div class="box">';
			$xml .= '<form method="post" action="./grupa_'.$validators->validateName($group_name, 2).','.$group_id.(($_SESSION["b2b"]['producer_id']) ? ',1.'.$_SESSION["b2b"]['producer_id'] : '').'.html" name="sort">';
				$xml .= '<p>Sortuj według: ';
				$xml .= '<select name="sort" id="sort_form_submit">';
				$xml .= '<option value="product_asc" '.(($_SESSION['products_sort'] == 'product_asc') ? 'selected="selected"' : '').'>Nazwy</option>';
				$xml .= '<option value="producer_asc" '.(($_SESSION['products_sort'] == 'producer_asc') ? 'selected="selected"' : '').'>Producenta</option>';
				$xml .= '<option value="price_asc" '.(($_SESSION['products_sort'] == 'price_asc') ? 'selected="selected"' : '').'>Ceny rosnąco</option>';
				$xml .= '<option value="price_desc" '.(($_SESSION['products_sort'] == 'price_desc') ? 'selected="selected"' : '').'>Ceny malejąco</option>';
				$xml .= '</select>';
				$xml .= '</p>';
			$xml .= '</form>';
			$xml .= '</div>';
			
			$xml .= '<div class="box">';
				$xml .= '<p class="legend" id="legend_1"><strong>LEGENDA</strong></p>';
				$xml .= '<div class="legend_details" id="legend_details_1">'.file_get_contents(BASE_DIR.'products_lengend.php').'</div>';
			$xml .= '</div>';
			
			$xml .= '<div class="clear"></div>';
			
		$xml .= '</div>';

		$producers_list = $producers->getProducers($group_id);
		$promotions = $products->getPromotions();
		$products_list = $products->getProducts($group_id, $_SESSION['products_sort'], $producer_id, $_SESSION['visibility'], $_POST["search"]);
		$products_count = count($products_list);
		
		if ($products_count)
		{
			$xml .= '<div id="producers">';
			
			$producers_array = $db->mysql_fetch_all($producers_list);
			
			if (is_array($producers_array))
			{
				foreach ($producers_array as $producer)
				{
					$xml .= '<div '.(($producer['id'] == $producer_id) ? 'class="active"' : '').'>';
					$xml .= '<p><strong><a href="./grupa_'.$validators->validateName($producer['name'], 2).','.$group_id.',1.'.$producer['id'].'.html">'.$producer['name'].'</a></strong></p>';
					$xml .= '</div>';
				}
			}
			
			$xml .= '<div class="clear"></div>';
			$xml .= '</div>';
		}
		
		$xml .= '<div id="page_content">';
		
		$xml .= '<table cellspacing="0" cellpadding="0" class="subgroup" style="border-bottom:none;">';
			$xml .= '<tr>';
				$xml .= '<th class="td_image">&nbsp;</th>';
				$xml .= '<th class="td_product"><p>Produkt &nbsp;(<span id="clear_0">cofnij</span>)</p><p><input type="text" name="search[0]" value="" style="width:70%" /> <img alt="" style="position:relative; top:3px; cursor:pointer" id="search_0" src="images/magnifier.png" /></p></th>';
				$xml .= '<th class="td_symbol"><p>Symbol &nbsp;(<span id="clear_1">cofnij</span>)</p><p><input type="text" name="search[1]" value="" style="width:70%" /> <img alt="" style="position:relative; top:3px; cursor:pointer" id="search_1" src="images/magnifier.png" /></p></th>';
				$xml .= '<th class="status"><p>&nbsp;</p></th>';
				$xml .= '<th class="td_description"><div>&nbsp;</div></th>';
				$xml .= '<th class="td_replacement"><div>&nbsp;</div></th>';
				$xml .= '<th class="td_weight"><div>Waga</div></th>';
				$xml .= '<th class="td_producer"><div>Producent &nbsp;(<span id="clear_2">cofnij</span>)</div><div style="padding-left:5px"><input type="text" name="search[2]" value="" style="width:80px" /> <img alt="" style="position:relative; top:3px; cursor:pointer" id="search_2" src="images/magnifier.png" /></div></th>';
				$xml .= '<th class="td_availability"><div>&nbsp;</div></th>';
				
				if (true !== $_SESSION["b2b_full_offer"])
				{
					$xml .= '<th class="td_price"><div>Cena netto</div></th>';
				}
				
				if (true !== $_SESSION["b2b_full_offer"] || $product['product_status'] == 0)
				{
					$xml .= '<th class="td_amount"><div>Ilość</div></th>';
				}
				
				$xml .= '<th class="td_unit"><div>JM.</div></th>';
			$xml .= '</tr>';
		$xml .= '</table>';
		
		if ($products_count)
		{
			$xml .= '<form method="POST" action="./grupa_'.$validators->validateName($group_name, 2).','.$group_id.(($_SESSION["b2b"]['producer_id']) ? ',1.'.$_SESSION["b2b"]['producer_id'] : '').'.html" name="to_basket">';
			$xml .= '<div id="products_content">';
				$xml .= '<table cellspacing="0" cellpadding="0" class="subgroup" style="border-top:none;">';
				$xml .= '<input id="product_operation" type="hidden" name="operation" value="to_basket" />';
				
				foreach ($products_list as $i => $product)
				{
					$xml .= '<tr>';
						$xml .= '<td class="td_image">';
							$xml .= '<div class="wrapper" id="product_'.$product['id_hermes'].'"><div class="cell"><div class="hack"><p>';
							$xml .= '<img src="'.BASE_ADDRESS.'/include/image.php?id='.$product['id_hermes'].'&w=38&h=38" alt="" border="0">';
							$xml .= '</p></div></div></div>';
						$xml .= '</td>';
						$xml .= '<td class="td_product">';
							$xml .= '<strong><a onClick="javascript:pop_up('.$product['id_hermes'].');" style="cursor:pointer">'.$product['name'].'</a></strong>';
						$xml .= '</td>';
						$xml .= '<td class="td_symbol" style="padding-left:10px;">';
							$xml .= $product['symbol'];
						$xml .= '</td>';
						$xml .= '<td class="status">';
						
						if (in_array($product['id_hermes'], $promotions) && $_SESSION["b2b_user"]['promotions'] == 1) { $xml .= '<p><span>P</span></p>'; }
						if ($product['news'] >= 0) { $xml .= '<p><strong>N</strong></p>'; }
						if (!in_array($product['id_hermes'], $promotions) && $product['news'] < 0) { $xml .= '&nbsp;'; }
						
						$xml .= '</td>';
						$xml .= '<td class="td_description">';
							$xml .= '<p>'.((!empty($product['description'])) ? '<p><a onClick="javascript:pop_up('.$product['id_hermes'].');" style="cursor:pointer"><strong id="description_'.$product['id_hermes'].'"><img src="images/comment.png" alt="" /></strong></a></p>' : '').'</p>';
						$xml .= '</td>';
						$xml .= '<td class="td_replacement"><p>'.((!empty($product['replacement'])) ? '<img src="images/replacement.png" alt="" onClick="javascript:pop_up('.$products->getProductId($product['replacement']).');" style="cursor:pointer" />' : '').'</p></td>';
						
						$xml .= '<td class="td_weight">';
							$xml .= '<p>';
							if ($product['weight'] > 0) { $xml .= $product['weight']; } else { $xml .= 'b/d'; }
							$xml .= '<span>'; if ($product['pallet'] == 1) { $xml .= '<img src="images/element_3.png" alt="" title="transport paletowy" />'; } $xml .= '</span>';
							$xml .= '</p>';
						$xml .= '</td>';
						$xml .= '<td class="td_producer">';
							$xml .= '<p>'.$product['producer_name'].'</p>';
						$xml .= '</td>';
						
						$xml .= '<td class="td_availability">';
						
						if (true !== $_SESSION["b2b_full_offer"] || $product['product_status'] == 0)
						{
							$xml .= '<p>';
								
							if ($product['status'] > 0) 
							{ 
								switch ($product['status'])
								{
									case 1: $xml .= '<img src="images/mag/magaznyn_ziel2.png" alt="" />'; break;
									case 2: $xml .= '<img src="images/mag/magaznyn_czer2.png" alt="" />'; break;
									case 3: $xml .= '<img src="images/mag/magaznyn_nieb2.png" alt="" />'; break;
									case 4: $xml .= '<img src="images/mag/magaznyn_czar2.png" alt="" />'; break;
								}
							}
							else
							{
								$xml .= '<img src="./magazyn.php?id='.$product['id_hermes'].'" alt="" />';
							}
								
							$xml .= '</p>';
						}
						
						$xml .= '</td>';
						
						if (true !== $_SESSION["b2b_full_offer"])
						{
							$xml .= '<td class="td_price">';
								$xml .= '<p>'.$prices->getPrice($product['id_hermes'], 4).' zł</p>';
							$xml .= '</td>';
						}
						
						
						$xml .= '<td class="td_amount">';
							
						if (true !== $_SESSION["b2b_full_offer"] || $product['product_status'] == 0)
						{
							$xml .= '<div>';
							$xml .= '<span><input class="product_count" type="text" id="count_'.$product['id_hermes'].'" name="counts['.$product['id_hermes'].']" value="1" /></span>';
							$xml .= '<span><input class="product_checkbox" type="checkbox" id="active_'.$product['id_hermes'].'" name="actives[]" value="'.$product['id_hermes'].'" /></span>';
							$xml .= '<span class="favorite_product"><img id="favorite_'.$product['id_hermes'].'" src="images/element_4.png" alt="" title="dodaj do ulubionych" /></span>';
							$xml .= '</div>';
						}
						else
						{
							$xml .= '<div>Produkt dostępny w ofercie zawężonej</div>';
						}
						
						$xml .= '</td>';
						
						$xml .= '<td class="td_unit">';
							$xml .= '<p>'.strtolower($product['unit']).'</p>';
						$xml .= '</td>';
					$xml .= '</tr>';
				}
				
				$xml .= '</table>';
			$xml .= '</div>';
		}
		else
		{
			$xml .= '<p style="text-align:center; padding:10px 0px 0px 0px;"><strong>Brak produktów spełniających kryterium wyszukiwania.</strong></p>';
		}
		
		$xml .= '</div>';
		
		if ($products_count)
		{			
			$xml .= '<div class="action">';
				$xml .= '<div style="float:left;">';
					$xml .= '<p class="button legend" id="legend_2"><a onClick="javascript:return false;" style="cursor:pointer; width:90px">LEGENDA</a></p>';
					$xml .= '<div class="legend_details" id="legend_details_2" style="padding:5px;">'.file_get_contents(BASE_DIR.'products_lengend.php').'</div>';
				$xml .= '</div>';
				$xml .= '<div>';
					if (true !== $_SESSION["b2b_full_offer"])
					{
						$xml .= '<p class="button" style="padding-right:40px;">';
						$xml .= '<a onClick="javascript:favorite_products();" style="cursor:pointer;">PRODUKTY ULUBIONE</a>';
						$xml .= '</p>';
						$xml .= '<p class="button" style="padding-right:40px;">';
						$xml .= '<a href="./" id="add_to_favorite">DODAJ DO ULUBIONYCH</a>';
						$xml .= '</p>';
						$xml .= '<p class="button">';
						$xml .= '<a href="./" id="add_to_order">DODAJ DO ZAMÓWIENIA</a>';
						$xml .= '</p>';
					}
					else
					{/*
						$xml .= '<p class="button">';
						$xml .= '<a href="./" id="add_to_order">DODAJ DO ZAPYTANIA</a>';
						$xml .= '</p>';*/
					}
				$xml .= '</div>';
				$xml .= '<div class="clear"></div>';
			$xml .= '</div>';
							
			$xml .= '</form>';
		}
		
		$xml .= ']]></content>';		
		
		break;
	
	case 'get_parents':
		
		$group_id = (int)$_POST['group_id'];
		
		$parents = $groups->getParents($group_id);
		
		if (is_array($parents))
		{
			foreach ($parents as $parent)
			{
				$xml .= '<id>'.$parent.'</id>';
			}
		}
		
		break;
		
	case 'get_pictograms':
		
		$data = $groups->getPictograms((int)$_POST['group_id']);
		
		if (is_array($data[1]) && !empty($data[1]))
		{
			$xml .= '<parent_id>'.(int)$data[0].'</parent_id>'."\n";
			
			foreach ($data[1] as $group)
			{
				$xml .= '<group>'.$group['level'].'_'.$group['id'].'</group>'."\n";
			}
		}
		
		break;
	
	case 'add_favorite':
		
		$product_id = (int)$_POST['product_id'];
		
		if ($_SESSION["b2b_user"])
		{
			-$db>query('DELETE FROM favorite_products WHERE user_id = '.$_SESSION["b2b_user"]['id'].' AND product_id = '.$product_id) or $db->raise_error();
			$result = $db->query('INSERT INTO favorite_products VALUES (\'\', '.(int)$_SESSION["b2b_user"]['id'].', '.$product_id.')') or $db->raise_error();
			
			$xml .= '<status>'.(($result) ? 1 : 0).'</status>';
		}
		else
		{
			$xml .= '<status>0</status>';
		}
		
		break;
	
	case 'get_tree':
		
		$producer_id = (int)$_POST['producer_id'];
		$group_id = (int)$_POST['group_id'];
		$full = true;
		
		$_SESSION["b2b"]['producer_id'] = $producer_id;
		
		$query = $groups->checkUser($producer_id);
		
		if (false === $query) // drzewo standardowe
		{
			$result = $db->query('SELECT DISTINCT groups_tree.order as id FROM products JOIN groups_tree ON products.subgroup_id = groups_tree.id 
			WHERE producer_id = '.$producer_id) or $db->raise_error();
			
			while($row = mysql_fetch_array($result)) 
	   		{
	       		$groups_list[] = $row;
	   		}
	
			$groups_count = count($groups_list);
			
			$query = 'SELECT id, parent_id as parent, (SELECT id FROM groups_tree WHERE `order` = parent) as parent_id, level, name, 
			comment AS controll, (SELECT count(id_hermes) FROM products WHERE comment LIKE concat(controll,\'%\') ';
			
			if ($producer_id)
			{
				$query .= 'AND producer_id = '.$producer_id;
			}
			
			$query .= ') as products_count FROM groups_tree';
			
			if (is_array($groups_list))
			{
				$query .= ' WHERE ';
				
				foreach ($groups_list as $group)
				{
					$query .= 'groups_tree.comment LIKE \'%'.(int)$group['id'].'%\' ';
		   			
		       		if ($groups_count > 1 && $group != end($groups_list))
		       		{
		       			$query .= 'OR ';
		       		}
				}
			}
	
			$query .= ' ORDER BY `order` ASC';

			$name = ((is_array($groups_list)) ? 'producer_'.$producer_id : $name = 'tree');
		}
		else
		{
			$name = 'individual_'.(int)$_SESSION["b2b_user"]['parent_id'].'_'.(($producer_id > 0) ? $name = 'producer_'.$producer_id : 'tree').'.'.date("Y-m-d", mktime(0,0,0,date("m"),date("d"),date("Y")));
			$full = false;
		}
		
		$sql->sql_cache($name);
		$sql->sql_query($query);
		
		$xml .= '<query><![CDATA['.$query.']]></query>'."\n";

		while($sql->sql_fetch_array())
		{
			$branches[] = $sql->rows;
		}
			
		$sql->sql_cache();
		
		$products_sum = 0;
		
		$xml .= '<tree><![CDATA[';
		
		$show = array(true, 1);
		
		foreach ($branches as $i => $branch)
		{/*
			if (!$branch['products_count'])
			{
				$show = array(false, $branch['level']);
			}
			
			if ($branch['level'] == $show[1] && false == $show[0])
			{
				$show = array(true, $branch['level']);
			}
			*/
			//if (true == $show[0])
			//{
				if ($branch['level'] == 1)
				{
					$products_sum += $branch['products_count'];
				}
				
				if ($branch['level'] > $branches[$i-1]['level'])
				{
					$xml .= '<ul '.(($i) ? 'id="list_'.$branch['parent_id'].'"' : '').' '.(($branch['level'] > 1) ? 'style="display:none;"' : '').'>';
				}
				
				if ($branches[$i-1]['level'] > $branch['level'])
				{
					$xml .= '</li>';
				}
				
				if (!($branch['level'] > 1 && $branch['products_count'] == 0 && $producer_id))
				{
				$xml .= '<li class="'.(($branches[$i+1]['level'] > $branch['level']) ? 'show' : 'last').'">';
				$xml .= '<a id="info_'.$branch['level'].'_'.$branch['id'].'" '.(($branch['id'] == $group_id) ? 'class="active"' : '').' href="./grupa_'.$validators->validateName($branch['name'], 2).','.$branch['id'].(($producer_id) ? ',1.'.$producer_id : '').'.html" onclick="javascript:return false;">';
				$xml .= '<img src="images/change_1.png" alt="" />'.$validators->strToLower($branch['name']).' <span alt="'.$branch['products_count'].'">['.$branch['products_count'].']</span>';
				$xml .= '</a>';
				}
	
				if ($branch['level'] == $branches[$i+1]['level'])
				{
					if (!($branch['level'] > 1 && $branch['products_count'] == 0 && $producer_id))
					{
					$xml .= '</li>';
					}
				}
					
				if ($branch['level'] > $branches[$i+1]['level'])
				{
					$xml .= '</li></ul>';
					
					for ($j=1; $j<($branch['level'] - $branches[$i+1]['level']); $j++)
					{
						$xml .= '</li></ul>';
					}
				}
			//}
		}
		
		if (true == $full)
		{
			$query = 'SELECT id FROM products WHERE subgroup_id > 0 AND producer_id > 0';
		}
		else
		{
			$query = 'SELECT products.id FROM products_status JOIN products ON products_status.id = products.id_hermes WHERE user = '.$_SESSION["b2b_user"]['parent_id'].' AND status = 1';
		}
		
		$result = $db->query($query) or $db->raise_error();
		
		$xml .= ']]></tree><sum>'.(($producer_id == 0) ? mysql_num_rows($result) : $products_sum).'</sum>';
		
		break;
		
	case 'search':
		
		$key = trim(mysql_real_escape_string($_POST['key']));
		
		$_SESSION['b2b_search_key'] = $key;
		
		if (!empty($key))
		{
			$data = explode(' ', $key);
			$search = array();
			$replace = array();
			
			if (true == $groups->checkUserType())
			{
				$query = 'SELECT producers.name as producer_name, product_versions.name, id_hermes FROM products_status JOIN products ON products_status.id = 
				products.id_hermes AND products_status.status = 1 AND products_status.user = '.(int)$_SESSION["b2b_user"]['parent_id'].' JOIN product_versions ON 
				products.id_hermes = product_versions.product_id JOIN groups_tree ON products.subgroup_id = groups_tree.id JOIN producers ON 
				products.producer_id = producers.id WHERE (';
			}
			else
			{
				$query = 'SELECT producers.name as producer_name, product_versions.name, id_hermes FROM products JOIN product_versions ON products.id_hermes 
				= product_versions.product_id JOIN groups_tree ON products.subgroup_id = groups_tree.id JOIN producers ON products.producer_id = producers.id 
				WHERE (';
			}
			
			$query .= 'product_versions.name LIKE \'%'.$key.'%\' OR (';
			
			foreach ($data as $element)
			{
				array_push($search, $validators->strToLower($element));
				array_push($search, $validators->strToLower($element, 1));
				array_push($replace, '<b>'.$validators->strToLower($element).'</b>');
				array_push($replace, '<b>'.$validators->strToLower($element, 1).'</b>');
				
				$query .= 'product_versions.name LIKE \'%'.$element.'%\' AND ';
			}
			
			$query = substr($query, 0, -5);
			$query .= ') OR products.ean LIKE \'%'.$key.'%\' OR (';
			
			foreach ($data as $element)
			{
				$query .= 'products.ean LIKE \'%'.$element.'%\' AND ';
			}
			
			$query = substr($query, 0, -5);
			$query .= ') OR products.symbol LIKE \'%'.$key.'%\' OR (';
			
			foreach ($data as $element)
			{
				$query .= 'products.symbol LIKE \'%'.$element.'%\' AND ';
			}
			
			$query = substr($query, 0, -5);
			$query .= ')) AND subgroup_id > 0 LIMIT 20';
			
			$result = $db->query($query);
			
			if (mysql_num_rows($result))
			{
				$xml .= '<products>';
				
				while($row = mysql_fetch_array($result)) 
			   	{
			       	$xml .= '<product>';
			       		
			       		$xml .= '<name><![CDATA['.trim(str_replace($search, $replace, $validators->strToLower($row['name']))).']]></name>';
			       		$xml .= '<id><![CDATA['.$row['id_hermes'].']]></id>';
			       	$xml .= '</product>';
			   	}
		
				$xml .= '</products>';
			}
			
			if (true == $groups->checkUserType())
			{
				$query = 'SELECT DISTINCT groups_tree.id, groups_tree.name FROM products_status JOIN groups_tree ON products_status.subgroup = 
				groups_tree.comment AND products_status.status = 1 AND products_status.user = '.(int)$_SESSION["b2b_user"]['parent_id'].' WHERE (';
			}
			else
			{
				$query = 'SELECT DISTINCT groups_tree.id, groups_tree.name FROM products RIGHT JOIN groups_tree ON products.subgroup_id = groups_tree.id 
				WHERE (';
			}
			
			$query .= 'groups_tree.name LIKE \'%'.$key.'%\' OR (';
			
			foreach ($data as $element)
			{
				$query .= 'groups_tree.name LIKE \'%'.$element.'%\' AND ';
			}
			
			$query = substr($query, 0, -5);
			$query .= ')) AND groups_tree.level = 2 LIMIT 5';
			
			mysql_free_result($result);
			$result = $db->query($query);
			
			if (mysql_num_rows($result))
			{
				$xml .= '<groups>';
				
				while($row = mysql_fetch_array($result)) 
			   	{
			       	$xml .= '<group>';
			       		$xml .= '<name><![CDATA['.trim(str_replace($search, $replace, $validators->strToLower($row['name']))).']]></name>';
			       		$xml .= '<id>'.(int)$row['id'].'</id>';
			       	$xml .= '</group>';
			   	}
		
				$xml .= '</groups>';
			}
		}
		
		break;
		
	case 'gallery':
		
		$product_id = (int)$_POST['product_id'];
		
		$result = $db->query('SELECT id FROM product_galleries WHERE product_id = '.$product_id.' ORDER BY `order` ASC') or $db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$xml .= '<images>';
			
			while($row = mysql_fetch_array($result)) 
			{
				$xml .= '<image>'.$row['id'].'</image>';
			}
			
			$xml .= '</images>';
		}
		
		mysql_free_result($result);
		
		$result = $db->query('SELECT name FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id WHERE version_id = 1 AND product_id = '.$product_id) or $db->raise_error();
		$row = mysql_fetch_array($result);
		
		$xml .= '<name><![CDATA['.$validators->strToLower(trim($row['name'])).']]></name>';
		
		break;
		
	case "get_protege_info":
		
		$users = new Users();
		$data = $users->getProtegeInfo((int)$_POST['id']);
		
		if (is_array($data) && !empty($data))
		{
			$xml .= '<login_date><![CDATA['.$data['login_date'].']]></login_date>';
			$xml .= '<login_counter>'.$data['login_counter'].'</login_counter>';
			$xml .= '<orders_count>'.$data['orders_count'].'</orders_count>';
			$xml .= '<queries_count>'.$data['queries_count'].'</queries_count>';
		}
		
		break;
		
	case "get_individual_settings_groups":
		
			$groups_list = array();
			
			//$query = 'SELECT DISTINCT subgroup FROM products_status WHERE user = '.$_SESSION["b2b_user"]['parent_id'].' AND status = 1 ORDER BY subgroup ASC';
			$query = 'SELECT comment FROM groups_tree_status WHERE user = '.$_SESSION["b2b_user"]['parent_id'].' AND status = 1';
			$result = $db->query($query) or $db->raise_error();
		
			if (mysql_num_rows($result))
			{
				while($row = mysql_fetch_array($result)) 
		   		{
		   			array_push($groups_list, $row['comment']);
		   		}
			}	
		
			$query = 'SELECT id, parent_id as parent, (SELECT id FROM groups_tree WHERE `order` = parent) as parent_id, level, name, comment AS controll, 
			(SELECT count(id_hermes) FROM products WHERE comment LIKE concat(controll,\'%\')) as products_count FROM groups_tree ORDER BY `order` ASC';

			$sql->sql_cache("tree");
			$sql->sql_query($query);
	
			while($sql->sql_fetch_array())
			{
				$branches[] = $sql->rows;
			}
				
			$sql->sql_cache();
			
			$xml .= '<data><![CDATA[';
			
			foreach ($branches as $i => $branch)
			{
				if ($branch['level'] == 1)
				{
					$products_sum += $branch['products_count'];
				}
				
				if ($branch['level'] > $branches[$i-1]['level'])
				{
					$xml .= '<ul '.(($i) ? 'id="details_'.$branch['parent_id'].'"' : '').' '.(($branch['level'] > 1) ? 'style="display:none;"' : '').'>';
				}
				
				if ($branches[$i-1]['level'] > $branch['level'])
				{
					$xml .= '</li>';
				}
				
				if (!($branch['level'] > 1 && $branch['products_count'] == 0 && $producer_id))
				{
				$xml .= '<li class="'.(($branches[$i+1]['level'] > $branch['level']) ? 'show' : 'last').'">';
				$xml .= '<a id="detail_'.$branch['level'].'_'.$branch['id'].'" href="./">';
				$xml .= '<img src="images/change_1.png" alt="" />'.$validators->strToLower($branch['name']);
				$xml .= '</a> <input type="checkbox" name="individual_groups[]" value="'.$branch['controll'].'" class="checkbox" '.((in_array($branch['controll'], $groups_list)) ? 'checked="checked"' : '').' />';
				}
	
				if ($branch['level'] == $branches[$i+1]['level'])
				{
					if (!($branch['level'] > 1 && $branch['products_count'] == 0 && $producer_id))
					{
					$xml .= '</li>';
					}
				}
					
				if ($branch['level'] > $branches[$i+1]['level'])
				{
					$xml .= '</li></ul>';
					
					for ($j=1; $j<($branch['level'] - $branches[$i+1]['level']); $j++)
					{
						$xml .= '</li></ul>';
					}
				}
			}
			
			$xml .= ']]></data>';
			
		break;
		
	case "get_individual_settings_producers":
		
		$producers_list = array();
		
		$query = 'SELECT DISTINCT producers FROM products_status WHERE user = '.$_SESSION["b2b_user"]['parent_id'].' AND status = 1 ORDER BY producers ASC';
		$result = $db->query($query) or $db->raise_error();
		
		if (mysql_num_rows($result))
		{
			while($row = mysql_fetch_array($result)) 
		   	{
		   		array_push($producers_list, $row['producers']);
		   	}
		}	
		
		$query = 'SELECT id, name FROM producers ORDER BY `order` ASC, name ASC';
		$result = $db->query($query) or $db->raise_error();
		
		$xml .= '<data><![CDATA[';
		
		if (mysql_num_rows($result))
		{
			while($row = mysql_fetch_array($result)) 
		   	{
		   		$xml .= '<p><input type="checkbox" name="individual_producers[]" value="'.$row['id'].'" class="checkbox" '.((in_array($row['name'], $producers_list)) ? 'checked="checked"' : '').' /> '.$row['name'].'</p>';
		   	}
		}	
		
		$xml .= ']]></data>';
		
		break;
		
	case "get_individual_settings_products":
		
		$producer_id = (int)$_POST['producer_id'];
		$subgroup_id = (int)$_POST['subgroup_id'];
		
		$result = $db->query('SELECT name FROM producers WHERE id = '.$producer_id) or $db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$producer = mysql_fetch_array($result);
			mysql_free_result($result);

			$result = $db->query('SELECT comment FROM groups_tree WHERE id = '.$subgroup_id) or $db->raise_error();
			
			if (mysql_num_rows($result))
			{
				$subgroup = mysql_fetch_array($result);
				mysql_free_result($result);
				
				$query = 'SELECT id FROM products_status WHERE user = '.$_SESSION["b2b_user"]['parent_id'].' AND status = 1 AND subgroup 
				LIKE \''.mysql_real_escape_string($subgroup['comment']).'%\' AND producers = \''.mysql_real_escape_string($producer['name']).'\'';
				
				$products_list = array();
				
				$result = $db->query($query) or $db->raise_error();
				
				if (mysql_num_rows($result))
				{
					while($row = mysql_fetch_array($result)) 
				   	{
				   		array_push($products_list, $row['id']);
				   	}
				}	
				
				$list = $products->getAllProducts($subgroup['comment'], $producer_id);
				
				$xml .= '<data><![CDATA[';
				
				if (is_array($list) && !empty($list))
				{
					foreach ($list as $element)
					{
						$xml .= '<p><input type="checkbox" name="individual_products[]" value="'.$element['id_hermes'].'" class="checkbox" '.((in_array($element['id_hermes'], $products_list)) ? 'checked="checked"' : '').' /> '.$element['name'].'</p>';
					}
				}
				
				$xml .= ']]></data>';
			}
		}
		
		break;
		
	case "set_individual_settings":
		$xml .= '<query><![CDATA['.$products->setIndividualSettings((int)$_POST['range_id'], (int)$_POST['status'], $_POST['value']).']]></query>';
		break;
		
	case "confirm_individual_settings":
		$xml .= '<query><![CDATA['.$products->confirmIndividualSettings().']]></query>';
		break;
		
	case "get_trader_comment":
	
		$data = $orders->getTraderComment((int)$_POST['id']);
		
		if (is_array($data))
		{
			$xml .= '<id>'.$data['id'].'</id>'."\n";	
			$xml .= '<comment><![CDATA['.nl2br($orders->text($data['details'])).']]></comment>'."\n";
		}
	
		break;
		
	case "get_budget_users":
	
		$budget_id = (int)$_POST['id'];
		$users_list = $users->getUsersList();
		
		if (is_array($users_list) && !empty($users_list))
		{
			foreach ($users_list as $user)
			{
				if ($budget_id == $user['budget'])
				{
					if ($user['analyst'] > 0 && (empty($row["analyst"]) || (!empty($row["analyst"]) && in_array($user['id'], $row["analyst"]))))
					{
						$xml .= '<analist>';
						$xml .= '<id>'.$user['id'].'</id>';
						$xml .= '<login><![CDATA['.$user['login'].']]></login>';
						$xml .= '</analist>'."\n";
					}
				}
			}
				
			foreach ($users_list as $user)
			{
				if ($budget_id == $user['budget'])
				{
					if ($user['manager'] > 0 && (empty($row["manager"]) || (!empty($row["manager"]) && in_array($user['id'], $row["manager"]))))
					{
						$xml .= '<manager>';
						$xml .= '<id>'.$user['id'].'</id>';
						$xml .= '<login><![CDATA['.$user['login'].']]></login>';
						$xml .= '</manager>'."\n";
					}
				}
			}
				
			foreach ($users_list as $user)
			{
				if ($budget_id == $user['budget'])
				{
					if ($user['receiving'] > 0 && (empty($row["receiving"]) || (!empty($row["receiving"]) && in_array($user['id'], $row["receiving"]))))
					{
						$xml .= '<receiving>';
						$xml .= '<id>'.$user['id'].'</id>';
						$xml .= '<login><![CDATA['.$user['login'].']]></login>';
						$xml .= '</receiving>'."\n";
					}
				}
			}

				
			$counter++;
		}
		
		break;
		
	case "change_special_order":
		$special_orders->changeSpecialOrder((int)$_POST['id']);
		break;
		
	case "get_page_content":
		
		switch ($_POST['page'])
		{
			case "orders":
				$validators = new Validators();
				$orders = new Orders();
				$params = unserialize($_POST["params"]);
				
				$user_names = $orders->getUsersNames();
				$limits = $orders->getLimits();
			
				$xml .= '<content><![CDATA[';
				
				if ($_SESSION["b2b_user"]['id'])
				{	
					if (empty($params['param2']) || in_array($params['param2'], array('u_zamawiajacego', 'akceptuj','odrzuc','odebralem')))
					{
						$list = $orders->getOrders(9);
						$orders_count = count($list);
						
						$xml .= '<p class="query_title"><strong>W Unimet - propozycja zamiennika</strong></p>
						<table class="proteges_list">
						<tr>
							<th style="width:65px">Nr zam.</th>
							<th style="width:65px">Wg. zapyt.</th>
							<th style="width:50px">Wydz.</th>
							<th style="width:150px">Zamawiający</th>
							<th style="width:150px">Analityk</th>
							<th style="width:100px">Data zam.</th>
							<th style="width:100px">Wartość netto</th>
							<th style="width:50px">Czeka</th>
							<th>Szczegóły</th>
						</tr>';
						
						if ($orders_count)
						{
							$counter = 0;
							
							foreach ($list as $key => $row)
							{			
								if (($_SESSION["b2b_user"]['observer'] == 1 || $_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['contractor'], $row['analyst'], $row['analyst_deputy'], $row['manager'], $row['manager_deputy'], $row['receiving'], $row['receiving_deputy'])))) && $row['query_id'] == 0)
								{
									if ($row['id'] != $list[$key-1]['id'])
									{
										$counter++;
										
										$details = '<table cellspacing="0" cellpadding="0" style="width:100%;">
										<tr>
											<th><strong>Nazwa produktu</strong></th>
											<th><strong>Propozycja zamiennika</strong></th>
											<th><strong>Producent</strong></th>
											<th><strong>Jedn. miary</strong></th>
											<th><strong>Ilość</strong></th>
											<th><strong>Cena netto</strong></th>
											<th><strong>Wartość netto</strong></th>
											<th><strong>Wartość brutto</strong></th>
											<th><strong title="komentarz handlowca">Kom. handl.</strong></th>
										</tr>';
										
										$weight = 0;
										$pallets = 0;
										$netto_sum  = 0;
										$tax_sum = 0;
										
										if ($row['payment'] == 'za pobraniem ( + 6zł)')
										{
											$payment = 1;
										}
										else 
										{
											$payment = 0;
										}
									}
									
									$tax = ((int)$row['tax']*$row['netto_price'])/100;
									$product_tax = sprintf("%0.2f", $tax*$row['amount']);
									$netto_value = sprintf("%0.2f", ($row['netto_price'] * $row['amount']));
									$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
									
									$details .= '<tr>';
									$details .= '<td>'.$row['name'].'</td>';
									$details .= '<td class="replacement"><input type="hidden" name="replacements[]" value="'.$row['replacement_element'].'" />'.(($row['replacement_id']) ? '<span><img title="zrezygnuj z zamiennika" src="images/delete_icon.png" alt="" /></span> <a onclick="javascript:pop_up('.$row['replacement_id'].');">'.$row['replacement_name'].' -  '.$row['replacement_symbol'].' ('.$prices->getPrice($row['replacement_id'], 6).' zł netto)</a>' : '&nbsp;').'</td>';
									$details .= '<td>'.$row['producer_name'].'</td>';
									$details .= '<td>';
									
									if ((int)$row['unit'])
									{
										switch ($row['unit'])
										{
											case 1: $details .= 'kg'; break;
											case 2: $details .= 'komplet'; break;
											case 3: $details .= 'metr kwadr.'; break;
											case 4: $details .= 'metr sześć.'; break;
											case 5: $details .= 'metr bierz.'; break;
											case 6: $details .= 'opakowanie'; break;
											case 7: $details .= 'para'; break;
											case 8: $details .= 'sztuka'; break;
										}
									}
									else
									{
										switch ($row['default_unit'])
										{
											case 'SZT': $details .= 'sztuka'; break;
											case 'KPL': $details .= 'komplet'; break;
											case 'KG': $details .= 'kg'; break;
											case 'OP': $details .= 'opakowanie'; break;
											case 'M2': $details .= 'metr kwadr.'; break;
											case 'MB': $details .= 'metr bierz.'; break;
											case 'M3': $details .= 'metr sześć.'; break;
											case 'PAR': $details .= 'para'; break;
											default: $details .= '&nbsp;'; break;
										}
									}
									
									$details .= '</td>';
									$details .= '<td>'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'</td>';
									$details .= '<td>'.$row['netto_price'].' zł</td>';
									$details .= '<td><span class="netto_value">'.$netto_value.'</span> zł</td>';
									$details .= '<td><span class="brutto_value">'.$brutto_value.'</span> zł</td>';
									$details .= '<td>'.((!empty($row['details'])) ? '<img class="info_change" src="images/informations_icon.png" alt="" id="detail_'.$row['detail_id'].'" />' : '&nbsp;').'</td>';
									$details .= '</tr>';
									
									$weight += $row['amount'] * $row['weight'];
										
									if ($row['pallet'] == 1)
									{
										$pallets ++;
									}
										
									$netto_sum += $netto_value;
									$tax_sum += $product_tax; 
									
									if ($row['id'] != $list[$key+1]['id'])
									{
										$details .= '
										</table>
										<div class="order_informations" style="border:1px solid #86b1d4; padding:5px; margin:5px 0px 5px 0px">
											<div class="path">
												<div class="element '.(empty($row['department']) ? 'active' : '').'">
													<p><span>Zamawiający</span></p>
													<p>'.$user_names[$row['contractor_login']].'</p>
													<p>'.$row['contractor_email'].'</p>
													<p><strong>reakcja:</strong> '.$row['contractor_date'].'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] == 0)
												{
													$details .= '<div class="element '.(!empty($row['department']) ? 'active' : '').'">
														<p><span>UNIMET - propozyjcja zamiennika</span></p>
														<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
														<p>'.$row['unimet_email'].'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
										
												$details .= '<div class="element">
													<p><span>Analityk</span></p>
													<p>'.$user_names[$row['analyst_login']].'</p>
													<p>'.$row['analyst_email'].'</p>
													<p>'.(($row['analyst_date'] != "1970-01-01") ? $row['analyst_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] > 0)
												{
													$details .= '<div class="element">
														<p><span>Administrator</span></p>
														<p>'.$user_names[$row['administrator_login']].'</p>
														<p>'.$row['administrator_email'].'</p>
														<p>'.(($row['administrator_date'] != "1970-01-01") ? $row['administrator_date'] : "").'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
												
												$details .= '
												<div class="element">
													<p><span>Kierownik</span></p>
													<p>'.$user_names[$row['manager_login']].'</p>
													<p>'.$row['manager_email'].'</p>
													<p>'.(($row['manager_date'] != "1970-01-01") ? $row['manager_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element">
													<p><span>UNIMET</span></p>
													<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
													<p>'.$row['unimet_email'].'</p>
													<p>'.(($row['delivery_date'] != "1970-01-01") ? '<strong>dostawa:</strong> '.$row['delivery_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element">
													<p><span>Odbierający</span></p>
													<p>'.$user_names[$row['receiving_login']].'</p>
													<p>'.$row['receiving_email'].'</p>
													<p>'.(($row['receiving_date'] != "1970-01-01") ? $row['receiving_date'] : "").'</p>
												</div>
												<div class="clear"></div>
											</div>
											<div class="info">';
										
											$details .= '<p><strong>Wydział:</strong> '.$row['department'].'</p>';
											$details .= '<p><strong>Numer MPK:</strong> '.$row['mpk'].'</p>';
											
											if ((int)$row['id'] == $list[$key-1]['id'])
											{
												$details .= '<p><strong>Wartość zamówienia netto:</strong> '.$row['netto_value'].' zł</p>';
											}
											
											$details .= '<p><strong>Data zamówienia:</strong> '.$row['add_date'].'</p>';
										
											if (!in_array($row['status'], array(6,7)))
											{
												$details .= '<p><strong>Opiekun handlowy:</strong> '.((!empty($row['unimet_representative'])) ? $row['unimet_representative'] : 'brak danych').'</p>
												<p><strong>Faktura VAT:</strong> '.((!empty($row['fv'])) ? $row['fv'] : 'brak danych').'</p>
												<p><strong>WZ:</strong> '.((!empty($row['wm'])) ? $row['wm'] : 'brak danych').' '.((!empty($row['wz_document'])) ? '(<a target="blank" href="'.BASE_ADDRESS.'../images/wz/'.$row['wz_document'].'" style="color:#044996">'.$row['wz_document'].'</a>)' : '').'</p>';
											}
										
											if ($validators->validateTextarea($row['order_comment']))
											{
												$details .= '<p><strong>Komentarz zamawiającego:</strong></p>
												<p>'.nl2br($row['order_comment']).'</p>';
											}
											
											if ($validators->validateTextarea($row['merchant_information']))
											{
												$details .= '<p><strong>Komentarz handlowca:</strong></p>
												<p>'.nl2br($row['merchant_information']).'</p>';
											}
											
											$details .= '</div>
										</div>';
										
										$transport_brutto = sprintf("%0.2f", $prices->getTransportPrice((($_SESSION["b2b_user"]['transport'] == '') ? $weight : 0), $row['brutto_value'], $pallets, $payment));
										$transport_netto  = sprintf("%0.2f", (100*$transport_brutto/122));
										$transport_tax = sprintf("%0.2f", (22*($transport_netto)/100));
									
										$total_sum = sprintf("%0.2f", ($row['brutto_value'] + $transport_brutto));
										
									
										$xml .= '<tr>
											<td class="bg_'.(($counter%2)+1).'" '.(($row['date_diff'] >= $limits['days']) ? 'style="background:#fdaaaa"' : '').'>'.$orders->formatId($row['id'], $row['query_id']).'</td>
											<td class="bg_'.(($counter%2)+1).'">'.((!empty($row['query_id'])) ? $orders->formatId($row['query_id'], $row['query_id']) : '&nbsp;').'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['department'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['contractor_login'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['analyst_login'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.date('Y-m-d', strtotime($row['add_date'])).'</td>
											<td class="bg_'.(($counter%2)+1).'"><span class="netto_value" alt="'.$row['id'].'">'.$row['netto_value'].'</span> zł</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['date_diff'].'</td>
											<td class="bg_'.(($counter%2)+1).'">
											<a href="#" class="see_details" id="see_'.$row['id'].'">szczegóły</a> 
											</td>
										</tr>
										<tr class="order_details" id="details_'.$row['id'].'" style="display:none;">
										<td colspan="9">'.$details.'</td>
										</tr>';
									}
								}
							}
						}
						else
						{
							$xml .= '<tr><td class="bg_2" colspan="9">Brak zamówień spełniających kryteria</td></tr>';
						}
						
						$xml .= '</table>';
					
						$list = $orders->getOrders(7);
						$orders_count = count($list);
						
						$xml .= '<p class="query_title"><strong>U zamawiającego</strong></p>
						<table class="proteges_list">
						<tr>
							<th style="width:65px">Nr zam.</th>
							<th style="width:65px">Wg. zapyt.</th>
							<th style="width:50px">Wydz.</th>
							<th style="width:150px">Zamawiający</th>
							<th style="width:150px">Analityk</th>
							<th style="width:100px">Data zam.</th>
							<th style="width:100px">Wartość netto</th>
							<th style="width:50px">Czeka</th>
							<th>Szczegóły</th>
						</tr>';
						
						if ($orders_count)
						{
							$counter = 0;
							
							foreach ($list as $key => $row)
							{			
								if ($_SESSION["b2b_user"]['observer'] == 1 || $_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['contractor'], $row['analyst'], $row['analyst_deputy'], $row['manager'], $row['manager_deputy'], $row['receiving'], $row['receiving_deputy']))))
								{
									if ($row['id'] != $list[$key-1]['id'])
									{
										$counter++;
										
										$details = '<table cellspacing="0" cellpadding="0" style="width:100%;">
										<tr>
											<th><strong>Nazwa produktu</strong></th>
											<th><strong>Symbol</strong></th>
											<th><strong>Propozycja zamiennika</strong></th>
											<th><strong>Producent</strong></th>
											<th><strong>Jedn. miary</strong></th>
											<th><strong>Ilość</strong></th>
											<th><strong>Cena netto</strong></th>
											<th><strong>Wartość netto</strong></th>
											<th><strong>Wartość brutto</strong></th>
											<th><strong title="komentarz handlowca">Kom. handl.</strong></th>
										</tr>';
										
										$weight = 0;
										$pallets = 0;
										$netto_sum  = 0;
										$tax_sum = 0;
										
										if ($row['payment'] == 'za pobraniem ( + 6zł)')
										{
											$payment = 1;
										}
										else 
										{
											$payment = 0;
										}
									}
									
									$tax = ((int)$row['tax']*$row['netto_price'])/100;
									$product_tax = sprintf("%0.2f", $tax*$row['amount']);
									$netto_value = sprintf("%0.2f", ($row['netto_price'] * $row['amount']));
									$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
									
									$details .= '<tr>';
									$details .= '<td>'.$row['name'].'</td>';
									$details .= '<td>'.$row['symbol'].'</td>';
									$details .= '<td class="replacement"><input type="hidden" name="replacements[]" value="'.$row['replacement_element'].'" />'.(($row['replacement_id']) ? '<span><img title="zrezygnuj z zamiennika" src="images/delete_icon.png" alt="" /></span> <a onclick="javascript:pop_up('.$row['replacement_id'].');">'.$row['replacement_name'].' -  '.$row['replacement_symbol'].' ('.$prices->getPrice($row['replacement_id'], 6).' zł netto)</a>' : '&nbsp;').'</td>';
									$details .= '<td>'.$row['producer_name'].'</td>';
									$details .= '<td>';
									
									if ((int)$row['unit'])
									{
										switch ($row['unit'])
										{
											case 1: $details .= 'kg'; break;
											case 2: $details .= 'komplet'; break;
											case 3: $details .= 'metr kwadr.'; break;
											case 4: $details .= 'metr sześć.'; break;
											case 5: $details .= 'metr bierz.'; break;
											case 6: $details .= 'opakowanie'; break;
											case 7: $details .= 'para'; break;
											case 8: $details .= 'sztuka'; break;
										}
									}
									else
									{
										switch ($row['default_unit'])
										{
											case 'SZT': $details .= 'sztuka'; break;
											case 'KPL': $details .= 'komplet'; break;
											case 'KG': $details .= 'kg'; break;
											case 'OP': $details .= 'opakowanie'; break;
											case 'M2': $details .= 'metr kwadr.'; break;
											case 'MB': $details .= 'metr bierz.'; break;
											case 'M3': $details .= 'metr sześć.'; break;
											case 'PAR': $details .= 'para'; break;
											default: $details .= '&nbsp;'; break;
										}
									}
									
									$details .= '</td>';
									
									if ($_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['analyst'], $row['analyst_deputy']))))
									{
										$details .= '<td><span class="amount_input"><input type="text" name="amount" alt="'.$row['id'].'#'.$row['detail_id'].'" value="'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'" /></span></td>';
									}
									else
									{
										$details .= '<td>'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'</td>';
									}
									
									$details .= '<td>'.$row['netto_price'].' zł</td>';
									$details .= '<td><span class="netto_value">'.$netto_value.'</span> zł</td>';
									$details .= '<td><span class="brutto_value">'.$brutto_value.'</span> zł</td>';
									$details .= '<td>'.((!empty($row['details'])) ? '<img class="info_change" src="images/informations_icon.png" alt="" id="detail_'.$row['detail_id'].'" />' : '&nbsp;').'</td>';
									$details .= '</tr>';
									
									$weight += $row['amount'] * $row['weight'];
										
									if ($row['pallet'] == 1)
									{
										$pallets ++;
									}
										
									$netto_sum += $netto_value;
									$tax_sum += $product_tax; 
									
									if ($row['id'] != $list[$key+1]['id'])
									{
										$details .= '
										</table>
										<div class="order_informations" style="border:1px solid #86b1d4; padding:5px; margin:5px 0px 5px 0px">
											<div class="path">
												<div class="element active">
													<p><span>Zamawiający</span></p>
													<p>'.$user_names[$row['contractor_login']].'</p>
													<p>'.$row['contractor_email'].'</p>
													<p><strong>reakcja:</strong> '.$row['contractor_date'].'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] == 0)
												{
													$details .= '<div class="element">
														<p><span>UNIMET - propozyjcja zamiennika</span></p>
														<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
														<p>'.$row['unimet_email'].'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
										
												$details .= '<div class="element">
													<p><span>Analityk</span></p>
													<p>'.$user_names[$row['analyst_login']].'</p>
													<p>'.$row['analyst_email'].'</p>
													<p>'.(($row['analyst_date'] != "1970-01-01") ? $row['analyst_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] > 0)
												{
													$details .= '<div class="element">
														<p><span>Administrator</span></p>
														<p>'.$user_names[$row['administrator_login']].'</p>
														<p>'.$row['administrator_email'].'</p>
														<p>'.(($row['administrator_date'] != "1970-01-01") ? $row['administrator_date'] : "").'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
												
												$details .= '
												<div class="element">
													<p><span>Kierownik</span></p>
													<p>'.$user_names[$row['manager_login']].'</p>
													<p>'.$row['manager_email'].'</p>
													<p>'.(($row['manager_date'] != "1970-01-01") ? $row['manager_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element">
													<p><span>UNIMET</span></p>
													<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
													<p>'.$row['unimet_email'].'</p>
													<p>'.(($row['delivery_date'] != "1970-01-01") ? '<strong>dostawa:</strong> '.$row['delivery_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element">
													<p><span>Odbierający</span></p>
													<p>'.$user_names[$row['receiving_login']].'</p>
													<p>'.$row['receiving_email'].'</p>
													<p>'.(($row['receiving_date'] != "1970-01-01") ? $row['receiving_date'] : "").'</p>
												</div>
												<div class="clear"></div>
											</div>
											<div class="info">';
										
											$details .= '<p><strong>Wydział:</strong> '.$row['department'].'</p>';
											$details .= '<p><strong>Numer MPK:</strong> '.$row['mpk'].'</p>';
											
											if ($row['query_id'] > 0)
											{
												$details .= '<p><strong>Według zapytania nr:</strong> '.$orders->formatId($row['query_id'], $row['query_id']).'</p>';
											}
											
											if ((int)$row['id'] == $list[$key-1]['id'])
											{
												$details .= '<p><strong>Wartość zamówienia netto:</strong> '.$row['netto_value'].' zł</p>';
											}
											
											$details .= '<p><strong>Data zamówienia:</strong> '.$row['add_date'].'</p>';
										
											if (!in_array($row['status'], array(6,7)))
											{
												$details .= '<p><strong>Opiekun handlowy:</strong> '.((!empty($row['unimet_representative'])) ? $row['unimet_representative'] : 'brak danych').'</p>
												<p><strong>Faktura VAT:</strong> '.((!empty($row['fv'])) ? $row['fv'] : 'brak danych').'</p>
												<p><strong>WZ:</strong> '.((!empty($row['wm'])) ? $row['wm'] : 'brak danych').' '.((!empty($row['wz_document'])) ? '(<a target="blank" href="'.BASE_ADDRESS.'../images/wz/'.$row['wz_document'].'" style="color:#044996">'.$row['wz_document'].'</a>)' : '').'</p>';
											}
										
											if ($validators->validateTextarea($row['order_comment']))
											{
												$details .= '<p><strong>Komentarz zamawiającego:</strong></p>
												<p>'.nl2br($row['order_comment']).'</p>';
											}
											
											if ($validators->validateTextarea($row['merchant_information']))
											{
												$details .= '<p><strong>Komentarz handlowca:</strong></p>
												<p>'.nl2br($row['merchant_information']).'</p>';
											}
											
											$details .= '</div>
										</div>';
										
										$transport_brutto = sprintf("%0.2f", $prices->getTransportPrice((($_SESSION["b2b_user"]['transport'] == '') ? $weight : 0), $row['brutto_value'], $pallets, $payment));
										$transport_netto  = sprintf("%0.2f", (100*$transport_brutto/122));
										$transport_tax = sprintf("%0.2f", (22*($transport_netto)/100));
									
										$total_sum = sprintf("%0.2f", ($row['brutto_value'] + $transport_brutto));

										$year_postfix = ((date('Y', strtotime($row['add_date'])) == date('Y')) ? '' : '_'. date('Y', strtotime($row['add_date'])));
										
										$xml .= '<tr>
											<td class="bg_'.(($counter%2)+1).'" '.(($row['date_diff'] >= $limits['days']) ? 'style="background:#fdaaaa"' : '').'>'.$orders->formatId($row['id'], $row['query_id']).'</td>
											<td class="bg_'.(($counter%2)+1).'">'.((!empty($row['query_id'])) ? $orders->formatId($row['query_id'], $row['query_id']) : '&nbsp;').'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['department'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['contractor_login'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['analyst_login'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.date('Y-m-d', strtotime($row['add_date'])).'</td>
											<td class="bg_'.(($counter%2)+1).'"><span class="netto_value" alt="'.$row['id'].'">'.$row['netto_value'].'</span> zł</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['date_diff'].'</td>
											<td class="bg_'.(($counter%2)+1).'">';
											
											if ($_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['contractor'], $row['contractor_deputy'])))) 
											{
												$xml .= '<span style="padding:0px 5px 0px 5px; display:'.(($row['budget_value' . $year_postfix] >= $row['netto_value']) ? "inline" : "none").'"><a  class="accept_archive_order" href="./zamowienia,akceptuj,'.$row['id'].'.html">akceptuj</a></span>';
											}
											$xml .= '<span style="padding:0px 5px 0px 5px;"><a href="#" class="see_details" id="see_'.$row['id'].'">szczegóły</a></span>';
											
 										
											if ($_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['contractor'], $row['contractor_deputy'])))) 
											{
												$xml .= '<span style="padding:0px 5px 0px 5px;"><a target="blank" alt="'.$row['id'].'" href="./include/archive_order.php?id='.$row['id'].'">wydruk</a></span>
												<strong style="padding:0px 5px 0px 5px; display:'.(($row['budget_value' . $year_postfix] >= $row['netto_value']) ? "none" : "inline").'" class="exceeded_budget">przekroczono budżet</strong>
												<span style="padding:0px 5px 0px 5px;"><a class="reject_order" alt="'.$row['id'].'" href="./zamowienia,odrzuc,'.$row['id'].'.html">odrzuć</a></span>';
											}
											
											$xml .= '</td>
										</tr>
										<tr class="order_details" id="details_'.$row['id'].'" style="display:none;">
										<td colspan="9">'.$details.'</td>
										</tr>';
									}
								}
							}
						}
						else
						{
							$xml .= '<tr><td class="bg_2" colspan="9">Brak zamówień spełniających kryteria</td></tr>';
						}
						
						$xml .= '</table>';
					}
					
					if (empty($params['param2']) || in_array($params['param2'], array('u_analityka', 'akceptuj','odrzuc','odebralem')))
					{
						$list = $orders->getOrders(1);
						$orders_count = count($list);
								
						$xml .= '<p class="query_title"><strong>U analityka</strong></p>
						<table class="proteges_list">
						<tr>
							<th style="width:65px">Nr zam.</th>
							<th style="width:65px">Wg. zapyt.</th>
							<th style="width:50px">Wydz.</th>
							<th style="width:150px">Zamawiający</th>
							<th style="width:150px">Analityk</th>
							<th style="width:100px">Data zam.</th>
							<th style="width:100px">Wartość netto</th>
							<th style="width:50px">Czeka</th>
							<th>Szczegóły</th>
						</tr>';
						
						if ($orders_count)
						{
							$counter = 0;
							
							foreach ($list as $key => $row)
							{
								if ($_SESSION["b2b_user"]['observer'] == 1 || $_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['contractor'], $row['analyst'], $row['analyst_deputy'], $row['manager'], $row['manager_deputy'], $row['receiving'], $row['receiving_deputy']))))
								{
									if ($row['id'] != $list[$key-1]['id'])
									{
										$counter++;
										
										$details = '<table cellspacing="0" cellpadding="0" style="width:100%;">
										<tr>
											<th><strong>Nazwa produktu</strong></td>
											<th><strong>Symbol</strong></th>
											<th><strong>Producent</strong></th>
											<th><strong>Jedn. miary</strong></th>
											<th><strong>Ilość</strong></td>
											<th><strong>Cena netto</strong></td>
											<th><strong>Wartość netto</strong></td>
											<th><strong>Wartość brutto</strong></td>
											<th><strong title="komentarz handlowca">Kom. handl.</strong></th>
										</tr>';
										
										$weight = 0;
										$pallets = 0;
										$netto_sum  = 0;
										$tax_sum = 0;
										
										if ($row['payment'] == 'za pobraniem ( + 6zł)')
										{
											$payment = 1;
										}
										else 
										{
											$payment = 0;
										}
									}
									
									$tax = ((int)$row['tax']*$row['netto_price'])/100;
									$product_tax = sprintf("%0.2f", $tax*$row['amount']);
									$netto_value = sprintf("%0.2f", ($row['netto_price'] * $row['amount']));
									$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
									
									$details .= '<tr>';
									$details .= '<td>'.$row['name'].'</td>';
									$details .= '<td>'.$row['symbol'].'</td>';
									$details .= '<td>'.$row['producer_name'].'</td>';
									$details .= '<td>';
									
									if ((int)$row['unit'])
									{
										switch ($row['unit'])
										{
											case 1: $details .= 'kg'; break;
											case 2: $details .= 'komplet'; break;
											case 3: $details .= 'metr kwadr.'; break;
											case 4: $details .= 'metr sześć.'; break;
											case 5: $details .= 'metr bierz.'; break;
											case 6: $details .= 'opakowanie'; break;
											case 7: $details .= 'para'; break;
											case 8: $details .= 'sztuka'; break;
										}
									}
									else
									{
										switch ($row['default_unit'])
										{
											case 'SZT': $details .= 'sztuka'; break;
											case 'KPL': $details .= 'komplet'; break;
											case 'KG': $details .= 'kg'; break;
											case 'OP': $details .= 'opakowanie'; break;
											case 'M2': $details .= 'metr kwadr.'; break;
											case 'MB': $details .= 'metr bierz.'; break;
											case 'M3': $details .= 'metr sześć.'; break;
											case 'PAR': $details .= 'para'; break;
											default: $details .= '&nbsp;'; break;
										}
									}
									
									$details .= '</td>';
									
									if ($_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['analyst'], $row['analyst_deputy']))))
									{
										$details .= '<td><span class="amount_input"><input type="text" name="amount" alt="'.$row['id'].'#'.$row['detail_id'].'" value="'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'" /></span></td>';
									}
									else
									{
										$details .= '<td>'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'</td>';
									}
									
									$details .= '<td>'.$row['netto_price'].' zł</td>';
									$details .= '<td><span class="netto_value">'.$netto_value.'</span> zł</td>';
									$details .= '<td><span class="brutto_value">'.$brutto_value.'</span> zł</td>';
									$details .= '<td>'.((!empty($row['details'])) ? '<img class="info_change" src="images/informations_icon.png" alt="" id="detail_'.$row['detail_id'].'" />' : '&nbsp;').'</td>';
									$details .= '</tr>';
									
									$weight += $row['amount'] * $row['weight'];
										
									if ($row['pallet'] == 1)
									{
										$pallets ++;
									}
										
									$netto_sum += $netto_value;
									$tax_sum += $product_tax; 
									
									if ($row['id'] != $list[$key+1]['id'])
									{
										$details .= '
										</table>
										<div class="order_informations" style="border:1px solid #86b1d4; padding:5px; margin:5px 0px 5px 0px">
											<div class="path">
												<div class="element">
													<p><span>Zamawiający</span></p>
													<p>'.$user_names[$row['contractor_login']].'</p>
													<p>'.$row['contractor_email'].'</p>
													<p><strong>reakcja:</strong> '.$row['contractor_date'].'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] == 0)
												{
													$details .= '<div class="element">
														<p><span>UNIMET - propozyjcja zamiennika</span></p>
														<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
														<p>'.$row['unimet_email'].'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
										
												$details .= '<div class="element active">
													<p><span>Analityk</span></p>
													<p>'.$user_names[$row['analyst_login']].'</p>
													<p>'.$row['analyst_email'].'</p>
													<p><strong>otrzymano:</strong> '.$row['contractor_date'].'</p>
													<p>'.(($row['analyst_date'] != "1970-01-01") ? '<strong>reakcja:</strong> '.$row['analyst_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] > 0)
												{
													$details .= '<div class="element">
														<p><span>Administrator</span></p>
														<p>'.$user_names[$row['administrator_login']].'</p>
														<p>'.$row['administrator_email'].'</p>
														<p>'.(($row['administrator_date'] != "1970-01-01") ? $row['administrator_date'] : "").'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
												
												$details .= '<div class="element">
													<p><span>Kierownik</span></p>
													<p>'.$user_names[$row['manager_login']].'</p>
													<p>'.$row['manager_email'].'</p>
													<p>'.(($row['manager_date'] != "1970-01-01") ? $row['manager_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element">
													<p><span>UNIMET</span></p>
													<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
													<p>'.$row['unimet_email'].'</p>
													<p>'.(($row['delivery_date'] != "1970-01-01") ? '<strong>dostawa:</strong> '.$row['delivery_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element">
													<p><span>Odbierający</span></p>
													<p>'.$user_names[$row['receiving_login']].'</p>
													<p>'.$row['receiving_email'].'</p>
													<p>'.(($row['receiving_date'] != "1970-01-01") ? $row['receiving_date'] : "").'</p>
												</div>
												<div class="clear"></div>
											</div>
											<div class="info">';
										
											$details .= '<p><strong>Wydział:</strong> '.$row['department'].'</p>';
											$details .= '<p><strong>Numer MPK:</strong> '.$row['mpk'].'</p>';
											
											if ($row['query_id'] > 0)
											{
												$details .= '<p><strong>Według zapytania nr:</strong> '.$orders->formatId($row['query_id'], $row['query_id']).'</p>';
											}
											
											if ((int)$row['id'] == $list[$key-1]['id'])
											{
												$details .= '<p><strong>Wartość zamówienia netto:</strong> '.$row['netto_value'].' zł</p>';
											}
											
											$details .= '<p><strong>Data zamówienia:</strong> '.$row['add_date'].'</p>';
										
											if (!in_array($row['status'], array(6,7)))
											{
												$details .= '<p><strong>Opiekun handlowy:</strong> '.((!empty($row['unimet_representative'])) ? $row['unimet_representative'] : 'brak danych').'</p>
												<p><strong>Faktura VAT:</strong> '.((!empty($row['fv'])) ? $row['fv'] : 'brak danych').'</p>
												<p><strong>WZ:</strong> '.((!empty($row['wm'])) ? $row['wm'] : 'brak danych').' '.((!empty($row['wz_document'])) ? '(<a target="blank" href="'.BASE_ADDRESS.'../images/wz/'.$row['wz_document'].'" style="color:#044996">'.$row['wz_document'].'</a>)' : '').'</p>';
											}
										
											if ($validators->validateTextarea($row['order_comment']))
											{
												$details .= '<p><strong>Komentarz zamawiającego:</strong></p>
												<p>'.nl2br($row['order_comment']).'</p>';
											}
											
											if ($validators->validateTextarea($row['merchant_information']))
											{
												$details .= '<p><strong>Komentarz handlowca:</strong></p>
												<p>'.nl2br($row['merchant_information']).'</p>';
											}
											
											$details .= '</div>
										</div>';
										
										$transport_brutto = sprintf("%0.2f", $prices->getTransportPrice((($_SESSION["b2b_user"]['transport'] == '') ? $weight : 0), $row['brutto_value'], $pallets, $payment));
										$transport_netto  = sprintf("%0.2f", (100*$transport_brutto/122));
										$transport_tax = sprintf("%0.2f", (22*($transport_netto)/100));
									
										$total_sum = sprintf("%0.2f", ($row['brutto_value'] + $transport_brutto));

										$year_postfix = ((date('Y', strtotime($row['add_date'])) == date('Y')) ? '' : '_'. date('Y', strtotime($row['add_date'])));
										
										$xml .= '<tr>
											<td class="bg_'.(($counter%2)+1).'" '.(($row['date_diff'] >= $limits['days']) ? 'style="background:#fdaaaa"' : '').'>'.$orders->formatId($row['id'], $row['query_id']).'</td>
											<td class="bg_'.(($counter%2)+1).'">'.((!empty($row['query_id'])) ? $orders->formatId($row['query_id'], $row['query_id']) : '&nbsp;').'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['department'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['contractor_login'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['analyst_login'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.date('Y-m-d', strtotime($row['add_date'])).'</td>
											<td class="bg_'.(($counter%2)+1).'"><span class="netto_value" alt="'.$row['id'].'">'.$row['netto_value'].'</span> zł</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['date_diff'].'</td>
											<td class="bg_'.(($counter%2)+1).'">';
											
											if ($_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['analyst'], $row['analyst_deputy'])))) 
											{
												$xml .= '<span style="padding:0px 5px 0px 5px; display:'.(($row['budget_value' . $year_postfix] >= $row['netto_value']) ? "inline" : "none").'"><a  class="accept_archive_order" href="./zamowienia,akceptuj,'.$row['id'].'.html">akceptuj</a></span>';
											}
											
											$xml .= '<span style="padding:0px 5px 0px 5px;"><a href="#" class="see_details" id="see_'.$row['id'].'">szczegóły</a></span>';
											
											if ($_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['analyst'], $row['analyst_deputy'])))) 
											{
												$xml .= '<span style="padding:0px 5px 0px 5px;"><a target="blank" alt="'.$row['id'].'" href="./include/archive_order.php?id='.$row['id'].'">wydruk</a></span>
												<strong style="padding:0px 5px 0px 5px; display:'.(($row['budget_value' . $year_postfix] >= $row['netto_value']) ? "none" : "inline").'" class="exceeded_budget">przekroczono budżet</strong>
												<span style="padding:0px 5px 0px 5px;"><a class="reject_order" alt="'.$row['id'].'" href="./zamowienia,odrzuc,'.$row['id'].'.html">odrzuć</a></span>';
											}
											
											$xml .= '</td>
										</tr>
										<tr class="order_details" id="details_'.$row['id'].'" style="display:none;">
										<td colspan="9">'.$details.'</td>
										</tr>';
									}
								}
							}
						}
						else
						{
							$xml .= '<tr><td class="bg_2" colspan="9">Brak zamówień spełniających kryteria</td></tr>';
						}
						
						$xml .= '</table>';
					}
					
					if (empty($params['param2']) || in_array($params['param2'], array('u_administratora', 'akceptuj','odrzuc','odebralem')))
					{
						$list = $orders->getOrders(8);
						$orders_count = count($list);
						
						$xml .= '<p class="query_title"><strong>U administratora</strong></p>
						<table class="proteges_list">
						<tr>
							<th style="width:65px">Nr zam.</th>
							<th style="width:65px">Wg. zapyt.</th>
							<th style="width:50px">Wydz.</th>
							<th style="width:150px">Zamawiający</th>
							<th style="width:150px">Analityk</th>
							<th style="width:100px">Data zam.</th>
							<th style="width:100px">Wartość netto</th>
							<th style="width:50px">Czeka</th>
							<th>Szczegóły</th>
						</tr>';
						
						if ($orders_count)
						{
							$counter = 0;
							
							foreach ($list as $key => $row)
							{
								if ($_SESSION["b2b_user"]['observer'] == 1 || $_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['contractor'], $row['analyst'], $row['analyst_deputy'], $row['manager'], $row['manager_deputy'], $row['receiving'], $row['receiving_deputy']))))
								{
									if ($row['id'] != $list[$key-1]['id'])
									{
										$counter++;
										
										$details = '<table cellspacing="0" cellpadding="0" style="width:100%;">
										<tr>
											<th><strong>Nazwa produktu</strong></td>
											<th><strong>Symbol</strong></th>
											<th><strong>Producent</strong></th>
											<th><strong>Jedn. miary</strong></th>
											<th><strong>Ilość</strong></td>
											<th><strong>Cena netto</strong></td>
											<th><strong>Wartość netto</strong></td>
											<th><strong>Wartość brutto</strong></td>
										</tr>';
										
										$weight = 0;
										$pallets = 0;
										$netto_sum  = 0;
										$tax_sum = 0;
										
										if ($row['payment'] == 'za pobraniem ( + 6zł)')
										{
											$payment = 1;
										}
										else 
										{
											$payment = 0;
										}
									}
									
									$tax = ((int)$row['tax']*$row['netto_price'])/100;
									$product_tax = sprintf("%0.2f", $tax*$row['amount']);
									$netto_value = sprintf("%0.2f", ($row['netto_price'] * $row['amount']));
									$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
									
									$details .= '<tr>';
									$details .= '<td>'.$row['name'].'</td>';
									$details .= '<td>'.$row['symbol'].'</td>';
									$details .= '<td>'.$row['producer_name'].'</td>';
									$details .= '<td>';
									
									if ((int)$row['unit'])
									{
										switch ($row['unit'])
										{
											case 1: $details .= 'kg'; break;
											case 2: $details .= 'komplet'; break;
											case 3: $details .= 'metr kwadr.'; break;
											case 4: $details .= 'metr sześć.'; break;
											case 5: $details .= 'metr bierz.'; break;
											case 6: $details .= 'opakowanie'; break;
											case 7: $details .= 'para'; break;
											case 8: $details .= 'sztuka'; break;
										}
									}
									else
									{
										switch ($row['default_unit'])
										{
											case 'SZT': $details .= 'sztuka'; break;
											case 'KPL': $details .= 'komplet'; break;
											case 'KG': $details .= 'kg'; break;
											case 'OP': $details .= 'opakowanie'; break;
											case 'M2': $details .= 'metr kwadr.'; break;
											case 'MB': $details .= 'metr bierz.'; break;
											case 'M3': $details .= 'metr sześć.'; break;
											case 'PAR': $details .= 'para'; break;
											default: $details .= '&nbsp;'; break;
										}
									}
									
									$details .= '</td>';
									
									if ($_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['analyst'], $row['analyst_deputy']))))
									{
										$details .= '<td><span class="amount_input"><input type="text" name="amount" alt="'.$row['id'].'#'.$row['detail_id'].'" value="'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'" /></span></td>';
									}
									else
									{
										$details .= '<td>'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'</td>';
									}
									
									$details .= '<td>'.$row['netto_price'].' zł</td>';
									$details .= '<td><span class="netto_value">'.$netto_value.'</span> zł</td>';
									$details .= '<td><span class="brutto_value">'.$brutto_value.'</span> zł</td>';
									$details .= '<td>'.((!empty($row['details'])) ? '<img class="info_change" src="images/informations_icon.png" alt="" id="detail_'.$row['detail_id'].'" />' : '&nbsp;').'</td>';
									$details .= '</tr>';
									
									$weight += $row['amount'] * $row['weight'];
										
									if ($row['pallet'] == 1)
									{
										$pallets ++;
									}
										
									$netto_sum += $netto_value;
									$tax_sum += $product_tax; 
									
									if ($row['id'] != $list[$key+1]['id'])
									{
										$details .= '
										</table>
										<div class="order_informations" style="border:1px solid #86b1d4; padding:5px; margin:5px 0px 5px 0px">
											<div class="path">
												<div class="element">
													<p><span>Zamawiający</span></p>
													<p>'.$user_names[$row['contractor_login']].'</p>
													<p>'.$row['contractor_email'].'</p>
													<p><strong>reakcja:</strong> '.$row['contractor_date'].'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] == 0)
												{
													$details .= '<div class="element">
														<p><span>UNIMET - propozyjcja zamiennika</span></p>
														<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
														<p>'.$row['unimet_email'].'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
										
												$details .= '<div class="element">
													<p><span>Analityk</span></p>
													<p>'.$user_names[$row['analyst_login']].'</p>
													<p>'.$row['analyst_email'].'</p>
													<p><strong>otrzymano:</strong> '.$row['contractor_date'].'</p>
													<p>'.(($row['analyst_date'] != "1970-01-01") ? '<strong>reakcja:</strong> '.$row['analyst_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] > 0)
												{
													$details .= '<div class="element active">
														<p><span>Administrator</span></p>
														<p>'.$user_names[$row['administrator_login']].'</p>
														<p>'.$row['administrator_email'].'</p>
														<p>'.(($row['analyst_date'] != "1970-01-01") ? '<strong>otrzymano:</strong> '.$row['analyst_date'] : "").'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
												
												$details .= '<div class="element">
													<p><span>Kierownik</span></p>
													<p>'.$user_names[$row['manager_login']].'</p>
													<p>'.$row['manager_email'].'</p>
													<p>'.(($row['manager_date'] != "1970-01-01") ? $row['manager_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element">
													<p><span>UNIMET</span></p>
													<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
													<p>'.$row['unimet_email'].'</p>
													<p>'.(($row['delivery_date'] != "1970-01-01") ? '<strong>dostawa:</strong> '.$row['delivery_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element">
													<p><span>Odbierający</span></p>
													<p>'.$user_names[$row['receiving_login']].'</p>
													<p>'.$row['receiving_email'].'</p>
													<p>'.(($row['receiving_date'] != "1970-01-01") ? $row['receiving_date'] : "").'</p>
												</div>
												<div class="clear"></div>
											</div>
											<div class="info">';
										
											$details .= '<p><strong>Wydział:</strong> '.$row['department'].'</p>';
											$details .= '<p><strong>Numer MPK:</strong> '.$row['mpk'].'</p>';
											
											if ($row['query_id'] > 0)
											{
												$details .= '<p><strong>Według zapytania nr:</strong> '.$orders->formatId($row['query_id'], $row['query_id']).'</p>';
											}
											
											if ((int)$row['id'] == $list[$key-1]['id'])
											{
												$details .= '<p><strong>Wartość zamówienia netto:</strong> '.$row['netto_value'].' zł</p>';
											}
											
											$details .= '<p><strong>Data zamówienia:</strong> '.$row['add_date'].'</p>';
										
											if (!in_array($row['status'], array(6,7)))
											{
												$details .= '<p><strong>Opiekun handlowy:</strong> '.((!empty($row['unimet_representative'])) ? $row['unimet_representative'] : 'brak danych').'</p>
												<p><strong>Faktura VAT:</strong> '.((!empty($row['fv'])) ? $row['fv'] : 'brak danych').'</p>
												<p><strong>WZ:</strong> '.((!empty($row['wm'])) ? $row['wm'] : 'brak danych').' '.((!empty($row['wz_document'])) ? '(<a target="blank" href="'.BASE_ADDRESS.'../images/wz/'.$row['wz_document'].'" style="color:#044996">'.$row['wz_document'].'</a>)' : '').'</p>';
											}
										
											if ($validators->validateTextarea($row['order_comment']))
											{
												$details .= '<p><strong>Komentarz zamawiającego:</strong></p>
												<p>'.nl2br($row['order_comment']).'</p>';
											}
											
											if ($validators->validateTextarea($row['merchant_information']))
											{
												$details .= '<p><strong>Komentarz handlowca:</strong></p>
												<p>'.nl2br($row['merchant_information']).'</p>';
											}
											
											$details .= '</div>
										</div>';
										
										$transport_brutto = sprintf("%0.2f", $prices->getTransportPrice((($_SESSION["b2b_user"]['transport'] == '') ? $weight : 0), $row['brutto_value'], $pallets, $payment));
										$transport_netto  = sprintf("%0.2f", (100*$transport_brutto/122));
										$transport_tax = sprintf("%0.2f", (22*($transport_netto)/100));
									
										$total_sum = sprintf("%0.2f", ($row['brutto_value'] + $transport_brutto));

										$year_postfix = ((date('Y', strtotime($row['add_date'])) == date('Y')) ? '' : '_'. date('Y', strtotime($row['add_date'])));
										
										$xml .= '<tr>
											<td class="bg_'.(($counter%2)+1).'" '.(($row['date_diff'] >= $limits['days']) ? 'style="background:#fdaaaa"' : '').'>'.$orders->formatId($row['id'], $row['query_id']).'</td>
											<td class="bg_'.(($counter%2)+1).'">'.((!empty($row['query_id'])) ? $orders->formatId($row['query_id'], $row['query_id']) : '&nbsp;').'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['department'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['contractor_login'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['analyst_login'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.date('Y-m-d', strtotime($row['add_date'])).'</td>
											<td class="bg_'.(($counter%2)+1).'"><span class="netto_value" alt="'.$row['id'].'">'.$row['netto_value'].'</span> zł</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['date_diff'].'</td>
											<td class="bg_'.(($counter%2)+1).'">';
											
											if ($_SESSION["b2b_user"]['admin'] == 1) 
											{
												$xml .= '<span style="padding:0px 5px 0px 5px; display:'.(($row['budget_value' . $year_postfix] >= $row['netto_value']) ? "inline" : "none").'"><a class="accept_archive_order" href="./zamowienia,akceptuj,'.$row['id'].'.html">akceptuj1</a></span>';
											}
											
											$xml .= '<span style="padding:0px 5px 0px 5px;"><a href="#" class="see_details" id="see_'.$row['id'].'">szczegóły</a></span>';
											
											if ($_SESSION["b2b_user"]['admin'] == 1) 
											{
												$xml .= '<span style="padding:0px 5px 0px 5px;"><a target="blank" alt="'.$row['id'].'" href="./include/archive_order.php?id='.$row['id'].'">wydruk</a></span>
												<strong style="display:'.(($row['budget_value' . $year_postfix] >= $row['netto_value']) ? "none" : "inline").'" class="exceeded_budget">przekroczono budżet</strong>
												<span style="padding:0px 5px 0px 5px;"><a class="reject_order" alt="'.$row['id'].'" href="./zamowienia,odrzuc,'.$row['id'].'.html">odrzuć</a></span>';
											}
											
											$xml .= '</td>
										</tr>
										<tr class="order_details" id="details_'.$row['id'].'" style="display:none;">
										<td colspan="9">'.$details.'</td>
										</tr>';
									}
								}
							}
						}
						else
						{
							$xml .= '<tr><td class="bg_2" colspan="9">Brak zamówień spełniających kryteria</td></tr>';
						}
						
						$xml .= '</table>';
					}
					
					if (empty($params['param2']) || in_array($params['param2'], array('u_kierownika', 'akceptuj','odrzuc','odebralem')))
					{
						$list = $orders->getOrders(2);
						$orders_count = count($list);
						
						$xml .= '<p class="query_title"><strong>U kierownika</strong></p>
						<table class="proteges_list">
						<tr>
							<th style="width:65px">Nr zam.</th>
							<th style="width:65px">Wg. zapyt.</th>
							<th style="width:50px">Wydz.</th>
							<th style="width:150px">Zamawiający</th>
							<th style="width:150px">Kierownik</th>
							<th style="width:100px">Data zam.</th>
							<th style="width:100px">Wartość netto</th>
							<th style="width:50px">Czeka</th>
							<th>Szczegóły</th>
						</tr>';
						
						if ($orders_count)
						{
							$counter = 0;
							
							foreach ($list as $key => $row)
							{
								if ($_SESSION["b2b_user"]['observer'] == 1 || $_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['contractor'], $row['analyst'], $row['analyst_deputy'], $row['manager'], $row['manager_deputy'], $row['receiving'], $row['receiving_deputy']))))
								{
									if ($row['id'] != $list[$key-1]['id'])
									{
										$counter++;
										
										$details = '
										<table cellspacing="0" cellpadding="0" style="width:100%;">
										<tr>
											<th><strong>Nazwa produktu</strong></td>
											<th><strong>Symbol</strong></th>
											<th><strong>Producent</strong></th>
											<th><strong>Jedn. miary</strong></th>
											<th><strong>Ilość</strong></td>
											<th><strong>Cena netto</strong></td>
											<th><strong>Wartość netto</strong></td>
											<th><strong>Wartość brutto</strong></td>
											<th><strong title="komentarz handlowca">Kom. handl.</strong></th>
										</tr>';
										
										$weight = 0;
										$pallets = 0;
										$netto_sum  = 0;
										$tax_sum = 0;
										
										if ($row['payment'] == 'za pobraniem ( + 6zł)')
										{
											$payment = 1;
										}
										else 
										{
											$payment = 0;
										}
									}
									
									$tax = ((int)$row['tax']*$row['netto_price'])/100;
									$product_tax = sprintf("%0.2f", $tax*$row['amount']);
									$netto_value = sprintf("%0.2f", ($row['netto_price'] * $row['amount']));
									$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
									
									$details .= '<tr>';
									$details .= '<td>'.$row['name'].'</td>';
									$details .= '<td>'.$row['symbol'].'</td>';
									$details .= '<td>'.$row['producer_name'].'</td>';
									$details .= '<td>';
									
									if ((int)$row['unit'])
									{
										switch ($row['unit'])
										{
											case 1: $details .= 'kg'; break;
											case 2: $details .= 'komplet'; break;
											case 3: $details .= 'metr kwadr.'; break;
											case 4: $details .= 'metr sześć.'; break;
											case 5: $details .= 'metr bierz.'; break;
											case 6: $details .= 'opakowanie'; break;
											case 7: $details .= 'para'; break;
											case 8: $details .= 'sztuka'; break;
										}
									}
									else
									{
										switch ($row['default_unit'])
										{
											case 'SZT': $details .= 'sztuka'; break;
											case 'KPL': $details .= 'komplet'; break;
											case 'KG': $details .= 'kg'; break;
											case 'OP': $details .= 'opakowanie'; break;
											case 'M2': $details .= 'metr kwadr.'; break;
											case 'MB': $details .= 'metr bierz.'; break;
											case 'M3': $details .= 'metr sześć.'; break;
											case 'PAR': $details .= 'para'; break;
											default: $details .= '&nbsp;'; break;
										}
									}
									
									$details .= '</td>';
									
									if ($_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['manager'], $row['manager_deputy']))))
									{
										$details .= '<td><span class="amount_input"><input type="text" name="amount" alt="'.$row['id'].'#'.$row['detail_id'].'" value="'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'" /></span></td>';
									}
									else
									{
										$details .= '<td>'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'</td>';
									}
									
									$details .= '<td>'.$row['netto_price'].' zł</td>';
									$details .= '<td><span class="netto_value">'.$netto_value.'</span> zł</td>';
									$details .= '<td><span class="brutto_value">'.$brutto_value.'</span> zł</td>';
									$details .= '<td>'.((!empty($row['details'])) ? '<img class="info_change" src="images/informations_icon.png" alt="" id="detail_'.$row['detail_id'].'" />' : '&nbsp;').'</td>';
									$details .= '</tr>';
									
									$weight += $row['amount'] * $row['weight'];
										
									if ($row['pallet'] == 1)
									{
										$pallets ++;
									}
										
									$netto_sum += $netto_value;
									$tax_sum += $product_tax; 
									
									if ($row['id'] != $list[$key+1]['id'])
									{
										$details .= '
										</table>
										<div class="order_informations" style="border:1px solid #86b1d4; padding:5px; margin:5px 0px 5px 0px">
											<div class="path">
												<div class="element">
													<p><span>Zamawiający</span></p>
													<p>'.$user_names[$row['contractor_login']].'</p>
													<p>'.$row['contractor_email'].'</p>
													<p><strong>reakcja:</strong> '.$row['contractor_date'].'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] == 0)
												{
													$details .= '<div class="element">
														<p><span>UNIMET - propozyjcja zamiennika</span></p>
														<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
														<p>'.$row['unimet_email'].'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
										
												$details .= '<div class="element">
													<p><span>Analityk</span></p>
													<p>'.$user_names[$row['analyst_login']].'</p>
													<p>'.$row['analyst_email'].'</p>
													<p><strong>otrzymano:</strong> '.$row['contractor_date'].'</p>
													<p>'.(($row['analyst_date'] != "1970-01-01") ? '<strong>reakcja:</strong> '.$row['analyst_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] > 0)
												{
													$details .= '<div class="element">
														<p><span>Administrator</span></p>
														<p>'.$user_names[$row['administrator_login']].'</p>
														<p>'.$row['administrator_email'].'</p>
														<p>'.(($row['analyst_date'] != "1970-01-01") ? '<strong>otrzymano:</strong> '.$row['analyst_date'] : "").'</p>
														<p>'.(($row['administrator_date'] != "1970-01-01") ? '<strong>reakcja:</strong> '.$row['administrator_date'] : "").'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
												
												$details .= '<div class="element active">
													<p><span>Kierownik</span></p>
													<p>'.$user_names[$row['manager_login']].'</p>
													<p>'.$row['manager_email'].'</p>';
													
													if ($row['query_id'] > 0)
													{
														$details .= '<p>'.(($row['administrator_date'] != "1970-01-01") ? '<strong>otrzymano:</strong> '.$row['administrator_date'] : "").'</p>';
													}
													else
													{
														$details .= '<p>'.(($row['analyst_date'] != "1970-01-01") ? '<strong>otrzymano:</strong> '.$row['analyst_date'] : "").'</p>';
													}
									
												$details .= '</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element">
													<p><span>UNIMET</span></p>
													<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
													<p>'.$row['unimet_email'].'</p>
													<p>'.(($row['delivery_date'] != "1970-01-01") ? '<strong>dostawa:</strong> '.$row['delivery_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element">
													<p><span>Odbierający</span></p>
													<p>'.$user_names[$row['receiving_login']].'</p>
													<p>'.$row['receiving_email'].'</p>
													<p>'.(($row['receiving_date'] != "1970-01-01") ? $row['receiving_date'] : "").'</p>
												</div>
												<div class="clear"></div>
											</div>
											<div class="info">';
										
											$details .= '<p><strong>Wydział:</strong> '.$row['department'].'</p>';
											$details .= '<p><strong>Numer MPK:</strong> '.$row['mpk'].'</p>';
											
											if ($row['query_id'] > 0)
											{
												$details .= '<p><strong>Według zapytania nr:</strong> '.$orders->formatId($row['query_id'], $row['query_id']).'</p>';
											}
											
											if ((int)$row['id'] == $list[$key-1]['id'])
											{
												$details .= '<p><strong>Wartość zamówienia netto:</strong> '.$row['netto_value'].' zł</p>';
											}
											
											$details .= '<p><strong>Data zamówienia:</strong> '.$row['add_date'].'</p>';
										
											if (!in_array($row['status'], array(6,7)))
											{
												$details .= '<p><strong>Opiekun handlowy:</strong> '.((!empty($row['unimet_representative'])) ? $row['unimet_representative'] : 'brak danych').'</p>
												<p><strong>Faktura VAT:</strong> '.((!empty($row['fv'])) ? $row['fv'] : 'brak danych').'</p>
												<p><strong>WZ:</strong> '.((!empty($row['wm'])) ? $row['wm'] : 'brak danych').' '.((!empty($row['wz_document'])) ? '(<a target="blank" href="'.BASE_ADDRESS.'../images/wz/'.$row['wz_document'].'" style="color:#044996">'.$row['wz_document'].'</a>)' : '').'</p>';
											}
										
											if ($validators->validateTextarea($row['order_comment']))
											{
												$details .= '<p><strong>Komentarz zamawiającego:</strong></p>
												<p>'.nl2br($row['order_comment']).'</p>';
											}
											
											if ($validators->validateTextarea($row['merchant_information']))
											{
												$details .= '<p><strong>Komentarz handlowca:</strong></p>
												<p>'.nl2br($row['merchant_information']).'</p>';
											}
											
											$details .= '</div>
										</div>';
										
										$transport_brutto = sprintf("%0.2f", $prices->getTransportPrice((($_SESSION["b2b_user"]['transport'] == '') ? $weight : 0), $row['brutto_value'], $pallets, $payment));
										$transport_netto  = sprintf("%0.2f", (100*$transport_brutto/122));
										$transport_tax = sprintf("%0.2f", (22*($transport_netto)/100));
									
										$total_sum = sprintf("%0.2f", ($row['brutto_value'] + $transport_brutto));

										$year_postfix = ((date('Y', strtotime($row['add_date'])) == date('Y')) ? '' : '_'. date('Y', strtotime($row['add_date'])));
										
										$xml .= '<tr>
											<td class="bg_'.(($counter%2)+1).'" '.(($counter%2)+1).'" '.(($row['date_diff'] >= $limits['days']) ? 'style="background:#fdaaaa"' : '').'>'.$orders->formatId($row['id'], $row['query_id']).'</td>
											<td class="bg_'.(($counter%2)+1).'">'.((!empty($row['query_id'])) ? $orders->formatId($row['query_id'], $row['query_id']) : '&nbsp;').'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['department'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['contractor_login'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['manager_login'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.date('Y-m-d', strtotime($row['add_date'])).'</td>
											<td class="bg_'.(($counter%2)+1).'"><span class="netto_value" alt="'.$row['id'].'">'.$row['netto_value'].'</span> zł</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['date_diff'].'</td>
											<td class="bg_'.(($counter%2)+1).'" style="text-align:center;">';
											
											if ($_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['manager'], $row['manager_deputy'])))) 
											{
												$xml .= '<span style="padding:0px 5px 0px 5px; display:'.(($row['budget_value' . $year_postfix] >= $row['netto_value']) ? "inline" : "none").'" ><a class="accept_archive_order" href="./zamowienia,akceptuj,'.$row['id'].'.html">akceptuj</a></span>';
											}
											
											$xml .= '<span style="padding:0px 5px 0px 5px;"><a href="#" class="see_details" id="see_'.$row['id'].'">szczegóły</a></span>';
											
											if ($_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['manager'], $row['manager_deputy'])))) 
											{
												$xml .= '<span style="padding:0px 5px 0px 5px;"><a target="blank" alt="'.$row['id'].'" href="./include/archive_order.php?id='.$row['id'].'">wydruk</a></span>
												<strong style="padding:0px 5px 0px 5px; display:'.(($row['budget_value' . $year_postfix] >= $row['netto_value']) ? "none" : "inline").'" class="exceeded_budget">przekroczono budżet</strong>
												<span style="padding:0px 5px 0px 5px;"><a class="reject_order" alt="'.$row['id'].'" href="./zamowienia,odrzuc,'.$row['id'].'.html">odrzuć</a></span>';
											}
											
											$xml .= '</td>
										</tr>
										<tr class="order_details" id="details_'.$row['id'].'" style="display:none;">
										<td colspan="9">'.$details.'</td>
										</tr>';
									}
								}
							}
						}
						else
						{
							$xml .= '<tr><td class="bg_2" colspan="9">Brak zamówień spełniających kryteria</td></tr>';
						}
						
						$xml .= '</table>';
					}
					
					if (empty($params['param2']) || in_array($params['param2'], array('w_unimet', 'akceptuj','odrzuc','odebralem')))
					{
						$list = $orders->getOrders(3);
						$orders_count = count($list);
						
						$xml .= '<p class="query_title"><strong>Złożone w Unimet</strong></p>
						<table class="proteges_list">
						<tr>
							<th style="width:65px">Nr zam.</th>
							<th style="width:65px">Wg. zapyt.</th>
							<th style="width:50px">Wydz.</th>
							<th style="width:150px">Zamawiający</th>
							<th style="width:150px">Data zam.</th>
							<th style="width:100px">Data dostawy</th>
							<th style="width:100px">Wartość netto</th>
							<th style="width:50px">Czeka</th>
							<th>Szczegóły</th>
						</tr>';
						
						if ($orders_count)
						{
							$counter = 0;
							
							foreach ($list as $key => $row)
							{
								if ($_SESSION["b2b_user"]['observer'] == 1 || $_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['contractor'], $row['analyst'], $row['analyst_deputy'], $row['manager'], $row['manager_deputy'], $row['receiving'], $row['receiving_deputy']))))
								{
									if ($row['id'] != $list[$key-1]['id'])
									{
										$counter++;
										
										$details = '
										<table cellspacing="0" cellpadding="0" style="width:100%;">
										<tr>
											<th><strong>Nazwa produktu</strong></td>
											<th><strong>Symbol</strong></th>
											<th><strong>Producent</strong></th>
											<th><strong>Jedn. miary</strong></th>
											<th><strong>Ilość</strong></td>
											<th><strong>Cena netto</strong></td>
											<th><strong>Wartość netto</strong></td>
											<th><strong>Wartość brutto</strong></td>
											<th><strong title="komentarz handlowca">Kom. handl.</strong></th>
										</tr>';
										
										$weight = 0;
										$pallets = 0;
										$netto_sum  = 0;
										$tax_sum = 0;
										
										if ($row['payment'] == 'za pobraniem ( + 6zł)')
										{
											$payment = 1;
										}
										else 
										{
											$payment = 0;
										}
									}
									
									$tax = ((int)$row['tax']*$row['netto_price'])/100;
									$product_tax = sprintf("%0.2f", $tax*$row['amount']);
									$netto_value = sprintf("%0.2f", ($row['netto_price'] * $row['amount']));
									$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
									
									$details .= '<tr>';
									$details .= '<td>'.$row['name'].'</td>';
									$details .= '<td>'.$row['symbol'].'</td>';
									$details .= '<td>'.$row['producer_name'].'</td>';
									$details .= '<td>';
									
									if ((int)$row['unit'])
									{
										switch ($row['unit'])
										{
											case 1: $details .= 'kg'; break;
											case 2: $details .= 'komplet'; break;
											case 3: $details .= 'metr kwadr.'; break;
											case 4: $details .= 'metr sześć.'; break;
											case 5: $details .= 'metr bierz.'; break;
											case 6: $details .= 'opakowanie'; break;
											case 7: $details .= 'para'; break;
											case 8: $details .= 'sztuka'; break;
										}
									}
									else
									{
										switch ($row['default_unit'])
										{
											case 'SZT': $details .= 'sztuka'; break;
											case 'KPL': $details .= 'komplet'; break;
											case 'KG': $details .= 'kg'; break;
											case 'OP': $details .= 'opakowanie'; break;
											case 'M2': $details .= 'metr kwadr.'; break;
											case 'MB': $details .= 'metr bierz.'; break;
											case 'M3': $details .= 'metr sześć.'; break;
											case 'PAR': $details .= 'para'; break;
											default: $details .= '&nbsp;'; break;
										}
									}
									
									$details .= '</td>';
									
									$details .= '<td>'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'</td>';
									$details .= '<td>'.$row['netto_price'].' zł</td>';
									$details .= '<td>'.$netto_value.' zł</td>';
									$details .= '<td>'.$brutto_value.' zł</td>';
									$details .= '<td>'.((!empty($row['details'])) ? '<img class="info_change" src="images/informations_icon.png" alt="" id="detail_'.$row['detail_id'].'" />' : '&nbsp;').'</td>';
									$details .= '</tr>';
									
									$weight += $row['amount'] * $row['weight'];
										
									if ($row['pallet'] == 1)
									{
										$pallets ++;
									}
										
									$netto_sum += $netto_value;
									$tax_sum += $product_tax; 
									
									if ($row['id'] != $list[$key+1]['id'])
									{
										$details .= '</table>
										<div class="order_informations" style="border:1px solid #86b1d4; padding:5px; margin:5px 0px 5px 0px">
											<div class="path">
												<div class="element">
													<p><span>Zamawiający</span></p>
													<p>'.$user_names[$row['contractor_login']].'</p>
													<p>'.$row['contractor_email'].'</p>
													<p><strong>reakcja:</strong> '.$row['contractor_date'].'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] == 0)
												{
													$details .= '<div class="element">
														<p><span>UNIMET - propozyjcja zamiennika</span></p>
														<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
														<p>'.$row['unimet_email'].'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
										
												$details .= '<div class="element">
													<p><span>Analityk</span></p>
													<p>'.$user_names[$row['analyst_login']].'</p>
													<p>'.$row['analyst_email'].'</p>
													<p><strong>otrzymano:</strong> '.$row['contractor_date'].'</p>
													<p>'.(($row['analyst_date'] != "1970-01-01") ? '<strong>reakcja:</strong> '.$row['analyst_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] > 0)
												{
													$details .= '<div class="element">
														<p><span>Administrator</span></p>
														<p>'.$user_names[$row['administrator_login']].'</p>
														<p>'.$row['administrator_email'].'</p>
														<p>'.(($row['analyst_date'] != "1970-01-01") ? '<strong>otrzymano:</strong> '.$row['analyst_date'] : "").'</p>
														<p>'.(($row['administrator_date'] != "1970-01-01") ? '<strong>reakcja:</strong> '.$row['administrator_date'] : "").'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
												
												$details .= '<div class="element">
													<p><span>Kierownik</span></p>
													<p>'.$user_names[$row['manager_login']].'</p>
													<p>'.$row['manager_email'].'</p>';
												
													if ($row['query_id'] > 0)
													{
														$details .= '<p>'.(($row['administrator_date'] != "1970-01-01") ? '<strong>otrzymano:</strong> '.$row['administrator_date'] : "").'</p>';
													}
													else
													{
														$details .= '<p>'.(($row['analyst_date'] != "1970-01-01") ? '<strong>otrzymano:</strong> '.$row['analyst_date'] : "").'</p>';
													}
												
												$details .= '<p>'.(($row['manager_date'] != "1970-01-01") ? '<strong>reakcja:</strong> '.$row['manager_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element active">
													<p><span>UNIMET</span></p>
													<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
													<p>'.$row['unimet_email'].'</p>
													<p>'.(($row['delivery_date'] != "1970-01-01") ? '<strong>dostawa:</strong> '.$row['delivery_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element">
													<p><span>Odbierający</span></p>
													<p>'.$user_names[$row['receiving_login']].'</p>
													<p>'.$row['receiving_email'].'</p>
													<p>'.(($row['receiving_date'] != "1970-01-01") ? '<strong>reakcja:</strong> '.$row['receiving_date'] : "").'</p>
												</div>
												<div class="clear"></div>
											</div>
											<div class="info">';
										
											$details .= '<p><strong>Wydział:</strong> '.$row['department'].'</p>';
											$details .= '<p><strong>Numer MPK:</strong> '.$row['mpk'].'</p>';
											
											if ($row['query_id'] > 0)
											{
												$details .= '<p><strong>Według zapytania nr:</strong> '.$orders->formatId($row['query_id'], $row['query_id']).'</p>';
											}
											
											if ((int)$row['id'] == $list[$key-1]['id'])
											{
												$details .= '<p><strong>Wartość zamówienia netto:</strong> '.$row['netto_value'].' zł</p>';
											}
											
											$details .= '<p><strong>Data zamówienia:</strong> '.$row['add_date'].'</p>';
										
											if (!in_array($row['status'], array(6,7)))
											{
												$details .= '<p><strong>Data dostawy:</strong> '.(($row['delivery_date'] != "1970-01-01" && !empty($row['delivery_date'])) ? $row['delivery_date'] : "brak danych").'</p>';
												$details .= '<p><strong>Opiekun handlowy:</strong> '.((!empty($row['unimet_representative'])) ? $row['unimet_representative'] : 'brak danych').'</p>
												<p><strong>Faktura VAT:</strong> '.((!empty($row['fv'])) ? $row['fv'] : 'brak danych').'</p>
												<p><strong>WZ:</strong> '.((!empty($row['wm'])) ? $row['wm'] : 'brak danych').' '.((!empty($row['wz_document'])) ? '(<a target="blank" href="'.BASE_ADDRESS.'../images/wz/'.$row['wz_document'].'" style="color:#044996">'.$row['wz_document'].'</a>)' : '').'</p>';
											}
										
											if ($validators->validateTextarea($row['order_comment']))
											{
												$details .= '<p><strong>Komentarz zamawiającego:</strong></p>
												<p>'.nl2br($row['order_comment']).'</p>';
											}
											
											if ($validators->validateTextarea($row['merchant_information']))
											{
												$details .= '<p><strong>Komentarz handlowca:</strong></p>
												<p>'.nl2br($row['merchant_information']).'</p>';
											}
											
											$details .= '</div>
										</div>';
										
										$transport_brutto = sprintf("%0.2f", $prices->getTransportPrice((($_SESSION["b2b_user"]['transport'] == '') ? $weight : 0), $row['brutto_value'], $pallets, $payment));
										$transport_netto  = sprintf("%0.2f", (100*$transport_brutto/122));
										$transport_tax = sprintf("%0.2f", (22*($transport_netto)/100));
									
										$total_sum = sprintf("%0.2f", ($row['brutto_value'] + $transport_brutto));
										
										$xml .= '<tr>
											<td class="bg_'.(($counter%2)+1).'" '.(($row['date_diff'] >= $limits['days']) ? 'style="background:#fdaaaa"' : '').'>'.$orders->formatId($row['id'], $row['query_id']).'</td>
											<td class="bg_'.(($counter%2)+1).'">'.((!empty($row['query_id'])) ? $orders->formatId($row['query_id'], $row['query_id']) : '&nbsp;').'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['department'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['contractor_login'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.date('Y-m-d', strtotime($row['add_date'])).'</td>
											<td class="bg_'.(($counter%2)+1).'">'.(($row['delivery_date'] != "1970-01-01") ? $row['delivery_date'] : "").'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['netto_value'].' zł</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['date_diff'].'</td>
											<td class="bg_'.(($counter%2)+1).'">
											<span style="padding:0px 5px 0px 5px;"><a href="#" class="see_details" id="see_'.$row['id'].'">szczegóły</a></span>
											<span style="padding:0px 5px 0px 5px;"><a target="blank" alt="'.$row['id'].'" href="./include/archive_order.php?id='.$row['id'].'">wydruk</a></span>  
											</td>
										</tr>
										<tr class="order_details" id="details_'.$row['id'].'" style="display:none;">
										<td colspan="9">'.$details.'</td>
										</tr>';
									}
								}
							}
						}
						else
						{
							$xml .= '<tr><td class="bg_2" colspan="9">Brak zamówień spełniających kryteria</td></tr>';
						}
						
						$xml .= '</table>';
					}
					
					if (empty($params['param2']) || in_array($params['param2'], array('w_drodze', 'akceptuj','odrzuc','odebralem')))
					{
						$list = $orders->getOrders(4);
						$orders_count = count($list);
						
						$xml .= '<p class="query_title"><strong>W drodze</strong></p>
						<table class="proteges_list">
						<tr>
							<th style="width:65px">Nr zam.</th>
							<th style="width:65px">Wg. zapyt.</th>
							<th style="width:50px">Wydz.</th>
							<th style="width:150px">Zamawiający</th>
							<th style="width:150px">Odbiorca</th>
							<th style="width:100px">Data dostawy</th>
							<th style="width:100px">Wartość netto</th>
							<th style="width:50px">Czeka</th>
							<th>Szczegóły</th>
						</tr>';
						
						if ($orders_count)
						{
							$counter = 0;
							
							foreach ($list as $key => $row)
							{
								if ($_SESSION["b2b_user"]['observer'] == 1 || $_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['contractor'], $row['analyst'], $row['analyst_deputy'], $row['manager'], $row['manager_deputy'], $row['receiving'], $row['receiving_deputy']))))
								{
									if ($row['id'] != $list[$key-1]['id'])
									{
										$counter++;
										
										$details = '
										<table cellspacing="0" cellpadding="0" style="width:100%;">
										<tr>
											<th><strong>Nazwa produktu</strong></td>
											<th><strong>Symbol</strong></th>
											<th><strong>Producent</strong></th>
											<th><strong>Jedn. miary</strong></th>
											<th><strong>Ilość</strong></td>
											<th><strong>Cena netto</strong></td>
											<th><strong>Wartość netto</strong></td>
											<th><strong>Wartość brutto</strong></td>
											<th><strong title="komentarz handlowca">Kom. handl.</strong></th>
										</tr>';
										
										$weight = 0;
										$pallets = 0;
										$netto_sum  = 0;
										$tax_sum = 0;
										
										if ($row['payment'] == 'za pobraniem ( + 6zł)')
										{
											$payment = 1;
										}
										else 
										{
											$payment = 0;
										}
									}
									
									$tax = ((int)$row['tax']*$row['netto_price'])/100;
									$product_tax = sprintf("%0.2f", $tax*$row['amount']);
									$netto_value = sprintf("%0.2f", ($row['netto_price'] * $row['amount']));
									$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
									
									$details .= '<tr>';
									$details .= '<td>'.$row['name'].'</td>';
									$details .= '<td>'.$row['symbol'].'</td>';
									$details .= '<td>'.$row['producer_name'].'</td>';
									$details .= '<td>';
									
									if ((int)$row['unit'])
									{
										switch ($row['unit'])
										{
											case 1: $details .= 'kg'; break;
											case 2: $details .= 'komplet'; break;
											case 3: $details .= 'metr kwadr.'; break;
											case 4: $details .= 'metr sześć.'; break;
											case 5: $details .= 'metr bierz.'; break;
											case 6: $details .= 'opakowanie'; break;
											case 7: $details .= 'para'; break;
											case 8: $details .= 'sztuka'; break;
										}
									}
									else
									{
										switch ($row['default_unit'])
										{
											case 'SZT': $details .= 'sztuka'; break;
											case 'KPL': $details .= 'komplet'; break;
											case 'KG': $details .= 'kg'; break;
											case 'OP': $details .= 'opakowanie'; break;
											case 'M2': $details .= 'metr kwadr.'; break;
											case 'MB': $details .= 'metr bierz.'; break;
											case 'M3': $details .= 'metr sześć.'; break;
											case 'PAR': $details .= 'para'; break;
											default: $details .= '&nbsp;'; break;
										}
									}
									
									$details .= '</td>';
									
									$details .= '<td>'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'</td>';
									$details .= '<td>'.$row['netto_price'].' zł</td>';
									$details .= '<td>'.$netto_value.' zł</td>';
									$details .= '<td>'.$brutto_value.' zł</td>';
									$details .= '<td>'.((!empty($row['details'])) ? '<img class="info_change" src="images/informations_icon.png" alt="" id="detail_'.$row['detail_id'].'" />' : '&nbsp;').'</td>';
									$details .= '</tr>';
									
									$weight += $row['amount'] * $row['weight'];
										
									if ($row['pallet'] == 1)
									{
										$pallets ++;
									}
										
									$netto_sum += $netto_value;
									$tax_sum += $product_tax; 
									
									if ($row['id'] != $list[$key+1]['id'])
									{
										$details .= '</table>
										<div class="order_informations" style="border:1px solid #86b1d4; padding:5px; margin:5px 0px 5px 0px">
											<div class="path">
												<div class="element">
													<p><span>Zamawiający</span></p>
													<p>'.$user_names[$row['contractor_login']].'</p>
													<p>'.$row['contractor_email'].'</p>
													<p><strong>reakcja:</strong> '.$row['contractor_date'].'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] == 0)
												{
													$details .= '<div class="element">
														<p><span>UNIMET - propozyjcja zamiennika</span></p>
														<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
														<p>'.$row['unimet_email'].'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
										
												$details .= '<div class="element">
													<p><span>Analityk</span></p>
													<p>'.$user_names[$row['analyst_login']].'</p>
													<p>'.$row['analyst_email'].'</p>
													<p><strong>otrzymano:</strong> '.$row['contractor_date'].'</p>
													<p>'.(($row['analyst_date'] != "1970-01-01") ? '<strong>reakcja:</strong> '.$row['analyst_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] > 0)
												{
													$details .= '<div class="element">
														<p><span>Administrator</span></p>
														<p>'.$user_names[$row['administrator_login']].'</p>
														<p>'.$row['administrator_email'].'</p>
														<p>'.(($row['analyst_date'] != "1970-01-01") ? '<strong>otrzymano:</strong> '.$row['analyst_date'] : "").'</p>
														<p>'.(($row['administrator_date'] != "1970-01-01") ? '<strong>reakcja:</strong> '.$row['administrator_date'] : "").'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
												
												$details .= '<div class="element">
													<p><span>Kierownik</span></p>
													<p>'.$user_names[$row['manager_login']].'</p>
													<p>'.$row['manager_email'].'</p>';
												
													if ($row['query_id'] > 0)
													{
														$details .= '<p>'.(($row['administrator_date'] != "1970-01-01") ? '<strong>otrzymano:</strong> '.$row['administrator_date'] : "").'</p>';
													}
													else
													{
														$details .= '<p>'.(($row['analyst_date'] != "1970-01-01") ? '<strong>otrzymano:</strong> '.$row['analyst_date'] : "").'</p>';
													}
												
												$details .= '<p>'.(($row['manager_date'] != "1970-01-01") ? '<strong>reakcja:</strong> '.$row['manager_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element">
													<p><span>UNIMET</span></p>
													<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
													<p>'.$row['unimet_email'].'</p>
													<p>'.(($row['delivery_date'] != "1970-01-01") ? '<strong>dostawa:</strong> '.$row['delivery_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element active">
													<p><span>Odbierający</span></p>
													<p>'.$user_names[$row['receiving_login']].'</p>
													<p>'.$row['receiving_email'].'</p>
													<p>'.(($row['receiving_date'] != "1970-01-01") ? '<strong>reakcja:</strong> '.$row['receiving_date'] : "").'</p>
												</div>
												<div class="clear"></div>
											</div>
											<div class="info">';
										
											$details .= '<p><strong>Wydział:</strong> '.$row['department'].'</p>';
											$details .= '<p><strong>Numer MPK:</strong> '.$row['mpk'].'</p>';
											
											if ($row['query_id'] > 0)
											{
												$details .= '<p><strong>Według zapytania nr:</strong> '.$orders->formatId($row['query_id'], $row['query_id']).'</p>';
											}
											
											if ((int)$row['id'] == $list[$key-1]['id'])
											{
												$details .= '<p><strong>Wartość zamówienia netto:</strong> '.$row['netto_value'].' zł</p>';
											}
											
											$details .= '<p><strong>Data zamówienia:</strong> '.$row['add_date'].'</p>';
										
											if (!in_array($row['status'], array(6,7)))
											{
												$details .= '<p><strong>Data dostawy:</strong> '.(($row['delivery_date'] != "1970-01-01" && !empty($row['delivery_date'])) ? $row['delivery_date'] : "brak danych").'</p>';
												$details .= '<p><strong>Opiekun handlowy:</strong> '.((!empty($row['unimet_representative'])) ? $row['unimet_representative'] : 'brak danych').'</p>
												<p><strong>Faktura VAT:</strong> '.((!empty($row['fv'])) ? $row['fv'] : 'brak danych').'</p>
												<p><strong>WZ:</strong> '.((!empty($row['wm'])) ? $row['wm'] : 'brak danych').' '.((!empty($row['wz_document'])) ? '(<a target="blank" href="'.BASE_ADDRESS.'../images/wz/'.$row['wz_document'].'" style="color:#044996">'.$row['wz_document'].'</a>)' : '').'</p>';
											}
										
											if ($validators->validateTextarea($row['order_comment']))
											{
												$details .= '<p><strong>Komentarz zamawiającego:</strong></p>
												<p>'.nl2br($row['order_comment']).'</p>';
											}
											
											if ($validators->validateTextarea($row['merchant_information']))
											{
												$details .= '<p><strong>Komentarz handlowca:</strong></p>
												<p>'.nl2br($row['merchant_information']).'</p>';
											}
											
											$details .= '</div>
										</div>';
										
										$transport_brutto = sprintf("%0.2f", $prices->getTransportPrice((($_SESSION["b2b_user"]['transport'] == '') ? $weight : 0), $row['brutto_value'], $pallets, $payment));
										$transport_netto  = sprintf("%0.2f", (100*$transport_brutto/122));
										$transport_tax = sprintf("%0.2f", (22*($transport_netto)/100));
									
										$total_sum = sprintf("%0.2f", ($row['brutto_value'] + $transport_brutto));
										
										$xml .= '<tr>
											<td class="bg_'.(($counter%2)+1).'" '.(($row['date_diff'] >= $limits['days']) ? 'style="background:#fdaaaa"' : '').'>'.$orders->formatId($row['id'], $row['query_id']).'</td>
											<td class="bg_'.(($counter%2)+1).'">'.((!empty($row['query_id'])) ? $orders->formatId($row['query_id'], $row['query_id']) : '&nbsp;').'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['department'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['contractor_login'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['receiving_login'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.(($row['delivery_date'] != "1970-01-01") ? $row['delivery_date'] : "").'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['netto_value'].' zł</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['date_diff'].'</td>
											<td class="bg_'.(($counter%2)+1).'">
											<span style="padding:0px 5px 0px 5px;"><a href="#" class="see_details" id="see_'.$row['id'].'">szczegóły</a></span>
											<span style="padding:0px 5px 0px 5px;"><a target="blank" alt="'.$row['id'].'" href="./include/archive_order.php?id='.$row['id'].'">wydruk</a></span>';
											if ($_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['receiving'], $row['receiving_deputy'])))) {
											$xml .= '<span style="padding:0px 5px 0px 5px;"><a href="./zamowienia,odebralem,'.$row['id'].'.html">odebrałem/am</a></span>';
											}
											
											$xml .= '</td>
										</tr>
										<tr class="order_details" id="details_'.$row['id'].'" style="display:none;">
										<td colspan="9">'.$details.'</td>
										</tr>';
									}
								}
							}
						}
						else
						{
							$xml .= '<tr><td class="bg_2" colspan="9">Brak zamówień spełniających kryteria</td></tr>';
						}
						
						$xml .= '</table>';
					}
					
					if ($orders->getStatus((int)$params['param3']) == 6 || in_array($params['param2'], array('odrzucone')))
					{
						$list = $orders->getOrders(6);
						$orders_count = count($list);
						
						$xml .= '<p class="query_title"><strong>Odrzucone</strong></p>
						<table class="proteges_list">
						<tr>
							<th style="width:65px">Nr zam.</th>
							<th style="width:65px">Wg. zapyt.</th>
							<th style="width:50px">Wydz.</th>
							<th style="width:150px">Zamawiający</th>
							<th style="width:150px">Data zam.</th>
							<th style="width:100px">Data dostawy</th>
							<th style="width:100px">Wartość netto</th>
							<th style="width:50px">Czeka</th>
							<th>Szczegóły</th>
						</tr>';
						
						if ($orders_count)
						{
							$counter = 0;
							
							foreach ($list as $key => $row)
							{
								if ($_SESSION["b2b_user"]['observer'] == 1 || $_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['contractor'], $row['analyst'], $row['analyst_deputy'], $row['manager'], $row['manager_deputy'], $row['receiving'], $row['receiving_deputy']))))
								{
									if ($row['id'] != $list[$key-1]['id'])
									{
										$counter++;
										
										$details = '
										<table cellspacing="0" cellpadding="0" style="width:100%;">
										<tr>
											<th><strong>Nazwa produktu</strong></td>
											<th><strong>Symbol</strong></th>
											<th><strong>Producent</strong></th>
											<th><strong>Jedn. miary</strong></th>
											<th><strong>Ilość</strong></td>
											<th><strong>Cena netto</strong></td>
											<th><strong>Wartość netto</strong></td>
											<th><strong>Wartość brutto</strong></td>
											<th><strong title="komentarz handlowca">Kom. handl.</strong></th>
										</tr>';
										
										$weight = 0;
										$pallets = 0;
										$netto_sum  = 0;
										$tax_sum = 0;
										
										if ($row['payment'] == 'za pobraniem ( + 6zł)')
										{
											$payment = 1;
										}
										else 
										{
											$payment = 0;
										}
									}
									
									$tax = ((int)$row['tax']*$row['netto_price'])/100;
									$product_tax = sprintf("%0.2f", $tax*$row['amount']);
									$netto_value = sprintf("%0.2f", ($row['netto_price'] * $row['amount']));
									$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
									
									$details .= '<tr>';
									$details .= '<td>'.$row['name'].'</td>';
									$details .= '<td>'.$row['symbol'].'</td>';
									$details .= '<td>'.$row['producer_name'].'</td>';
									$details .= '<td>';
									
									if ((int)$row['unit'])
									{
										switch ($row['unit'])
										{
											case 1: $details .= 'kg'; break;
											case 2: $details .= 'komplet'; break;
											case 3: $details .= 'metr kwadr.'; break;
											case 4: $details .= 'metr sześć.'; break;
											case 5: $details .= 'metr bierz.'; break;
											case 6: $details .= 'opakowanie'; break;
											case 7: $details .= 'para'; break;
											case 8: $details .= 'sztuka'; break;
										}
									}
									else
									{
										switch ($row['default_unit'])
										{
											case 'SZT': $details .= 'sztuka'; break;
											case 'KPL': $details .= 'komplet'; break;
											case 'KG': $details .= 'kg'; break;
											case 'OP': $details .= 'opakowanie'; break;
											case 'M2': $details .= 'metr kwadr.'; break;
											case 'MB': $details .= 'metr bierz.'; break;
											case 'M3': $details .= 'metr sześć.'; break;
											case 'PAR': $details .= 'para'; break;
											default: $details .= '&nbsp;'; break;
										}
									}
									
									$details .= '</td>';
									
									$details .= '<td>'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'</td>';
									$details .= '<td>'.$row['netto_price'].' zł</td>';
									$details .= '<td>'.$netto_value.' zł</td>';
									$details .= '<td>'.$brutto_value.' zł</td>';
									$details .= '<td>'.((!empty($row['details'])) ? '<img class="info_change" src="images/informations_icon.png" alt="" id="detail_'.$row['detail_id'].'" />' : '&nbsp;').'</td>';
									$details .= '</tr>';
									
									$weight += $row['amount'] * $row['weight'];
										
									if ($row['pallet'] == 1)
									{
										$pallets ++;
									}
										
									$netto_sum += $netto_value;
									$tax_sum += $product_tax; 
									
									if ($row['id'] != $list[$key+1]['id'])
									{
										$details .= '</table>
										<div class="order_informations" style="border:1px solid #86b1d4; padding:5px; margin:5px 0px 5px 0px">
											<div class="path">
												<div class="element">
													<p><span>Zamawiający</span></p>
													<p>'.$user_names[$row['contractor_login']].'</p>
													<p>'.$row['contractor_email'].'</p>
													<p>'.$row['contractor_date'].'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
												
												if ($row['query_id'] == 0)
												{
													$details .= '<div class="element">
														<p><span>UNIMET - propozyjcja zamiennika</span></p>
														<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
														<p>'.$row['unimet_email'].'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
										
												$details .= '<div class="element">
													<p><span>Analityk</span></p>
													<p>'.$user_names[$row['analyst_login']].'</p>
													<p>'.$row['analyst_email'].'</p>
													<p>'.(($row['analyst_date'] != "1970-01-01") ? $row['analyst_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] > 0)
												{
													$details .= '<div class="element">
														<p><span>Administrator</span></p>
														<p>'.$user_names[$row['administrator_login']].'</p>
														<p>'.$row['administrator_email'].'</p>
														<p>'.(($row['administrator_date'] != "1970-01-01") ? $row['administrator_date'] : "").'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
												
												$details .= '<div class="element">
													<p><span>Kierownik</span></p>
													<p>'.$user_names[$row['manager_login']].'</p>
													<p>'.$row['manager_email'].'</p>
													<p>'.(($row['manager_date'] != "1970-01-01") ? $row['manager_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element">
													<p><span>UNIMET</span></p>
													<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
													<p>'.$row['unimet_email'].'</p>
													<p>'.(($row['delivery_date'] != "1970-01-01") ? $row['delivery_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element">
													<p><span>Odbierający</span></p>
													<p>'.$user_names[$row['receiving_login']].'</p>
													<p>'.$row['receiving_email'].'</p>
													<p>'.(($row['receiving_date'] != "1970-01-01") ? $row['receiving_date'] : "").'</p>
												</div>
												<div class="clear"></div>
											</div>
											<div class="info">';
												
											if ($validators->validateTextarea($row['reject_comment']))
											{
												$details .= '<p><strong>Powód odrzucenia:</strong></p>
												<p style="color:#BD3D37;">'.nl2br($row['reject_comment']).'</p>';
											}
										
											$details .= '<p><strong>Wydział:</strong> '.$row['department'].'</p>';
											$details .= '<p><strong>Numer MPK:</strong> '.$row['mpk'].'</p>';
											
											if ($row['query_id'] > 0)
											{
												$details .= '<p><strong>Według zapytania nr:</strong> '.$orders->formatId($row['query_id'], $row['query_id']).'</p>';
											}
											
											if ((int)$row['id'] == $list[$key-1]['id'])
											{
												$details .= '<p><strong>Wartość zamówienia netto:</strong> '.$row['netto_value'].' zł</p>';
											}
											
											$details .= '<p><strong>Data zamówienia:</strong> '.$row['add_date'].'</p>';
											
											if (!in_array($row['status'], array(6,7)))
											{
												$details .= '<p><strong>Data dostawy:</strong> '.(($row['delivery_date'] != "1970-01-01" && !empty($row['delivery_date'])) ? $row['delivery_date'] : "brak danych").'</p>';
												$details .= '<p><strong>Opiekun handlowy:</strong> '.((!empty($row['unimet_representative'])) ? $row['unimet_representative'] : 'brak danych').'</p>
												<p><strong>Faktura VAT:</strong> '.((!empty($row['fv'])) ? $row['fv'] : 'brak danych').'</p>
												<p><strong>WZ:</strong> '.((!empty($row['wm'])) ? $row['wm'] : 'brak danych').' '.((!empty($row['wz_document'])) ? '(<a target="blank" href="'.BASE_ADDRESS.'../images/wz/'.$row['wz_document'].'" style="color:#044996">'.$row['wz_document'].'</a>)' : '').'</p>';
											}
											
											if ($validators->validateTextarea($row['order_comment']))
											{
												$details .= '<p><strong>Komentarz zamawiającego:</strong></p>
												<p>'.nl2br($row['order_comment']).'</p>';
											}
											
											if ($validators->validateTextarea($row['merchant_information']))
											{
												$details .= '<p><strong>Komentarz handlowca:</strong></p>
												<p>'.nl2br($row['merchant_information']).'</p>';
											}
												
											$details .= '</div>
										</div>';
										
										$transport_brutto = sprintf("%0.2f", $prices->getTransportPrice((($_SESSION["b2b_user"]['transport'] == '') ? $weight : 0), $row['brutto_value'], $pallets, $payment));
										$transport_netto  = sprintf("%0.2f", (100*$transport_brutto/122));
										$transport_tax = sprintf("%0.2f", (22*($transport_netto)/100));
									
										$total_sum = sprintf("%0.2f", ($row['brutto_value'] + $transport_brutto));
										
										$xml .= '<tr>
											<td class="bg_'.(($counter%2)+1).'">'.$orders->formatId($row['id'], $row['query_id']).'</td>
											<td class="bg_'.(($counter%2)+1).'">'.((!empty($row['query_id'])) ? $orders->formatId($row['query_id'], $row['query_id']) : '&nbsp;').'</td>
											<td class="bg_'.(($counter%2)+1).'"'.$row['department'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['contractor_login'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.date('Y-m-d', strtotime($row['add_date'])).'</td>
											<td class="bg_'.(($counter%2)+1).'">'.(($row['delivery_date'] != "1970-01-01") ? $row['delivery_date'] : "").'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['netto_value'].' zł</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['date_diff'].'</td>
											<td class="bg_'.(($counter%2)+1).'">
											<span style="padding:0px 5px 0px 5px;"><a href="#" class="see_details" id="see_'.$row['id'].'">szczegóły</a></span>
											<span style="padding:0px 5px 0px 5px;"><a target="blank" alt="'.$row['id'].'" href="./include/archive_order.php?id='.$row['id'].'">wydruk</a></span>  
											</td>
										</tr>
										<tr class="order_details" id="details_'.$row['id'].'" style="display:none;">
										<td colspan="9">'.$details.'</td>
										</tr>';
									}
								}
							}
						}
						else
						{
							$xml .= '<tr><td class="bg_2" colspan="9">Brak zamówień spełniających kryteria</td></tr>';
						}
						
						$xml .= '</table>';
					}
					
					if ($orders->getStatus((int)$params['param3']) == 5 || in_array($params['param2'], array('archiwum')))
					{
						$year_list = array();
						$years = $orders->getOrderYears();

						foreach ((array)$years as $row)
						{
							$year_list[] = $row[0];
						}

						if (in_array((int)$params['param3'], $year_list))
						{
							$actual_year = (int)$params['param3'];
						}
						else
						{
							$actual_year = $year_list[0];
						}

						$list = $orders->getOrders(5, false, $actual_year);
						$orders_count = count($list);
						
						$xml .= '<p class="query_title">
							<span><strong>Archiwum</strong></span>';

							foreach ((array)$year_list as $year)
							{
								$xml .= '<span class="year"><a href="/zamowienia,archiwum,'.$year.'.html" ' . (($year == $actual_year) ? 'class="active"' : '') . '><strong>'.$year.'</strong></a></span>';
							}
							
							$xml .= '<span class="clear"></span>
						</p>
						<table class="proteges_list">
						<tr>
							<th style="width:65px">Nr zam.</th>
							<th style="width:65px">Wg. zapyt.</th>
							<th style="width:50px">Wydz.</th>
							<th style="width:150px">Zamawiający</th>
							<th style="width:150px">Data zam.</th>
							<th style="width:100px">Data dostawy</th>
							<th style="width:100px">Wartość netto</th>
							<th style="width:50px">Czeka</th>
							<th>Szczegóły</th>
						</tr>';
						
						if ($orders_count)
						{
							$counter = 0;
							
							foreach ($list as $key => $row)
							{
								if (($_SESSION["b2b_user"]['observer'] == 1 || $_SESSION["b2b_user"]['admin'] == 1 || (in_array($_SESSION["b2b_user"]['id'], array($row['contractor'], $row['analyst'], $row['analyst_deputy'], $row['manager'], $row['manager_deputy'], $row['receiving'], $row['receiving_deputy'])))))
								{
									if ($row['id'] != $list[$key-1]['id'])
									{
										$counter++;
										
										$details = '
										<table cellspacing="0" cellpadding="0" style="width:100%;">
										<tr>
											<th><strong>Nazwa produktu</strong></td>
											<th><strong>Symbol</strong></th>
											<th><strong>Producent</strong></th>
											<th><strong>Jedn. miary</strong></th>
											<th><strong>Ilość</strong></td>
											<th><strong>Cena netto</strong></td>
											<th><strong>Wartość netto</strong></td>
											<th><strong>Wartość brutto</strong></td>
											<th><strong title="komentarz handlowca">Kom. handl.</strong></th>
										</tr>';
										
										$weight = 0;
										$pallets = 0;
										$netto_sum  = 0;
										$tax_sum = 0;
										
										if ($row['payment'] == 'za pobraniem ( + 6zł)')
										{
											$payment = 1;
										}
										else 
										{
											$payment = 0;
										}
									}
									
									$tax = ((int)$row['tax']*$row['netto_price'])/100;
									$product_tax = sprintf("%0.2f", $tax*$row['amount']);
									$netto_value = sprintf("%0.2f", ($row['netto_price'] * $row['amount']));
									$brutto_value = sprintf("%0.2f", ($row['netto_price'] + $tax) * $row['amount']);
									
									$details .= '<tr>';
									$details .= '<td>'.$row['name'].'</td>';
									$details .= '<td>'.$row['symbol'].'</td>';
									$details .= '<td>'.$row['producer_name'].'</td>';
									$details .= '<td>';
									
									if ((int)$row['unit'])
									{
										switch ($row['unit'])
										{
											case 1: $details .= 'kg'; break;
											case 2: $details .= 'komplet'; break;
											case 3: $details .= 'metr kwadr.'; break;
											case 4: $details .= 'metr sześć.'; break;
											case 5: $details .= 'metr bierz.'; break;
											case 6: $details .= 'opakowanie'; break;
											case 7: $details .= 'para'; break;
											case 8: $details .= 'sztuka'; break;
										}
									}
									else
									{
										switch ($row['default_unit'])
										{
											case 'SZT': $details .= 'sztuka'; break;
											case 'KPL': $details .= 'komplet'; break;
											case 'KG': $details .= 'kg'; break;
											case 'OP': $details .= 'opakowanie'; break;
											case 'M2': $details .= 'metr kwadr.'; break;
											case 'MB': $details .= 'metr bierz.'; break;
											case 'M3': $details .= 'metr sześć.'; break;
											case 'PAR': $details .= 'para'; break;
											default: $details .= '&nbsp;'; break;
										}
									}
									
									$details .= '</td>';
									
									$details .= '<td>'.(((int)$row['amount'] == (float)$row['amount']) ? (int)$row['amount'] : $row['amount']).'</td>';
									$details .= '<td>'.$row['netto_price'].' zł</td>';
									$details .= '<td>'.$netto_value.' zł</td>';
									$details .= '<td>'.$brutto_value.' zł</td>';
									$details .= '<td>'.((!empty($row['details'])) ? '<img class="info_change" src="images/informations_icon.png" alt="" id="detail_'.$row['detail_id'].'" />' : '&nbsp;').'</td>';
									$details .= '</tr>';
									
									$weight += $row['amount'] * $row['weight'];
										
									if ($row['pallet'] == 1)
									{
										$pallets ++;
									}
										
									$netto_sum += $netto_value;
									$tax_sum += $product_tax; 
									
									if ($row['id'] != $list[$key+1]['id'])
									{
										$details .= '</table>
										<div class="order_informations" style="border:1px solid #86b1d4; padding:5px; margin:0px 0px 5px 0px">
											<div class="path">
												<div class="element">
													<p><span>Zamawiający</span></p>
													<p>'.$user_names[$row['contractor_login']].'</p>
													<p>'.$row['contractor_email'].'</p>
													<p>'.$row['contractor_date'].'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
										
												if ($row['query_id'] == 0)
												{
													$details .= '<div class="element">
														<p><span>UNIMET - propozyjcja zamiennika</span></p>
														<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
														<p>'.$row['unimet_email'].'</p>
													</div>
													<div class="arrow">
														<strong>&#187;</strong>
													</div>';
												}
										
												$details .= '<div class="element">
												<p><span>Analityk</span></p>
												<p>'.$user_names[$row['analyst_login']].'</p>
												<p>'.$row['analyst_email'].'</p>
												<p>'.(($row['analyst_date'] != "1970-01-01") ? $row['analyst_date'] : "").'</p>
											</div>
											<div class="arrow">
												<strong>&#187;</strong>
											</div>';
										
											if ($row['query_id'] > 0)
											{
												$details .= '<div class="element">
													<p><span>Administrator</span></p>
													<p>'.$user_names[$row['administrator_login']].'</p>
													<p>'.$row['administrator_email'].'</p>
													<p>'.(($row['administrator_date'] != "1970-01-01") ? $row['administrator_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>';
											}
											
											$details .= '<div class="element">
												<p><span>Kierownik</span></p>
												<p>'.$user_names[$row['manager_login']].'</p>
												<p>'.$row['manager_email'].'</p>
												<p>'.(($row['manager_date'] != "1970-01-01") ? $row['manager_date'] : "").'</p>
											</div>
											<div class="arrow">
												<strong>&#187;</strong>
											</div>
												<div class="element">
													<p><span>UNIMET</span></p>
													<p>'.((!empty($row['unimet_representative'])) ? $user_names[$row['unimet_representative']] : 'brak danych').'</p>
													<p>'.$row['unimet_email'].'</p>
													<p>'.(($row['delivery_date'] != "1970-01-01") ? $row['delivery_date'] : "").'</p>
												</div>
												<div class="arrow">
													<strong>&#187;</strong>
												</div>
												<div class="element">
													<p><span>Odbierający</span></p>
													<p>'.$user_names[$row['receiving_login']].'</p>
													<p>'.$row['receiving_email'].'</p>
													<p>'.(($row['receiving_date'] != "1970-01-01") ? $row['receiving_date'] : "").'</p>
												</div>
												<div class="clear"></div>
											</div>
											<div class="info">';
										
											$details .= '<p><strong>Wydział:</strong> '.$row['department'].'</p>';
											$details .= '<p><strong>Numer MPK:</strong> '.$row['mpk'].'</p>';
											
											if ($row['query_id'] > 0)
											{
												$details .= '<p><strong>Według zapytania nr:</strong> '.$orders->formatId($row['query_id'], $row['query_id']).'</p>';
											}
											
											if ((int)$row['id'] == $list[$key-1]['id'])
											{
												$details .= '<p><strong>Wartość zamówienia netto:</strong> '.$row['netto_value'].' zł</p>';
											}
											
											$details .= '<p><strong>Data zamówienia:</strong> '.$row['add_date'].'</p>';
											
											if (!in_array($row['status'], array(6,7)))
											{
												$details .= '<p><strong>Data dostawy:</strong> '.(($row['delivery_date'] != "1970-01-01" && !empty($row['delivery_date'])) ? $row['delivery_date'] : "brak danych").'</p>';
												$details .= '<p><strong>Opiekun handlowy:</strong> '.((!empty($row['unimet_representative'])) ? $row['unimet_representative'] : 'brak danych').'</p>
												<p><strong>Faktura VAT:</strong> '.((!empty($row['fv'])) ? $row['fv'] : 'brak danych').'</p>
												<p><strong>WZ:</strong> '.((!empty($row['wm'])) ? $row['wm'] : 'brak danych').' '.((!empty($row['wz_document'])) ? '(<a target="blank" href="'.BASE_ADDRESS.'../images/wz/'.$row['wz_document'].'" style="color:#044996">'.$row['wz_document'].'</a>)' : '').'</p>';
											}
											
											if ($validators->validateTextarea($row['order_comment']))
											{
												$details .= '<p><strong>Komentarz zamawiającego:</strong></p>
												<p>'.nl2br($row['order_comment']).'</p>';
											}
											
											if ($validators->validateTextarea($row['merchant_information']))
											{
												$details .= '<p><strong>Komentarz handlowca:</strong></p>
												<p>'.nl2br($row['merchant_information']).'</p>';
											}
												
											$details .= '</div>
										</div>';
										
										$transport_brutto = sprintf("%0.2f", $prices->getTransportPrice((($_SESSION["b2b_user"]['transport'] == '') ? $weight : 0), $row['brutto_value'], $pallets, $payment));
										$transport_netto  = sprintf("%0.2f", (100*$transport_brutto/122));
										$transport_tax = sprintf("%0.2f", (22*($transport_netto)/100));
									
										$total_sum = sprintf("%0.2f", ($row['brutto_value'] + $transport_brutto));
										
										$xml .= '<tr>
											<td class="bg_'.(($counter%2)+1).'">'.$orders->formatId($row['id'], $row['query_id']).'</td>
											<td class="bg_'.(($counter%2)+1).'">'.((!empty($row['query_id'])) ? $orders->formatId($row['query_id'], $row['query_id']) : '&nbsp;').'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['department'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['contractor_login'].'</td>
											<td class="bg_'.(($counter%2)+1).'">'.date('Y-m-d', strtotime($row['add_date'])).'</td>
											<td class="bg_'.(($counter%2)+1).'">'.(($row['delivery_date'] != "1970-01-01") ? $row['delivery_date'] : "").'</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['netto_value'].' zł</td>
											<td class="bg_'.(($counter%2)+1).'">'.$row['date_diff'].'</td>
											<td class="bg_'.(($counter%2)+1).'">
											<span style="padding:0px 5px 0px 5px;"><a href="#" class="see_details" id="see_'.$row['id'].'">szczegóły</a></span>
											<span style="padding:0px 5px 0px 5px;"><a target="blank" alt="'.$row['id'].'" href="./include/archive_order.php?id='.$row['id'].'">wydruk</a></span>  
											</td>
										</tr>
										<tr class="order_details" id="details_'.$row['id'].'" style="display:none;">
										<td colspan="9">'.$details.'</td>
										</tr>';
									}
								}
							}
						}
						else
						{
							$xml .= '<tr><td class="bg_2" colspan="9">Brak zamówień spełniających kryteria</td></tr>';
						}
						
						$xml .= '</table>';
					}
				}
				
				$xml .= ']]></content>';
				
				break;
				
			case "summary":
			
				$params = unserialize($_POST["params"]);
			
				$summary = $products->getSummary($params["order"], $params["types"], $params["like"]);
				
				$xml .= '<content><![CDATA[';
	
				while($product = mysql_fetch_array($summary)) 
			   	{
			       		$xml .= '<tr><td>';
			       		
				       	switch ($product['status'])
				       	{
				       		case 1: $xml .= 'oczekujące w Unimet'; break;
				       		case 2: $xml .= 'w trakcie realizacji'; break;
				       		case 3: $xml .= 'w drodze'; break;
				       		case 4: $xml .= 'archiwum'; break;
				       		case 5: $xml .= 'odrzucone'; break;
				       		case 6: $xml .= 'u analityka'; break;
				       		case 7: $xml .= 'u kierownika'; break;
				       		case 8: $xml .= 'weryfikacja w Unimet'; break;
				       		case 9: $xml .= 'u zamawiającego'; break;
				       		case 10: $xml .= 'u administratora'; break;
				       	}
				       	
				       	$xml .= '</td>
						<td><p>'.$orders->formatId($product['order_id'], $product['query_id']).'</p></td>
						<td><p>'.((!empty($product['number'])) ? $product['number'] : "&nbsp;").'</p></td>
						<td><p>'.(($product['query_id'] > 0) ? $orders->formatId($product['query_id'], $product['query_id']) : "&nbsp;").'</p></td>
						<td>'.$product['contractor_login'].'</td>
						<td>'.$product['department'].'</td>
						<td>'.$product['mpk'].'</td>
						<td>'.$product['product'].'</td>
						<td>'.$product['symbol'].'</td>
						<td>'.$product['producer'].'</td>
						<td><p>'.(((int)$product['amount'] == $product['amount']) ? (int)$product['amount'] : $product['amount']).'</p></td>
						<td><p>'.$product['tax'].'</p></td>
						<td><p>'.sprintf("%0.2f", ($product['tax'] * $product['netto_price'] / 100)).'</p></td>
						<td><p>'.$product['netto_price'].'</p></td>
						<td><p>'.sprintf("%0.2f", ($product['netto_price'] * $product['amount'])).'</p></td>
						<td><p>'.sprintf("%0.2f", ($product['brutto_price'] * $product['amount'])).'</p></td>
						<td class="last">'.$product['add_date'].'</td>
						<td class="last">'.((!empty($product['fv'])) ? $product['fv'] : "&nbsp;").'</td>
						<td class="last">'.((!empty($product['wm'])) ? $product['wm'] : "&nbsp;").'</td>
					</tr>';
			   	}
			   	
			   	$xml .= ']]></content>';
			
				break;
		}
	
		break;
}

$xml .= '</response>';
echo gzencode($xml);
?>

<?php
class Informations extends Validators
{
	private $_db;
	private $_user_id = 0;
	private $_login_date;
	
	public function __construct()
	{	
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		
		$user = $this->getSession("b2b_user");
		
		if (is_array($user))
		{
			$this->_user_id = (int)$user['id'];
			$this->_login_date = date('Y-m-d', strtotime($user['login_date']));
		}
	}
	
	public function getInformations($type = 0)
	{
		if ($this->_user_id > 0)
		{
			$result = $this->_db->query('SELECT add_date FROM users WHERE id = '.$this->_user_id) or $this->_db->raise_error();
			
			if (mysql_num_rows($result))
			{
				$details = mysql_fetch_array($result);
				mysql_free_result($result);
				
				$result = $this->_db->query('SELECT communicat_id FROM communicat_details WHERE user_id = '.$this->_user_id) or $this->_db->raise_error();
				
				$exceptions = array();
				
				while($row = mysql_fetch_array($result)) 
		   		{
		       		array_push($exceptions, $row['communicat_id']);
		   		}
				
		   		mysql_free_result($result);

		   		$result = $this->_db->query('SELECT id, title, content, IF(`start` <= \''.$this->_login_date.'\' AND `end` >= \''.$this->_login_date.'\', 1, 0) as controll FROM communicats WHERE `end` >= \''.$this->_login_date.'\' AND `end` >= \''.date('Y-m-d').'\' AND `start` <= \''.$this->_login_date.'\' ORDER BY `start` ASC') or $this->_db->raise_error();
				$list = array();
				
				while($row = mysql_fetch_array($result)) 
		   		{
		   			if ((!in_array($row['id'], $exceptions) && $type == 0) || $type == 1)
		   			{
		       			array_push($list, array('id' => $row['id'], 'title' => $row['title'], 'content' => $row['content'], 'controll' => $row['controll']));
		   			}
		   		}
		   		
		   		return $list;
			}
		}
		
		return false;
	}
	
	public function checkInformation($id)
	{
		$this->_db->query('INSERT INTO communicat_details VALUES (\'\', '.$id.', '.$this->_user_id.')') or $this->_db->raise_error();
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}

$informations = new Informations();
?>
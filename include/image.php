<?php
$id = (((int)$_GET['id']) ? (int)$_GET['id'] : $_GET['uid']);
$width = (int)$_GET['w'];
$height = (int)$_GET['h'];
$type = (((int)$_GET['id']) ? 0 : 1);

include('../include/config.php');
include(BASE_DIR.'/class/class.Image.php');

$image_path = BASE_DIR.'../../cashe/images/'.$width.'/'.$id.(($type) ? '_1' : '').'.jpg';

if (!file_exists($image_path))
{
	if (!is_dir(BASE_DIR.'/../../cashe/images/'.$width))
	{
		mkdir(BASE_DIR.'/../../cashe/images/'.$width);
	}
	
	
	$file_path = BASE_DIR.'../../../b2b.unimet.pl/import_images'.(($type) ? '_dod' : '').'/'.$id.'.xxx';
	
	if (!file_exists($file_path))
	{
		$file_path = BASE_DIR.'/../../cashe/images/no_image.png';
	}
	
	$image = new Image($file_path, $width, $height, $id, $type);
	
	if (file_exists($file_path))
	{
		header('Content-Type: image/jpeg'); 
		echo file_get_contents($image_path);
	}
}
else 
{
	header('Content-Type: image/jpeg'); 
	echo file_get_contents($image_path);
}
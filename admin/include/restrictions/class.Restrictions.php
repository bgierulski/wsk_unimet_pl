<?php
class Restrictions extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_type;
	private $_user_id;
	private $_producer_id;
	private $_product_id;
	private $_group_id;
	private $_first = 0;
	private $_last = 0;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
	}
	
	public function findUsers($key)
	{
		$query = 'SELECT DISTINCT name FROM users WHERE login LIKE \'%'.trim(mysql_escape_string($key)).'%\' OR name LIKE \'%'.trim(mysql_escape_string($key)).'%\' 
		OR email LIKE \'%'.trim(mysql_escape_string($key)).'%\' OR identificator LIKE \'%'.trim(mysql_escape_string($key)).'%\'';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();	
		return $result;
	}
	
	public function findProducts($key)
	{
		$query = 'SELECT product_versions.name FROM products JOIN product_versions ON products.id_hermes = product_versions.product_id WHERE 
		products.id_hermes LIKE \'%'.trim(mysql_escape_string($key)).'%\' OR products.symbol LIKE \'%'.trim(mysql_escape_string($key)).'%\' OR 
		product_versions.name LIKE \'%'.trim(mysql_escape_string($key)).'%\'';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();	
		return $result;
	}
	
	public function getProductGroup($subgroup_id) 
	{
		$result = $this->_db->query('SELECT comment FROM groups_tree WHERE id = '.$subgroup_id) or $this->_db->raise_error();
		
		if (mysql_num_rows($result)) 
		{
			$row = mysql_fetch_array($result);
			
			$data = explode('-', $row['comment']);
			$elements_count = count($data);
			
			$query = 'SELECT name, level FROM groups_tree WHERE comment IN (';
			
			foreach ($data as $key => $element)
			{
				$comment = '';
				
				for ($i=0; $i<($elements_count - $key); $i++)
				{
					$comment .= $data[$i].'-';
				}
				
				$query .= '\''.substr($comment, 0, -1).'\',';
			}
			
			$query = substr($query, 0, -1).') ORDER BY `order` ASC';
			$result = $this->_db->query($query) or $this->_db->raise_error();
			
			return $this->_db->mysql_fetch_all($result);
		}
	}
	
	public function findGroups($key)
	{
		$words = array();
		
		$query = 'SELECT id, comment FROM groups_tree WHERE name LIKE \'%'.trim(mysql_escape_string($key)).'%\' OR  comment LIKE \'%'.trim(mysql_escape_string($key)).'%\'';
		$result = $this->_db->query($query) or $this->_db->raise_error();	
		
		if (mysql_num_rows($result))
		{
			while($row = mysql_fetch_array($result)) 
	   		{
	   			$details = $this->getProductGroup($row['id']);
	   			$name =  '';
					
				foreach ($details as $detail)
				{
					$name .= $detail['name'].' -> ';
				}
					
				$name = substr($name, 0, -4).' ('.trim($row['comment']).')';
				array_push($words, $this->textClear($name));
	   		}
		}
		
		return $words;
	}
	
	public function findProducers($key)
	{
		$query = 'SELECT name FROM producers WHERE name LIKE \'%'.trim(mysql_escape_string($key)).'%\'';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();	
		return $result;
	}
	
	public function deleteRestriction($id)
	{
		$this->_db->query('DELETE FROM groups_tree_changes WHERE id = '.$id) or $this->_db->raise_error();	
	}
	
	public function getRestrictions($key)
	{
		$query = 'SELECT groups_tree_changes.id, users.login, users.name, users.identificator, element_type, IF (element_type = 0, (SELECT name FROM product_versions WHERE product_id = 
		element_id AND version_id = 1), IF (element_type = 1, (SELECT name FROM groups_tree WHERE id = element_id), (SELECT name FROM producers WHERE id = 
		element_id))) as element_name FROM groups_tree_changes JOIN users ON groups_tree_changes.user_id = users.id WHERE users.name LIKE 
		\'%'.trim(mysql_escape_string($key)).'%\' OR users.identificator LIKE \'%'.trim(mysql_escape_string($key)).'%\' ';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function validateRestriction()
	{
		$this->clear();
		
		$this->_type = $values['type'] = trim($_POST['type']);
		
		switch ($this->_type)
		{
			case 1:
				
				$group = $values['group'] = $this->textClear($_POST['group']);
				$data = explode('(', $group);
				$details = explode(')', $data[count($data)-1]);
				
				$query = 'SELECT id, `order`, level FROM groups_tree WHERE comment = \''.trim(mysql_escape_string($details[0])).'\'';
				$result = $this->_db->query($query) or $this->_db->raise_error();
				
				if (mysql_num_rows($result))
				{
					$row = mysql_fetch_array($result);
					$this->_group_id = $row['id'];
					$this->_first = $row['order'];
					
					$query = 'SELECT `order` FROM groups_tree WHERE level <= '.$row['level'].' AND id > '.$row['id'].' ORDER BY `order` ASC LIMIT 1';
					
					mysql_free_result($result);
					$result = $this->_db->query($query) or $this->_db->raise_error();
					
					if (mysql_num_rows($result))
					{
						$row = mysql_fetch_array($result);
						$this->_last = $row['order'];
					}
					else
					{
						$this->_last = 100;
					}
				}
				else
				{
					$this->_errors++;
					$errors['group'] = 'Grupa o podanej nazwie nie istnieje';
				}
				
				break;
				
			case 2:
				
				$producer = $values['producer'] = $this->textClear($_POST['producer']);
				
				$query = 'SELECT id FROM producers WHERE name = \''.trim(mysql_escape_string($producer)).'\'';
				$result = $this->_db->query($query) or $this->_db->raise_error();
				
				if (mysql_num_rows($result))
				{
					$row = mysql_fetch_array($result);
					$this->_producer_id = $row['id'];
				}
				else
				{
					$this->_errors++;
					$errors['producer'] = 'Producent o podanej nazwie nie istnieje';
				}
				
				break;
				
			case 3:
				$product = $values['product'] = $this->textClear($_POST['product']);
				
				$query = 'SELECT id FROM product_versions WHERE name = \''.trim(mysql_escape_string($product)).'\'';
				$result = $this->_db->query($query) or $this->_db->raise_error();
				
				if (mysql_num_rows($result))
				{
					$row = mysql_fetch_array($result);
					$this->_product_id = $row['id'];
				}
				else
				{
					$this->_errors++;
					$errors['product'] = 'Produkt o podanej nazwie nie istnieje';
				}
				
				break;
		}
		
		$user = $values['user'] = trim(mysql_escape_string($_POST['user']));
		
		$query = 'SELECT id FROM users WHERE name = \''.trim(mysql_escape_string($user)).'\' AND id_hermes = parent_id LIMIT 1';
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
				
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			$this->_user_id = $row['id'];
		}
		else
		{
			$this->_errors++;
			$errors['user'] = 'Użytkownik o podanej nazwie nie istnieje';
		}
		
		if (!$this->_type)
		{
			$this->_errors++;
			$errors['type'] = 'Typ ograniczenia jest wymagany';
		}
		
		if (!$this->_errors)
		{
			if (isset($_POST['add_restriction']))
			{
				$this->_addRestriction();
			}
		}
		else 
		{
			$this->setSession("values", $values);
			$this->setSession("errors", $errors);
		}
	}
	
	private function _addRestriction()
	{
		if ($this->_type == 3)
		{
			$this->_type = 0;
		}
		
		$query = 'SELECT parent_id FROM users WHERE id = '.$this->_user_id;
		$result = $this->_db->query($query) or $this->_db->raise_error();
		
		if (mysql_num_rows($result))
		{
			$row = mysql_fetch_array($result);
			$query = 'SELECT id FROM users WHERE parent_id = '.$row['parent_id'];
			
			mysql_free_result($result);
			$result = $this->_db->query($query) or $this->_db->raise_error();
			
			while($row = mysql_fetch_array($result)) 
	   		{
	   			switch ($this->_type)
	   			{
	   				case 1:
	   					$query = 'INSERT INTO groups_tree_changes VALUES (\'\', '.$row['id'].', 1, '.$this->_group_id.', \''.$this->_first.'\', \''.$this->_last.'\', 0)';
	   					break;
	   					
	   				case 2:
	   					$query = 'INSERT INTO groups_tree_changes VALUES (\'\', '.$row['id'].', 2, '.$this->_producer_id.', 0, 0, 0)';
	   					break;
	   					
	   				default:
	   					$query = 'INSERT INTO groups_tree_changes VALUES (\'\', '.$row['id'].', 0, '.$this->_product_id.', 0, 0, 0)';
	   					break;
	   			}
	   			
	   			$this->_db->query($query) or $this->_db->raise_error();
	   		}
		}
		
		$communicats['ok'] = 'Ograniczenie zostało dodane';
		$this->setSession("communicats", $communicats);
		header("Location: ../restrictions/list");
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}
?>
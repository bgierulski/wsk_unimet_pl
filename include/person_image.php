<?php
$person_id = (int)$_GET['id'];
$person_image = $_GET['file'];
$width = (int)$_GET['w'];
$height = (int)$_GET['h'];

include('../include/config.php');
include(BASE_DIR.'/class/class.Image.php');

$image_path = BASE_DIR.'../images/merchants/'.$person_id.'/'.$person_image;
$data = explode('.', $image_path);
$extension = strtolower($data[count($data)-1]);

switch ($extension)
{
	case 'png': header('Content-Type: image/png'); break;
	case 'gif': header('Content-Type: image/gif'); break;
	default: header('Content-Type: image/jpeg');   break;
}

if (file_exists($image_path))
{
	$image = new Image($image_path, $width, $height, 0, 0);
}
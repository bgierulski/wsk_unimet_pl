<?php
class Documents extends Validators 
{
	private $_db;
	private $_errors;
	
	private $_user = array();
	private $_parent_id = 0;
	
	public function __construct()
	{
		$this->_db = new db_mysql(HOST, LOGIN, PASSWORD, DATABASE);
		
		$this->_user = $this->getSession('b2b_user');
	}
	
	private function _getParent($user_id)
	{
		$result = $this->_db->query('SELECT id_hermes FROM users WHERE id_hermes = parent_id AND id_hermes != 0 AND id = '.$user_id) or $this->_db->raise_error();
		
		if (!mysql_num_rows($result))
		{
			mysql_free_result($result);
			$result = $this->_db->query('SELECT parent_id FROM user_tree WHERE user_id = '.$user_id) or $this->_db->raise_error();
			$row = mysql_fetch_array($result);
			
			$this->_getParent($row['parent_id']);
		}
		else
		{
			$row = mysql_fetch_array($result);
			$this->_parent_id = $row['id_hermes'];
		}
	}
	
	public function getDocuments($details = array())
	{
		$this->_getParent($this->_user['id']);
		
		$query = 'SELECT id, document, add_date, matureness, charge, type FROM factures WHERE user_id = '.$this->_parent_id;
		
		if (true == $details['date'])
		{
			$query .= ' AND matureness < \''.date('Y-m-d').'\' AND charge > 0';
		}
		else if (true == $details['charge'])
		{
			$query .= ' AND charge > 0 AND cleared = 0';
		}
		
		$result = $this->_db->query($query) or $this->_db->raise_error();
		return $this->_db->mysql_fetch_all($result);
	}
	
	public function __destruct()
	{
		$this->_db->close();
	}
}

$documents = new Documents();
?>